//
//  AppDelegate.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/16.
//

#import <UIKit/UIKit.h>
#import "WXApi.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate, WXApiDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

