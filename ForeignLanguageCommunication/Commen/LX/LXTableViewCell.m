//
//  LXTableViewCell.m
//  ZLFun
//
//  Created by 李林轩 on 2017/7/4.
//  Copyright © 2017年 Hello World. All rights reserved.
//

#import "LXTableViewCell.h"

@implementation LXTableViewCell


-(void)cellActionWithName:(NSString*)strName withObject:(id)obj withSender:(id)sender{
    
    if ([self.clickDelegate respondsToSelector:@selector(getCellSelectName:withObject:withSender:)]) {
        [self.clickDelegate getCellSelectName:strName withObject:obj withSender:sender];
    }
    
}


@end
