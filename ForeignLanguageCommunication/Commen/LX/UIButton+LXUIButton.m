
//
//  UIButton+LXUIButton.m
//  扩展基类
//
//  Created by 李林轩 on 16/1/31.
//  Copyright © 2016年 李林轩. All rights reserved.
//

#import "UIButton+LXUIButton.h"

@implementation UIButton (LXUIButton)

@dynamic textColor;
@dynamic text;
@dynamic fontSize;

/**
 * btn属性:背景色，text
 */
-(void)getBackgroundColor:(UIColor*)color text:(NSString *)text textColor:(UIColor*)textColor{
    self.backgroundColor=color;
    [self setTitle:text forState:UIControlStateNormal];
    [self setTitleColor:textColor forState:UIControlStateNormal];

}
/**
 * btn属性:背景色，图片
 */
-(void)getBackgroundColor:(UIColor*)color image:(NSString *)imageStr{
    self.backgroundColor=color;
    [self setImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];

}
/**
 * btn属性:背景色，图片和点击切换图片
 */
-(void)getBackgroundColor:(UIColor*)color image:(NSString *)imageStr selectedImge:(NSString*)imageSelected{
    self.backgroundColor=color;
    [self setImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:imageSelected] forState:UIControlStateSelected];

}
/*
 * btn-layer-圆角属性：
 *
 */
-(void)getCornerRadius:(CGFloat)cornerRadius masksToBounds:(BOOL)masksToBounds {
    self.layer.cornerRadius=cornerRadius;
    self.layer.masksToBounds=masksToBounds;
    
}
/*
 * btn-layer-边属性：
 *
 */
-(void)getBorderColor:(UIColor*)borderColor borderWidth:(CGFloat)borderWidth {
    self.layer.borderColor=borderColor.CGColor;
    self.layer.borderWidth=borderWidth;
    
}
/**
 * btn-font 字体
 *
 */
-(void)setFontSize:(CGFloat)fontSize{
    
    if (IS_IPHONE_5) {
        fontSize = fontSize-1;
    }else if (IS_IPHONE_6||IS_IPHONE_X){
        fontSize = fontSize+1;
    }else if (IS_IPHONE_6PLUS){
        fontSize = fontSize+2;
    }else if (IS_IPHONE_Xr1||IS_IPHONE_Xr2){
        fontSize = fontSize+2;
    }else if (IS_IPHONE_Xs_Max){
        fontSize = fontSize+3;
        
    }
    
    self.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:fontSize];
    
}

/**
 * btn 设置文字
 *
 */
-(void)setText:(NSString *)text{
    
    [self setTitle:text forState:UIControlStateNormal];
    
}

/**
 * btn 设置文字颜色
 *
 */
-(void)setTextColor:(UIColor *)textColor{
    
    [self setTitleColor:textColor forState:UIControlStateNormal];
    
}
/**
 * btn 点击事件
 *
 */
-(void)addTarget:(id)target action:(SEL)action{
    
    [self addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
}





@end
