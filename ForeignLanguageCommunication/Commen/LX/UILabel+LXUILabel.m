//
//  UILabel+LXUILabel.m
//  扩展基类
//
//  Created by 李林轩 on 16/1/31.
//  Copyright © 2016年 李林轩. All rights reserved.
//

#import "UILabel+LXUILabel.h"

@implementation UILabel (LXUILabel)

/*
 * labe属性：背景色，字体背景色，字体大小，位置，
 *
 */
-(void)getTextColor:(UIColor*)color backgroundColor:(UIColor*)backColor textAlignment:(NSInteger)textAlignment fontSize:(CGFloat)fontSize text:(NSString*)text{
    
    if (IS_IPHONE_5) {
        fontSize = fontSize-1;
    }
    if (IS_IPHONE_6PLUS) {
        fontSize = fontSize+1;
    }
    
    self.textColor=color;
    self.textAlignment=textAlignment;
    self.font=[UIFont fontWithName:@"PingFangSC-Regular" size:fontSize];
   
    self.text=text;
    if (backColor==nil) {
        backColor=[UIColor clearColor];
    }else{
    self.backgroundColor=backColor;
    }
    
}

/*
 * labe属性：背景色，字体背景色，字体大小，位置，行数，自动适配
 *
 */
-(void)getTextColor:(UIColor*)color backgroundColor:(UIColor*)backColor textAlignment:(NSInteger)textAlignment fontSize:(CGFloat)fontSize lines:(NSInteger)numlines autoFontSizeBOOL:(BOOL)textFontSize text:(NSString*)text{
    self.backgroundColor=backColor;
    self.textColor=color;
    self.textAlignment=textAlignment;
    if (iosSystemVersion>=9.0) {
        self.font=[UIFont fontWithName:@"PingFangSC-Light" size:fontSize];
    }else{
        self.font=[UIFont systemFontOfSize:fontSize];
        
    }
    self.numberOfLines=0;
    self.adjustsFontSizeToFitWidth=textFontSize;
    self.text=text;

}

/*
 * labe-layer-圆角属性：
 *
 */
-(void)getCornerRadius:(CGFloat)cornerRadius masksToBounds:(BOOL)masksToBounds {
    self.layer.cornerRadius=cornerRadius;
    self.layer.masksToBounds=masksToBounds;

}
/*
 * labe-layer-边属性：
 *
 */
-(void)getBorderColor:(UIColor*)borderColor borderWidth:(CGFloat)borderWidth {
    self.layer.borderColor=borderColor.CGColor;
    self.layer.borderWidth=borderWidth;
    
}






@end
