//
//  UIButton+LXUIButton.h
//  扩展基类
//
//  Created by 李林轩 on 16/1/31.
//  Copyright © 2016年 李林轩. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (LXUIButton)
/**
 * btn属性:背景色，text
 */
-(void)getBackgroundColor:(UIColor*)color text:(NSString *)text textColor:(UIColor*)textColor;

/**
 * btn属性:背景色，图片
 */
-(void)getBackgroundColor:(UIColor*)color image:(NSString *)imageStr;
/**
 * btn属性:背景色，图片和点击切换图片
 */
-(void)getBackgroundColor:(UIColor*)color image:(NSString *)imageStr selectedImge:(NSString*)imageSelected;

/**
 * btn-layer-圆角属性：
 *
 */
-(void)getCornerRadius:(CGFloat)cornerRadius masksToBounds:(BOOL)masksToBounds;
/**
 * btn-layer-边属性：
 *
 */
-(void)getBorderColor:(UIColor*)borderColor borderWidth:(CGFloat)borderWidth;

/**
 * btn-fontSize 字体大小
 *
 */
@property(nonatomic,assign)CGFloat fontSize;
/**
 * btn 设置文字
 *
 */
@property(nonatomic,copy)NSString *text;
/**
 * btn 设置文字颜色
 *
 */
@property(nonatomic,strong)UIColor *textColor;
/**
 * btn 点击事件
 *
 */
-(void)addTarget:(id)target action:(SEL)action;

@end
