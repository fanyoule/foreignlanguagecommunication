//
//  UIView+LXPosition.h
//  扩展基类
//
//  Created by 李林轩 on 16/1/31.
//  Copyright © 2016年 李林轩. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LXPosition)
@property(nonatomic)CGFloat rightLX;//控件右边
@property(nonatomic)CGFloat bottomLX;//控件下面
@property(nonatomic)CGFloat xCenterLX;//控件x一半
@property(nonatomic)CGFloat yCenterLX;//控件的y一半
@property(nonatomic)CGFloat xLX;//控件x
@property(nonatomic)CGFloat yLX;//控件的y
@property(nonatomic)CGFloat widthLX;//控件宽
@property(nonatomic)CGFloat heighLX;//控件的高
@property(nonatomic,weak)UIViewController *viewController;//用于跳转
+ (NSString *)toString;
/*
 * 跳转控制器
 *
 */
-(void)pushViewController:(NSString*)ViewController param:(NSDictionary*)param;
/*
 * labe-初始化：
 *
 */
+(id)initX:(CGFloat)x y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height;
/**
 * -初始化：
 *
 **/
+(id)init;

/**
 * -点击动画：
 *
 **/
-(void)startAnimations;

/**
 * -点击动画：设置缩放倍数
 *
 **/
-(void)startAnimations:(float)duration;

/**
 * 给视图绘制添加阴影效果
 *
 **/

-(void)shadowRadius:(CGFloat)radius shadowOpacity:(CGFloat)opacity size:(CGSize)sizeMake color:(UIColor*)color;

/**
 * 给视图添加圆角
 *
 **/
-(void)cornerRadii:(CGSize)size Corners:(UIRectCorner)Corners;
@end
