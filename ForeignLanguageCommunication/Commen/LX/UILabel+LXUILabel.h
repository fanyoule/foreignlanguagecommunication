//
//  UILabel+LXUILabel.h
//  扩展基类
//
//  Created by 李林轩 on 16/1/31.
//  Copyright © 2016年 李林轩. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (LXUILabel)
/**
 * labe属性：背景色，字体背景色，字体大小，位置，
 */
-(void)getTextColor:(UIColor*)color backgroundColor:(UIColor*)backColor textAlignment:(NSInteger)textAlignment fontSize:(CGFloat)fontSize text:(NSString*)text;

/**
 * labe属性：背景色，字体背景色，字体大小，位置，行数，自动适配
 *
 */
-(void)getTextColor:(UIColor*)color backgroundColor:(UIColor*)backColor textAlignment:(NSInteger)textAlignment fontSize:(CGFloat)fontSize lines:(NSInteger)numlines autoFontSizeBOOL:(BOOL)textFontSize text:(NSString*)text;

/**
 * labe-layer-圆角属性：
 *
 */
-(void)getCornerRadius:(CGFloat)cornerRadius masksToBounds:(BOOL)masksToBounds;
/**
 * labe-layer-边属性：
 *
 */
-(void)getBorderColor:(UIColor*)borderColor borderWidth:(CGFloat)borderWidth;

@end
