//
//  LXTableViewCell.h
//  ZLFun
//
//  Created by 李林轩 on 2017/7/4.
//  Copyright © 2017年 Hello World. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LXViewClickProtocol <NSObject>
/**
 name：cell类名
 obj：参数
 sender:
 **/
-(void)getCellSelectName:(NSString *)name withObject:(id)obj withSender:(id)sender;

@end
@interface LXTableViewCell : UITableViewCell
@property(nonatomic,weak)id<LXViewClickProtocol> clickDelegate;
-(void)cellActionWithName:(NSString*)strName withObject:(id)obj withSender:(id)sender;
@end
