//
//  NSArray+ObjectIndex.m
//  ZLFun
//
//  Created by 李林轩 on 17/1/18.
//  Copyright © 2017年 Hello World. All rights reserved.
//

#import "NSArray+ObjectIndex.h"

@implementation NSArray (ObjectIndex)


- (id)objectAtIndexCheck:(NSUInteger)index
{
    if (index >= [self count]) {
        return nil;
    }
    
    id value = [self objectAtIndex:index];
    if (value == [NSNull null]) {
        return nil;
    }
    return value;
}

@end
