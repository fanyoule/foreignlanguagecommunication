//
//  UIView+LXPosition.m
//  扩展基类
//
//  Created by 李林轩 on 16/1/31.
//  Copyright © 2016年 李林轩. All rights reserved.
//

#import "UIView+LXPosition.h"

@implementation UIView (LXPosition)
@dynamic rightLX;
@dynamic bottomLX;
@dynamic xCenterLX;
@dynamic yCenterLX;
@dynamic xLX;
@dynamic yLX;
@dynamic widthLX;
@dynamic heighLX;
@dynamic viewController;
-(CGFloat)rightLX{
    return CGRectGetMaxX(self.frame);

}
-(CGFloat)bottomLX{
    return CGRectGetMaxY(self.frame);
}
-(CGFloat)xCenterLX{
    return  CGRectGetMidX(self.frame);
}
-(CGFloat)yCenterv{

    return CGRectGetMidY(self.frame);
}
-(CGFloat)xLX{
    return self.frame.origin.x;
}
-(CGFloat)yLX{
    return self.frame.origin.y;
}
-(CGFloat)widthLX{
    return self.frame.size.width;

}
-(CGFloat)heighLX{
    return self.frame.size.height;

}
+ (NSString *)toString {
    return NSStringFromClass([self class]);
}
/*
 * labe-初始化：
 *
 */
+(id)initX:(CGFloat)x y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height{
    
    return  [[self alloc]initWithFrame:CGRectMake(x, y, width, height)];
    
}
- (UIViewController *)viewController {

    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}
/*
 * 跳转控制器
 *
 */
-(void)pushViewController:(NSString*)ViewController param:(NSDictionary*)param{
    
    id pushVC = [[NSClassFromString(ViewController) alloc]init];
    if (param) {
        for (NSString *key in param.allKeys) {
            [pushVC setValue:param[key] forKey:key];
        }
    }
    [self.viewController.navigationController pushViewController:pushVC animated:YES];
    
    
    
}
/*
 * -初始化：
 *
 */
+(id)init{
    return  [[self alloc]init];
    
}

/*
 * -点击动画：
 *
 */
-(void)startAnimations{
    
    [UIView animateWithDuration:0.08 animations:^{
        self.layer.transform = CATransform3DMakeScale(0.8, 0.8, 1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            self.layer.transform = CATransform3DMakeScale(1, 1, 1);
        }];
        
    }];
    
}
/*
 * -点击动画：设置缩放倍数
 *
 */
-(void)startAnimations:(float)duration{
    
    [UIView animateWithDuration:0.08 animations:^{
        self.layer.transform = CATransform3DMakeScale(duration, duration, 1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            self.layer.transform = CATransform3DMakeScale(1, 1, 1);
        }];
        
    }];
    
}

/*
 * 给视图绘制添加阴影效果
 *
 **/

-(void)shadowRadius:(CGFloat)radius shadowOpacity:(CGFloat)opacity size:(CGSize)sizeMake color:(UIColor*)color{
    
    self.layer.shadowColor = color.CGColor;//shadowColor阴影颜色
    self.layer.shadowOffset = sizeMake;//shadowOffset阴影偏移，默认(0, -3),这个跟shadowRadius配合使用
    self.layer.shadowOpacity = opacity;//阴影透明度，默认0
    self.layer.shadowRadius = radius;//阴影半径，默认3
    
    //路径阴影
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    float width = self.bounds.size.width;
    float height = self.bounds.size.height;
    float x = self.bounds.origin.x;
    float y = self.bounds.origin.y;
    float addWH = 10;
    
    CGPoint topLeft      = self.bounds.origin;
    CGPoint topMiddle = CGPointMake(x+(width/2),y-addWH);
    CGPoint topRight     = CGPointMake(x+width,y);
    
    CGPoint rightMiddle = CGPointMake(x+width+addWH,y+(height/2));
    
    CGPoint bottomRight  = CGPointMake(x+width,y+height);
    CGPoint bottomMiddle = CGPointMake(x+(width/2),y+height+addWH);
    CGPoint bottomLeft   = CGPointMake(x,y+height);
    
    
    CGPoint leftMiddle = CGPointMake(x-addWH,y+(height/2));
    
    [path moveToPoint:topLeft];
    //添加四个二元曲线
    [path addQuadCurveToPoint:topRight
                 controlPoint:topMiddle];
    [path addQuadCurveToPoint:bottomRight
                 controlPoint:rightMiddle];
    [path addQuadCurveToPoint:bottomLeft
                 controlPoint:bottomMiddle];
    [path addQuadCurveToPoint:topLeft
                 controlPoint:leftMiddle];
    //设置阴影路径
    self.layer.shadowPath = path.CGPath;
    
}
/**
 * 给视图添加圆角
 *
 **/
-(void)cornerRadii:(CGSize)size Corners:(UIRectCorner)Corners{
    
    UIBezierPath * maskPath = [UIBezierPath bezierPathWithRoundedRect:self.layer.bounds byRoundingCorners:Corners cornerRadii:size];
    CAShapeLayer * maskLayer = [CAShapeLayer new];
    maskLayer.frame = self.layer.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}






@end
