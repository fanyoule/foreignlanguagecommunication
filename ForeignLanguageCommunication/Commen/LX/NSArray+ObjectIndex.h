//
//  NSArray+ObjectIndex.h
//  ZLFun
//
//  Created by 李林轩 on 17/1/18.
//  Copyright © 2017年 Hello World. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (ObjectIndex)
/*!
 @method objectAtIndexCheck:
 @abstract 检查是否越界和NSNull如果是返回nil
 @result 返回对象
 */
- (id)objectAtIndexCheck:(NSUInteger)index;

@end
