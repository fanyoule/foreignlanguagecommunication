//
//  CityModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CityModel : NSObject
@property (nonatomic, strong) NSArray *area;
@property (nonatomic, copy) NSString *name;
@end

NS_ASSUME_NONNULL_END
