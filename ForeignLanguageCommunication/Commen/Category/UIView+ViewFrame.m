//
//  UIView+ViewFrame.m
//  VoiceLive
//
//  Created by mac on 2020/8/28.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "UIView+ViewFrame.h"

@implementation UIView (ViewFrame)
- (CGFloat)x{
    return self.frame.origin.x;
}
- (CGFloat)y{
    return self.frame.origin.y;
}
- (CGFloat)width{
    return self.bounds.size.width;
}
- (CGFloat)height{
    return self.bounds.size.height;
}
- (CGFloat)centerX{
    return self.center.x;
}
-(CGFloat)centerY{
    return self.center.y;
}
- (CGFloat)top{
    return self.y;
}
- (CGFloat)left{
    return self.x;
}
- (CGFloat)bottom{
    return self.y+self.height;
}
- (CGFloat)right{
    return self.x+self.width;
}
- (CGSize)size {
    return self.frame.size;
}



- (void)setX:(CGFloat)x{
    CGRect rect = self.frame;
    rect.origin.x = x;
    self.frame = rect;
}
- (void)setY:(CGFloat)y{
    CGRect rect = self.frame;
    rect.origin.y = y;
    self.frame = rect;
}
- (void)setWidth:(CGFloat)width{
    CGRect rect = self.frame;
    rect.size.width = width;
    self.frame = rect;
}
- (void)setHeight:(CGFloat)height{
    CGRect rect = self.frame;
    rect.size.height = height;
    self.frame = rect;
}
- (void)setCenterX:(CGFloat)centerx{
    CGPoint p = self.center;
    p.x = centerx;
    self.center = p;
}
- (void)setCenterY:(CGFloat)centery{
    CGPoint p = self.center;
    p.y = centery;
    self.center = p;
}
- (void)setSize:(CGSize)size {
    self.width = size.width;
    self.height = size.height;
}


- (void)addSubviews:(NSSet *)objects {
    [objects enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
       
        UIView *view = (UIView *)obj;
        [self addSubview:view];
        
        
    }];
}
- (void)setCornerWithRadius:(CGFloat)radius {
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
}
- (void)setCornerWithRadius:(CGFloat)radius borderWidth:(CGFloat)width borderColor:(UIColor *)color {
    self.layer.cornerRadius = radius;
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = width;
    self.layer.masksToBounds = YES;
}
@end
