//
//  NSString+Custom.h
//  MHBaseFramework
//
//  Created by mahong on 15/11/29.
//  Copyright © 2015年 mahong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <UIKit/UIKit.h>

@interface NSString (Custom)

/**
 *  MD5 加密
 *
 *  @return 加密后字符串
 */
- (NSString *)md5String;

/**
 *  sha1 加密
 *
 *  @return 加密后字符串
 */
- (NSString *)sha1String;


/**
 *  URL 编码
 *
 *  @return 编码字符串
 */
- (NSString *)stringByURLEncode;

/**
 *  URL 解码
 *
 *  @return 解码字符串
 */
- (NSString *)stringByURLDecode;

/**
 *  获取当前文本的size
 *
 *  @param font    字体大小
 *  @param maxSize 最大的约束size
 *
 *  @return 返回文本的size
 */
- (CGSize)sizeForFont:(UIFont *)font maxSize:(CGSize)maxSize;

/**
 *  已知文本宽度，获取文本高度
 *
 *  @param font  字体
 *  @param width 宽度
 *
 *  @return 高度
 */
- (CGFloat)heightForFont:(UIFont *)font width:(CGFloat)width;

/**
 *  已知文本高度，获取文本宽度
 *
 *  @param font   字体
 *  @param height 高度
 *
 *  @return 宽度
 */
- (CGFloat)widthForFont:(UIFont *)font height:(CGFloat)height;

+ (NSString *)getTimeString:(NSString *)timeInterVal;
/**
 *  去除字符串首尾空格
 *
 *  @return 字符串
 */
- (NSString *)stringByTrim;

/**
 *  判断字符串是否为空  nil, @"", @"  ", @"\n"
 *
 *  @return YES:不为空
 */
- (BOOL)isNotBlank;

/**
 *  判断字符串知否包含子串
 *
 *  @param string 子串
 *
 *  @return YES：包含指定字符串
 */
- (BOOL)containsString:(NSString *)string;

/**
 *  字符串转换为NSData
 *
 *  @return nsdata
 */
- (NSData *)dataValue;

/**
 *  Json字符串转化字典
 *
 *  @return 字典
 */
- (NSDictionary *)dictionaryFromJsonStr;

/**
 *  判断是否为邮箱格式
 *
 *  @return bool
 */
- (BOOL)isEmail;

- (BOOL)isPhoneNumber;

- (BOOL)isFixLine;

/**
 *  Base64 加密
 *
 *  @return 加密字符串
 */
- (NSString *)encodeToBase64;

/**
 *  Base64 解密
 *
 *  @return 解密字符串
 */
- (NSString *)decodeBase64;
//判断是否含有表情符号 yes-有 no-没有
+ (BOOL)stringContainsEmoji:(NSString *)string;
//判断第三方键盘中的表情
+ (BOOL)hasEmoji:(NSString*)string;
//去除表情
+ (NSString *)disableEmoji:(NSString *)text;
//返回字符串首字母
+ (NSString *)Returntheinitials:(NSString *)text;
//返回时间戳
+ (long long)getTimeStampFromString:(NSString *)dataString;
//时间戳转字符串 时间
+ (NSString *)getTimeStringStyle2ByStamps:(long)timeStamp;
//去除float后面无效的0
+ (NSString *)changeFloatWithFloat:(CGFloat)floatValue;

+ (NSString *)changeFloatWithString:(NSString *)stringFloat;
@end
