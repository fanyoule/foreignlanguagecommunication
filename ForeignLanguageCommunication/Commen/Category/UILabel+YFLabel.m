//
//  UILabel+YFLabel.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/8/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "UILabel+YFLabel.h"

@implementation UILabel (YFLabel)

+ (instancetype)initLabelTextFont:(CGFloat)font textColor:(UIColor *)textColor title:(NSString *)text {
    
    UILabel *label = [[UILabel alloc] init];
    label.text = text;
    label.font = [UIFont systemFontOfSize:font];
    label.textColor = textColor;
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    return label;
}

- (CGFloat)getHeightLineWithString:(NSString *)string withWidth:(CGFloat)width withFont:(CGFloat)font {
    
    //1.1最大允许绘制的文本范围
    CGSize size = CGSizeMake(width, 2000);
    //1.2配置计算时的行截取方法,和contentLabel对应
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:10];
    //1.3配置计算时的字体的大小
    //1.4配置属性字典
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:font], NSParagraphStyleAttributeName:style};
    //2.计算
    //如果想保留多个枚举值,则枚举值中间加按位或|即可,并不是所有的枚举类型都可以按位或,只有枚举值的赋值中有左移运算符时才可以
    CGFloat height = [string boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height;
    
    return height;
}
- (void)oneStr:(NSString *)string1 twoStr:(NSString *)string2 {
    NSString *tempStr = [NSString stringWithFormat:@"%@%@", string1, string2];
    // 1.创建一个富文本
    NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:tempStr];
    NSInteger len = tempStr.length - 1;
    [attri addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, len)];
    [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, len)];

    // 2.添加表情图片
    NSTextAttachment *attch = [[NSTextAttachment alloc] init];
    // 表情图片
    attch.image = [UIImage imageNamed:@"pinglun_hf"];
    // 设置图片大小
    attch.bounds = CGRectMake(0, -1, 11, 11);

    // 创建带有图片的富文本
    NSAttributedString *str = [NSAttributedString attributedStringWithAttachment:attch];
    [attri insertAttributedString:str atIndex:string1.length];// 插入某个位置
    // 用label的attributedText属性来使用富文本
    self.attributedText = attri;
}


@end
