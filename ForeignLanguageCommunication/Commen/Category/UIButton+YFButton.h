//
//  UIButton+YFButton.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/8/4.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (YFButton)

/**
 创建文字btn
 */
+ (instancetype)initButtonTitleFont:(CGFloat)font titleColor:(UIColor *)color titleName:(NSString *)name;

- (void)setButtonTitleFont:(CGFloat)font titleColor:(UIColor *)color titleName:(NSString *)name;

/**
创建图片btn
*/
+ (instancetype)initButtonImageName:(NSString *)imageName;

/**
 创建文字图片btn
 */
+ (instancetype)initButtonTitleFont:(CGFloat)font titleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backColor imageName:(NSString *)imageName titleName:(NSString *)titleName;

- (void)setButtonTitleFont:(CGFloat)font titleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backColor imageName:(NSString *)imageName titleName:(NSString *)titleName;


/**
 创建文字圆角btn
 */
+ (instancetype)initButtonTitleFont:(CGFloat)font titleColor:(UIColor *)titleColor titleName:(NSString *)titleName backgroundColor:(UIColor *)backColor radius:(CGFloat)radius;

- (void)setButtonTitleFont:(CGFloat)font titleColor:(UIColor *)titleColor titleName:(NSString *)titleName backgroundColor:(UIColor *)backColor radius:(CGFloat)radius;

@end

NS_ASSUME_NONNULL_END
