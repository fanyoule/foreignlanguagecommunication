//
//  UIButton+YFButton.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/8/4.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "UIButton+YFButton.h"

@implementation UIButton (YFButton)

+ (instancetype)initButtonTitleFont:(CGFloat)font titleColor:(UIColor *)color titleName:(NSString *)name {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:name forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn setTitleColor:[color colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:font]];
    return btn;
}

- (void)setButtonTitleFont:(CGFloat)font titleColor:(UIColor *)color titleName:(NSString *)name {
    
    [self setTitle:name forState:UIControlStateNormal];
    [self setTitleColor:color forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont systemFontOfSize:font]];
}

// 创建图片btn
+ (instancetype)initButtonImageName:(NSString *)imageName {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    return btn;
}

// 创建文字图片btn
+ (instancetype)initButtonTitleFont:(CGFloat)font titleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backColor imageName:(NSString *)imageName titleName:(NSString *)titleName {
    
    titleName = titleName == nil ? @"" : titleName;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setTitle:titleName forState:UIControlStateNormal];
    [btn setTitleColor:titleColor forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:font]];
    btn.backgroundColor = backColor;
    return btn;
}

- (void)setButtonTitleFont:(CGFloat)font titleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backColor imageName:(NSString *)imageName titleName:(NSString *)titleName {
    
    [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [self setTitle:titleName forState:UIControlStateNormal];
    [self setTitleColor:titleColor forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont systemFontOfSize:font]];
}


// 创建文字圆角btn
+ (instancetype)initButtonTitleFont:(CGFloat)font titleColor:(UIColor *)titleColor titleName:(NSString *)titleName backgroundColor:(UIColor *)backColor radius:(CGFloat)radius {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:titleName forState:UIControlStateNormal];
    [btn setTitleColor:titleColor forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:font]];
    btn.backgroundColor = backColor;
    if (radius > 0) {
        btn.layer.cornerRadius = radius;
        btn.layer.masksToBounds = YES;
    }
    return btn;
}

- (void)setButtonTitleFont:(CGFloat)font titleColor:(UIColor *)titleColor titleName:(NSString *)titleName backgroundColor:(UIColor *)backColor radius:(CGFloat)radius {
    
    [self setTitle:titleName forState:UIControlStateNormal];
    [self setTitleColor:titleColor forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont systemFontOfSize:font]];
    self.backgroundColor = backColor;
    if (radius > 0) {
        self.layer.cornerRadius = radius;
        self.layer.masksToBounds = YES;
    }
}

@end
