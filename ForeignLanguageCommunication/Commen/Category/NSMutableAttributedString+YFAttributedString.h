//
//  NSMutableAttributedString+YFAttributedString.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/8/4.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (YFAttributedString)

+ (NSAttributedString *)attributedStringWithMessage:(NSString *)message paragraphSpacing:(CGFloat)spacing lineSpacing:(CGFloat)lineSpace firstStr:(NSString *)firstStr secendStr:(NSString *)secendStr thirdStr:(NSString *)thirdStr;

+ (NSAttributedString *)attributedStringWithMessage:(NSString *)message paragraphSpacing:(CGFloat)spacing lineSpacing:(CGFloat)lineSpace strArray:(NSArray <NSString *> *)strArray;
@end

NS_ASSUME_NONNULL_END
