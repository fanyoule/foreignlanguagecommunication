//
//  UILabel+YFLabel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/8/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (YFLabel)

/**
 创建标准标签
 */
+ (instancetype)initLabelTextFont:(CGFloat)font textColor:(UIColor *)textColor title:(NSString *)text;

/** 根据字符串算label高度*/
- (CGFloat)getHeightLineWithString:(NSString *)string withWidth:(CGFloat)width withFont:(CGFloat)font;
- (void)oneStr:(NSString *)string1 twoStr:(NSString *)string2;
@end

NS_ASSUME_NONNULL_END
