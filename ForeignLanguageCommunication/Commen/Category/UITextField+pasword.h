//
//  UITextField+pasword.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/10/17.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol GTTextFieldDelegate <UITextFieldDelegate>

@optional
- (void)textFieldDidDeleteBackward:(UITextField *)textField;

@end
@interface UITextField (pasword)

@property (weak, nonatomic) id <GTTextFieldDelegate> delegate;
/**
 *  监听删除按钮
 *  object:UITextField
 */
extern NSString * const GTTextFieldDidDeleteBackwardNotification;
@end

NS_ASSUME_NONNULL_END
