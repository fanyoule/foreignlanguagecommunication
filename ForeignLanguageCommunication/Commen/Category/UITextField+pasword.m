//
//  UITextField+pasword.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/10/17.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "UITextField+pasword.h"
#import <objc/runtime.h>

NSString * const GTTextFieldDidDeleteBackwardNotification = @"textfield_did_notification";
@implementation UITextField (pasword)
+ (void)load {
    Method method1 = class_getInstanceMethod([self class], NSSelectorFromString(@"deleteBackward"));
    Method method2 = class_getInstanceMethod([self class], @selector(yx_deleteBackward));
    method_exchangeImplementations(method1, method2);
}

- (void)yx_deleteBackward {
    [self yx_deleteBackward];
    
    if ([self.delegate respondsToSelector:@selector(textFieldDidDeleteBackward:)])
    {
        id <GTTextFieldDelegate> delegate  = (id<GTTextFieldDelegate>)self.delegate;
        [delegate textFieldDidDeleteBackward:self];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:GTTextFieldDidDeleteBackwardNotification object:self];
}

@end
