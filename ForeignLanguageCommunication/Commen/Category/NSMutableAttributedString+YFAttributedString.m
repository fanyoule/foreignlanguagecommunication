//
//  NSMutableAttributedString+YFAttributedString.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/8/4.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "NSMutableAttributedString+YFAttributedString.h"

@implementation NSMutableAttributedString (YFAttributedString)

+ (NSAttributedString *)attributedStringWithMessage:(NSString *)message paragraphSpacing:(CGFloat)spacing lineSpacing:(CGFloat)lineSpace firstStr:(NSString *)firstStr secendStr:(NSString *)secendStr thirdStr:(NSString *)thirdStr {
     
    // 设置属性
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    // 设置行间距
    paragraphStyle.paragraphSpacing = spacing; // 段落间距
    paragraphStyle.lineSpacing = lineSpace; // 行间距
    NSDictionary *attributes = @{
                                 NSForegroundColorAttributeName:CustomColor(@"999999"),
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithString:message attributes:attributes];
    [attrStr addAttributes:@{
                             NSLinkAttributeName:firstStr
                             }
                     range:[message rangeOfString:firstStr]];
    [attrStr addAttributes:@{
                             NSLinkAttributeName:secendStr
                             }
                     range:[message rangeOfString:secendStr]];
    [attrStr addAttributes:@{
            NSLinkAttributeName:thirdStr
            }
    range:[message rangeOfString:thirdStr]];
    return attrStr;
}

+ (NSAttributedString *)attributedStringWithMessage:(NSString *)message paragraphSpacing:(CGFloat)spacing lineSpacing:(CGFloat)lineSpace strArray:(NSArray <NSString *> *)strArray{

    // 设置属性
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    // 设置行间距
    paragraphStyle.paragraphSpacing = spacing; // 段落间距
    paragraphStyle.lineSpacing = lineSpace; // 行间距
    NSDictionary *attributes = @{
                                 NSForegroundColorAttributeName:CustomColor(@"999999"),
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithString:message attributes:attributes];
        
    for (int i = 0; i < strArray.count; i ++) {
        NSString *str = strArray[i];
        [attrStr addAttributes:@{NSLinkAttributeName: str}
                            range:[message rangeOfString:str]];
    }
        
    return attrStr;
}
@end
