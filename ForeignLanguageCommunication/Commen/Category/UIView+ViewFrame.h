//
//  UIView+ViewFrame.h
//  VoiceLive
//
//  Created by mac on 2020/8/28.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (ViewFrame)
- (CGFloat)x;
- (CGFloat)y;
- (CGFloat)width;
- (CGFloat)height;
- (CGFloat)right;
- (CGFloat)left;
- (CGFloat)top;
- (CGFloat)bottom;
- (CGSize)size;
- (CGFloat)centerX;
- (CGFloat)centerY;

- (void)setX:(CGFloat)x;
- (void)setY:(CGFloat)y;
- (void)setWidth:(CGFloat)width;
- (void)setHeight:(CGFloat)height;
- (void)setCenterX:(CGFloat)centerx;
- (void)setCenterY:(CGFloat)centery;
- (void)setSize:(CGSize)size;

- (void)addSubviews:(NSSet *)objects;
///圆角
- (void)setCornerWithRadius:(CGFloat)radius;
///圆角加边框
- (void)setCornerWithRadius:(CGFloat)radius borderWidth:(CGFloat)width borderColor:(UIColor *)color;


@end

NS_ASSUME_NONNULL_END
