//
//  YKSearchTextField.m
//  VoiceLive
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "YKSearchTextField.h"

@interface YKSearchTextField () <UITextFieldDelegate>

@property (nonatomic, strong) UIView *placeHolderView;
@property (nonatomic, strong) UILabel *placeHolderLabel;
@property (nonatomic, strong) UIImageView *searchIconView;

@property (nonatomic, assign) BOOL activity;



@end

@implementation YKSearchTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init {
    if (self = [super init]) {
        [self setUpSubView];
    }
    return self;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)setUpSubView{
    [self addSubview:self.placeHolderView];
    [self addSubview:self.inputView];
    [self.placeHolderView addSubview:self.placeHolderLabel];
    [self.placeHolderView addSubview:self.searchIconView];
    self.backgroundColor = CustomColor(@"#E5E5E7");
    [self addObserver];
}

- (UITextField *)inputView {
    if (!_inputView) {
        _inputView = [[UITextField alloc] init];
        _inputView.delegate = self;
        _inputView.font = kFont(14);
        _inputView.textColor = CustomColor(@"#222222");

        _inputView.clearButtonMode = UITextFieldViewModeAlways;
        _inputView.returnKeyType = UIReturnKeySearch;
    }
    return _inputView;
}

- (UIView *)placeHolderView {
    if (!_placeHolderView) {
        _placeHolderView = UIView.new;
    }
    return _placeHolderView;
}

- (UILabel *)placeHolderLabel {
    if (!_placeHolderLabel) {
        _placeHolderLabel = UILabel.new;
        _placeHolderLabel.font = kFont(14);
        _placeHolderLabel.textColor = CustomColor(@"#999999");
    }
    return _placeHolderLabel;
}

- (UIImageView *)searchIconView {
    if (!_searchIconView) {
        _searchIconView = [[UIImageView alloc] initWithImage:kImage(@"icon_home_seartch_black")];
        [_searchIconView sizeToFit];
    }
    return _searchIconView;
}
- (void)setSearchIcon:(NSString *)searchIcon {
    _searchIcon = searchIcon;
    self.searchIconView.image = kImage(searchIcon);
}

- (void)setFont:(UIFont *)font {
    _font = font;
    self.inputView.font = font;
}
- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    self.inputView.textColor = textColor;
}

- (void)setTintColor:(UIColor *)tintColor {
    _tintColor = tintColor;
    self.inputView.tintColor = tintColor;
}

- (void)setPlaceHolderFont:(UIFont *)placeHolderFont {
    _placeHolderFont = placeHolderFont;
    self.placeHolderLabel.font = placeHolderFont;
}
- (void)setPlaceHolderColor:(UIColor *)placeHolderColor {
    _placeHolderColor = placeHolderColor;
    self.placeHolderLabel.textColor = placeHolderColor;
}

- (void)setPlaceHolder:(NSString *)placeHolder {
    _placeHolder = placeHolder;
    self.placeHolderLabel.text = placeHolder;
}
- (void)addObserver {
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}
- (void)keyboardWillShow:(NSNotification *)notice {
    
}
- (void)textDidChange:(NSNotification *)notice {
    if (self.inputView.text.length) {
        self.placeHolderLabel.hidden = YES;
    }else{
        self.placeHolderLabel.hidden = NO;
    }
    if ([self.delegate respondsToSelector:@selector(searchViewDIdChangeText:)]) {
        [self.delegate searchViewDIdChangeText:self.inputView];
    }
}
#pragma mark textfield delegate
//有文字的时候 隐藏占位文字  占位到左边  无文字 时，激活状态 左边  未激活  根据枚举确定位置  显示占位文字
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //开始输入
    self.activity = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchViewDidBeginEdit:)]) {
        [self.delegate searchViewDidBeginEdit:textField];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField {

    self.activity = NO;
    //判断是否有文字 没有 显示占位文字  有 隐藏占位 搜索图标位置不变
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchViewDidEndEdit:)]) {
        [self.delegate searchViewDidEndEdit:textField];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(searchViewDidClickSearchKey:)]) {
        [self.delegate searchViewDidClickSearchKey:textField];
    }
    return YES;
}



- (void)setActivity:(BOOL)activity {
    _activity = activity;
    if (activity) {
        if (self.inputView.text.length>0) {
            return;
        }
        CGRect rect = self.placeHolderView.frame;
        rect.origin.x = self.leftMargin;
        
        
        [UIView animateWithDuration:0.3 animations:^{
            self.placeHolderView.frame = rect;
            self.inputView.tintColor = UIColor.clearColor;
        } completion:^(BOOL finished) {
            
            if (self.tintColor) {
                self.inputView.tintColor = self.tintColor;
            }
            else self.inputView.tintColor = CustomColor(@"#222222");
        }];
    }
    else {
        if (self.placeHolderPosition == SearchViewPlaceHolderPositionCenter) {
            //没有文字 回到中间位置
            //有文字 不变
            if (self.inputView.text.length) {
                return;
            }
            else {
                [UIView animateWithDuration:0.3 animations:^{
                           self.placeHolderView.center = CGPointMake(self.width / 2, self.height / 2);
                           self.inputView.tintColor = UIColor.clearColor;
                       } completion:^(BOOL finished) {
                           if (self.tintColor) {
                               self.inputView.tintColor = self.tintColor;
                           }
                           else self.inputView.tintColor = CustomColor(@"#2222222");
                       }];
                
            }
        }
    }
}



- (void)layoutSubviews {
    [super layoutSubviews];
    //如果是输入状态 直接返回
    //图片 文字  边距
    self.placeHolderPadding = self.placeHolderPadding == 0  ? 5:self.placeHolderPadding;
    //图标与左边的边距  圆角半径  or  设定好的边距
    self.leftMargin = self.leftMargin == 0 ? self.layer.cornerRadius : self.leftMargin;
    
    
    //输入框的登高 左 留出图标的边距 右 留出圆角
    self.inputView.frame = CGRectMake(self.leftMargin+self.searchIconView.width+self.placeHolderPadding, 0, self.width - (self.leftMargin+self.searchIconView.width+self.placeHolderPadding), self.height);
    
    if (self.activity) {
        return;
    }
    if (self.inputView.text.length) {
        return;
    }
    if (!self.placeHolder) {
        self.placeHolderLabel.frame = CGRectZero;
    }else {
        if (!self.placeHolderFont) {
            self.placeHolderLabel.font = self.inputView.font;
        }
        
        [self.placeHolderLabel sizeToFit];
    }
    //图片 文字  边距
    self.placeHolderPadding = self.placeHolderPadding == 0  ? 5:self.placeHolderPadding;
    //图标与左边的边距  圆角半径  or  设定好的边距
    self.leftMargin = self.leftMargin == 0 ? self.layer.cornerRadius : self.leftMargin;
    
    self.placeHolderView.bounds = CGRectMake(0, 0, self.searchIconView.width+self.placeHolderLabel.width+self.placeHolderPadding, self.height);
    
    self.searchIconView.frame = CGRectMake(0, (self.placeHolderView.height-self.searchIconView.height)/2, self.searchIconView.width, self.searchIconView.height);
    self.placeHolderLabel.frame = CGRectMake(self.searchIconView.width+self.placeHolderPadding, (self.placeHolderView.height-self.placeHolderLabel.height)/2, self.placeHolderLabel.width, self.placeHolderLabel.height);
    
    
    if (self.placeHolderPosition == SearchViewPlaceHolderPositionCenter) {
        //占位  在中间
        
        self.placeHolderView.center = CGPointMake(self.width / 2, self.height / 2);
    }else {
        self.placeHolderView.x = self.leftMargin;
        self.placeHolderView.y = 0;
    }
    
}


@end
