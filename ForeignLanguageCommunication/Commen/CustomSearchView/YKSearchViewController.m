//
//  YKSearchViewController.m
//  VoiceLive
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//
/*
 push or present a new searchVC
 push searchBar on navBar without finish button
 present searchbar at top with finish button
 */
#import "YKSearchViewController.h"


@interface YKSearchViewController ()<YKSearchBarDelegate>


@end

@implementation YKSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpSubView];
    
}
- (void)setUpSubView {
    
    if (self.navigationController.childViewControllers.count > 1) {
        [self.navView addSubview:self.searchBar];
        
        self.searchBar.alwaysShowFinish = YES;
        self.searchBar.searchTextView.layer.cornerRadius = 35 / 2;
        self.searchBar.backgroundColor = UIColor.clearColor;
        [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(30);
            make.right.mas_offset(0);
            make.top.mas_offset(SBarHeight+4.5);
            make.height.mas_equalTo(35);
            
        }];
    }else {
        self.navView.hidden = YES;
        [self.view addSubview:self.searchBar];
        self.searchBar.backgroundColor = UIColor.clearColor;
        self.searchBar.alwaysShowFinish = YES;
        self.searchBar.searchTextView.layer.cornerRadius = 35 / 2;
        self.searchBar.searchTextView.placeHolderPosition = SearchViewPlaceHolderPositionCenter;
        [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.right.mas_offset(0);
            make.top.mas_offset(SBarHeight+4.5);
            make.height.mas_equalTo(35);
        }];
    }
}

- (YKSearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [YKSearchBar new];
        _searchBar.delegate = self;
        _searchBar.backgroundColor = CustomColor(@"#E5E5E7");
        _searchBar.backgroundColor = UIColor.whiteColor;
        _searchBar.alwaysShowFinish = YES;
        [_searchBar.finishButton setTitle:@"取消" forState:UIControlStateNormal];
        _searchBar.searchTextView.tintColor = CustomColor(@"#181818");
        _searchBar.searchTextView.placeHolderPadding = 7;
        _searchBar.searchTextView.placeHolder = @"搜索";
    }
    return _searchBar;
}


- (void)searchBarBeginEdit:(UITextField *)textField{
   
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
