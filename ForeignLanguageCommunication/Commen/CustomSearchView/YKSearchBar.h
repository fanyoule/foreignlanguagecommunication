//
//  YKSearchBar.h
//  VoiceLive
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKSearchTextField.h"


@protocol YKSearchBarDelegate <NSObject>

@optional
- (void)finishSearch:(UITextField *)textField;
- (void)searchBarBeginEdit:(UITextField *)textField;
- (void)searchBarDidEndEdit:(UITextField *)textField;


@end
NS_ASSUME_NONNULL_BEGIN
//未激活 输入框 边距0  激活 显示按钮
@interface YKSearchBar : UIView<SearchViewDelegate>
/** 输入框*/
@property (nonatomic, strong) YKSearchTextField *searchTextView;
/** 完成按钮*/
@property (nonatomic, strong) UIButton *finishButton;
/** 内边距*/
@property (nonatomic, assign) UIEdgeInsets margin;

/** 按钮与输入框间距*/
@property (nonatomic, assign) CGFloat paddingToButton;
/** 总是显示完成按钮*/
@property (nonatomic, assign) BOOL alwaysShowFinish;
@property (nonatomic, weak) id <YKSearchBarDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
