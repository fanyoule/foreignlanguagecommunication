//
//  YKSearchTextField.h
//  VoiceLive
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchViewDelegate <NSObject>

@optional
- (void)searchViewDidBeginEdit:(UITextField *)textField;
- (void)searchViewDidEndEdit:(UITextField *)textField;

- (void)searchViewDidClickSearchKey:(UITextField *)textfield;
- (void)searchViewDIdChangeText:(UITextField *)textField;
@end

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, SearchViewPlaceHolderPosition) {
    SearchViewPlaceHolderPositionDefault,
    SearchViewPlaceHolderPositionCenter,
    
};

@interface YKSearchTextField : UIView

@property (nonatomic, strong) UITextField *inputView;

@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, strong) UIColor *placeHolderColor;
/** 光标颜色 */
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, strong) UIFont *font;
/** 占位文字字体 */
@property (nonatomic, strong) UIFont *placeHolderFont;


@property (nonatomic, copy) NSString *placeHolder;

/** 图标与占位文字之间距离 */
@property (nonatomic, assign) CGFloat placeHolderPadding;
/** 搜索icon左边边距 */
@property (nonatomic, assign) CGFloat leftMargin;

@property (nonatomic, assign) SearchViewPlaceHolderPosition placeHolderPosition;

@property (nonatomic, weak) id <SearchViewDelegate> delegate;

@property (nonatomic, copy) NSString *searchIcon;

@end

NS_ASSUME_NONNULL_END
