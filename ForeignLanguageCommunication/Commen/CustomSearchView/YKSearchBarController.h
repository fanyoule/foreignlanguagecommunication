//
//  YKSearchBarController.h
//  VoiceLive
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**
 
 继承自系统
 */
@interface YKSearchBarController : UISearchController
/**输入框背景色*/
@property (nonatomic, strong) UIColor *textFieldColor;
/**取消按钮文字颜色*/
@property (nonatomic, strong) UIColor *cancelColor;
/**搜索图标*/
@property (nonatomic, strong) UIImage *searchIcon;
/**占位文字*/
@property (nonatomic, copy) NSString *placeHolder;
/**是否居中*/
@property (nonatomic, assign) BOOL placeHolderCenter;
/**取消按钮文字*/
@property (nonatomic, copy) NSString *cancelTitle;

/** 此时是否居中*/
- (void)changePlaceHolderCenterPosition:(BOOL)isCenter;

@end

NS_ASSUME_NONNULL_END
