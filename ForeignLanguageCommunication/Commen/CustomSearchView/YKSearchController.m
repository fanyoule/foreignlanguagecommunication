//
//  YKSearchController.m
//  VoiceLive
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "YKSearchController.h"

@interface YKSearchController ()

@end

@implementation YKSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)shouldChangePlaceHolderCenter:(BOOL)isCenter {
    if (isCenter) {
        
        CGRect barFrame = self.navView.frame;
        barFrame.origin.y = -SNavBarHeight;
        CGRect tableFrame = self.mainTableView.frame;
        tableFrame.origin.y = SBarHeight;
        tableFrame.size.height = self.view.height - SBarHeight;
        self.navView.frame = barFrame;
        
        CGFloat top = self.mainTableView.tableHeaderView.height;
        [self.mainTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_offset(SBarHeight);
            make.bottom.left.right.mas_equalTo(self.view);
        }];
    }
    else {
        
        self.navView.y = 0;
        self.mainTableView.y = SNavBarHeight;
        self.mainTableView.height = self.view.height - SNavBarHeight;
    }
}
- (YKSearchBarController *)searchVC {
    if (!_searchVC) {
        _searchVC = [[YKSearchBarController alloc] initWithSearchResultsController:nil];
        _searchVC.searchBar.delegate = self;
        _searchVC.searchResultsUpdater = self;
        _searchVC.dimsBackgroundDuringPresentation = NO;
        _searchVC.cancelColor = CustomColor(@"#181818");
        _searchVC.cancelTitle = @"取消";
        _searchVC.placeHolder = @"输入昵称";
        _searchVC.placeHolderCenter = NO;
        _searchVC.textFieldColor = CustomColor(@"#E5E5E7");
        _searchVC.searchIcon = kImage(@"icon_home_search");
        
        _searchVC.searchBar.returnKeyType = UIReturnKeySearch;
    }
    return _searchVC;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
