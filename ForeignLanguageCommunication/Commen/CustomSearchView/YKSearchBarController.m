//
//  YKSearchBarController.m
//  VoiceLive
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "YKSearchBarController.h"



@interface YKSearchBarController ()
@property (nonatomic, strong) UITextField *textF;
/**居中时 偏移的X值*/
@property (nonatomic, assign) CGFloat iconOffX;

@end

@implementation YKSearchBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (instancetype)initWithSearchResultsController:(UIViewController *)searchResultsController {
    if (self = [super initWithSearchResultsController:searchResultsController]) {
        self.textF.layer.cornerRadius = 18;
        self.textF.clipsToBounds = YES;
        [self.searchBar setBackgroundColor:UIColor.clearColor];
        [self.searchBar setBackgroundImage:[UIImage new]];//去掉上下黑线
        [self.searchBar setBarTintColor:UIColor.whiteColor];
        self.iconOffX = 0;
    }
    return self;
}

- (void)setSearchIcon:(UIImage *)searchIcon {
    _searchIcon = searchIcon;
    [self.searchBar setImage:searchIcon forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
}
- (void)setCancelColor:(UIColor *)cancelColor {
    _cancelColor = cancelColor;
    self.searchBar.tintColor = cancelColor;
}
- (void)setPlaceHolder:(NSString *)placeHolder {
    _placeHolder = placeHolder;
    self.searchBar.placeholder = placeHolder;
    
}
- (void)setTextFieldColor:(UIColor *)textFieldColor {
    _textFieldColor = textFieldColor;
    
    if (self.textF) {
        self.textF.backgroundColor = textFieldColor;
    }
}
- (void)setCancelTitle:(NSString *)cancelTitle {
    _cancelTitle = cancelTitle;
    
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTitle:cancelTitle];
}

- (void)setPlaceHolderCenter:(BOOL)placeHolderCenter {
    //居中
    _placeHolderCenter = placeHolderCenter;
    if (placeHolderCenter) {
        //
        UILabel *label = UILabel.new;
        label.font = self.textF.font;
        label.text = self.placeHolder;
        [label sizeToFit];
        CGSize size = label.size;
        
        CGFloat placeW = size.width+10+5;
        CGFloat offX = ((KSW - 30) - placeW) / 2;//图标居中时的X值
        [self.searchBar setPositionAdjustment:UIOffsetMake(offX, 0) forSearchBarIcon:UISearchBarIconSearch];//
        self.iconOffX = offX;
    }
    
}
- (UITextField *)textF {
    if (!_textF) {
        _textF = [Util getSubView:self.searchBar class:@"UISearchBarTextField"];
    }
    return _textF;
}

- (void)changePlaceHolderCenterPosition:(BOOL)isCenter {
    if (isCenter) {
        [self.searchBar setPositionAdjustment:UIOffsetMake(self.iconOffX, 0) forSearchBarIcon:UISearchBarIconSearch];//
    }else {
        [self.searchBar setPositionAdjustment:UIOffsetMake(0, 0) forSearchBarIcon:UISearchBarIconSearch];//
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
