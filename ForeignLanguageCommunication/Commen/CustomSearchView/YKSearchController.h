//
//  YKSearchController.h
//  VoiceLive
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "BaseViewController.h"
#import "YKSearchBarController.h"
NS_ASSUME_NONNULL_BEGIN

@interface YKSearchController : BaseViewController <UISearchBarDelegate,UISearchResultsUpdating>
/**需要自己声明新的*/
@property (nonatomic, strong) YKSearchBarController *searchVC;
/** 在代理中调用 修改占位居中状态*/
- (void)shouldChangePlaceHolderCenter:(BOOL)isCenter;

//@property (nonatomic, assign) BOOL shouldRecover;
@end

NS_ASSUME_NONNULL_END
