//
//  YKSearchBar.m
//  VoiceLive
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "YKSearchBar.h"

@interface YKSearchBar ()
/**是否输入状态 */
@property (nonatomic, assign) BOOL activity;
@property (nonatomic, copy) NSString *searchText;
@end

@implementation YKSearchBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init {
    if (self = [super init]) {
        [self setUpSubView];
    }
    return self;
}

- (void)setUpSubView {
    [self addSubview:self.searchTextView];
    [self addSubview:self.finishButton];
}


//输入框 带圆角
- (YKSearchTextField *)searchTextView {
    if (!_searchTextView) {
        _searchTextView = [[YKSearchTextField alloc] init];
        
        _searchTextView.delegate = self;
    }
    return _searchTextView;
}

- (UIButton *)finishButton {
    if (!_finishButton) {
        _finishButton = [UIButton buttonWithType:UIButtonTypeCustom];
        // 默认 无背景色 主色调文字 完成  14font
        [_finishButton setTitle:@"完成" forState:UIControlStateNormal];
        [_finishButton setTitleColor:CustomColor(@"#181818") forState:UIControlStateNormal];
        _finishButton.titleLabel.font = kFont(14);
        [_finishButton addTarget:self action:@selector(clickFinish:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _finishButton;
}


- (CGFloat)paddingToButton {
    if (_paddingToButton < 5) {
        return 5;
    }
    return _paddingToButton;
}
- (void)clickFinish:(UIButton *)sender {
    //完成事件
     [self endEditing:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(finishSearch:)]) {
        [self.delegate finishSearch:self.searchTextView.inputView];
    }
   
}
- (void)searchViewDidClickSearchKey:(UITextField *)textfield{
    [self endEditing:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchBarDidEndEdit:)]) {
        [self.delegate searchBarDidEndEdit:self.searchTextView.inputView];
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
    //显示按钮 激活状态下显示
    if (self.alwaysShowFinish) {
        self.finishButton.hidden = NO;
        [self.finishButton sizeToFit];
        
        self.finishButton.width = self.finishButton.width+30;
        self.finishButton.frame = CGRectMake(self.width-self.finishButton.width, 0, self.finishButton.width, self.height);
        self.searchTextView.frame = CGRectMake(0, 0, self.finishButton.x - self.paddingToButton, self.height);
        return;
    }
    if (self.activity) {
        self.finishButton.hidden = NO;
        [self.finishButton sizeToFit];
        self.finishButton.height = self.height;
        self.finishButton.right = self.width;
        self.finishButton.y = 0;
        
        self.searchTextView.frame = CGRectMake(0, 0, self.finishButton.x - self.paddingToButton, self.height);
    }
    else {
        self.finishButton.hidden = YES;
        self.searchTextView.frame = self.bounds;
    }
    
}

- (void)searchViewDidBeginEdit:(UITextField *)textField {
    self.activity = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchBarBeginEdit:)]) {
        [self.delegate searchBarBeginEdit:textField];
    }
}
- (void)searchViewDidEndEdit:(UITextField *)textField {
    self.activity = NO;
    self.searchText = textField.text;
    if (self.delegate && [self.delegate respondsToSelector:@selector(searchBarDidEndEdit:)]) {
        [self.delegate searchBarDidEndEdit:textField];
    }
}

- (void)setActivity:(BOOL)activity {
    _activity = activity;
    if (self.alwaysShowFinish) {
        return;
    }
    if (_activity) {
        self.finishButton.hidden = NO;
        [self.finishButton sizeToFit];
        
        
        self.finishButton.width = self.finishButton.width+30;
        self.finishButton.frame = CGRectMake(self.width-self.finishButton.width, 0, self.finishButton.width, self.height);
        self.searchTextView.frame = CGRectMake(0, 0, self.finishButton.x - self.paddingToButton, self.height);
    }
    else {
        self.finishButton.hidden = YES;
        self.searchTextView.frame = self.bounds;
    }
}
@end
