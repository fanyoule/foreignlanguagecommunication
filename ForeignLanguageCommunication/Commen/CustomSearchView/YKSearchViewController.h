//
//  YKSearchViewController.h
//  VoiceLive
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//present展示 搜索栏在顶部  否则 在导航栏

#import "BaseViewController.h"
#import "YKSearchBar.h"
NS_ASSUME_NONNULL_BEGIN
/**自定义搜索VC*/
@interface YKSearchViewController : BaseViewController

@property (nonatomic, strong) YKSearchBar *searchBar;


@end

NS_ASSUME_NONNULL_END
