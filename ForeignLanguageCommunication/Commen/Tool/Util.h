//
//  Util.h
//  VoiceLive
//
//  Created by mac on 2020/8/26.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Util : NSObject
+(void)setCornerModelWithView:(UIView *)cornerView model:(UIRectCorner)model cornerRadius:(CGFloat)cornerRadius;
/**
 生成对应颜色,指定尺寸的图片
 */
+ (UIImage *)getImageWithColor:(UIColor *)imageColor size:(CGSize)imageSize;
/**
    指定view生成渐变色图层
 */
+ (void)createGradientLayerWithViwe:(UIView *)gradientView colors:(NSArray<UIColor *> *)colors type:(BOOL)vertical;
/**
    从view中查找指定class的子视图
 */
+ (id)getSubView:(UIView *)mainView class:(NSString *)classStr;
/**
    根据颜色生成图片
    指定高度
 */
+ (UIImage*)GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height;

//MARK: 东八区时间
+ (NSString *)getChinaTime;
/**获取当前的时间戳*/
+ (NSString *)getCurrentTimestamp;
/**时间戳转日期*/
+ (NSString *)timeStampToDateString:(double)timestamp;

/**获取指定路径文件的格式 mimeType*/
+ (NSString *)mimeTypeForFileAtPath:(NSString *)path;
/**MD5加密*/
+ (NSString *)md5:(NSString *)string;
+ (NSString *)createMD5WithString:(NSString *)originString;

/**麦克风请求授权*/
+ (void)requestAudioAuth:(void(^)(void))success fail:(void(^)(void))fail;
/**相机请求授权*/
+ (void)requestCameraAuth:(void(^)(void))success fail:(void(^)(void))fail;
/**八位随机数*/
+ (NSString *)createRandomString;


/**当前的控制器*/
+ (UIViewController *)currentVC;


/**重绘图片 指定尺寸*/
+ (UIImage *)resizeImageWithImage:(UIImage *)image size:(CGSize)size;
/**七牛云 小图片*/
+ (NSURL *)getQNMiniPicWithUrl:(NSString *)url;
@end

NS_ASSUME_NONNULL_END
