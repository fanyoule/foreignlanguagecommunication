//
//  WXApiManager.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/5.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

#define WXPayFinishName @"wxPayFinishName"
@protocol WXApiManagerDelegate <NSObject>

@optional
- (void)managerDidRecvAuthResponse:(SendAuthResp *)response;

@end

NS_ASSUME_NONNULL_BEGIN

@interface WXApiManager : NSObject<WXApiDelegate>

@property (nonatomic, assign) id<WXApiManagerDelegate> delegate;

+ (instancetype)sharedManager;

@end

NS_ASSUME_NONNULL_END
