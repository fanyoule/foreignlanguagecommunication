//
//  Util.m
//  VoiceLive
//
//  Created by mac on 2020/8/26.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "Util.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CommonCrypto/CommonDigest.h>
#import <AVFoundation/AVFoundation.h>

#define MD5_salt @"30c722c6a546ss4306a93a814c9f0a"
@implementation Util

+ (void)setCornerModelWithView:(UIView *)cornerView model:(UIRectCorner)model cornerRadius:(CGFloat)cornerRadius {
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:cornerView.bounds byRoundingCorners:model cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = path.CGPath;
    maskLayer.frame = cornerView.bounds;
    
    cornerView.layer.mask = maskLayer;
}
//根据颜色生成图片
+ (UIImage *)getImageWithColor:(UIColor *)imageColor size:(CGSize)imageSize {
    CGRect rect = CGRectMake(0.0, 0.0, imageSize.width, imageSize.height);
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [imageColor CGColor]);
    CGContextFillRect(context, rect);

    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


+ (void)createGradientLayerWithViwe:(UIView *)gradientView colors:(NSArray<UIColor *> *)colors type:(BOOL)vertical{
    
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
       gradientLayer.frame = gradientView.bounds;
    
    NSMutableArray *mutArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < colors.count; i ++) {
        UIColor *color = colors[i];
        
        [mutArray addObject:(__bridge id)color.CGColor];
    }
    
    gradientLayer.colors = [NSArray arrayWithArray:mutArray];
    if (vertical) {
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(0, 1);
    } else {
        gradientLayer.startPoint = CGPointMake(0, 0);
        gradientLayer.endPoint = CGPointMake(1, 0);
    }
    
    gradientLayer.locations = @[@0,@1];
    [gradientView.layer insertSublayer:gradientLayer atIndex:0];
    
}
//视图的指定类别的子视图
+ (id)getSubView:(UIView *)mainView class:(NSString *)classStr{
    for (UIView *subView in mainView.subviews) {
        
        if ([subView isKindOfClass:NSClassFromString(classStr)]) {
            return subView;
        }
        if (subView.subviews.count>0) {
            UIView *view = [self getSubView:subView class:classStr];
            if (view) {
                return view;
            }
        }
    }
    return nil;
}
//根据颜色 生成图片

+ (UIImage*)getImageWithColor:(UIColor*)color andHeight:(CGFloat)height
{
    CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, r);

    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return img;
}
//MARK: 东八区时间
+ (NSString *)getChinaTime {
    NSDate *utc = [NSDate date];
    NSDateFormatter *formater = [NSDateFormatter new];
    
    formater.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *sss = [formater stringFromDate:utc];
    
    
    
    
    NSTimeZone *timeZ = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    NSDateFormatter *format = [NSDateFormatter new];
    [format setTimeZone:timeZ];
    format.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *date = [NSDate date];
    NSString *timeStr = [format stringFromDate:date];
    
    NSDate *newdate = [format dateFromString:timeStr];
    NSInteger timeS  = [newdate timeIntervalSince1970];
    
    return [NSString stringWithFormat:@"%ld000",timeS];
   
}
// MARK: 获取当前时间戳
+ (NSString *)getCurrentTimestamp {
   
    
    
    NSDate *date = [NSDate date];
    NSInteger timeStamp = [NSNumber numberWithDouble:[date timeIntervalSince1970]].integerValue;
    
    return [NSString stringWithFormat:@"%ld000",timeStamp];
}
// MARK: 时间戳转换
+ (NSString *)timeStampToDateString:(double)timestamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *timeStr = [format stringFromDate:date];
    return timeStr;
}

// MARK: 获取文件的MIMEType

+ (NSString *)mimeTypeForFileAtPath:(NSString *)path {
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef) [path pathExtension], NULL);
    CFStringRef MIMEType = UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
    if (!MIMEType) {
        return @"application/octet-stream";
    }
    return (__bridge NSString *)(MIMEType);
}
// MARK: MD5加密
+ (NSString *)md5:(NSString *)string {
    const char *cStr = [string UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [result appendFormat:@"%02X", digest[i]];
    }
    [result stringByAppendingString:MD5_salt];
    return result;
}
+ (NSString *)createMD5WithString:(NSString *)originString {
    NSString *a = [MD5_salt substringWithRange:NSMakeRange(0, 1)];
    NSString *b = [MD5_salt substringWithRange:NSMakeRange(2, 1)];
    NSString *c = [MD5_salt substringWithRange:NSMakeRange(5, 1)];
    NSString *d = [MD5_salt substringWithRange:NSMakeRange(4, 1)];
    NSString *preString = [NSString stringWithFormat:@"%@%@%@%@%@",a,b,originString,c,d];
    const char *cStr = [preString UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [result appendFormat:@"%02X", digest[i]];
    }
    return result;
}

//权限查询
//MARK: 麦克风请求授权
+ (void)requestAudioAuth:(void(^)(void))success fail:(void(^)(void))fail {
    AVAuthorizationStatus micStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    switch (micStatus) {
        case AVAuthorizationStatusDenied:
            //拒绝
        {
            if (fail) {
                fail();
            }
        }
            break;
           case AVAuthorizationStatusAuthorized:
            //授权
        {
            if (success) {
                success();
            }
        }
            break;
            case AVAuthorizationStatusRestricted:
            //限制性使用
        {
            if (fail) {
                fail();
            }
        }
            break;
            case AVAuthorizationStatusNotDetermined:
            //未查询
        {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
                if (granted) {
                    //允许使用
                    if (success) {
                        success();
                    }
                }else {
                    //不允许
                    if (fail) {
                        fail();
                    }
                }
            }];
        }
            break;
            
        default:
            break;
    }
}

//MARK: 相机请求授权
+ (void)requestCameraAuth:(void(^)(void))success fail:(void(^)(void))fail {
    AVAuthorizationStatus camaraStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (camaraStatus) {
        case AVAuthorizationStatusNotDetermined:
            //未授权
        {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    if (success) {
                        success();
                    }
                }else {
                    if (fail) {
                        fail();
                    }
                }
            }];
        }
            break;
          case AVAuthorizationStatusDenied:
            //拒绝
        {
            if (fail) {
                fail();
            }
        }
            break;
            case AVAuthorizationStatusAuthorized:
            //授权
        {
            if (success) {
                success();
            }
        }
            break;
        default:
            {
                       if (fail) {
                           fail();
                       }
                   }
            break;
    }
}

+ (UIViewController *)currentVC {
    //获取当前顶层控制器
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    //app默认windowLevel是UIWindowLevelNormal，如果不是，找到UIWindowLevelNormal的
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    //    如果是present上来的appRootVC.presentedViewController 不为nil
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
        
        if ([nextResponder isKindOfClass:[UITabBarController class]]){
            UITabBarController * tabbar = (UITabBarController *)nextResponder;
            UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
            //        UINavigationController * nav = tabbar.selectedViewController ; 上下两种写法都行
            result=nav.childViewControllers.lastObject;

        }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
            UIViewController * nav = (UIViewController *)nextResponder;
            result = nav.childViewControllers.lastObject;
        }
        else{
            while (nextResponder) {
                
                if ([nextResponder isKindOfClass:[UIViewController class]] ) {
                    result = nextResponder;
                    break;
                }
                nextResponder = [nextResponder nextResponder];
            }
        }
    }else{
        nextResponder = appRootVC;
        if ([nextResponder isKindOfClass:[UITabBarController class]]){
            UITabBarController * tabbar = (UITabBarController *)nextResponder;
            UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
            //        UINavigationController * nav = tabbar.selectedViewController ; 上下两种写法都行
            result=nav.childViewControllers.lastObject;

        }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
            UIViewController * nav = (UIViewController *)nextResponder;
            result = nav.childViewControllers.lastObject;
        }
        else{
            while (nextResponder) {
                
                if ([nextResponder isKindOfClass:[UIViewController class]] ) {
                    result = nextResponder;
                    break;
                }
                nextResponder = [nextResponder nextResponder];
            }
        }
    }

    
    return result;
}

//MARK: 重绘图片 指定尺寸
+ (UIImage *)resizeImageWithImage:(UIImage *)image size:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *resizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizeImage;
}



+ (NSString *)createRandomString {
        NSTimeInterval random=[NSDate timeIntervalSinceReferenceDate];

        NSLog(@"now:%.8f",random);

        NSString *randomString = [NSString stringWithFormat:@"%.8f",random];

        NSString *randompassword = [[randomString componentsSeparatedByString:@"."]objectAtIndex:1];
        NSLog(@"randompassword:%@",randompassword);
        return randompassword;
}

+ (NSURL *)getQNMiniPicWithUrl:(NSString *)url{
    
    NSString *newUrl = [NSString stringWithFormat:@"%@?imageView2/2/w/400/h/200",url];
    
    return [NSURL URLWithString:newUrl];
    
}
@end
