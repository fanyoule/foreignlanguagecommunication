//
//  UserInfoModel.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//

#import "UserInfoModel.h"

@implementation UserInfoModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}
@end
