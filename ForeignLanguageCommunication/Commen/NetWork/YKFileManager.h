//
//  YKFileManager.h
//  VoiceLive
//
//  Created by mac on 2020/10/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    DocumentPath,
    cachesPath,
    TmpPath,
} YKFileLocation;

@interface YKFileManager : NSObject

@property (nonatomic, copy) NSString *rootHomePath;
// /Library目录
@property(nonatomic, copy) NSString *libraryPath;
// /Documents目录
@property(nonatomic, copy) NSString *documentPath;
// /caches目录
@property(nonatomic, copy) NSString *cachesPath;
// /preference目录
@property(nonatomic, copy) NSString *preferencesPath;
//  /tmp目录
@property(nonatomic, copy)  NSString *tmpPath;

// MARK: 创建文件夹在指定目录
- (NSString *)createDirectoryTo:(YKFileLocation)location WithdirectoryName:(NSString *)name;
// MARK: 写入文件 传入文件路径
- (void)writeDataToDirectoryPath:(NSString *)directoryPath withData:(id)data;
// MARK: - 读取文件 传入文件的路径（包含文件名）
- (id)loadDataWithPath:(NSString *)filePath;

// 判断文件是否存在
- (BOOL)isSxistAtPath:(NSString *)filePath;

/**
 移动文件

 @param oldPath 旧的的path
 @param newPath 新的path
 */
- (void)moveFileNameWithOldPath:(NSString *)oldPath toNewPath:(NSString *)newPath ;

- (void)deleteFileWithfileName:(NSString *)fileName;

// 计算文件大小
- (unsigned long long)fileSizeAtPath:(NSString *)filePath;

// 计算整个文件夹中所有文件大小
- (unsigned long long)folderSizeAtPath:(NSString*)folderPath;

- (NSString *)getDocumentFilePathWith:(NSString *)fileName;

- (void)removeremoveItemAtPath:(NSString *)path;


@end

NS_ASSUME_NONNULL_END
