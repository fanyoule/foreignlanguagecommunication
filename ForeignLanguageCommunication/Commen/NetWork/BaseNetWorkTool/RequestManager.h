//
//  RequestManager.h
//  VoiceLive
//
//  Created by mac on 2020/10/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKHttpRequestManager.h"
#import "UrlBuilder.h"
NS_ASSUME_NONNULL_BEGIN

@interface RequestManager : NSObject
#pragma mark - IM
//互动消息主界面
+ (void)getIMDataWithParameters:(NSMutableDictionary*)parameters IMHost:(NSString *)host UrlString:(NSString*)url Success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
#pragma mark - IMpost
+ (void)postIMDataWithParameters:(NSMutableDictionary*)parameters IMHost:(NSString *)host UrlString:(NSString*)url Success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// MARK: GET不登录(无参数)
+ (void)getDonLogIn:(NSString *)URL withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// MARK: GET不登录(有参数)
+ (void)getDonLogIn:(NSString *)URL incomingParameter:(NSMutableDictionary *)dictionary withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// MARK: GET通用
+ (void)getObtainUrl:(NSString *)Url incomingParameter:(NSMutableDictionary *)dictionary  withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// MARK: POST通用
+ (void)postObtainUrl:(NSString *)Url incomingParameter:(NSMutableDictionary *)dictionary  withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 业务参数
// 查询业务配置
+ (void)getQueryBusinessConfigurationWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;

#pragma mark - 登陆管理

// 验证码登录
+ (void)getLoginByVerifyCodeWithPhone:(NSString *)phone vcode:(NSString *)vcode loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 通过友盟token一键登录
+ (void)getLoginByVerifyCodeWithPhone:(NSString *)youmengToken loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 账号密码登录
+ (void)loginByPwdWithAccount:(NSString *)account passwd:(NSString *)passwd loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// qq登录
+ (void)loginByQQWithOpenId:(NSString *)openId loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 微信登录
+ (void)loginByWeChatWithOpenId:(NSString *)openId loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页查询附近的人
+ (void)nearbyPersonalUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 首页
// 首页排行榜
+ (void)chartsUserId:(long)userId userName:(NSString *)userName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 首页排行榜(个人)
+ (void)chartsSelfUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 外语角界面
+ (void)englishIndexWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 首页匹配类型
+ (void)matchWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 小语种全部匹配类型
+ (void)matchMinWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 交流圈界面
+ (void)momentsIndexWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 首页搜索-用户
+ (void)searchUserWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize userName:(NSString *)userName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 明星教练
+ (void)starCoachWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 投票
+ (void)voteWithUserId:(long)userId toUserId:(long)toUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 世界动态
// 新增世界动态评论
+ (void)addPublishCommentsWithUserId:(long)userId isAuthor:(int)isAuthor publishId:(long)publishId comment:(NSString *)comment commentPic:(NSString *)commentPic withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 新增动态评论回复
+ (void)addPublishCommentsReplyWithTouid:(long)touid uid:(long)uid commentsId:(long)commentsId publishId:(long)publishId replyComment:(NSString *)replyComment replyCommentPic:(NSString *)replyCommentPic withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 新增世界动态
+ (void)addPublishsWithUserId:(long)userId type:(int)type picUrl:(NSString *)picUrl content:(NSString *)content videoCoverUrl:(NSString *)videoCoverUrl videoUrl:(NSString *)videoUrl withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除动态评论
+ (void)deletePublishCommentsById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除动态评论回复
+ (void)deletePublishCommentsReplyById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除世界动态(个人作品)
+ (void)deletePublishsByIds:(NSString *)ids withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页查询动态列表(我的作品列表)
+ (void)getMyPublishListWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 查询世界动态详情
+ (void)getPublishsById:(long)checkUserId Id:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页查询世界动态列表
+ (void)getPublishsListWithPage:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 世界动态点赞/取消点赞(收藏)
+ (void)postPublishsLikeWithId:(long)publishId likedPostId:(long)likedPostId status:(int)status withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页查询某个动态的评论
+ (void)queryPublishCommentsWithId:(long)Id pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - VIP
// VIP信息
+ (void)VIPInfo:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// VIP充值列表
+ (void)VIPRechargeList:(NSString *)platform withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 个人群公告管理
// 新增个人群公告
+ (void)addPersonalClusterNoticeWithId:(long)clusterId userId:(long)userId idStick:(int)idStick title:(NSString *)title content:(NSString *)content withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除个人群公告
+ (void)deletePersonalClusterNoticeById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 查询个人群公告
+ (void)getPersonalClusterNoticeWithId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群公告
+ (void)updatePersonalClusterNoticeId:(long)Id userId:(long)userId idStick:(int)idStick title:(NSString *)title content:(NSString *)content withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 个人群成员管理
// 申请入群，需要审核的接口
+ (void)applyClusterWithId:(long)clusterId userId:(long)userId clusterAuditInfo:(NSString *)clusterAuditInfo withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 群成员列表
+ (void)clusterMembersWithId:(long)Id userId:(long)userId sysId:(NSString *)sysId nickname:(NSString *)nickname withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除群成员/退群
+ (void)deletePersonalClusterMemberByIdWithId:(long)clusterId userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 查询个人群成员
+ (void)getPersonalClusterMemberById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 用户可添加到个人群的好友列表
+ (void)getUserPerClusterAddListWithId:(long)clusterId userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 拉人入群,直接加入群的接口
+ (void)inviteClusterWithId:(long)clusterId userIds:(NSString *)userIds withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 设置管理员
+ (void)updatePersonalClusterMemberWithId:(long)clusterId userId:(long)userId admin:(int)admin withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 个人群管理
// 查询个人群成员
+ (void)IMClusterInfoWithId:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 新增个人群
+ (void)addPersonalClusterWithId:(long)userId clusterName:(NSString *)clusterName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除个人群
+ (void)deletePersonalClusterById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 查询个人群
+ (void)getPersonalClusterById:(long)userId Id:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 查询用户在群信息
+ (void)getUserClusterVoWithId:(long)userId clusterId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 我加入的群组
+ (void)myJoinedClusterWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 搜索个人群
+ (void)searchPersonalClusterWithId:(NSString *)sysId name:(NSString *)name withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 搜索群后查询群信息
+ (void)searchToClusterInfoWithId:(long)userId Id:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId audit:(int)audit auditAnswer:(NSString *)auditAnswer auditQuestion:(NSString *)auditQuestion avatar:(long)avatar chair:(int)chair clusterIntro:(NSString *)clusterIntro clusterName:(NSString *)clusterName gameId:(long)gameId isNoDisturbing:(int)isNoDisturbing isShield:(int)isShield isTop:(int)isTop withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题）
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId audit:(long)audit auditQuestion:(NSString *)auditQuestion auditAnswer:(NSString *)auditAnswer withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题）
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId audit:(long)audit withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群是否开启主席模式（0关闭，1开启）
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId chair:(long)chair withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群是否屏蔽：0屏蔽 1不屏蔽
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId isShield:(long)isShield withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群是否免打扰：0关闭 1开启
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId isNoDisturbing:(long)isNoDisturbing withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群介绍
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId clusterIntro:(NSString *)clusterIntro withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群名称
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId clusterName:(NSString *)clusterName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改个人群名称(是否置顶)
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId isTop:(long)isTop withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;

#pragma mark - 代理管理
// 申请成为代理
+ (void)addAgentWithId:(long)userId agentArea:(NSString *)agentArea content:(NSString *)content name:(NSString *)name phone:(NSString *)phone withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 添加下属
+ (void)addSubordinateWithId:(long)userId subordinateIds:(NSString *)subordinateIds withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 添加下属列表
+ (void)addSubordinateListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 代理信息,根据status判断-1：拒绝，0：待审核，1：正常，2：取消代理
+ (void)agentInfoWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 代理金额明细
+ (void)agentProfitDetailWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 解除代理
+ (void)removeSubordinateWithId:(long)subordinateId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 下属列表
+ (void)subordinateListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 关注/好友
// 关注
+ (void)addFollowsMappingWithId:(long)userId followUserId:(long)followUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 取消关注
+ (void)deleteFollowsMappingWithId:(long)userId followUserId:(long)followUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 粉丝列表
+ (void)fansListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 关注列表
+ (void)followListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 好友列表
+ (void)friendsListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 是否好友
+ (void)isFriendWithId:(long)userId toUserId:(long)toUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 地区城市
// 获得城市信息
+ (void)getCityWithAreaCode:(NSString *)areaCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 获得省份列表
+ (void)getProvinceSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 实时匹配
// 取消实时匹配
+ (void)cancelMatchWithId:(long)userId typeId:(long)typeId chatType:(int)chatType withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 根据类型实时匹配
+ (void)matchByTypeWithId:(long)userId typeId:(long)typeId chatType:(int)chatType avatar:(NSString *)avatar nickname:(NSString *)nickname withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 解锁当次需要消耗的金币
+ (void)unlockIngWithId:(long)userId typeId:(long)typeId chatType:(int)chatType gold:(long)gold type:(int)type avatar:(NSString *)avatar nickname:(NSString *)nickname withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 意见反馈
// 新增用户意见反馈
+ (void)addUserFeedbackWithId:(long)userId contact:(NSString *)contact picUrls:(NSString *)picUrls title:(NSString *)title views:(NSString *)views withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 搜索
// 分页条件查询房间
+ (void)getSearchRoomPages:(int)pageNum pageSize:(int)pageSize roomName:(NSString *)roomName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页条件查询用户
+ (void)getSearchUserPages:(int)pageNum userId:(long)userId pageSize:(int)pageSize userName:(NSString *)userName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页条件搜索个人群
+ (void)searchPersonalClustersPages:(int)pageNum sysId:(NSString *)sysId pageSize:(int)pageSize name:(NSString *)name withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 收藏--动态（作品）、课程、房间
// 新增(取消)收藏---课程、房间
+ (void)addCollectWithId:(long)userId typeId:(long)typeId type:(int)type status:(int)status withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页查询课程收藏列表
+ (void)getCourseCollectListPages:(int)pageNum userId:(long)userId pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页查询动态收藏列表
+ (void)getPublishCollectListPages:(int)pageNum userId:(long)userId pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 教练管理
// 教练认证
+ (void)addCoachWithId:(long)userId applyCrowd:(NSString *)applyCrowd content:(NSString *)content languages:(NSString *)languages name:(NSString *)name phone:(NSString *)phone sex:(NSString *)sex teachingContent:(NSString *)teachingContent withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 教练详情
+ (void)coachDetailWithId:(long)userId coachId:(long)coachId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 教练认证前验证,有返回为已认证教练，无返回为可进行认证教练
+ (void)coachVerifyWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 搜索教练(找教练筛选)
+ (void)searchCoachWithId:(long)userId applyCrowd:(long)applyCrowd price:(NSString *)price name:(NSString *)name pageNum:(int)pageNum pageSize:(int)pageSize teachingContent:(long)teachingContent withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 搜索教练(首页搜索教练，和上面用一个接口)
+ (void)homeSearchCoach:(NSMutableDictionary *)dictionary withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改教练详情
+ (void)updateCoachWithMutableDictionary:(NSMutableDictionary *)dic withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 文件管理
// base64上传文件
+ (void)base64Size:(long)size contentType:(NSString *)contentType file64:(NSString *)file64 fileName:(NSString *)fileName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 根据文件名删除文件
+ (void)deleteByName:(NSString *)fileName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 根据文件id查询文件
+ (void)downloadById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 根据文件名查询文件
+ (void)downloadByName:(NSString *)fileName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 上传文件
+ (void)postFileWithFilepath:(NSString *)path withName:(NSString *)typeName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
//MARK: 上传图片
+ (void)uploadImagesWith:(NSArray<UIImage *> *)imageArray withSuccess:(HttpResponseSuccessBlock)success fail:(HttpResponseFailBlock)fail withProgress:(nonnull DownloadProgressBlock)progress;
// 公用带参数
+ (void)PostMyDataWithParameters:(NSMutableDictionary*)parameters UrlString:(NSString*)url Success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// get带参数
+ (void)getMyDataWithParameters:(NSMutableDictionary*)parameters UrlString:(NSString*)url Success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
#pragma mark - 查询标签
// 根据类型查询标签：0日常爱好  1教学内容   2适用人群   3语种
+ (void)getLabelByType:(long)type withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 用户管理
// 拉黑/取消拉黑用户
+ (void)blackUserWithId:(long)userId blackUserId:(long)blackUserId type:(int)type withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 更换手机
+ (void)changePhoneWithId:(long)userId phone:(NSString *)phone vCode:(NSString *)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 忘记密码
+ (void)forgetPwd:(NSString *)newPwd phone:(NSString *)phone vCode:(NSString *)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 我的黑名单
+ (void)getMyBlackUser:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 个人资料
+ (void)getUserById:(long)userId toUserId:(long)toUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// IM关注
+ (void)imFocus:(long)userId targetUserId:(long)targetUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 个人资料进语音/视频/聊天判断
+ (void)interactCount:(long)userId type:(int)type withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 邀请码邀请
+ (void)inviteUser:(long)Id inviteCode:(NSString *)inviteCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 我的 页面
+ (void)myPage:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 匹配剩余次数
+ (void)remainMatchCount:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 举报（投诉）用户、房间、课程、动态、音乐、教练
+ (void)report:(long)userId reportId:(long)reportId reportType:(int)reportType type:(int)type imageUrl:(NSString *)imageUrl message:(NSString *)message withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改密码
+ (void)updPwd:(long)Id newPwd:(NSString *)newPwd oldPwd:(NSString *)oldPwd withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 用户登陆成功后完善资料或用户资料修改时调用此接口
+ (void)updUser:(long)Id avatar:(NSString *)avatar nickname:(NSString *)nickname area:(NSString *)area birthday:(NSString *)birthday city:(NSString *)city height:(long)height hobby:(NSString *)hobby introduce:(NSString *)introduce loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot occupation:(NSString *)occupation province:(NSString *)province qq:(NSString *)qq sex:(int)sex weight:(int)weight wx:(NSString *)wx withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 解绑微信
+ (void)postUserRomveUserWxSetWithId:(long)userId phone:(NSString *)phone vCode:(NSString *)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 更新/绑定用户的微信
+ (void)postUserUpUserWxSetWithId:(long)userId openId:(NSString *)openId phone:(NSString *)phone vCode:(NSString *)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 用户聊天设置
// 查询用户聊天信息设置
+ (void)getUserChatSetWithId:(long)userId toUserId:(long)toUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改用户聊天设置
+ (void)updateUserChatSetWithId:(long)userId toUserId:(long)toUserId black:(int)black report:(int)report silence:(int)silence top:(int)top withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改用户聊天设置(是否免打扰)
+ (void)updateUserChatSetWithId:(long)userId toUserId:(long)toUserId silence:(int)silence withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;

#pragma mark - 用户聊天设置
// 获取验证码
+ (void)getVerifyCode:(NSString *)phone withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 验证码校验
+ (void)verifyCode:(NSString *)phone vCode:(long)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 系统消息
// 群组消息助手
+ (void)clusterWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 评论消息
+ (void)commentMessageWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程消息
+ (void)coursesMessageWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 处理群组入群通知
+ (void)dealClusterAccessId:(long)Id status:(int)status withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 系统消息主界面
+ (void)indexWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 官方消息
+ (void)officialMessageWithPage:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 消息-房间
+ (void)roomMessageWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 房间在线用户列表
+ (void)roomWealthToDayWithUserId:(NSString *)userId roomNum:(NSString *)roomNum withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 钱包消息
+ (void)walletMessageWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 职业信息
// 获取行业信息
+ (void)getIndustrySuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 获取行业信息
+ (void)getProfessionWithId:(long)industryId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 设置个人禁言
// 新增个人群禁言
+ (void)addPersonalClusterSilenceWithUserId:(long)userId clusterId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 解除个人群禁言
+ (void)deletePersonalClusterSilenceById:(long)userId clusterId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 查询个人群禁言
+ (void)getPersonalClusterSilenceWithClusterId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 课程评价管理
// 新增课程评价
+ (void)addEvaluationWithId:(long)userId courseId:(long)courseId content:(NSString *)content evaluation:(int)evaluation withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除评价
+ (void)delEvaluationById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 查询课程评价
+ (void)getEvaluationById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 邀请码邀请记录
// 邀请记录
+ (void)inviteLogWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 钱包管理
// 发起提现申请(支付宝)
+ (void)alipayAddWalletWithdrawWithId:(long)userId account:(NSString *)account name:(NSString *)name type:(int)type number:(long)number withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 发起提现申请(微信)
+ (void)wxpayAddWalletWithdrawWithId:(long)userId account:(NSString *)account type:(int)type number:(long)number withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail ;


// 获得交易详情
+ (void)getDealDetailWithId:(NSString *)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 钱包余额
+ (void)getWalletByUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 查询钱包交易记录
+ (void)getWalletLogById:(long)userId type:(int)type withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 充值金币
+ (void)rechargeGoldWithUserId:(long)userId accountType:(int)accountType number:(long)number withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 充值金币-支付后回调
+ (void)rechargeGoldPaySuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 充值金币-微信支付后回调
+ (void)rechargeGoldWxPaySuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 充值页面
+ (void)rechargePageWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 频道歌曲
// 上传歌曲
+ (void)musicWithUserId:(long)userId pathId:(long)pathId menuId:(long)menuId type:(int)type singer:(NSString *)singer name:(NSString *)name withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 新增频道歌曲，单个
+ (void)addMusicChannelWithUserId:(long)userId musicId:(long)musicId roomId:(long)roomId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 新增频道歌曲，多个
+ (void)addMusicChannelsWithUserId:(long)userId menuId:(long)menuId roomId:(long)roomId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 用户下麦，删除频道所有歌曲
+ (void)deleteMusicChannelWithUserId:(long)userId roomId:(long)roomId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除频道歌曲
+ (void)deleteMusicChannelById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 分页查询频道歌曲列表
+ (void)getMusicChannelByRoomId:(long)roomId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma mark - 课程管理
// 新增课程
+ (void)addCourseWithUserId:(long)userId applyCrowd:(NSString *)applyCrowd content:(NSString *)content name:(NSString *)name pics:(NSString *)pics time:(long)time teachingContent:(long)teachingContent price:(long)price withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 同意退款
+ (void)agreeRefundWithOrderId:(long)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 支付宝购买课程
+ (void)buyCourseSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 金币购买课程
+ (void)buyCourseByGoldWithUserId:(long)userId courseId:(long)courseId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 微信购买课程
+ (void)buyCourseWxSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 取消退款
+ (void)cancelRefundWithOrderId:(long)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程上下架
+ (void)changeStatusWithId:(long)Id status:(int)status withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程编辑列表
+ (void)coachCourseWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程结束
+ (void)courseFinishWithOrderId:(long)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程订单详情
+ (void)courseOrderWithOrderId:(long)orderId userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程订单退款
+ (void)courseRefundWithOrderId:(long)orderId reason:(NSString *)reason withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程开始
+ (void)courseStartWithOrderId:(long)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程自动取消天数
+ (void)daysToCancelSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 删除课程
+ (void)deleteCourseById:(long)Id userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 金币购买课程打折
+ (void)discountByGoldSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程详情
+ (void)getCourseById:(long)Id userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 我的课程(用户)
+ (void)myCourseWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 课程管理
+ (void)myCourseOrderWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 修改课程
+ (void)updateCourseWithModifyThe:(NSMutableDictionary *)dic withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 微信购买课程
+ (void)wxBuyCourseCourseId:(long)courseId userId:(long)userId success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
// 支付宝购买课程
+ (void)aliBuyCourseCourseId:(long)courseId userId:(long)userId success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;

+ (void)uploadImage:(NSArray *)imageArr withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;


#pragma 房间管理
// 房间禁言列表
+ (void)getRoomForbiddenList:(long)roomId pageNum:(int)pageNum pageSize:(int)pageSize nickname:(NSString *)nickname withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;

// 获得提现手续费
+ (void)geWithdrawFeeSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail;
@end

NS_ASSUME_NONNULL_END
