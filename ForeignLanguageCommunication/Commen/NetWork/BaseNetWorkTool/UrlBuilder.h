//
//  UrlBuilder.h
//  VoiceLive
//
//  Created by mac on 2020/10/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>
/** */


/**
 代理说明：http://pic.sxsyingyu.com/gwugwbfhj8u7lejqykoa.html
 平台使用协议： http://pic.sxsyingyu.com/inuyg8q5viupurfinmhp.html
 用户充值协议： http://pic.sxsyingyu.com/33laod1gtbqys8b84sqv.html
 谁想说英语用户协议： http://pic.sxsyingyu.com/vsyt4h7ll47pziepd93y.html
 用户隐私协议： http://pic.sxsyingyu.com/28808pfxgz3yk7zdz9lc.html
 
 */
#pragma mark - 登陆管理
// 验证码登录
extern NSString * const URL_getLoginByVerifyCode;
// 通过友盟token一键登录
extern NSString * const URL_getPhoneByYoumengToken;
// 账号密码登录
extern NSString * const URL_loginByPwd;
// qq登录
extern NSString * const URL_loginByQQ;
// 微信登录
extern NSString * const URL_loginByWeChat;
// 分页查询附近的人
extern NSString * const URL_nearbyPersonal;

#pragma mark - 业务参数
// 查询业务配置
extern NSString * const URL_configGetConfig;

#pragma mark - 首页
// 首页排行榜
extern NSString * const URL_charts;
// 首页排行榜(个人)
extern NSString * const URL_chartsSelf;
// 外语角界面
extern NSString * const URL_englishIndex;
// 首页匹配类型
extern NSString * const URL_match;
// 小语种全部匹配类型
extern NSString * const URL_matchMin;
// 交流圈界面
extern NSString * const URL_momentsIndex;
// 首页搜索-用户
extern NSString * const URL_searchUser;
// 明星教练
extern NSString * const URL_starCoach;
// 投票
extern NSString * const URL_vote;

#pragma mark - 世界动态
// 新增世界动态评论
extern NSString * const URL_addPublishComments;
// 新增动态评论回复
extern NSString * const URL_addPublishCommentsReply;
// 新增世界动态
extern NSString * const URL_addPublishs;
// 删除动态评论
extern NSString * const URL_deletePublishCommentsById;
// 删除动态评论回复
extern NSString * const URL_deletePublishCommentsReplyById;
// 删除世界动态(个人作品)
extern NSString * const URL_deletePublishsById;
// 分页查询动态列表(我的作品列表)
extern NSString * const URL_getMyPublishList;
// 查询世界动态详情
extern NSString * const URL_getPublishsById;
// 分页查询世界动态列表
extern NSString * const URL_getPublishsList;
// 世界动态点赞/取消点赞(收藏)
extern NSString * const URL_postPublishsLike;
// 分页查询某个动态的评论
extern NSString * const URL_queryPublishComments;

#pragma mark - VIP
// VIP信息
extern NSString * const URL_VIPInfo;
// VIP充值列表
extern NSString * const URL_VIPRechargeList;
// VIP充值支付宝
extern NSString * const URL_recharge;
// VIP充值微信
extern NSString * const URL_rechargeWx;


#pragma mark - 个人群公告管理
// 新增个人群公告
extern NSString * const URL_addPersonalClusterNotice;
// 删除个人群公告
extern NSString * const URL_deletePersonalClusterNoticeById;
// 查询个人群公告
extern NSString * const URL_getPersonalClusterNotice;
// 修改个人群公告
extern NSString * const URL_updatePersonalClusterNotice;


#pragma mark - 个人群成员管理
// 申请入群，需要审核的接口
extern NSString * const URL_applyCluster;
// 群成员列表
extern NSString * const URL_clusterMembers;
// 删除群成员/退群
extern NSString * const URL_deletePersonalClusterMemberById;
// 查询个人群成员
extern NSString * const URL_getPersonalClusterMemberById;
// 用户可添加到个人群的好友列表
extern NSString * const URL_getUserPerClusterAddList;
// 拉人入群,直接加入群的接口
extern NSString * const URL_inviteCluster;
// 设置管理员
extern NSString * const URL_updatePersonalClusterMember;


#pragma mark - 个人群管理
// 根据群id查询群信息
extern NSString * const URL_IMClusterInfo;
// 新增个人群
extern NSString * const URL_addPersonalCluster;
// 删除个人群
extern NSString * const URL_deletePersonalClusterById;
// 查询个人群
extern NSString * const URL_getPersonalClusterById;
// 查询用户在群信息
extern NSString * const URL_getUserClusterVo;
// 我加入的群组
extern NSString * const URL_myJoinedCluster;
// 搜索个人群
extern NSString * const URL_searchPersonalCluster;
// 搜索群后查询群信息
extern NSString * const URL_searchToClusterInfo;
// 修改个人群
extern NSString * const URL_updatePersonalCluster;


#pragma mark - 代理管理
// 申请成为代理
extern NSString * const URL_addAgent;
// 添加下属
extern NSString * const URL_addSubordinate;
// 添加下属列表
extern NSString * const URL_addSubordinateList;
// 代理信息,根据status判断-1：拒绝，0：待审核，1：正常，2：取消代理
extern NSString * const URL_agentInfo;
// 代理金额明细
extern NSString * const URL_agentProfitDetail;
// 解除代理
extern NSString * const URL_removeSubordinate;
// 下属列表
extern NSString * const URL_subordinateList;


#pragma mark - 关注/好友
// 关注
extern NSString * const URL_addFollowsMapping;
// 取消关注
extern NSString * const URL_deleteFollowsMapping;
// 粉丝列表
extern NSString * const URL_fansList;
// 关注列表
extern NSString * const URL_followList;
// 好友列表
extern NSString * const URL_friendsList;
// 是否好友
extern NSString * const URL_isFriend;


#pragma mark - 地区城市
// 获得城市信息
extern NSString * const URL_getCity;
// 获得省份列表
extern NSString * const URL_getProvince;


#pragma mark - 实时匹配
// 取消实时匹配
extern NSString * const URL_cancelMatch;
// 根据类型实时匹配
extern NSString * const URL_matchByType;
// 解锁当次需要消耗的金币
extern NSString * const URL_unlockIng;


#pragma mark - 意见反馈
// 新增用户意见反馈
extern NSString * const URL_addUserFeedback;


#pragma mark - 搜索
// 分页条件查询房间
extern NSString * const URL_getSearchRoomPages;
// 分页条件查询用户
extern NSString * const URL_getSearchUserPages;
// 分页条件搜索个人群
extern NSString * const URL_searchPersonalClusters;


#pragma mark - 收藏--动态（作品）、课程、房间
// 新增(取消)收藏---课程、房间
extern NSString * const URL_addCollect;
// 分页查询课程收藏列表
extern NSString * const URL_getCourseCollectList;
// 分页查询动态收藏列表
extern NSString * const URL_getPublishCollectList;


#pragma mark - 教练管理
// 教练认证
extern NSString * const URL_addCoach;
// 教练详情
extern NSString * const URL_coachDetail;
// 教练认证前验证,有返回为已认证教练，无返回为可进行认证教练
extern NSString * const URL_coachVerify;
// 搜索教练
extern NSString * const URL_searchCoach;
// 修改教练详情
extern NSString * const URL_updateCoach;


#pragma mark - 文件管理
// base64上传文件
extern NSString * const URL_base64;
// 根据文件名删除文件
extern NSString * const URL_deleteByName;
// 根据文件id查询文件
extern NSString * const URL_downloadById;
// 根据文件名查询文件
extern NSString * const URL_downloadByName;
// 上传文件(支持多文件)
extern NSString * const URL_upload;


#pragma mark - 查询标签
// 根据类型查询标签：0日常爱好  1教学内容   2适用人群   3语种
extern NSString * const URL_getLabelByType;


#pragma mark - 用户管理
// 拉黑/取消拉黑用户
extern NSString * const URL_blackUser;
// 更换手机
extern NSString * const URL_changePhone;
// 忘记密码
extern NSString * const URL_forgetPwd;
// 我的黑名单
extern NSString * const URL_getMyBlackUser;
// 个人资料
extern NSString * const URL_getUserById;
// IM关注
extern NSString * const URL_imFocus;
// 个人资料进语音/视频/聊天判断
extern NSString * const URL_interactCount;
// 邀请码邀请
extern NSString * const URL_inviteUser;
// 我的 页面
extern NSString * const URL_myPage;
// 匹配剩余次数
extern NSString * const URL_remainMatchCount;
// 举报（投诉）用户、房间、课程、动态、音乐、教练
extern NSString * const URL_report;
// 修改密码
extern NSString * const URL_updPwd;
// 用户登陆成功后完善资料或用户资料修改时调用此接口
extern NSString * const URL_updUser;
// 解绑微信
extern NSString * const URL_romveUserWx;
// 更新/绑定用户的微信
extern NSString * const URL_upUserWx;
#pragma mark - 用户聊天设置
// 查询用户聊天信息设置
extern NSString * const URL_getUserChatSet;
// 修改用户聊天设置
extern NSString * const URL_updateUserChatSet;


#pragma mark - 短信管理
// 获取验证码
extern NSString * const URL_getVerifyCode;
// 验证码校验
extern NSString * const URL_verifyCode;



#pragma mark - 系统消息
// 群组消息助手
extern NSString * const URL_cluster;
// 评论消息
extern NSString * const URL_commentMessage;
// 课程消息
extern NSString * const URL_coursesMessage;
// 处理群组入群通知
extern NSString * const URL_dealClusterAccess;
// 系统消息主界面
extern NSString * const URL_index;
// 官方消息
extern NSString * const URL_officialMessage;
// 消息-房间
extern NSString * const URL_roomMessage;
// 房间在线用户列表
extern NSString * const URL_roomWealthToDay;
// 钱包消息
extern NSString * const URL_walletMessage;


#pragma mark - 职业信息
// 获取行业信息
extern NSString * const URL_getIndustry;
// 获取行业信息
extern NSString * const URL_getProfession;


#pragma mark - 设置个人禁言
// 新增个人群禁言
extern NSString * const URL_addPersonalClusterSilence;
// 解除个人群禁言
extern NSString * const URL_deletePersonalClusterSilenceById;
// 查询个人群禁言
extern NSString * const URL_getPersonalClusterSilence;


#pragma mark - 课程评价管理
// 新增课程评价
extern NSString * const URL_addEvaluation;
// 删除评价
extern NSString * const URL_delEvaluationById;
// 查询课程评价
extern NSString * const URL_getEvaluationById;


#pragma mark - 邀请码邀请记录
// 邀请记录
extern NSString * const URL_inviteLog;


#pragma mark - 钱包管理
// 发起提现申请（支付宝）
extern NSString * const URL_alipayAddWalletWithdraw;
// 发起提现申请（微信）
extern NSString * const URL_wxpayAddWalletWithdraw;
// 获得交易详情
extern NSString * const URL_getDealDetail;
// 钱包余额
extern NSString * const URL_getWalletByUserId;
// 查询钱包交易记录
extern NSString * const URL_getWalletLogById;
// 充值金币
extern NSString * const URL_rechargeGold;
// 充值金币-支付后回调
extern NSString * const URL_rechargeGoldPay;
// 充值金币-微信支付后回调
extern NSString * const URL_rechargeGoldWxPay;
// 充值页面
extern NSString * const URL_rechargePage;


#pragma mark - 频道歌曲
// 上传歌曲
extern NSString * const URL_Music;
// 新增频道歌曲，单个
extern NSString * const URL_addMusicChannel;
// 新增频道歌曲，多个
extern NSString * const URL_addMusicChannels;
// 用户下麦，删除频道所有歌曲
extern NSString * const URL_deleteMusicChannel;
// 删除频道歌曲
extern NSString * const URL_deleteMusicChannelById;
// 分页查询频道歌曲列表
extern NSString * const URL_getMusicChannelByRoomId;


#pragma mark - 课程管理
// 新增课程
extern NSString * const URL_addCourse;
// 同意退款
extern NSString * const URL_agreeRefund;
// 支付宝购买课程
extern NSString * const URL_buyCourse;
// 金币购买课程
extern NSString * const URL_buyCourseByGold;
// 微信购买课程
extern NSString * const URL_buyCourseWx;
// 取消退款
extern NSString * const URL_cancelRefund;
// 课程上下架
extern NSString * const URL_changeStatus;
// 课程编辑列表
extern NSString * const URL_coachCourse;
// 课程结束
extern NSString * const URL_courseFinish;
// 课程订单详情
extern NSString * const URL_courseOrder;
// 课程订单退款
extern NSString * const URL_courseRefund;
// 课程开始
extern NSString * const URL_courseStart;
// 课程自动取消天数
extern NSString * const URL_daysToCancel;
// 删除课程
extern NSString * const URL_deleteCourseById;
// 金币购买课程打折
extern NSString * const URL_discountByGold;
// 课程详情
extern NSString * const URL_getCourseById;
// 我的课程(用户)
extern NSString * const URL_myCourse;
// 课程管理
extern NSString * const URL_myCourseOrder;
// 修改课程
extern NSString * const URL_updateCourse;

// 微信购买课程
extern NSString * const URL_wxBuyCourse;
// 支付宝购买课程
extern NSString * const URL_aliBuyCourse;

extern NSString * const URL_geWithdrawFee;


#pragma mark ---------------IM ----------
/**总未读数*/
extern NSString * const URL_IM_TotalUnRead;

#pragma 房间管理
// 房间禁言列表
extern NSString * const URL_getRoomForbiddenList;
