//
//  YKHttpRequestManager.h
//  VoiceLive
//
//  Created by mac on 2020/10/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//网络请求

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

static CGFloat AFRequestTimeOut = 20.0;
extern NSString * _Nullable const hostUrl;

typedef enum : NSUInteger {
    KHttpRequestTypeGet,
     KHttpRequestTypePost,
     KHttpRequestTypePut,
     KHttpRequestTypeDelete,
     KHttpRequestTypePatch,
} HttpRequestType;

typedef enum : NSUInteger {
    KNetWorkStatusNotReachable,
    KNetWorkStatusReachableViaWiFi,
    KNetWorkStatusReachableViaWWan,
    KNetWorkStatusUnknown,
} KNetWorkStatus;

typedef enum : NSUInteger {
    HttpRequestSerializer,
    JsonRequestSerializer,
} AFrequestSerializer;



typedef void(^HttpResponseSuccessBlock)(id _Nullable response);

typedef void(^HttpResponseFailBlock)(NSError * _Nullable error);

typedef void(^DownloadProgressBlock)(int64_t bytesRead, int64_t totalBytes);



@interface YKHttpRequestManager : NSObject
/**网络状况*/
@property (nonatomic, assign) KNetWorkStatus netWorkStatus;
/**请求头*/
@property (nonatomic, strong) NSDictionary * _Nullable headDic;
@property (nonatomic, copy ) NSString * _Nullable  IMHostUrl;
@property (nonatomic, assign) AFrequestSerializer requestSerializer;


/**检查网络是否可用*/
- (BOOL)checkNetWork;


+ (YKHttpRequestManager *)shared;





- (void)GetWithUrl:(NSString *)urlString withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withShouldLogin:(BOOL)shouldLogin;

- (void)PostWithUrl:(NSString *)urlString withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock _Nullable )success withFail:(HttpResponseFailBlock)fail withShouldLogin:(BOOL)shouldLogin;

- (void)UploadImagesWithUrl:(NSString *_Nullable)urlString withImages:(NSArray *)imageArray withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withProgress:(DownloadProgressBlock)progress withShouldLogin:(BOOL)shouldLogin;


- (void)downloadWithUrl:(NSString *)urlString withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withProgress:(DownloadProgressBlock)progress withShouldLogin:(BOOL)shouldLogin;

- (void)uploadFileWithFilePath:(NSString *)filePath withName:(NSString *)fileName withMimeType:(NSString *)mimeType withUrl:(NSString *)urlString withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withDownloadProgress:(DownloadProgressBlock)progress withShouldLogin:(BOOL)shouldLogin;



@end


