//
//  YKHttpRequestManager.m
//  VoiceLive
//
//  Created by mac on 2020/10/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
// 创建 network manager 设置head timeout certified
// 访问接口  解析返回值  调用回调
//

#import "YKHttpRequestManager.h"
#import "BaseNetworkModel.h"
#import "YKFileManager.h"

#if DEBUG
NSString * const hostUrl = @"http://47.104.23.218:19980";
//NSString * const hostUrl = @"http://192.168.31.175:19980";
#else
NSString * const hostUrl = @"http://47.104.23.218:19980";
#endif


static YKHttpRequestManager *_once;

@interface YKHttpRequestManager ()
@property (nonatomic, strong) YKFileManager *fileManager;
@end

@implementation YKHttpRequestManager
/** init*/
+ (YKHttpRequestManager *)shared {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _once = [[YKHttpRequestManager alloc] init];
        
    });
    return _once;
}
- (instancetype)init {
    if (self = [super init]) {
        //        self.netWorkStatus = KNetWorkStatusUnknown;
        //先检查网络
        [self checkNetworkStatus];
    }
    return self;
}
- (YKFileManager *)fileManager {
    if (!_fileManager) {
        _fileManager = [[YKFileManager alloc] init];
    }
    return _fileManager;
}

/**网络状况*/
- (void)checkNetworkStatus {
    AFNetworkReachabilityManager *reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    [reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusNotReachable) {
            self.netWorkStatus = KNetWorkStatusNotReachable;
        } else if (status == AFNetworkReachabilityStatusUnknown) {
            self.netWorkStatus = KNetWorkStatusUnknown;
        } else if (status == AFNetworkReachabilityStatusReachableViaWiFi) {
            self.netWorkStatus = KNetWorkStatusReachableViaWiFi;
        } else if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
            self.netWorkStatus = KNetWorkStatusReachableViaWWan;
        }
    }];
    [reachabilityManager startMonitoring];
}
//检查网络 当网络不可达，网络不能使用
- (BOOL)checkNetWork {
    BOOL network = YES;
    [self checkNetworkStatus];
    if (self.netWorkStatus == KNetWorkStatusNotReachable) {
        network = NO;
    }
    return network;
}
/**session manager*/
- (AFHTTPSessionManager *)getSessionManager {
    //YKHttpTool *tool = [YKHttpTool sharedNetworkTool];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSString *baseURL = [NSString stringWithFormat:@"%@",hostUrl];
    NSLog(@"hosturl===%@",hostUrl);
    NSLog(@"imurl===%@",self.IMHostUrl);
    if (self.IMHostUrl) {
        baseURL = self.IMHostUrl;
        NSLog(@"不为空");
        // self.IMHostUrl = @"";
    }else{
        NSLog(@"9999为空");
    }
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager] initWithBaseURL:[NSURL URLWithString:baseURL] sessionConfiguration:sessionConfig];
    self.IMHostUrl = nil;
    
    //BASE URL
    //    AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager] initWithBaseURL:[NSURL URLWithString:hostUrl] sessionConfiguration:sessionConfig];
    
    
    //正常使用AFHTTPRequestSerializer  要求上传json的时候 使用AFJSONRequestSerializer
    if (self.requestSerializer == HttpRequestSerializer) {
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    } else if (self.requestSerializer == JsonRequestSerializer) {
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    //超时设定
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = AFRequestTimeOut;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    //返回信息序列化
    //返回内容的格式接受json/html多种格式
    AFJSONResponseSerializer *responseSer = [AFJSONResponseSerializer serializer];
    responseSer.removesKeysWithNullValues = YES;
    manager.responseSerializer = responseSer;
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"multipart/form-data", nil]];
    
    //请求头
//    for (id key in self.headDic) {
//        [manager.requestSerializer setValue:self.headDic[key] forHTTPHeaderField:key];
//    }
    
    NSString *timeStamp = [Util getCurrentTimestamp];
    NSString *randomString = [Util createRandomString];
    NSString *oldString = [NSString stringWithFormat:@"%@%@",timeStamp,randomString];
    NSString *md5String = [Util createMD5WithString:oldString];
    NSLog(@"time==%@",timeStamp);
    NSLog(@"random==%@",randomString);
    NSLog(@"md5==%@",md5String);
    
    [manager.requestSerializer setValue:timeStamp forHTTPHeaderField:@"timestamp"];
    [manager.requestSerializer setValue:randomString forHTTPHeaderField:@"nonce"];
    [manager.requestSerializer setValue:md5String forHTTPHeaderField:@"sign"];
    
    //安全策略
    [manager setSecurityPolicy:[self customSecurityPolicy]];
    
    return manager;
}


// MARK: 安全证书
- (AFSecurityPolicy *)customSecurityPolicy {
    //安全证书验证
    // 三种模式 none 不验证   public 只验证证书公钥不验证证书有效期  cer 本地存有服务端证书的拷贝 验证有效期
    //不正确的证书此处使用AFSSLPinningModeCertificate会导致崩溃
//    NSString *cerPath = [[NSBundle mainBundle] pathForResource:@"mj.quyangapp.com" ofType:@".cer"];
//    NSData *cerData = [NSData dataWithContentsOfFile:cerPath];
    
    AFSecurityPolicy *securityPolicy;
    securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    
//    securityPolicy.pinnedCertificates = [NSSet setWithObjects:cerData, nil];
    //是否允许无效的证书通过 (包括自己生成的证书)
    securityPolicy.allowInvalidCertificates = YES;
    //是否验证证书域名
    securityPolicy.validatesDomainName = NO;
    
    return securityPolicy;
}

//MARK: 请求访问
- (void)GetWithUrl:(NSString *)urlString withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withShouldLogin:(BOOL)shouldLogin{
    [self requestWithHttpMethod:KHttpRequestTypeGet withUrl:urlString withParams:params withShouldLogin:shouldLogin withSuccess:success withFail:fail withDownload:nil];
}
- (void)PostWithUrl:(NSString *)urlString  withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withShouldLogin:(BOOL)shouldLogin{
    [self requestWithHttpMethod:KHttpRequestTypePost withUrl:urlString withParams:params withShouldLogin:shouldLogin withSuccess:success withFail:fail withDownload:nil];
}
- (void)requestWithHttpMethod:(HttpRequestType)requestType withUrl:(NSString *)urlString withParams:(NSDictionary *)params withShouldLogin:(BOOL)shouldLogin withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withDownload:(DownloadProgressBlock)download {
    NSLog(@"url--%@",urlString);
    //先判断是否需要登录
    AFHTTPSessionManager *manager = [self getSessionManager];
    if (shouldLogin) {
        //需要登录状态
        if (![self isUserLogin]) {
            //要求登录
            [self goToLogin];
            return;
        }
    }
    switch (requestType) {
        case KHttpRequestTypeGet: {
            //GET
            
            [manager GET:urlString parameters:params headers:self.headDic progress:^(NSProgress * _Nonnull downloadProgress) {
                if (download) {
                    download(downloadProgress.completedUnitCount,downloadProgress.totalUnitCount);
                }
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                //因为data格式不确定，不提前解析
                NSNumber *number = responseObject[@"status"];
                
                if (number.intValue == 401 || number.intValue == 403) {
                    //去登录
                    
                    return;
                }
                if (number.intValue == 200) {
                    if (success) {
                        success(responseObject);
                    }
                } else {
                    if (fail) {
                        NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:responseObject[@"msg"], NSLocalizedDescriptionKey,nil];
                        fail([NSError errorWithDomain:NSCocoaErrorDomain code:number.intValue userInfo:info]);
                    }
                }
//                if (success) {
//                    success(responseObject);
//                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self handleFailureWithURL:urlString Parameters:params Action:@"GET" Task:task Error:error Failure:fail];
            }];
        }
            break;
        case KHttpRequestTypePost: {
            //POST
            
            NSLog(@"%@",manager.requestSerializer);
            
            [manager POST:urlString parameters:params headers:self.headDic progress:^(NSProgress * _Nonnull uploadProgress) {
                if (download) {
                    download(uploadProgress.completedUnitCount,uploadProgress.totalUnitCount);
                }
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"%@",task.currentRequest);
                NSNumber *number = responseObject[@"status"];
                
                
                if (number.intValue == 401 || number.intValue == 403) {
                    //去登录
                    
                    return;
                }
                if (number.intValue == 200) {
                    if (success) {
                        success(responseObject);
                    }
                } else {
                    if (fail) {
                        NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:responseObject[@"msg"], NSLocalizedDescriptionKey,nil];
                        fail([NSError errorWithDomain:NSCocoaErrorDomain code:number.intValue userInfo:info]);
                    }
                }
//                if (success) {
//                    success(responseObject);
//                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self handleFailureWithURL:urlString Parameters:params Action:@"post" Task:task Error:error Failure:fail];
//                if (fail) {
//                    fail(error);
//                }
            }];
        }
            break;
            
        default:
            break;
    }
}

- (void)downloadWithUrl:(NSString *)urlString withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withProgress:(DownloadProgressBlock)progress withShouldLogin:(BOOL)shouldLogin{
    AFHTTPSessionManager *manager = [self getSessionManager];
//    if (shouldLogin) {
//        if (![self isUserLogin]) {
//            [self goToLogin];
//            return;
//        }else {
//            //需要登录状态 取出token
//            //授权token
//            UserModel *model = [[UserFile shareUser]getUserInfo];
//            if (model.token) {
//                //有token 加入请求头
//                [manager.requestSerializer setValue:model.token forHTTPHeaderField:@"Authorization"];
//            }
//        }
//    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReturnCacheDataDontLoad timeoutInterval:10.0];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        //进度
        if (progress) {
            progress(downloadProgress.completedUnitCount,downloadProgress.totalUnitCount);
        }
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        //目标路径  返回数据
        
        NSURL *downloadUrl = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [downloadUrl URLByAppendingPathComponent:@"zipFile.zip"];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        //完成后  路径 返回信息
        if (!error) {
            success(filePath);
        }
    }];
}

- (void)uploadFileWithFilePath:(NSString *)filePath withName:(NSString *)fileName withMimeType:(NSString *)mimeType withUrl:(NSString *)urlString withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withDownloadProgress:(DownloadProgressBlock)progress withShouldLogin:(BOOL)shouldLogin{
    
    AFHTTPSessionManager *manager = [self getSessionManager];
    if (shouldLogin) {
        if (![self isUserLogin]) {
            [self goToLogin];
            return;
        }else {
           
        }
    }
    [manager POST:urlString parameters:params headers:self.headDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSString *suffixString;
        if ([mimeType isEqualToString:@"video/mpeg4"]) {
            suffixString = @"mp4";
        }else {
            suffixString = [mimeType componentsSeparatedByString:@"/"][1];
        }
        //使用当前时间戳生成文件名
        NSString *uploadName = [NSString stringWithFormat:@"%@.%@",[Util getCurrentTimestamp],suffixString];
        NSError *error;
        NSString *typeStr = @"application/octet-stream";
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        if (data) {
            [formData appendPartWithFileData:data name:@"file" fileName:uploadName mimeType:typeStr];
        }
        else{
            [formData appendPartWithFileURL:[NSURL URLWithString:filePath] name:fileName fileName:uploadName mimeType:typeStr error:&error];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress.completedUnitCount,uploadProgress.totalUnitCount);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        if (fail) {
//            fail(error);
//        }
        [self handleFailureWithURL:urlString Parameters:params Action:@"POST" Task:task Error:error Failure:fail];
    }];
}
- (void)UploadImagesWithUrl:(NSString *)urlString withImages:(NSArray *)imageArray withParams:(NSDictionary *)params withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail withProgress:(DownloadProgressBlock)progress withShouldLogin:(BOOL)shouldLogin {
    
    AFHTTPSessionManager *manager = [self getSessionManager];
//    if (shouldLogin) {
//        if (![self isUserLogin]) {
//            [self goToLogin];
//            return;
//        }else {
//            //需要登录状态 取出token
//            //授权token
//            UserModel *model = [[UserFile shareUser]getUserInfo];
//            if (model.token) {
//                //有token 加入请求头
//                [manager.requestSerializer setValue:model.token forHTTPHeaderField:@"Authorization"];
//            }
//        }
//    }
    [manager POST:urlString parameters:params headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        int i = 0;
        for (id image in imageArray) {
            
            NSDate *date = [NSDate date];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyyMMddHHmmss"];
            NSString *dateStr = [formatter stringFromDate:date];
            
            if ([image isKindOfClass:[NSData class]]) {
                NSString *fileName = [NSString stringWithFormat:@"%@%d.gif",dateStr,i];
                // name 后台要求上传时的参数名称  fileName:文件自己的名称
                //                NSData *imageData = [NSData dataWithData:image];
                [formData appendPartWithFileData:image name:@"file" fileName:fileName mimeType:@"image/gif"];
            }
            else if ([image isKindOfClass:[NSURL class]]) {
                NSString *fileName = [NSString stringWithFormat:@"%@%d.gif",dateStr,i];
                
                [formData appendPartWithFileURL:(NSURL *)image name:@"file" fileName:fileName mimeType:@"image/gif" error:nil];
            }
            else{
                NSString *fileName = [NSString stringWithFormat:@"%@%d.jpg",dateStr,i];
                NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
                // name 后台要求上传时的参数名称  fileName:文件自己的名称
                [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/jpeg"];
            }
            i++;
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress.completedUnitCount,uploadProgress.totalUnitCount);
        }
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        if (fail) {
//            fail(error);
//        }
        [self handleFailureWithURL:urlString Parameters:params Action:@"" Task:task Error:error Failure:fail];
    }];
}
//MARK: 先判断有没有登录 没有登录 用户 发出登录通知
- (BOOL)isUserLogin {
    //判断用户当前是否在登录状态
    BOOL isLogin = NO;
    UserInfoModel *model = [[UserInfoManager shared] getUserDetailInfo];
    if (model) {
        isLogin = YES;
    }
//    UserInfoModel *userM = [[UserFile shareUser] getUserPrivateInfo];
//    if (userM && userM.ID > 0) {
//        isLogin = YES;
//    }
    return isLogin;
}
- (void)goToLogin {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notice_Login" object:nil userInfo:nil];
}

#pragma mark - 处理请求失败
/**
 *  处理请求失败
 *
 *  @param url        请求URL
 *  @param parameters 请求参数
 *  @param task       请求任务
 *  @param error      错误信息
 *  @param failure    失败回调
 */
- (void)handleFailureWithURL:(NSString *) url Parameters:(NSDictionary *)parameters Action:(NSString *) action Task:(NSURLSessionDataTask *) task Error:(NSError *) error Failure:(HttpResponseFailBlock) failure
{
    NSHTTPURLResponse *response = (NSHTTPURLResponse *) task.response;
    NSInteger statusCode = response.statusCode;
    
    /** 设置error信息 */
    if (statusCode == 0)
    {
        // 网络不好
        error = [[NSError alloc] initWithDomain:@"似乎与网络断开了链接,请检查设置" code:statusCode userInfo:nil];
    }
    else if (statusCode == 200)
    {
        // 成功
        error = [[NSError alloc] initWithDomain:@"请求成功" code:statusCode userInfo:nil];
    }
    else
    {   // 服务器错误
        error = [[NSError alloc] initWithDomain:@"服务器错误" code:statusCode userInfo:nil];
    }
    
    /** 捕获error异常 */
    @try
    {
        [NSException raise:@"接口请求返回数据错误" format:@"URL=%@\n参数=%@\n错误信息=%@",url,parameters,error];
    }
    @catch (NSException *exception)
    {
        NSLog(@"exception : %@",exception);
        
        // 上报错误信息给服务器
//        [GloriaHandler commitRequestErrorInfo:task URL:url requestData:[GloriaHandler dictionaryOrArrayToJson:parameters] Action:action Error:error];
    }
    @finally
    {
        if (failure) {
            failure(error);
        }
    }
}

@end
