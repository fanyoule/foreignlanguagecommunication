//
//  RequestManager.m
//  VoiceLive
//
//  Created by mac on 2020/10/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "RequestManager.h"

@implementation RequestManager
#pragma mark - IM主界面
+ (void)getIMDataWithParameters:(NSMutableDictionary*)parameters IMHost:(NSString *)host UrlString:(NSString*)url Success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
      YKHttpRequestManager *manager = [YKHttpRequestManager shared];
         manager.requestSerializer = HttpRequestSerializer;
         manager.IMHostUrl = host;
     
    [manager GetWithUrl:url withParams:parameters withSuccess:^(id  _Nullable response) {
            if (success) {
    
                success(response);
                  }
       } withFail:^(NSError * _Nullable error) {
             if (fail) {
                     fail(error);
                 }
       } withShouldLogin:YES];
}

#pragma mark - IMpost
+ (void)postIMDataWithParameters:(NSMutableDictionary*)parameters IMHost:(NSString *)host UrlString:(NSString*)url Success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
      YKHttpRequestManager *manager = [YKHttpRequestManager shared];
         manager.requestSerializer = HttpRequestSerializer;
         manager.IMHostUrl = host;
     
    [manager PostWithUrl:url withParams:parameters withSuccess:^(id  _Nullable response) {
        if (success) {
            success(response);
        }
    } withFail:^(NSError * _Nullable error) {
        if (fail) {
            fail(error);
        }
    } withShouldLogin:YES];
}
// MARK: GET不登录(无参数)
+ (void)getDonLogIn:(NSString *)URL withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    [manager GetWithUrl:URL withParams:nil withSuccess:^(id  _Nullable response) {
        if (success) {
            success(response);
        }
    } withFail:^(NSError * _Nullable error) {
        if (fail) {
            fail(error);
        }
        
    } withShouldLogin:NO];
}
// MARK: GET不登录(有参数)
+ (void)getDonLogIn:(NSString *)URL incomingParameter:(NSMutableDictionary *)dictionary withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    [manager GetWithUrl:URL withParams:dictionary withSuccess:^(id  _Nullable response) {
        if (success) {
            success(response);
        }
    } withFail:^(NSError * _Nullable error) {
        if (fail) {
            fail(error);
        }
        
    } withShouldLogin:NO];
}
// MARK: GET通用
+(void)getObtainUrl:(NSString *)Url incomingParameter:(NSMutableDictionary *)dictionary withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    [manager GetWithUrl:Url withParams:dictionary withSuccess:^(id  _Nullable response) {
        if (success) {
            success(response);
        }
    } withFail:^(NSError * _Nullable error) {
        if (fail) {
            fail(error);
        }
        
    } withShouldLogin:YES];
    
}
// MARK: POST通用
+ (void)postObtainUrl:(NSString *)Url incomingParameter:(NSMutableDictionary *)dictionary withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    [manager PostWithUrl:Url withParams:dictionary withSuccess:^(id  _Nullable response) {
        if (success) {
            success(response);
        }
    } withFail:^(NSError * _Nullable error) {
        if (fail) {
            fail(error);
        }
    } withShouldLogin:YES];
}
#pragma mark - 业务参数
// 查询业务配置
+ (void)getQueryBusinessConfigurationWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager GetWithUrl:URL_configGetConfig withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
#pragma mark - 登陆管理
// 验证码登录
+ (void)getLoginByVerifyCodeWithPhone:(NSString *)phone vcode:(NSString *)vcode loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     phone      手机      query   true     string
     vcode      验证码    query   true      string
     loginLat  登陆纬度    query  false     string
     loginLot 登陆经度     query  false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"phone": phone,
                           @"vcode": vcode,
                           @"loginLat": loginLat,
                           @"loginLot": loginLot};
    [manager PostWithUrl:URL_getLoginByVerifyCode withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
    
}
// 通过友盟token一键登录
+ (void)getLoginByVerifyCodeWithPhone:(NSString *)youmengToken loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     youmengToken 友盟token      query   true     string
     loginLat       登陆纬度      query  false     string
     loginLot       登陆经度      query  false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"youmengToken": youmengToken,
                           @"loginLat": loginLat,
                           @"loginLot": loginLot};
    [manager PostWithUrl:URL_getPhoneByYoumengToken withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 账号密码登录
+ (void)loginByPwdWithAccount:(NSString *)account passwd:(NSString *)passwd loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     account        账号         query   true     string
     passwd         密码         query   true     string
     loginLat       登陆纬度      query  false     string
     loginLot       登陆经度      query  false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"account": account,
                           @"passwd": passwd,
                           @"loginLat": loginLat,
                           @"loginLot": loginLot};
    [manager PostWithUrl:URL_loginByPwd withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// qq登录
+ (void)loginByQQWithOpenId:(NSString *)openId loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     openId         openId       query   true     string
     loginLat       登陆纬度      query  false     string
     loginLot       登陆经度      query  false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"openId": openId,
                           @"loginLat": loginLat,
                           @"loginLot": loginLot};
    [manager PostWithUrl:URL_loginByQQ withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 微信登录
+ (void)loginByWeChatWithOpenId:(NSString *)openId loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     openId         openId       query   true     string
     loginLat       登陆纬度      query  false     string
     loginLot       登陆经度      query  false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"openId": openId,
                           @"loginLat": loginLat,
                           @"loginLot": loginLot};
    [manager PostWithUrl:URL_loginByWeChat withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 分页查询附近的人
+ (void)nearbyPersonalUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     userId         用户ID       query   false     string
     loginLat       登陆纬度      query   true     string
     loginLot       登陆经度      query   true     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"pageSize": [NSNumber numberWithInt:pageSize],
                           @"pageNum": [NSNumber numberWithInt:pageNum],
                           @"loginLat": loginLat,
                           @"loginLot": loginLot};
    [manager GetWithUrl:URL_nearbyPersonal withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}

#pragma mark - 首页
// 首页排行榜
+ (void)chartsUserId:(long)userId userName:(NSString *)userName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userName       用户昵称            query   false     ref
     userId         发起访问的用户id     query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
//                           @"userName": userName
                           
    };
    [manager GetWithUrl:URL_charts withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 首页排行榜(个人)
+ (void)chartsSelfUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         发起访问的用户id     query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_chartsSelf withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 外语角界面
+ (void)englishIndexWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager GetWithUrl:URL_englishIndex withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}

// 首页匹配类型
+ (void)matchWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager GetWithUrl:URL_match withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 小语种全部匹配类型
+ (void)matchMinWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager GetWithUrl:URL_matchMin withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 交流圈界面
+ (void)momentsIndexWithSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager GetWithUrl:URL_momentsIndex withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 首页搜索-用户
+ (void)searchUserWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize userName:(NSString *)userName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     userId         用户ID       query   true     integer
     userName       用户昵称      query   false    string
     */
    if (!userName) {
        userName = @"";
    }
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize),
                           @"userName": userName};
    [manager GetWithUrl:URL_searchUser withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 明星教练
+ (void)starCoachWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_starCoach withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}

// 投票
+ (void)voteWithUserId:(long)userId toUserId:(long)toUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     toUserId        得票用户id       query   false     integer
     userId          投票用户id       query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"toUserId": [NSNumber numberWithLong:toUserId]};
    [manager PostWithUrl:URL_vote withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}

#pragma mark - 世界动态
// 新增世界动态评论
+ (void)addPublishCommentsWithUserId:(long)userId isAuthor:(int)isAuthor publishId:(long)publishId comment:(NSString *)comment commentPic:(NSString *)commentPic withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     comment        评论内容         query   true     string
     isAuthor   是否是作者：0不是 1是  query   true     Integer
     publishId       动态id         query   true     Long
     userId         评论用户id       query   true     Long
     commentPic     评论图片url      query   false    string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"commentPic": commentPic,
                           @"comment": comment,
                           @"isAuthor": @(isAuthor),
                           @"publishId": [NSNumber numberWithLong:publishId],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addPublishComments withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 新增动态评论回复
+ (void)addPublishCommentsReplyWithTouid:(long)touid uid:(long)uid commentsId:(long)commentsId publishId:(long)publishId replyComment:(NSString *)replyComment replyCommentPic:(NSString *)replyCommentPic withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     commentsId        评论ID            query   true     Long
     publishId          动态id              query   true     Long
     replyComment       回复内容            query   true     string
     touid              被回复用户id         query   true     Long
     uid                回复用户ID           query   true     Long
     replyCommentPic     评论图片url         query   false    string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"replyCommentPic": replyCommentPic,
                           @"commentsId": [NSNumber numberWithLong:commentsId],
                           @"replyComment": replyComment,
                           @"publishId": [NSNumber numberWithLong:publishId],
                           @"uid": [NSNumber numberWithLong:uid],
                           @"touid": [NSNumber numberWithLong:touid]};
    [manager PostWithUrl:URL_addPublishCommentsReply withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}

// 新增世界动态
+ (void)addPublishsWithUserId:(long)userId type:(int)type picUrl:(NSString *)picUrl content:(NSString *)content videoCoverUrl:(NSString *)videoCoverUrl videoUrl:(NSString *)videoUrl withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     content        动态内容                                query   true     string
     type       1 文字+图片、2 文字+视频                      query   true     Integer
     userId         评论用户id                              query   true     Long
     picUrl      图片url，多张已逗号分隔,图片和视频只能传一种      query   false    string
     videoCoverUrl  视频封面url,只要有视频url，这个字段必须有      query   false    string
     videoUrl     视频url,图片和视频只能传一种                  query   false    string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"videoCoverUrl": videoCoverUrl,
                           @"content": content,
                           @"videoUrl": videoUrl,
                           @"type": @(type),
                           @"picUrl": picUrl,
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addPublishs withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 删除动态评论
+ (void)deletePublishCommentsById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id        动态评论id     query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_deletePublishCommentsById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 删除动态评论回复
+ (void)deletePublishCommentsReplyById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id        动态评论回复表id      query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_deletePublishCommentsReplyById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 删除世界动态(个人作品)
+ (void)deletePublishsByIds:(NSString *)ids withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     ids       世界动态ids，删除多个用逗号隔开      query   true     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"ids": ids};
    [manager PostWithUrl:URL_deletePublishsById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}

// 分页查询动态列表(我的作品列表)
+ (void)getMyPublishListWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_getMyPublishList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 查询世界动态详情
+ (void)getPublishsById:(long)checkUserId Id:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     checkUserId   查看的用户id      query   true     integer
     id             世界动态id      query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"checkUserId": [NSNumber numberWithLong:checkUserId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_getPublishsById withParams:dict withSuccess:^(id  _Nullable response) {
        dispatch_async(dispatch_get_main_queue(), ^{
            success(response);
        });
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 分页查询世界动态列表
+ (void)getPublishsListWithPage:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_getPublishsList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 世界动态点赞/取消点赞(收藏)
+ (void)postPublishsLikeWithId:(long)publishId likedPostId:(long)likedPostId status:(int)status withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     likedPostId     点赞的用户id       query   true    Long
     publishId       动态id            query   true     Long
     status     点赞状态，0取消，1点赞    query   true     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"status": @(status),
                           @"likedPostId": [NSNumber numberWithLong:likedPostId],
                           @"publishId": [NSNumber numberWithLong:publishId]};
    [manager PostWithUrl:URL_postPublishsLike withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 分页查询动态列表(我的作品列表)
+ (void)queryPublishCommentsWithId:(long)Id pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     id             用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_queryPublishComments withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}

#pragma mark - VIP
// VIP信息
+ (void)VIPInfo:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId        用户id      query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_VIPInfo withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// VIP充值列表
+ (void)VIPRechargeList:(NSString *)platform withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     platform     平台：安卓，IOS     query   false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"platform": platform};
    [manager GetWithUrl:URL_VIPRechargeList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


#pragma mark - 个人群公告管理
// 新增个人群公告
+ (void)addPersonalClusterNoticeWithId:(long)clusterId userId:(long)userId idStick:(int)idStick title:(NSString *)title content:(NSString *)content withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId     群id                query   true     Long
     content      公告内容              query   true     String
     idStick 是否置顶：1置顶，0不置顶     query   true     Integer
     title        公告标题             query   true     String
     userId       发布人id              query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"idStick": @(idStick),
                           @"content": content,
                           @"title": title,
                           @"userId": [NSNumber numberWithLong:userId],
                           @"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager PostWithUrl:URL_addPersonalClusterNotice withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 删除个人群公告
+ (void)deletePersonalClusterNoticeById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id        个人群公告id      query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_deletePersonalClusterNoticeById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 查询个人群公告
+ (void)getPersonalClusterNoticeWithId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId        群id      query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager GetWithUrl:URL_getPersonalClusterNotice withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群公告
+ (void)updatePersonalClusterNoticeId:(long)Id userId:(long)userId idStick:(int)idStick title:(NSString *)title content:(NSString *)content withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id            公告id               query   true     Long
     content      公告内容              query   true     String
     idStick 是否置顶：1置顶，0不置顶     query   true     Integer
     title        公告标题             query   true     String
     userId       发布人id              query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"idStick": @(idStick),
                           @"content": content,
                           @"title": title,
                           @"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalClusterNotice withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 个人群成员管理
// 申请入群，需要审核的接口
+ (void)applyClusterWithId:(long)clusterId userId:(long)userId clusterAuditInfo:(NSString *)clusterAuditInfo withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterAuditInfo 入群审核内容           query   true     String
     clusterId        群id                 query   true     Integer
     userId           用户id               query   true     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"clusterAuditInfo": clusterAuditInfo,
                           @"userId": [NSNumber numberWithLong:userId],
                           @"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager PostWithUrl:URL_applyCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 群成员列表
+ (void)clusterMembersWithId:(long)Id userId:(long)userId sysId:(NSString *)sysId nickname:(NSString *)nickname withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id        群id                 query   true     Integer
     userId    用户id               query   true     Integer
     nickname  入群审核内容           query   false     String
     sysId     入群审核内容           query   false     String
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"nickname": nickname,
                           @"userId": [NSNumber numberWithLong:userId],
                           @"sysId": sysId,
                           @"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_clusterMembers withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 删除群成员/退群
+ (void)deletePersonalClusterMemberByIdWithId:(long)clusterId userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId        群id                 query   true     Integer
     userId           用户id               query   true     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager PostWithUrl:URL_deletePersonalClusterMemberById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 查询个人群成员
+ (void)getPersonalClusterMemberById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id        个人群成员id        query   true     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_getPersonalClusterMemberById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 用户可添加到个人群的好友列表
+ (void)getUserPerClusterAddListWithId:(long)clusterId userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId        个人群id             query   true     Integer
     userId           用户id               query   true     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager GetWithUrl:URL_getUserPerClusterAddList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 拉人入群,直接加入群的接口
+ (void)inviteClusterWithId:(long)clusterId userIds:(NSString *)userIds withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId        群id                 query   true     Integer
     userIds          用户id               query   true    string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userIds": userIds,
                           @"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager PostWithUrl:URL_inviteCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 设置管理员
+ (void)updatePersonalClusterMemberWithId:(long)clusterId userId:(long)userId admin:(int)admin withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     admin    是否是管理员（0普通成员，1管理员） query   true     Integer
     clusterId        群id                 query   true     Integer
     userId           用户id               query   true     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"admin": [NSNumber numberWithInt:admin],
                           @"userId": [NSNumber numberWithLong:userId],
                           @"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager PostWithUrl:URL_updatePersonalClusterMember withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 个人群管理
// 查询个人群成员
+ (void)IMClusterInfoWithId:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id        群id        query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_IMClusterInfo withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 新增个人群
+ (void)addPersonalClusterWithId:(long)userId clusterName:(NSString *)clusterName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterName      群id                 query   false     string
     userId           用户id               query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"clusterName": clusterName};
    [manager PostWithUrl:URL_addPersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 删除个人群
+ (void)deletePersonalClusterById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id        个人群id        query   true     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_deletePersonalClusterById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 查询个人群
+ (void)getPersonalClusterById:(long)userId Id:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id               群id                 query   false     string
     userId           用户id               query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_getPersonalClusterById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 查询用户在群信息
+ (void)getUserClusterVoWithId:(long)userId clusterId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId        群id                 query   true     Integer
     userId           用户id               query   true     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager GetWithUrl:URL_getUserClusterVo withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 我加入的群组
+ (void)myJoinedClusterWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId           用户id               query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_myJoinedCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 搜索个人群
+ (void)searchPersonalClusterWithId:(NSString *)sysId name:(NSString *)name withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     name           群名              query   false     string
     sysId         群号               query   false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"sysId": sysId,
                           @"name": name};
    [manager GetWithUrl:URL_searchPersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 搜索群后查询群信息
+ (void)searchToClusterInfoWithId:(long)userId Id:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id               群id                 query   false     Integer
     userId           用户id               query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_searchToClusterInfo withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 修改个人群
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId audit:(int)audit auditAnswer:(NSString *)auditAnswer auditQuestion:(NSString *)auditQuestion avatar:(long)avatar chair:(int)chair clusterIntro:(NSString *)clusterIntro clusterName:(NSString *)clusterName gameId:(long)gameId isNoDisturbing:(int)isNoDisturbing isShield:(int)isShield isTop:(int)isTop withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                      query   true     Integer
     userId         当前用户id                 query   true     Integer
     audit          入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题） query   false     Integer
     auditAnswer    入群验证答案               query   false     String
     auditQuestion  入群验证问题               query   false     String
     avatar         头像id                    query   false     long
     chair          是否开启主席模式（0关闭，1开启）query   false    long
     clusterIntro   群介绍                     query   false     String
     clusterName    群名称                     query   false     String
     gameId         群游戏                     query   false     Integer
     isNoDisturbing 是否免打扰：0关闭 1开启       query   false     Integer
     isShield       是否屏蔽：0屏蔽 1不屏蔽       query   false     Integer
     isTop          是否置顶：0不置顶 1置顶       query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"auditAnswer": auditAnswer,
                           @"auditQuestion": auditQuestion,
                           @"clusterIntro": clusterIntro,
                           @"clusterName": clusterName,
                           @"avatar": [NSNumber numberWithLong:avatar],
                           @"userId": [NSNumber numberWithLong:userId],
                           @"gameId": [NSNumber numberWithLong:gameId],
                           @"chair": [NSNumber numberWithInt:chair],
                           @"audit": [NSNumber numberWithInt:audit],
                           @"isNoDisturbing": [NSNumber numberWithInt:isNoDisturbing],
                           @"isShield": [NSNumber numberWithInt:isShield],
                           @"isTop": [NSNumber numberWithInt:isTop],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题）
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId audit:(long)audit auditQuestion:(NSString *)auditQuestion auditAnswer:(NSString *)auditAnswer withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                       query   true     Integer
     userId         当前用户id                  query   true     Integer
     auditAnswer    入群验证答案               query   false     String
     auditQuestion  入群验证问题               query   false     String
     audit          入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题） query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"audit": [NSNumber numberWithLong:audit],
                           @"userId": [NSNumber numberWithLong:userId],
                           @"auditAnswer": auditAnswer,
                           @"auditQuestion": auditQuestion,
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题）
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId audit:(long)audit withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                       query   true     Integer
     userId         当前用户id                  query   true     Integer
     audit          入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题） query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"audit": [NSNumber numberWithLong:audit],
                           @"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群是否开启主席模式（0关闭，1开启）
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId chair:(long)chair withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                       query   true     Integer
     userId         当前用户id                  query   true     Integer
     chair          是否开启主席模式（0关闭，1开启）query   false    long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"chair": [NSNumber numberWithLong:chair],
                           @"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群是否屏蔽：0屏蔽 1不屏蔽
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId isShield:(long)isShield withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                       query   true     Integer
     userId         当前用户id                  query   true     Integer
     isShield       是否屏蔽：0屏蔽 1不屏蔽       query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"isShield": [NSNumber numberWithLong:isShield],
                           @"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群是否免打扰：0关闭 1开启
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId isNoDisturbing:(long)isNoDisturbing withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                       query   true     Integer
     userId         当前用户id                  query   true     Integer
     isNoDisturbing 是否免打扰:0关闭 1开启        query   false    Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"isNoDisturbing": [NSNumber numberWithLong:isNoDisturbing],
                           @"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群介绍
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId clusterIntro:(NSString *)clusterIntro withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                      query   true     Integer
     userId         当前用户id                 query   true     Integer
     clusterIntro   群介绍                     query   false    String
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"clusterIntro": clusterIntro,
                           @"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群名称
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId clusterName:(NSString *)clusterName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                      query   true     Integer
     userId         当前用户id                 query   true     Integer
     clusterName    群名称                     query   false     String
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"clusterName": clusterName,
                           @"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改个人群名称(是否置顶)
+ (void)updatePersonalClusterWithId:(long)Id userId:(long)userId isTop:(long)isTop withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id             群id                      query   true     Integer
     userId         当前用户id                 query   true     Integer
     isTop          是否置顶：0不置顶 1置顶       query   false     Integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"clusterName": [NSNumber numberWithLong:isTop],
                           @"userId": [NSNumber numberWithLong:userId],
                           @"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_updatePersonalCluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 代理管理
// 申请成为代理
+ (void)addAgentWithId:(long)userId agentArea:(NSString *)agentArea content:(NSString *)content name:(NSString *)name phone:(NSString *)phone withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     agentArea   代理区域              query   true     String
     content     经验简介              query   true     String
     name        代理姓名              query   true     String
     phone       手机号                query   true     String
     userId      用户id               query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"agentArea": agentArea,
                           @"content": content,
                           @"name": name,
                           @"phone": phone,
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addAgent withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 添加下属
+ (void)addSubordinateWithId:(long)userId subordinateIds:(NSString *)subordinateIds withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     subordinateIds   下属ids       query   true     String
     userId         用户id          query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"subordinateIds": subordinateIds,
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addSubordinate withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 添加下属列表
+ (void)addSubordinateListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id          query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addSubordinateList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 代理信息,根据status判断-1：拒绝，0：待审核，1：正常，2：取消代理
+ (void)agentInfoWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id          query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_agentInfo withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 代理金额明细
+ (void)agentProfitDetailWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id          query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_agentProfitDetail withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 解除代理
+ (void)removeSubordinateWithId:(long)subordinateId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     subordinateId         下属id         query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"subordinateId": [NSNumber numberWithLong:subordinateId]};
    [manager PostWithUrl:URL_removeSubordinate withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 下属列表
+ (void)subordinateListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id          query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_subordinateList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


#pragma mark - 关注/好友
// 关注
+ (void)addFollowsMappingWithId:(long)userId followUserId:(long)followUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     followUserId   被关注用户id     query   false     Long
     userId         用户id          query   false     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"followUserId": [NSNumber numberWithLong:followUserId]};
    [manager PostWithUrl:URL_addFollowsMapping withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 取消关注
+ (void)deleteFollowsMappingWithId:(long)userId followUserId:(long)followUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     followUserId   被关注用户id     query   false     Long
     userId         用户id          query   false     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"followUserId": [NSNumber numberWithLong:followUserId]};
    [manager PostWithUrl:URL_deleteFollowsMapping withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 粉丝列表
+ (void)fansListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id          query   false     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_fansList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 关注列表
+ (void)followListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id          query   false     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_followList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 好友列表
+ (void)friendsListWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id          query   false     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_friendsList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 是否好友
+ (void)isFriendWithId:(long)userId toUserId:(long)toUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     toUserId       对方用户id     query   false     Long
     userId         用户id        query   false     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"toUserId": [NSNumber numberWithLong:toUserId]};
    [manager PostWithUrl:URL_isFriend withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 地区城市
// 获得城市信息
+ (void)getCityWithAreaCode:(NSString *)areaCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     areaCode       省份的地区id     query   false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"areaCode": areaCode};
    [manager GetWithUrl:URL_getCity withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 获得省份列表
+ (void)getProvinceSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {

    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager GetWithUrl:URL_getProvince withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 实时匹配
// 取消实时匹配
+ (void)cancelMatchWithId:(long)userId typeId:(long)typeId chatType:(int)chatType withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     chatType  聊天类型：1语音 2视频 3图文  query   true     Long
     typeId         类型ID              query   true     Long
     userId         用户id              query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"chatType": [NSNumber numberWithInt:chatType],
                           @"typeId": [NSNumber numberWithLong:typeId]};
    [manager GetWithUrl:URL_cancelMatch withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 根据类型实时匹配
+ (void)matchByTypeWithId:(long)userId typeId:(long)typeId chatType:(int)chatType avatar:(NSString *)avatar nickname:(NSString *)nickname withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     avatar       用户头像               query   true     string
     nickname     用户昵称               query   true     string
     chatType  聊天类型：1语音 2视频 3图文  query   true     Long
     typeId         类型ID              query   true     Long
     userId         用户id              query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"avatar": avatar,
                           @"nickname": nickname,
                           @"chatType": [NSNumber numberWithInt:chatType],
                           @"typeId": [NSNumber numberWithLong:typeId]};
    [manager GetWithUrl:URL_matchByType withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 解锁当次需要消耗的金币
+ (void)unlockIngWithId:(long)userId typeId:(long)typeId chatType:(int)chatType gold:(long)gold type:(int)type avatar:(NSString *)avatar nickname:(NSString *)nickname withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     avatar       用户头像               query   true     string
     gold         所需要的消耗金币         query   true     Long
     nickname     用户昵称               query   true     string
     type  1：匹配 2：语音，3：视频，4：聊天  query   true    ref
     userId         用户id              query   true     Long
     chatType  聊天类型：1语音 2视频 3图文  query  false     Long
     typeId         类型ID              query   false     Long
     *///聊天类型：1语音 2视频 3图文 (只有type是匹配的时候才传)
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"gold": [NSNumber numberWithLong:gold],
                           @"avatar": avatar,
                           @"nickname": nickname,
                           @"chatType": [NSNumber numberWithInt:chatType],
                           @"type": [NSNumber numberWithInt:type],
                           @"typeId": [NSNumber numberWithLong:typeId]};
    [manager GetWithUrl:URL_unlockIng withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 意见反馈
// 新增用户意见反馈
+ (void)addUserFeedbackWithId:(long)userId contact:(NSString *)contact picUrls:(NSString *)picUrls title:(NSString *)title views:(NSString *)views withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     contact        联系人                     query   true     string
     picUrls 反馈意见图片的urls，多个url用逗号隔开 query   true     string
     title          标题                      query   true     String
     views         反馈意见                    query   true     String
     userId         用户id                    query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"title": title,
                           @"views": views,
                           @"picUrls": picUrls,
                           @"contact": contact};
    [manager PostWithUrl:URL_addUserFeedback withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


#pragma mark - 搜索
// 分页条件查询房间
+ (void)getSearchRoomPages:(int)pageNum pageSize:(int)pageSize roomName:(NSString *)roomName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true     ref
     pageSize       每页记录数     query   true     ref
     roomName     输入的房间名     query   false    string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"pageNum": @(pageNum),
                           @"pageSize": @(pageSize),
                           @"roomName": roomName};
    [manager GetWithUrl:URL_getSearchRoomPages withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 分页条件查询用户
+ (void)getSearchUserPages:(int)pageNum userId:(long)userId pageSize:(int)pageSize userName:(NSString *)userName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true     ref
     pageSize       每页记录数     query   true     ref
     userName     输入的用户名     query   false    string
     userId   发起搜索的用户id      query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"pageNum": @(pageNum),
                           @"pageSize": @(pageSize),
                           @"userId": [NSNumber numberWithLong:userId],
                           @"userName": userName};
    [manager GetWithUrl:URL_getSearchUserPages withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 分页条件搜索个人群
+ (void)searchPersonalClustersPages:(int)pageNum sysId:(NSString *)sysId pageSize:(int)pageSize name:(NSString *)name withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true     ref
     pageSize       每页记录数     query   true     ref
     name           群名          query   false    string
     sysId          群号          query   false    string
     */
    if (!sysId) {
        sysId = @"";
    }
    if (!name) {
        name = @"";
    }
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"pageNum": @(pageNum),
                           @"pageSize": @(pageSize),
                           @"sysId": sysId,
                           @"name": name};
    [manager GetWithUrl:URL_searchPersonalClusters withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


#pragma mark - 收藏--动态（作品）、课程、房间
// 新增(取消)收藏---课程、房间
+ (void)addCollectWithId:(long)userId typeId:(long)typeId type:(int)type status:(int)status withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     status       点赞状态，0取消，1点赞     query   true     Long
     type         收藏类型：1房间 2课程      query   true     Long
     typeId       收藏类型的id             query   true     Long
     userId         用户id                query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"type": [NSNumber numberWithInt:type],
                           @"status": [NSNumber numberWithInt:status],
                           @"typeId": [NSNumber numberWithLong:typeId]};
    [manager PostWithUrl:URL_addCollect withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 分页查询课程收藏列表
+ (void)getCourseCollectListPages:(int)pageNum userId:(long)userId pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true     ref
     pageSize       每页记录数     query   true     ref
     userId          用户id      query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"pageNum": @(pageNum),
                           @"pageSize": @(pageSize),
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_getCourseCollectList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 分页查询动态收藏列表
+ (void)getPublishCollectListPages:(int)pageNum userId:(long)userId pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true     ref
     pageSize       每页记录数     query   true     ref
     userId          用户id      query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"pageNum": @(pageNum),
                           @"pageSize": @(pageSize),
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_getPublishCollectList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 教练管理
// 教练认证
+ (void)addCoachWithId:(long)userId applyCrowd:(NSString *)applyCrowd content:(NSString *)content languages:(NSString *)languages name:(NSString *)name phone:(NSString *)phone sex:(NSString *)sex teachingContent:(NSString *)teachingContent withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     applyCrowd        适应人群，关联标签id拼接      query   true     string
     content           简介                       query   true     string
     languages         语种，关联标签id拼接          query   true     string
     name              教练姓名                    query   true     String
     phone             手机                       query   true     String
     sex               性别0女，1男                query   true     String
     teachingContent   教学内容，关联标签id拼接      query   true     String
     userId            用户id                     query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"applyCrowd": applyCrowd,
                           @"content": content,
                           @"languages": languages,
                           @"phone": phone,
                           @"sex": sex,
                           @"teachingContent": teachingContent,
                           @"name": name};
    [manager PostWithUrl:URL_addCoach withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 教练详情
+ (void)coachDetailWithId:(long)userId coachId:(long)coachId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     coachId           教练id                     query   true     Long
     userId            用户id                     query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"coachId": [NSNumber numberWithLong:coachId]};
    [manager PostWithUrl:URL_coachDetail withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 教练认证前验证,有返回为已认证教练，无返回为可进行认证教练
+ (void)coachVerifyWithId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId            用户id                     query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_coachVerify withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 搜索教练(找教练筛选)
+ (void)searchCoachWithId:(long)userId applyCrowd:(long)applyCrowd price:(NSString *)price name:(NSString *)name pageNum:(int)pageNum pageSize:(int)pageSize teachingContent:(long)teachingContent withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum           当前页                     query   true     ref
     pageSize          每页记录数                  query   true     ref
     applyCrowd        适应人群，标签id             query   false    Long
     name              教练姓名                    query   false     String
     price             价格区间，逗号连接            query   false     String
     teachingContent   教学内容，标签id             query   false     Long
     userId            用户id                     query   false     Long
     */
    
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"applyCrowd": [NSNumber numberWithLong:applyCrowd],
                           @"price": price,
                           @"pageNum": [NSNumber numberWithInt:pageNum],
                           @"pageSize": [NSNumber numberWithInt:pageSize],
                           @"teachingContent": [NSNumber numberWithLong:teachingContent],
                           @"name": name};
    [manager PostWithUrl:URL_searchCoach withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 搜索教练(首页搜索教练，和上面用一个接口)
+ (void)homeSearchCoach:(NSMutableDictionary *)dictionary withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    [manager PostWithUrl:URL_searchCoach withParams:dictionary withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}

// 修改教练详情
+ (void)updateCoachWithMutableDictionary:(NSMutableDictionary *)dic withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     applyCrowd        适应人群，关联标签id拼接      query   true     string
     content           简介                       query   true     string
     languages         语种                       query   true     string
     phone             手机                       query   true     String
     teachingContent   教学内容，关联标签id拼接      query   true     String
     userId            用户id                     query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
//    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
//                           @"applyCrowd": applyCrowd,
//                           @"content": content,
//                           @"languages": languages,
//                           @"phone": phone,
//                           @"teachingContent": teachingContent};
    [manager PostWithUrl:URL_updateCoach withParams:dic withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


#pragma mark - 文件管理
// base64上传文件
+ (void)base64Size:(long)size contentType:(NSString *)contentType file64:(NSString *)file64 fileName:(NSString *)fileName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     contentType   文件类型        query   true     string
     file64       base64编码      query   true     string
     fileName      文件名称        query   true     string
     size          文件大小        query   true     ref
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"contentType": contentType,
                           @"file64": file64,
                           @"fileName": fileName,
                           @"size": [NSNumber numberWithLong:size]};
    [manager GetWithUrl:URL_base64 withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 根据文件名删除文件
+ (void)deleteByName:(NSString *)fileName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     fileName      文件名称        query   true     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"fileName": fileName};
    [manager GetWithUrl:URL_deleteByName withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 根据文件id查询文件
+ (void)downloadById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id      文件id        query   true     red
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_downloadById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 根据文件名查询文件
+ (void)downloadByName:(NSString *)fileName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     fileName      文件名称        query   true     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"fileName": fileName};
    [manager GetWithUrl:URL_downloadByName withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 上传文件
+ (void)postFileWithFilepath:(NSString *)path withName:(NSString *)typeName withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    [manager uploadFileWithFilePath:path withName:@"file" withMimeType:typeName withUrl:URL_upload withParams:nil withSuccess:^(id  _Nullable response) {
        if (success) {
            success(response);
        }
    } withFail:^(NSError * _Nullable error) {
        if (fail) {
            fail(error);
        }
        
    } withDownloadProgress:^(int64_t bytesRead, int64_t totalBytes) {
       
    } withShouldLogin:NO];
}


#pragma mark - 查询标签
// 根据类型查询标签：0日常爱好  1教学内容   2适用人群   3语种
+ (void)getLabelByType:(long)type withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     type        标签类型：0日常爱好 1教学内容 2适用人群 3语种     query   true     ref
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"type": [NSNumber numberWithLong:type]};
    [manager GetWithUrl:URL_getLabelByType withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 用户管理
// 拉黑/取消拉黑用户
+ (void)blackUserWithId:(long)userId blackUserId:(long)blackUserId type:(int)type withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     blackUserId   被拉黑的用户id          query   true     Long
     type         0：取消拉黑,其它值：拉黑   query   true     ref
     userId         用户id                query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"type": [NSNumber numberWithInt:type],
                           @"blackUserId": [NSNumber numberWithLong:blackUserId]};
    [manager PostWithUrl:URL_blackUser withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 更换手机
+ (void)changePhoneWithId:(long)userId phone:(NSString *)phone vCode:(NSString *)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     phone   手机         query   true     string
     vCode   验证码        query   true    string
     id      用户id       query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:userId],
                           @"vCode": vCode,
                           @"phone": phone};
    [manager PostWithUrl:URL_changePhone withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 忘记密码
+ (void)forgetPwd:(NSString *)newPwd phone:(NSString *)phone vCode:(NSString *)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     phone   手机         query   true     string
     vCode   验证码        query   true    string
     newPwd  新密码       query   true     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"newPwd": newPwd,
                           @"vCode": vCode,
                           @"phone": phone};
    [manager PostWithUrl:URL_forgetPwd withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 我的黑名单
+ (void)getMyBlackUser:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id                query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_getMyBlackUser withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 个人资料
+ (void)getUserById:(long)userId toUserId:(long)toUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     toUserId      被查看的用户id          query   true     Long
     userId         用户id                query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"toUserId": [NSNumber numberWithLong:toUserId]};
    [manager PostWithUrl:URL_getUserById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// IM关注
+ (void)imFocus:(long)userId targetUserId:(long)targetUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     targetUserId   对方用户id             query   true     Long
     userId         用户id                query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"targetUserId": [NSNumber numberWithLong:targetUserId]};
    [manager GetWithUrl:URL_imFocus withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 个人资料进语音/视频/聊天判断
+ (void)interactCount:(long)userId type:(int)type withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     type      2：语音，3：视频，4：聊天      query   true     ref
     userId         用户id                query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"type": [NSNumber numberWithInt:type]};
    [manager PostWithUrl:URL_interactCount withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 邀请码邀请
+ (void)inviteUser:(long)Id inviteCode:(NSString *)inviteCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     inviteCode   邀请码      query   true     string
     id         用户id        query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
                           @"inviteCode": inviteCode};
    [manager PostWithUrl:URL_inviteUser withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 我的 页面
+ (void)myPage:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id        query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_myPage withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 匹配剩余次数
+ (void)remainMatchCount:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户id        query   true     Long
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_remainMatchCount withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 举报（投诉）用户、房间、课程、动态、音乐、教练
+ (void)report:(long)userId reportId:(long)reportId reportType:(int)reportType type:(int)type imageUrl:(NSString *)imageUrl message:(NSString *)message withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     reportId   被举报的信息id-- 用户、房间、课程、动态、音乐、教练    query   true     Long
     reportType 举报类型（1泄露隐私，2人身攻击，3淫秽色情，4垃圾广告，5敏感信息，6侵权，7其他）   query   true     Long
     type       举报种类：0 用户 1房间 2课程 3动态 4音乐 5教练     query   true     Long
     userId       发起举报的用户id                             query   true     Long
     imageUrl    举报证据图片url（多张逗号分隔）                  query   true     String
     message       举报描述信息                                 query   true     String
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"message": message,
                           @"imageUrl": imageUrl,
                           @"reportId": [NSNumber numberWithLong:reportId],
                           @"reportType": [NSNumber numberWithInt:reportType],
                           @"type": [NSNumber numberWithInt:type]};
    [manager PostWithUrl:URL_report withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 修改密码
+ (void)updPwd:(long)Id newPwd:(NSString *)newPwd oldPwd:(NSString *)oldPwd withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id       用户id                  query   true     Long
     newPwd    新密码                 query   true     String
     oldPwd    旧密码                 query   true     String
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
                           @"newPwd": newPwd,
                           @"oldPwd": oldPwd};
    [manager PostWithUrl:URL_updPwd withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 用户登陆成功后完善资料或用户资料修改时调用此接口
+ (void)updUser:(long)Id avatar:(NSString *)avatar nickname:(NSString *)nickname area:(NSString *)area birthday:(NSString *)birthday city:(NSString *)city height:(long)height hobby:(NSString *)hobby introduce:(NSString *)introduce loginLat:(NSString *)loginLat loginLot:(NSString *)loginLot occupation:(NSString *)occupation province:(NSString *)province qq:(NSString *)qq sex:(int)sex weight:(int)weight wx:(NSString *)wx withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     avatar     头像url                 query   true     String
     id         用户id                  query   true     Long
     nickname   昵称                  query    true     String
     area       区                   query    false     String
     birthday   生日                  query    false     String
     city       市                   query     false     String
     height     身高                 query     false     Long
     hobby      爱好，id拼接           query    false     String
     introduce  个人简介               query    false     String
     loginLat   登陆纬度                query    false     String
     loginLot   登陆经度                query    false     String
     occupation   职业id               query    false     String
     province     省                   query    false     String
     qq       QQ的openid               query    false     String
     sex       性别0：女，1：男          query    true     Long
     weight    体重                    query    true     Long
     wx       微信openid              query     false     String
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
                           @"height": [NSNumber numberWithLong:height],
                           @"avatar": avatar,
                           @"nickname": nickname,
                           @"area": area,
                           @"birthday": birthday,
                           @"city": city,
                           @"hobby": hobby,
                           @"introduce": introduce,
                           @"loginLat": loginLat,
                           @"loginLot": loginLot,
                           @"occupation": occupation,
                           @"province": province,
                           @"qq": qq,
                           @"wx": wx,
                           @"sex": [NSNumber numberWithLong:sex],
                           @"weight": [NSNumber numberWithLong:weight]};
    [manager PostWithUrl:URL_updUser withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 更新/绑定用户的微信
+ (void)postUserUpUserWxSetWithId:(long)userId openId:(NSString *)openId phone:(NSString *)phone vCode:(NSString *)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
    /*
     openId          微信openid      query   true     string
     phone             手机号         query   true     string
     vCode            验证码          query   true     string
     userId          当前用户id       query   true     integer
     
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"openId": openId,
                           @"phone" : phone,
                           @"vCode" : vCode};
    [manager GetWithUrl:URL_upUserWx withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 解绑微信
+ (void)postUserRomveUserWxSetWithId:(long)userId phone:(NSString *)phone vCode:(NSString *)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
    /*
     phone             手机号         query   true     string
     vCode            验证码          query   true     string
     userId          当前用户id       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"phone" : phone,
                           @"vCode" : vCode};
    [manager GetWithUrl:URL_romveUserWx withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


#pragma mark - 用户聊天设置
// 查询用户聊天信息设置
+ (void)getUserChatSetWithId:(long)userId toUserId:(long)toUserId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     toUserId      被操作的用户id      query   true     integer
     userId          当前用户id       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"toUserId": [NSNumber numberWithLong:toUserId]};
    [manager GetWithUrl:URL_getUserChatSet withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改用户聊天设置
+ (void)updateUserChatSetWithId:(long)userId toUserId:(long)toUserId black:(int)black report:(int)report silence:(int)silence top:(int)top withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     toUserId      被操作的用户id      query   true     integer
     userId          当前用户id       query   true     integer
     black  是否举报 1: 举报 0：不举报  query   true     integer
     report 是否举报 1：已举报 0：未举报   query   true     integer
     silence 是否免打扰 1：开启免打扰 0：关闭免打扰 query   true     integer
     top   是否置顶 1：置顶 0：不置顶    query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"black": [NSNumber numberWithInt:black],
                           @"report": [NSNumber numberWithInt:report],
                           @"silence": [NSNumber numberWithInt:silence],
                           @"top": [NSNumber numberWithInt:top],
                           @"toUserId": [NSNumber numberWithLong:toUserId]};
    [manager PostWithUrl:URL_updateUserChatSet withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 修改用户聊天设置(是否免打扰)
+ (void)updateUserChatSetWithId:(long)userId toUserId:(long)toUserId silence:(int)silence withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     toUserId      被操作的用户id      query   true     integer
     userId          当前用户id       query   true     integer
     silence 是否免打扰 1：开启免打扰 0：关闭免打扰 query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"silence": [NSNumber numberWithInt:silence],
                           @"toUserId": [NSNumber numberWithLong:toUserId]};
    [manager PostWithUrl:URL_updateUserChatSet withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}

#pragma mark - 用户聊天设置
// 获取验证码
+ (void)getVerifyCode:(NSString *)phone withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     phone         手机        query   true     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"phone": phone};
    [manager PostWithUrl:URL_getVerifyCode withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 验证码校验
+ (void)verifyCode:(NSString *)phone vCode:(long)vCode withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     phone         手机        query   true     ref
     vCode         验证码       query   true     ref
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"phone": phone,
                           @"vCode": [NSNumber numberWithLong:vCode]};
    [manager PostWithUrl:URL_verifyCode withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


#pragma mark - 系统消息
// 群组消息助手
+ (void)clusterWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_cluster withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 评论消息
+ (void)commentMessageWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_commentMessage withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 课程消息
+ (void)coursesMessageWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_coursesMessage withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 处理群组入群通知
+ (void)dealClusterAccessId:(long)Id status:(int)status withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     status    是否同意 0：同意 1：拒绝   query   true    ref
     id             该通知id           query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
                           @"status": @(status)};
    [manager GetWithUrl:URL_dealClusterAccess withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 系统消息主界面
+ (void)indexWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_index withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 官方消息
+ (void)officialMessageWithPage:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_officialMessage withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 消息-房间
+ (void)roomMessageWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_roomMessage withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 房间在线用户列表
+ (void)roomWealthToDayWithUserId:(NSString *)userId roomNum:(NSString *)roomNum withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     roomNum        房间号                     query   true     string
     userId         用户id数组(多个逗号隔开)      query   true     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": userId,
                           @"roomNum": roomNum};
    [manager GetWithUrl:URL_roomWealthToDay withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 钱包消息
+ (void)walletMessageWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_walletMessage withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 职业信息
// 获取行业信息
+ (void)getIndustrySuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager GetWithUrl:URL_getIndustry withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 获取行业信息
+ (void)getProfessionWithId:(long)industryId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     industryId         行业id       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"industryId": [NSNumber numberWithLong:industryId]};
    [manager GetWithUrl:URL_getProfession withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 设置个人禁言
// 新增个人群禁言
+ (void)addPersonalClusterSilenceWithUserId:(long)userId clusterId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId      群Id         query   true     integer
     userId         被禁言人id     query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"clusterId": [NSNumber numberWithLong:clusterId],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addPersonalClusterSilence withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 解除个人群禁言
+ (void)deletePersonalClusterSilenceById:(long)userId clusterId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId      群Id         query   true     integer
     userId         被禁言人id     query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"clusterId": [NSNumber numberWithLong:clusterId],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_deletePersonalClusterSilenceById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 查询个人群禁言
+ (void)getPersonalClusterSilenceWithClusterId:(long)clusterId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     clusterId      群Id         query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"clusterId": [NSNumber numberWithLong:clusterId]};
    [manager GetWithUrl:URL_getPersonalClusterSilence withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 课程评价管理
// 新增课程评价
+ (void)addEvaluationWithId:(long)userId courseId:(long)courseId content:(NSString *)content evaluation:(int)evaluation withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     content       评价               query   false     string
     courseId     课程id              query   false     integer
     evaluation 1:好评，2：中评，3：差评 query   false     integer
     userId         用户id            query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"courseId": [NSNumber numberWithLong:courseId],
                           @"content": content,
                           @"evaluation": [NSNumber numberWithInt:evaluation],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addEvaluation withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 删除评价
+ (void)delEvaluationById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id      课程评价id         query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_delEvaluationById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 查询课程评价
+ (void)getEvaluationById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id      课程评价id         query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager GetWithUrl:URL_getEvaluationById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 邀请码邀请记录
// 邀请记录
+ (void)inviteLogWithUserId:(long)userId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     userId         用户ID       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_inviteLog withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


#pragma mark - 钱包管理
// 发起提现申请(支付宝)
+ (void)alipayAddWalletWithdrawWithId:(long)userId account:(NSString *)account name:(NSString *)name type:(int)type number:(long)number withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     account     支付宝账号                         query   true     string
     name       支付宝用户名                        query   true     string
     number     金额(金币或者钻石)                   query   true     integer
     type   提现类型 0:代理提现 1:课程收益提现         query   true     integer
     userId      当前用户id                         query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"account": account,
                           @"name": name,
                           @"number": [NSNumber numberWithLong:number],
                           @"type": [NSNumber numberWithInt:type],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_alipayAddWalletWithdraw withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 发起提现申请(微信)
+ (void)wxpayAddWalletWithdrawWithId:(long)userId account:(NSString *)account type:(int)type number:(long)number withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     account       微信账号                         query   true     string
     number        金额(E币)                        query   true     integer
     type   提现类型 0:代理提现 1:课程收益提现          query   true     integer
     userId        当前用户id                       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"account": account,
                           @"number": [NSNumber numberWithLong:number],
                           @"type": [NSNumber numberWithInt:type],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_wxpayAddWalletWithdraw withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}


// 获得交易详情
+ (void)getDealDetailWithId:(NSString *)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     orderId      订单流水号        query   true     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"orderId": orderId};
    [manager GetWithUrl:URL_getDealDetail withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 钱包余额
+ (void)getWalletByUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     phoneType   充值手机渠道 0：安卓 1：苹果      query   true     ref
     userId      当前用户id                     query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"phoneType": [NSNumber numberWithInt:1],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_getWalletByUserId withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 查询钱包交易记录
+ (void)getWalletLogById:(long)userId type:(int)type withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     type   查询类型 0钱包明细 1代理明细 2课程明细   query   true     ref
     userId      用户id                       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"type": [NSNumber numberWithInt:type],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_getWalletLogById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 充值金币
+ (void)rechargeGoldWithUserId:(long)userId accountType:(int)accountType number:(long)number withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     accountType 充值账号渠道 ：银行 1：微信 2：支付宝 3苹果内购 query   true     ref
     number      充值金额（元）                   query   true     integer
     phoneType   充值手机渠道 0：安卓 1：苹果      query   true     ref
     userId      当前用户id                     query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"phoneType": [NSNumber numberWithInt:1],
                           @"accountType": [NSNumber numberWithInt:accountType],
                           @"number": [NSNumber numberWithLong:number],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_rechargeGold withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 充值金币-支付后回调
+ (void)rechargeGoldPaySuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager PostWithUrl:URL_rechargeGoldPay withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 充值金币-微信支付后回调
+ (void)rechargeGoldWxPaySuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager PostWithUrl:URL_rechargeGoldWxPay withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 充值页面
+ (void)rechargePageWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     phoneType   充值手机渠道 0：安卓 1：苹果      query   true     ref
     userId      当前用户id                     query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"phoneType": [NSNumber numberWithInt:1],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_rechargePage withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 频道歌曲
// 上传歌曲
+ (void)musicWithUserId:(long)userId pathId:(long)pathId menuId:(long)menuId type:(int)type singer:(NSString *)singer name:(NSString *)name withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     menuId     歌单id               query   true     integer
     name       歌名                  query   true     string
     pathId     歌曲文件id              query   true     ref
     singer     歌手                  query   true     string
     type       歌曲类型0原唱，1伴奏      query   true     ref
     userId      当前用户id             query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"type": [NSNumber numberWithInt:type],
                           @"name": name,
                           @"singer": singer,
                           @"pathId": [NSNumber numberWithLong:pathId],
                           @"menuId": [NSNumber numberWithLong:menuId],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_Music withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 新增频道歌曲，单个
+ (void)addMusicChannelWithUserId:(long)userId musicId:(long)musicId roomId:(long)roomId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     musicId    歌曲id               query   false     integer
     roomId      房间Id              query   true     integer
     userId      用户id              query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"musicId": [NSNumber numberWithLong:musicId],
                           @"roomId": [NSNumber numberWithLong:roomId],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addMusicChannel withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 新增频道歌曲，多个
+ (void)addMusicChannelsWithUserId:(long)userId menuId:(long)menuId roomId:(long)roomId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     menuId     歌单id               query   false     integer
     roomId      房间Id              query   true     integer
     userId      用户id              query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"menuId": [NSNumber numberWithLong:menuId],
                           @"roomId": [NSNumber numberWithLong:roomId],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_addMusicChannels withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 用户下麦，删除频道所有歌曲
+ (void)deleteMusicChannelWithUserId:(long)userId roomId:(long)roomId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     roomId      房间Id              query   true     integer
     userId      用户id              query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"roomId": [NSNumber numberWithLong:roomId],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_deleteMusicChannel withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 删除频道歌曲
+ (void)deleteMusicChannelById:(long)Id withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id      频道歌曲id         query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id]};
    [manager PostWithUrl:URL_deleteMusicChannelById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 分页查询频道歌曲列表
+ (void)getMusicChannelByRoomId:(long)roomId pageNum:(int)pageNum pageSize:(int)pageSize withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     roomId         房间id       query   true     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"roomId": [NSNumber numberWithLong:roomId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize)};
    [manager GetWithUrl:URL_getMusicChannelByRoomId withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}


#pragma mark - 课程管理
// 新增课程
+ (void)addCourseWithUserId:(long)userId applyCrowd:(NSString *)applyCrowd content:(NSString *)content name:(NSString *)name pics:(NSString *)pics time:(long)time teachingContent:(long)teachingContent price:(long)price withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     applyCrowd         适应人群，关联标签id拼接字符串    query   false     string
     content            简介                          query   false     string
     name               课程名称                       query   false     string
     pics               图片url拼接字符串               query   false     string
     price              价格                          query   false     integer
     teachingContent    教学内容，标签id                query   false     integer
     time               课程时间                       query   false     integer
     userId             用户id                        query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"time": [NSNumber numberWithLong:time],
                           @"teachingContent": [NSNumber numberWithLong:teachingContent],
                           @"price": [NSNumber numberWithLong:price],
                           @"name": name,
                           @"pics": pics,
                           @"content": content,
                           @"applyCrowd": applyCrowd};
    [manager PostWithUrl:URL_addCourse withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 同意退款
+ (void)agreeRefundWithOrderId:(long)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     orderId      订单id         query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"orderId": [NSNumber numberWithLong:orderId]};
    [manager PostWithUrl:URL_agreeRefund withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 支付宝购买课程
+ (void)buyCourseSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager PostWithUrl:URL_buyCourse withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 金币购买课程
+ (void)buyCourseByGoldWithUserId:(long)userId courseId:(long)courseId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     courseId    课程id               query   false     integer
     phoneType 支付手机类型 0：安卓 1：苹果  query   false     integer
     userId      用户id               query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId],
                           @"phoneType": @(1),
                           @"courseId": [NSNumber numberWithLong:courseId]};
    [manager PostWithUrl:URL_buyCourseByGold withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 微信购买课程
+ (void)buyCourseWxSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager PostWithUrl:URL_buyCourseWx withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 取消退款
+ (void)cancelRefundWithOrderId:(long)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     orderId      订单id         query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"orderId": [NSNumber numberWithLong:orderId]};
    [manager PostWithUrl:URL_cancelRefund withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 课程上下架
+ (void)changeStatusWithId:(long)Id status:(int)status withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id         订单id           query   false     integer
     status     1:上架，2：下架    query   false     ref
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
                           @"status": [NSNumber numberWithInt:status]};
    [manager PostWithUrl:URL_changeStatus withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 课程编辑列表
+ (void)coachCourseWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId      用户id         query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_coachCourse withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 课程结束
+ (void)courseFinishWithOrderId:(long)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     orderId      订单id                      query   false     integer
     platform  教练手机上课平台（1，安卓，2，苹果） query   false     ref
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"orderId": [NSNumber numberWithLong:orderId],
                           @"platform": [NSNumber numberWithInt:2]};
    [manager PostWithUrl:URL_courseFinish withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 课程订单详情
+ (void)courseOrderWithOrderId:(long)orderId userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     orderId      订单id                  query   false     integer
     userId        用户id                 query   false     ref
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"orderId": [NSNumber numberWithLong:orderId],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_courseOrder withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 课程订单退款
+ (void)courseRefundWithOrderId:(long)orderId reason:(NSString *)reason withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     orderId      订单id                  query   false     integer
     reason        用户id                 query   false     string
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"orderId": [NSNumber numberWithLong:orderId],
                           @"reason": reason};
    [manager PostWithUrl:URL_courseRefund withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 课程开始
+ (void)courseStartWithOrderId:(long)orderId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     orderId      订单id                  query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"orderId": [NSNumber numberWithLong:orderId]};
    [manager PostWithUrl:URL_courseStart withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 课程自动取消天数
+ (void)daysToCancelSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager PostWithUrl:URL_daysToCancel withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 删除课程
+ (void)deleteCourseById:(long)Id userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id           课程id            query   false     integer
     userId       用户id            query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_deleteCourseById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 金币购买课程打折
+ (void)discountByGoldSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{};
    [manager PostWithUrl:URL_discountByGold withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:NO];
}
// 课程详情
+ (void)getCourseById:(long)Id userId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     id           课程id            query   false     integer
     userId       用户id            query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager GetWithUrl:URL_getCourseById withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 我的课程(用户)
+ (void)myCourseWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId      用户id         query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_myCourse withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 课程管理
+ (void)myCourseOrderWithUserId:(long)userId withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     userId      教练用户id        query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_myCourseOrder withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 修改课程
+ (void)updateCourseWithModifyThe:(NSMutableDictionary *)dic withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     applyCrowd         适应人群，关联标签id拼接字符串    query   false     string
     content            简介                          query   false     string
     name               课程名称                       query   false     string
     pics               图片url拼接字符串               query   false     string
     price              价格                          query   false     integer
     teachingContent    教学内容，标签id                query   false     integer
     time               课程时间                       query   false     integer
     id                 课程id                        query   false     integer
     */
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
//    NSDictionary *dict = @{@"id": [NSNumber numberWithLong:Id],
//                           @"time": [NSNumber numberWithLong:time],
//                           @"teachingContent": [NSNumber numberWithLong:teachingContent],
//                           @"price": [NSNumber numberWithLong:price],
//                           @"name": name,
//                           @"pics": pics,
//                           @"content": content,
//                           @"applyCrowd": applyCrowd};
    [manager PostWithUrl:URL_updateCourse withParams:dic withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 微信购买课程
+ (void)wxBuyCourseCourseId:(long)courseId userId:(long)userId success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     accountType 账号类型 0：银行 1：微信 2：支付宝 3苹果内购    query   false   ref
     courseId           课程id                            query   false    integer
     phoneType          支付手机类型 0：安卓 1：苹果          query   false     ref
     userId              用户id                           query   false     string
     */
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"accountType": @(1),
                           @"courseId": [NSNumber numberWithLong:courseId],
                           @"phoneType": @(1),
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_wxBuyCourse withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
// 支付宝购买课程
+ (void)aliBuyCourseCourseId:(long)courseId userId:(long)userId success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     accountType 账号类型 0：银行 1：微信 2：支付宝 3苹果内购    query   false   ref
     courseId           课程id                            query   false    integer
     phoneType          支付手机类型 0：安卓 1：苹果          query   false     ref
     userId              用户id                           query   false     string
     */
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"accountType": @(2),
                           @"courseId": [NSNumber numberWithLong:courseId],
                           @"phoneType": @(1),
                           @"userId": [NSNumber numberWithLong:userId]};
    [manager PostWithUrl:URL_aliBuyCourse withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}

+ (void)uploadImage:(NSArray *)imageArr withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    [[YKHttpRequestManager shared] UploadImagesWithUrl:URL_upload withImages:imageArr withParams:nil withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withProgress:^(int64_t bytesRead, int64_t totalBytes) {
        
    } withShouldLogin:YES];
}

//MARK: 上传图片
+ (void)uploadImagesWith:(NSArray<UIImage *> *)imageArray withSuccess:(HttpResponseSuccessBlock)success fail:(HttpResponseFailBlock)fail withProgress:(nonnull DownloadProgressBlock)progress{
    
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    [manager UploadImagesWithUrl:URL_upload withImages:imageArray withParams:nil withSuccess:^(id  _Nullable response) {
        if (response) {
            NSNumber *code = response[@"status"];
            if (code.intValue==200) {
                NSArray *array = response[@"data"];
                NSLog(@"%@",response[@"msg"]);
                if (success) {
                    success(response);
                }
            }
        }
    } withFail:^(NSError * _Nullable error) {
        if (fail) {
            fail(error);
        }
    } withProgress:^(int64_t bytesRead, int64_t totalBytes) {
        if (progress) {
            progress(bytesRead,totalBytes);
        }
    } withShouldLogin:YES];
}

//get带参数
+ (void)getMyDataWithParameters:(NSMutableDictionary*)parameters UrlString:(NSString*)url Success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail{
      YKHttpRequestManager *manager = [YKHttpRequestManager shared];
         manager.requestSerializer = HttpRequestSerializer;
    
       [manager GetWithUrl:url withParams:parameters withSuccess:^(id  _Nullable response) {
            if (success) {
                      success(response);
                  }
       } withFail:^(NSError * _Nullable error) {
             if (fail) {
                     fail(error);
                 }
       } withShouldLogin:YES];
}
// 公用带参数
+ (void)PostMyDataWithParameters:(NSMutableDictionary*)parameters UrlString:(NSString*)url Success:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
        manager.requestSerializer = HttpRequestSerializer;
      [manager PostWithUrl:url withParams:parameters withSuccess:^(id  _Nullable response) {
          if (success) {
              success(response);
          }
      } withFail:^(NSError * _Nullable error) {
          if (fail) {
              fail(error);
          }
      } withShouldLogin:YES];
}

#pragma 房间管理
// 房间禁言列表
+ (void)getRoomForbiddenList:(long)roomId pageNum:(int)pageNum pageSize:(int)pageSize nickname:(NSString *)nickname withSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    /*
     pageNum         当前页       query   true    ref
     pageSize       每页记录数    query   true     ref
     roomId         房间id       query   true     integer
     nickname       用户昵称      query   false    string
     */
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    NSDictionary *dict = @{@"roomId": [NSNumber numberWithLong:roomId],
                           @"pageNum": @(pageNum),
                           @"pageSize": @(pageSize),
                           @"nickname": nickname};
    [manager GetWithUrl:URL_getRoomForbiddenList withParams:dict withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}

// 获得提现手续费
+ (void)geWithdrawFeeSuccess:(HttpResponseSuccessBlock)success withFail:(HttpResponseFailBlock)fail {
    YKHttpRequestManager *manager = [YKHttpRequestManager shared];
    manager.requestSerializer = HttpRequestSerializer;
    [manager GetWithUrl:URL_geWithdrawFee withParams:@{} withSuccess:^(id  _Nullable response) {
        success(response);
    } withFail:^(NSError * _Nullable error) {
        fail(error);
    } withShouldLogin:YES];
}
@end
