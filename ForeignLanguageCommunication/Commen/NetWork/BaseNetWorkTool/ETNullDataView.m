//
//  ETNullView.m
//  lezhu_new
//
//  Created by zl on 2018/4/28.
//  Copyright © 2018年 Lezhu. All rights reserved.
//

#import "ETNullDataView.h"

@implementation ETNullDataView

- (void)dealloc{
    self.btnNullView = nil;
}
-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];

        self.nullImageView = [[UIImageView alloc]init];
        self.nullImageView.image = [UIImage imageNamed:@"icon_search_none_data"];
        [self addSubview:self.nullImageView];
        [self.nullImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.centerY.mas_equalTo(self).mas_offset(-40);
        }];
        
        self.tiplabel = [[UILabel alloc] init];
        self.tiplabel.textColor =  UIColor.grayColor;
        self.tiplabel.textAlignment = NSTextAlignmentCenter;
        self.tiplabel.font = [UIFont systemFontOfSize:13];
        self.tiplabel.text = @"暂无更多数据";
        [self addSubview:self.tiplabel];
        [self.tiplabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nullImageView.mas_bottom).mas_offset(10);
                make.centerX.mas_equalTo(self.nullImageView);
            }];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];

        self.nullImageView = [[UIImageView alloc]init];
        self.nullImageView.image = [UIImage imageNamed:@"icon_search_none_data"];
        [self addSubview:self.nullImageView];
        [self.nullImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self);
            make.centerY.mas_equalTo(self).mas_offset(-40);
        }];
        
        self.tiplabel = [[UILabel alloc] init];
        self.tiplabel.textColor = UIColor.grayColor;
        self.tiplabel.textAlignment = NSTextAlignmentCenter;
        self.tiplabel.font = [UIFont systemFontOfSize:13];
        self.tiplabel.text = @"暂无更多数据";
        [self addSubview:self.tiplabel];
        [self.tiplabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nullImageView.mas_bottom).mas_offset(10);
            make.centerX.mas_equalTo(self.nullImageView);
        }];
    }
    return self;
}

//- (void)showBtnWithString:(NSString *)tip{
//    UIButton *btn = [[UIButton alloc] init];
//    [btn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
//    btn.layer.cornerRadius  = 2;
//    btn.layer.masksToBounds = YES;
//    btn.layer.borderWidth = 1.0f;
//    btn.layer.borderColor = kColorHex(@"#FE7803").CGColor;
//    btn.titleLabel.font = [UIFont systemFontOfSize:14.];
//    [btn setTitleColor:kColorHex(@"#FE7803") forState:UIControlStateNormal];
//    [self addSubview:btn];
//    [btn setTitle:tip forState:UIControlStateNormal];
//    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.tiplabel.mas_bottom).mas_offset(18);
//        make.centerX.mas_equalTo(self);
//        make.size.mas_equalTo(CGSizeMake(91, 33));
//    }];
//}

- (void)setNullImage:(NSString *)img andTip:(NSString *)tip{
    self.nullImageView.hidden = NO;
    if(img){
        self.nullImageView.image = [UIImage imageNamed:img];
    }
    if(tip){
        self.tiplabel.text = tip;
    }
}

- (void)hideDefaultImageWithTip:(NSString *)tip{
    self.nullImageView.hidden = YES;
    self.tiplabel.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    if(tip){
        self.tiplabel.text = tip;
    }
}

- (void)btnClick{
    if(self.btnNullView){
        self.btnNullView();
    }
}

@end







