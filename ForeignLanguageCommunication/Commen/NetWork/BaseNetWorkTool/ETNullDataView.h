//
//  ETNullView.h
//  lezhu_new
//
//  Created by zl on 2018/4/28.
//  Copyright © 2018年 Lezhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ETNullDataView : UIView
@property (nonatomic,strong) UILabel *tiplabel;
@property (nonatomic,strong) UIImageView *nullImageView;
@property (nonatomic,copy) void(^btnNullView)(void);
- (void)showBtnWithString:(NSString *)tip;
- (void)setNullImage:(NSString *)img andTip:(NSString *)tip;
- (void)hideDefaultImageWithTip:(NSString *)tip;
@end





