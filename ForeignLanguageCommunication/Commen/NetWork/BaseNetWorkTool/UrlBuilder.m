//
//  UrlBuilder.m
//  VoiceLive
//
//  Created by mac on 2020/10/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "UrlBuilder.h"

#pragma mark - 登陆管理
// 验证码登录
NSString * const URL_getLoginByVerifyCode = @"/api/login/getLoginByVerifyCode";
// 通过友盟token一键登录
NSString * const URL_getPhoneByYoumengToken = @"/api/login/getPhoneByYoumengToken";
// 账号密码登录
NSString * const URL_loginByPwd = @"/api/login/loginByPwd";
// qq登录
NSString * const URL_loginByQQ = @"/api/login/loginByQQ";
// 微信登录
NSString * const URL_loginByWeChat = @"/api/login/loginByWeChat";
// 分页查询附近的人
NSString * const URL_nearbyPersonal = @"/api/login/nearbyPersonal";

#pragma mark - 业务参数
// 查询业务配置
NSString * const URL_configGetConfig = @"/api/config/getConfig";

#pragma mark - 首页
// 首页排行榜
NSString * const URL_charts = @"/api/index/charts";
// 首页排行榜(个人)
NSString * const URL_chartsSelf = @"/api/index/chartsSelf";
// 外语角界面
NSString * const URL_englishIndex = @"/api/index/englishIndex";
// 首页匹配类型
NSString * const URL_match = @"/api/index/match";
// 小语种全部匹配类型
NSString * const URL_matchMin = @"/api/index/matchMin";
// 交流圈界面
NSString * const URL_momentsIndex = @"/api/index/momentsIndex";
// 首页搜索-用户
NSString * const URL_searchUser = @"/api/index/searchUser";
// 明星教练
NSString * const URL_starCoach = @"/api/index/starCoach";
// 投票
NSString * const URL_vote = @"/api/index/vote";


#pragma mark - 世界动态
// 新增世界动态评论
NSString * const URL_addPublishComments = @"/api/publishs/addPublishComments";
// 新增动态评论回复
NSString * const URL_addPublishCommentsReply = @"/api/publishs/addPublishCommentsReply";
// 新增世界动态
NSString * const URL_addPublishs = @"/api/publishs/addPublishs";
// 删除动态评论
NSString * const URL_deletePublishCommentsById = @"/api/publishs/deletePublishCommentsById";
// 删除动态评论回复
NSString * const URL_deletePublishCommentsReplyById = @"/api/publishs/deletePublishCommentsReplyById";
// 删除世界动态(个人作品)
NSString * const URL_deletePublishsById = @"/api/publishs/deletePublishsById";
// 分页查询动态列表(我的作品列表)
NSString * const URL_getMyPublishList = @"/api/publishs/getMyPublishList";
// 查询世界动态详情
NSString * const URL_getPublishsById = @"/api/publishs/getPublishsById";
// 分页查询世界动态列表
NSString * const URL_getPublishsList = @"/api/publishs/getPublishsList";
// 世界动态点赞/取消点赞(收藏)
NSString * const URL_postPublishsLike = @"/api/publishs/postPublishsLike";
// 分页查询某个动态的评论
NSString * const URL_queryPublishComments = @"/api/publishs/queryPublishComments";


#pragma mark - VIP
// VIP信息
NSString * const URL_VIPInfo = @"/api/vip/VIPInfo";
// VIP充值列表
NSString * const URL_VIPRechargeList = @"/api/vip/VIPRechargeList";
// VIP充值支付宝
NSString * const URL_recharge = @"/api/vip/recharge";
// VIP充值微信
NSString * const URL_rechargeWx = @"/api/vip/rechargeWx";


#pragma mark - 个人群公告管理
// 新增个人群公告
NSString * const URL_addPersonalClusterNotice = @"/api/personalClusterNotice/addPersonalClusterNotice";
// 删除个人群公告
NSString * const URL_deletePersonalClusterNoticeById = @"/api/personalClusterNotice/deletePersonalClusterNoticeById";
// 查询个人群公告
NSString * const URL_getPersonalClusterNotice = @"/api/personalClusterNotice/getPersonalClusterNotice";
// 修改个人群公告
NSString * const URL_updatePersonalClusterNotice = @"/api/personalClusterNotice/updatePersonalClusterNotice";


#pragma mark - 个人群成员管理
// 申请入群，需要审核的接口
NSString * const URL_applyCluster = @"/api/personalClusterMember/applyCluster";
// 群成员列表
NSString * const URL_clusterMembers = @"/api/personalClusterMember/clusterMembers";
// 删除群成员/退群
NSString * const URL_deletePersonalClusterMemberById = @"/api/personalClusterMember/deletePersonalClusterMemberById";
// 查询个人群成员
NSString * const URL_getPersonalClusterMemberById = @"/api/personalClusterMember/getPersonalClusterMemberById";
// 用户可添加到个人群的好友列表
NSString * const URL_getUserPerClusterAddList = @"/api/personalClusterMember/getUserPerClusterAddList";
// 拉人入群,直接加入群的接口
NSString * const URL_inviteCluster = @"/api/personalClusterMember/inviteCluster";
// 设置管理员
NSString * const URL_updatePersonalClusterMember = @"/api/personalClusterMember/updatePersonalClusterMember";


#pragma mark - 个人群管理
// 根据群id查询群信息
NSString * const URL_IMClusterInfo = @"/api/personalCluster/IMClusterInfo";
// 新增个人群
NSString * const URL_addPersonalCluster = @"/api/personalCluster/addPersonalCluster";
// 删除个人群
NSString * const URL_deletePersonalClusterById = @"/api/personalCluster/deletePersonalClusterById";
// 查询个人群
NSString * const URL_getPersonalClusterById = @"/api/personalCluster/getPersonalClusterById";
// 查询用户在群信息
NSString * const URL_getUserClusterVo = @"/api/personalCluster/getUserClusterVo";
// 我加入的群组
NSString * const URL_myJoinedCluster = @"/api/personalCluster/myJoinedCluster";
// 搜索个人群
NSString * const URL_searchPersonalCluster = @"/api/personalCluster/searchPersonalCluster";
// 搜索群后查询群信息
NSString * const URL_searchToClusterInfo = @"/api/personalCluster/searchToClusterInfo";
// 修改个人群
NSString * const URL_updatePersonalCluster = @"/api/personalCluster/updatePersonalCluster";


#pragma mark - 代理管理
// 申请成为代理
NSString * const URL_addAgent = @"/api/agent/addAgent";
// 添加下属
NSString * const URL_addSubordinate = @"/api/agent/addSubordinate";
// 添加下属列表
NSString * const URL_addSubordinateList = @"/api/agent/addSubordinateList";
// 代理信息,根据status判断-1：拒绝，0：待审核，1：正常，2：取消代理
NSString * const URL_agentInfo = @"/api/agent/agentInfo";
// 代理金额明细
NSString * const URL_agentProfitDetail = @"/api/agent/agentProfitDetail";
// 解除代理
NSString * const URL_removeSubordinate = @"/api/agent/removeSubordinate";
// 下属列表
NSString * const URL_subordinateList = @"/api/agent/subordinateList";


#pragma mark - 关注/好友
// 关注
NSString * const URL_addFollowsMapping = @"/api/followsMapping/addFollowsMapping";
// 取消关注
NSString * const URL_deleteFollowsMapping = @"/api/followsMapping/deleteFollowsMapping";
// 粉丝列表
NSString * const URL_fansList = @"/api/followsMapping/fansList";
// 关注列表
NSString * const URL_followList = @"/api/followsMapping/followList";
// 好友列表
NSString * const URL_friendsList = @"/api/followsMapping/friendsList";
// 是否好友
NSString * const URL_isFriend = @"/api/followsMapping/isFriend";


#pragma mark - 地区城市
// 获得城市信息
NSString * const URL_getCity = @"/api/area/getCity";
// 获得省份列表
NSString * const URL_getProvince = @"/api/area/getProvince";


#pragma mark - 实时匹配
// 取消实时匹配
NSString * const URL_cancelMatch = @"/api/match/cancelMatch";
// 根据类型实时匹配
NSString * const URL_matchByType = @"/api/match/matchByType";
// 解锁当次需要消耗的金币
NSString * const URL_unlockIng = @"/api/match/unlockIng";


#pragma mark - 意见反馈
// 新增用户意见反馈
NSString * const URL_addUserFeedback = @"/api/userFeedback/addUserFeedback";


#pragma mark - 搜索
// 分页条件查询房间
NSString * const URL_getSearchRoomPages = @"/api/serach/getSearchRoomPages";
// 分页条件查询用户
NSString * const URL_getSearchUserPages = @"/api/serach/getSearchUserPages";
// 分页条件搜索个人群
NSString * const URL_searchPersonalClusters = @"/api/serach/searchPersonalCluster";


#pragma mark - 收藏--动态（作品）、课程、房间
// 新增(取消)收藏---课程、房间
NSString * const URL_addCollect = @"/api/collect/addCollect";
// 分页查询课程收藏列表
NSString * const URL_getCourseCollectList = @"/api/collect/getCourseCollectList";
// 分页查询动态收藏列表
NSString * const URL_getPublishCollectList = @"/api/collect/getPublishCollectList";


#pragma mark - 教练管理
// 教练认证
NSString * const URL_addCoach = @"/api/coach/addCoach";
// 教练详情
NSString * const URL_coachDetail = @"/api/coach/coachDetail";
// 教练认证前验证,有返回为已认证教练，无返回为可进行认证教练
NSString * const URL_coachVerify = @"/api/coach/coachVerify";
// 搜索教练
NSString * const URL_searchCoach = @"/api/coach/searchCoach";
// 修改教练详情
NSString * const URL_updateCoach = @"/api/coach/updateCoach";


#pragma mark - 文件管理
// base64上传文件
NSString * const URL_base64 = @"/api/storage/base64";
// 根据文件名删除文件
NSString * const URL_deleteByName = @"/api/storage/deleteByName";
// 根据文件id查询文件
NSString * const URL_downloadById = @"/api/storage/downloadById";
// 根据文件名查询文件
NSString * const URL_downloadByName = @"/api/storage/downloadByName";
// 上传文件(支持多文件)
NSString * const URL_upload = @"/api/storage/upload";


#pragma mark - 查询标签
// 根据类型查询标签：0日常爱好  1教学内容   2适用人群   3语种
NSString * const URL_getLabelByType = @"/api/label/getLabelByType";


#pragma mark - 用户管理
// 拉黑/取消拉黑用户
NSString * const URL_blackUser = @"/api/user/blackUser";
// 更换手机
NSString * const URL_changePhone = @"/api/user/changePhone";
// 忘记密码
NSString * const URL_forgetPwd = @"/api/user/forgetPwd";
// 我的黑名单
NSString * const URL_getMyBlackUser = @"/api/user/getMyBlackUser";
// 个人资料
NSString * const URL_getUserById = @"/api/user/getUserById";
// IM关注
NSString * const URL_imFocus = @"/api/user/imFocus";
// 个人资料进语音/视频/聊天判断
NSString * const URL_interactCount = @"/api/user/interactCount";
// 邀请码邀请
NSString * const URL_inviteUser = @"/api/user/inviteUser";
// 我的 页面
NSString * const URL_myPage = @"/api/user/myPage";
// 匹配剩余次数
NSString * const URL_remainMatchCount = @"/api/user/remainMatchCount";
// 举报（投诉）用户、房间、课程、动态、音乐、教练
NSString * const URL_report = @"/api/user/report";
// 修改密码
NSString * const URL_updPwd = @"/api/user/updPwd";
// 用户登陆成功后完善资料或用户资料修改时调用此接口
NSString * const URL_updUser = @"/api/user/updUser";
// 解绑微信
NSString * const URL_romveUserWx = @"/api/user/romveUserWx";
// 更新/绑定用户的微信
NSString * const URL_upUserWx = @"/api/user/upUserWx";

#pragma mark - 用户聊天设置
// 查询用户聊天信息设置
NSString * const URL_getUserChatSet = @"/api/userChatSet/getUserChatSet";
// 修改用户聊天设置
NSString * const URL_updateUserChatSet = @"/api/userChatSet/updateUserChatSet";

#pragma mark - 短信管理
// 获取验证码
NSString * const URL_getVerifyCode = @"/api/sms/getVerifyCode";
// 验证码校验
NSString * const URL_verifyCode = @"/api/sms/verifyCode";


#pragma mark - 系统消息
// 群组消息助手
NSString * const URL_cluster = @"/api/systemMessage/cluster";
// 评论消息
NSString * const URL_commentMessage = @"/api/systemMessage/commentMessage";
// 课程消息
NSString * const URL_coursesMessage = @"/api/systemMessage/coursesMessage";
// 处理群组入群通知
NSString * const URL_dealClusterAccess = @"/api/systemMessage/dealClusterAccess";
// 系统消息主界面
NSString * const URL_index = @"/api/systemMessage/index";
// 官方消息
NSString * const URL_officialMessage = @"/api/systemMessage/officialMessage";
// 消息-房间
NSString * const URL_roomMessage = @"/api/systemMessage/roomMessage";
// 房间在线用户列表
NSString * const URL_roomWealthToDay = @"/api/systemMessage/roomWealthToDay";
// 钱包消息
NSString * const URL_walletMessage = @"/api/systemMessage/walletMessage";


#pragma mark - 职业信息
// 获取行业信息
NSString * const URL_getIndustry = @"/api/industry/getIndustry";
// 获取行业信息
NSString * const URL_getProfession = @"/api/industry/getProfession";


#pragma mark - 设置个人禁言
// 新增个人群禁言
NSString * const URL_addPersonalClusterSilence = @"/api/personalClusterSilence/addPersonalClusterSilence";
// 解除个人群禁言
NSString * const URL_deletePersonalClusterSilenceById = @"/api/personalClusterSilence/deletePersonalClusterSilenceById";
// 查询个人群禁言
NSString * const URL_getPersonalClusterSilence = @"/api/personalClusterSilence/getPersonalClusterSilence";


#pragma mark - 课程评价管理
// 新增课程评价
NSString * const URL_addEvaluation = @"/api/evaluation/addEvaluation";
// 删除评价
NSString * const URL_delEvaluationById = @"/api/evaluation/delEvaluationById";
// 查询课程评价
NSString * const URL_getEvaluationById = @"/api/evaluation/getEvaluationById";


#pragma mark - 邀请码邀请记录
// 邀请记录
NSString * const URL_inviteLog = @"/api/inviteLog/inviteLog";


#pragma mark - 钱包管理
// 发起提现申请（支付宝）
NSString * const URL_alipayAddWalletWithdraw = @"/api/alipay/addWalletWithdraw";
// 发起提现申请（微信）
NSString * const URL_wxpayAddWalletWithdraw = @"/api/wxpay/addWalletWithdraw";

// 获得交易详情
NSString * const URL_getDealDetail = @"/api/wallet/getDealDetail";
// 钱包余额
NSString * const URL_getWalletByUserId = @"/api/wallet/getWalletByUserId";
// 查询钱包交易记录
NSString * const URL_getWalletLogById = @"/api/wallet/getWalletLogById";
// 充值金币
NSString * const URL_rechargeGold = @"/api/wallet/rechargeGold";
// 充值金币-支付后回调
NSString * const URL_rechargeGoldPay = @"/api/wallet/rechargeGoldPay";
// 充值金币-微信支付后回调
NSString * const URL_rechargeGoldWxPay = @"/api/wallet/rechargeGoldWxPay";
// 充值页面
NSString * const URL_rechargePage = @"/api/wallet/rechargePage";



#pragma mark - 频道歌曲
// 上传歌曲
NSString * const URL_Music = @"/api/musicChannel/Music";
// 新增频道歌曲，单个
NSString * const URL_addMusicChannel = @"/api/musicChannel/addMusicChannel";
// 新增频道歌曲，多个
NSString * const URL_addMusicChannels = @"/api/musicChannel/addMusicChannels";
// 用户下麦，删除频道所有歌曲
NSString * const URL_deleteMusicChannel = @"/api/musicChannel/deleteMusicChannel";
// 删除频道歌曲
NSString * const URL_deleteMusicChannelById = @"/api/musicChannel/deleteMusicChannelById";
// 分页查询频道歌曲列表
NSString * const URL_getMusicChannelByRoomId = @"/api/musicChannel/getMusicChannelByRoomId";


#pragma mark - 课程管理
// 新增课程
NSString * const URL_addCourse = @"/api/course/addCourse";
// 同意退款
NSString * const URL_agreeRefund = @"/api/course/agreeRefund";
// 支付宝购买课程
NSString * const URL_buyCourse = @"/api/course/buyCourse";
// 金币购买课程
NSString * const URL_buyCourseByGold = @"/api/course/buyCourseByGold";
// 微信购买课程
NSString * const URL_buyCourseWx = @"/api/course/buyCourseWx";
// 取消退款
NSString * const URL_cancelRefund = @"/api/course/cancelRefund";
// 课程上下架
NSString * const URL_changeStatus = @"/api/course/changeStatus";
// 课程编辑列表
NSString * const URL_coachCourse = @"/api/course/coachCourse";
// 课程结束
NSString * const URL_courseFinish = @"/api/course/courseFinish";
// 课程订单详情
NSString * const URL_courseOrder = @"/api/course/courseOrder";
// 课程订单退款
NSString * const URL_courseRefund = @"/api/course/courseRefund";
// 课程开始
NSString * const URL_courseStart = @"/api/course/courseStart";
// 课程自动取消天数
NSString * const URL_daysToCancel = @"/api/course/daysToCancel";
// 删除课程
NSString * const URL_deleteCourseById = @"/api/course/deleteCourseById";
// 金币购买课程打折
NSString * const URL_discountByGold = @"/api/course/discountByGold";
// 课程详情
NSString * const URL_getCourseById = @"/api/course/getCourseById";
// 我的课程(用户)
NSString * const URL_myCourse = @"/api/course/myCourse";
// 课程管理
NSString * const URL_myCourseOrder = @"/api/course/myCourseOrder";
// 修改课程
NSString * const URL_updateCourse = @"/api/course/updateCourse";

// 微信购买课程
NSString * const URL_wxBuyCourse = @"/api/wxpay/buyCourse";
// 支付宝购买课程
NSString * const URL_aliBuyCourse = @"/api/alipay/buyCourse";

NSString * const URL_geWithdrawFee = @"/api/wallet/geWithdrawFee";


#pragma mark ---------------IM ----------
/**总未读数*/
NSString * const URL_IM_TotalUnRead = @"/api/messageHandler/getUnreadNum";


#pragma 房间管理
// 房间禁言列表
NSString * const URL_getRoomForbiddenList = @"/api/room/getRoomForbiddenList";
