//
//  BaseNetworkModel.h
//  VoiceLive
//
//  Created by mac on 2020/10/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseNetworkModel : NSObject

/**数据内容*/
@property (nonatomic) id data;
/**信息*/
@property (nonatomic, copy) NSString *msg;
/**状态码*/
@property (nonatomic, assign) NSInteger status;

@end

NS_ASSUME_NONNULL_END
