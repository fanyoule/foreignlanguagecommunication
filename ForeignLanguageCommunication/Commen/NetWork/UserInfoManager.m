//
//  UserInfoManager.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//
#define kUserDefaultKey @"UserModel"
#import "UserInfoManager.h"

@implementation UserInfoManager
static UserInfoManager *manager = nil;
+ (instancetype)shared{
    
    
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!manager) {
            manager = [[UserInfoManager alloc] init];
        }
    });
    return manager;
}
- (id)copyWithZone:(NSZone *)zone {
    
    return manager;
}
- (id)mutableCopyWithZone:(NSZone *)zone {
    return manager;
}

+ (void)saveInfoWithUserModel:(UserModel *)model{
    if (model) {
        
        NSData *data = [model mj_JSONData];
        [[NSUserDefaults standardUserDefaults] setValue:data forKey:kUserDefaultKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
}

+ (void)deleteAllUserData{
//    NSData *userData = [[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultKey];
//    if (!userData) {
//        return;
//    }
//
//    NSString *string = [userData mj_JSONObject];
//    if (string) {
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserDefaultKey];
//    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserDefaultKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"key === %@",kUserDefaultKey);
}

- (UserInfoModel * _Nullable)getUserDetailInfo {
    NSData *userData = [[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultKey];
    if (!userData) {
        return nil;
    }
    
    NSDictionary *dic = [userData mj_JSONObject];
    if (!dic) {
        return nil;
    }
    UserModel *model = [UserModel mj_objectWithKeyValues:dic];
    if (!model) {
        return nil;
    }
    
    UserInfoModel *userInfoM = [UserInfoModel mj_objectWithKeyValues:model.user];
    
    return userInfoM;
    
}

- (NSInteger)getUserID{
    UserInfoModel *userM = [self getUserDetailInfo];
    
    if (userM) {
        return userM.ID;
    }
    return 0;
}

-(NSString *)inviteCode{
    UserInfoModel *userM = [self getUserDetailInfo];
    
    if (userM) {
        return userM.inviteCode;
    }
    return 0;
}


- (NSString *)getUserNickName {
    UserInfoModel *userM = [self getUserDetailInfo];
    if (userM) {
        return userM.nickname;
    }
    return nil;
}

- (NSInteger)getUserHeight{
    UserInfoModel *userM = [self getUserDetailInfo];
    
    if (userM) {
        return userM.height;
    }
    return 0;
}
- (NSInteger)getUserWeight{
    UserInfoModel *userM = [self getUserDetailInfo];
    
    if (userM) {
        return userM.weight;
    }
    return 0;
}

- (NSInteger)sysId{
    UserInfoModel *userM = [self getUserDetailInfo];
    
    if (userM) {
        return userM.sysId;
    }
    return 0;
}

- (NSString *)phone{
    UserInfoModel *userM = [self getUserDetailInfo];
    
    if (userM) {
        return userM.phone;
    }
    return 0;
}

- (NSString *)wx{
    UserInfoModel *userM = [self getUserDetailInfo];
    
    if (userM) {
        return userM.wx;
    }
    return 0;
}

- (void)updateUserInfo:(UserInfoModel *)userInfoModel{
    if (userInfoModel) {
        NSData *userData = [[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultKey];
        NSDictionary *dic = [userData mj_JSONObject];
        UserModel *model = [UserModel mj_objectWithKeyValues:dic];
        model.user = [userInfoModel mj_JSONObject];
        
        
        NSData *data = [model mj_JSONData];
        [[NSUserDefaults standardUserDefaults] setValue:data forKey:kUserDefaultKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSString * _Nullable)getUserAvatar{
    UserInfoModel *userM = [self getUserDetailInfo];
    
    if (userM) {
        return userM.avatar;
    }
    return nil;
}

- (BOOL)isLogin{
    BOOL isLogin = NO;
    
    UserInfoModel *userM = [self getUserDetailInfo];
    if (userM) {
        isLogin = YES;
    }
    return isLogin;
}
@end
