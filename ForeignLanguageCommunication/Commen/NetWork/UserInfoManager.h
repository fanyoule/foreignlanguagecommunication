//
//  UserInfoManager.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoManager : NSObject<NSCopying,NSMutableCopying>
+ (instancetype)shared;
/**保存所有用户信息 包括有token信息*/
+ (void)saveInfoWithUserModel:(UserModel *)model;

+ (void)deleteAllUserData;

/**获取用户信息  用户详细信息*/
- (UserInfoModel * _Nullable)getUserDetailInfo;
/**获取用户ID*/
- (NSInteger)getUserID;
/**获取用户昵称*/
- (NSString * _Nullable)getUserNickName;
/**获取用户身高*/
- (NSInteger)getUserHeight;
/**获取体重*/
- (NSInteger)getUserWeight;
/**获取头像*/
- (NSString * _Nullable)getUserAvatar;

/** 邀请码*/
- (NSString *)inviteCode;
/** 系统id*/
- (NSInteger)sysId;
/** 手机号*/
- (NSString *)phone;
/** 微信*/
- (NSString *)wx;


- (void)updateUserInfo:(UserInfoModel *)userInfoModel;

- (BOOL)isLogin;



@end

NS_ASSUME_NONNULL_END
