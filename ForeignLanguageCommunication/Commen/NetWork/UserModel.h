//
//  UserModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserModel : NSObject

@property (nonatomic, copy) NSString *expireTime;
@property (nonatomic) BOOL firstLogin;
@property (nonatomic, copy) NSString *ipaddr;
@property (nonatomic, copy) NSString *loginLocation;
@property (nonatomic, copy) NSString *loginTime;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, strong) NSDictionary *user;
@property (nonatomic, copy) NSString *userName;
/*
 data =     {
        expireTime = 1610708478163;
        firstLogin = 0;
        ipaddr = "114.235.19.23";
        loginLocation = "XX XX";
        loginTime = 1610697678163;
        token = "056bfb31-36e1-4dfc-9300-1c426e997467";
        user =         {
            area = "\U4e91\U9f99\U533a";
            avatar = "http://pic.sxsyingyu.com/4bhsetsvkv8ykycp0s80.jpeg";
            birthday = "1980-01-01";
            city = "\U5f90\U5dde\U5e02";
            createDate = "2020-12-30";
            height = 165;
            hobby = "";
            id = 35;
            introduce = gogogogogo;
            inviteCode = 4x2oW7;
            loginLat = "34.252228";
            loginLot = "117.242335";
            nickname = "\U79cb\U4e91";
            occupation = 26;
            phone = 18952343542;
            province = "\U6c5f\U82cf\U7701";
            realName = "\U891a\U79cb\U4e91\U6559\U7ec3";
            safemodeStatus = 0;
            sex = 0;
            status = 1;
            sysId = 24896301;
            weight = 60;
        };
        username = 24896301;
    };

 */

@end

NS_ASSUME_NONNULL_END
