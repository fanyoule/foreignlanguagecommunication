//
//  UserInfoModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoModel : NSObject
/**常驻城市*/
@property (nonatomic, copy) NSString *area;
/**头像*/
@property (nonatomic, copy) NSString *avatar;
/**生日*/
@property (nonatomic, copy) NSString *birthday;
/**城市*/
@property (nonatomic, copy) NSString *city;
/**创建时间 2020-12-30*/
@property (nonatomic, copy) NSString *createDate;
/**身高*/
@property (nonatomic) NSInteger height;
/**爱好*/
@property (nonatomic, copy) NSString *hobby;
/**ID*/
@property (nonatomic) NSInteger ID;
/**简介*/
@property (nonatomic, copy) NSString *introduce;
/**邀请码*/
@property (nonatomic, copy) NSString *inviteCode;
/**纬度*/
@property (nonatomic, copy) NSString *loginLat;
/**经度*/
@property (nonatomic, copy) NSString *loginLot;
/**昵称*/
@property (nonatomic, copy) NSString *nickname;
/**职业*/
@property (nonatomic, copy) NSString *occupation;
/**手机*/
@property (nonatomic, copy) NSString *phone;
/**省*/
@property (nonatomic, copy) NSString *province;
/**真实姓名*/
@property (nonatomic, copy) NSString *realName;
/**安全模式*/
@property (nonatomic) NSInteger safemodeStatus;
/**性别 0 1*/
@property (nonatomic) NSInteger sex;
/**状态*/
@property (nonatomic) NSInteger status;
/**系统ID*/
@property (nonatomic) NSInteger sysId;
/**体重*/
@property (nonatomic) NSInteger weight;
/** 微信*/
@property (nonatomic, strong)NSString *wx;
/*
 user =         {
     area = "\U4e91\U9f99\U533a";
     avatar = "http://pic.sxsyingyu.com/4bhsetsvkv8ykycp0s80.jpeg";
     birthday = "1980-01-01";
     city = "\U5f90\U5dde\U5e02";
     createDate = "2020-12-30";
     height = 165;
     hobby = "";
     id = 35;
     introduce = gogogogogo;
     inviteCode = 4x2oW7;
     loginLat = "34.252228";
     loginLot = "117.242335";
     nickname = "\U79cb\U4e91";
     occupation = 26;
     phone = 18952343542;
     province = "\U6c5f\U82cf\U7701";
     realName = "\U891a\U79cb\U4e91\U6559\U7ec3";
     safemodeStatus = 0;
     sex = 0;
     status = 1;
     sysId = 24896301;
     weight = 60;
 };
 */
@end

NS_ASSUME_NONNULL_END
