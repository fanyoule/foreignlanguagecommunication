//
//  Macro.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/16.
//

#ifndef Macro_h
#define Macro_h

#import "ApplePayViewController.h"
#import <AlipaySDK/AlipaySDK.h>

//沙盒测试环境验证
#define SANDBOX @"https://sandbox.itunes.apple.com/verifyReceipt"
//正式环境验证
#define AppStore @"https://buy.itunes.apple.com/verifyReceipt"

#define IMPostHost @"https://47.104.23.218:20443"
#define IMhost     @"47.104.23.218"
#define IMport     @"45000"

#define kWeakSelf(type)    __weak typeof(type) weak##type = type; // weak
#define kStrongSelf(type)  __strong typeof(type) strong##type = weak##type; // strong

#define SetCache(value, key)    [[NSUserDefaults standardUserDefaults] setBool:value forKey:key]
#define GetCache(key)           [[NSUserDefaults standardUserDefaults] boolForKey:key]
#define ISNoDisturbingKey(value) [NSString stringWithFormat:@"isNoDisturbing_%@", LongToString(value)] // 免打扰
#define ISTopKey(value)          [NSString stringWithFormat:@"isTop_%@", LongToString(value)]          // 置顶

//设备系统相关
#define kSystemVersion      [[[UIDevice currentDevice] systemVersion] floatValue]
#define iOS10               (kSystemVersion >= 10.0)
#define iOS11               @available(iOS 11.0, *)

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136),[[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)) : NO)

#define iPhone6plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)

#define iPhoneX ([[UIApplication sharedApplication] statusBarFrame].size.height>20?YES:NO)


#define LongToString(value) [NSString stringWithFormat:@"%ld", value]
//常用高度
//状态栏高度
#define  SBarHeight       (DX_IS_IPhoneX_All ? 44.f : 20.f)
//状态栏+导航栏高度
#define  SNavBarHeight  (DX_IS_IPhoneX_All ? 88.f : 64.f)
//tabbar高度
#define  TabbarHeight         (DX_IS_IPhoneX_All ? (49.f+34.f) : 49.f)
//底部距离安全区域的距离
#define  TabbarSafeMargin         (DX_IS_IPhoneX_All ? 34.f : 0.f)
//应用size
#define  APPSIZE   [[UIScreen mainScreen] bounds].size
/// 屏幕宽度，会根据横竖屏的变化而变化
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)

/// 屏幕高度，会根据横竖屏的变化而变化
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREENAPPLYSPACE(x) SCREEN_WIDTH / 375.0 * (x)
#define SCREENAPPLYHEIGHT(x) SCREEN_HEIGHT / 667.0 * (x)

#define YL_kScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define YL_kScreenHeight ([UIScreen mainScreen].bounds.size.height)

#define kBackgroundColor        RGBA(243, 243, 243, 1)
#define kColor(value)           [UIColor colorWithHexString:value alpha:1]
#define kTHEMECOLOR             [UIColor colorWithHexString:@"#3478F5" alpha:1]
#define kPlayVideoNotification  @"kPlayVideoNotification"
#define kRefreshComment         @"kRefreshComment"
#define kRefreshWorldList       @"kRefreshWorldList"

//判断字符串是否为空
#define IS_VALID_STRING(string) !((![string isKindOfClass:[NSString class]])||[string isEqualToString:@""] || (string == nil) || [string isEqualToString:@"<null>"]|| [string isEqualToString:@"(null)"]|| [string isEqualToString:@"null"]|| [string isEqualToString:@"nil"] || [string isKindOfClass:[NSNull class]]||[[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0)


//判断数组是否为空
#define IS_VALID_ARRAY(array) (array && [array isKindOfClass:[NSArray class]] && [array count])
//屏幕适配

// 判断 iPad
#define DX_UI_IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

// 判断iPhone X
#define DX_Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

//判断iPHoneXr | 11
#define DX_Is_iPhoneXR ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !DX_UI_IS_IPAD : NO)

//判断iPHoneXs | 11Pro
#define DX_Is_iPhoneXS ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !DX_UI_IS_IPAD : NO)

//判断iPhoneXs Max | 11ProMax
#define DX_Is_iPhoneXS_MAX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !DX_UI_IS_IPAD : NO)

//判断iPhone12_Mini
#define DX_Is_iPhone12_Mini ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1080, 2340), [[UIScreen mainScreen] currentMode].size) && !DX_UI_IS_IPAD : NO)

//判断iPhone12 | 12Pro
#define DX_Is_iPhone12 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1170, 2532), [[UIScreen mainScreen] currentMode].size) && !DX_UI_IS_IPAD : NO)

//判断iPhone12 Pro Max
#define DX_Is_iPhone12_ProMax ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1284, 2778), [[UIScreen mainScreen] currentMode].size) && !DX_UI_IS_IPAD : NO)

//x系列
#define DX_IS_IPhoneX_All (DX_Is_iPhoneX || DX_Is_iPhoneXR || DX_Is_iPhoneXS || DX_Is_iPhoneXS_MAX || DX_Is_iPhone12_Mini || DX_Is_iPhone12 || DX_Is_iPhone12_ProMax)

//是否为iphone5
#define IS_IPHONE_5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//是否为iphone6
#define IS_IPHONE_6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)) : NO)


//是否为iphone6Plus
#define IS_IPHONE_6PLUS  ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)


//判断iPhoneX，Xs（iPhoneX，iPhoneXs）
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
//判断iPhoneXr
#define IS_IPHONE_Xr1 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) : NO)
//判断iPhoneXr
#define IS_IPHONE_Xr2 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1624), [[UIScreen mainScreen] currentMode].size) : NO)
//判断iPhoneXsMax
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)

//是否为iphone X
#define IS_IPHONE_X_XR_XMAX   (DX_Is_iPhoneX || DX_Is_iPhoneXR || DX_Is_iPhoneXS || DX_Is_iPhoneXS_MAX || DX_Is_iPhone12_Mini || DX_Is_iPhone12 || DX_Is_iPhone12_ProMax)
//#define IS_IPHONE_X_XR_XMAX   (IS_IPHONE_X || IS_IPHONE_Xr1 || IS_IPHONE_Xr2 || IS_IPHONE_Xs_Max)
//判断系统
#define  iosSystemVersion  [[[UIDevice currentDevice] systemVersion] floatValue]
//判断如果是iPhone X
#define IOS11_OR_LATER_SPACE(par)({ float space = 0.0;if (@available(iOS 11.0, *)) space = par;(space);})
#define IOS11_OR_LATER_SPACE88(par)({ float space = 64;if (IS_IPHONE_X_XR_XMAX) space = par;(space);})
#define IOS11_OR_LATER_SPACE20(par)({ float space = 20;if (IS_IPHONE_X_XR_XMAX) space = par;(space);})
#define IOS11_OR_LATER_SPACE0(par)({ float space = 0;if (IS_IPHONE_X_XR_XMAX) space = par;(space);})


#define JF_KEY_WINDOW    [UIApplication sharedApplication].keyWindow
#define TOP_SPACE88or64     IOS11_OR_LATER_SPACE88(88)
#define TOP_SPACE20or44     IOS11_OR_LATER_SPACE20(44)
#define TOP_SPACE44or0     IOS11_OR_LATER_SPACE0(44)
#define TOP_SPACE24or0     IOS11_OR_LATER_SPACE0(24)
#define BOTTOM_SPACE34or0  IOS11_OR_LATER_SPACE0(34)

// UIScreen height.
#define ScreenHeight        [UIScreen mainScreen].bounds.size.height
#define KSW                 [UIScreen mainScreen].bounds.size.width
#define KSH                 [UIScreen mainScreen].bounds.size.height-SNavBarHeight

#define kScale_Width        (KSW / 375)
#define kScaleSize(size)    (size)*kScale_Width

//颜色值
#define ColorRGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

#define ColorRGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(r)/255.0 blue:(r)/255.0 alpha:a]
#define rgba(r, g, b, a) [UIColor colorWithRed:r / 255.0 green:g / 255.0 blue:b / 255.0 alpha:a]
#define RGBA(r,g,b,a) [UIColor colorWithRed:r / 255.0 green:g / 255.0 blue:b / 255.0 alpha:a]
#define CustomColor(color)  [UIColor colorWithHexString:color alpha:1]
#define appColor   RGBA(116, 92, 240, 1)

#define CellLineColor   RGBA(243, 243, 243, 1)

#define kTabTextColor       [UIColor colorWithHexString:@"#3478F5" alpha:1]
#define kC99                [UIColor colorWithHexString:@"#999999" alpha:1]
//字体 字号与字重
#define kFont(size)              [UIFont systemFontOfSize:(size)]
#define kFont_Bold(size)         [UIFont systemFontOfSize:(size) weight:UIFontWeightBold]
#define kFont_Medium(size)       [UIFont systemFontOfSize:(size) weight:UIFontWeightMedium]
#define kFont_Regular(size)      [UIFont systemFontOfSize:(size) weight:UIFontWeightRegular]

#define kImage(string)          [UIImage imageNamed:string]
#define image(string)           [UIImage imageNamed:string]

#if DEBUG

#define NSLog(FORMAT, ...) fprintf(stderr,"[%s:%d行] %s\n",[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#else

#define NSLog(FORMAT, ...) nil;

#endif

#endif /* Macro_h */
