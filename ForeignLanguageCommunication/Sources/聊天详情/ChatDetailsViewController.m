//
//  ChatDetailsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//  聊天详情

#import "ChatDetailsViewController.h"
#import "SetUpTableViewCell.h"
@interface ChatDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;

@end

@implementation ChatDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"聊天详情";
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.navView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.navView);
        make.height.equalTo(@0.5);
    }];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    [self addHeadView];
    // Do any additional setup after loading the view.
}

- (void)addHeadView {
    
    UIView *headView = UIView.new;
    headView.backgroundColor = UIColor.whiteColor;
    headView.frame = CGRectMake(0, 0, KSW, kScaleSize(126));
    
    UIView *backView = UIView.new;
    backView.backgroundColor = RGBA(245, 245, 245, 1);
    [headView addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(headView);
        make.height.equalTo(@10);
    }];
    self.portraitImageV = UIImageView.new;
    self.portraitImageV.image = [UIImage imageNamed:@"测试头像"];
    self.portraitImageV.layer.cornerRadius = 50/2;
    self.portraitImageV.clipsToBounds = YES;
    self.portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
    [headView addSubview:self.portraitImageV];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(headView).offset(25);
        make.width.height.equalTo(@50);
    }];
    self.nameLabel = UILabel.new;
    self.nameLabel.text = @"嘻嘻嘻嘻嘻嘻嘻";
    self.nameLabel.textColor = RGBA(24, 24, 24, 1);
    self.nameLabel.font = kFont_Medium(14);
    self.nameLabel.textAlignment = 1;
    [headView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV.mas_bottom).offset(10);
        make.left.right.equalTo(self.portraitImageV);
    }];
    
    self.mainTableView.tableHeaderView = headView;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SetUpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetUpTableViewCell"];
        if (!cell) {
            cell = [[SetUpTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SetUpTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
    if (indexPath.row == 0) {
        cell.titleLabel.text = @"消息免打扰";
        cell.arrowImageV.hidden = YES;
        cell.contentLabel.hidden = YES;
        
        UISwitch *swi = [[UISwitch alloc]init];
        
        [cell addSubview:swi];
        [swi mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell);
            make.right.equalTo(cell).offset(-15);
        }];
        
    }else{
        cell.titleLabel.text = @"清空聊天记录";
        cell.arrowImageV.hidden = YES;
        cell.contentLabel.hidden = YES;
    }
        return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
