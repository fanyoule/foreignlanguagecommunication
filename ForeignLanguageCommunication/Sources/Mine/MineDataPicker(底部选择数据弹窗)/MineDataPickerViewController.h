//
//  MineDataPickerViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import <UIKit/UIKit.h>

typedef void(^clickFinishBlock)(NSArray *selectModelArray);

NS_ASSUME_NONNULL_BEGIN

@interface MineDataPickerViewController : UIViewController
/**选择一个还是多个  (YES:一个 ， NO:多个)*/
@property (nonatomic, assign) BOOL showSingle;
/**中间选择是否显示选中的颜色*/
@property (nonatomic) BOOL needShowColor;
/**点击了确定按钮*/
@property (nonatomic, copy) clickFinishBlock confirmBlock;

/** 判断   0：身高 ， 1：体重  2： 日常爱好 ，3： 语种 ， 4：教学内容 ，5适应人群 */
@property (nonatomic, assign)NSInteger isJudge;

@property (nonatomic,copy) NSString *titleString;

@property (nonatomic, strong) NSMutableArray *dataArray;

@end

NS_ASSUME_NONNULL_END
