//
//  MineDataPickerModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineDataPickerModel : NSObject
@property (nonatomic, assign)NSInteger ID;

@property (nonatomic, copy) NSString *name;



@property (nonatomic) BOOL selected;


@end

NS_ASSUME_NONNULL_END
