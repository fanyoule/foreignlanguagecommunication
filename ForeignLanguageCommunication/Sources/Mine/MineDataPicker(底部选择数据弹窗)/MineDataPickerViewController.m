//
//  MineDataPickerViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import "MineDataPickerViewController.h"

#import "MineDataPickerTableViewCell.h"
#import "MineDataPickerCollectionViewCell.h"

@interface MineDataPickerViewController ()<UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *dataTableview;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;


@property (nonatomic, strong) NSMutableArray *selectDataArray;

@property (nonatomic, strong) UICollectionView *bottomCollectionView;

@property (nonatomic, strong) MineDataPickerModel *selectModel;
@end

@implementation MineDataPickerViewController

- (void)setTitleString:(NSString *)titleString{
    _titleString = titleString;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.view.backgroundColor = UIColor.clearColor;

    
    self.bottomViewHeight.constant = 44+TabbarSafeMargin;

    self.contentSizeInPop = [UIScreen mainScreen].bounds.size;
    self.popController.containerView.backgroundColor = [UIColor clearColor];
    [self setupSubview];
    

//    UITapGestureRecognizer *tapDismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDismiss:)];
//    [self.view addGestureRecognizer:tapDismiss];
    self.titleLabel.text = self.titleString;
    [self getData];

   

}

- (void)setupSubview {
    self.dataTableview.delegate = self;
    self.dataTableview.dataSource = self;
    self.dataTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.bottomCollectionView.backgroundColor = UIColor.whiteColor;
    [self.bottomView addSubview:self.bottomCollectionView];
    [self.bottomCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.mas_offset(0);
            make.height.mas_equalTo(44);
    }];
}


- (UICollectionView *)bottomCollectionView {
    if (!_bottomCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
        layout.estimatedItemSize = CGSizeMake(60, 24);
        
        _bottomCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _bottomCollectionView.delegate = self;
        _bottomCollectionView.dataSource = self;
        [_bottomCollectionView registerClass:[MineDataPickerCollectionViewCell class] forCellWithReuseIdentifier:@"select"];
    }
    return _bottomCollectionView;
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (NSMutableArray *)selectDataArray {
    if (!_selectDataArray) {
        _selectDataArray = [NSMutableArray array];
    }
    return _selectDataArray;
}


- (void)tapToDismiss:(UIGestureRecognizer *)ges {
    CGPoint currentP = [ges locationInView:self.view];
    if (currentP.y >= self.view.height / 2) {
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


//MARK: -------top button click--------
- (IBAction)confirm:(id)sender {
    if (self.confirmBlock) {
        self.confirmBlock(self.selectDataArray);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
//MARK: --------tableview datasource----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MineDataPickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"data"];
    if (!cell) {
        cell = [[MineDataPickerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"data"];
    }
    cell.needShowColor = self.needShowColor;
    MineDataPickerModel *model = [[MineDataPickerModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MineDataPickerModel *model = self.dataArray[indexPath.row];
    
    
    //选中某一个
    if (self.showSingle) {
        if (self.selectModel == model) {
            return;
        }
        
        if (self.selectModel) {
            self.selectModel.selected = NO;
            
            [self.selectDataArray removeObject:self.selectModel];
        }
        self.selectModel = model;
        self.selectModel.selected = YES;
        [self.selectDataArray addObject:self.selectModel];
        
        [self.dataTableview reloadData];
        [self.bottomCollectionView reloadData];
        
    }else{
        //可以选择多个
        if (model.selected) {
            return;
        }
        
        if ([self.selectDataArray containsObject:model]) {
            
            return;
        }
        
        model.selected = YES;
        [self.selectDataArray addObject:model];
        [self.dataTableview reloadData];
        [self.bottomCollectionView reloadData];
    }
    
}

//MARK: collection view datasource----------------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.selectDataArray.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MineDataPickerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"select" forIndexPath:indexPath];
    
    cell.model = self.selectDataArray[indexPath.item];
    return cell;
}
//MARK: ------collection view delegate-------
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //点击取消选择
    MineDataPickerModel *model = self.selectDataArray[indexPath.item];
    model.selected = NO;
    [self.selectDataArray removeObject:model];
    
    [self.dataTableview reloadData];
    [self.bottomCollectionView reloadData];
}



- (void)getData {
    if (self.isJudge == 0) {
        NSArray *array = @[@"不显示",@"140cm",@"150cm",@"160cm",@"165cm",@"170cm",@"175cm",@"180cm",@"185cm",@"190cm"];
        
        for (int i = 0; i < array.count; i ++) {
            MineDataPickerModel *model = [[MineDataPickerModel alloc] init];
            model.name = array[i];
            [self.dataArray addObject:model];
        }
        
        
        
    }else if (self.isJudge == 1){
        NSArray *array = @[@"不显示",@"40kg",@"50kg",@"60kg",@"80kg"];
        for (int i = 0; i < array.count; i ++) {
            MineDataPickerModel *model = [[MineDataPickerModel alloc] init];
            model.name = array[i];
            [self.dataArray addObject:model];
        }
    }else if (self.isJudge == 2){
        // 日常爱好
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@(0) forKey:@"type"];
        [RequestManager getObtainUrl:@"/api/label/getLabelByType" incomingParameter:dic withSuccess:^(id  _Nullable response) {
            NSLog(@"日常爱好 === %@",response);
            if ([response[@"status"] integerValue] == 200) {
                NSArray *array = response[@"data"];
                for (int i = 0; i < array.count; i ++) {
                    MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:array[i]];
                    
                    [self.dataArray addObject:model];
                }
            }
            [self.dataTableview reloadData];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }else if (self.isJudge == 3){
        // 语种
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@(3) forKey:@"type"];
        [RequestManager getObtainUrl:@"/api/label/getLabelByType" incomingParameter:dic withSuccess:^(id  _Nullable response) {
            NSLog(@"日常爱好 === %@",response);
            if ([response[@"status"] integerValue] == 200) {
                NSArray *array = response[@"data"];
                for (int i = 0; i < array.count; i ++) {
                    MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:array[i]];
                    
                    [self.dataArray addObject:model];
                }
            }
            [self.dataTableview reloadData];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }else if (self.isJudge == 4){
        // 教学内容
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@(1) forKey:@"type"];
        [RequestManager getObtainUrl:@"/api/label/getLabelByType" incomingParameter:dic withSuccess:^(id  _Nullable response) {
            NSLog(@"日常爱好 === %@",response);
            if ([response[@"status"] integerValue] == 200) {
                NSArray *array = response[@"data"];
                for (int i = 0; i < array.count; i ++) {
                    MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:array[i]];
                    
                    [self.dataArray addObject:model];
                }
            }
            [self.dataTableview reloadData];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }else if (self.isJudge == 5){
        // 适应人群
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@(2) forKey:@"type"];
        [RequestManager getObtainUrl:@"/api/label/getLabelByType" incomingParameter:dic withSuccess:^(id  _Nullable response) {
            NSLog(@"日常爱好 === %@",response);
            if ([response[@"status"] integerValue] == 200) {
                NSArray *array = response[@"data"];
                for (int i = 0; i < array.count; i ++) {
                    MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:array[i]];
                    
                    [self.dataArray addObject:model];
                }
            }
            [self.dataTableview reloadData];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
    
    
    
    //获取数据
    
    //取到的数据 第一个默认选择？
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
