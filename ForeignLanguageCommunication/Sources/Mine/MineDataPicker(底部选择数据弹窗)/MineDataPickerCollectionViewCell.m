//
//  MineDataPickerCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import "MineDataPickerCollectionViewCell.h"

@interface MineDataPickerCollectionViewCell ()
@property (nonatomic, strong) UILabel *contentLabel;
@end

@implementation MineDataPickerCollectionViewCell



- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.textColor = CustomColor(@"#3478F5");
        _contentLabel.font = kFont_Medium(14);
        _contentLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _contentLabel;
}

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupSubview];
    }
    return self;
}

- (void)setupSubview {
    self.contentView.layer.cornerRadius = 3.0;
    self.contentView.layer.borderColor = CustomColor(@"#3478F5").CGColor;
    self.contentView.layer.borderWidth = 1;
    
    [self.contentView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
        make.height.mas_equalTo(24);
        
    }];
    [self.contentLabel layoutIfNeeded];
    CGFloat w = self.contentLabel.size.width + 10;
    [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(w));
    }];
    
}
- (void)setModel:(MineDataPickerModel *)model{
    _model = model;
    self.contentLabel.text = model.name;
}

@end
