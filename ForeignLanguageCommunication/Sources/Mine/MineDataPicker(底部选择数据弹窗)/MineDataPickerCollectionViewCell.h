//
//  MineDataPickerCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import <UIKit/UIKit.h>
#import "MineDataPickerModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MineDataPickerCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) MineDataPickerModel *model;

@end

NS_ASSUME_NONNULL_END
