//
//  MineDataPickerTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import "MineDataPickerTableViewCell.h"

@interface MineDataPickerTableViewCell ()

@end

@implementation MineDataPickerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubview];
    }
    return self;
}
- (void)setupSubview {
    [self.contentView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.contentView);
    }];
    
    
    UIView *line = [UIView new];
    line.backgroundColor = CustomColor(@"#DFDFDF");
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_offset(0);
            make.height.mas_equalTo(1);
    }];
}
- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.textColor = CustomColor(@"#181818");
        _contentLabel.font = kFont_Medium(14);
        
    }
    return _contentLabel;
}

- (void)setModel:(MineDataPickerModel *)model {
    _model = model;
    
    self.contentLabel.text = model.name;
    if (!self.needShowColor) {
        return;
    }
    
    
    if (model.selected) {
        self.contentLabel.textColor = CustomColor(@"#3478F5");
    }else{
        self.contentLabel.textColor = CustomColor(@"#181818");
    }
}

@end
