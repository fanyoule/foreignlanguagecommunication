//
//  MineDataPickerTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import <UIKit/UIKit.h>
#import "MineDataPickerModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MineDataPickerTableViewCell : UITableViewCell
@property (nonatomic,strong) MineDataPickerModel *model;
/**选中是否显示颜色*/
@property (nonatomic) BOOL needShowColor;

@property (nonatomic, strong) UILabel *contentLabel;
@end

NS_ASSUME_NONNULL_END
