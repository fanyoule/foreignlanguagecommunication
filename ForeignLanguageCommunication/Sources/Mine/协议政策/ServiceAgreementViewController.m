//
//  ServiceAgreementViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/25.
//  服务协议

#import "ServiceAgreementViewController.h"
#import <WebKit/WebKit.h>
@interface ServiceAgreementViewController ()<WKUIDelegate>

@end

@implementation ServiceAgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"用户使用协议";
    self.navView.backgroundColor = [UIColor whiteColor];
    // 初始化配置对象
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    // 默认是NO，这个值决定了用内嵌HTML5播放视频还是用本地的全屏控制
    configuration.allowsInlineMediaPlayback = YES;
    // 自动播放, 不需要用户采取任何手势开启播放
    if (@available(iOS 10.0, *)) {
        // WKAudiovisualMediaTypeNone 音视频的播放不需要用户手势触发, 即为自动播放
        configuration.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypeAudio;
    } else {
        configuration.mediaTypesRequiringUserActionForPlayback = NO;
    }
    WKWebView *webView = [[WKWebView alloc]initWithFrame:CGRectMake(0,self.navView.size.height, APPSIZE.width, APPSIZE.height-TabbarSafeMargin) configuration:configuration];
      webView.UIDelegate =self;
      webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
      webView.contentMode = UIViewContentModeRedraw;
      webView.opaque = YES;
      [self.view addSubview:webView];
      [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://pic.sxsyingyu.com/vsyt4h7ll47pziepd93y.html"]]];
    
    // Do any additional setup after loading the view.
}

@end
