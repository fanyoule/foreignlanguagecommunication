//
//  MyPageTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyPageTableViewCell : UITableViewCell
/**名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/**图片*/
@property (nonatomic, strong)UIImageView *markImageV;
@end

NS_ASSUME_NONNULL_END
