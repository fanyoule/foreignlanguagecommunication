//
//  MyPageTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/16.
//

#import "MyPageTableViewCell.h"

@interface MyPageTableViewCell ()


/**箭头*/
@property (nonatomic, strong)UIImageView *arrowImageV;
/**线*/
@property (nonatomic, strong)UIView *lineView;

@end
@implementation MyPageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.markImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.arrowImageV];
    [self.contentView addSubview:self.lineView];
    [self.markImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(25);
        make.width.height.equalTo(@21);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.markImageV.mas_right).offset(24);
        make.centerY.equalTo(self.contentView);
    }];
    [self.arrowImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel);
        make.right.equalTo(self.contentView).offset(-30);
        make.height.equalTo(@0.5);
        make.bottom.equalTo(self.contentView).offset(-0.5);
    }];
}

- (UIImageView *)markImageV{
    if (!_markImageV) {
        _markImageV = [[UIImageView alloc]init];
        _markImageV.image = [UIImage imageNamed:@"我的课程"];
    }
    return _markImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"我的课程";
        _nameLabel.textColor = RGBA(34, 34, 34, 1);
        _nameLabel.font = kFont_Medium(15);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UIImageView *)arrowImageV{
    if (!_arrowImageV) {
        _arrowImageV = [[UIImageView alloc]init];
        _arrowImageV.image = [UIImage imageNamed:@"我的-箭头"];
    }
    return _arrowImageV;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
