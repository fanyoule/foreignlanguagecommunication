//
//  MyVIPOpenTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/16.
//

#import "MyVIPOpenTableViewCell.h"
@interface MyVIPOpenTableViewCell ()
/**背景*/
@property (nonatomic, strong) UIImageView *backImageV;
/**会员图片*/
@property (nonatomic, strong) UIImageView *membersImageV;
/**文字*/
@property (nonatomic, strong) UILabel *privilegeLabel;
/**续费或者开通背景*/
@property (nonatomic, strong) UIView *openBackView;
@end
@implementation MyVIPOpenTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.backImageV];
    [self.contentView addSubview:self.membersImageV];
    [self.contentView addSubview:self.privilegeLabel];
    [self.contentView addSubview:self.openBackView];
    [self.contentView addSubview:self.openLabel];
    
    [self.backImageV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.contentView).offset(0);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@78);
        make.center.mas_equalTo(self.contentView);
//        make.bottom.equalTo(self.contentView).offset(0).priority(600);
    }];
    [self.membersImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.backImageV).offset(20);
    }];
    [self.openLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backImageV).offset(28);
        make.right.equalTo(self.backImageV).offset(-32);
    }];
    [self.openBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.openLabel).offset(-7.5);
        make.left.equalTo(self.openLabel).offset(-13.5);
        make.right.equalTo(self.openLabel).offset(13.5);
        make.bottom.equalTo(self.openLabel).offset(7.5);
    }];
    [self.privilegeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.openBackView);
        make.right.equalTo(self.openBackView.mas_left).offset(-10);
    }];
    
    
    
}
- (UIImageView *)backImageV{
    if (!_backImageV) {
        _backImageV = [[UIImageView alloc]init];
        _backImageV.image = [UIImage imageNamed:@"我的-会员背景"];
    }
    return _backImageV;
}
- (UIImageView *)membersImageV{
    if (!_membersImageV) {
        _membersImageV = [[UIImageView alloc]init];
        _membersImageV.image = [UIImage imageNamed:@"会员"];
    }
    return _membersImageV;
}
- (UILabel *)privilegeLabel{
    if (!_privilegeLabel) {
        _privilegeLabel = [[UILabel alloc]init];
        _privilegeLabel.text = @"开通立享多重特权";
        _privilegeLabel.textColor = RGBA(238, 203, 166, 1);
        _privilegeLabel.font = kFont_Medium(12);
        _privilegeLabel.textAlignment = 0;
    }
    return _privilegeLabel;
}
- (UIView *)openBackView{
    if (!_openBackView) {
        _openBackView = [[UIView alloc]init];
        _openBackView.backgroundColor = RGBA(238, 203, 166, 1);
        _openBackView.layer.cornerRadius  = 14;
        _openBackView.layer.masksToBounds = YES;
    }
    return _openBackView;
}
- (UILabel *)openLabel{
    if (!_openLabel) {
        _openLabel = [[UILabel alloc]init];
        _openLabel.text = @"立即开通";
        _openLabel.textColor = RGBA(79, 52, 38, 1);
        _openLabel.font = kFont_Bold(14);
        _openLabel.textAlignment = 1;
    }
    return _openLabel;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
