//
//  MyVIPOpenTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyVIPOpenTableViewCell : UITableViewCell

/**续费或者开通*/
@property (nonatomic, strong) UILabel *openLabel;

@end

NS_ASSUME_NONNULL_END
