//
//  MyFanViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "MyFanViewController.h"
#import "AttentionListTableViewCell.h"

#import "PayAttentionTheListModel.h"

#import "MyPersonalInformationVC.h"// 个人资料
#import "ThePersonalDataModel.h"
@interface MyFanViewController ()<UITableViewDelegate,UITableViewDataSource,UnionMyPersonalInformationVCDelegate>


@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation MyFanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"粉丝";
    
    [self creatUI];
    [self getData];
}
-(void)creatUI{
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    self.mainTableView.scrollEnabled = YES;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    [self.mainTableView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.mainTableView);
    }];
    
}
// MARK: 粉丝列表
-(void)getData{
    [self.dataArray removeAllObjects];
    [RequestManager fansListWithId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"粉丝列表 === %@",response);
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                PayAttentionTheListModel *model = [PayAttentionTheListModel mj_objectWithKeyValues:array[i]];
                [self.dataArray addObject:model];
            }
        }
        self.nullView.hidden = self.dataArray.count;
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AttentionListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AttentionListTableViewCell"];
    if (!cell) {
        cell = [[AttentionListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AttentionListTableViewCell"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    PayAttentionTheListModel *model = [[PayAttentionTheListModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.listModel = model;
    
    if (model.isFollow) {
        [cell.button setTitle:@"互相关注" forState:UIControlStateNormal];
    } else {
        [cell.button setTitle:@"+回关" forState:UIControlStateNormal];
    }
    [cell.button whenTapped:^{
        if ([cell.button.titleLabel.text isEqual:@"互相关注"]) {
            [RequestManager deleteFollowsMappingWithId:[[UserInfoManager shared] getUserID] followUserId:model.ID withSuccess:^(id  _Nullable response) {
                NSLog(@"取消关注 == %@",response);
                if ([response[@"status"] integerValue] == 200) {
                    model.isFollow = YES;
                    [cell.button setTitle:@"互相关注" forState:UIControlStateNormal];
                }
//                [self.mainTableView reloadData];
            } withFail:^(NSError * _Nullable error) {
                
            }];
        } else {
            [RequestManager addFollowsMappingWithId:[[UserInfoManager shared] getUserID] followUserId:model.ID withSuccess:^(id  _Nullable response) {
                NSLog(@"关注 === %@",response);
                if ([response[@"status"] integerValue] == 200) {
                    model.isFollow = YES;
                    [cell.button setTitle:@"互相关注" forState:UIControlStateNormal];
                }
            } withFail:^(NSError * _Nullable error) {
                
            }];
        }
        
    }];
    
    
    return cell;
       
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PayAttentionTheListModel *model = self.dataArray[indexPath.row];
    MyPersonalInformationVC *vc= [[MyPersonalInformationVC alloc]init];
    vc.ID = model.ID;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)clickMyPersonalInformationVC{
    [self getData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
