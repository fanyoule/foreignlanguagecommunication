//
//  MyAttentionViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "MyAttentionViewController.h"
#import "AttentionListTableViewCell.h"
#import "ChatDetailsViewController.h"// 聊天详情"

#import "PayAttentionTheListModel.h"
#import "MyPersonalInformationVC.h"// 个人资料
#import "ThePersonalDataModel.h"
@interface MyAttentionViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (nonatomic, strong)NSMutableArray *dataArray;

@end

@implementation MyAttentionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"关注";
    [self creatUI];
    
    [self getData];
}
-(void)creatUI{
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    self.mainTableView.scrollEnabled = YES;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    [self.mainTableView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.mainTableView);
    }];
    
}
// MARK: 关注列表
-(void)getData{
    
    [RequestManager followListWithId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"关注列表 === %@",response);
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                PayAttentionTheListModel *model = [PayAttentionTheListModel mj_objectWithKeyValues:array[i]];
                [self.dataArray addObject:model];
            }
        }
        self.nullView.hidden = self.dataArray.count;
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AttentionListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AttentionListTableViewCell"];
    if (!cell) {
        cell = [[AttentionListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AttentionListTableViewCell"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    PayAttentionTheListModel *model = [[PayAttentionTheListModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.listModel = model;
    if (model.isFollow) {
        [cell.button setTitle:@"互相关注" forState:UIControlStateNormal];
    } else {
        [cell.button setTitle:@"已关注" forState:UIControlStateNormal];
    }
    [cell.button whenTapped:^{
        //MARK: 取消关注
        UIAlertController *controller=[UIAlertController alertControllerWithTitle:nil message:@"确定不再关注此人" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *act2=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [RequestManager deleteFollowsMappingWithId:[[UserInfoManager shared] getUserID] followUserId:model.ID withSuccess:^(id  _Nullable response) {
                NSLog(@"取消关注 == %@",response);
                if ([response[@"status"] integerValue] == 200) {
                    [self.dataArray removeObject:model];
                }else{
                    [self showText:response[@"msg"]];
                }
                [self.mainTableView reloadData];
            } withFail:^(NSError * _Nullable error) {
                
            }];
            
        }];
        UIAlertAction *act1=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                 
        }];
        [controller addAction:act1];
        [controller addAction:act2];
        [self presentViewController:controller animated:YES completion:^{
                                
        }];
        
    }];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PayAttentionTheListModel *model = [[PayAttentionTheListModel alloc]init];
    model = self.dataArray[indexPath.row];
    if (indexPath.row == 1) {
        ChatDetailsViewController *vc= [[ChatDetailsViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc] init];
        vc.ID = model.ID;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
