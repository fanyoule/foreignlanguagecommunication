//
//  AttentionListTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "AttentionListTableViewCell.h"
@interface AttentionListTableViewCell ()
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/** 签名*/
@property (nonatomic, strong)UILabel *signatureLabel;

/** 线*/
@property (nonatomic, strong)UIView *lineView;
@end

@implementation AttentionListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.button];
    [self.contentView addSubview:self.signatureLabel];
    [self.contentView addSubview:self.lineView];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@40);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV).offset(3);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    [self.button sizeToFit];
    CGFloat w = self.button.bounds.size.width + 10;
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
        make.width.equalTo(@(w));
        make.height.equalTo(@30);
    }];
    [self.signatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
        make.bottom.equalTo(self.portraitImageV).offset(-3);
        make.right.equalTo(self.button.mas_left).offset(-20);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.nameLabel);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
    }];
    
}

-(void)setListModel:(PayAttentionTheListModel *)listModel{
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:listModel.avatar]];
    self.nameLabel.text = listModel.nickname;
    self.signatureLabel.text = listModel.introduce;
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像2"];
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel  = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(21, 21, 21, 1);
        _nameLabel.font = kFont_Medium(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UIButton *)button{
    if (!_button) {
        _button = [[UIButton alloc]init];
        _button.layer.cornerRadius = 6;
        _button.clipsToBounds = YES;
        _button.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
        _button.layer.borderWidth = 1;
        [_button setTitle:@"取消关注" forState:(UIControlStateNormal)];
        [_button setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _button.titleLabel.font = kFont(14);
    }
    return _button;
}

- (UILabel *)signatureLabel{
    if (!_signatureLabel) {
        _signatureLabel = [[UILabel alloc]init];
        _signatureLabel.text = @"签名签名签名签名签名签名";
        _signatureLabel.textColor = RGBA(153, 153, 153, 1);
        _signatureLabel.font = kFont_Medium(12);
        _signatureLabel.textAlignment = 0;
    }
    return _signatureLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
