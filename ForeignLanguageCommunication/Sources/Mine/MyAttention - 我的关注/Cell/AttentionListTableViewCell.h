//
//  AttentionListTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import <UIKit/UIKit.h>
#import "PayAttentionTheListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface AttentionListTableViewCell : UITableViewCell
/** 关注按钮*/
@property (nonatomic, strong)UIButton *button;

@property (nonatomic, strong)PayAttentionTheListModel *listModel;
@end

NS_ASSUME_NONNULL_END
