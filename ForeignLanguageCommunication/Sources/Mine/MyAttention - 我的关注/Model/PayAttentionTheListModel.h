//
//  PayAttentionTheListModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PayAttentionTheListModel : NSObject
/** 区*/
@property (nonatomic, strong)NSString *area;
/** 头像*/
@property (nonatomic, strong)NSString *avatar;
/** 生日*/
@property (nonatomic, strong)NSString *birthday;
/** 市*/
@property (nonatomic, strong)NSString *city;
/** 创建时间*/
@property (nonatomic, strong)NSString *createDate;
/** 身高*/
@property (nonatomic, assign)NSInteger height;
/** 爱好*/
@property (nonatomic, strong)NSString *hobby;
/** 身份证号*/
@property (nonatomic, strong)NSString *idCard;
/** 个人介绍*/
@property (nonatomic, strong)NSString *introduce;
/** 邀请码*/
@property (nonatomic, strong)NSString *inviteCode;
/** 是否教练*/
@property (nonatomic)BOOL isCoach;
/** 是否关注*/
@property (nonatomic)BOOL isFollow;
/** 登陆纬度*/
@property (nonatomic, strong)NSString *loginLat;
/** 登陆纬度*/
@property (nonatomic, strong)NSString *loginLot;
/** 昵称*/
@property (nonatomic, strong)NSString *nickname;
/** 职业*/
@property (nonatomic, strong)NSString *occupation;
/** 密码*/
@property (nonatomic, strong)NSString *passwd;
/** 手机*/
@property (nonatomic, strong)NSString *phone;
/** 省*/
@property (nonatomic, strong)NSString *province;
/** QQopenId*/
@property (nonatomic, strong)NSString *qq;
/** 姓名*/
@property (nonatomic, strong)NSString *realName;
/** 青少年模式密码*/
@property (nonatomic, strong)NSString *safemodePwd;
/** 青少年模式开关：0关闭 1开启*/
@property (nonatomic, assign)NSInteger safemodeStatus;
/** 性别：0 女，1 男*/
@property (nonatomic, assign)NSInteger sex;
/** 状态*/
@property (nonatomic, assign)NSInteger status;
/** 系统用户id*/
@property (nonatomic, strong)NSString *sysId;
/** 体重*/
@property (nonatomic, assign)NSInteger weight;
/** 微信openId*/
@property (nonatomic, strong)NSString *wx;
/** 用户id*/
@property (nonatomic, assign)NSInteger ID;
@end

NS_ASSUME_NONNULL_END
