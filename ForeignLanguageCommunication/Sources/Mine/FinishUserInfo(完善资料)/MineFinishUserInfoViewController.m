//
//  MineFinishUserInfoViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import "MineFinishUserInfoViewController.h"

@interface MineFinishUserInfoViewController ()
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation MineFinishUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navTitleString = @"完善资料";
    self.navView.backgroundColor = UIColor.whiteColor;
    self.topConstraint.constant = SNavBarHeight+20;
    self.avatarView.layer.cornerRadius = 30;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.contentMode = UIViewContentModeScaleToFill;
    self.avatarView.image = kImage(@"icon_default_head");
    self.avatarView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectAvatar:)];
    [self.avatarView addGestureRecognizer:tapAvatar];
}



- (void)selectAvatar:(UITapGestureRecognizer *)ges {
    [self.view endEditing:YES];
    
}


- (IBAction)saveInfo:(id)sender {
}

@end
