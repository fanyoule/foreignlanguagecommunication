//
//  FinishUerInfoModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FinishUerInfoModel : NSObject
// title 输入类型  默认数据 高度 允许键盘

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *defaultContent;
@property (nonatomic) BOOL isKeyboard;
@property (nonatomic) CGFloat defaultHeight;


@end

NS_ASSUME_NONNULL_END
