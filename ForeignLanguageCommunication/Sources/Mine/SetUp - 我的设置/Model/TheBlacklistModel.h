//
//  TheBlacklistModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TheBlacklistModel : NSObject
/** 生日*/
@property (nonatomic, strong)NSString *birthday;
/** 城市*/
@property (nonatomic, strong)NSString *city;
/** 头像*/
@property (nonatomic, strong)NSString *headUrl;
/** 身高*/
@property (nonatomic, assign)NSInteger height;
/** ID*/
@property (nonatomic, assign)NSInteger ID;
/** 个人介绍*/
@property (nonatomic, strong)NSString *introduce;
/** 邀请码*/
@property (nonatomic, strong)NSString *inviteCode;
/** 昵称*/
@property (nonatomic, strong)NSString *nickName;
/** 职业*/
@property (nonatomic, strong)NSString *occupation;
/** 手机*/
@property (nonatomic, strong)NSString *phone;
/** 性别（0女 1男）*/
@property (nonatomic, assign)NSInteger sex;
/** 系统id*/
@property (nonatomic, strong)NSString *sysId;
/** 体重*/
@property (nonatomic, assign)NSInteger weight;


@end

NS_ASSUME_NONNULL_END
