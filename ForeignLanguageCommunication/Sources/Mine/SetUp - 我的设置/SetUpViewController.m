//
//  SetUpViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "SetUpViewController.h"

#import "SetUpTableViewCell.h"
#import "TheGrayBarTableViewCell.h"// 灰色的条"
#import "ChangeThePasswordViewController.h"//修改密码
#import "ModifyThePhoneNumberViewController.h"// 修改手机号
#import "AboutViewController.h"// 关于
#import "ServiceAgreementViewController.h"// 服务协议
#import "PrivacyPolicyViewController.h"// 隐私政策
#import "TheBlacklistViewController.h"// 黑名单
#import "MineViewController.h"
@interface SetUpViewController ()<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, assign)NSInteger audit;

@end

@implementation SetUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"设置";
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 14;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 8 || indexPath.row == 10 || indexPath.row == 13) {
        return 10;
    }else{
        return 50;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 8 || indexPath.row == 10 || indexPath.row == 13) {
        TheGrayBarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheGrayBarTableViewCell"];
        if (!cell) {
            cell = [[TheGrayBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheGrayBarTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }
    SetUpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetUpTableViewCell"];
    if (!cell) {
        cell = [[SetUpTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SetUpTableViewCell"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    if (indexPath.row == 1) {
        cell.titleLabel.text = @"新消息通知";
        cell.arrowImageV.hidden = YES;
        cell.contentLabel.hidden = YES;
        
        UISwitch * swi = [[UISwitch alloc]init];
        // 设置控件开启状态填充色
        swi.onTintColor = ColorRGB(116, 92, 240);
        // 设置控件关闭状态填充色
        swi.tintColor = ColorRGB(245, 245, 245);
        // 设置控件开关按钮颜色
        swi.thumbTintColor = [UIColor whiteColor];
        if (swi.isOn == NO) {
            self.audit = 0;
        }else{
            self.audit = 1;
        }
        [swi addTarget:self action:@selector(changeColor:) forControlEvents:UIControlEventValueChanged];
        [cell addSubview:swi];
        [swi mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell);
            make.right.equalTo(cell).offset(-15);
        }];
        
    }else if (indexPath.row == 3){
        cell.titleLabel.text = @"手机号";
        
        cell.contentLabel.text = [NSString stringWithFormat:@"%@（仅自己可见）",[[UserInfoManager shared] phone]];
    }else if (indexPath.row == 4){
        cell.titleLabel.text = @"绑定微信";
        if ([[UserInfoManager shared] wx].length <= 0) {
            cell.contentLabel.text = @"未绑定";
        }else{
            cell.contentLabel.text = @"已绑定";
        }
    }else if (indexPath.row == 5){
        cell.titleLabel.text = @"修改密码";
        cell.contentLabel.hidden = YES;
    }else if (indexPath.row == 6){
        cell.titleLabel.text = @"黑名单";
        cell.contentLabel.hidden = YES;
    }else if (indexPath.row == 7){
        cell.titleLabel.text = @"关于";
        cell.contentLabel.hidden = YES;
    }else if (indexPath.row == 9){
        cell.titleLabel.text = @"清除图片缓存";
        
        CGFloat cache = [[SDImageCache sharedImageCache] totalDiskSize];
        
        cell.contentLabel.text = [NSString stringWithFormat:@"%0.1fM",cache/1024/1024];
    }else if (indexPath.row == 11){
        cell.titleLabel.text = @"用户使用协议";
        cell.contentLabel.hidden = YES;
    }else if (indexPath.row == 12){
        cell.titleLabel.text = @"用户隐私政策";
        cell.contentLabel.hidden = YES;
    }
    
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 3) {
            ModifyThePhoneNumberViewController *vc = [[ModifyThePhoneNumberViewController alloc]init];
            vc.isJudge = 1;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 4){
            NSLog(@"绑定微信");
            NSInteger binding = 0;
            if ([[UserInfoManager shared] wx].length <= 0) {
                binding = 1;
            }else{
                binding = 2;
            }
            ModifyThePhoneNumberViewController *vc = [[ModifyThePhoneNumberViewController alloc]init];
            vc.isBinding = binding;
            vc.isJudge = 2;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else if (indexPath.row == 5){
            ChangeThePasswordViewController *vc = [[ChangeThePasswordViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 6){
            NSLog(@"黑名单");
            TheBlacklistViewController *vc = [[TheBlacklistViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 7){
            NSLog(@"关于");
            AboutViewController *vc = [[AboutViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 9){
            NSLog(@"清除图片缓存");
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"清理缓存数据" message:@"根据缓存文件大小，清理时间从几秒到几分钟不等，请耐心等待" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        if ([[SDImageCache sharedImageCache] totalDiskSize] == 0) {
                            
                            [self  showText:@"您还没有缓存,不需要清理哦!"];

                        }else{
                            
                            [[SDImageCache sharedImageCache] clearMemory]; // 清理内存图片
                            [self  showText:@"清除成功"];
                            [self.mainTableView reloadData];
                        }
                    NSLog(@"确定");
                }];
                
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            
            
        }else if (indexPath.row == 11){
            NSLog(@"用户使用协议");
            ServiceAgreementViewController *vc = [[ServiceAgreementViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];

        }else if (indexPath.row == 12){
            NSLog(@"用户隐私政策");
            PrivacyPolicyViewController *vc = [[PrivacyPolicyViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];

        }
    }
   
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.whiteColor;
    UIButton *exitBtn = [[UIButton alloc]init];
    [exitBtn setTitle:@"退出登录" forState:(UIControlStateNormal)];
    [exitBtn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateNormal)];
    exitBtn.titleLabel.font = kFont_Medium(15);
    [exitBtn addTarget:self action:@selector(logOut) forControlEvents:(UIControlEventTouchDown)];
    [view addSubview:exitBtn];
    [exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(view);
    }];
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    
    return 50;
    
}

// MARK: 退出登录
- (void)logOut {
    [UserInfoManager deleteAllUserData];
    [[ChatIMSend share] disconnect];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logOut" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"退出登录");
    
}

-(void)changeColor:(UISwitch *)swi{
    
}


@end
