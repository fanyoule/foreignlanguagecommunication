//
//  ModifyTheNewPhoneNumberModel.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/22.
//

#import "ModifyTheNewPhoneNumberModel.h"
#import "CQCountDownButton.h"
@interface ModifyTheNewPhoneNumberModel ()<UITextFieldDelegate,CQCountDownButtonDelegate,CQCountDownButtonDataSource>
@property (nonatomic, strong)UILabel *titleLabel;


@property (nonatomic, strong)UITextField *phoneTextField;

@property (nonatomic, strong)UITextField *codeTextField;

@property (nonatomic, strong)UIButton *exitBtn;

@property (nonatomic, strong)CQCountDownButton *countDownBtn;
@end

@implementation ModifyTheNewPhoneNumberModel

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"修改手机号";
    self.navView.backgroundColor = RGBA(245, 245, 245, 1);
    
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    self.exitBtn = [[UIButton alloc]init];
    self.exitBtn.tag = 700;
    [self.exitBtn setTitle:@"提交验证" forState:(UIControlStateNormal)];
    [self.exitBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.exitBtn.titleLabel.font = kFont_Bold(15);
    self.exitBtn.layer.cornerRadius = 6;
    self.exitBtn.clipsToBounds = YES;
    self.exitBtn.backgroundColor = RGBA(52, 120, 245, 0.5);
    [self.exitBtn addTarget:self action:@selector(clickDetermine:) forControlEvents:(UIControlEventTouchDown)];
    [self.view addSubview:self.exitBtn];
    [self.exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin-20);
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@44);
    }];
    
    UIView *backView1 = [[UIView alloc]init];
    backView1.backgroundColor = UIColor.whiteColor;
    backView1.layer.cornerRadius = 40/2;
    backView1.clipsToBounds = YES;
    [self.view addSubview:backView1];
    [backView1 addSubview:self.titleLabel];
    [backView1 addSubview:self.phoneTextField];
    [backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@40);
        make.top.equalTo(self.navView.mas_bottom).offset(10);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backView1);
        make.left.equalTo(backView1).offset(15);
        make.width.equalTo(@30);
    }];
    [self.phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_right).offset(10);
        make.centerY.equalTo(backView1);
        make.right.equalTo(backView1).offset(-20);
    }];
    
    UIView *backView2 = [[UIView alloc]init];
    backView2.backgroundColor = UIColor.whiteColor;
    backView2.layer.cornerRadius = 40/2;
    backView2.clipsToBounds = YES;
    [self.view addSubview:backView2];
    [backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.width.equalTo(@260);
        make.height.equalTo(@40);
        make.top.equalTo(backView1.mas_bottom).offset(10);
    }];
    [backView2 addSubview:self.codeTextField];
    [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(backView2);
        make.width.equalTo(@100);
    }];
    [self.view addSubview: self.countDownBtn];
    [self.countDownBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backView2);
        make.left.equalTo(backView2.mas_right).offset(10);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@40);
    }];
    
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"+86";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (CQCountDownButton *)countDownBtn {
    if (!_countDownBtn) {
        _countDownBtn = [[CQCountDownButton alloc] init];
        _countDownBtn.delegate = self;
        _countDownBtn.dataSource = self;
        [_countDownBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countDownBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _countDownBtn.titleLabel.font = kFont_Medium(15);
        _countDownBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _countDownBtn.layer.cornerRadius = 40/2;
        _countDownBtn.clipsToBounds = YES;
    }
    return _countDownBtn;
}

- (UITextField *)phoneTextField{
    if (!_phoneTextField) {
        _phoneTextField = [[UITextField alloc]init];
        _phoneTextField.tag = 800;
        _phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
        _phoneTextField.backgroundColor = ColorRGBA(255, 255, 255, 1);
        _phoneTextField.placeholder = @"请输入手机号";
        _phoneTextField.font = kFont_Medium(15);
        _phoneTextField.delegate = self;
        [_phoneTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    }
    return _phoneTextField;
}

- (UITextField *)codeTextField{
    if (!_codeTextField) {
        _codeTextField = [[UITextField alloc]init];
        _codeTextField.tag = 801;
        _codeTextField.keyboardType = UIKeyboardTypeNumberPad;
        _codeTextField.backgroundColor = ColorRGBA(255, 255, 255, 1);
        _codeTextField.placeholder = @"请输入验证码";
        _codeTextField.font = kFont_Medium(15);
        _codeTextField.delegate = self;
        
        [_codeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    }
    return _codeTextField;
}
//MARK: 倒计时按钮 代理
- (NSUInteger)startCountDownNumOfCountDownButton:(CQCountDownButton *)countDownButton{
    //设置倒计时总时间
    return 60;
}
//倒计时点击事件
- (void)countDownButtonDidClick:(CQCountDownButton *)countDownButton{
    //发送验证码请求 成功后 开始倒计时
    if (self.phoneTextField.text.length != 11) {
        [self showText:@"请输入正确的手机号"];
        [countDownButton setEnabled:YES];
    }else{
        NSLog(@"手机号 === %@",self.phoneTextField.text);
        [RequestManager getVerifyCode:self.phoneTextField.text withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"发送成功"];
                [countDownButton startCountDown];
            }else{
                [self showText:response[@"msg"]];
            }
            
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"失败 === %@",error);
        }];
    }
    
    
    
}

- (void)countDownButtonDidEndCountDown:(CQCountDownButton *)countDownButton{
    //倒计时结束的回调
    [countDownButton setEnabled:YES];
    [countDownButton setTitle:@"重新发送" forState:UIControlStateNormal];
    [countDownButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    countDownButton.titleLabel.font = kFont_Medium(15);
}
- (void)countDownButtonDidStartCountDown:(CQCountDownButton *)countDownButton{
    //倒计时开始的回调
    NSLog(@"开始倒计时");
    
}
- (void)countDownButtonDidInCountDown:(CQCountDownButton *)countDownButton withRestCountDownNum:(NSInteger)restCountDownNum{
    //倒计时进行中的回调
    [countDownButton setEnabled:NO];
    
    NSLog(@"倒计时时间==%ld",restCountDownNum);
    [countDownButton setTitle:[NSString stringWithFormat:@"重新发送(%lds)",restCountDownNum] forState:UIControlStateNormal];
    [countDownButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    countDownButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [countDownButton setTitleColor:UIColor.lightGrayColor forState:UIControlStateNormal];
    [countDownButton sizeToFit];
    CGFloat w = countDownButton.bounds.size.width + 20;
    [countDownButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(w));
    }];
}
//MARK: 重置密码
- (void)clickToResetPWD:(UIButton *)sender {
    //请求重置密码
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)textFieldDidChange:(UITextField *)textField {
    int kMaxLength = 11;
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入方式
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
    
    if (textField.tag == 800) {
        
    }else{
        if (textField.text.length <= 0) {
            self.exitBtn.backgroundColor = RGBA(52, 120, 245, 0.5);
            self.exitBtn.tag = 700;
        }else{
            self.exitBtn.backgroundColor = RGBA(52, 120, 245, 1);
            self.exitBtn.tag = 701;
        }
    }
}

-(void)clickDetermine:(UIButton *)button{
    if (button.tag == 700) {
        
    }else{
        
        [RequestManager changePhoneWithId:[[UserInfoManager shared] getUserID] phone:self.phoneTextField.text vCode:self.codeTextField.text withSuccess:^(id  _Nullable response) {
            NSLog(@"%@",response);
            if ([response[@"status"] integerValue] == 200) {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"修改失败 ====  %@",error);
            [self showText:error.localizedDescription];
        }];
    }
    
}

@end
