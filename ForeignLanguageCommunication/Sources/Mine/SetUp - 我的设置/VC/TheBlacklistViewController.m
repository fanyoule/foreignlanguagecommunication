//
//  TheBlacklistViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/25.
//  黑名单

#import "TheBlacklistViewController.h"
#import "MembersBannedTableViewCell.h"
#import "TheBlacklistModel.h"
@interface TheBlacklistViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation TheBlacklistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"黑名单";
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    [self getData];
    // Do any additional setup after loading the view.
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

-(void)getData{
    [RequestManager getMyBlackUser:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"黑名单 === %@",response);
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                TheBlacklistModel *model = [TheBlacklistModel mj_objectWithKeyValues:array[i]];
                [self.dataArray addObject:model];
            }
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MembersBannedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MembersBannedTableViewCell"];
        if (!cell) {
            cell = [[MembersBannedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MembersBannedTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
    TheBlacklistModel *model = [[TheBlacklistModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    
    [cell.removeBtn whenTapped:^{
        NSLog(@"解除");
        [RequestManager blackUserWithId:[[UserInfoManager shared] getUserID] blackUserId:model.ID type:0 withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"已取消拉黑"];
                [self.dataArray removeObject:model];
                [self.mainTableView reloadData];
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }];
    return cell;
}

@end
