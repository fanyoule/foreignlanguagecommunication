//
//  ModifyThePhoneNumberViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ModifyThePhoneNumberViewController : BaseViewController

/** 1：手机号  2：绑定微信*/
@property (nonatomic, assign)NSInteger isJudge;
/** 1：未绑定  2：绑定*/
@property (nonatomic, assign)NSInteger isBinding;
@end

NS_ASSUME_NONNULL_END
