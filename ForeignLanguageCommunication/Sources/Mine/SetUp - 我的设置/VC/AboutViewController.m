//
//  AboutViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/25.
//  关于

#import "AboutViewController.h"
#import "ServiceAgreementViewController.h"// 服务协议
#import "PrivacyPolicyViewController.h"// 隐私政策
@interface AboutViewController ()
/** 版本号*/
@property (nonatomic, strong)UILabel *versionLabel;



@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"关于";
    
    self.navView.backgroundColor = RGBA(245, 245, 245, 1);
    
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    
    [self addControls];
}

-(void)addControls{
    
    UIView *backView1 = [[UIView alloc]init];
    backView1.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView1];
    [backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom).offset(20);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@50);
    }];
    
    UILabel *currentLabel = [[UILabel alloc]init];
    currentLabel.text = @"当前版本";
    currentLabel.textColor = RGBA(24, 24, 24, 1);
    currentLabel.font = kFont_Medium(20);
    currentLabel.textAlignment = 0;
    [backView1 addSubview:currentLabel];
    [currentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backView1);
        make.left.equalTo(backView1).offset(15);
    }];
    
    self.versionLabel = [[UILabel alloc]init];
    self.versionLabel.text = @"V1.0.1";
    self.versionLabel.textColor = RGBA(153, 153, 153, 1);
    self.versionLabel.font = kFont_Medium(15);
    self.versionLabel.textAlignment = 2;
    [backView1 addSubview:self.versionLabel];
    [self.versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backView1);
        make.right.equalTo(backView1.mas_right).offset(-15);
    }];
    
    UIView *backView2 = [[UIView alloc]init];
    backView2.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView2];
    [backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backView1.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@50);
    }];
    
    UILabel *detectionLabel = [[UILabel alloc]init];
    detectionLabel.text = @"检测更新";
    detectionLabel.textColor = RGBA(24, 24, 24, 1);
    detectionLabel.font = kFont_Medium(20);
    detectionLabel.textAlignment = 0;
    [backView2 addSubview:detectionLabel];
    [detectionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backView2);
        make.left.equalTo(backView2).offset(15);
    }];
    
    UIImageView *arrowImageV = [[UIImageView alloc]init];
    arrowImageV.image = [UIImage imageNamed:@"我的-箭头"];
    [backView2 addSubview:arrowImageV];
    [arrowImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backView2);
        make.right.equalTo(backView2).offset(-15);
        make.width.equalTo(@10);
        make.height.equalTo(@15);
    }];
    
    UIButton *serviceBtn = [[UIButton alloc]init];
    [serviceBtn setTitle:@"服务协议" forState:(UIControlStateNormal)];
    [serviceBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
    serviceBtn.titleLabel.font = kFont(15);
    [serviceBtn addTarget:self action:@selector(serviceAgreement) forControlEvents:(UIControlEventTouchDown)];
    [self.view addSubview:serviceBtn];
    [serviceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_centerX).offset(-5);
        make.bottom.equalTo(self.view).offset(-10-TabbarSafeMargin);
    }];
    UIButton *privacyBtn = [[UIButton alloc]init];
    [privacyBtn setTitle:@"隐私政策" forState:(UIControlStateNormal)];
    [privacyBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
    privacyBtn.titleLabel.font = kFont(15);
    [privacyBtn addTarget:self action:@selector(privacyPolicy) forControlEvents:(UIControlEventTouchDown)];
    [self.view addSubview:privacyBtn];
    [privacyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_centerX).offset(5);
        make.bottom.equalTo(self.view).offset(-10-TabbarSafeMargin);
    }];
    
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(153, 153, 153, 1);
    [self.view addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@0.5);
        make.top.equalTo(backView1.mas_bottom);
    }];
    
    [backView2 whenTapped:^{
        // 检测更新
        NSLog(@"检测更新");
        [self showText:@"当前是最新版本"];
    }];
    
    
    
}

// MARK: 服务协议
-(void)serviceAgreement{
    NSLog(@"服务协议");
    ServiceAgreementViewController *vc = [[ServiceAgreementViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

// MARK: 隐私政策
-(void)privacyPolicy{
    NSLog(@"隐私政策");
    PrivacyPolicyViewController *vc = [[PrivacyPolicyViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
