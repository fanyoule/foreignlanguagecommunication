//
//  ModifyThePhoneNumberViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//  修改手机号

#import "ModifyThePhoneNumberViewController.h"

#import "ModifyTheNewPhoneNumberModel.h"// 绑定新的手机号
#import "CQCountDownButton.h"
#import <UMShare/UMShare.h>
@interface ModifyThePhoneNumberViewController ()<UITextFieldDelegate,CQCountDownButtonDelegate,CQCountDownButtonDataSource>
@property (nonatomic, strong)UIButton *exitBtn;

@property (nonatomic, strong)UITextField *textField;

@property (nonatomic, strong)CQCountDownButton *countDownBtn;
@end

@implementation ModifyThePhoneNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isJudge == 1) {
        self.navTitleString = @"修改手机号";
        
    }else{
        self.navTitleString = @"绑定微信";
    }
    
    self.navView.backgroundColor = RGBA(245, 245, 245, 1);
    
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    
    UILabel *numberLabel = [[UILabel alloc]init];
    numberLabel.text = [NSString stringWithFormat:@"将发送短信验证码至%@",[[UserInfoManager shared] phone]];
    numberLabel.textColor = RGBA(153, 153, 153, 1);
    numberLabel.font = kFont_Bold(12);
    numberLabel.textAlignment = 0;
    [self.view addSubview:numberLabel];
    [numberLabel  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(10);
    }];

        
    
    self.exitBtn = [[UIButton alloc]init];
    
    [self.exitBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
    self.exitBtn.titleLabel.font = kFont_Bold(15);
    self.exitBtn.layer.cornerRadius = 6;
    self.exitBtn.clipsToBounds = YES;
    self.exitBtn.backgroundColor = RGBA(52, 120, 245, 0.5);
    self.exitBtn.tag = 700;
    [self.exitBtn addTarget:self action:@selector(clickDetermine:) forControlEvents:(UIControlEventTouchDown)];
    [self.view addSubview:self.exitBtn];
    [self.exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin-20);
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@44);
    }];
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(numberLabel.mas_bottom).offset(10);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@70);
    }];
    [view addSubview:self.textField];
    [view addSubview:self.countDownBtn];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.left.equalTo(view).offset(10);
        make.width.equalTo(@260);
    }];
    [self.countDownBtn sizeToFit];
    CGFloat w = self.countDownBtn.bounds.size.width + 20;
    [self.countDownBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.right.equalTo(view).offset(-15);
        make.width.equalTo(@(w));
    }];
    
        if (self.isBinding == 1) {
            [self.exitBtn setTitle:@"绑定" forState:(UIControlStateNormal)];
        }else if (self.isBinding == 2){
            [self.exitBtn setTitle:@"解绑" forState:(UIControlStateNormal)];
        }else{
            [self.exitBtn setTitle:@"确定" forState:(UIControlStateNormal)];
        }
}


- (UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc]init];
        _textField.keyboardType = UIKeyboardTypeNumberPad;
        _textField.backgroundColor = ColorRGBA(255, 255, 255, 1);
        _textField.placeholder = @"请输入验证码";
        _textField.font = kFont_Medium(15);
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    }
    return _textField;
}

- (CQCountDownButton *)countDownBtn {
    if (!_countDownBtn) {
        _countDownBtn = [[CQCountDownButton alloc] init];
        _countDownBtn.delegate = self;
        _countDownBtn.dataSource = self;
        [_countDownBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countDownBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _countDownBtn.titleLabel.font = kFont_Medium(15);
        _countDownBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _countDownBtn.layer.cornerRadius = 30/2;
        _countDownBtn.clipsToBounds = YES;
    }
    return _countDownBtn;
}


//MARK: 倒计时按钮 代理
- (NSUInteger)startCountDownNumOfCountDownButton:(CQCountDownButton *)countDownButton{
    //设置倒计时总时间
    return 60;
}
//倒计时点击事件
- (void)countDownButtonDidClick:(CQCountDownButton *)countDownButton{
    //发送验证码请求 成功后 开始倒计时
    
    [RequestManager getVerifyCode:[[UserInfoManager shared] phone] withSuccess:^(id  _Nullable response) {
        if ([response[@"status"] integerValue] == 200) {
            [self showText:@"发送成功"];
            [countDownButton startCountDown];
        }else{
            [self showText:response[@"msg"]];
        }
            
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"失败 === %@",error);
    }];
    
}

- (void)countDownButtonDidEndCountDown:(CQCountDownButton *)countDownButton{
    //倒计时结束的回调
    [countDownButton setEnabled:YES];
    [countDownButton setTitle:@"重新发送" forState:UIControlStateNormal];
    [countDownButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    countDownButton.titleLabel.font = kFont_Medium(15);
}
- (void)countDownButtonDidStartCountDown:(CQCountDownButton *)countDownButton{
    //倒计时开始的回调
    NSLog(@"开始倒计时");
    
}
- (void)countDownButtonDidInCountDown:(CQCountDownButton *)countDownButton withRestCountDownNum:(NSInteger)restCountDownNum{
    //倒计时进行中的回调
    [countDownButton setEnabled:NO];
    
    NSLog(@"倒计时时间==%ld",restCountDownNum);
    [countDownButton setTitle:[NSString stringWithFormat:@"重新发送(%lds)",restCountDownNum] forState:UIControlStateNormal];
    [countDownButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    countDownButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [countDownButton sizeToFit];
    CGFloat w = countDownButton.bounds.size.width + 20;
    [countDownButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(w));
    }];
}
//MARK: 重置密码
- (void)clickToResetPWD:(UIButton *)sender {
    //请求重置密码
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}








- (void)textFieldDidChange:(UITextField *)textField {
    int kMaxLength = 30;
    
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入方式
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
    
    if (textField.text.length <= 0) {
        self.exitBtn.backgroundColor = RGBA(52, 120, 245, 0.5);
        self.exitBtn.tag = 700;
    }else{
        self.exitBtn.backgroundColor = RGBA(52, 120, 245, 1);
        self.exitBtn.tag = 701;
    }
    
}

-(void)clickDetermine:(UIButton *)button{
    long code = [self.textField.text intValue];
    if (button.tag == 700) {
        
    }else{
        if (self.isJudge == 1) {
            [RequestManager verifyCode:[[UserInfoManager shared] phone] vCode:code withSuccess:^(id  _Nullable response) {
                if ([response[@"status"] integerValue] == 200) {
                    [self.navigationController popViewControllerAnimated:YES];
                    
                    ModifyTheNewPhoneNumberModel *vc = [[ModifyTheNewPhoneNumberModel alloc]init];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                    
                    NSLog(@"点击了确定");
                    
                }else{
                    [self showText:response[@"msg"]];
                }
            } withFail:^(NSError * _Nullable error) {
                NSLog(@"%@",error);
            }];
        }else{
            if (self.isBinding == 1) {
                NSString *openId = nil;
                [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:self completion:^(id result, NSError *error) {
                    if (result) {
                        NSSLog(@"授权成功 ====== %@",result);
//                        openId = result[@"openid"];
                    }else{
                        NSSLog(@"授权失败 ====== %@",error);
                    }
                }];
                
                
//                [RequestManager postUserUpUserWxSetWithId:<#(long)#> openId:<#(nonnull NSString *)#> phone:<#(nonnull NSString *)#> vCode:<#(nonnull NSString *)#> withSuccess:<#^(id  _Nullable response)success#> withFail:<#^(NSError * _Nullable error)fail#>]
                
                
            }else{
                [RequestManager postUserRomveUserWxSetWithId:[[UserInfoManager shared] getUserID] phone:[[UserInfoManager shared] phone] vCode:self.textField.text withSuccess:^(id  _Nullable response) {
                    
                } withFail:^(NSError * _Nullable error) {
                    
                }];
            }
        }
        
        
    }
}



@end
