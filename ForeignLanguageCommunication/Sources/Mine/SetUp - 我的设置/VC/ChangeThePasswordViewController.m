//
//  ChangeThePasswordViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//  修改密码

#import "ChangeThePasswordViewController.h"
#import "CoachCertificationInformationTableViewCell.h"
@interface ChangeThePasswordViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
/** 旧密码*/
@property (nonatomic, strong)NSString *oldPasswordStr;
/** 新密码*/
@property (nonatomic, strong)NSString *theNewPassword;
/** 确认密码*/
@property (nonatomic, strong)NSString *confirmPasswordStr;
@property (nonatomic, strong) UIButton *button;
@end

@implementation ChangeThePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"修改密码";
    self.navView.backgroundColor = UIColor.whiteColor;
    
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    [self setUpForDismissKeyboard];
}

- (void)setUpForDismissKeyboard {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    UITapGestureRecognizer *singleTapGR =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapAnywhereToDismissKeyboard:)];
    NSOperationQueue *mainQuene =[NSOperationQueue mainQueue];
    [nc addObserverForName:UIKeyboardWillShowNotification
                    object:nil
                     queue:mainQuene
                usingBlock:^(NSNotification *note){
                    [self.view addGestureRecognizer:singleTapGR];
                }];
    [nc addObserverForName:UIKeyboardWillHideNotification
                    object:nil
                     queue:mainQuene
                usingBlock:^(NSNotification *note){
                    [self.view removeGestureRecognizer:singleTapGR];
                }];
}
 
- (void)tapAnywhereToDismissKeyboard:(UIGestureRecognizer *)gestureRecognizer {
    //此method会将self.view里所有的subview的first responder都resign掉
    [self.view endEditing:YES];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 3;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 50;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CoachCertificationInformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoachCertificationInformationTableViewCell"];
        if (!cell) {
            cell = [[CoachCertificationInformationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CoachCertificationInformationTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
    
    if (indexPath.row == 0) {
        cell.titleLabel.text = @"旧密码";
        cell.textField.placeholder = @"请输入您的旧密码";
        cell.textField.tag = 800;
        
    }else if (indexPath.row == 1){
        cell.titleLabel.text = @"新密码";
        cell.textField.placeholder = @"请输入您的新密码";
        cell.textField.tag = 801;
        
    }else if (indexPath.row == 2){
        cell.titleLabel.text = @"确认密码";
        cell.textField.placeholder = @"请再次输入您的新密码";
        cell.textField.tag = 802;
        
    }
    cell.textField.delegate = self;
    [cell.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    
        return cell;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.clearColor;
    
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    return 10;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.clearColor;
    UIButton *exitBtn = [[UIButton alloc]init];
    [exitBtn setTitle:@"确定" forState:(UIControlStateNormal)];
    [exitBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
    exitBtn.titleLabel.font = kFont_Bold(15);
    exitBtn.layer.cornerRadius = 6;
    exitBtn.clipsToBounds = YES;
    exitBtn.userInteractionEnabled = NO;
    exitBtn.backgroundColor = RGBA(52, 120, 245, 0.5);
//    exitBtn.backgroundColor = kTHEMECOLOR;
    exitBtn.tag = 500;
    [exitBtn addTarget:self action:@selector(logOut) forControlEvents:(UIControlEventTouchDown)];
    [view addSubview:exitBtn];
    self.button = exitBtn;
    [exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.left.equalTo(view).offset(15);
        make.right.equalTo(view).offset(-15);
        make.height.equalTo(@44);
    }];
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    
    return 104;
    
}

- (void)textFieldDidChange:(UITextField *)textField {
    int kMaxLength = 30;
    
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入方式
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
    if (textField.tag == 800) {
        self.oldPasswordStr = textField.text;
    }else if (textField.tag == 801){
        self.theNewPassword = textField.text;
    }else if (textField.tag == 802){
        self.confirmPasswordStr = textField.text;
    }
    if (self.oldPasswordStr.length > 0 && self.theNewPassword.length > 0 && self.confirmPasswordStr.length > 0) {
        self.button.backgroundColor = kTHEMECOLOR;
        self.button.userInteractionEnabled = YES;
    } else {
        self.button.backgroundColor = RGBA(52, 120, 245, 0.5);
        self.button.userInteractionEnabled = NO;
    }
    
}

-(void)logOut{
    if (self.oldPasswordStr.length <= 0) {
        [self showText:@"请输入原密码"];
    }else if (self.theNewPassword.length <= 0){
        [self showText:@"请输入新密码"];
    }else if (self.confirmPasswordStr.length <= 0){
        [self showText:@"请再次确认新密码"];
    }else{
        [RequestManager updPwd:[[UserInfoManager shared] getUserID] newPwd:self.theNewPassword oldPwd:self.oldPasswordStr withSuccess:^(id  _Nullable response) {
            NSLog(@"改密码 === %@",response);
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"修改成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"失败改密码 === %@",error);
            [self showText:@"用户名或密码错误"];
            
        }];
    }
    
    
    NSLog(@"点击了确定");
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
