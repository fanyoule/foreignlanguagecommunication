//
//  MembersBannedTableViewCell.m
//  VoiceLive
//
//  Created by 申修智 on 2020/11/17.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "MembersBannedTableViewCell.h"

@implementation MembersBannedTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        
    }
    return self;
}


-(void)addControls{
    [self.contentView addSubview:self.portraitImageview];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.removeBtn];
    [self.contentView addSubview:self.lineView];
    [self.portraitImageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.and.height.equalTo(@44);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.portraitImageview.mas_right).offset(10);
    }];
    [self.removeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
        make.width.equalTo(@51);
        make.height.equalTo(@28);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.bottom.equalTo(self.contentView);
        make.left.equalTo(self.nameLabel);
        make.height.equalTo(@0.5);
    }];
}

- (void)setModel:(TheBlacklistModel *)model{
    _model =model;
    [self.portraitImageview sd_setImageWithURL:[NSURL URLWithString:model.headUrl]];
    self.nameLabel.text = model.nickName;
    
    
}

- (UIImageView *)portraitImageview{
    if (!_portraitImageview) {
        _portraitImageview = [[UIImageView alloc]init];
        _portraitImageview.image = [UIImage imageNamed:@"测试头像"];
        _portraitImageview.layer.cornerRadius = 6;
        _portraitImageview.layer.masksToBounds = YES;
    }
    return _portraitImageview;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(34, 34, 34,  1);
        _nameLabel.font = kFont_Medium(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UIButton *)removeBtn{
    if (!_removeBtn) {
        _removeBtn = [[UIButton alloc]init];
        [_removeBtn setTitle:@"解除" forState:(UIControlStateNormal)];
        [_removeBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _removeBtn.titleLabel.font = kFont(14);
        _removeBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _removeBtn.layer.cornerRadius = 14;
        _removeBtn.layer.masksToBounds = YES;
    }
    return _removeBtn;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
