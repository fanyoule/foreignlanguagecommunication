//
//  MembersBannedTableViewCell.h
//  VoiceLive
//
//  Created by 申修智 on 2020/11/17.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TheBlacklistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MembersBannedTableViewCell : UITableViewCell
@property(nonatomic,strong)UIImageView *portraitImageview;
@property(nonatomic, strong)UILabel *nameLabel;
@property(nonatomic, strong)UIButton *removeBtn;
@property(nonatomic, strong)UIView *lineView;

@property (nonatomic, strong)TheBlacklistModel *model;
@end

NS_ASSUME_NONNULL_END
