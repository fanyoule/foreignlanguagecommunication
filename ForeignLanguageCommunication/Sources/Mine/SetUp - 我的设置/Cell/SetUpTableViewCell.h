//
//  SetUpTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetUpTableViewCell : UITableViewCell
/** 标题*/
@property (nonatomic ,strong)UILabel *titleLabel;
/** 箭头*/
@property ( nonatomic, strong)UIImageView *arrowImageV;
/** 内容*/
@property (nonatomic, strong)UILabel *contentLabel;
@end

NS_ASSUME_NONNULL_END
