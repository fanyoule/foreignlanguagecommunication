//
//  SetUpTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "SetUpTableViewCell.h"
@interface SetUpTableViewCell ()


/** 线*/
@property (nonatomic, strong)UIView *lineView;
@end
@implementation SetUpTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.arrowImageV];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.lineView];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.arrowImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.arrowImageV.mas_left).offset(-10);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(1);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
    }];
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"手机号";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UIImageView *)arrowImageV{
    if (!_arrowImageV) {
        _arrowImageV = [[UIImageView alloc]init];
        _arrowImageV.image = [UIImage imageNamed:@"我的-箭头"];
    }
    return _arrowImageV;
}
- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"";
        _contentLabel.textColor = RGBA(102, 102, 102, 1);
        _contentLabel.font = kFont_Medium(14);
        _contentLabel.textAlignment = 2;
    }
    return _contentLabel;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
