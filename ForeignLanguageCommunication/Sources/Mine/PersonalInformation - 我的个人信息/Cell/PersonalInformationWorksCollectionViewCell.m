//
//  PersonalInformationWorksCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "PersonalInformationWorksCollectionViewCell.h"

@interface PersonalInformationWorksCollectionViewCell ()



@end
@implementation PersonalInformationWorksCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.imageV];
    [self.imageV addSubview:self.playImageV];
    [self.imageV addSubview:self.playLabel];
    [self.contentView addSubview:self.selector];
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.contentView);
    }];
    [self.playImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageV).offset(5);
        make.bottom.equalTo(self.imageV).offset(-5);
        make.width.height.equalTo(@10);
    }];
    [self.playLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.playImageV);
        make.left.equalTo(self.playImageV.mas_right).offset(5);
    }];
    [self.selector mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageV).offset(5);
        make.right.equalTo(self.imageV).offset(-5);
        make.width.height.equalTo(@20);
    }];
}

- (void)setWorkmodel:(MyCollectionWorksModel *)workmodel{
    _workmodel = workmodel;
    
    NSArray  *arrayURLImage = [workmodel.picUrl componentsSeparatedByString:@","];
    
    if (workmodel.type == 1) {
        if (arrayURLImage != nil && ![arrayURLImage isKindOfClass:[NSNull class]] && (arrayURLImage.count != 0)) {
            [self.imageV sd_setImageWithURL:[NSURL URLWithString:arrayURLImage[0]]];
        }
    }else{
        [self.imageV sd_setImageWithURL:[NSURL URLWithString:workmodel.videoCoverUrl]];
    }
    
    self.playLabel.text = [NSString stringWithFormat:@"%ld",workmodel.browseNum];
    
    
}

- (SDAnimatedImageView *)imageV{
    if (!_imageV) {
        _imageV = [[SDAnimatedImageView alloc]init];
        _imageV.image = [UIImage imageNamed:@"添加照片"];
        _imageV.contentMode = UIViewContentModeScaleAspectFill;
        _imageV.layer.cornerRadius = 6;
        _imageV.clipsToBounds = YES;
        
    }
    return _imageV;
}

- (UIImageView *)playImageV{
    if (!_playImageV) {
        _playImageV = [[UIImageView alloc]init];
        _playImageV.image = [UIImage imageNamed:@"作品-播放"];
    }
    return _playImageV;
}

- (UILabel *)playLabel{
    if (!_playLabel) {
        _playLabel = [[UILabel alloc]init];
        _playLabel.text = @"210";
        _playLabel.textColor = RGBA(255, 255, 255, 1);
        _playLabel.font = kFont_Bold(14);
        _playLabel.textAlignment = 0;
    }
    return _playLabel;
}

-(UIButton *)selector{
    if (!_selector) {
        _selector = [[UIButton alloc]init];
        [_selector setImage:[UIImage imageNamed:@"作品-未选中"] forState:(UIControlStateNormal)];
        [_selector setImage:[UIImage imageNamed:@"作品-选中"] forState:(UIControlStateSelected)];
    }
    return _selector;
}
- (void)setShouldEdit:(BOOL)shouldEdit {
    _shouldEdit = shouldEdit;
    self.selector.hidden = !shouldEdit;
}
@end
