//
//  PersonalInformationWorksCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import <UIKit/UIKit.h>
#import "MyCollectionWorksModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PersonalInformationWorksCollectionViewCell : UICollectionViewCell
/** 选中*/
@property (nonatomic, strong)UIButton *selector;

/** 播放量图片*/
@property (nonatomic, strong)UIImageView *playImageV;
/** 播放量*/
@property (nonatomic, strong)UILabel *playLabel;

@property (nonatomic) BOOL shouldEdit;

@property (nonatomic, strong)MyCollectionWorksModel *workmodel;

/** 图片*/
@property (nonatomic, strong)SDAnimatedImageView *imageV;
@end

NS_ASSUME_NONNULL_END
