//
//  PersonalInformationRulesTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "PersonalInformationRulesTableViewCell.h"

@implementation PersonalInformationRulesTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UILabel *label = [[UILabel alloc]init];
            label.text = @"请勿通过平台进行不法交易，如被举报核实将做封号处理";
            label.textColor = RGBA(153, 153, 153, 1);
            label.font = kFont(10);
            label.textAlignment = 1;
            [self.contentView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.contentView).offset(15);
                make.centerX.equalTo(self.contentView);
            }];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
