//
//  PersonalInformationAboutHisWorksTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import <UIKit/UIKit.h>
#import "ThePersonalDataModel.h"
#import "MyCollectionWorksModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^PersonalInformationWorksBlock)(MyCollectionWorksModel *model);
@interface PersonalInformationAboutHisWorksTableViewCell : UITableViewCell
/** 箭头*/
@property (nonatomic, strong)UIButton *arrowBtn;


@property (nonatomic, strong)ThePersonalDataModel *dataModel;
@property (nonatomic, copy) PersonalInformationWorksBlock block;
@end

NS_ASSUME_NONNULL_END
