//
//  PersonalDataReportingCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "PersonalDataReportingCollectionViewCell.h"
@interface PersonalDataReportingCollectionViewCell ()
@property (nonatomic, strong)UIImageView *reportsBackgroundImageV;

@property (nonatomic, strong)UILabel *uploadLabel;

@property(nonatomic, strong)UIImageView *reportIamgeV;

@end
@implementation PersonalDataReportingCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.reportsBackgroundImageV];
    [self.contentView addSubview:self.uploadLabel];
    [self.contentView addSubview:self.reportIamgeV];
    [self.reportsBackgroundImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.contentView);
    }];
    [self.uploadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.reportsBackgroundImageV);
        make.bottom.equalTo(self.reportsBackgroundImageV).offset(-10);
    }];
    [self.reportIamgeV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.reportsBackgroundImageV);
        make.top.equalTo(self.reportsBackgroundImageV).offset(20);
    }];
    
}

- (UIImageView *)reportsBackgroundImageV{
    if (!_reportsBackgroundImageV) {
        _reportsBackgroundImageV = [[UIImageView alloc]init];
        _reportsBackgroundImageV.backgroundColor = RGBA(238, 238, 238, 1);
        _reportsBackgroundImageV.image = [UIImage imageNamed:@""];
        _reportsBackgroundImageV.layer.cornerRadius = 6;
        _reportsBackgroundImageV.clipsToBounds = YES;
        _reportsBackgroundImageV.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        _reportsBackgroundImageV.layer.borderWidth = 0.5;
    }
    return _reportsBackgroundImageV;
}

- (UILabel *)uploadLabel{
    if (!_uploadLabel) {
        _uploadLabel = [[UILabel alloc]init];
        _uploadLabel.text = @"上传照片";
        _uploadLabel.textColor = RGBA(51, 51, 51, 1);
        _uploadLabel.font = kFont(12);
        _uploadLabel.textAlignment = 1;
    }
    return _uploadLabel;
}

- (UIImageView *)reportIamgeV{
    if (!_reportIamgeV) {
        _reportIamgeV = [[UIImageView alloc]init];
        _reportIamgeV.image = [UIImage imageNamed:@"我加入的房间"];
    }
    return _reportIamgeV;
}

@end
