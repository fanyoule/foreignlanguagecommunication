//
//  PersonalInformationImageInformationTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "PersonalInformationImageInformationTableViewCell.h"

@interface PersonalInformationImageInformationTableViewCell ()
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 距离背景*/
@property (nonatomic, strong)UIView *distanceView;
/** 距离图片*/
@property (nonatomic, strong)UIImageView *distanceImageV;
/** 距离Label*/
@property (nonatomic, strong)UILabel *distanceLabel;

///** 城市*/
@property (nonatomic, strong)UILabel *cityLabel;
///** 职业*/
@property (nonatomic, strong)UILabel *occupationLabel;


@end

@implementation PersonalInformationImageInformationTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = RGBA(223, 223, 223, 1);
        [self.contentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.contentView);
            make.height.equalTo(@0.5);
        }];
    }
    return self;
}



-(void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.cityLabel];
    [self.contentView addSubview:self.occupationLabel];
    
    [self.contentView addSubview:self.distanceView];
    [self.distanceView addSubview:self.distanceImageV];
    [self.distanceView addSubview:self.distanceLabel];
    

    
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(22.5);
        make.centerX.equalTo(self.contentView);
        make.width.height.equalTo(@95);
    }];
    [self.distanceLabel sizeToFit];
    [self.distanceImageV sizeToFit];
    CGFloat w = self.distanceLabel.bounds.size.width + self.distanceImageV.bounds.size.width + 20;
    [self.distanceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.width.equalTo(@(w));
        make.height.equalTo(@18);
        make.top.equalTo(self.portraitImageV.mas_bottom).offset(36);
    }];
    [self.distanceImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.distanceView);
        make.left.equalTo(self.distanceView).offset(7);
        
    }];
    [self.distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.distanceView);
        make.left.equalTo(self.distanceImageV.mas_right).offset(7);
    }];
    
    [self.cityLabel sizeToFit];
    CGFloat w2 = self.cityLabel.bounds.size.width + 20;
    [self.cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV.mas_bottom).offset(10);
        make.right.equalTo(self.contentView.mas_centerX).offset(-5);
        make.width.equalTo(@(w2));
    }];
    [self.occupationLabel sizeToFit];
    CGFloat w3 = self.occupationLabel.bounds.size.width + 20;
    [self.occupationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV.mas_bottom).offset(10);
        make.left.equalTo(self.contentView.mas_centerX).offset(5);
        make.width.equalTo(@(w3));
    }];
    
    [self.contentView addSubview:self.focusBtn];
    [self.focusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV);
        make.right.equalTo(self.contentView).offset(-15);
        make.width.equalTo(@40);
        make.height.equalTo(@20);
    }];
    
}

-(void)setDataModel:(ThePersonalDataModel *)dataModel{
    
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:dataModel.headUrl]];
    if (dataModel.distance.length <= 0 || dataModel.distance == nil) {
        self.distanceLabel.text = @"0km";
    }else{
        self.distanceLabel.text = [NSString stringWithFormat:@"%@km",dataModel.distance];
    }
    
    
    self.cityLabel.text = dataModel.city;
    self.occupationLabel.text = dataModel.occupation;
    
}
- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
        _portraitImageV.layer.cornerRadius = 10;
        _portraitImageV.clipsToBounds = YES;
        _portraitImageV.image = [UIImage imageNamed:@"测试头像"];
    }
    return _portraitImageV;
}
- (UIView *)distanceView{
    if (!_distanceView) {
        _distanceView = [[UIView alloc]init];
        _distanceView.backgroundColor = RGBA(153, 153, 153, 0.1);
        _distanceView.layer.cornerRadius = 9;
        _distanceView.clipsToBounds = YES;
    }
    return _distanceView;
}

- (UIImageView *)distanceImageV{
    if (!_distanceImageV) {
        _distanceImageV = [[UIImageView alloc]init];
        _distanceImageV.image = [UIImage imageNamed:@"个人信息-距离"];
    }
    return _distanceImageV;
}

- (UILabel *)distanceLabel{
    if (!_distanceLabel) {
        _distanceLabel = [[UILabel alloc]init];
        _distanceLabel.text = @"1.2km";
        _distanceLabel.textColor = RGBA(153, 153, 153, 1);
        _distanceLabel.font = kFont(12);
        _distanceLabel.textAlignment = 0;
    }
    return _distanceLabel;
}

- (UILabel *)cityLabel{
    if (!_cityLabel) {
        _cityLabel = [[UILabel alloc]init];
        _cityLabel.text = @"徐州市";
        _cityLabel.textColor = RGBA(52, 120, 245, 1);
        _cityLabel.font = kFont(12);
        _cityLabel.textAlignment = 1;
        _cityLabel.backgroundColor = RGBA(52, 120, 245, 0.1);
        _cityLabel.layer.cornerRadius = 6;
        _cityLabel.clipsToBounds = YES;
    }
    return _cityLabel;
}

- (UILabel *)occupationLabel{
    if (!_occupationLabel) {
        _occupationLabel = [[UILabel alloc]init];
        _occupationLabel.text = @"职业";
        _occupationLabel.textColor = RGBA(52, 120, 245, 1);
        _occupationLabel.font = kFont(12);
        _occupationLabel.textAlignment = 1;
        _occupationLabel.backgroundColor = RGBA(52, 120, 245, 0.1);
        _occupationLabel.layer.cornerRadius = 6;
        _occupationLabel.clipsToBounds = YES;
        
    }
    return _occupationLabel;
}

- (UIButton *)focusBtn{
    if (!_focusBtn) {
        _focusBtn = [[UIButton alloc]init];
        [_focusBtn setTitle:@"关注" forState:(UIControlStateNormal)];
        [_focusBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _focusBtn.titleLabel.font = kFont(14);
        _focusBtn.layer.cornerRadius = 6;
        _focusBtn.clipsToBounds = YES;
        _focusBtn.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
        _focusBtn.layer.borderWidth = 1;
    }
    return _focusBtn;
}

 
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
