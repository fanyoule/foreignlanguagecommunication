//
//  PersonalInformationAboutHisWorksTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "PersonalInformationAboutHisWorksTableViewCell.h"
#import "PersonalInformationWorksCollectionViewCell.h"

@interface PersonalInformationAboutHisWorksTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource>
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property(nonatomic, strong)UILabel *titleLabel;


/** 无作品图片*/
@property (nonatomic, strong)UIImageView *backImageV;
/** 无座片文字*/
@property (nonatomic, strong)UILabel *backLabel;

@property (nonatomic, strong)UICollectionView *collectionView;

@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation PersonalInformationAboutHisWorksTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.backView];
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(10);
            make.bottom.equalTo(self.contentView).offset(-10);
        }];
        
        [self.backView addSubview:self.titleLabel];
        [self.backView addSubview:self.arrowBtn];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backView).offset(17);
            make.left.equalTo(self.backView).offset(15);
            make.height.equalTo(@30);
        }];
        [self.arrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.titleLabel);
            make.right.equalTo(self.backView).offset(-15);
            make.height.equalTo(self.titleLabel);
            make.width.equalTo(@(30));
        }];
        

    }
    return self;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

-(void)setDataModel:(ThePersonalDataModel *)dataModel{
    [self.dataArray removeAllObjects];
    _dataModel.publishs = dataModel.publishs;
    
    if (dataModel.isMe) {
        self.titleLabel.text = @"我的作品";
    }else{
        self.titleLabel.text = @"他/她的作品";
    }
    
    if ((dataModel.publishs != nil && ![dataModel.publishs isKindOfClass:[NSNull class]] && (dataModel.publishs.count != 0))) {
        [self addControls];//有作品
        for (int i = 0; i < dataModel.publishs.count; i++) {
            MyCollectionWorksModel *model = [MyCollectionWorksModel mj_objectWithKeyValues:dataModel.publishs[i]];
            if (i < 6) {
                [self.dataArray addObject:model];
            }
        }
        [self.collectionView reloadData];
    }else{
        [self thereNoWork];// 无作品
    }
}

//MARK: 无作品时显示
-(void)thereNoWork{
    [self.backView addSubview:self.backImageV];
    [self.backView addSubview:self.backLabel];
    [self.backImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(21.5);
    }];
    [self.backLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backImageV.mas_bottom).offset(20);
        make.centerX.equalTo(self.backImageV);
    }];
}
// MARK: 有作品时显示
-(void)addControls{
    [self.backView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.left.right.bottom.equalTo(self.backView);
    }];
    
    [self.collectionView registerClass:[PersonalInformationWorksCollectionViewCell class] forCellWithReuseIdentifier:@"PersonalInformationWorksCollectionViewCell"];
}


- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(255, 255, 255, 1);
    }
    return _backView;
}

- (UIImageView *)backImageV{
    if (!_backImageV) {
        _backImageV = [[UIImageView alloc]init];
        _backImageV.image = [UIImage imageNamed:@"个人资料-无作品背景"];
    }
    return _backImageV;
}
- (UILabel *)backLabel{
    if (!_backLabel) {
        _backLabel = [[UILabel alloc]init];
        _backLabel.text = @"他还没有发布任何作品哦~";
        _backLabel.textColor = RGBA(153, 153, 153, 1);
        _backLabel.font = kFont(13);
        _backLabel.textAlignment = 1;
    }
    return _backLabel;
}


- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = ColorRGBA(255, 255, 255, 1);
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.allowsMultipleSelection = YES;
    }
    return _collectionView;
}



- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"他/她的作品";
        _titleLabel.textColor = RGBA(51, 51, 51, 1);
        _titleLabel.font = kFont(16);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UIButton *)arrowBtn{
    if (!_arrowBtn) {
        _arrowBtn = [[UIButton alloc]init];
        [_arrowBtn setImage:[UIImage imageNamed:@"个人资料-箭头"] forState:(UIControlStateNormal)];
    }
    return _arrowBtn;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PersonalInformationWorksCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PersonalInformationWorksCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    cell.selector.hidden = YES;
    MyCollectionWorksModel *model = [[MyCollectionWorksModel alloc]init];
    model = self.dataArray[indexPath.item];
    cell.workmodel = model;
    
        return cell;
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    return CGSizeMake(KSW/3-20, KSW/3-20);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 10, 15);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MyCollectionWorksModel *model = self.dataArray[indexPath.item];
    if (self.block) {
        self.block(model);
    }
}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
//{
//
//    return 20;//上下间隙
//}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//
//    return 15;//左右间隙
//}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
