//
//  PersonalDetailsTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "PersonalDetailsTableViewCell.h"
@interface PersonalDetailsTableViewCell ()

/** 线*/
@property (nonatomic, strong)UIView *lineView;
@end
@implementation PersonalDetailsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.contentlabel];
    [self.contentView addSubview:self.lineView];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.contentlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(-15).priority(800);
        make.left.equalTo(self.titleLabel.mas_right).offset(20);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.titleLabel);
        make.right.equalTo(self.contentlabel);
        make.height.equalTo(@0.5);
    }];
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"身高";
        _titleLabel.textColor = RGBA(34, 34, 34, 1);
        _titleLabel.font = kFont_Medium(16);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UILabel *)contentlabel{
    if (!_contentlabel) {
        _contentlabel = [[UILabel alloc]init];
        _contentlabel.text = @"180cm";
        _contentlabel.textColor = RGBA(24, 24, 24, 1);
        _contentlabel.font = kFont_Medium(14);
        _contentlabel.textAlignment = 2;
        _contentlabel.numberOfLines = 0;
    }
    return _contentlabel;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
