//
//  PersonalDetailsTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonalDetailsTableViewCell : UITableViewCell
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 内容*/
@property (nonatomic, strong)UILabel *contentlabel;
@end

NS_ASSUME_NONNULL_END
