//
//  PersonalInformationImageInformationTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import <UIKit/UIKit.h>
#import "ThePersonalDataModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PersonalInformationImageInformationTableViewCell : UITableViewCell
/** 关注*/
@property (nonatomic, strong)UIButton *focusBtn;


@property (nonatomic, strong)ThePersonalDataModel *dataModel;
@end

NS_ASSUME_NONNULL_END
