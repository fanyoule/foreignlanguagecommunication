//
//  PersonalInformationBottomView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonalInformationBottomView : UIView
/** 私聊*/
@property (nonatomic, strong)UIButton *chatBtn;
/** 投票*/
@property (nonatomic, strong)UIButton *voteBtn;
@end

NS_ASSUME_NONNULL_END
