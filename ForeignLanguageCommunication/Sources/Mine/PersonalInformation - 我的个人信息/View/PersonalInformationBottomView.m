//
//  PersonalInformationBottomView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "PersonalInformationBottomView.h"

@interface PersonalInformationBottomView ()

@end

@implementation PersonalInformationBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
        self.backgroundColor = RGBA(255, 255, 255, 1);
        
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = RGBA(223, 223, 223, 1);
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
            make.height.equalTo(@0.5);
        }];
        
    }
    return self;
}

-(void)addControls{
//    UIView *backView = [[UIView alloc]init];
//    backView.backgroundColor = RGBA(255, 255, 255, 1);
//    [self addSubview:backView];
//    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.equalTo(self);
//        make.height.equalTo(@44);
//    }];
    
    [self addSubview:self.chatBtn];
    [self addSubview:self.voteBtn];
    [self.chatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self);
        make.right.equalTo(self.mas_centerX);
    }];
    [self.voteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(self);
        make.left.equalTo(self.mas_centerX);
    }];
    
}

- (UIButton *)chatBtn{
    if (!_chatBtn) {
        _chatBtn = [[UIButton alloc]init];
        [_chatBtn setImage:[UIImage imageNamed:@"个人资料-私聊"] forState:(UIControlStateNormal)];
        [_chatBtn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateNormal)];
        [_chatBtn setTitle:@"私聊" forState:(UIControlStateNormal)];
        _chatBtn.titleLabel.font = kFont_Medium(16);
        
    }
    return _chatBtn;
}

- (UIButton *)voteBtn{
    if (!_voteBtn) {
        _voteBtn = [[UIButton alloc]init];
        [_voteBtn setImage:[UIImage imageNamed:@"个人资料-未投票"] forState:(UIControlStateNormal)];
        [_voteBtn setImage:[UIImage imageNamed:@"个人资料-已投票"] forState:(UIControlStateSelected)];
        [_voteBtn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateNormal)];
        [_voteBtn setTitle:@"投票" forState:(UIControlStateNormal)];
        [_voteBtn setTitle:@"已投" forState:(UIControlStateSelected)];
        [_voteBtn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateSelected)];
        _voteBtn.titleLabel.font = kFont_Medium(16);
    }
    return _voteBtn;
}

@end
