//
//  MyPersonalInformationVC.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//  个人信息

#import "MyPersonalInformationVC.h"
#import "PersonalInformationBottomView.h"// 底部View

#import "PersonalInformationImageInformationTableViewCell.h"
#import "PersonalInformationAboutHisWorksTableViewCell.h"
#import "PersonalDetailsTableViewCell.h"
#import "PersonalInformationRulesTableViewCell.h"
//#import "ToReportViewController.h"// 举报
#import "ComplaintsViewController.h"// 举报

#import "ShareThePopupWindow.h"// 分享弹窗
#import <HWPopController/HWPop.h>

#import "MineDataPickerModel.h"//爱好model
#import "MyWorkViewController.h"// 他的作品
#import "NewsAllBuddyListModel.h"
#import "ChatDetailsVC.h"
#import "ChatDetailsGroupVC.h"
#import "WorldDetailViewController.h"

@interface MyPersonalInformationVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)PersonalInformationBottomView *bottonView;

@property (nonatomic, strong)NSMutableArray *hobbyArray;
@property (nonatomic, strong)ThePersonalDataModel *model;
@property (nonatomic, strong) UIButton *rightButton;
@end

@implementation MyPersonalInformationVC
- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleLabel.text = self.model.nickName;
    self.navView.backgroundColor = RGBA(255, 255, 255, 1);
    
    
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    UIButton *rightBtn = [[UIButton alloc]init];
    [rightBtn setImage:[UIImage imageNamed:@"个人资料-更多"] forState:(UIControlStateNormal)];
    [rightBtn addTarget:self action:@selector(moreAndMore) forControlEvents:(UIControlEventTouchDown)];
    [self.navView addSubview:rightBtn];
    self.rightButton = rightBtn;
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_offset(0);
        make.top.mas_offset(SBarHeight);
        make.right.equalTo(self.navView).offset(-15);
        make.width.equalTo(@25);
    }];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin-44);
    }];
    [self bottomTheView];
    // Do any additional setup after loading the view.
    [self getData];
}
- (void)getData {
    kWeakSelf(self)
    [RequestManager getUserById:[UserInfoManager shared].getUserID toUserId:self.ID withSuccess:^(id  _Nullable response) {
        NSDictionary *data = response[@"data"];
        weakself.model = [ThePersonalDataModel mj_objectWithKeyValues:data];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] postNotificationName:kPlayVideoNotification object:nil];
}
-(void)setModel:(ThePersonalDataModel *)model{
    _model = model;
    for (int i = 0; i < self.model.hobby.count; i++) {
        // 爱好
        MineDataPickerModel *pickerModel = [MineDataPickerModel mj_objectWithKeyValues:self.model.hobby[i]];
        [self.hobbyArray addObject:pickerModel.name];
    }

    [self.mainTableView reloadData];
    
    if (self.model.isMe) {
        self.rightButton.hidden = YES;
        self.bottonView.hidden = YES;
        [self.mainTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
        }];
    }else{
        self.rightButton.hidden = NO;
        self.bottonView.hidden = NO;
    }
    if (self.model.isVoted == YES) {
        self.bottonView.voteBtn.selected = YES;
    }else{
        self.bottonView.voteBtn.selected = NO;
    }
}

- (NSMutableArray *)hobbyArray{
    if (!_hobbyArray) {
        _hobbyArray = [[NSMutableArray alloc]init];
    }
    return _hobbyArray;
}

//MARK:更多按钮
-(void)moreAndMore{
    NSLog(@"点击了更多按钮");
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"取消拉黑" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [RequestManager blackUserWithId:[[UserInfoManager shared] getUserID] blackUserId:self.model.ID type:0 withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"已取消拉黑"];
                self.model.isBlack = NO;
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"拉黑" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [RequestManager blackUserWithId:[[UserInfoManager shared] getUserID] blackUserId:self.model.ID type:1 withSuccess:^(id  _Nullable response) {
            NSLog(@"拉黑 === %@",response);
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"已拉黑"];
                self.model.isBlack = YES;
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        ComplaintsViewController *vc = [[ComplaintsViewController alloc]init];
        vc.reportID = self.model.ID;
        vc.isJudge = 1;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"取消");
    }];
    if (self.model.isBlack == YES) {
        [actionSheet addAction:action4];
    }else{
        [actionSheet addAction:action1];
    }
    //把action添加到actionSheet里
    
    [actionSheet addAction:action2];
    [actionSheet addAction:action3];
    //相当于之前的[actionSheet show];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

//MARK:更多作品
-(void)moreWorks{
    
    MyWorkViewController *vc = [[MyWorkViewController alloc]init];
    vc.isJudge = 2;
    vc.dataModel = self.model;
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"点击了更多作品");
}

// MARK: 底部View
-(void)bottomTheView{
    
    [self.view addSubview:self.bottonView];
    [self.bottonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.equalTo(@(44+TabbarSafeMargin));
    }];
    
//    if (self.model.isVoted == YES) {
//        self.bottonView.voteBtn.selected = YES;
//    }else{
//        self.bottonView.voteBtn.selected = NO;
//    }
    
    [self.bottonView.chatBtn addTarget:self action:@selector(clickTheChatBtn:) forControlEvents:(UIControlEventTouchDown)];
    
    [self.bottonView.voteBtn addTarget:self action:@selector(clickTheVoteBtn:) forControlEvents:(UIControlEventTouchDown)];
    
    
}
// MARK:  私聊点击事件
-(void)clickTheChatBtn:(UIButton *)button {
//    NSMutableArray *marr = [[NSMutableArray alloc]initWithArray:self.navigationController.viewControllers];
//    for (UIViewController *vc in marr) {
//        if ([vc isKindOfClass:[ChatDetailsVC class]] || [vc isKindOfClass:[ChatDetailsGroupVC class]]) {
//            [marr removeObject:vc];
//            break;
//        }
//    }
//    self.navigationController.viewControllers = marr;
    NewsAllBuddyListModel *lisModel = [[NewsAllBuddyListModel alloc]init];
    lisModel.ID = self.model.ID;
    lisModel.nickname = self.model.nickName;
    ChatDetailsVC *vc = [[ChatDetailsVC alloc]init];
    vc.buddyModel = lisModel;
    [self.navigationController pushViewController:vc animated:NO];
}

// MARK:  投票点击事件
-(void)clickTheVoteBtn:(UIButton *)button{
    if (button.selected == NO) {
        NSLog(@"得票用户 === %ld",self.model.ID);
        NSLog(@"得票用户 === %ld",[[UserInfoManager shared] getUserID]);
        
        [RequestManager voteWithUserId:[[UserInfoManager shared] getUserID] toUserId:self.model.ID withSuccess:^(id  _Nullable response) {
            NSLog(@"投票 === %@",response);
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"投票成功"];
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"失败 === %@",error);
        }];
        button.selected = YES;
    }else{
        [self showText:@"已投票"];
        
        
    }
}

- (PersonalInformationBottomView *)bottonView{
    if (!_bottonView) {
        _bottonView = [[PersonalInformationBottomView alloc]init];
        
    }
    return _bottonView;
}

//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    UIView *view = [[UIView alloc]init];
//    UILabel *label = [[UILabel alloc]init];
//    label.text = @"请勿通过平台进行不法交易，如被举报核实将做封号处理";
//    label.textColor = RGBA(153, 153, 153, 1);
//    label.font = kFont(10);
//    label.textAlignment = 1;
//    [view addSubview:label];
//    [label mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(view).offset(15);
//        make.centerX.equalTo(view);
//    }];
//    return view;
//
//}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 50;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 8;
}
// cell 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 210;
    }else if (indexPath.row == 1){
        return 340;
    }else{
        return 50;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        PersonalInformationImageInformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalInformationImageInformationTableViewCell"];
            if (!cell) {
                cell = [[PersonalInformationImageInformationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonalInformationImageInformationTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.dataModel = self.model;
        if (self.model.isMe == YES) {
            cell.focusBtn.hidden = YES;
        }else{
            if (self.model.isFollow == YES) {
                cell.focusBtn.hidden = YES;
            }else{
                cell.focusBtn.hidden = NO;
            }
        }
        [cell.focusBtn addTarget:self action:@selector(clickFocus:) forControlEvents:(UIControlEventTouchDown)];
        return cell;
    }else if (indexPath.row == 1) {
        PersonalInformationAboutHisWorksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalInformationAboutHisWorksTableViewCell"];
            if (!cell) {
                cell = [[PersonalInformationAboutHisWorksTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonalInformationAboutHisWorksTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.arrowBtn addTarget:self action:@selector(moreWorks) forControlEvents:(UIControlEventTouchDown)];
        cell.backgroundColor = RGBA(245, 245, 245, 1);
        cell.dataModel = self.model;
        cell.block = ^(MyCollectionWorksModel * _Nonnull model) {
            WorldDetailViewController *vc = [[WorldDetailViewController alloc]init];
            vc.worksID = model.ID;
            [self.navigationController pushViewController:vc animated:YES];
        };
        return cell;
    }else if (indexPath.row == 7){
        
        PersonalInformationRulesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalInformationRulesTableViewCell"];
            if (!cell) {
                cell = [[PersonalInformationRulesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonalInformationRulesTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = RGBA(245, 245, 245, 1);
        return cell;
    }else{
        PersonalDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalDetailsTableViewCell"];
            if (!cell) {
                cell = [[PersonalDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonalDetailsTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = RGBA(255, 255, 255, 1);
        if (indexPath.row == 2) {
            if (self.model.height == 0) {
                cell.contentlabel.text = @"不显示";
            }else{
                cell.contentlabel.text = [NSString stringWithFormat:@"%ldcm",self.model.height];
            }
            
        }else if (indexPath.row == 3){
            cell.titleLabel.text = @"体重";
            if (self.model.weight == 0) {
                cell.contentlabel.text = @"不显示";
            }else{
                cell.contentlabel.text = [NSString stringWithFormat:@"%ldkg",self.model.weight];
            }
            
        }else if (indexPath.row == 4){
            cell.titleLabel.text = @"常驻城市";
            if (self.model.city.length <= 0) {
                cell.contentlabel.text =  @"无";
            }else{
                cell.contentlabel.text =  self.model.city;
            }
            
        }else if (indexPath.row == 5){
            cell.titleLabel.text = @"兴趣爱好";
            
            NSString *hobbyStr = [self.hobbyArray componentsJoinedByString:@"、"];
            if (hobbyStr.length <= 0) {
                cell.contentlabel.text =  @"无";
            }else{
                cell.contentlabel.text =  hobbyStr;
            }
            
        }else if (indexPath.row == 6){
            cell.titleLabel.text = @"个人介绍";
            if (self.model.introduce.length <= 0) {
                cell.contentlabel.text =  @"无";
            }else{
                cell.contentlabel.text =  self.model.introduce;
            }
            
        }
        
        return cell;
    }
    
}

-(void)clickFocus:(UIButton *)button{
    [RequestManager addFollowsMappingWithId:[[UserInfoManager shared] getUserID] followUserId:self.model.ID withSuccess:^(id  _Nullable response) {
        NSLog(@"关注 === %@",response);
        if ([response[@"status"] integerValue] == 200) {
            button.hidden = YES;
            self.model.isFollow = YES;
            if ([self.delegate respondsToSelector:@selector(clickMyPersonalInformationVC)]) {
                [self.delegate clickMyPersonalInformationVC];
            }
            [self.mainTableView reloadData];
        }
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

//#pragma mark - 底部View
//-(void)clikePersonalInformationBottomView:(NSInteger)tag{
//    // 500==视频  501==私聊  502==投票  503==语音
//    NSLog(@"tag值 === %ld",tag);
//    ShareThePopupWindow *vc = [ShareThePopupWindow new];
//    [vc popupWithPopType:HWPopTypeShrinkIn dismissType: HWDismissTypeNone];
//}



@end
