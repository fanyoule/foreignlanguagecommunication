//
//  ToReportViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "ToReportViewController.h"
#import "PersonalDataReportingCollectionViewCell.h"
@interface ToReportViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate>

@property(nonatomic, strong) UIView *bgView;
@property(nonatomic, strong) UICollectionView *collectionView;

@property(nonatomic, strong) NSMutableArray *totalGift;//

@property (nonatomic, strong)UITextView *textView;
@property(nonatomic, strong)UILabel *promptLabel;
@end

@implementation ToReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleLabel.text = @"举报";
    [self addRightBtnWith:@"提交"];
    self.view.backgroundColor = RGBA(245, 245, 245, 1);

    [self prepareSubviews];
    // Do any additional setup after loading the view.
}


- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.delegate = self;
        _textView.font = kFont_Medium(15);
        _textView.textContainerInset = UIEdgeInsetsMake(15, 10, 10, 10);
    }
    return _textView;
}
-(UILabel *)promptLabel{
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc]init];
        _promptLabel.numberOfLines = 0;
        _promptLabel.text = @"请输入举报原因";
        _promptLabel.font = kFont_Medium(15);
        _promptLabel.textColor = ColorRGB(154, 154, 154);
        _promptLabel.backgroundColor = [UIColor clearColor];
    }
    return _promptLabel;
}

- (UICollectionView *)collectionView
{
    
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor clearColor];
        
        [_collectionView registerClass:[PersonalDataReportingCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([PersonalDataReportingCollectionViewCell class])];
        
        _collectionView.showsHorizontalScrollIndicator=NO;
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.backgroundColor=[UIColor clearColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollEnabled = NO;
    }
    return _collectionView;
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 12;
        _bgView.layer.masksToBounds = YES;
    }
    return _bgView;
}

-(void)prepareSubviews{
    
    [self.view addSubview:self.bgView];
    [self.bgView addSubview:self.collectionView];
    [self.view addSubview:self.textView];
    [self.view addSubview:self.promptLabel];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.navView.mas_bottom).offset(10);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.equalTo(@400);
    }];
    
    
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView).offset(10);
        make.left.and.right.equalTo(self.bgView);
        make.height.equalTo(@200);
    }];
    [self.promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView).offset(15);
        make.left.equalTo(self.textView).offset(15);
    }];
    
      
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.top.equalTo(self.textView.mas_bottom);
    }];
    
}



- (NSMutableArray *)totalGift{
    if (!_totalGift) {
        _totalGift = [[NSMutableArray alloc]init];
    }
    return _totalGift;
}

#pragma mark - collection
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PersonalDataReportingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PersonalDataReportingCollectionViewCell class]) forIndexPath:indexPath];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    [self.bgView layoutIfNeeded];
    CGFloat w = self.bgView.size.width/3-20;
    return CGSizeMake(w, w);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 10, 10, 10);
}

-(void)textViewDidChange:(UITextView*)textView

{
    if([textView.text length] == 0){
      self.promptLabel.text = @"请输入举报原因";

    }else{
       self.promptLabel.text = @"";//这里给空

    }

    //计算剩余字数   不需要的也可不写

    NSString *nsTextCotent = textView.text;

    NSInteger existTextNum = [nsTextCotent length];

//     self.contentLabel.text = [NSString stringWithFormat:@"%ld/500",existTextNum];
    

}

//设置超出最大字数（140字）即不可输入 也是textview的代理方法
-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text{
    if ([text isEqualToString:@"\n"]) {     //这里"\n"对应的是键盘的 return 回收键盘之用
        
       [textView resignFirstResponder];

       return YES;

    }
    if (range.location >= 200){
     
            return NO;

    }else{
       return YES;

    }

}

@end
