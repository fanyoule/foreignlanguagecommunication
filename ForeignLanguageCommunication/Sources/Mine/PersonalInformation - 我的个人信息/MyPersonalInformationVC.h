//
//  MyPersonalInformationVC.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/4.
//

#import "BaseViewController.h"
#import "ThePersonalDataModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol UnionMyPersonalInformationVCDelegate <NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)clickMyPersonalInformationVC;

@end
@interface MyPersonalInformationVC : BaseViewController

@property(nonatomic, weak) id<UnionMyPersonalInformationVCDelegate> delegate;
@property (nonatomic, assign) long ID;

@end

NS_ASSUME_NONNULL_END
