//
//  MyPageModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyPageModel : NSObject
/** 收藏*/
@property (nonatomic, assign)NSInteger collect;
/** 粉丝*/
@property (nonatomic, assign)NSInteger fans;
/** 关注*/
@property (nonatomic, assign)NSInteger follow;
/** 头像*/
@property (nonatomic, strong)NSString *headUrl;
/** ID*/
@property (nonatomic, assign)NSInteger ID;
/** 是否代理*/
@property (nonatomic)BOOL isAgent;
/** isVIP*/
@property (nonatomic)BOOL isVIP;
/** 昵称*/
@property (nonatomic, strong)NSString *nickName;
/** 账号*/
@property (nonatomic, strong)NSString *sysId;

@end

NS_ASSUME_NONNULL_END
