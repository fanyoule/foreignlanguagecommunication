//
//  InstructorVerificationPriorCertificationModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/19.
//  教练认证前验证

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InstructorVerificationPriorCertificationModel : NSObject
/** 适应人群，关联标签id拼接*/
@property (nonatomic, strong)NSString *applyCrowd;
/** 适应人群*/
@property (nonatomic, strong)NSArray * applyList;
/** 简介*/
@property (nonatomic, strong)NSString * content;
/** 创建时间*/
@property (nonatomic, strong)NSString * createTime;
/** 语种*/
@property (nonatomic, strong)NSArray * languageList;
/** 语种*/
@property (nonatomic, strong)NSString * languages;
/** 教练姓名*/
@property (nonatomic, strong)NSString * name;
/** 手机*/
@property (nonatomic, strong)NSString * phone;
/** 真实姓名*/
@property (nonatomic, strong)NSString * realName;
/** 性别*/
@property (nonatomic, strong)NSString * sex;
/** 1:正常，2：封号*/
@property (nonatomic, assign)NSInteger  status;
/** 教学内容*/
@property (nonatomic, strong)NSArray * teachList;
/** 教学内容，关联标签id拼接*/
@property (nonatomic, strong)NSString * teachingContent;
/** 用户id  教练id */
@property (nonatomic, assign)NSInteger userId ;


@property (nonatomic)BOOL selected;

@end

NS_ASSUME_NONNULL_END
