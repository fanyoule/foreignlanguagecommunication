//
//  VipHeadView.h
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TheMemberCenterModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface VipHeadView : UIView
/** vipModel
 */
@property (nonatomic, strong) TheMemberCenterModel *model;
@end

NS_ASSUME_NONNULL_END
