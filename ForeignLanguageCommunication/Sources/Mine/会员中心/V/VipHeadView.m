//
//  VipHeadView.m
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "VipHeadView.h"

#import "GradientLabel.h"

@interface VipHeadView ()
@property (nonatomic, strong) UIImageView *avatarView;
@property (nonatomic, strong) UILabel *nickNameLabel;
@property (nonatomic, strong) UILabel *IDLabel;
@property (nonatomic, strong) UILabel *timeLabel;//到期时间

@property (nonatomic, strong) UILabel *current_growvaluelabel;//当前成长值
@property (nonatomic, strong) GradientLabel *currrent_levelLaebl;//当前等级
@property (nonatomic, strong) UILabel *next_levelLabel;//下一级
@property (nonatomic, strong) UILabel *needsGrowValueLabel;//需要成长值

@property (nonatomic, strong) UISlider *valueSlider;

@end

@implementation VipHeadView


- (instancetype)init {
    if (self = [super init]) {
        [self setUpSubView];
    }
    return self;
}


- (void)setUpSubView {
    UIImageView *blackImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vip_back"]];
    [self addSubview:blackImageView];
    blackImageView.frame = CGRectMake(0, 0, KSW, 209);
    
    UIImageView *topImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vip_top"]];
    [self addSubview:topImageView];
    [topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.right.mas_offset(-15);
        make.bottom.mas_equalTo(blackImageView.mas_bottom).mas_offset(TOP_SPACE20or44);
        make.height.mas_equalTo(141*(KSW-30)/345);
    }];
    
    self.avatarView = [[UIImageView alloc] init];
    [self addSubview:self.avatarView];
    self.avatarView.layer.cornerRadius = 6;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.backgroundColor = UIColor.orangeColor;
    [self.avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(47);
        make.left.mas_equalTo(topImageView.mas_left).mas_offset(10);
        make.top.mas_equalTo(topImageView.mas_top).mas_offset(18);
    }];
    
    
    
    self.nickNameLabel = UILabel.new;
    self.nickNameLabel.text = @"昵称";
    self.nickNameLabel.font = kFont_Bold(17);
    self.nickNameLabel.textColor = [UIColor colorWithHexString:@"#4C321C" alpha:1];
    [self addSubview:self.nickNameLabel];
    [self.nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.avatarView.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.avatarView).mas_offset(5);
    }];
    self.IDLabel = UILabel.new;
    self.IDLabel.text = @"ID:1973086";
    self.IDLabel.font = kFont_Regular(13);
    self.IDLabel.textColor = [UIColor colorWithHexString:@"#4C321C" alpha:1];
    [self addSubview:self.IDLabel];
    [self.IDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nickNameLabel);
        make.bottom.mas_equalTo(self.avatarView).mas_offset(-5);
    }];
    
    self.timeLabel = UILabel.new;
    self.timeLabel.font = kFont_Regular(13);
    self.timeLabel.textColor = [UIColor colorWithHexString:@"#4C321C" alpha:1];
    self.timeLabel.text = @"有效期至2020.05.27，请尽快续费，享受特权";
    [self addSubview:self.timeLabel];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.avatarView);
        make.bottom.mas_equalTo(topImageView).mas_offset(-25);
    }];
    
    
    
    UILabel *titleLabel = UILabel.new;
    titleLabel.text = @"会员特权";
    titleLabel.font = kFont_Bold(16);
    titleLabel.textColor = UIColor.blackColor;
    [self addSubview:titleLabel];
    [titleLabel sizeToFit];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.top.mas_equalTo(topImageView.mas_bottom).mas_offset(30);
        make.height.mas_equalTo(titleLabel.frame.size.height);
    }];
    
    CGFloat w = (KSW - 40)/2;
    
    NSArray *imageVArray = @[@"会员中心-匹配",@"会员中心-语音",@"会员中心-视频",@"会员中心-聊天"];
    NSArray *labelArray = @[@"匹配次数不限",@"语音次数不限",@"视频次数不限",@"聊天次数不限"];
    for (int i = 0; i < imageVArray.count; i++) {
        UIButton *btn = [[UIButton alloc]init];
        btn.backgroundColor = RGBA(245, 245, 245, 1);
        btn.layer.cornerRadius = 6;
        btn.clipsToBounds = YES;
        [self addSubview:btn];
        
        CGFloat col = i % 2;
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(titleLabel.mas_bottom).offset((i/2)*70+15);
            make.left.equalTo(self).offset(col*w+15+col*10);
            make.height.equalTo(@60);
            make.width.equalTo(@(w));
        }];
        
        UIImageView *imageV = [[UIImageView alloc]init];
        imageV.image = [UIImage imageNamed:imageVArray[i]];
        [btn addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(btn);
            make.left.equalTo(btn).offset(15);
        }];
        UILabel *label = [[UILabel alloc]init];
        label.text = labelArray[i];
        label.textColor = RGBA(24, 24, 24, 1);
        label.font = kFont_Medium(13);
        label.textAlignment = 1;
        [btn addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(btn);
            make.left.equalTo(imageV.mas_right).offset(15);
        }];
        
        
    }
    
}
- (void)setModel:(TheMemberCenterModel *)model{
    
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:[[UserInfoManager shared] getUserAvatar]]];
    self.nickNameLabel.text = [[UserInfoManager shared] getUserNickName];
    self.IDLabel.text = [NSString stringWithFormat:@"ID: %ld",[[UserInfoManager shared] sysId]];
    if (model.endTime == nil) {
        self.timeLabel.text = @"您还不是会员，开通可享受更多特权";
    }else{
        self.timeLabel.text = [NSString stringWithFormat:@"有效期至%@，请尽快续费，享受特权",model.endTime];
    }
    
}



- (void)clickMore:(UIButton *)sender {
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
