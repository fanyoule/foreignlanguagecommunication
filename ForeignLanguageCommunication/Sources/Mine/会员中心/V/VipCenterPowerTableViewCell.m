//
//  VipCenterPowerTableViewCell.m
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "VipCenterPowerTableViewCell.h"

#import "VipPowerIconView.h"

@interface VipCenterPowerTableViewCell ()
@property (nonatomic, strong) UIScrollView *myScrollView;
@end

@implementation VipCenterPowerTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSUbView];
    }
    return self;
}


- (void)setUpSUbView {
    
    UILabel *leftLabel = [[UILabel alloc] init];
    leftLabel.text = @"会员特权";
    [self.contentView addSubview:leftLabel];
    leftLabel.textColor = UIColor.blackColor;
    leftLabel.font = kFont_Bold(16);
    [leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.top.mas_offset(20);
    }];
    
    UIImageView *rightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_right_gray"]];
    [self.contentView addSubview:rightImageView];
    [rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(8, 14));
        make.right.mas_offset(-15);
        make.centerY.mas_equalTo(leftLabel);
    }];
    UILabel *rightLabel = [[UILabel alloc] init];
    [self.contentView addSubview:rightLabel];
    rightLabel.text = @"更多";
    rightLabel.font = kFont_Regular(13);
    rightLabel.textColor = [UIColor colorWithHexString:@"#222222" alpha:1];
    
    [rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(leftLabel);
        make.right.mas_equalTo(rightImageView.mas_left).mas_offset(-5);
    }];
    
    self.myScrollView = [[UIScrollView alloc] init];
    [self.contentView addSubview:self.myScrollView];
    
    [self.myScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(leftLabel.mas_bottom).mas_offset(10);
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(110);
    }];
    
    CGFloat contentW = 130 * 7 + 60 + 30;
    self.myScrollView.contentSize = CGSizeMake(contentW, 0);
    self.myScrollView.showsHorizontalScrollIndicator = NO;
    for (int i = 0; i < 7; i ++) {
        VipPowerIconView *iconView = [[VipPowerIconView alloc] initWithFrame:CGRectMake(15+(130+10)*i, 15, 130, 80) vipPowerType:i];
        iconView.backgroundColor = UIColor.whiteColor;
        [self.myScrollView addSubview:iconView];
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
