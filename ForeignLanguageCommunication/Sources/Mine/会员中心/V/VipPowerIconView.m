//
//  VipPowerIconView.m
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "VipPowerIconView.h"


@interface VipPowerIconView ()
@property (nonatomic, strong) UIImageView *iconView;//图标
@property (nonatomic, strong) UILabel *titleLabel;
@end
@implementation VipPowerIconView


- (instancetype)initWithFrame:(CGRect)frame vipPowerType:(VipPowerType)type {
    if (self = [super initWithFrame:frame]) {
        [self setUpSubViewWithType:type];
        [self layOutViews];
        self.layer.cornerRadius = 6;
        self.layer.shadowColor = [UIColor colorWithHexString:@"#000000" alpha:0.16].CGColor;
        self.layer.shadowOffset = CGSizeMake(0, 3);
        self.layer.shadowOpacity = 1;
        self.layer.shadowRadius = 15;
    }
    return self;
}
- (void)setUpSubViewWithType:(VipPowerType)type {
    
    self.iconView = [[UIImageView alloc] init];
    [self addSubview:self.iconView];
    self.titleLabel = [[UILabel alloc] init];
    [self addSubview:self.titleLabel];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor colorWithHexString:@"#222222" alpha:1];
    self.titleLabel.font = kFont_Regular(13);
    switch (type) {
        case VipPowerTypeAvatar:
            {
                self.titleLabel.text = @"会员专属头像框";
                self.iconView.image = [UIImage imageNamed:@"icon_vip_avatar"];
            }
            break;
        case VipPowerTypeVipEmoji:
        {
            self.titleLabel.text = @"解锁专属表情";
            self.iconView.image = [UIImage imageNamed:@"icon_emoji"];
        }
            break;
        case VipPowerTypeAddGrowValue:
        {
            self.titleLabel.text = @"个人成长值加速";
            self.iconView.image = [UIImage imageNamed:@"icon_grow"];
        }
            break;
        case VipPowerTypeHideToFriend: {
            self.titleLabel.text = @"对好友隐身";
            self.iconView.image = [UIImage imageNamed:@"icon_hide"];
        }
            break;
        case VipPowerTypeColorfulAccount: {
            self.titleLabel.text = @"账号颜色";
            self.iconView.image = [UIImage imageNamed:@"icon_account_color"];
        }
            break;
        case VipPowerTypeColorfulNickName: {
            self.titleLabel.text = @"获得彩色昵称";
            self.iconView.image = [UIImage imageNamed:@"icon_nick"];
        }
            break;
        case VipPowerTypeFriendAdressList: {
            self.titleLabel.text = @"好友通讯录";
            self.iconView.image = [UIImage imageNamed:@"icon_addresslist"];
        }
            break;
        default:
            break;
    }
}

- (void)layOutViews {
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.top.mas_offset(14);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconView.mas_bottom).mas_offset(10);
        make.centerX.mas_equalTo(self);
    }];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
