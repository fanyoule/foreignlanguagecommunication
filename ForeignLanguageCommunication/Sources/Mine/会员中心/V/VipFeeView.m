//
//  VipFeeView.m
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "VipFeeView.h"

@interface VipFeeView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *feeLabel;

@end

@implementation VipFeeView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUpSubView];
    
}

- (void)setUpSubView {
    
    self.layer.cornerRadius = 6;
    self.backgroundColor = UIColor.whiteColor;
    self.layer.borderColor = [UIColor colorWithHexString:@"#D7D7D7" alpha:1].CGColor;
    self.layer.borderWidth = 1;
    
}

- (void)setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    if (selected) {
        self.backgroundColor = [UIColor colorWithHexString:@"#FBF1E4" alpha:1];
        self.layer.borderColor = [UIColor colorWithHexString:@"#FBB751" alpha:1].CGColor;
    }else {
        self.backgroundColor = UIColor.whiteColor;
        self.layer.borderColor = [UIColor colorWithHexString:@"#D7D7D7" alpha:1].CGColor;
    }
}

- (void)setModel:(PrepaidPhoneListModel *)model{
    _model = model;
    NSString *feeStr = [NSString stringWithFormat:@"%ld",model.price];
    self.titleLabel.text = @"年费会员";
    self.timeLabel.text = [NSString stringWithFormat:@"%@个月",model.rechargeTime];
    
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] init];
    NSAttributedString *firstStr = [[NSAttributedString alloc] initWithString:feeStr attributes:@{NSFontAttributeName:kFont_Bold(20),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#FBB751" alpha:1]}];
    NSAttributedString *secondStr = [[NSAttributedString alloc] initWithString:@"元/月" attributes:@{NSFontAttributeName:kFont_Regular(14),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#222222" alpha:1]}];
    [attrStr appendAttributedString:firstStr];
    [attrStr appendAttributedString:secondStr];
    self.feeLabel.attributedText = attrStr;
}

- (void)setFeeType:(VipFeeType)feeType {
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
