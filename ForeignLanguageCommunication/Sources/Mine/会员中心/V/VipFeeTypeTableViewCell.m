//
//  VipFeeTypeTableViewCell.m
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "VipFeeTypeTableViewCell.h"
#import "VipFeeView.h"
#import "PrepaidPhoneListModel.h"


@interface VipFeeTypeTableViewCell ()
@property (weak, nonatomic) IBOutlet UIButton *buyVipBtn;
@property (nonatomic, strong) UIScrollView *myScrollview;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) VipFeeView *selectView;
@end

@implementation VipFeeTypeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.buyVipBtn.layer.cornerRadius = 22;
    self.myScrollview = [[UIScrollView alloc] init];
    [self.contentView addSubview:self.myScrollview];
    
    self.myScrollview.showsHorizontalScrollIndicator = NO;
    self.myScrollview.contentSize = CGSizeMake(120*4+60, 0);
    [self.myScrollview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(50);
        make.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(142);
    }];
}

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    [self setUpSUbView];
}

- (void)setUpSUbView {
    [self.myScrollview removeAllSubviews];
    
    for (int i = 0; i < self.dataArray.count; i ++) {
        PrepaidPhoneListModel *model = [PrepaidPhoneListModel mj_objectWithKeyValues:self.dataArray[i]];
        VipFeeView *feeView = [[NSBundle mainBundle] loadNibNamed:@"VipFeeView" owner:nil options:nil].firstObject;
        [self.myScrollview addSubview:feeView];
        feeView.tag = i;
        feeView.feeType = i;
        feeView.model = model;
        feeView.frame = CGRectMake(15+(120+10)*i, 1, 120, 140);
        if (i==0) {
            self.selectView = feeView;
            self.selectView.selected = YES;
        }
        [feeView addTarget:self action:@selector(clickFeeView:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)buyVip:(id)sender {
    if (!self.selectView) {
        return;
    }
    if (self.block) {
        self.block(self.selectView.tag);
    }
}

- (void)clickFeeView:(VipFeeView *)sender {
    sender.selected = !sender.isSelected;
    NSLog(@"dianjile111");
    if (self.selectView == sender) {
        return;
    }
    self.selectView.selected = NO;
    self.selectView = sender;
}

@end
