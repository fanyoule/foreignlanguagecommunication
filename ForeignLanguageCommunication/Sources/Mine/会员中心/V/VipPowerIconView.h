//
//  VipPowerIconView.h
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, VipPowerType) {
    VipPowerTypeColorfulNickName,//彩色昵称
    VipPowerTypeAddGrowValue,//成长值加速
    VipPowerTypeFriendAdressList,//好友通讯录
    VipPowerTypeHideToFriend,//对好友隐身
    VipPowerTypeVipEmoji,//专属表情
    VipPowerTypeAvatar,//专属头像框
    VipPowerTypeColorfulAccount,//账号颜色
    
};
@interface VipPowerIconView : UIView
@property (nonatomic, assign) VipPowerType vipType;

- (instancetype)initWithFrame:(CGRect)frame vipPowerType:(VipPowerType)type;

@end

NS_ASSUME_NONNULL_END
