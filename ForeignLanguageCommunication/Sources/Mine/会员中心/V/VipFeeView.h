//
//  VipFeeView.h
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrepaidPhoneListModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, VipFeeType) {
    VipFeeTypeYear,
    VipFeeTypeHalfYear,
    VipFeeTypeThreeMonth,
};
@interface VipFeeView : UIControl
@property (nonatomic, assign) VipFeeType feeType;

@property (nonatomic, strong)PrepaidPhoneListModel *model;
@end

NS_ASSUME_NONNULL_END
