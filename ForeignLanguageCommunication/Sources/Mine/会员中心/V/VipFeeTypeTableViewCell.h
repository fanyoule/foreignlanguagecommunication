//
//  VipFeeTypeTableViewCell.h
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^nameVipFeeTypeTableViewCellBlock)(NSInteger);
NS_ASSUME_NONNULL_BEGIN

@interface VipFeeTypeTableViewCell : UITableViewCell
@property (nonatomic, strong)NSMutableArray *dataArray;

@property (nonatomic, copy) nameVipFeeTypeTableViewCellBlock block;
@end

NS_ASSUME_NONNULL_END
