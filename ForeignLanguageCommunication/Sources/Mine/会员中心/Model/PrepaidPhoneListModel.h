//
//  PrepaidPhoneListModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PrepaidPhoneListModel : NSObject
/** 平台*/
@property (nonatomic, strong)NSString *platform;
/** 费用*/
@property (nonatomic, assign)NSInteger price;
/** 规格*/
@property (nonatomic, strong)NSString *rechargeTime;
/** id*/
@property (nonatomic, assign)NSInteger ID;







@end

NS_ASSUME_NONNULL_END
