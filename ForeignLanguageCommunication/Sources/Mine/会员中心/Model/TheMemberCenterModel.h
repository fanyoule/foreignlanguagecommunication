//
//  TheMemberCenterModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TheMemberCenterModel : NSObject
/** 用户头像*/
@property (nonatomic, strong)NSString *avatar;
/** 结束时间*/
@property (nonatomic, strong)NSString *endTime;
/** 用户昵称*/
@property (nonatomic, strong)NSString *nickname;
/** 开始时间*/
@property (nonatomic, strong)NSString *startTime;
/** 用户系统id*/
@property (nonatomic, strong)NSString *sysId;
/** 用户id*/
@property (nonatomic, assign)NSInteger userId;


@end

NS_ASSUME_NONNULL_END
