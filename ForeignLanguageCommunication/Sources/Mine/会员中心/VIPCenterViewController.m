//
//  VIPCenterViewController.m
//  VoiceLive
//
//  Created by mac on 2020/8/10.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "VIPCenterViewController.h"
#import "VipHeadView.h"
#import "VipCenterPowerTableViewCell.h"
#import "VipFeeTypeTableViewCell.h"
#import "TheMemberCenterModel.h"// 会员信息model
#import "PrepaidPhoneListModel.h"// 充值列表Model
#import <StoreKit/StoreKit.h>

@interface VIPCenterViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
SKPaymentTransactionObserver,
SKProductsRequestDelegate
>
@property (nonatomic, strong) VipHeadView *vipHeadView;
/** model */
@property (nonatomic, strong) NSMutableArray *modelArray;
/** model*/
@property (nonatomic, strong) TheMemberCenterModel *vModel;

@property (nonatomic, strong) NSString *applePayId;
@property (nonatomic, strong) NSArray *applePayIdArr;

@end

@implementation VIPCenterViewController
- (NSMutableArray *)modelArray {
    if (!_modelArray) {
        _modelArray = [[NSMutableArray alloc]init];
    }
    return _modelArray;
}
-(void)viewWillAppear:(BOOL)animated{
    [self requestVIPData];
    [self prepaidPhoneList];
    [self.view bringSubviewToFront:self.navView];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view bringSubviewToFront:self.navView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.applePayIdArr = @[@"com.sayEnglish.app.001", @"com.sayEnglish.app.002", @"com.sayEnglish.app.003", @"com.sayEnglish.app.004"];
    
    [self setUpSubView];
    [self setUpNavView];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    // 返回你所需要的状态栏样式
    return UIStatusBarStyleLightContent;
}
- (void)viewDidDisappear:(BOOL)animated {
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

- (void)setUpNavView {
    self.navTitleString = @"会员中心";
    [self.backBtn setImage:[UIImage imageNamed:@"icon_nav_back_white"] forState:UIControlStateNormal];
    self.navTitleLabel.textColor = UIColor.whiteColor;
    self.navView.backgroundColor = UIColor.clearColor;
}

- (void)setUpSubView {
    [self.view addSubview:self.mainTableView];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.frame = self.view.bounds;
    self.mainTableView.scrollEnabled = YES;
    [self.mainTableView registerNib:[UINib nibWithNibName:@"VipFeeTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"VipFeeTypeTableViewCell"];
    self.vipHeadView = [[VipHeadView alloc]init];
    self.vipHeadView.model = self.vModel;
    
    self.mainTableView.tableHeaderView = self.vipHeadView;
    self.vipHeadView.frame = CGRectMake(0, 0, KSW, 460);
}

#pragma mark -delegate datasource-
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VipFeeTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VipFeeTypeTableViewCell"];
    cell.dataArray  = self.modelArray;
    kWeakSelf(self)
    cell.block = ^(NSInteger index) {
        [weakself pay:index];
    };
    return cell;
}
- (void)pay:(NSInteger)index {
    PrepaidPhoneListModel *model = self.modelArray[index];
    self.applePayId = self.applePayIdArr[index];
    [self applePay];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = UIView.new;
    view.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5" alpha:1];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300;
}
#pragma mark - 获取vip数据
- (void)requestVIPData {
    [RequestManager VIPInfo:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"会员中心 === %@",response);
        NSDictionary *dic = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            self.vModel = [TheMemberCenterModel mj_objectWithKeyValues:dic];
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
#pragma mark - 充值列表
- (void)prepaidPhoneList{
    [RequestManager VIPRechargeList:@"IOS" withSuccess:^(id  _Nullable response) {
        NSLog(@"充值列表 ===  %@",response);
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue ]== 200) {
            for (int i = 0; i < array.count; i++) {
                PrepaidPhoneListModel *model = [PrepaidPhoneListModel mj_objectWithKeyValues:array[i]];
                [self.modelArray addObject:model];
            }
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

- (TheMemberCenterModel *)vModel{
    if (!_vModel) {
        _vModel = [[TheMemberCenterModel alloc]init];
    }
    return _vModel;
}
- (void)applePay {
    if ([SKPaymentQueue canMakePayments]) {
        //监听购买结果
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [self requestProductData];
    } else {
        NSLog(@"不允许程序内付费");
    }
}
//去苹果服务器请求商品
- (void)requestProductData {
    NSArray *product = [[NSArray alloc] initWithObjects:self.applePayId, nil];
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:nsset];
    request.delegate = self;
    [request start];
}
//收到产品返回信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"--------------收到产品反馈消息---------------------");
    NSArray *product = response.products;
    NSLog(@"productID:%@", response.invalidProductIdentifiers);
    if(product.count == 0){
        [self showError:@"查找不到商品信息"];
        return;
    }
    [SVProgressHUD showWithStatus:@"支付中"];
    SKProduct *p = nil;
    for(SKProduct *pro in product) {
        NSLog(@"%@", [pro description]);
        NSLog(@"%@", [pro localizedTitle]);
        NSLog(@"%@", [pro localizedDescription]);
        NSLog(@"%@", [pro price]);
        NSLog(@"%@", [pro productIdentifier]);
        
        if([pro.productIdentifier isEqualToString: self.applePayId]){
            p = pro;
        }
    }
    if (p==nil) {
        [self showError:@"支付失败"];
        NSLog(@"支付调取失败");
        return;
    }
    SKPayment *payment = [SKPayment paymentWithProduct:p];
    NSLog(@"发送购买请求");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
  
}
//请求失败
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self showError:@"支付失败"];
}

- (void)requestDidFinish:(SKRequest *)request {
    [SVProgressHUD dismiss];
}

//监听购买结果
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transaction{
    [SVProgressHUD dismiss];
    for(SKPaymentTransaction *tran in transaction){
       
        switch(tran.transactionState) {
            case SKPaymentTransactionStatePurchased:{
                [self verifyPurchaseWithPaymentTransaction:tran];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"buyed"];
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStatePurchasing:
                break;
            case SKPaymentTransactionStateRestored:{
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStateFailed:{
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
                [self showError:@"购买失败"];
            }
                break;
            default:
                break;
        }
    }
}

//交易结束
- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"交易结束");
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)dealloc {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)verifyPurchaseWithPaymentTransaction:(SKPaymentTransaction *)transaction {
    [SVProgressHUD dismiss];
    // 验证凭据，获取到苹果返回的交易凭据
    // appStoreReceiptURL iOS7.0增加的，购买交易完成后，会将凭据存放在该地址
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    // 从沙盒中获取到购买凭据
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    //向自己的服务器验证购买凭证
    //最好将返回的数据转换成 base64再传给后台,后台再转换回来;以防返回字符串中有特字符传给后台显示空
    NSString *resu=[[NSString alloc]initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
    NSString *environment = [self environmentForReceipt:resu];
    
    // 发送网络POST请求，对购买凭据进行验证
    //测试验证地址:https://sandbox.itunes.apple.com/verifyReceipt
    //正式验证地址:https://buy.itunes.apple.com/verifyReceipt
    NSURL *url = [NSURL URLWithString:AppStore];
    if ([environment isEqualToString:@"environment=Sandbox"]) {
        url = [NSURL URLWithString:SANDBOX];
    }
    NSMutableURLRequest *urlRequest =
    [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f];
    urlRequest.HTTPMethod = @"POST";
    NSString *encodeStr = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    NSString *payload = [NSString stringWithFormat:@"{\"receipt-data\" : \"%@\"}", encodeStr];
    NSData *payloadData = [payload dataUsingEncoding:NSUTF8StringEncoding];
    urlRequest.HTTPBody = payloadData;
    // 提交验证请求，并获得官方的验证JSON结果 iOS9后更改了另外的一个方法
    NSData *result = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error:nil];
    // 官方验证结果为空
    if (result == nil) {
        [self showError:@"购买失败"];
        return;
    }
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingAllowFragments error:nil];
    if (dict != nil) {
        // 比对字典中以下信息基本上可以保证数据安全
        NSLog(@"字典%@",dict);
        NSLog(@"验证成功！购买的商品是：%@  %@", [dict objectForKey:@"product_id"], [dict objectForKey:@"productName"]);
        NSArray *array = dict[@"receipt"][@"in_app"];
//        NSDictionary *inAppDict = [array lastObject];
        [self showText:@"购买成功"];
        dispatch_async(dispatch_get_main_queue(), ^{
//            [g_server iOSAsynChronous:self.resultObject recepit:encodeStr toView:self];
        });
        
    } else {
        [self showError:@"购买失败"];
    }
}
//收据的环境判断；
- (NSString *)environmentForReceipt:(NSString * )str {
    str= [str stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    str=[str stringByReplacingOccurrencesOfString:@" " withString:@""];
    str=[str stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSArray * arr=[str componentsSeparatedByString:@";"];
    //存储收据环境的变量
    NSString * environment=arr[2];
    return environment;
}
@end
