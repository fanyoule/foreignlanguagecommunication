//
//  RefundViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "RefundViewController.h"
#import "MyCourseIsVersatileCollectionViewCell.h"
#import "DetailsTheRefundViewController.h"
#import "MyOfCourseModel.h"
#import "ChatDetailsVC.h"
@interface RefundViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) NSMutableDictionary *cellDic;


@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation RefundViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(30);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    self.cellDic = [[NSMutableDictionary alloc]init];
    
    [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class] forCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell"];
    
    // MARK: 下拉刷新
        self.mainCollectionView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
}
// MARK: 下拉刷新回调
-(void)loadNewData{
    [self getData];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getData];
}

-(void)getData{
    kWeakSelf(self)
    [self.dataArray removeAllObjects];
    if (self.isJudge == 1) {// 我的课程
        [RequestManager myCourseWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
            NSLog(@"我的课程 === %@",response);
            NSArray *array = response[@"data"];
            if ([response[@"status"] integerValue] == 200) {
                for (int i = 0; i < array.count; i++) {
                    MyOfCourseModel *model = [MyOfCourseModel mj_objectWithKeyValues:array[i]];
                    if ([model.status isEqualToString:@"已上完"]) {
                        
                    }else if ([model.status isEqualToString:@"待上课"]){
                        
                    }else{
                        [self.dataArray addObject:model];
                    }
                }
            }
            [self.mainCollectionView reloadData];
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        } withFail:^(NSError * _Nullable error) {
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        }];
    }else{// 课程管理
        [RequestManager myCourseOrderWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
            NSLog(@"课程管理 === %@",response);
            NSArray *array = response[@"data"];
            if ([response[@"status"] integerValue] == 200) {
                for (int i = 0; i < array.count; i++) {
                    MyOfCourseModel *model = [MyOfCourseModel mj_objectWithKeyValues:array[i]];
                    if ([model.status isEqualToString:@"已上完"]||[model.status isEqualToString:@"待确认退款"]||[model.status isEqualToString:@"已同意"]||[model.status isEqualToString:@"已失效"]) {
                        [self.dataArray addObject:model];
                    }
                }
            }
            [self.mainCollectionView reloadData];
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        } withFail:^(NSError * _Nullable error) {
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        }];
    }
    
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = [self.cellDic objectForKey:[NSString stringWithFormat:@"%@", indexPath]];
    if (identifier == nil) {
           identifier = [NSString stringWithFormat:@"%@%@", @"MyCourseIsVersatileCollectionViewCell", [NSString stringWithFormat:@"%@", indexPath]];
           [_cellDic setValue:identifier forKey:[NSString stringWithFormat:@"%@", indexPath]];
           // 注册Cell
           [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class]  forCellWithReuseIdentifier:identifier];
       }
    MyCourseIsVersatileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    
    MyOfCourseModel *model = [[MyOfCourseModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    if (self.isJudge == 1) {// 我的课程
        [cell.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.userUrl]];
        cell.nameLabel.text = model.coachName;
        if (model.refund == 3) {
            cell.classLabel.text = @"已同意";
            [cell.blueBtn setTitle:@"私信" forState:(UIControlStateNormal)];
            
            [cell.blueBtn whenTapped:^{
                [self openChat:model];
            }];
            cell.grayBtn.hidden = YES;
        }else{
            cell.classLabel.text = @"待确认退款";
            [cell.blueBtn setTitle:@"取消退款" forState:(UIControlStateNormal)];
            [cell.grayBtn setTitle:@"私信" forState:(UIControlStateNormal)];
            
            [cell.grayBtn whenTapped:^{
                [self openChat:model];
            }];
            [cell.blueBtn whenTapped:^{
                [self clickCancel:model.ID];
            }];
        }
    }else{// 课程管理
        [cell.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.userUrl]];
        cell.nameLabel.text = model.nickname;
        cell.classLabel.text = model.status;
        if ([model.status isEqualToString:@"已同意"]||[model.status isEqualToString:@"已失效"]) {
            cell.grayBtn.hidden = YES;
            [cell.blueBtn setTitle:@"私信" forState:(UIControlStateNormal)];
            [cell.blueBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
            cell.blueBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
            
            [cell.blueBtn whenTapped:^{
                [self openChat:model];
            }];
            
        }else if ([model.status isEqualToString:@"待确认退款"]){
            [cell.grayBtn setTitle:@"私信" forState:(UIControlStateNormal)];
            [cell.blueBtn setTitle:@"同意" forState:(UIControlStateNormal)];
            
            [cell.grayBtn whenTapped:^{
                [self openChat:model];
            }];
            [cell.blueBtn whenTapped:^{
                NSLog(@"点击了同意");
                [RequestManager agreeRefundWithOrderId:model.ID withSuccess:^(id  _Nullable response) {
                    if ([response[@"status"] integerValue] == 200) {
                        model.status = @"已同意";
                        [self.mainCollectionView reloadData];
                    }else{
                        [self showText:response[@"msg"]];
                    }
                }withFail:^(NSError * _Nullable error) {
                    
                }];
            }];
        }
    }
    return cell;
}
- (void)openChat:(MyOfCourseModel *)model {
    long userId = self.isJudge == 1 ? model.coachId : model.userId;
    if (userId == [UserInfoManager shared].getUserID) {
        [self showText:@"不能和自己聊天"];
        return;
    }
    NSString *name = self.isJudge == 1 ? model.coachName : model.nickname;
    NewsAllBuddyListModel *lisModel = [[NewsAllBuddyListModel alloc]init];
    lisModel.ID = userId;
    lisModel.nickname = name;
    ChatDetailsVC *vc = [[ChatDetailsVC alloc]init];
    vc.buddyModel = lisModel;
    [self.navigationController pushViewController:vc animated:NO];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    return CGSizeMake(KSW-30, 198);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MyOfCourseModel *model = [[MyOfCourseModel alloc]init];
    model = self.dataArray[indexPath.row];
    DetailsTheRefundViewController *vc = [[DetailsTheRefundViewController alloc]init];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)clickMessages:(NSInteger)orderId{
    //私信
}

-(void)clickCancel:(NSInteger)orderId{
    // 取消退款
    NSLog(@"订单id == %ld",orderId);
    [RequestManager cancelRefundWithOrderId:orderId withSuccess:^(id  _Nullable response) {
        if ([response[@"status"] integerValue] == 200) {
            [self getData];
        }else{
            [self showText:response[@"msg"]];
        }
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

@end
