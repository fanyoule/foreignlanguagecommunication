//
//  MyCourseIsVersatileCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "MyCourseIsVersatileCollectionViewCell.h"

@interface MyCourseIsVersatileCollectionViewCell ()

/** 封面*/
@property (nonatomic, strong)UIImageView *coverImageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 价格图标*/
@property (nonatomic, strong)UILabel *priceIcon;
/** 价格*/
@property (nonatomic, strong)UILabel *priceLabel;
/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;

@end

@implementation MyCourseIsVersatileCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.coverImageV];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.priceIcon];
    [self.contentView addSubview:self.priceLabel];
    [self.contentView addSubview:self.rulesLabel];
    [self.contentView addSubview:self.classLabel];
    [self.contentView addSubview:self.grayBtn];
    [self.contentView addSubview:self.blueBtn];
    
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@25);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.portraitImageV);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    [self.classLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.portraitImageV);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.coverImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV.mas_bottom).offset(15);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@80);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.coverImageV);
        make.left.equalTo(self.coverImageV.mas_right).offset(10);
        make.right.equalTo(self.contentView).offset(-20);
    }];
    [self.priceIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverImageV.mas_right).offset(10);
        make.bottom.equalTo(self.coverImageV).offset(-5);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceIcon.mas_right);
        make.bottom.equalTo(self.priceIcon).offset(5);
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceLabel.mas_right).offset(5);
        make.bottom.equalTo(self.priceIcon);
    }];
    [self.blueBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(-20);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
    [self.grayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.blueBtn.mas_left).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-20);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
}

- (void)setCourseModel:(MyCourseCollectionModel *)courseModel{
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:courseModel.avatarUrl]];
    self.nameLabel.text = courseModel.coachName;
    [self.coverImageV sd_setImageWithURL:[NSURL URLWithString:courseModel.courseUrl]];
    self.titleLabel.text = courseModel.courseName;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld",courseModel.price];
    
    self.rulesLabel.text = [NSString stringWithFormat:@"/%ld分钟/一节课",courseModel.time];
    
}

- (void)setModel:(MyOfCourseModel *)model{
    _model = model;
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.userUrl]];
    if (model.nickname.length <= 0) {
        self.nameLabel.text = model.coachName;
    }else{
        self.nameLabel.text = model.nickname;
    }
    
    
    
    [self.coverImageV sd_setImageWithURL:[NSURL URLWithString:model.coursePic]];
    self.titleLabel.text = model.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld",model.price];
    self.rulesLabel.text = [NSString stringWithFormat:@"/%ld分钟/一节课",model.time];
}


- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像2"];
        _portraitImageV.layer.cornerRadius = 3;
        _portraitImageV.clipsToBounds = YES;
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _portraitImageV;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Medium(14);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}
- (UIImageView *)coverImageV{
    if (!_coverImageV) {
        _coverImageV = [[UIImageView alloc]init];
        _coverImageV.image = [UIImage imageNamed:@"测试头像2"];
        _coverImageV.layer.cornerRadius = 6;
        _coverImageV.clipsToBounds = YES;
        _coverImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _coverImageV;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"青少年课程口语1小时练习青少年课程口语1小时练习青少年课程口语...小时练习";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(14);
        _titleLabel.textAlignment = 0;
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}
- (UILabel *)priceIcon{
    if (!_priceIcon) {
        _priceIcon = [[UILabel alloc]init];
        _priceIcon.text = @"¥";
        _priceIcon.textColor = RGBA(52, 120, 245, 1);
        _priceIcon.font = kFont_Medium(11);
        _priceIcon.textAlignment = 0;
    }
    return _priceIcon;
}
- (UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.text = @"600";
        _priceLabel.textColor = RGBA(52, 120, 245, 1);
        _priceLabel.font = kFont_Bold(24);
        _priceLabel.textAlignment = 0;
    }
    return _priceLabel;
}
- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"/90分钟/一节课";
        _rulesLabel.textColor = RGBA(52, 120, 245, 1);
        _rulesLabel.font = kFont_Medium(11);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}
- (UILabel *)classLabel{
    if (!_classLabel) {
        _classLabel = [[UILabel alloc]init];
        _classLabel.text = @"待上课";
        _classLabel.textColor = RGBA(52, 120, 245, 1);
        _classLabel.font = kFont_Medium(14);
        _classLabel.textAlignment = 2;
    }
    return _classLabel;
}
- (UIButton *)grayBtn{
    if (!_grayBtn) {
        _grayBtn = [[UIButton alloc]init];
        [_grayBtn setTitle:@"退款" forState:(UIControlStateNormal)];
        [_grayBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        _grayBtn.layer.cornerRadius = 6;
        _grayBtn.clipsToBounds = YES;
        _grayBtn.layer.borderWidth = 0.5;
        _grayBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        _grayBtn.titleLabel.font = kFont_Medium(14);
    }
    return _grayBtn;
}
- (UIButton *)blueBtn{
    if (!_blueBtn) {
        _blueBtn = [[UIButton alloc]init];
        [_blueBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [_blueBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _blueBtn.layer.cornerRadius = 6;
        _blueBtn.clipsToBounds = YES;
        _blueBtn.layer.borderWidth = 0.5;
        _blueBtn.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
        _blueBtn.titleLabel.font = kFont_Medium(14);
    }
    return _blueBtn;
}



@end
