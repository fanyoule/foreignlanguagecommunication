//
//  ReasonsApplyingRefundCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import <UIKit/UIKit.h>
#import "MyOfCourseModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol ReasonsApplyingRefundCollectionViewCell<NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)clickReasonsApplyingRefundCollectionViewCell:(NSString *)string;

@end
@interface ReasonsApplyingRefundCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong)MyOfCourseModel *model;

@property (nonatomic, weak) id<ReasonsApplyingRefundCollectionViewCell> cellDelegate;
@end

NS_ASSUME_NONNULL_END
