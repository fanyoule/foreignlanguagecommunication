//
//  TheOrderInformationHasBeenPlacedCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "TheOrderInformationHasBeenPlacedCollectionViewCell.h"
@interface TheOrderInformationHasBeenPlacedCollectionViewCell ()
@property(nonatomic, strong)UIView *blueView;
/** 订单信息*/
@property(nonatomic, strong)UILabel *informationLabel;



/** 课程费用*/
@property(nonatomic, strong)UILabel *courseLabel;
/** 课程费用钱数*/
@property(nonatomic, strong)UILabel *costLabel;

/** 开始时间*/
@property (nonatomic, strong)UILabel *startLabel;
/** 开始时间  时间*/
@property (nonatomic, strong)UILabel *startTimeLabel;
/** 结束时间*/
@property (nonatomic, strong)UILabel *terminationLabel;
/** 结束时间  时间*/
@property (nonatomic, strong)UILabel *terminationTimeLabel;

/** 创建时间*/
@property(nonatomic, strong)UILabel *createLabel;
/** 创建时间 时间*/
@property(nonatomic, strong)UILabel *timeLabel;
/** 订单编号*/
@property(nonatomic, strong)UILabel *orderLabel;


@end
@implementation TheOrderInformationHasBeenPlacedCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.blueView];
    [self.contentView addSubview:self.informationLabel];
    [self.contentView addSubview:self.courseLabel];
    [self.contentView addSubview:self.costLabel];
    [self.contentView addSubview:self.startLabel];
    [self.contentView addSubview:self.startTimeLabel];
    [self.contentView addSubview:self.terminationLabel];
    [self.contentView addSubview:self.terminationTimeLabel];
    [self.contentView addSubview:self.createLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.orderLabel];
    [self.contentView addSubview:self.numberLabel];
    [self.contentView addSubview:self.pasteBtn];
    [self.blueView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(21);
        make.left.equalTo(self.contentView).offset(15);
        make.width.equalTo(@5);
        make.height.equalTo(@14);
    }];
    [self.informationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.blueView);
        make.left.equalTo(self.blueView.mas_right).offset(5);
    }];
    [self.courseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.blueView.mas_bottom).offset(15);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.costLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.courseLabel);
        make.left.equalTo(self.contentView).offset(116);
    }];
    [self.startLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.courseLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.startTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.startLabel);
        make.left.equalTo(self.contentView).offset(116);
    }];
    [self.terminationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.startLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.terminationTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.terminationLabel);
        make.left.equalTo(self.contentView).offset(116);
    }];
    
    [self.createLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.terminationLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.createLabel);
        make.left.equalTo(self.contentView).offset(116);
    }];
    
    [self.orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.createLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.orderLabel);
        make.left.equalTo(self.contentView).offset(116);
    }];
    [self.pasteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.orderLabel);
        make.right.equalTo(self.contentView).offset(-15);
    }];
}

- (void)setModel:(MyOfCourseModel *)model{
    self.costLabel.text = [NSString stringWithFormat:@"%ld元",model.price];
    self.timeLabel.text = model.createTime;
    self.numberLabel.text = model.orderNum;
    self.startTimeLabel.text = model.startTime;
    self.terminationTimeLabel.text = model.endTime;
}

- (UIView *)blueView{
    if (!_blueView) {
        _blueView = [[UIView alloc]init];
        _blueView.backgroundColor = RGBA(52, 120, 245, 1);
        _blueView.layer.cornerRadius = 2.5;
        _blueView.clipsToBounds = YES;
    }
    return _blueView;
}
- (UILabel *)informationLabel{
    if (!_informationLabel) {
        _informationLabel = [[UILabel alloc]init];
        _informationLabel.text = @"订单信息";
        _informationLabel.textColor = RGBA(24, 24, 24, 1);
        _informationLabel.font = kFont_Bold(16);
        _informationLabel.textAlignment = 0;
    }
    return _informationLabel;
}
- (UILabel *)courseLabel{
    if (!_courseLabel) {
        _courseLabel = [[UILabel alloc]init];
        _courseLabel.text = @"课程费用：";
        _courseLabel.textColor = RGBA(24, 24, 24, 1);
        _courseLabel.font = kFont_Medium(13);
        _courseLabel.textAlignment = 0;
    }
    return _courseLabel;
}
- (UILabel *)costLabel{
    if (!_costLabel) {
        _costLabel = [[UILabel alloc]init];
        _costLabel.text = @"1000元";
        _costLabel.textColor = RGBA(24, 24, 24, 1);
        _costLabel.font = kFont_Medium(13);
        _costLabel.textAlignment = 0;
    }
    return _costLabel;
}
- (UILabel *)createLabel{
    if (!_createLabel) {
        _createLabel = [[UILabel alloc]init];
        _createLabel.text = @"创建时间：";
        _createLabel.textColor = RGBA(24, 24, 24, 1);
        _createLabel.font = kFont_Medium(13);
        _createLabel.textAlignment = 0;
    }
    return _createLabel;
}
- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.text = @"2020-05-10   10:00";
        _timeLabel.textColor = RGBA(24, 24, 24, 1);
        _timeLabel.font = kFont_Medium(13);
        _timeLabel.textAlignment = 0;
    }
    return _timeLabel;
}
- (UILabel *)orderLabel{
    if (!_orderLabel) {
        _orderLabel = [[UILabel alloc]init];
        _orderLabel.text = @"订单编号：";
        _orderLabel.textColor = RGBA(24, 24, 24, 1);
        _orderLabel.font = kFont_Medium(13);
        _orderLabel.textAlignment = 0;
    }
    return _orderLabel;
}
- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.text = @"35468514616";
        _numberLabel.textColor = RGBA(24, 24, 24, 1);
        _numberLabel.font = kFont_Medium(13);
        _numberLabel.textAlignment = 0;
    }
    return _numberLabel;
}
- (UIButton *)pasteBtn{
    if (!_pasteBtn) {
        _pasteBtn = [[UIButton alloc]init];
        [_pasteBtn setTitle:@"复制" forState:(UIControlStateNormal)];
        [_pasteBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _pasteBtn.titleLabel.font = kFont_Medium(13);
    }
    return _pasteBtn;
}
- (UILabel *)startLabel{
    if (!_startLabel) {
        _startLabel = [[UILabel alloc]init];
        _startLabel.text = @"开始时间";
        _startLabel.textColor = RGBA(24, 24, 24, 1);
        _startLabel.font = kFont_Medium(13);
        _startLabel.textAlignment = 0;
    }
    return _startLabel;
}
- (UILabel *)startTimeLabel{
    if (!_startTimeLabel) {
        _startTimeLabel = [[UILabel alloc]init];
        _startTimeLabel.text = @"2020-05-10    10:00";
        _startTimeLabel.textColor = RGBA(24, 24, 24, 1);
        _startTimeLabel.font = kFont_Medium(13);
        _startTimeLabel.textAlignment = 0;
        
    }
    return _startTimeLabel;
}

- (UILabel *)terminationLabel{
    if (!_terminationLabel) {
        _terminationLabel = [[UILabel alloc]init];
        _terminationLabel.text = @"结束时间";
        _terminationLabel.textColor = RGBA(24, 24, 24, 1);
        _terminationLabel.font = kFont_Medium(13);
        _terminationLabel.textAlignment = 0;
    }
    return _terminationLabel;
}
- (UILabel *)terminationTimeLabel{
    if (!_terminationTimeLabel) {
        _terminationTimeLabel = [[UILabel alloc]init];
        _terminationTimeLabel.text = @"2020-05-10    10:00";
        _terminationTimeLabel.textColor = RGBA(24, 24, 24, 1);
        _terminationTimeLabel.font = kFont_Medium(13);
        _terminationTimeLabel.textAlignment = 0;
    }
    return _terminationTimeLabel;
}
@end
