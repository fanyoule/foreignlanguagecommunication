//
//  ReasonsApplyingRefundCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "ReasonsApplyingRefundCollectionViewCell.h"

@interface ReasonsApplyingRefundCollectionViewCell ()<UITextViewDelegate>
/** 退款金额*/
@property (nonatomic, strong)UILabel *refundLabel;
/** 金额*/
@property (nonatomic ,strong)UILabel *amountLabel;
/** 退款原因*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 退款内容*/
@property (nonatomic, strong)UITextView *textView;
/** 提示语*/
@property (nonatomic, strong)UILabel *promptLabel;
@end

@implementation ReasonsApplyingRefundCollectionViewCell


-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.refundLabel];
    [self.contentView addSubview:self.amountLabel];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.textView];
    [self.textView addSubview:self.promptLabel];
    [self.refundLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.refundLabel);
        make.left.equalTo(self.refundLabel.mas_right).offset(15);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.refundLabel.mas_bottom).offset(20);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(0);
        make.left.equalTo(self.contentView).offset(5);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-5);
    }];
    [self.promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView).offset(10);
        make.left.equalTo(self.textView).offset(10);
        make.right.equalTo(self.textView).offset(-10);
    }];
    
}
- (void)setModel:(MyOfCourseModel *)model{
    self.amountLabel.text = [NSString stringWithFormat:@"¥%ld",model.price];
}

- (UILabel *)refundLabel{
    if (!_refundLabel) {
        _refundLabel = [[UILabel alloc]init];
        _refundLabel.text = @"退款金额：";
        _refundLabel.textColor = RGBA(24, 24, 24, 1);
        _refundLabel.font = kFont_Medium(15);
        _refundLabel.textAlignment = 0;
    }
    return _refundLabel;
}

- (UILabel *)amountLabel{
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc]init];
        _amountLabel.text = @"¥600.00";
        _amountLabel.textColor = RGBA(52, 120, 245, 1);
        _amountLabel.font = kFont_Medium(15);
        _amountLabel.textAlignment = 0;
    }
    return _amountLabel;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"退款原因：";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.delegate = self;
        _textView.font = kFont_Medium(15);
        _textView.textContainerInset = UIEdgeInsetsMake(10, 8, 10, 10);
    }
    return _textView;
}

- (UILabel *)promptLabel{
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc]init];
        _promptLabel.text = @"填写退款愿意，仅限100字";
        _promptLabel.textColor = RGBA(153, 153, 153, 1);
        _promptLabel.font = kFont_Medium(15);
        _promptLabel.textAlignment = 0;
    }
    return _promptLabel;
}

-(void)textViewDidChange:(UITextView*)textView
{
    if([textView.text length] == 0){
      self.promptLabel.text = @"填写退款愿意，仅限100字";

    }else{
       self.promptLabel.text = @"";//这里给空

    }

    //计算剩余字数   不需要的也可不写

    NSString *nsTextCotent = textView.text;

    NSInteger existTextNum = [nsTextCotent length];

    
    if ([self.cellDelegate respondsToSelector:@selector(clickReasonsApplyingRefundCollectionViewCell:)]) {
        [self.cellDelegate clickReasonsApplyingRefundCollectionViewCell:textView.text];
    }

}

//设置超出最大字数（140字）即不可输入 也是textview的代理方法

-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text{
    if ([text isEqualToString:@"\n"]) {     //这里"\n"对应的是键盘的 return 回收键盘之用
        
       [textView resignFirstResponder];

       return YES;

    }
    if (range.location >= 100){
     
            return NO;

    }else{
       return YES;

    }

}
@end
