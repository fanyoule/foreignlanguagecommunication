//
//  RefundOrderInformationCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "RefundOrderInformationCollectionViewCell.h"
@interface RefundOrderInformationCollectionViewCell ()
@property(nonatomic, strong)UIView *blueView;
/** 订单信息*/
@property(nonatomic, strong)UILabel *informationLabel;



/** 订单编号*/
@property(nonatomic, strong)UILabel *orderLabel;

/** 退款原因*/
@property (nonatomic, strong)UILabel *refundLabel;
/** 退款原因  原因*/
@property (nonatomic, strong)UILabel *refundReasonLabel;


@end
@implementation RefundOrderInformationCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.blueView];
    [self.contentView addSubview:self.informationLabel];
    
    [self.contentView addSubview:self.refundLabel];
    [self.contentView addSubview:self.refundReasonLabel];
    [self.contentView addSubview:self.orderLabel];
    [self.contentView addSubview:self.numberLabel];
    [self.contentView addSubview:self.pasteBtn];
    
    
    [self.blueView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(21);
        make.left.equalTo(self.contentView).offset(15);
        make.width.equalTo(@5);
        make.height.equalTo(@14);
    }];
    [self.informationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.blueView);
        make.left.equalTo(self.blueView.mas_right).offset(5);
    }];
   
    [self.refundLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(-30).priority(600);
        make.left.equalTo(self.contentView).offset(15);
        make.height.equalTo(@15);
    }];
    [self.refundReasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.refundLabel);
        make.left.equalTo(self.contentView).offset(116);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
    [self.orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.refundLabel.mas_top).offset(-10);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.orderLabel);
        make.left.equalTo(self.contentView).offset(116);
    }];
    [self.pasteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.orderLabel);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
    
//    NSArray *titleArray = @[@"课程费用",@"申请退款时间：",@"同意退款时间：",@"退款失败时间：",@"创建时间"];
//    NSArray *contentArray = @[@"1000元",@"2020-05-10    10:00",@"2020-05-10    10:00",@"2020-05-10    10:00",@"2020-05-10    10:00"];
//
//    for (int i = 0; i < titleArray.count; i++) {
//        UIView *view = UIView.new;
//        view.backgroundColor = UIColor.clearColor;
//        [self.contentView addSubview:view];
//        [view mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self.informationLabel.mas_bottom).offset(i*15+i*10+10);
//            make.left.right.equalTo(self.contentView);
////            make.bottom.equalTo(self.orderLabel.mas_top).offset(-10);
//            make.height.equalTo(@15);
//        }];
//
//        UILabel *titleLabel = UILabel.new;
//        titleLabel.text = titleArray[i];
//        titleLabel.textColor = RGBA(24, 24, 24, 1);
//        titleLabel.font = kFont_Medium(13);
//        titleLabel.textAlignment = 0;
//        [view addSubview:titleLabel];
//        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(view);
//            make.left.equalTo(view).offset(15);
//        }];
//
//        UILabel *contentLabel = UILabel.new;
//        contentLabel.text = contentArray[i];
//        contentLabel.textColor = RGBA(24, 24, 24, 1);
//        contentLabel.font = kFont_Medium(13);
//        contentLabel.textAlignment = 0;
//        [view addSubview:contentLabel];
//        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(titleLabel);
//            make.left.equalTo(view).offset(116);
//        }];
//
//    }
    
}
-(void)setModel:(MyOfCourseModel *)model{
    NSArray *titleArray = [NSArray array];
    NSArray *contentArray = [NSArray array];
    if (model.refund == 2) {
        titleArray = @[@"课程费用",@"申请退款时间：",@"创建时间"];
        contentArray = @[[NSString stringWithFormat:@"%ld元",model.price],model.applyTime ? model.applyTime : @"",model.createTime];
    }else if (model.refund == 3){
        titleArray = @[@"课程费用",@"申请退款时间：",@"同意退款时间：",@"退款成功时间：",@"创建时间"];
        contentArray = @[[NSString stringWithFormat:@"%ld元",model.price],model.applyTime ? model.applyTime : @"",model.agreeTime ? model.agreeTime : @"",model.successTime ? model.successTime : @"",model.createTime];
    }else if (model.refund == 4){
        titleArray = @[@"课程费用",@"申请退款时间：",@"同意退款时间：",@"退款失败时间：",@"创建时间"];
        contentArray = @[[NSString stringWithFormat:@"%ld元",model.price],model.applyTime ? model.applyTime : @"",model.agreeTime ? model.agreeTime : @"",model.failTime ? model.failTime : @"",model.createTime];
    }
    
    for (int i = 0; i < titleArray.count; i++) {
        UIView *view = UIView.new;
        view.backgroundColor = UIColor.clearColor;
        [self.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.informationLabel.mas_bottom).offset(i*15+i*10+10);
            make.left.right.equalTo(self.contentView);
//            make.bottom.equalTo(self.orderLabel.mas_top).offset(-10);
            make.height.equalTo(@15);
        }];
        
        UILabel *titleLabel = UILabel.new;
        titleLabel.text = titleArray[i];
        titleLabel.textColor = RGBA(24, 24, 24, 1);
        titleLabel.font = kFont_Medium(13);
        titleLabel.textAlignment = 0;
        [view addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(view);
            make.left.equalTo(view).offset(15);
        }];
        
        UILabel *contentLabel = UILabel.new;
        contentLabel.text = contentArray[i];
        contentLabel.textColor = RGBA(24, 24, 24, 1);
        contentLabel.font = kFont_Medium(13);
        contentLabel.textAlignment = 0;
        [view addSubview:contentLabel];
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(titleLabel);
            make.left.equalTo(view).offset(116);
        }];
    }
    
    
    self.numberLabel.text = model.orderNum;
    self.refundReasonLabel.text = model.reason;
    
}

- (UIView *)blueView{
    if (!_blueView) {
        _blueView = [[UIView alloc]init];
        _blueView.backgroundColor = RGBA(52, 120, 245, 1);
        _blueView.layer.cornerRadius = 2.5;
        _blueView.clipsToBounds = YES;
    }
    return _blueView;
}
- (UILabel *)informationLabel{
    if (!_informationLabel) {
        _informationLabel = [[UILabel alloc]init];
        _informationLabel.text = @"订单信息";
        _informationLabel.textColor = RGBA(24, 24, 24, 1);
        _informationLabel.font = kFont_Bold(16);
        _informationLabel.textAlignment = 0;
    }
    return _informationLabel;
}

- (UILabel *)orderLabel{
    if (!_orderLabel) {
        _orderLabel = [[UILabel alloc]init];
        _orderLabel.text = @"订单编号：";
        _orderLabel.textColor = RGBA(24, 24, 24, 1);
        _orderLabel.font = kFont_Medium(13);
        _orderLabel.textAlignment = 0;
    }
    return _orderLabel;
}
- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.text = @"35468514616";
        _numberLabel.textColor = RGBA(24, 24, 24, 1);
        _numberLabel.font = kFont_Medium(13);
        _numberLabel.textAlignment = 0;
    }
    return _numberLabel;
}
- (UIButton *)pasteBtn{
    if (!_pasteBtn) {
        _pasteBtn = [[UIButton alloc]init];
        [_pasteBtn setTitle:@"复制" forState:(UIControlStateNormal)];
        [_pasteBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _pasteBtn.titleLabel.font = kFont_Medium(13);
    }
    return _pasteBtn;
}

- (UILabel *)refundLabel{
    if (!_refundLabel) {
        _refundLabel = [[UILabel alloc]init];
        _refundLabel.text = @"退款原因";
        _refundLabel.textColor = RGBA(24, 24, 24, 1);
        _refundLabel.font = kFont_Medium(13);
        _refundLabel.textAlignment = 0;
    }
    return _refundLabel;
}

- (UILabel *)refundReasonLabel{
    if (!_refundReasonLabel) {
        _refundReasonLabel = [[UILabel alloc]init];
        _refundReasonLabel.text = @"主动退款/失效课程，主动退款/失效课程，主动退款/失效课程";
        _refundReasonLabel.textColor = RGBA(24, 24, 24, 1);
        _refundReasonLabel.font = kFont_Medium(13);
        _refundReasonLabel.textAlignment = 0;
        _refundReasonLabel.numberOfLines = 0;
    }
    return _refundReasonLabel;
}



@end
