//
//  RefundOrderInformationCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import <UIKit/UIKit.h>
#import "MyOfCourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RefundOrderInformationCollectionViewCell : UICollectionViewCell
/** 订单编号  编号*/
@property(nonatomic, strong)UILabel *numberLabel;
/** 复制*/
@property(nonatomic, strong)UIButton *pasteBtn;

@property (nonatomic, strong)MyOfCourseModel *model;
@end

NS_ASSUME_NONNULL_END
