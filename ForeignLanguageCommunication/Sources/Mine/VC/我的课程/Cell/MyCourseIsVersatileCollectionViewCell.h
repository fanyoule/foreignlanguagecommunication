//
//  MyCourseIsVersatileCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import <UIKit/UIKit.h>
#import "MyCourseCollectionModel.h"
#import "MyOfCourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyCourseIsVersatileCollectionViewCell : UICollectionViewCell
/** 待上课*/
@property (nonatomic, strong)UILabel *classLabel;
/** 灰色按钮*/
@property (nonatomic, strong)UIButton *grayBtn;
/** 蓝色按钮*/
@property (nonatomic, strong)UIButton *blueBtn;
/** 头像*/
@property (nonatomic,strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;

@property (nonatomic, strong)MyCourseCollectionModel *courseModel;

@property (nonatomic, strong)MyOfCourseModel *model;
@end

NS_ASSUME_NONNULL_END
