//
//  MyCourseViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyCourseViewController : BaseViewController

/** 判断 返回上一级 还是上上级  1：上上级  其他：上一级*/
@property (nonatomic, assign)NSInteger judge;
@end

NS_ASSUME_NONNULL_END
