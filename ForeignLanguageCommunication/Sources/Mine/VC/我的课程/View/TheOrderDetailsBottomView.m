//
//  TheOrderDetailsBottomView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "TheOrderDetailsBottomView.h"

@implementation TheOrderDetailsBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.refundBtn];
    [self addSubview:self.messagesBtn];
    [self.messagesBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-15);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
    [self.refundBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.messagesBtn.mas_left).offset(-10);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
}
- (UIButton *)refundBtn{
    if (!_refundBtn) {
        _refundBtn = [[UIButton alloc]init];
        [_refundBtn setTitle:@"退款" forState:(UIControlStateNormal)];
        [_refundBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        _refundBtn.layer.cornerRadius = 3;
        _refundBtn.clipsToBounds = YES;
        _refundBtn.layer.borderWidth = 0.5;
        _refundBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        _refundBtn.titleLabel.font = kFont_Medium(14);
    }
    return _refundBtn;
}
- (UIButton *)messagesBtn{
    if (!_messagesBtn) {
        _messagesBtn = [[UIButton alloc]init];
        [_messagesBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [_messagesBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _messagesBtn.layer.cornerRadius = 3;
        _messagesBtn.clipsToBounds = YES;
        _messagesBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _messagesBtn.titleLabel.font = kFont_Medium(14);
    }
    return _messagesBtn;
}

@end
