//
//  TheOrderDetailsBottomView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TheOrderDetailsBottomView : UIView
/** 退款*/
@property (nonatomic, strong)UIButton *refundBtn;
/** 私信*/
@property (nonatomic, strong)UIButton *messagesBtn;

@end

NS_ASSUME_NONNULL_END
