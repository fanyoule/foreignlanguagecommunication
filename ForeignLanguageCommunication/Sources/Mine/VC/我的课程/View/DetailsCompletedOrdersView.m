//
//  DetailsCompletedOrdersView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "DetailsCompletedOrdersView.h"

@implementation DetailsCompletedOrdersView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.complaintsBtn];
    [self addSubview:self.evaluationBtn];
    [self addSubview:self.messagesBtn];
    [self.evaluationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-15);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
    
    [self.complaintsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.evaluationBtn.mas_left).offset(-10);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
    
    [self.messagesBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.complaintsBtn.mas_left).offset(-10);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
}
- (UIButton *)complaintsBtn{
    if (!_complaintsBtn) {
        _complaintsBtn = [[UIButton alloc]init];
        [_complaintsBtn setTitle:@"投诉" forState:(UIControlStateNormal)];
        [_complaintsBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        _complaintsBtn.layer.cornerRadius = 3;
        _complaintsBtn.clipsToBounds = YES;
        _complaintsBtn.layer.borderWidth = 0.5;
        _complaintsBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        _complaintsBtn.titleLabel.font = kFont_Medium(14);
    }
    return _complaintsBtn;
}
- (UIButton *)messagesBtn{
    if (!_messagesBtn) {
        _messagesBtn = [[UIButton alloc]init];
        [_messagesBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [_messagesBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        _messagesBtn.layer.cornerRadius = 3;
        _messagesBtn.clipsToBounds = YES;
        _messagesBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        _messagesBtn.layer.borderWidth = 0.5;
        _messagesBtn.titleLabel.font = kFont_Medium(14);
    }
    return _messagesBtn;
}

- (UIButton *)evaluationBtn{
    if (!_evaluationBtn) {
        _evaluationBtn = [[UIButton alloc]init];
        [_evaluationBtn setTitle:@"评价" forState:(UIControlStateNormal)];
        [_evaluationBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _evaluationBtn.layer.cornerRadius = 3;
        _evaluationBtn.clipsToBounds = YES;
        _evaluationBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _evaluationBtn.titleLabel.font = kFont_Medium(14);
    }
    return _evaluationBtn;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
