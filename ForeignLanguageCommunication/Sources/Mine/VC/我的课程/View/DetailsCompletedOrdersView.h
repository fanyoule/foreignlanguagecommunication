//
//  DetailsCompletedOrdersView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailsCompletedOrdersView : UIView
/** 投诉*/
@property (nonatomic, strong)UIButton *complaintsBtn;
/** 私信*/
@property (nonatomic, strong)UIButton *messagesBtn;
/** 评价*/
@property (nonatomic, strong)UIButton *evaluationBtn;
@end

NS_ASSUME_NONNULL_END
