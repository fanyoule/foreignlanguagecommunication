//
//  ForAClassViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "ForAClassViewController.h"
#import "MyCourseIsVersatileCollectionViewCell.h"
#import "TheOrderDetailsViewController.h"
#import "ToApplyForRefundViewController.h"// 退款vc
#import "MatchingPrivateChatVoiceVideoPopupWindow.h"
#import <HWPopController/HWPop.h>
#import "MyOfCourseModel.h"
#import "ChatDetailsVC.h"


@interface ForAClassViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong)UIView *backView;
@property (nonatomic, strong)UILabel *rulesLabel;

@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation ForAClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTopRules];
    
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class] forCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell"];
    
    // MARK: 下拉刷新
        self.mainCollectionView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
}
// MARK: 下拉刷新回调
-(void)loadNewData{
    [self getData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getData];
    
}
-(void)getData{
    kWeakSelf(self)
    [self.dataArray removeAllObjects];
    if (self.isJudge == 1) {// 我的课程
        [RequestManager myCourseWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
            NSLog(@"我的课程 === %@",response);
            NSArray *array = response[@"data"];
            if ([response[@"status"] integerValue] == 200) {
                for (int i = 0; i < array.count; i++) {
                    MyOfCourseModel *model = [MyOfCourseModel mj_objectWithKeyValues:array[i]];
                    if ([model.status isEqualToString:@"待上课"]) {
                        [self.dataArray addObject:model];
                    }
                }
            }
            [self.mainCollectionView reloadData];
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        } withFail:^(NSError * _Nullable error) {
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        }];
    }else{// 课程管理
        [RequestManager myCourseOrderWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
            NSLog(@"课程管理 === %@",response);
            NSArray *array = response[@"data"];
            if ([response[@"status"] integerValue] == 200) {
                for (int i = 0; i < array.count; i++) {
                    MyOfCourseModel *model = [MyOfCourseModel mj_objectWithKeyValues:array[i]];
                    if ([model.status isEqualToString:@"待上课"]) {
                        [self.dataArray addObject:model];
                    }
                }
            }
            [self.mainCollectionView reloadData];
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        } withFail:^(NSError * _Nullable error) {
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        }];
    }
    
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


// MARK： 头部规则
-(void)addTopRules{
    [RequestManager daysToCancelSuccess:^(id  _Nullable response) {
        NSLog(@"课程自动取消天数 === %@",response);
        self.rulesLabel.text = [NSString stringWithFormat:@"购买日起%@天内未上课，自动失效及退款。",response[@"data"]];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    [self.view addSubview:self.backView];
    [self.view addSubview:self.rulesLabel];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@30);
        make.top.equalTo(self.view).offset(30);
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(15);
    }];
}


- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(214, 228, 253, 1);
    }
    return _backView;
}
- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"购买日起30天内未上课，自动失效及退款。";
        _rulesLabel.textColor = RGBA(52, 120, 245, 1);
        _rulesLabel.font = kFont_Medium(14);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MyCourseIsVersatileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    MyOfCourseModel *model = [[MyOfCourseModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    kWeakSelf(self)
    if (self.isJudge == 1) {// 我的课程
        [cell.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.userUrl]];
        cell.nameLabel.text = model.coachName;
        [cell.grayBtn whenTapped:^{
            // 退款点击事件
            ToApplyForRefundViewController *vc = [[ToApplyForRefundViewController alloc]init];
            vc.model = model;
            [self.navigationController pushViewController:vc animated:YES];
        }];
        [cell.blueBtn whenTapped:^{
//            MatchingPrivateChatVoiceVideoPopupWindow *vc = [MatchingPrivateChatVoiceVideoPopupWindow new];
//            [vc popupWithPopType:HWPopTypeShrinkIn dismissType: HWDismissTypeNone];
            [weakself openChat:model];
        }];
    }else{// 课程管理
        [cell.grayBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [cell.blueBtn setTitle:@"视频上课" forState:(UIControlStateNormal)];
        [cell.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.userUrl]];
        cell.nameLabel.text = model.nickname;
        [cell.blueBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@80);
        }];
        [cell.grayBtn whenTapped:^{
            [weakself openChat:model];
        }];
        [cell.blueBtn whenTapped:^{
            [weakself openVideo:model];
        }];
    }
        return cell;
  
}
- (void)openVideo:(MyOfCourseModel *)model {
    long userId = self.isJudge == 1 ? model.coachId : model.userId;
    if (userId == [UserInfoManager shared].getUserID) {
        [self showText:@"不能和自己聊天"];
        return;
    }
    NSString *name = self.isJudge == 1 ? model.coachName : model.nickname;
    NSLog(@"视频上课");
}
- (void)openChat:(MyOfCourseModel *)model {
    long userId = self.isJudge == 1 ? model.coachId : model.userId;
    if (userId == [UserInfoManager shared].getUserID) {
        [self showText:@"不能和自己聊天"];
        return;
    }
    NSString *name = self.isJudge == 1 ? model.coachName : model.nickname;
    NewsAllBuddyListModel *lisModel = [[NewsAllBuddyListModel alloc]init];
    lisModel.ID = userId;
    lisModel.nickname = name;
    ChatDetailsVC *vc = [[ChatDetailsVC alloc]init];
    vc.buddyModel = lisModel;
    [self.navigationController pushViewController:vc animated:NO];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    return CGSizeMake(KSW-30, 198);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MyOfCourseModel *model = [[MyOfCourseModel alloc]init];
    model = self.dataArray[indexPath.row];
    TheOrderDetailsViewController *vc = [[TheOrderDetailsViewController alloc]init];
    vc.isJudge = self.isJudge;
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}



@end
