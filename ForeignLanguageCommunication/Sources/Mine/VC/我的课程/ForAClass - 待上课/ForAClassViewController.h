//
//  ForAClassViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ForAClassViewController : BaseViewController
/** 判断  1：我的课程 ， 2：课程管理*/
@property (nonatomic, assign)NSInteger isJudge;

@end

NS_ASSUME_NONNULL_END
