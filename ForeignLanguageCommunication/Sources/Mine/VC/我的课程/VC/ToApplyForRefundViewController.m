//
//  ToApplyForRefundViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//  申请退款

#import "ToApplyForRefundViewController.h"
#import "MyCourseIsVersatileCollectionViewCell.h"
#import "ReasonsApplyingRefundCollectionViewCell.h"
@interface ToApplyForRefundViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,ReasonsApplyingRefundCollectionViewCell>
/** 提交按钮*/
@property (nonatomic ,strong)UIButton *submitBtn;

/** 退款原因*/
@property (nonatomic, copy)NSString *whyString;
@end

@implementation ToApplyForRefundViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"申请退款";
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    
    
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class] forCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell"];
    [self.mainCollectionView registerClass:[ReasonsApplyingRefundCollectionViewCell class] forCellWithReuseIdentifier:@"ReasonsApplyingRefundCollectionViewCell"];
    
    [self.view addSubview:self.submitBtn];
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-10-TabbarSafeMargin);
        make.right.equalTo(self.view).offset(-15);
        make.left.equalTo(self.view).offset(15);
        make.height.equalTo(@44);
    }];
    
}
- (void)setModel:(MyOfCourseModel *)model{
    _model = model;
    [self.mainCollectionView reloadData];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 2;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        MyCourseIsVersatileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 12;
        cell.layer.masksToBounds = YES;
        cell.classLabel.hidden = YES;
        cell.blueBtn.hidden = YES;
        cell.grayBtn.hidden = YES;
        cell.model = self.model;
        return cell;
    }else{
        ReasonsApplyingRefundCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ReasonsApplyingRefundCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 6;
        cell.layer.masksToBounds = YES;
        cell.cellDelegate = self;
        cell.model = self.model;
        return cell;
    }
    
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        return CGSizeMake(KSW-30, 155);
    }else{
        return CGSizeMake(KSW-30, 190);
    }
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}

- (UIButton *)submitBtn{
    if (!_submitBtn) {
        _submitBtn = [[UIButton alloc]init];
        [_submitBtn setTitle:@"提交" forState:(UIControlStateNormal)];
        [_submitBtn setTitleColor:RGBA(254, 254, 254, 1) forState:(UIControlStateNormal)];
        _submitBtn.titleLabel.font = kFont_Bold(15);
        _submitBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _submitBtn.layer.cornerRadius = 6;
        _submitBtn.clipsToBounds = YES;
        [_submitBtn addTarget:self action:@selector(clickSubmit) forControlEvents:(UIControlEventTouchDown)];
    }
    return _submitBtn;
}

-(void)clickReasonsApplyingRefundCollectionViewCell:(NSString *)string{
    // 退款原因
    NSLog(@"退款原因 == %@",string);
    self.whyString = string;
}
// MARK: 提交退款
-(void)clickSubmit{
    if (self.whyString == nil) {
        self.whyString = @"无";
    }
    [RequestManager courseRefundWithOrderId:self.model.ID reason:self.whyString withSuccess:^(id  _Nullable response) {
        if ([response[@"status"] integerValue] == 200) {
            if (self.isJudge == 2) {
                if (self.navigationController.viewControllers.count >= 2) {
                        UIViewController *vc = [self.navigationController.viewControllers objectOrNilAtIndex:1];
                        [self.navigationController popToViewController:vc animated:YES];
                    }
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            [self showText:response[@"msg"]];
        }
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
