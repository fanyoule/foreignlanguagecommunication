//
//  PurchaseCourseOrderDetailsViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/28.
//

#import "BaseViewController.h"
#import "CourseDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PurchaseCourseOrderDetailsViewController : BaseViewController

@property(nonatomic, strong)CourseDetailsModel *model;

@end

NS_ASSUME_NONNULL_END
