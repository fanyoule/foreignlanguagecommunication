//
//  PurchaseCourseOrderDetailsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/28.
//

#import "PurchaseCourseOrderDetailsViewController.h"
#import "PayThePopupWindow.h"
#import "MyCourseViewController.h"
@interface PurchaseCourseOrderDetailsViewController ()
@property (nonatomic, strong)UIView *backView;

/** 图片*/
@property (nonatomic, strong)UIImageView *pictureImageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 钱*/
@property (nonatomic, strong)UILabel *moneyLabel;
/** 价格*/
@property (nonatomic, strong)UILabel *priceLabel;
/** 时间*/
@property (nonatomic, strong)UILabel *timeLabel;

/** 立即购买*/
@property (nonatomic, strong)UIButton *purchaseBtn;

/** 折扣*/
@property (nonatomic, assign)CGFloat discount;
@end

@implementation PurchaseCourseOrderDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navTitleString = @"订单详情";
    self.view.backgroundColor = UIColor.whiteColor;
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.navView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.left.equalTo(self.navView);
        make.height.equalTo(@0.5);
    }];
    
    [self.view addSubview:self.backView];
    [self.view addSubview:self.pictureImageV];
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.moneyLabel];
    [self.view addSubview:self.priceLabel];
    [self.view addSubview:self.timeLabel];
    [self.view addSubview:self.purchaseBtn];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@120);
    }];
    [self.pictureImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(15);
        make.height.width.equalTo(@80);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pictureImageV);
        make.left.equalTo(self.pictureImageV.mas_right).offset(10);
        make.right.equalTo(self.backView).offset(-15);
    }];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.pictureImageV);
        make.left.equalTo(self.pictureImageV.mas_right).offset(10);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.moneyLabel.mas_right);
        make.bottom.equalTo(self.pictureImageV);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo (self.priceLabel.mas_right);
        make.bottom.equalTo(self.pictureImageV);
    }];
    
    [self.purchaseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@44);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin-5);
    }];
    // Do any additional setup after loading the view.
    [self getData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveWXPayFinishNotification:) name:WXPayFinishName object:nil];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setModel:(CourseDetailsModel *)model{
    _model = model;
    if (model.coursePic != nil ) {
        NSString *imageStr =model.coursePic[0];
        [self.pictureImageV sd_setImageWithURL:[NSURL URLWithString:imageStr]];
    }
    self.titleLabel.text = model.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld",model.price];
    
    self.timeLabel.text = [NSString stringWithFormat:@"/%ld分钟/一节课",model.time];
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.cornerRadius = 12;
        _backView.layer.shadowColor = [UIColor blackColor].CGColor;
        _backView.layer.shadowOffset = CGSizeMake(0.1f, 0.1f);
        _backView.layer.shadowOpacity = 0.5f;
    }
    return _backView;
}

- (UIImageView *)pictureImageV{
    if (!_pictureImageV) {
        _pictureImageV = [[UIImageView alloc]init];
        _pictureImageV.image = kImage(@"测试头像");
        _pictureImageV.layer.cornerRadius = 6;
        _pictureImageV.clipsToBounds = YES;
        _pictureImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _pictureImageV;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"青少年课程口语1小时练习青少年课程口语1小时练习青少年课程口语...小时练习";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont(14);
        _titleLabel.textAlignment = 0;
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}
- (UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc]init];
        _moneyLabel.text = @"¥";
        _moneyLabel.textColor = RGBA(52, 120, 245, 1);
        _moneyLabel.font = kFont(11);
        _moneyLabel.textAlignment = 0;
    }
    return _moneyLabel;
}
- (UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.text = @"55858";
        _priceLabel.textColor = RGBA(52, 120, 245, 1);
        _priceLabel.font = kFont_Bold(24);
        _priceLabel.textAlignment = 0;
    }
    return _priceLabel;
}

- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.text = @"/90分钟/一节课";
        _timeLabel.textColor = RGBA(52, 120, 245, 1);
        _timeLabel.font = kFont_Medium(11);
        _timeLabel.textAlignment = 0;
    }
    return _timeLabel;
}

- (UIButton *)purchaseBtn{
    if (!_purchaseBtn) {
        _purchaseBtn = [[UIButton alloc]init];
        [_purchaseBtn setTitle:@"立即购买" forState:(UIControlStateNormal)];
        [_purchaseBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _purchaseBtn.titleLabel.font = kFont_Bold(15);
        _purchaseBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _purchaseBtn.layer.cornerRadius = 6;
        _purchaseBtn.clipsToBounds = YES;
        [_purchaseBtn addTarget:self action:@selector(clickPurchaseBtn) forControlEvents:(UIControlEventTouchDown)];
    }
    return _purchaseBtn;
}

-(void)getData{
    [RequestManager discountByGoldSuccess:^(id  _Nullable response) {
        NSLog(@"打折 ==== %@",response);
        NSString *string =response[@"data"][@"discount"];
        
        NSString *strUrl = [string stringByReplacingOccurrencesOfString:@"%" withString:@""];
        
        self.discount = [strUrl floatValue]/10;
        
        NSLog(@"折扣 === %0.1lf",self.discount);
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
#pragma mark -- 立即购买
-(void)clickPurchaseBtn{
    PayThePopupWindow *VC = [PayThePopupWindow new];
    VC.discount = self.discount;
    VC.model = self.model;
    HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                    popController.backgroundAlpha = 0.1;//背景色透明度
                    popController.animationDuration = 0.3;
                    popController.popPosition = 2;
                    popController.popType = 5;
                    popController.dismissType = 5;
                    popController.shouldDismissOnBackgroundTouch = YES;
                    [popController presentInViewController:self];
    kWeakSelf(self)
    VC.payment = ^(NSInteger judge) {
        [weakself showAlert:judge];
    };
}
- (void)showAlert:(NSInteger)judge {
    if (judge == 1) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"购买成功，耐心等待教练与你联系" preferredStyle:UIAlertControllerStyleAlert];
                
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            MyCourseViewController *vc = [[MyCourseViewController alloc]init];
            vc.judge = 1;
            [self.navigationController pushViewController:vc animated:YES];
        }];
                
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        
    }
}

-(void)receiveWXPayFinishNotification:(NSNotification *)notifi {
    PayResp *resp = notifi.object;
    switch (resp.errCode) {
        case WXSuccess:{
//            [self showText:@"支付成功"];
            [self showAlert:1];
            break;
        }
        case WXErrCodeUserCancel:{
            //取消了支付
            [self showText:@"取消了支付"];
            break;
        }
        default:{
            //支付错误
            [self showText:[NSString stringWithFormat:@"支付失败！retcode = %d, retstr = %@", resp.errCode, resp.errStr]];
            break;
        }
    }
}

@end
