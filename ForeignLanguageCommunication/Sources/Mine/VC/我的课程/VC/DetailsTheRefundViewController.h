//
//  DetailsTheRefundViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "BaseViewController.h"
#import "MyOfCourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface DetailsTheRefundViewController : BaseViewController
@property (nonatomic, strong)MyOfCourseModel *model;
//@property (nonatomic, assign) long ID;
@end

NS_ASSUME_NONNULL_END
