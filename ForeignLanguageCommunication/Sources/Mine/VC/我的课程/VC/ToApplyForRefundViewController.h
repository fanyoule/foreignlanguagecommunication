//
//  ToApplyForRefundViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "BaseViewController.h"
#import "MyOfCourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ToApplyForRefundViewController : BaseViewController
/** 判断 1：待上课跳转过来  ， 2：订单详情跳转过来*/
@property (nonatomic, assign)NSInteger isJudge;

@property (nonatomic, strong)MyOfCourseModel *model;
@end

NS_ASSUME_NONNULL_END
