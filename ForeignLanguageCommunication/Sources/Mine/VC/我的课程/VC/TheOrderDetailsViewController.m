//
//  TheOrderDetailsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "TheOrderDetailsViewController.h"
#import "MyCourseIsVersatileCollectionViewCell.h"
#import "TheOrderInformationCollectionViewCell.h"
#import "TheOrderDetailsBottomView.h"// 底部View
#import "ToApplyForRefundViewController.h"// 退款
@interface TheOrderDetailsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
/** 底部View*/
@property (nonatomic, strong)TheOrderDetailsBottomView *bottomView;

/** 课程订单详情model*/
@property (nonatomic, strong)MyOfCourseModel *myOfCourseModel;
@end

@implementation TheOrderDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"订单详情";
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
        make.height.equalTo(@49);
        make.left.right.equalTo(self.view);
    }];
    [self.bottomView.refundBtn addTarget:self action:@selector(clickRefund) forControlEvents:(UIControlEventTouchDown)];
    
  
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    
    [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class] forCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell"];
    [self.mainCollectionView registerClass:[TheOrderInformationCollectionViewCell class] forCellWithReuseIdentifier:@"TheOrderInformationCollectionViewCell"];
    
}

- (void)setModel:(MyOfCourseModel *)model{
    _model = model;
    
    [RequestManager courseOrderWithOrderId:model.ID userId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSDictionary *dic = response[@"data"];
        NSLog(@"课程详情 === %@",response);
        if ([response[@"status"] integerValue] == 200) {
            self.myOfCourseModel = [MyOfCourseModel mj_objectWithKeyValues:dic];
            
        }else{
            [self showText:response[@"msg"]];
        }
        [self.mainCollectionView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
}
- (MyOfCourseModel *)myOfCourseModel{
    if (!_myOfCourseModel) {
        _myOfCourseModel = [[MyOfCourseModel alloc]init];
    }
    return _myOfCourseModel;
}


- (TheOrderDetailsBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [TheOrderDetailsBottomView new];
        _bottomView.backgroundColor = UIColor.whiteColor;
    }
    return _bottomView;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 2;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        MyCourseIsVersatileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 12;
        cell.layer.masksToBounds = YES;
        cell.classLabel.hidden = YES;
        cell.blueBtn.hidden = YES;
        cell.grayBtn.hidden = YES;
        cell.model = self.myOfCourseModel;
        
        if (self.isJudge == 1) {// 我的课程
            [cell.portraitImageV sd_setImageWithURL:[NSURL URLWithString:self.model.userUrl]];
            cell.nameLabel.text = self.model.coachName;
            
        }else{// 课程管理
            [cell.portraitImageV sd_setImageWithURL:[NSURL URLWithString:self.model.userUrl]];
            cell.nameLabel.text = self.model.nickname;
        }
        
        
        
        
        
        
        return cell;
    }else{
        TheOrderInformationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TheOrderInformationCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 6;
        cell.layer.masksToBounds = YES;
        cell.model = self.myOfCourseModel;
        
        [cell.pasteBtn whenTapped:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = cell.numberLabel.text;
            [self showText:@"复制成功"];
        }];
        
        return cell;
    }
    
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        return CGSizeMake(KSW-30, 155);
    }else{
        return CGSizeMake(KSW-30, 130);
    }
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}

//MARK: 退款
-(void)clickRefund{
    ToApplyForRefundViewController *vc = [[ToApplyForRefundViewController alloc]init];
    vc.model = self.myOfCourseModel;
    vc.isJudge = 2;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
