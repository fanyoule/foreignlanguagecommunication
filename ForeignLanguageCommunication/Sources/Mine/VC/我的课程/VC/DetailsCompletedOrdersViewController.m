//
//  DetailsCompletedOrdersViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//  已完成订单详情

#import "DetailsCompletedOrdersViewController.h"
#import "MyCourseIsVersatileCollectionViewCell.h"
#import "TheOrderInformationHasBeenPlacedCollectionViewCell.h"
#import "DetailsCompletedOrdersView.h"// 底部View
#import "EvaluationViewController.h"// 评价VC
@interface DetailsCompletedOrdersViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
/** 底部View*/
@property (nonatomic, strong)DetailsCompletedOrdersView *bottomView;

@end

@implementation DetailsCompletedOrdersViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"订单详情";
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
        make.height.equalTo(@49);
        make.left.right.equalTo(self.view);
    }];
    
    
    
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    
    [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class] forCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell"];
    [self.mainCollectionView registerClass:[TheOrderInformationHasBeenPlacedCollectionViewCell class] forCellWithReuseIdentifier:@"TheOrderInformationHasBeenPlacedCollectionViewCell"];
    
    
    
    [self.bottomView.complaintsBtn addTarget:self action:@selector(clickComplaints) forControlEvents:(UIControlEventTouchDown)];
    [self.bottomView.evaluationBtn addTarget:self action:@selector(clickEvaluation) forControlEvents:(UIControlEventTouchDown)];
    [self.bottomView.messagesBtn addTarget:self action:@selector(clickMessages) forControlEvents:(UIControlEventTouchDown)];
    
    
}

- (DetailsCompletedOrdersView *)bottomView{
    if (!_bottomView) {
        _bottomView = [DetailsCompletedOrdersView new];
        _bottomView.backgroundColor = UIColor.whiteColor;
    }
    return _bottomView;
}

- (void)setModel:(MyOfCourseModel *)model{
    _model = model;
    [self.mainCollectionView reloadData];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 2;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        MyCourseIsVersatileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 12;
        cell.layer.masksToBounds = YES;
        cell.classLabel.hidden = YES;
        cell.blueBtn.hidden = YES;
        cell.grayBtn.hidden = YES;
        cell.model = self.model;
        return cell;
    }else{
        TheOrderInformationHasBeenPlacedCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TheOrderInformationHasBeenPlacedCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 6;
        cell.layer.masksToBounds = YES;
        cell.model = self.model;
        
        [cell.pasteBtn whenTapped:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = cell.numberLabel.text;
            [self showText:@"复制成功"];
        }];
        return cell;
    }
    
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        return CGSizeMake(KSW-30, 155);
    }else{
        return CGSizeMake(KSW-30, 180);
    }
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}


-(void)clickComplaints{
    NSLog(@"点击了投诉");
}
-(void)clickEvaluation{
    NSLog(@"点击了评价");
    EvaluationViewController *vc = [[EvaluationViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)clickMessages{
    NSLog(@"点击了私信");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
