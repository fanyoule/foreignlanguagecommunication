//
//  DetailsTheRefundViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//  退款详情

#import "DetailsTheRefundViewController.h"
#import "MyCourseIsVersatileCollectionViewCell.h"
#import "RefundOrderInformationCollectionViewCell.h"
#import "TheOrderDetailsBottomView.h"// 底部View
#import "ChatDetailsVC.h"


@interface DetailsTheRefundViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
/** 底部View*/
@property (nonatomic, strong)TheOrderDetailsBottomView *bottomView;

@end

@implementation DetailsTheRefundViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"订单详情";
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
        make.height.equalTo(@49);
        make.left.right.equalTo(self.view);
    }];
    
    
    
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    
    [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class] forCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell"];
    [self.mainCollectionView registerClass:[RefundOrderInformationCollectionViewCell class] forCellWithReuseIdentifier:@"RefundOrderInformationCollectionViewCell"];
    
    
    
    if ([self.model.status isEqualToString:@"已同意"]||[self.model.status isEqualToString:@"已失效"]) {
        self.bottomView.refundBtn.hidden = YES;
//        [self.bottomView.refundBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [self.bottomView.messagesBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [self.bottomView.messagesBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@80);
        }];
        // 两个按钮根据状态 是否显示取消退款，如果不显示  改变refundBtn 这个按钮的位置 颜色 和文字颜色 边框颜色；
//        [self.bottomView.refundBtn addTarget:self action:@selector(clickMessages) forControlEvents:(UIControlEventTouchDown)];
        [self.bottomView.messagesBtn addTarget:self action:@selector(clickMessages) forControlEvents:(UIControlEventTouchDown)];
    } else {
        [self.bottomView.refundBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [self.bottomView.messagesBtn setTitle:@"取消退款" forState:(UIControlStateNormal)];
        [self.bottomView.messagesBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@80);
        }];
        // 两个按钮根据状态 是否显示取消退款，如果不显示  改变refundBtn 这个按钮的位置 颜色 和文字颜色 边框颜色；
        [self.bottomView.refundBtn addTarget:self action:@selector(clickMessages) forControlEvents:(UIControlEventTouchDown)];
        [self.bottomView.messagesBtn addTarget:self action:@selector(cancelTheRefund) forControlEvents:(UIControlEventTouchDown)];
    }
    
    
    
    
    
//    [self getData];
}
//- (void)getData {
//    [self showSVP];
//    kWeakSelf(self)
//    [RequestManager courseOrderWithOrderId:self.ID userId:[UserInfoManager shared].getUserID withSuccess:^(id  _Nullable response) {
//        [weakself dissSVP];
//        weakself.model = [MyOfCourseModel mj_objectWithKeyValues:response[@"data"]];
//        [weakself.mainCollectionView reloadData];
//    } withFail:^(NSError * _Nullable error) {
//        [weakself showText:error.localizedDescription];
//    }];
//}

- (TheOrderDetailsBottomView *)bottomView{
    if (!_bottomView) {
        _bottomView = [TheOrderDetailsBottomView new];
        _bottomView.backgroundColor = UIColor.whiteColor;
    }
    return _bottomView;
}

-(void)setModel:(MyOfCourseModel *)model{
    _model = model;
    [self.mainCollectionView reloadData];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.model) {
        return 1;   //返回section数
    } else {
        return 0;
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 2;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        MyCourseIsVersatileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 12;
        cell.layer.masksToBounds = YES;
        cell.classLabel.hidden = YES;
        cell.blueBtn.hidden = YES;
        cell.grayBtn.hidden = YES;
        cell.model = self.model;
        return cell;
    }else{
        RefundOrderInformationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RefundOrderInformationCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.cornerRadius = 6;
        cell.layer.masksToBounds = YES;
        
        cell.model = self.model;
        
        [cell.pasteBtn whenTapped:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = cell.numberLabel.text;
            [self showText:@"复制成功"];
        }];
        return cell;
    }
    
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (indexPath.row == 0) {
        return CGSizeMake(KSW-30, 155);
    }else{
        CGFloat h = 0;
        if (self.model.refund == 2) {
            h = 20*3 +135;
        }else if (self.model.refund == 3){
            h = 20*5 + 145;
        }else if (self.model.refund == 4){
            h = 20*5 + 145;
        }else{
            h = 20*4 + 140;
        }
        return CGSizeMake(KSW-30, h);
    }
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}

-(void)clickMessages{
    if (self.model.coachId == [UserInfoManager shared].getUserID) {
        [self showText:@"不能和自己聊天"];
        return;
    }
    NewsAllBuddyListModel *lisModel = [[NewsAllBuddyListModel alloc]init];
    lisModel.ID = self.model.coachId;
    lisModel.nickname = self.model.coachName;
    ChatDetailsVC *vc = [[ChatDetailsVC alloc]init];
    vc.buddyModel = lisModel;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)cancelTheRefund{
    NSLog(@"点击了取消退款");
    [RequestManager cancelRefundWithOrderId:self.model.ID withSuccess:^(id  _Nullable response) {
        if ([response[@"status"] integerValue] == 200) {
            if (self.navigationController.viewControllers.count >= 2) {
                    UIViewController *vc = [self.navigationController.viewControllers objectOrNilAtIndex:1];
                    [self.navigationController popToViewController:vc animated:YES];
                }
        }else{
            [self showText:response[@"msg"]];
        }
    } withFail:^(NSError * _Nullable error) {
        
    }];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
