//
//  HasFinishedTheViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "HasFinishedTheViewController.h"
#import "MyCourseIsVersatileCollectionViewCell.h"
#import "DetailsCompletedOrdersViewController.h"
#import "EvaluationViewController.h"// 评价VC
#import "ComplaintsViewController.h"// 投诉
#import "MyOfCourseModel.h"
#import "ChatDetailsVC.h"
@interface HasFinishedTheViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation HasFinishedTheViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(30);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class] forCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell"];
    
    // MARK: 下拉刷新
        self.mainCollectionView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
}

// MARK: 下拉刷新回调
-(void)loadNewData{
    [self getData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getData];
}

-(void)getData{
    kWeakSelf(self)
    [self.dataArray removeAllObjects];
    if (self.isJudge == 1) {// 我的课程
        [RequestManager myCourseWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
            NSLog(@"我的课程 === %@",response);
            NSArray *array = response[@"data"];
            if ([response[@"status"] integerValue] == 200) {
                for (int i = 0; i < array.count; i++) {
                    MyOfCourseModel *model = [MyOfCourseModel mj_objectWithKeyValues:array[i]];
                    if ([model.status isEqualToString:@"已上完"]) {
                        [self.dataArray addObject:model];
                    }
                }
            }
            [self.mainCollectionView reloadData];
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        } withFail:^(NSError * _Nullable error) {
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        }];
    }else{// 课程管理
        [RequestManager myCourseOrderWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
            NSLog(@"课程管理 === %@",response);
            NSArray *array = response[@"data"];
            if ([response[@"status"] integerValue] == 200) {
                for (int i = 0; i < array.count; i++) {
                    MyOfCourseModel *model = [MyOfCourseModel mj_objectWithKeyValues:array[i]];
                    if ([model.status isEqualToString:@"正常"]) {
                        [self.dataArray addObject:model];
                    }
                }
            }
            [self.mainCollectionView reloadData];
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        } withFail:^(NSError * _Nullable error) {
            [weakself.mainCollectionView.mj_header endRefreshing];
            [weakself.mainCollectionView.mj_footer endRefreshing];
        }];
    }
    
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MyCourseIsVersatileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    MyOfCourseModel *model = [[MyOfCourseModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    kWeakSelf(self)
    if (self.isJudge == 1) { //我的课程
        [cell.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.userUrl]];
        cell.nameLabel.text = model.coachName;
        cell.classLabel.text = @"已上完";
        [cell.grayBtn setTitle:@"投诉" forState:(UIControlStateNormal)];
        
        UIButton *messagesBtn = UIButton.new;
        [messagesBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [messagesBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        messagesBtn.titleLabel.font = kFont_Medium(14);
        messagesBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        messagesBtn.layer.borderWidth = 0.5;
        messagesBtn.layer.cornerRadius = 6;
        messagesBtn.clipsToBounds = YES;
        [cell addSubview:messagesBtn];
        [messagesBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell.grayBtn);
            make.right.equalTo(cell.grayBtn.mas_left).offset(-10);
            make.width.equalTo(@60);
            make.height.equalTo(@28);
        }];
        
        NSLog(@"评价 === %@",model);
        if (model.evaluation == YES) {
            [cell.blueBtn setTitle:@"已评价" forState:(UIControlStateNormal)];
        }else{
            [cell.blueBtn setTitle:@"评价" forState:(UIControlStateNormal)];
            [cell.blueBtn whenTapped:^{
                // 评价
                EvaluationViewController *vc = [[EvaluationViewController alloc]init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
            }];
        }
        
        [cell.grayBtn whenTapped:^{
            // 投诉
            ComplaintsViewController *vc = [[ComplaintsViewController alloc]init];
            vc.reportID = model.userId;
            [self.navigationController pushViewController:vc animated:YES];
        }];
        [messagesBtn whenTapped:^{
            // 私信
            [weakself openChat:model];
        }];
    }else{// 课程管理
        [cell.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.userUrl]];
        cell.nameLabel.text = model.nickname;
        cell.classLabel.text = model.status;
        cell.grayBtn.hidden = YES;
        
        [cell.blueBtn setTitle:@"私信" forState:(UIControlStateNormal)];
        [cell.blueBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        cell.blueBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        
        [cell.blueBtn whenTapped:^{
            [weakself openChat:model];
        }];
        
    }
    return cell;
}
- (void)openChat:(MyOfCourseModel *)model {
    long userId = self.isJudge == 1 ? model.coachId : model.userId;
    if (userId == [UserInfoManager shared].getUserID) {
        [self showText:@"不能和自己聊天"];
        return;
    }
    NSString *name = self.isJudge == 1 ? model.coachName : model.nickname;
    NewsAllBuddyListModel *lisModel = [[NewsAllBuddyListModel alloc]init];
    lisModel.ID = userId;
    lisModel.nickname = name;
    ChatDetailsVC *vc = [[ChatDetailsVC alloc]init];
    vc.buddyModel = lisModel;
    [self.navigationController pushViewController:vc animated:NO];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    return CGSizeMake(KSW-30, 198);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
//{
//
//    return 20;//上下间隙
//}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//
//    return 15;//左右间隙
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MyOfCourseModel *model = [[MyOfCourseModel alloc]init];
    model = self.dataArray[indexPath.row];
    DetailsCompletedOrdersViewController *vc = [[DetailsCompletedOrdersViewController alloc]init];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
