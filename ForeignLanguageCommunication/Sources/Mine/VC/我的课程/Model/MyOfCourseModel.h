//
//  MyOfCourseModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyOfCourseModel : NSObject
/** 同意退款时间*/
@property (nonatomic, strong)NSString *agreeTime  ;
/** 同意退款时间*/
@property (nonatomic, strong)NSString *applyTime  ;
/** 教练id*/
@property (nonatomic, assign)NSInteger coachId  ;
/** 教练昵称*/
@property (nonatomic, strong)NSString *coachName  ;
/** 教练头像*/
@property (nonatomic, strong)NSString *coachUrl  ;
/** 课程id*/
@property (nonatomic, assign)NSInteger courseId ;
/** 课程图片*/
@property (nonatomic, strong)NSString *coursePic  ;
/** 创建时间*/
@property (nonatomic, strong)NSString *createTime  ;
/** 款项是否延迟发放1：正常，2：延迟，3：已发放*/
@property (nonatomic, assign)NSInteger delay  ;
/** 课程结束时间*/
@property (nonatomic, strong)NSString *endTime  ;
/** 是否已评价*/
@property (nonatomic)BOOL evaluation ;
/** 失效时间*/
@property (nonatomic, strong)NSString *failTime  ;
/** 订单id*/
@property (nonatomic, assign)NSInteger ID  ;
/** 课程名称*/
@property (nonatomic, strong)NSString *name  ;
/** 用户昵称*/
@property (nonatomic, strong)NSString *nickname  ;
/** 订单编号*/
@property (nonatomic, strong)NSString *orderNum  ;
/** 价格*/
@property (nonatomic, assign)NSInteger price  ;
/** 退款原因*/
@property (nonatomic, strong)NSString *reason  ;
/** 退款状态1：未退款，2：申请退款，3：退款成功，4：退款失败*/
@property (nonatomic, assign)NSInteger refund  ;
/** 课程开始时间*/
@property (nonatomic, strong)NSString *startTime  ;
/** 订单状态1：待上课，2：已上完*/
@property (nonatomic, strong)NSString *status  ;
/** 退款成功时间*/
@property (nonatomic, strong)NSString *successTime  ;
/** 课程时间*/
@property (nonatomic, assign)NSInteger time  ;
/** 用户id*/
@property (nonatomic, assign)NSInteger userId  ;
/** 用户头像*/
@property (nonatomic, strong)NSString *userUrl  ;

@end

NS_ASSUME_NONNULL_END
