//
//  MineHeadView.m
//  VoiceLive
//
//  Created by mac on 2020/8/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "MineHeadView.h"

@interface MineHeadView ()
@property (nonatomic, strong) UIImageView *avatarView;//头像
@property (nonatomic, strong) UILabel *nameLabel;//昵称
@property (nonatomic, strong) UILabel *IDLabel;//ID
@property (nonatomic, strong) UILabel *favoritesNumberLabel;//收藏夹
@property (nonatomic, strong) UILabel *attentionNumberLabel;//关注
@property (nonatomic, strong) UILabel *fanNumberLabel;//粉丝


@end
@implementation MineHeadView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
//- (instancetype)initWithFrame:(CGRect)frame {
//    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = UIColor.clearColor;
//        [self setUpSubView];
//    }
//    return self;
//}
- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = UIColor.clearColor;
        [self setUpSubView];
     
    }
    return self;
}
- (void)setModel:(MyPageModel *)model{
    _model = model;
    if ([[UserInfoManager shared] isLogin] == NO) {
        model = nil;
        [self.nameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.avatarView);
        }];
    }else{
        [self.avatarView sd_setImageWithURL:[NSURL URLWithString:model.headUrl]];
        self.nameLabel.text = model.nickName;
        self.IDLabel.text = [NSString stringWithFormat:@"ID:%@",model.sysId];
        
        self.attentionNumberLabel.text = [NSString stringWithFormat:@"%ld",model.collect];
        
        self.fanNumberLabel.text = [NSString stringWithFormat:@"%ld",model.follow];
        self.favoritesNumberLabel.text = [NSString stringWithFormat:@"%ld",model.fans];
        if (self.attentionNumberLabel.text.length==0) {
            self.attentionNumberLabel.text = @"0";
        }
        if (self.fanNumberLabel.text.length==0) {
               self.fanNumberLabel.text = @"0";
        }
        
        if (self.favoritesNumberLabel.text.length==0) {
              self.favoritesNumberLabel.text = @"0";
        }
    }
    
    NSLog(@"收藏夹===%ld",model.collect);
}
- (void)setUpSubView {
//    self.backgroundColor = [UIColor redColor];
    //顶部背景图片
    UIImageView *topImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"背景图"]];
    [self addSubview:topImageView];
    //计算图片适应尺寸之后的高度
    CGFloat h = kScaleSize(192);
    topImageView.contentMode = UIViewContentModeScaleToFill;
//    [topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.equalTo(self);
//        make.height.mas_equalTo(h);
//    }];
    topImageView.frame = CGRectMake(0, 0, KSW, h);
    //头像 圆的，白色边框
    self.avatarView = [[UIImageView alloc] init];
    self.avatarView.layer.cornerRadius = 12;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.borderColor = UIColor.whiteColor.CGColor;
    self.avatarView.image = [UIImage imageNamed:@"头像"];
    self.avatarView.layer.borderWidth =  2.0;
    [self addSubview:self.avatarView];
    self.avatarView.frame = CGRectMake(30, 74, 58, 58);
 
//    [self.avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.height.mas_equalTo(68);
//        make.left.mas_offset(30);
//        make.top.mas_offset(74);
//    }];
    
    //昵称label
    self.nameLabel = [UILabel new];
    [self addSubview:self.nameLabel];
    self.nameLabel.textColor = UIColor.whiteColor;
    self.nameLabel.backgroundColor = UIColor.clearColor;
    self.nameLabel.font = kFont_Bold(18);
    self.nameLabel.text = @"未登录";
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.avatarView.mas_right).mas_offset(15);
        make.top.mas_equalTo(self.avatarView.mas_top).mas_offset(8);
    }];
    
    //ID label
    self.IDLabel = [UILabel new];
    [self addSubview:self.IDLabel];
    self.IDLabel.font = kFont_Medium(12);
    self.IDLabel.backgroundColor = UIColor.clearColor;
    self.IDLabel.textColor = UIColor.whiteColor;
    self.IDLabel.text = @"";
    [self.IDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.nameLabel);
        make.bottom.mas_equalTo(self.avatarView.mas_bottom).mas_offset(-8);
    }];
    
    
//    //右边缘 设置
//    UIButton *setUpTheBtn = [[UIButton alloc]init];
//    [setUpTheBtn setImage:[UIImage imageNamed:@"我的-设置"] forState:(UIControlStateNormal)];
//    [self addSubview:setUpTheBtn];
//    [setUpTheBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self).offset(SBarHeight+10);
//        make.right.equalTo(self).offset(-15);
//    }];
//    [setUpTheBtn addTarget:self action:@selector(clickToInfo:) forControlEvents:UIControlEventTouchUpInside];
    //右边缘 个人资料
    UIButton *personalDataBtn = [[UIButton alloc]init];
    [personalDataBtn setImage:[UIImage imageNamed:@"我的-个人信息"] forState:(UIControlStateNormal)];
    [self addSubview:personalDataBtn];
    [personalDataBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(SBarHeight+10);
        make.right.equalTo(self).offset(-15);
    }];
    [personalDataBtn addTarget:self action:@selector(clickPersonalInformation:) forControlEvents:UIControlEventTouchUpInside];
  
    
    
    //圆角视图 收藏 关注 粉丝
    UIView *numberValueBackView = [UIView new];
    numberValueBackView.backgroundColor = UIColor.whiteColor;
    numberValueBackView.layer.cornerRadius = 12.0f;
    [self addSubview:numberValueBackView];
    
    
    
    [numberValueBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(77);
        make.leading.mas_offset(12.5);
        make.trailing.mas_offset(-12.5);
//        make.top.mas_equalTo(topImageView.mas_bottom).mas_offset(-40);
        make.centerY.equalTo(topImageView.mas_bottom);
//        make.bottom.mas_offset(-10);
    }];
   
    
    //收藏 关注 粉丝
    self.attentionNumberLabel = [self numberLabel];
    
    [numberValueBackView addSubview:self.attentionNumberLabel];
    
    self.fanNumberLabel = [self numberLabel];
    [numberValueBackView addSubview:self.fanNumberLabel];
    self.favoritesNumberLabel = [self numberLabel];
    [numberValueBackView addSubview:self.favoritesNumberLabel];
    
    [self.attentionNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(20);
        make.left.mas_offset(0);
        make.width.mas_equalTo(self.fanNumberLabel);
    }];
    
    [self.fanNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.attentionNumberLabel);
        make.left.mas_equalTo(self.attentionNumberLabel.mas_right);
        make.width.mas_equalTo(self.favoritesNumberLabel);
    }];
    [self.favoritesNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.attentionNumberLabel);
        make.left.mas_equalTo(self.fanNumberLabel.mas_right);
        make.right.mas_offset(0);
        make.width.mas_equalTo(self.attentionNumberLabel);
    }];
    
    NSArray *array = @[@"收藏夹",@"关注",@"粉丝"];
    CGFloat w = (KSW - 25) / 3;
    for (int i = 0; i < array.count; i ++) {
        UILabel *titleLabel = UILabel.new;
        titleLabel.textColor = RGBA(24, 24, 24, 1);
        titleLabel.font = kFont_Medium(12);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = array[i];
        [numberValueBackView addSubview:titleLabel];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_offset(-20);
            make.width.mas_equalTo(w);
            make.left.mas_offset(i*w);
        }];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundColor:UIColor.clearColor];
        [btn addTarget:self action:@selector(clickNumberBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i;
        [numberValueBackView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_offset(0);
            make.width.mas_equalTo(w);
            make.bottom.mas_offset(-20);
            make.left.mas_offset(i*w);
        }];
    }
//    //头像
//    UserModel *model =[[UserFile shareUser]getUserInfo];
//        NSLog(@"string===%@",model.user.avatarUrl);
//     [self.avatarView sd_setImageWithURL:[NSURL URLWithString:model.user.avatarUrl]];
//     //昵称
//    self.nameLabel.text = model.user.nickname;
//    //id
//    self.IDLabel.text = [NSString stringWithFormat:@"ID: %lu",model.user.sysid];
    
}

- (UILabel *)numberLabel{
    
    UILabel *label = UILabel.new;
    label.textColor = [UIColor colorWithHexString:@"#222222" alpha:1];
    label.font = kFont_Bold(18);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"0";
    return label;
}


- (void)clickNumberBtn:(UIButton *)sender {
    //点击了代表数值的按钮
    NSLog(@"点击了数值按钮");
    
    if (self.goToNumberInfoBlock) {
        self.goToNumberInfoBlock(sender.tag);
    }
}

- (void)clickPersonalInformation:(UIButton *)sender{
    //点击跳转个人信息
    if (self.gotoInfoBlock) {
        self.gotoInfoBlock();
    }
}
//- (void)clickToInfo:(UIButton *)sender{
//    //点击跳转设置
//    if (self.goSetUpBock) {
//        self.goSetUpBock();
//    }
//}
@end
