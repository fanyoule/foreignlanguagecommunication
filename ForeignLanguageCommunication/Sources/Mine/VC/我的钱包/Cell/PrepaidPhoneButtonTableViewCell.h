//
//  PrepaidPhoneButtonTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PrepaidPhoneButtonTableViewCell : UITableViewCell
@property (nonatomic, strong)UIButton *prepaidBtn;
@end

NS_ASSUME_NONNULL_END
