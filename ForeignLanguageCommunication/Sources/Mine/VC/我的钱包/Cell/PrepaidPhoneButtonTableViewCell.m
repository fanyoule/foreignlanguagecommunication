//
//  PrepaidPhoneButtonTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//

#import "PrepaidPhoneButtonTableViewCell.h"
@interface PrepaidPhoneButtonTableViewCell ()



@end
@implementation PrepaidPhoneButtonTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.prepaidBtn];
    [self.prepaidBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.contentView).offset(15);
        make.bottom.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@50);
    }];
}
- (UIButton *)prepaidBtn{
    if (!_prepaidBtn) {
        _prepaidBtn = [[UIButton alloc]init];
        [_prepaidBtn setTitle:@"充值" forState:(UIControlStateNormal)];
        [_prepaidBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _prepaidBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _prepaidBtn.layer.cornerRadius = 6;
        _prepaidBtn.clipsToBounds = YES;
    }
    return _prepaidBtn;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
