//
//  TheWalletDetailsTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import <UIKit/UIKit.h>
#import "TheWalletDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TheWalletDetailsTableViewCell : UITableViewCell

@property (nonatomic, strong)TheWalletDetailsModel *model;

@end

NS_ASSUME_NONNULL_END
