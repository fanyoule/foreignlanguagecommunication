//
//  IncomeTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IncomeTableViewCell : UITableViewCell
/** 标题*/
@property (nonatomic, strong)UILabel *titlelabel;
/** 总收入*/
@property (nonatomic, strong)UILabel *moneyLabel;
/** 提现按钮*/
@property (nonatomic, strong)UIButton *withdrawalBtn;
@end

NS_ASSUME_NONNULL_END
