//
//  TopUpAmountTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopUpAmountTableViewCell : UITableViewCell
/** 金额*/
@property (nonatomic, strong)UILabel *amountLabel;
@end

NS_ASSUME_NONNULL_END
