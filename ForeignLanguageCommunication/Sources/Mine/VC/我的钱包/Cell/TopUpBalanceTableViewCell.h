//
//  TopUpBalanceTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import <UIKit/UIKit.h>
#import "DetailsOfMyWalletModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol TopUpBalanceTableViewCellDelegate<NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)clickTopUpBalanceTableViewCell:(UIButton *)button SelectedAmount:(NSString *)amount;

@end

@interface TopUpBalanceTableViewCell : UITableViewCell

@property (nonatomic, strong)DetailsOfMyWalletModel *model;

@property (nonatomic, weak) id<TopUpBalanceTableViewCellDelegate> cellDelegate;

/** 充值比例*/
@property (nonatomic, assign)NSInteger exGoldRate;


/** 其他金额*/
@property (nonatomic, strong) UITextField *textField;
/** 协议*/
@property (nonatomic, strong) UILabel *agreementLabel;

- (void)updataCell;
@end

NS_ASSUME_NONNULL_END
