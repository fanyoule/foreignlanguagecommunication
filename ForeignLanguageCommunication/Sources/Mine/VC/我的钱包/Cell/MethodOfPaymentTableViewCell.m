//
//  MethodOfPaymentTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "MethodOfPaymentTableViewCell.h"
@interface MethodOfPaymentTableViewCell ()

/** 选中按钮*/
@property (nonatomic, strong)UIButton *selectedBtn;
/** 线*/
@property (nonatomic, strong)UIView *lineView;
@end
@implementation MethodOfPaymentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.imageV];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.selectedBtn];
    [self.contentView addSubview:self.lineView];
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.imageV.mas_right).offset(10);
    }];
    [self.selectedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
    }];
    
}
- (UIImageView *)imageV{
    if (!_imageV) {
        _imageV = [[UIImageView alloc]init];
        _imageV.image = [UIImage imageNamed:@"微信"];
        
    }
    return _imageV;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"微信支付";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(14);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UIButton *)selectedBtn{
    if (!_selectedBtn) {
        _selectedBtn = [[UIButton alloc]init];
        [_selectedBtn setImage:[UIImage imageNamed:@"未选中"] forState:(UIControlStateNormal)];
        [_selectedBtn setImage:[UIImage imageNamed:@"选中"] forState:(UIControlStateSelected)];
        [_selectedBtn addTarget:self action:@selector(checkThe:) forControlEvents:(UIControlEventTouchDown)];
    }
    return _selectedBtn;
}

-(UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}


-(void)checkThe:(UIButton *)button{
    button.selected = !button.isSelected;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
