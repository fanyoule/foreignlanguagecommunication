//
//  TopUpBalanceTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "TopUpBalanceTableViewCell.h"

@interface TopUpBalanceTableViewCell  ()



/** 规则*/
@property (nonatomic, strong) UILabel *rulesLabel;

@end

@implementation TopUpBalanceTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    
    [self.contentView addSubview:self.rulesLabel];
    [self.contentView addSubview:self.agreementLabel];
    [self.contentView addSubview:self.textField];
    
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_centerX).offset(20);
        make.bottom.equalTo(self.contentView).offset(-15);
    }];
    [self.agreementLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.rulesLabel.mas_right);
        make.centerY.equalTo(self.rulesLabel);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@44);
        make.bottom.equalTo(self.rulesLabel.mas_top).offset(-15).priority(600);
        make.top.equalTo(self.contentView).offset(220);
    }];
}
- (void)updataCell {
    self.textField.hidden = YES;
    
    [self.rulesLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_centerX).offset(20);
        make.bottom.equalTo(self.contentView).offset(-15);
    }];
    [self.agreementLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.rulesLabel.mas_right);
        make.centerY.equalTo(self.rulesLabel);
    }];
    [self.textField mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0);
        make.bottom.equalTo(self.rulesLabel.mas_top).offset(-15).priority(600);
        make.top.equalTo(self.contentView).offset(220);
    }];
}
- (void)setExGoldRate:(NSInteger)exGoldRate{
    _exGoldRate = exGoldRate;
}

-(void)setModel:(DetailsOfMyWalletModel *)model{
    
    CGFloat w = (KSW- 50)/3;
    
    if (model.location6 == 0) {
        
    } else {
        NSArray *array = @[@(model.location1),@(model.location2),@(model.location3),@(model.location4),@(model.location5),@(model.location6)];
        for (int i = 0; i < array.count; i ++) {
            UIButton *btn = [[UIButton alloc]init];
            btn.tag = 600 + i;
            btn.backgroundColor = RGBA(245, 245, 245, 1);
            btn.layer.cornerRadius = 6;
            btn.clipsToBounds = YES;
            NSString *Amount = array[i];
            [btn whenTapped:^{
                [self selectedRechargeAmount:btn SelectedAmount:Amount];
            }];
            [self.contentView addSubview:btn];
            
            NSInteger col = i %3;
            
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.contentView).offset((i/3)*100+10);
                make.left.equalTo(self.contentView).offset(col*w+15+col*10);
                make.width.equalTo(@(w));
                make.height.equalTo(@90);
            }];
            
            int q = [array[i] intValue] * 7;
            NSString *location = [NSString stringWithFormat:@"%d",q];
            NSLog(@"e币 === %@",location);
            UILabel *numberLabel = [[UILabel alloc]init];
            numberLabel.text = location;
            numberLabel.textColor = RGBA(51, 51, 51, 1);
            numberLabel.font = kFont_Bold(22);
            numberLabel.textAlignment = 1;
            [btn addSubview:numberLabel];
            [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(btn).offset(5);
                make.top.equalTo(btn).offset(15);
            }];
            
            UIImageView *EImageV = UIImageView.new;
            EImageV.image = [UIImage imageNamed:@"钱包-钱"];
            [btn addSubview:EImageV];
            [EImageV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(numberLabel);
                make.right.equalTo(numberLabel.mas_left);
                make.width.height.equalTo(@18);
            }];
            
            UILabel *amount = [[UILabel alloc]init];
            amount.text = [NSString stringWithFormat:@"%@元",Amount];
            amount.textColor = RGBA(102, 102, 102, 1);
            amount.font = kFont_Regular(13);
            amount.textAlignment = 1;
            [btn addSubview:amount];
            [amount mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(btn);
                make.top.equalTo(numberLabel.mas_bottom).offset(5);
            }];
        }
    }
}


-(void)selectedRechargeAmount:(UIButton *)button SelectedAmount:(NSString *)amount{
    
    if ([self.cellDelegate respondsToSelector:@selector(clickTopUpBalanceTableViewCell:SelectedAmount:)]) {
        [self.cellDelegate clickTopUpBalanceTableViewCell:button SelectedAmount:amount];
    }
    
}



- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"充值代表已阅读并同意";
        _rulesLabel.textColor = RGBA(102, 120, 102, 1);
        _rulesLabel.font = kFont_Regular(11);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}
- (UILabel *)agreementLabel{
    if (!_agreementLabel) {
        _agreementLabel = [[UILabel alloc]init];
        _agreementLabel.text = @"《用户充值协议》";
        _agreementLabel.textColor = RGBA(52, 120, 245, 1);
        _agreementLabel.font = kFont_Regular(11);
        _agreementLabel.textAlignment = 0;
    }
    return _agreementLabel;
}

- (UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc]init];
        _textField.keyboardType = UIKeyboardTypeDecimalPad;
        _textField.backgroundColor = RGBA(245, 245, 245, 1);
        _textField.layer.cornerRadius = 6;
        _textField.clipsToBounds = YES;
        _textField.placeholder = @"其他金额";
        _textField.font = kFont_Regular(13);
        
    }
    return _textField;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
