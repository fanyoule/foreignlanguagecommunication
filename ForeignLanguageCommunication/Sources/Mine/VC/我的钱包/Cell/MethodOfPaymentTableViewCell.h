//
//  MethodOfPaymentTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MethodOfPaymentTableViewCell : UITableViewCell
/** 图片*/
@property (nonatomic, strong)UIImageView *imageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
@end

NS_ASSUME_NONNULL_END
