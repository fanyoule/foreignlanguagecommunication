//
//  IncomeTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//

#import "IncomeTableViewCell.h"
@interface IncomeTableViewCell ()

/** 线*/
@property (nonatomic, strong)UIView *lineView;
@end
@implementation IncomeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.titlelabel];
    [self.contentView addSubview:self.moneyLabel];
    [self.contentView addSubview:self.withdrawalBtn];
    [self.contentView addSubview:self.lineView];
    
    [self.titlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.titlelabel.mas_right).offset(20);
    }];
    [self.withdrawalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-20);
        make.width.equalTo(@82);
        make.height.equalTo(@30);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(1).priority(600);
        make.left.equalTo(self.titlelabel);
        make.right.equalTo(self.withdrawalBtn);
        make.height.equalTo(@0.5);
        make.top.equalTo(self.contentView).offset(50);
    }];
    
    
}

- (UILabel *)titlelabel{
    if (!_titlelabel) {
        _titlelabel = [[UILabel alloc]init];
        _titlelabel.text = @"我的收入";
        _titlelabel.textColor = RGBA(51, 51, 51, 1);
        _titlelabel.font = kFont(15);
        _titlelabel.textAlignment = 0;
    }
    return _titlelabel;
}
- (UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc]init];
        _moneyLabel.text = @"￥0.00";
        _moneyLabel.textColor = RGBA(51, 51, 51, 1);
        _moneyLabel.font = kFont_Bold(17);
        _moneyLabel.textAlignment = 0;
    }
    return _moneyLabel;
}
- (UIButton *)withdrawalBtn{
    if (!_withdrawalBtn) {
        _withdrawalBtn = [[UIButton alloc]init];
        _withdrawalBtn.backgroundColor = RGBA(246, 247, 250, 1);
        [_withdrawalBtn setTitle:@"提现" forState:(UIControlStateNormal)];
        [_withdrawalBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _withdrawalBtn.layer.cornerRadius = 5;
        _withdrawalBtn.clipsToBounds = YES;
    }
    return _withdrawalBtn;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(231, 231, 231, 1);
    }
    return _lineView;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
