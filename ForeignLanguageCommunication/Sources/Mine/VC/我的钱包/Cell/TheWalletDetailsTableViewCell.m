//
//  TheWalletDetailsTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//  

#import "TheWalletDetailsTableViewCell.h"
@interface TheWalletDetailsTableViewCell ()
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 时间*/
@property (nonatomic, strong)UILabel *timeLabel;
/** 金额*/
@property (nonatomic, strong)UILabel *amountLabel;
/** 状态*/
@property (nonatomic, strong)UILabel *stateLabel;
/** 线*/
@property (nonatomic, strong)UIView *lineView;

@end
@implementation TheWalletDetailsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.amountLabel];
    [self.contentView addSubview:self.stateLabel];
    [self.contentView addSubview:self.lineView];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_centerY).offset(-5);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_centerY).offset(5);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_centerY).offset(-5);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_centerY).offset(5);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
    }];
    
}

- (void)setModel:(TheWalletDetailsModel *)model{
    if (model.tradeType == 0) {
        self.titleLabel.text = @"购买课程";
    }else if (model.tradeType == 1){
        self.titleLabel.text = @"购买会员(首充)";
    }else if (model.tradeType == 2){
        self.titleLabel.text = @"匹配";
    }else if (model.tradeType == 3){
        self.titleLabel.text = @"提现";
    }else if (model.tradeType == 4){
        self.titleLabel.text = @"视频";
    }else if (model.tradeType == 5){
        self.titleLabel.text = @"直系收益";
    }else if (model.tradeType == 6){
        self.titleLabel.text = @"退款";
    }else if (model.tradeType == 7){
        self.titleLabel.text = @"购买会员（复购）";
    }else if (model.tradeType == 8){
        self.titleLabel.text = @"充值金币";
    }else if (model.tradeType == 9){
        self.titleLabel.text = @"礼物收益";
    }else if (model.tradeType == 10){
        self.titleLabel.text = @"礼物支出";
    }else if (model.tradeType == 11){
        self.titleLabel.text = @"二级收益";
    }else if (model.tradeType == 12){
        self.titleLabel.text = @"语音";
    }else if (model.tradeType == 13){
        self.titleLabel.text = @"聊天";
    }else if (model.tradeType == 14){
        self.titleLabel.text = @"分销收益";
    }else if (model.tradeType == 15){
        self.titleLabel.text = @"创建房间";
    }else if (model.tradeType == 16){
        self.titleLabel.text = @"续租房间";
    }else if (model.tradeType == 17){
        self.titleLabel.text = @"课程收益";
    }
    
    self.timeLabel.text = model.createTime;
    NSString *currency = nil;
    if (model.currency == 0) {
        currency = @"人民币";
    }else{
        currency = @"E币";
    }
    NSString *type = nil;
    if (model.type == 0) {
        type = @"+";
    }else{
        type = @"-";
    }
    
    self.amountLabel.text = [NSString stringWithFormat:@"%@%0.2f%@",type,model.number,currency];
    if (model.status == 0) {
        self.stateLabel.text = @"处理中";
    }else if (model.status == 1){
        self.stateLabel.text = @"审核通过";
    }else if (model.status == 2){
        self.stateLabel.text = @"审核未通过";
    }else if (model.status == 3){
        self.stateLabel.hidden = YES;
    }
    
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"课程名称";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(14);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.text = @"2020-06-10";
        _timeLabel.textColor = RGBA(24, 24, 24, 1);
        _timeLabel.font = kFont_Medium(12);
        _timeLabel.textAlignment = 0;
    }
    return _timeLabel;
}
- (UILabel *)amountLabel{
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc]init];
        _amountLabel.text = @"+1100金币";
        _amountLabel.textColor = RGBA(24, 24, 24, 1);
        _amountLabel.font = kFont_Medium(14);
        _amountLabel.textAlignment = 2;
    }
    return _amountLabel;
}
- (UILabel *)stateLabel{
    if (!_stateLabel) {
        _stateLabel = [[UILabel alloc]init];
        _stateLabel.text = @"已到账";
        _stateLabel.textColor = RGBA(52, 120, 245, 1);
        _stateLabel.font = kFont_Medium(12);
        _stateLabel.textAlignment = 2;
    }
    return _stateLabel;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
