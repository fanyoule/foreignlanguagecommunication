//
//  TopUpAmountTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "TopUpAmountTableViewCell.h"
@interface TopUpAmountTableViewCell ()
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;

/** 元*/
@property (nonatomic, strong)UILabel *yuanLabel;
@end
@implementation TopUpAmountTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.backView];
    
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.amountLabel];
    [self.contentView addSubview:self.yuanLabel];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.bottom.right.equalTo(self.contentView).priority(600);
        make.height.equalTo(@50);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(15);
    }];
    [self.yuanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.right.equalTo(self.backView).offset(-15);
    }];
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.right.equalTo(self.yuanLabel.mas_left).offset(-5);
    }];
    
    
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = UIColor.whiteColor;
    }
    return _backView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"充值金额";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(14);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UILabel *)amountLabel{
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc]init];
        _amountLabel.text = @"0";
        _amountLabel.textColor = RGBA(251, 113, 154, 1);
        _amountLabel.font = kFont_Medium(14);
        _amountLabel.textAlignment = 2;
    }
    return _amountLabel;
}
- (UILabel *)yuanLabel{
    if (!_yuanLabel) {
        _yuanLabel = [[UILabel alloc]init];
        _yuanLabel.text = @"元";
        _yuanLabel.textColor = RGBA(24, 24, 24, 1);
        _yuanLabel.font = kFont_Medium(14);
        _yuanLabel.textAlignment = 2;
    }
    return _yuanLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
