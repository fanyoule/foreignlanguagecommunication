//
//  MethodOfPaymentPopupWindow.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import "MethodOfPaymentPopupWindow.h"

@interface MethodOfPaymentPopupWindow ()
@property (nonatomic, strong)UIView *backView;

@property (nonatomic, strong) UILabel *titelLabel;
@end

@implementation MethodOfPaymentPopupWindow

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentSizeInPop = CGSizeMake(CGRectGetWidth([UIScreen mainScreen].bounds) ,207);
     
    self.popController.containerView.layer.cornerRadius = 9;
    self.popController.containerView.backgroundColor = [UIColor whiteColor];
       
    [self addUI];
}
-(void)addUI{
    [self.view addSubview:self.titelLabel];
    [self.titelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(self.view.mas_top).offset(20);
    }];
    NSArray *imageArray = @[@"微信",@"支付宝"];
    NSArray *titleArray = @[@"微信账户",@"支付宝账户"];
    for (int i = 0; i<titleArray.count; i++) {
        UIView *bgView = [[UIView alloc]init];
        bgView.tag = 200+i;
        [self.view addSubview:bgView];
        [bgView whenTapped:^{
            [self clickView:bgView];
        }];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.view);
            make.top.mas_equalTo(self.view.mas_top).offset(50*i+59);
            make.height.mas_offset(50);
        }];
        UIImageView *photoImageV = [[UIImageView alloc]init];
        photoImageV.image = kImage(imageArray[i]);
        photoImageV.contentMode = UIViewContentModeScaleAspectFill;
        [bgView addSubview:photoImageV];
        [photoImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(bgView.mas_left).offset(16);
            make.centerY.mas_equalTo(bgView.mas_centerY);
            make.height.width.mas_offset(25);
        }];
        UILabel *label =[[UILabel alloc]init];
        label.text = titleArray[i];
        label.textColor = rgba(34, 34, 34, 1);
        label.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:14];
        [bgView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(bgView.mas_centerY);
            make.left.mas_equalTo(photoImageV.mas_right).offset(15);
            
        }];
        UIImageView *clickImageV = [[UIImageView alloc]init];
        clickImageV.contentMode = UIViewContentModeScaleAspectFill;
        clickImageV.tag = 100+i;
        [bgView addSubview:clickImageV];
        [clickImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(bgView.mas_right).offset(-15);
            make.centerY.mas_equalTo(bgView.mas_centerY);
            make.height.mas_offset(12);
            make.width.mas_offset(18);
        }];
        UILabel *linLabel = [[UILabel alloc]init];
        linLabel.backgroundColor = rgba(223, 223, 223, 0.8);
        [bgView addSubview:linLabel];
        [linLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(bgView.mas_left).offset(16);
            make.right.mas_equalTo(bgView.mas_right);
            make.bottom.mas_equalTo(bgView.mas_bottom).offset(-1);
            make.height.mas_offset(0.6);
        }];
        if (i==2) {
            linLabel.backgroundColor = [UIColor clearColor];
        }
        if ([titleArray[i] isEqualToString:self.selected]) {
            clickImageV.image = kImage(@"my_gouxuan");
        }
        
    }
     
}
-(void)clickView:(UIView *)sender{
    for (int i = 0; i<2; i++) {
        UIImageView *imageV = (UIImageView *)[self.view viewWithTag:100+i];
        imageV.image = kImage(@"");
    }
    UIImageView *imageSendV = (UIImageView *)[self.view viewWithTag:sender.tag-100];
    imageSendV.image = kImage(@"my_gouxuan");
     NSArray *titleArray = @[@"微信账户",@"支付宝账户"];
    if ([self.delegate respondsToSelector:@selector(clickMethodOfPaymentPopupWindow:)]) {
        [self.delegate clickMethodOfPaymentPopupWindow:titleArray[sender.tag-200]];
    }
}
- (UILabel *)titelLabel {
    if (!_titelLabel) {
        _titelLabel = [[UILabel alloc]init];
        _titelLabel.text = @"提现方式";
        _titelLabel.textColor = rgba(34, 34, 34, 1);
        _titelLabel.font =  [UIFont fontWithName:@"PingFangSC-Semibold" size:15];
    }
    return _titelLabel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
