//
//  MethodOfPaymentPopupWindow.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol MethodOfPaymentPopupWindowDelegate <NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)clickMethodOfPaymentPopupWindow:(NSString *)string;

@end
@interface MethodOfPaymentPopupWindow : UIViewController
/** 选中的 */
@property (nonatomic, strong) NSString *selected;
@property(nonatomic, weak) id<MethodOfPaymentPopupWindowDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
