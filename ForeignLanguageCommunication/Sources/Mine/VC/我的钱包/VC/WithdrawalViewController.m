//
//  WithdrawalViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//  提现

#import "WithdrawalViewController.h"
#import "CoachCertificationInformationTableViewCell.h"
#import "ModifyThePhoneNumberViewController.h"
#import "MethodOfPaymentPopupWindow.h"// 提现方式弹窗
@interface WithdrawalViewController ()<UITableViewDelegate,UITableViewDataSource,MethodOfPaymentPopupWindowDelegate,UITextFieldDelegate>
/** 真实姓名*/
@property (nonatomic, strong)NSString *name;
/** 提现金额*/
@property (nonatomic, assign)NSInteger withdrawal;
@property (nonatomic, assign) CGFloat fee;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UILabel *feeLab;
@property (nonatomic, assign) NSInteger selectType;
@end

@implementation WithdrawalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fee = 0.0;
    self.selectType = 1; // 1: 选择的微信 2: 选择的支付宝
    self.navTitleString = @"提现";
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    self.mainTableView.tableHeaderView = self.headerView;
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    [self getData];
}
- (void)getData {
    kWeakSelf(self)
    [RequestManager geWithdrawFeeSuccess:^(id  _Nullable response) {
        weakself.fee = [response[@"data"] floatValue];
        weakself.feeLab.text = [NSString stringWithFormat:@"提现手续费%.1f%%", weakself.fee];
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"***");
    }];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [self.mainTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 3;
    }else{
        return 2;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CoachCertificationInformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoachCertificationInformationTableViewCell"];
            if (!cell) {
                cell = [[CoachCertificationInformationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CoachCertificationInformationTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                cell.titleLabel.text = @"真实姓名";
                cell.textField.placeholder = @"请填写真实姓名";
                cell.textField.tag = 200;
                cell.textField.delegate = self;
                [cell.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
            }else if (indexPath.row == 1){
                cell.titleLabel.text = @"提现方式";
                cell.textField.text = @"微信账户";
                cell.textField.enabled = NO;
                cell.textField.tag = 300;
                UIImageView *arrow = [[UIImageView alloc]init];
                arrow.image = [UIImage imageNamed:@"个人资料-箭头"];
                [cell addSubview:arrow];
                [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(cell);
                    make.right.equalTo(cell).offset(-15);
                }];
                [cell whenTapped:^{
                    //选择账号类型
                    //0 微信 1 支付宝
                    MethodOfPaymentPopupWindow *VC = [MethodOfPaymentPopupWindow new];
                    VC.selected = cell.textField.text;
                    VC.delegate= self;
                    HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                                    popController.backgroundAlpha = 0.6;//背景色透明度
                                    popController.animationDuration = 0.3;
                                    popController.popPosition = 2;
                                    popController.popType = 5;
                                    popController.dismissType = 5;
                                    popController.shouldDismissOnBackgroundTouch = YES;
                                    [popController presentInViewController:self];
                }];
                
                
                
            }else if (indexPath.row == 2){
                cell.titleLabel.text = @"提现账号";
                if ([[UserInfoManager shared] wx].length <= 0) {
                    cell.textField.text = @"绑定的微信账号(未绑定)";
                }else{
                    cell.textField.text = [[UserInfoManager shared] wx];
                }
//                cell.textField.text = @"绑定的微信账号";
                cell.textField.tag = 400;
                cell.textField.enabled = NO;
                UIImageView *arrow = [[UIImageView alloc]init];
                arrow.image = [UIImage imageNamed:@"个人资料-箭头"];
                [cell addSubview:arrow];
                [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(cell);
                    make.right.equalTo(cell).offset(-15);
                }];
                [cell whenTapped:^{
                    if (self.selectType == 1 && [[UserInfoManager shared] wx].length <= 0) {
                        NSLog(@"绑定微信");
                        NSInteger binding = 1;
                        ModifyThePhoneNumberViewController *vc = [[ModifyThePhoneNumberViewController alloc]init];
                        vc.isBinding = binding;
                        vc.isJudge = 2;
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                }];
            }
        }
        return cell;
    }else{
        CoachCertificationInformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoachCertificationInformationTableViewCell"];
            if (!cell) {
                cell = [[CoachCertificationInformationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CoachCertificationInformationTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                cell.titleLabel.text = @"余额";
                cell.textField.text = [NSString stringWithFormat:@"%ldE币",self.balance];
                cell.textField.enabled = NO;
            }else if (indexPath.row == 1){
                cell.titleLabel.text = @"提现金额";
                cell.textField.placeholder = [NSString stringWithFormat:@"%ldE币",self.balance];
                cell.textField.keyboardType = UIKeyboardTypeDecimalPad;
                cell.textField.tag = 500;
                cell.textField.delegate = self;
                [cell.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
            }
        }
        return cell;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
      
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.clearColor;
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return nil;
    }else{
        UIView *view = UIView.new;
        view.backgroundColor = UIColor.clearColor;
        UIButton *withdrawal = [[UIButton alloc]init];
        [withdrawal setTitle:@"提现" forState:(UIControlStateNormal)];
        [withdrawal setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        withdrawal.backgroundColor = RGBA(52, 120, 245, 1);
        withdrawal.titleLabel.font = kFont_Bold(15);
        withdrawal.layer.cornerRadius = 6;
        withdrawal.clipsToBounds = YES;
        [withdrawal addTarget:self action:@selector(determineTheWithdrawal) forControlEvents:(UIControlEventTouchDown)];
        [view addSubview:withdrawal];
        [withdrawal mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(view);
            make.left.equalTo(view).offset(15);
            make.right.equalTo(view).offset(-15);
            make.height.equalTo(@50);
        }];
        return view;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else{
        return 110;
    }
    
}
// MARK: 提现
-(void)determineTheWithdrawal {
    if (self.name.length <= 0) {
        [self showText:@"请输入真实姓名"];
        return;
    }
    if (self.withdrawal == 0){
        [self showText:@"请输入提现金额"];
        return;
    }
    
    NSLog(@"点击提现");
    UITextField *textField = (UITextField*)[self.view viewWithTag:300];
    if ([textField.text isEqualToString:@"支付宝账户"]) {
        [SVProgressHUD showWithStatus:@""];
        [RequestManager alipayAddWalletWithdrawWithId:[[UserInfoManager shared] getUserID] account:[[UserInfoManager shared] phone] name:self.name type:self.type number:self.withdrawal withSuccess:^(id  _Nullable response) {
            NSLog(@"提现成功  ==== %@",response);
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"提现成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [self showText:response[@"msg"]];
            }
            [SVProgressHUD dismiss];
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"提现失败  ==== %@",error.userInfo);
            [SVProgressHUD dismiss];
            [self showText:error.localizedDescription];
            
        }];
    }else{
        [SVProgressHUD showWithStatus:@""];
        [RequestManager wxpayAddWalletWithdrawWithId:[[UserInfoManager shared] getUserID] account:[[UserInfoManager shared] phone] type:self.type number:self.withdrawal withSuccess:^(id  _Nullable response) {
            [SVProgressHUD dismiss];
            [self showText:@"提现成功"];
            [self.navigationController popViewControllerAnimated:YES];
        } withFail:^(NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            [self showText:error.localizedDescription];
        }];
    }
    
}
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField.tag == 200) {
        self.name = textField.text;
    }
    if (textField.tag == 500) {
        self.withdrawal = [textField.text integerValue];
    }
    
}


- (void)clickMethodOfPaymentPopupWindow:(NSString *)string{
    
    UITextField *textField = (UITextField*)[self.view viewWithTag:300];
    textField.text = string;
    
    UITextField *textField2 = (UITextField*)[self.view viewWithTag:400];
    if ([string isEqualToString:@"微信账户"]) {
        self.selectType = 1;
        if ([[UserInfoManager shared] wx].length <= 0) {
            textField2.text = @"绑定的微信账号(未绑定)";
        }else{
            textField2.text = [[UserInfoManager shared] wx];
        }
//        textField2.text = @"绑定的微信账号";
    } else {
        self.selectType = 2;
        textField2.text = [[UserInfoManager shared] phone];
    }
    
}

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KSW, 30)];
        _headerView.backgroundColor = [UIColor colorWithHexString:@"#3478F5" alpha:0.5];
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, KSW-20, 30)];
        lab.font = kFont(13);
        lab.textColor = kTHEMECOLOR;
//        lab.text = [NSString stringWithFormat:@"提现手续费%.1f%%", self.fee];
        [_headerView addSubview:lab];
        self.feeLab = lab;
    }
    return _headerView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
