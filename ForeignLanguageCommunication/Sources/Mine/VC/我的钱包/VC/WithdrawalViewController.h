//
//  WithdrawalViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawalViewController : BaseViewController
/** 余额*/
@property (nonatomic, assign)NSInteger balance;
/** 提现类型*/
@property (nonatomic, assign)int type;

@end

NS_ASSUME_NONNULL_END
