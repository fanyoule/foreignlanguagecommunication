//
//  TheWalletDetailsViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TheWalletDetailsViewController : BaseViewController
/** 判断 0：充值明细 ， 1：代理明细 ， 2：课程明细*/
@property (nonatomic, assign)int isJudge;

@end

NS_ASSUME_NONNULL_END
