//
//  TheWalletDetailsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//  明细

#import "TheWalletDetailsViewController.h"
#import "TheWalletDetailsTableViewCell.h"
#import "TransactionDetailsViewController.h"//交易详情
#import "MyPointsTransactionDetailsOtherVC.h"//


#import "TheWalletDetailsModel.h"
@interface TheWalletDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)NSMutableArray *dataArray;

@end

@implementation TheWalletDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isJudge == 0) {
        self.navTitleString = @"充值明细";
    }else if (self.isJudge == 1){
        self.navTitleString = @"代理明细";
    }else if (self.isJudge == 2){
        self.navTitleString = @"课程明细";
    }
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    [self getData];
    // Do any additional setup after loading the view.
}
// MARK: 明细查询
-(void)getData{
    
    [RequestManager getWalletLogById:[[UserInfoManager shared] getUserID] type:self.isJudge withSuccess:^(id  _Nullable response) {
        NSLog(@"明细查询 === %@",response);
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                TheWalletDetailsModel *model = [TheWalletDetailsModel mj_objectWithKeyValues:array[i]];
                [self.dataArray addObject:model];
            }
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TheWalletDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheWalletDetailsTableViewCell"];
        if (!cell) {
            cell = [[TheWalletDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheWalletDetailsTableViewCell"];
        }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    
    TheWalletDetailsModel *model = [[TheWalletDetailsModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TheWalletDetailsModel *model = [[TheWalletDetailsModel alloc]init];
    model = self.dataArray[indexPath.row];
    
    TransactionDetailsViewController *vc = [[TransactionDetailsViewController alloc]init];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];

    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
        UIView *view = UIView.new;
        view.backgroundColor = UIColor.clearColor;
        return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 10;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
