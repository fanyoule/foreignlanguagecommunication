//
//  TransactionDetailsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import "TransactionDetailsViewController.h"
#import "WithdrawalHeadTableViewCell.h"
#import "WithdrawalStateTableViewCell.h"
#import "TypeTheDetailsTableViewCell.h"
@interface TransactionDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation TransactionDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"明细详情";
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    // Do any additional setup after loading the view.
}

-(void)setModel:(TheWalletDetailsModel *)model{
    _model = model;
    [self.mainTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.model.tradeType == 3) {
        return 3;
    }
    return 2;
    
}
// cell 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 140;
    }else if (indexPath.row == 1){
        if (self.model.tradeType == 3) {
            return 205;
        }else{
            return 150;
        }
    }else{
        return 150;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1) {
        if (self.model.tradeType == 3) {
            WithdrawalStateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WithdrawalStateTableViewCell"];
            if (!cell) {
                cell = [[WithdrawalStateTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WithdrawalStateTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor clearColor];
            cell.model = self.model;
            return cell;
        }else{
            TypeTheDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TypeTheDetailsTableViewCell"];
            if (!cell) {
                cell = [[TypeTheDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TypeTheDetailsTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor clearColor];
            cell.model = self.model;
            return cell;
        }
    }else if (indexPath.row == 2){
        TypeTheDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TypeTheDetailsTableViewCell"];
        if (!cell) {
            cell = [[TypeTheDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TypeTheDetailsTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor clearColor];
        cell.model = self.model;
        return cell;
    }
    
    
    
    WithdrawalHeadTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WithdrawalHeadTableViewCell"];
    if (!cell) {
        cell = [[WithdrawalHeadTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WithdrawalHeadTableViewCell"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor clearColor];
    cell.model = self.model;
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
