//
//  MyPointsTransactionDetailsVC.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyPointsTransactionDetailsVC : UIViewController
/** 订单流水号 */
@property (nonatomic, strong) NSString *orderId;
@end

NS_ASSUME_NONNULL_END
