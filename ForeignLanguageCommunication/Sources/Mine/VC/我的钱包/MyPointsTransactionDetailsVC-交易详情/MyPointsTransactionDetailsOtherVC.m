//
//  MyPointsTransactionDetailsOtherVC.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "MyPointsTransactionDetailsOtherVC.h"

@interface MyPointsTransactionDetailsOtherVC ()
@property (nonatomic,strong)UILabel    *labTitle;
/** 积分number */
@property (nonatomic, strong) UILabel *integralLabel;

@end

@implementation MyPointsTransactionDetailsOtherVC
-(void)addNav{
    UIView *nav = UIView.new;
    nav.frame = CGRectMake(0, 0, KSW, TOP_SPACE88or64);
    nav.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:nav];
    self.labTitle = UILabel.new;
    self.labTitle.frame = CGRectMake(KSW/2-60, TOP_SPACE20or44, 120, 44);
    self.labTitle.font = kFont_Medium(18);
    self.labTitle.textColor =[UIColor blackColor];
    self.labTitle.text = @"明细详情";
    self.labTitle.textAlignment = 0;
    
    [nav addSubview:self.labTitle];
    UIButton *btnLeft = UIButton.new;
    btnLeft.frame = CGRectMake(5, TOP_SPACE20or44, 80, 44);
    
    btnLeft.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [btnLeft setImage:[UIImage imageNamed:@"icon_nav_back"] forState:(UIControlStateNormal)];
    [btnLeft setImageEdgeInsets:(UIEdgeInsetsMake(1, 15, 10, 40))];
    
    btnLeft.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [nav addSubview:btnLeft];

    [btnLeft whenTapped:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];

 
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self addNav];
    [self addUI];
}
-(void)addUI{

UILabel *wayLabel = [[UILabel alloc]init];
wayLabel.textColor = rgba(34, 34, 34, 1);
wayLabel.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:14];
    self.model.trade = 0;
[self.self.view addSubview:wayLabel];

    switch ( self.model.trade) {
       case 0:
      
           wayLabel.text = @"积分兑换星钻";
       
           break;
           case 1:
       
            wayLabel.text = @"礼物收益";
      
           break;
           case 2:
       
             wayLabel.text= @"发红包";
      
           break;
           case 3:
             wayLabel.text = @"收红包";
             
                  break;
           case 4:
             
            wayLabel.text = @"提现";
             
                  break;
           case 5:
              
            wayLabel.text = @"送礼";
              
                  break;
           case 6:
                       
         wayLabel.text = @"充值星钻";
                       
                   break;
       default:
           break;
   }
[wayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    make.centerX.mas_equalTo(self.self.view.mas_centerX);
    make.top.mas_equalTo(self.view.mas_top).offset(TOP_SPACE88or64+30);
}];
UIView *linergralBGView = [[UIView alloc]init];
[self.view addSubview:linergralBGView];
[linergralBGView mas_makeConstraints:^(MASConstraintMaker *make) {
    make.centerX.mas_equalTo(self.view.mas_centerX);
    make.top.mas_equalTo(wayLabel.mas_bottom).offset(15);
    make.height.mas_offset(20);
}];
//积分
 
self.integralLabel.text = [NSString stringWithFormat:@"-%lu",self.model.amount];
    if (self.model.type == 0) {
        // 收入
         self.integralLabel.text = [NSString stringWithFormat:@"+%lu",self.model.amount];
     }
[self.view addSubview:self.integralLabel];
[self.integralLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    make.left.mas_equalTo(linergralBGView.mas_left);
    make.bottom.mas_equalTo(linergralBGView.mas_bottom);
}];
UILabel *individualPointsLabel = [[UILabel alloc]init];
individualPointsLabel.text = @"个人积分";
individualPointsLabel.textColor = rgba(34, 34, 34, 1);
individualPointsLabel.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:14];
[self.view addSubview:individualPointsLabel];
    if (self.model.currency== 0) {
        individualPointsLabel.text = @"星钻";
    }
[individualPointsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    make.left.mas_equalTo(self.integralLabel.mas_right).offset(5);
    make.bottom.mas_equalTo(linergralBGView.mas_bottom);
    make.right.mas_equalTo(linergralBGView.mas_right);
}];
//分割线一
UILabel *linLabel = [[UILabel alloc]init];
linLabel.backgroundColor =rgba(223, 223, 223, 0.7);
[self.view addSubview:linLabel];
    [linLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.8);
        make.top.mas_equalTo(linergralBGView.mas_bottom).offset(20);
    }];

    //类型
       NSArray *typeArray = @[@"类型:",@"时间:",@"单号:"];
       //提现
    NSArray *withdrawalArray = @[wayLabel.text,self.model.createTime,self.model.orderId];
       for (int i = 0; i<typeArray.count; i++) {
           UILabel *typeLable = [[UILabel alloc]init];
           typeLable.textColor = rgba(153, 153, 153, 1);
           typeLable.text = typeArray[i];
           typeLable.font  =[UIFont fontWithName:@"PingFang-SC-Medium" size:14];
           [self.view addSubview:typeLable];
           [typeLable mas_makeConstraints:^(MASConstraintMaker *make) {
               make.left.mas_equalTo(self.view.mas_left).offset(17);
               make.top.mas_equalTo(linLabel.mas_bottom).offset(i*30+20);
               make.height.mas_offset(20);
           }];
           UILabel *withdrawalLabel = [[UILabel alloc]init];
           withdrawalLabel.text = withdrawalArray[i];
           withdrawalLabel.textColor = rgba(153, 153, 153, 1);
           withdrawalLabel.font = [UIFont fontWithName:@"PingFang-SC-Medium" size:14];
           [self.view addSubview:withdrawalLabel];
           [withdrawalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
               make.left.mas_equalTo(self.view.mas_left).offset(134);
               make.centerY.mas_equalTo(typeLable.mas_centerY);
               make.height.mas_offset(20);
           }];
           
       }
}

- (UILabel *)integralLabel {
    if (!_integralLabel) {
        _integralLabel = [[UILabel alloc]init];
        _integralLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:24];
        _integralLabel.textColor = rgba(34, 34, 34, 1);
    }
    return _integralLabel;
}
@end
