//
//  MyPointsTransactionDetailsOtherVC.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyWalletTransactionRecordsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyPointsTransactionDetailsOtherVC : UIViewController
/** model */
@property (nonatomic, strong)MyWalletTransactionRecordsModel *model;
@end

NS_ASSUME_NONNULL_END
