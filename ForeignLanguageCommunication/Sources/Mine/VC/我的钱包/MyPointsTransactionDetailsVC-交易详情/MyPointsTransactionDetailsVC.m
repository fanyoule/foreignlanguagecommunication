//
//  MyPointsTransactionDetailsVC.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "MyPointsTransactionDetailsVC.h"
#import "MyPointsTransactionDetailsModel.h"
@interface MyPointsTransactionDetailsVC ()
@property (nonatomic,strong)UILabel    *labTitle;
/** 明细详情 */
@property (nonatomic, strong)UIScrollView *scrollV;
/** 积分number */
@property (nonatomic, strong) UILabel *integralLabel;
/** model */
@property (nonatomic, strong) MyPointsTransactionDetailsModel *model;
@end

@implementation MyPointsTransactionDetailsVC
-(void)addNav{
    
    
    UIView *nav = UIView.new;
    nav.frame = CGRectMake(0, 0, KSW, SNavBarHeight);
    nav.backgroundColor =[UIColor whiteColor];
    [self.view addSubview:nav];
    self.labTitle = UILabel.new;
    
    self.labTitle.font = kFont_Medium(18);
    self.labTitle.textColor =[UIColor blackColor];
    self.labTitle.text = @"明细详情";
    self.labTitle.textAlignment = 0;
    [nav addSubview:self.labTitle];
    [self.labTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nav).offset(SBarHeight+10);
        make.centerX.equalTo(nav);
    }];
    
    UIButton *btnLeft = UIButton.new;
//    btnLeft.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [btnLeft setImage:[UIImage imageNamed:@"icon_nav_back"] forState:(UIControlStateNormal)];
    [nav addSubview:btnLeft];
    [btnLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nav).offset(SBarHeight+10);
        make.left.equalTo(nav).offset(10);
        make.width.height.equalTo(@30);
    }];
    
    [btnLeft whenTapped:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];

 
}
-(void)viewWillAppear:(BOOL)animated{
    [self requstData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addNav];
    [self addUI];
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
}
-(void)addUI{
    
    [self.view addSubview:self.scrollV];
    [self.scrollV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_top).offset(TOP_SPACE88or64);
        make.left.right.bottom.mas_equalTo(self.view);
    }];

    UILabel *wayLabel = [[UILabel alloc]init];
    wayLabel.text = @"提现到微信";
    wayLabel.textColor = rgba(34, 34, 34, 1);
    wayLabel.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:14];
    [self.scrollV addSubview:wayLabel];
    [wayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.scrollV.mas_centerX);
        make.top.mas_equalTo(self.scrollV.mas_top).offset(34);
    }];
    UIView *linergralBGView = [[UIView alloc]init];
    [self.scrollV addSubview:linergralBGView];
    [linergralBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.scrollV.mas_centerX);
        make.top.mas_equalTo(wayLabel.mas_bottom).offset(15);
        make.height.mas_offset(20);
    }];
    //积分
    self.integralLabel.text = [NSString stringWithFormat:@"%ld",self.model.number];
    [self.scrollV addSubview:self.integralLabel];
    [self.integralLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(linergralBGView.mas_left);
        make.bottom.mas_equalTo(linergralBGView.mas_bottom);
    }];
    UILabel *individualPointsLabel = [[UILabel alloc]init];
    individualPointsLabel.text = @"元";
    individualPointsLabel.textColor = rgba(34, 34, 34, 1);
    individualPointsLabel.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:14];
    [self.scrollV addSubview:individualPointsLabel];
    [individualPointsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.integralLabel.mas_right).offset(5);
        make.bottom.mas_equalTo(linergralBGView.mas_bottom);
        make.right.mas_equalTo(linergralBGView.mas_right);
    }];
    //分割线一
    UILabel *linLabel = [[UILabel alloc]init];
    linLabel.backgroundColor =rgba(223, 223, 223, 1);
    [self.scrollV addSubview:linLabel];
    [linLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.8);
        make.top.equalTo(linergralBGView.mas_bottom).offset(20);
    }];
    
    //进度竖线
    UILabel *onePointLabel = [[UILabel alloc]init];
    onePointLabel.backgroundColor = RGBA(52, 120, 245, 1);
    onePointLabel.layer.cornerRadius = 5;
    onePointLabel.clipsToBounds = YES;
    [self.scrollV addSubview:onePointLabel];
    [onePointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(35);
        make.top.equalTo(linLabel.mas_bottom).offset(23);
        make.height.width.mas_equalTo(10);
    }];
    
    UILabel *oneLine = [[UILabel alloc]init];
    oneLine.backgroundColor = RGBA(52, 120, 245, 1);
    [self.scrollV addSubview:oneLine];
    [oneLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(onePointLabel.mas_bottom).offset(0);
        make.centerX.equalTo(onePointLabel);
        make.width.mas_equalTo(0.5);
        make.height.mas_equalTo(53);
    }];
    
    UILabel *twoLine = [[UILabel alloc]init];
    twoLine.backgroundColor = rgba(153, 153, 153, 1);
    [self.scrollV addSubview:twoLine];
    [twoLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(oneLine);
        make.top.mas_equalTo(oneLine.mas_bottom);
        make.width.mas_equalTo(0.5);
        make.height.mas_equalTo(53);
    }];
    
    UILabel *twoPointLabel = [[UILabel alloc]init];
    twoPointLabel.backgroundColor = rgba(153, 153, 153, 1);
    twoPointLabel.layer.cornerRadius = 5;
    twoPointLabel.clipsToBounds = YES;
    [self.scrollV addSubview:twoPointLabel];
    [twoPointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(twoLine);
        make.top.mas_equalTo(twoLine.mas_bottom).offset(0);
        make.width.height.mas_equalTo(10);
    }];
    
    UIImageView *clockImageV = [[UIImageView alloc]init];
    clockImageV.image = [UIImage imageNamed:@"my_clock"];
    clockImageV.contentMode  = UIViewContentModeScaleAspectFill;
    clockImageV.layer.cornerRadius = 13;
    clockImageV.clipsToBounds = YES;
    [self.scrollV addSubview:clockImageV];
    [clockImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(oneLine.mas_bottom).offset(-13);
        make.centerX.mas_equalTo(oneLine.mas_centerX);
        make.width.height.mas_equalTo(26);
    }];
    //分割线二
    UILabel *bottomLine =[[UILabel alloc]init];
    bottomLine.backgroundColor =rgba(223, 223, 223, 1);
    [self.scrollV addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.mas_equalTo(0.8);
        make.top.mas_equalTo(twoPointLabel.mas_bottom).offset(50);
    }];
    
    
    self.model.status = 2;
    
    
    if (self.model.status == 2) {
         //类型
           NSArray *typeArray = @[@"类型:",@"申请时间:",@"提现失败时间:",@"提现账户:",@"订单号:",@"失败原因："];
           //提现
        NSArray *withdrawalArray = @[@"提现",self.model.createTime,self.model.passTime,@"4564765",[NSString stringWithFormat:@"%@",self.model.orderId],self.model.note];
           for (int i = 0; i<typeArray.count; i++) {
               UILabel *typeLable = [[UILabel alloc]init];
               typeLable.textColor = rgba(153, 153, 153, 1);
               typeLable.text = typeArray[i];
               typeLable.font  =[UIFont fontWithName:@"PingFang-SC-Medium" size:14];
               [self.scrollV addSubview:typeLable];
               [typeLable mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.mas_equalTo(self.scrollV.mas_left).offset(17);
                   make.top.mas_equalTo(bottomLine.mas_bottom).offset(i*30+20);
                   make.height.mas_offset(20);
               }];
               UILabel *withdrawalLabel = [[UILabel alloc]init];
               withdrawalLabel.text = withdrawalArray[i];
               withdrawalLabel.textColor = rgba(153, 153, 153, 1);
               withdrawalLabel.font = [UIFont fontWithName:@"PingFang-SC-Medium" size:14];
              
               [self.scrollV addSubview:withdrawalLabel];
               [withdrawalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.mas_equalTo(self.scrollV.mas_left).offset(134);
                   make.centerY.mas_equalTo(typeLable.mas_centerY);
                   make.height.mas_offset(20);
               }];
               
           }
    }else{
        //类型
           NSArray *typeArray = @[@"类型:",@"申请时间:",@"到账时间:",@"提现账户:",@"订单号:"];
        if (self.model.passTime) {
            NSLog(@"9999");
        }else{
            NSLog(@"为空");
            self.model.passTime = @"";
        }
        if (self.model.createTime) {
                 NSLog(@"9999");
             }else{
                 NSLog(@"为空");
               self.model.createTime = @"";
             }
           //提现
//           NSArray *withdrawalArray = @[@"提现",self.model.createTime,self.model.passTime,@"4564765",[NSString stringWithFormat:@"%@",self.model.orderId]];
        NSArray *withdrawalArray = @[@"提现",@"2020-06-10  12:00",@"2020-06-10  12:00",@"12345678900",@"0123456789"];
           for (int i = 0; i<typeArray.count; i++) {
               UILabel *typeLable = [[UILabel alloc]init];
               typeLable.textColor = rgba(153, 153, 153, 1);
               typeLable.text = typeArray[i];
               typeLable.font  =[UIFont fontWithName:@"PingFang-SC-Medium" size:14];
               [self.scrollV addSubview:typeLable];
               [typeLable mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.mas_equalTo(self.scrollV.mas_left).offset(17);
                   make.top.mas_equalTo(bottomLine.mas_bottom).offset(i*30+20);
                   make.height.mas_offset(20);
               }];
               UILabel *withdrawalLabel = [[UILabel alloc]init];
               withdrawalLabel.text = withdrawalArray[i];
               withdrawalLabel.textColor = rgba(153, 153, 153, 1);
               withdrawalLabel.font = [UIFont fontWithName:@"PingFang-SC-Medium" size:14];
              
               [self.scrollV addSubview:withdrawalLabel];
               [withdrawalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.left.mas_equalTo(self.scrollV.mas_left).offset(134);
                   make.centerY.mas_equalTo(typeLable.mas_centerY);
                   make.height.mas_offset(20);
               }];
               
           }
    }
   
    //发起体现申请
    UILabel *initiateLabel = [[UILabel alloc]init];
    initiateLabel.text = @"发起提现申请";
    initiateLabel.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:15];
    initiateLabel.textColor = rgba(153, 153, 153, 1);
    [self.scrollV addSubview:initiateLabel];
    [initiateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(onePointLabel.mas_centerY);
        make.left.mas_equalTo(self.scrollV.mas_left).offset(81);
        make.height.mas_offset(15);
    }];
    // 系统处理
    UILabel *dealLabel = [[UILabel alloc]init];
    dealLabel.text = @"系统处理中";
    dealLabel.textColor = rgba(34, 34, 34, 1);
    dealLabel.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:15];
    [self.scrollV addSubview:dealLabel];
    [dealLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(clockImageV.mas_centerY).offset(-2);
         make.left.mas_equalTo(self.scrollV.mas_left).offset(81);
        make.height.mas_offset(16);
    }];
    //预计到账时间
    UILabel *expectLabel = [[UILabel alloc]init];
    expectLabel.text = [NSString stringWithFormat:@"预计%@到账",self.model.expectedTime];
    expectLabel.textColor = rgba(153, 153, 153, 1);
    expectLabel.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:12];
    [self.scrollV addSubview:expectLabel];
    [expectLabel mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.mas_equalTo(self.scrollV.mas_left).offset(81);
        make.height.mas_offset(12);
        make.top.mas_equalTo(clockImageV.mas_centerY).offset(3);
    }];
    //成功
    UILabel *sucessLabel = [[UILabel alloc]init];
    sucessLabel.text = @"提现成功";
    sucessLabel.textColor = rgba(153, 153, 153, 1);
    sucessLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:16];
    [self.scrollV addSubview:sucessLabel];
    [sucessLabel mas_makeConstraints:^(MASConstraintMaker *make) {
      make.left.mas_equalTo(self.scrollV.mas_left).offset(81);
        make.centerY.mas_equalTo(twoPointLabel.mas_centerY);
        make.height.mas_offset(15);
    }];
    if (self.model.status==1) {
        //待处理
    }else{
        //失败或者成功
        twoPointLabel.backgroundColor = RGBA(52, 120, 245, 1);
        twoLine.backgroundColor = RGBA(52, 120, 245, 1);
        //处理中
        dealLabel.textColor = RGBA(153, 153, 153, 1);
        if (self.model.status ==2 ) {
            sucessLabel.text = @"提现失败";
        }
        
    }
}
- (UIScrollView *)scrollV {
    if (!_scrollV) {
        _scrollV = [[UIScrollView alloc]init];
        _scrollV.contentInset = UIEdgeInsetsZero;
         _scrollV.scrollIndicatorInsets = UIEdgeInsetsZero;
        _scrollV.showsHorizontalScrollIndicator = NO;
         if (@available(iOS 11.0, *)) {
             _scrollV.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
         } else {
             // Fallback on earlier versions
         }
    }
    return _scrollV;
}
- (UILabel *)integralLabel {
    if (!_integralLabel) {
        _integralLabel = [[UILabel alloc]init];
        _integralLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:24];
        _integralLabel.textColor = rgba(34, 34, 34, 1);
    }
    return _integralLabel;
}
#pragma mark - 获取交易详情
-(void)requstData{
//    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//    dic[@"orderId"] = self.orderId;
//    [RequestManager getMyDataWithParameters:dic UrlString:@"/api/wallet/getDealDetail" Success:^(id  _Nullable response) {
//        NSLog(@"response===%@",response);
//        if ([response[@"status"]intValue]==200) {
//            self.model = [MyPointsTransactionDetailsModel mj_objectWithKeyValues:response[@"data"]];
//            [self.scrollV removeAllSubviews];
//            [self addUI];
//        }
//    } withFail:^(NSError * _Nullable error) {
//        NSLog(@"errr===%@",error);
//    }];
}

@end
