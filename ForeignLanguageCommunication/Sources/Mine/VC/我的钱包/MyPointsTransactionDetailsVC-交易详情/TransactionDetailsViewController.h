//
//  TransactionDetailsViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import "BaseViewController.h"
#import "TheWalletDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TransactionDetailsViewController : BaseViewController
@property (nonatomic, strong)TheWalletDetailsModel *model;
@end

NS_ASSUME_NONNULL_END
