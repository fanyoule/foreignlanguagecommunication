//
//  TypeTheDetailsTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import "TypeTheDetailsTableViewCell.h"
@interface TypeTheDetailsTableViewCell ()

@end
@implementation TypeTheDetailsTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
    }
    return self;
}

-(void)setModel:(TheWalletDetailsModel *)model{
    NSString *string = nil;
    if (model.type == 0) {
        string = @"收入";
    }else if (model.type == 1){
        string = @"支出";
    }
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
    }];
    
    NSArray *titleArray = [NSArray array];
    NSArray *contentArray = [NSArray array];
    if (model.tradeType == 3) {
        titleArray = @[@"类型:",@"申请时间:",@"到账时间:",@"提现账户:",@"单号:"];
        contentArray = @[string,model.createTime,model.passTime,model.account,model.orderId];
    }else{
        titleArray = @[@"类型:",@"时间:",@"单号:"];
        contentArray = @[string,model.createTime,model.orderId];
    }
    
        for (int i = 0; i < contentArray.count; i++) {
            UIView *view = UIView.new;
            view.backgroundColor = UIColor.clearColor;
            [self.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(lineView.mas_bottom).offset(i*15+i*10+30);
                make.left.right.equalTo(self.contentView);
                make.height.equalTo(@15);
            }];
            
            UILabel *titleLabel = UILabel.new;
            titleLabel.text = titleArray[i];
            titleLabel.textColor = RGBA(153, 153, 153, 1);
            titleLabel.font = kFont_Medium(13);
            titleLabel.textAlignment = 0;
            [view addSubview:titleLabel];
            [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(view);
                make.left.equalTo(view).offset(15);
            }];
            
            UILabel *contentLabel = UILabel.new;
            contentLabel.text = contentArray[i];
            contentLabel.textColor = RGBA(24, 24, 24, 1);
            contentLabel.font = kFont_Medium(13);
            contentLabel.textAlignment = 0;
            [view addSubview:contentLabel];
            [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(titleLabel);
                make.left.equalTo(view).offset(116);
            }];
        }
    
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
