//
//  WithdrawalHeadTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import <UIKit/UIKit.h>
#import "TheWalletDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface WithdrawalHeadTableViewCell : UITableViewCell

/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 金额*/
@property (nonatomic, strong)UILabel *amountLabel;
/** E币图片*/
@property (nonatomic, strong)UIImageView *imageV;

@property (nonatomic, strong)TheWalletDetailsModel *model;
@end

NS_ASSUME_NONNULL_END
