//
//  WithdrawalStateTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import "WithdrawalStateTableViewCell.h"
@interface WithdrawalStateTableViewCell ()
/** 发起申请*/
@property (nonatomic, strong)UILabel *initiateLabel;
/** 发起时间*/
@property (nonatomic, strong)UILabel *initiateTimeLabel;
/** 处理中*/
@property (nonatomic, strong)UILabel *dealWithlabel;
/** 提现结果*/
@property (nonatomic, strong)UILabel *resultsLabel;
/** 提现结果时间*/
@property (nonatomic, strong)UILabel *resultsTimeLabel;

@property (nonatomic, strong)UIView *point1;
@property (nonatomic, strong)UIView *point2;
@property (nonatomic, strong)UIView *point3;

@property (nonatomic, strong)UIView *lineView1;
@property (nonatomic, strong)UIView *lineView2;


/** 分割线*/
@property (nonatomic, strong)UIView *line;
@end
@implementation WithdrawalStateTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.line];
    [self.contentView addSubview:self.lineView1];
    [self.contentView addSubview:self.lineView2];
    [self.contentView addSubview:self.point1];
    [self.contentView addSubview:self.point2];
    [self.contentView addSubview:self.point3];
    
    [self.contentView addSubview:self.initiateLabel];
    [self.contentView addSubview:self.initiateTimeLabel];
    [self.contentView addSubview:self.dealWithlabel];
    [self.contentView addSubview:self.resultsLabel];
    [self.contentView addSubview:self.resultsTimeLabel];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
    }];
    [self.point1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(25);
        make.left.equalTo(self.contentView).offset(35);
        make.width.height.equalTo(@10);
    }];
    [self.point2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.point1.mas_bottom).offset(47);
        make.left.equalTo(self.contentView).offset(35);
        make.width.height.equalTo(@10);
    }];
    [self.point3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.point2.mas_bottom).offset(47);
        make.left.equalTo(self.contentView).offset(35);
        make.width.height.equalTo(@10);
    }];
    [self.lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.point1);
        make.top.equalTo(self.point1.mas_centerY);
        make.width.equalTo(@0.5);
        make.bottom.equalTo(self.point2.mas_centerY);
    }];
    [self.lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.point2);
        make.top.equalTo(self.point2.mas_centerY);
        make.width.equalTo(@0.5);
        make.bottom.equalTo(self.point3.mas_centerY);
    }];
    
    
    [self.initiateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.point1);
        make.left.equalTo(self.point1.mas_right).offset(43);
    }];
    [self.initiateTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.initiateLabel);
        make.top.equalTo(self.initiateLabel.mas_bottom).offset(2);
    }];
    [self.dealWithlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.point2);
        make.left.equalTo(self.point2.mas_right).offset(43);
    }];
    [self.resultsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.point3);
        make.left.equalTo(self.point3.mas_right).offset(43);
    }];
    [self.resultsTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.resultsLabel);
        make.top.equalTo(self.resultsLabel.mas_bottom).offset(2);
    }];
    
}

- (void)setModel:(TheWalletDetailsModel *)model{
    
    UIImageView *imageV = [[UIImageView alloc]init];
    imageV.image = [UIImage imageNamed:@"my_clock"];
    [self.contentView addSubview:imageV];
    
    if (model.status == 0) {// 处理中
        self.initiateTimeLabel.text = model.createTime;
        self.point3.backgroundColor = RGBA(153, 153, 153, 1);
        self.lineView2.backgroundColor = RGBA(153, 153, 153, 1);
        
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.point2);
            make.centerY.equalTo(self.point2);
            make.width.height.equalTo(@25);
        }];
        self.resultsLabel.backgroundColor = RGBA(153, 153, 153, 1);
        self.resultsTimeLabel.hidden = YES;
    }else if (model.status == 1){
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.point3);
            make.centerY.equalTo(self.point3);
            make.width.height.equalTo(@25);
        }];
        self.initiateTimeLabel.text = model.createTime;
        self.resultsTimeLabel.text = model.passTime;
        self.resultsLabel.text = @"提现成功";
        
    }else if (model.status == 2){
        imageV.image = [UIImage imageNamed:@"icon--提现失败"];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.point3);
            make.centerY.equalTo(self.point3);
            make.width.height.equalTo(@25);
        }];
        self.initiateTimeLabel.text = model.createTime;
        self.resultsTimeLabel.text = model.passTime;
        self.resultsLabel.text = @"提现失败";
        
    }
    
}

- (UILabel *)initiateLabel{
    if (!_initiateLabel) {
        _initiateLabel = [[UILabel alloc]init];
        _initiateLabel.text = @"发起提现申请";
        _initiateLabel.textColor = RGBA(24, 24, 24, 1);
        _initiateLabel.font = kFont_Medium(15);
        _initiateLabel.textAlignment = 0;
    }
    return _initiateLabel;
}
- (UILabel *)initiateTimeLabel{
    if (!_initiateTimeLabel) {
        _initiateTimeLabel = [[UILabel alloc]init];
        _initiateTimeLabel.text = @"2020-06-18  12:00";
        _initiateTimeLabel.textColor = RGBA(153, 153, 153, 1);
        _initiateTimeLabel.font = kFont_Medium(12);
        _initiateTimeLabel.textAlignment = 0;
    }
    return _initiateTimeLabel;
}
- (UILabel *)dealWithlabel{
    if (!_dealWithlabel) {
        _dealWithlabel = [[UILabel alloc]init];
        _dealWithlabel.text = @"处理中";
        _dealWithlabel.textColor = RGBA(24, 24, 24, 1);
        _dealWithlabel.font = kFont_Medium(15);
        _dealWithlabel.textAlignment = 0;
    }
    return _dealWithlabel;
}
- (UILabel *)resultsLabel{
    if (!_resultsLabel) {
        _resultsLabel = [[UILabel alloc]init];
        _resultsLabel.text = @"到账成功";
        _resultsLabel.textColor = RGBA(24, 24, 24, 1);
        _resultsLabel.font = kFont_Medium(15);
        _resultsLabel.textAlignment = 0;
    }
    return _resultsLabel;
}
- (UILabel *)resultsTimeLabel{
    if (!_resultsTimeLabel) {
        _resultsTimeLabel = [[UILabel alloc]init];
        _resultsTimeLabel.text = @"2020-06-18  12:00";
        _resultsTimeLabel.textColor = RGBA(153, 153, 153, 1);
        _resultsTimeLabel.font = kFont_Medium(12);
        _resultsTimeLabel.textAlignment = 0;
    }
    return _resultsTimeLabel;
}

- (UIView *)point1{
    if (!_point1) {
        _point1 = [[UIView alloc]init];
        _point1.backgroundColor = RGBA(52, 120, 245, 1);
        _point1.layer.cornerRadius = 10/2;
        _point1.clipsToBounds = YES;
    }
    return _point1;
}

- (UIView *)point2{
    if (!_point2) {
        _point2 = [[UIView alloc]init];
        _point2.backgroundColor = RGBA(52, 120, 245, 1);
        _point2.layer.cornerRadius = 10/2;
        _point2.clipsToBounds = YES;
    }
    return _point2;
}

- (UIView *)point3{
    if (!_point3) {
        _point3 = [[UIView alloc]init];
        _point3.backgroundColor = RGBA(52, 120, 245, 1);
        _point3.layer.cornerRadius = 10/2;
        _point3.clipsToBounds = YES;
    }
    return _point3;
}

- (UIView *)lineView1{
    if (!_lineView1) {
        _lineView1 = [[UIView alloc]init];
        _lineView1.backgroundColor = RGBA(52, 120, 245, 1);
    }
    return _lineView1;
}
- (UIView *)lineView2{
    if (!_lineView2) {
        _lineView2 = [[UIView alloc]init];
        _lineView2.backgroundColor = RGBA(52, 120, 245, 1);
    }
    return _lineView2;
}

- (UIView *)line{
    if (!_line) {
        _line = [[UIView alloc]init];
        _line.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _line;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
