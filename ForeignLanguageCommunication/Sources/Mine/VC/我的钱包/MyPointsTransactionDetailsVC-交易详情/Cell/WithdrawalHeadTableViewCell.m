//
//  WithdrawalHeadTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import "WithdrawalHeadTableViewCell.h"

@implementation WithdrawalHeadTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.amountLabel];
    [self.contentView addSubview:self.imageV];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(43);
        make.centerX.equalTo(self.contentView);
    }];
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
        make.centerX.equalTo(self.contentView).offset(-10);
    }];
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.amountLabel);
        make.left.equalTo(self.amountLabel.mas_right);
        make.width.height.equalTo(@30);
    }];
    
}

- (void)setModel:(TheWalletDetailsModel *)model{
    NSString *string = nil;
    if (model.type == 0) {
        string = @"+";
    }else{
        string = @"-";
    }
    self.amountLabel.text = [NSString stringWithFormat:@"%@%0.2lf",string,model.number];
    
    if (model.tradeType == 0) {
        self.titleLabel.text = @"购买课程";
    }else if (model.tradeType == 1){
        self.titleLabel.text = @"购买会员(首充)";
    }else if (model.tradeType == 2){
        self.titleLabel.text = @"匹配";
    }else if (model.tradeType == 3){
//        if (model.accountType == 0) {
//            //银行
//        }else if (model.accountType == 1){
//            // 微信
//        }else if (model.accountType == 2){
//            // 支付宝
//        }else if (model.accountType == 3){
//            // 苹果内购
//        }
        self.titleLabel.text = @"提现-到支付宝";
    }else if (model.tradeType == 4){
        self.titleLabel.text = @"视频";
    }else if (model.tradeType == 5){
        self.titleLabel.text = @"直系收益";
    }else if (model.tradeType == 6){
        self.titleLabel.text = @"退款";
    }else if (model.tradeType == 7){
        self.titleLabel.text = @"购买会员（复购）";
    }else if (model.tradeType == 8){
        self.titleLabel.text = @"充值金币";
    }else if (model.tradeType == 9){
        self.titleLabel.text = @"礼物收益";
    }else if (model.tradeType == 10){
        self.titleLabel.text = @"礼物支出";
    }else if (model.tradeType == 11){
        self.titleLabel.text = @"二级收益";
    }else if (model.tradeType == 12){
        self.titleLabel.text = @"语音";
    }else if (model.tradeType == 13){
        self.titleLabel.text = @"聊天";
    }else if (model.tradeType == 14){
        self.titleLabel.text = @"分销收益";
    }else if (model.tradeType == 15){
        self.titleLabel.text = @"创建房间";
    }else if (model.tradeType == 16){
        self.titleLabel.text = @"续租房间";
    }else if (model.tradeType == 17){
        self.titleLabel.text = @"课程收益";
    }
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"提现到支付宝";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(18);
        _titleLabel.textAlignment = 1;
    }
    return _titleLabel;
}

- (UILabel *)amountLabel{
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc]init];
        _amountLabel.text = @"-3000";
        _amountLabel.textColor = RGBA(24, 24, 24, 1);
        _amountLabel.font = kFont_Bold(36);
        _amountLabel.textAlignment = 1;
    }
    return _amountLabel;
}

- (UIImageView *)imageV{
    if (!_imageV) {
        _imageV = [[UIImageView alloc]init];
        _imageV.image = [UIImage imageNamed:@"钱包-钱"];
    }
    return _imageV;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
