//
//  MyPointsTransactionDetailsModel.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "MyPointsTransactionDetailsModel.h"

@implementation MyPointsTransactionDetailsModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}
@end
