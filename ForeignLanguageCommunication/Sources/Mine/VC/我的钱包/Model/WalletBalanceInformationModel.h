//
//  WalletBalanceInformationModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WalletBalanceInformationModel : NSObject
/** 代理收益*/
@property (nonatomic, assign)NSInteger agentProfit;
/** 课程收益*/
@property (nonatomic, assign)NSInteger courseProfit;
/** 金币数量*/
@property (nonatomic, assign)NSInteger gold;
/** 钻石数量*/
@property (nonatomic, assign)NSInteger jewel;
/** 数据更新时间*/
@property (nonatomic, strong)NSString *updateTime;
/** 用户id*/
@property (nonatomic, assign)NSInteger userId;


@end

NS_ASSUME_NONNULL_END
