//
//  DetailsOfMyWalletModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/20.
//  我的钱包 充值位置

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailsOfMyWalletModel : NSObject
/** 余额*/
@property (nonatomic, assign)NSInteger balance;
/** 位置1*/
@property (nonatomic, assign)NSInteger location1;
/** 位置2*/
@property (nonatomic, assign)NSInteger location2;
/** 位置3*/
@property (nonatomic, assign)NSInteger location3;
/** 位置4*/
@property (nonatomic, assign)NSInteger location4;
/** 位置5*/
@property (nonatomic, assign)NSInteger location5;
/** 位置6*/
@property (nonatomic, assign)NSInteger location6;
/** 用户ID*/
@property (nonatomic, assign)NSInteger userId;



@end

NS_ASSUME_NONNULL_END
