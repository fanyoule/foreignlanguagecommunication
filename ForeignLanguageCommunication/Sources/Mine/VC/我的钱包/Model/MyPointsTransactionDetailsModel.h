//
//  MyPointsTransactionDetailsModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyPointsTransactionDetailsModel : NSObject
/**账号，只有提现有 */
@property (nonatomic, strong) NSString *account;
/**申请时间 */
@property (nonatomic, strong) NSString *createTime;
/**预计审核时间，只有提现有 */
@property (nonatomic, strong) NSString *expectedTime;
/**失败原因，只有提现有*/
@property (nonatomic, strong) NSString *note;
/**金额*/
@property (nonatomic, assign) NSInteger number;
/**交易流水*/
@property (nonatomic, strong) NSString *orderId;
/**审核通过时间/到账时间，只有提现有*/
@property (nonatomic, strong) NSString *passTime;
/**状态,只有提现有 0：审批通过 1：待审批 2：拒绝*/
@property (nonatomic, assign) NSInteger status;
/**交易类型 0:积分兑换星钻 1:礼物收益 2:发红包 3:收红包 4:提现 5:送礼物 6:充值星钻*/
@property (nonatomic, assign) NSInteger  trade;
/**收支类型 收入:0 支出:1*/
@property (nonatomic, assign) NSInteger   type;
@end

NS_ASSUME_NONNULL_END
