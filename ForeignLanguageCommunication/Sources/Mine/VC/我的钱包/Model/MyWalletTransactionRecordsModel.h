//
//  MyWalletTransactionRecordsModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyWalletTransactionRecordsModel : NSObject
/** 额度 */
@property (nonatomic, assign) NSInteger amount;
/** 交易时间 */
@property (nonatomic, strong) NSString *createTime;
/** 货币类型 0:星钻 1:个人积分 */
@property (nonatomic, assign) NSInteger  currency;
/** 交易流水号 */
@property (nonatomic, strong) NSString *orderId;
/** 交易状态 0：已通过 1：审核中 2：审核未通过 */
@property (nonatomic, assign) NSInteger  status;
/** 交易类型 0:积分兑换星钻 1:礼物收益 2:发红包 3:收红包 4:提现 5:送礼6:充值星钻*/
@property (nonatomic, assign) NSInteger  trade;
/** 收支类型 收入:0 支出:1*/
@property (nonatomic, assign) NSInteger  type;
/** 用户id*/
@property (nonatomic, assign) NSInteger  userId;
@end

NS_ASSUME_NONNULL_END
