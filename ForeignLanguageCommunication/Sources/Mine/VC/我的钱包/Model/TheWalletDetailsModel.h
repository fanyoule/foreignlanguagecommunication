//
//  TheWalletDetailsModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//  钱包明细

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TheWalletDetailsModel : NSObject
/** 提现到的账号*/
@property (nonatomic, strong)NSString *account;
/** 提现人的真实姓名*/
@property (nonatomic, strong)NSString *accountName;
/** 账号类型 0：银行 1：微信 2：支付宝 3苹果内购*/
@property (nonatomic, assign)NSInteger accountType;
/** 课程id（只有交易类型为购买课程时有）*/
@property (nonatomic, assign)NSInteger courseId;
/** 创建时间*/
@property (nonatomic, strong)NSString *createTime;
/** 0：人民币 1：金币 2：钻石*/
@property (nonatomic, assign)NSInteger currency;
/** 备注*/
@property (nonatomic, strong)NSString *notes;
/** 金额*/
@property (nonatomic, assign)CGFloat number;
/** 订单号*/
@property (nonatomic, strong)NSString *orderId;
/** 审核完成时间*/
@property (nonatomic, strong)NSString *passTime;
/** 支付订单号*/
@property (nonatomic, strong)NSString *payOrderNum;
/** 支付手机类型 0：安卓 1：苹果*/
@property (nonatomic, assign)NSInteger phoneType;
/** 购买的数量*/
@property (nonatomic, assign)NSInteger quantity;
/** 关联订单号*/
@property (nonatomic, strong)NSString *relationOrderId;
/** 产生该订单的房间*/
@property (nonatomic, strong)NSString *roomSysid;
/** 0:处理中 1：审核通过 2：审核未通过 3：不需要审核*/
@property (nonatomic, assign)NSInteger status;
/** 交易类型 0：购买课程 1：购买会员(首充) 2：匹配 3：提现 4：视频 5：直系收益 6：退款 7 ：购买会员（复购）8：充值金币 9:礼物收益 10:礼物支出，11：二级收益,12:语音,13:聊天 14:分销收益 15创建房间 16续租房间,17：课程收益*/
@property (nonatomic, assign)NSInteger tradeType;
/** 收入支出类型 0：收入 1：支出*/
@property (nonatomic, assign)NSInteger type ;
/** 用户id*/
@property (nonatomic, assign)NSInteger userId;

@end

NS_ASSUME_NONNULL_END
