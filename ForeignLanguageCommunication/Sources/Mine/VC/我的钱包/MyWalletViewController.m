//
//  MyWalletViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "MyWalletViewController.h"
#import "TheWalletDetailsViewController.h"// 明细
#import "WithdrawalViewController.h"// 提现
#import "TopUpPopupWindow.h"// 充值弹窗
#import <HWPopController/HWPop.h>
#import "TopUpBalanceTableViewCell.h"
#import "TopUpAmountTableViewCell.h"
#import "PrepaidPhoneButtonTableViewCell.h"
#import "IncomeTableViewCell.h"
#import "TopUpAgreementViewController.h"// 充值协议
#import "DetailsOfMyWalletModel.h"//我的钱包 位置
#import "WalletBalanceInformationModel.h"//钱包余额信息

@interface MyWalletViewController ()<UITableViewDelegate,UITableViewDataSource,TopUpBalanceTableViewCellDelegate,UITextFieldDelegate>
/** 余额数量*/
@property (nonatomic, strong)UILabel *numberLabel;


/** 充值比例*/
@property (nonatomic, assign)NSInteger exGoldRate;

@property (nonatomic, strong)DetailsOfMyWalletModel *walletModel;

@property(nonatomic, strong)WalletBalanceInformationModel *informationModel;

@property (nonatomic, strong)UIButton *button;

/** 选中的金额*/
@property (nonatomic, strong)NSString *selectAmount;
@end

@implementation MyWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.navTitleString = @"我的钱包";
    [self addRightBtnWith:@"明细"];
    
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    
    
    UIView *headView = [[UIView alloc]init];
    headView.backgroundColor = RGBA(255, 255, 255, 1);
    headView.frame = CGRectMake(0, 0, KSW, kScaleSize(175));
    UIImageView *backImageV = [[UIImageView alloc]init];
    backImageV.image = [UIImage imageNamed:@"我的钱包-余额"];
    [headView addSubview:backImageV];
    [backImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(headView);
    }];
    UILabel *balanceLabel = UILabel.new;
    balanceLabel.text = @"余额";
    balanceLabel.textColor = RGBA(255, 255, 255, 1);
    balanceLabel.font = kFont(16);
    balanceLabel.textAlignment = 1;
    [headView addSubview:balanceLabel];
    [balanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(backImageV).offset(49);
        make.centerX.equalTo(backImageV).offset(-10);
    }];
    UIImageView *coinImageV = UIImageView.new;
    coinImageV.image = [UIImage imageNamed:@"钱包-钱"];
    [headView addSubview:coinImageV];
    [coinImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(balanceLabel);
        make.left.equalTo(balanceLabel.mas_right).offset(5);
        make.width.height.equalTo(@15);
    }];
    self.numberLabel = UILabel.new;
    self.numberLabel.text = @"4500";
    self.numberLabel.textColor = RGBA(255, 255, 255, 1);
    self.numberLabel.font = kFont_Bold(26);
    self.numberLabel.textAlignment = 1;
    [headView addSubview:self.numberLabel];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backImageV);
        make.top.equalTo(balanceLabel.mas_bottom).offset(15);
    }];
    
    self.mainTableView.tableHeaderView = headView;
    [self getData];
    
}

-(void)getData{
    [RequestManager getWalletByUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"钱包详情 === %@",response);
        self.exGoldRate = [response[@"data"][@"exGoldRate"] integerValue];
        NSDictionary *locationConfig = response[@"data"][@"locationConfig"];
        NSDictionary *wallet = response[@"data"][@"wallet"];
        if ([response[@"status"] integerValue] == 200) {
            
            self.walletModel = [DetailsOfMyWalletModel mj_objectWithKeyValues:locationConfig];
            self.numberLabel.text = [NSString stringWithFormat:@"%ld", self.walletModel.balance];
            
            self.informationModel = [WalletBalanceInformationModel mj_objectWithKeyValues:wallet];
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

-(DetailsOfMyWalletModel *)walletModel{
    if (!_walletModel) {
        _walletModel = [[DetailsOfMyWalletModel alloc]init];
    }
    return _walletModel;
}
- (WalletBalanceInformationModel *)informationModel{
    if (!_informationModel) {
        _informationModel = [[WalletBalanceInformationModel alloc]init];
    }
    return _informationModel;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 3;
    }else{
        return 2;
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            TopUpBalanceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TopUpBalanceTableViewCell"];
                if (!cell) {
                    cell = [[TopUpBalanceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TopUpBalanceTableViewCell"];
                }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
            cell.cellDelegate = self;
            cell.model = self.walletModel;
            cell.exGoldRate = self.exGoldRate;
            cell.textField.delegate = self;
            cell.textField.tag = 900;
            [cell.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
            [cell.agreementLabel whenTapped:^{
                [self.view endEditing:YES];
                TopUpAgreementViewController *vc = [[TopUpAgreementViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }];
            return cell;
        }else if (indexPath.row == 1){
            TopUpAmountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TopUpAmountTableViewCell"];
            if (!cell) {
                cell = [[TopUpAmountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TopUpAmountTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor clearColor];
            
            cell.amountLabel.tag = 700;
            
        return cell;
        }else{
            PrepaidPhoneButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrepaidPhoneButtonTableViewCell"];
            if (!cell) {
                cell = [[PrepaidPhoneButtonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PrepaidPhoneButtonTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor clearColor];
            [cell.prepaidBtn addTarget:self action:@selector(clickTopUpBtn) forControlEvents:(UIControlEventTouchDown)];
            return cell;
        }
    }else{
        IncomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IncomeTableViewCell"];
        if (!cell) {
            cell = [[IncomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IncomeTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        
        if (indexPath.row == 0) {
            cell.titlelabel.text = @"代理收入";
            cell.moneyLabel.text = [NSString stringWithFormat:@"￥%ld",self.informationModel.agentProfit];
            [cell.withdrawalBtn whenTapped:^{
                [self clickWithdrawal:cell.withdrawalBtn type:0];
            }];
        }else{
            cell.titlelabel.text = @"课程收入";
            cell.moneyLabel.text = [NSString stringWithFormat:@"￥%ld",self.informationModel.courseProfit];
            [cell.withdrawalBtn whenTapped:^{
                [self clickWithdrawal:cell.withdrawalBtn type:1];
            }];
        }
//        [cell.withdrawalBtn addTarget:self action:@selector(clickWithdrawal) forControlEvents:(UIControlEventTouchDown)];
        
    return cell;
    }  

}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.clearColor;
    UILabel *label = [[UILabel alloc]init];
    label.text = @"   我的收入";
    label.textColor = RGBA(51, 51, 51, 1);
    label.font = kFont_Bold(17);
    label.textAlignment = 0;
    label.backgroundColor = UIColor.whiteColor;
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(view);
        make.bottom.equalTo(view).offset(-10);
        make.height.equalTo(@50).priority(600);
    }];
    if (section == 0) {
        return nil;
    }else{
        return view;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else{
        return 60;
    }

}


// MARK: 明细
-(void)rightClick:(UIButton *)sender{
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"充值明细" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        TheWalletDetailsViewController *vc = [[TheWalletDetailsViewController alloc]init];
        vc.isJudge = 0;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"代理明细" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        TheWalletDetailsViewController *vc = [[TheWalletDetailsViewController alloc]init];
        vc.isJudge = 1;
        [self.navigationController pushViewController:vc animated:YES];
        
    }];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"课程明细" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        TheWalletDetailsViewController *vc = [[TheWalletDetailsViewController alloc]init];
        vc.isJudge = 2;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [cancelAction setValue:RGBA(153, 153, 153, 1) forKey:@"titleTextColor"];
    [alert addAction:action1];
    [alert addAction:action2];
    [alert addAction:action3];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

// MARK: 充值
-(void)clickTopUpBtn {
    if (!self.selectAmount || self.selectAmount.length <= 0) {
        [self showText:@"请选择充值金额"];
        return;
    }
    [self.view endEditing:YES];
    TopUpPopupWindow *VC = [TopUpPopupWindow new];
    NSLog(@"选中的金额 === %@",self.selectAmount);
    VC.selectAmount = self.selectAmount;
    HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                     popController.backgroundAlpha = 0.1;//背景色透明度
                     popController.animationDuration = 0.3;
                     popController.popPosition = 2;
                     popController.popType = 5;
                     popController.dismissType = 5;
                     popController.shouldDismissOnBackgroundTouch = YES;
                     [popController presentInViewController:self];
}
// MARK: 提现
-(void)clickWithdrawal:(UIButton *)button type:(int)type{
    [self.view endEditing:YES];
    WithdrawalViewController *vc = [[WithdrawalViewController alloc]init];
    vc.balance = self.walletModel.balance;
    vc.type = type;
    [self.navigationController pushViewController:vc animated:YES];
}

// MARK: 选择金额
-(void)clickTopUpBalanceTableViewCell:(UIButton *)button SelectedAmount:(nonnull NSString *)amount{
    self.button.tag = 400;
    [self.view endEditing:YES];
    
    if (!self.button) {
        self.button = button;
    }else{
        
        if (self.button == button) {
            return;
        }
        
        self.button.selected = NO;
        NSInteger index = self.button.tag - 600;
 
        self.button.backgroundColor = RGBA(245, 245, 245, 1);
    }
    
    button.backgroundColor = RGBA(52, 120, 245, 0.3);
    
    self.button = button;
    self.button.selected = YES;
    
    UITextField *textField = (UITextField *)[self.view viewWithTag:900];
    textField.text = nil;
    
    UILabel *label = (UILabel *)[self.view viewWithTag:700];
    label.text = [NSString stringWithFormat:@"%@",amount];
    self.selectAmount = [NSString stringWithFormat:@"%@",amount];
    NSLog(@"选中的金额 === %@",self.selectAmount);
    
}
- (void)textFieldDidChange:(UITextField *)textField {
    int kMaxLength = 9;
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入方式
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
    
    if (textField.text.length > 0) {
        UIButton *button = (UIButton *)[self.view viewWithTag:400];
        button.backgroundColor = RGBA(52, 120, 245, 0.3);
    }
    
    UILabel *label = (UILabel *)[self.view viewWithTag:700];
    label.text = [NSString stringWithFormat:@"%@",textField.text];
    self.selectAmount = textField.text;
    NSLog(@"选中的金额 === %@",self.selectAmount);
    
}
@end
