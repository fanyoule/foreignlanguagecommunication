//
//  FeedbackViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "FeedbackViewController.h"
#import "ThePictureCollectionViewCell.h"
@interface FeedbackViewController ()<UITextFieldDelegate,UITextViewDelegate,HXPhotoViewDelegate>
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property (nonatomic, strong)UILabel *titlelabel;
/** 标题星*/
@property (nonatomic, strong)UILabel *titleStarLabel;
/** 标题内容*/
@property (nonatomic, strong)UITextField *titleTextField;

/** 内容*/
@property (nonatomic, strong)UILabel *contentLabel;
/** 内容星*/
@property (nonatomic, strong)UILabel *contentStarLabel;
/** 反馈内容*/
@property (nonatomic, strong)UITextView *contentTextView;
/** 提示语*/
@property (nonatomic, strong)UILabel *cluesLabel;

/** 联系方式*/
@property (nonatomic, strong)UILabel *contactLabel;
/** */
@property (nonatomic, strong)UITextField *contactTextField;

/** 反馈图片*/
@property (nonatomic, strong)UILabel *imageLabel;
/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;

/**图片管理器*/
@property (nonatomic, strong) HXPhotoManager *photoManager;
/**选择图片View*/
@property (nonatomic, strong) HXPhotoView *photoView;

/**图片URL集合*/
@property (nonatomic, strong) NSArray *imageUrlArray;

/**存储图片/视频 model*/
@property (nonatomic, copy) NSArray *allPhotoArray;
/**图片Image*/
@property (nonatomic, strong) NSMutableArray *assetArray;
/**是否原图*/
@property (nonatomic) BOOL isOriginal;
//MARK: 判断当前是否可以点击发布按钮
//发布条件 文字/图片/视频/语音/  音频 audioPath判断    tagModel 判断
@property (nonatomic) BOOL hasText;
@property (nonatomic) BOOL hasPhoto;
@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"问题反馈";
    [self addRightBtnWith:@"提交"];
    self.navView.backgroundColor = UIColor.whiteColor;
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    self.allPhotoArray = [NSMutableArray array];
    
    [self.view addSubview:self.mainScrollView];
    self.mainScrollView.bounces = YES;
    self.mainScrollView.scrollEnabled = YES;
    self.mainScrollView.backgroundColor = kBackgroundColor;
    self.mainScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.mainScrollView.frame = CGRectMake(0, SNavBarHeight, KSW, ScreenHeight-SNavBarHeight);
    self.mainScrollView.contentSize = CGSizeMake(0, self.mainScrollView.height+20);
    
    [self.mainScrollView addSubview:self.backView];
    
    [self.backView addSubview:self.titlelabel];
    [self.backView addSubview:self.titleStarLabel];
    
    [self.backView addSubview:self.contentLabel];
    [self.backView addSubview:self.contentStarLabel];
    [self.backView addSubview:self.contentTextView];
    [self.backView addSubview:self.cluesLabel];
    [self.backView addSubview:self.contactLabel];
    
    [self.backView addSubview:self.imageLabel];
    [self.backView addSubview:self.rulesLabel];
    
    UIView *titleView = [[UIView alloc]init];
    titleView.backgroundColor = RGBA(245, 245, 245, 1);
    titleView.layer.cornerRadius = 6;
    titleView.layer.masksToBounds = YES;
    [self.backView addSubview:titleView];
    [titleView addSubview:self.titleTextField];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(20);
        make.right.equalTo(self.backView).offset(-15);
        make.top.equalTo(self.titlelabel.mas_bottom).offset(10);
        make.height.equalTo(@50);
    }];
    [self.titleTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleView).offset(15);
        make.right.bottom.top.equalTo(titleView);
    }];
    
    UIView *contactView = [[UIView alloc]init];
    contactView.backgroundColor = RGBA(245, 245, 245, 1);
    contactView.layer.cornerRadius = 6;
    contactView.layer.masksToBounds = YES;
    [self.backView addSubview:contactView];
    [contactView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(20);
        make.top.equalTo(self.contactLabel.mas_bottom).offset(10);
        make.right.equalTo(self.backView).offset(-15);
        make.height.equalTo(@50);
    }];
    [contactView addSubview:self.contactTextField];
    [self.contactTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contactView).offset(15);
        make.right.bottom.top.equalTo(contactView);
    }];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mainScrollView).offset(15);
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@550);
    }];
    [self.titlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).offset(20);
        make.left.equalTo(self.backView).offset(15);
    }];
    [self.titleStarLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titlelabel);
        make.left.equalTo(self.titlelabel.mas_right);
    }];
    
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(15);
        make.top.equalTo(self.titleTextField.mas_bottom).offset(15);
    }];
    [self.contentStarLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentLabel);
        make.left.equalTo(self.contentLabel.mas_right);
    }];
    [self.contentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentLabel.mas_bottom).offset(10);
        make.left.equalTo(self.backView).offset(20);
        make.right.equalTo(self.backView).offset(-15);
        make.height.equalTo(@150);
    }];
    [self.cluesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentTextView).offset(10);
        make.left.equalTo(self.contentTextView).offset(15);
        make.right.equalTo(self.contentTextView).offset(-10);
    }];
    
    
    [self.contactLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentTextView.mas_bottom).offset(15);
        make.left.equalTo(self.backView).offset(15);
    }];
    
    
    [self.imageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contactTextField.mas_bottom).offset(15);
        make.left.equalTo(self.backView).offset(15);
        
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.imageLabel);
        make.left.equalTo(self.imageLabel.mas_right);
    }];
    
    
    [self.mainScrollView addSubview:self.photoView];
//    self.photoView.frame = CGRectMake(0, 220, KSW, kScaleSize(110));
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageLabel.mas_bottom).offset(5);
        make.left.right.equalTo(self.backView);
        make.height.equalTo(@(kScaleSize(110)));
    }];
    
    
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.cornerRadius = 12;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UILabel *)titlelabel{
    if (!_titlelabel) {
        _titlelabel = [[UILabel alloc]init];
        _titlelabel.text = @"反馈标题";
        _titlelabel.textColor = RGBA(24, 24, 24, 1);
        _titlelabel.font = kFont_Bold(18);
        _titlelabel.textAlignment = 0;
    }
    return _titlelabel;
}
- (UILabel *)titleStarLabel{
    if (!_titleStarLabel) {
        _titleStarLabel = [[UILabel alloc]init];
        _titleStarLabel.text = @"*";
        _titleStarLabel.textColor = UIColor.redColor;
    }
    return _titleStarLabel;
}
- (UITextField *)titleTextField{
    if (!_titleTextField) {
        _titleTextField = [[UITextField alloc]init];
        _titleTextField.backgroundColor = RGBA(245, 245, 245, 1);
        _titleTextField.tag = 900;
        _titleTextField.placeholder = @"请用一句话描述您遇到的问题";
        _titleTextField.font = kFont(13);
        _titleTextField.layer.cornerRadius = 6;
        _titleTextField.layer.masksToBounds = YES;
        _titleTextField.delegate = self;
        [_titleTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    }
    return _titleTextField;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"反馈内容";
        _contentLabel.textColor = RGBA(24, 24, 24, 1);
        _contentLabel.font = kFont_Bold(18);
        _contentLabel.textAlignment = 0;
    }
    return _contentLabel;
}
- (UILabel *)contentStarLabel{
    if (!_contentStarLabel) {
        _contentStarLabel = [[UILabel alloc]init];
        _contentStarLabel.text = @"*";
        _contentStarLabel.textColor = UIColor.redColor;
    }
    return _contentStarLabel;
}
- (UITextView *)contentTextView{
    if (!_contentTextView) {
        _contentTextView = [[UITextView alloc]init];
        _contentTextView.delegate = self;
        _contentTextView.backgroundColor = RGBA(245, 245, 245, 1);
        _contentTextView.layer.cornerRadius = 6;
        _contentTextView.clipsToBounds = YES;
        _contentTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
    }
    return _contentTextView;
}

-(UILabel *)cluesLabel{
    if (!_cluesLabel) {
        _cluesLabel = [[UILabel alloc]init];
        _cluesLabel.text = @"请详细说明，以便于我们解决问题，你最多可以填写500字。";
        _cluesLabel.textColor = rgba(153, 153, 153, 1);
        _cluesLabel.font = kFont(13);
        _cluesLabel.numberOfLines = 0;
    }
    return _cluesLabel;
}

- (UILabel *)contactLabel{
    if (!_contactLabel) {
        _contactLabel = [[UILabel alloc]init];
        _contactLabel.text = @"联系方式";
        _contactLabel.textColor = RGBA(24, 24, 24, 1);
        _contactLabel.font = kFont_Bold(18);
        _contactLabel.textAlignment = 0;
    }
    return _contactLabel;
}
- (UITextField *)contactTextField{
    if (!_contactTextField) {
        _contactTextField = [[UITextField alloc]init];
        _contactTextField.backgroundColor = RGBA(245, 245, 245, 1);
        _contactTextField.tag = 901;
        _contactTextField.placeholder = @"请输入您的手机号或邮箱（以便我们及时联系）";
        _contactTextField.font = kFont(13);
        _contactTextField.layer.cornerRadius = 6;
        _contactTextField.layer.masksToBounds = YES;
        _contactTextField.delegate = self;
        [_contactTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    }
    return _contactTextField;
}
- (UILabel *)imageLabel{
    if (!_imageLabel) {
        _imageLabel = [[UILabel alloc]init];
        _imageLabel.text = @"反馈图片";
        _imageLabel.textColor = RGBA(24, 24, 24, 1);
        _imageLabel.font = kFont_Bold(18);
        _imageLabel.textAlignment = 0;
    }
    return _imageLabel;
}

- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"（最多3张）";
        _rulesLabel.textColor = RGBA(153, 153, 153, 1);
        _rulesLabel.font = kFont(13);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}


- (void)textFieldDidChange:(UITextField *)textField {
    int kMaxLength = 30;
    
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入方式
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
    
}

-(void)textViewDidChange:(UITextView*)textView {
    
    if([textView.text length] == 0){
        self.cluesLabel.hidden = NO;
        
    }else{
        self.cluesLabel.hidden = YES;
        
    }
    
}

//设置超出最大字数（140字）即不可输入 也是textview的代理方法
-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text{
    if ([text isEqualToString:@"\n"]) {     //这里"\n"对应的是键盘的 return 回收键盘之用
        
        [textView resignFirstResponder];
        
        return YES;
        
    }
    if (range.location >= 500){
        
        return NO;
        
    }else{
        return YES;
        
    }
    
}


- (void)rightClick:(UIButton *)sender{
    if (!self.titleTextField.text || self.titleTextField.text.length <= 0) {
        [self showText:@"请输入标题"];
        return;
    }else if (!self.contentTextView.text || self.contentTextView.text.length <= 0){
        [self showText:@"请输入反馈内容"];
        return;
    }
//    else if (!self.contactTextField.text || self.contactTextField.text.length <= 0){
//        [self showText:@"请输入联系方式"];
//    }
    else {
        if (self.allPhotoArray.count <= 0) {
            NSString *contact = (!self.contactTextField.text || self.contactTextField.text.length <= 0) ? @"" : self.contactTextField.text;
            [self showSVP];
            [RequestManager addUserFeedbackWithId:[[UserInfoManager shared] getUserID] contact:contact picUrls:@"" title:self.titleTextField.text views:self.contentTextView.text withSuccess:^(id  _Nullable response) {
                NSLog(@"成功 ==== %@",response);
                if ([response[@"status"] integerValue] == 200) {
                    [self.navigationController popViewControllerAnimated:YES];
                    [self showText:@"反馈成功"];
                }
            } withFail:^(NSError * _Nullable error) {
                [self showText:error.localizedDescription];
            }];
        } else {
            [self postFiles];
        }
        
    }
}



//MARK: --------------Photo View delegate------------
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {

    self.allPhotoArray = [NSArray arrayWithArray:photos];
    [self.assetArray removeAllObjects];

    self.isOriginal = isOriginal;
    if (self.allPhotoArray.count > 0) {
        self.hasPhoto = YES;
        
    }else {
        self.hasPhoto = NO;
    }
}

//MARK: -----------------net work--------------------
- (void)uploadImage {
    [RequestManager uploadImage:self.assetArray withSuccess:^(id  _Nullable response) {
        NSArray *data = response[@"data"];
        NSMutableArray *muArr = [NSMutableArray array];
        for (int i = 0; i < data.count; i ++) {
            NSString *url = data[i][@"url"];
            [muArr addObject:url];
        }
        self.imageUrlArray = muArr;
        NSLog(@"images==%@",data);
            [self getData];
    } withFail:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
    }];
}

//MARK: ---------获取图片或者视频的数据/文件地址------------
static int requestCount = 0;
- (void)postFiles {
    kWeakSelf(self);
    NSLog(@"总数 === %@",self.allPhotoArray);
    [self requestMediaSourceWith:self.allPhotoArray[requestCount] success:^(id  _Nullable response) {
       //成功 继续获取下一个
        //图片
        [weakself.assetArray insertObject:response atIndex:requestCount];
        if (requestCount == weakself.allPhotoArray.count-1) {
            //结束
            //开始上传
//            [SVProgressHUD showWithStatus:@"图片上传中"];
            [SVProgressHUD dismiss];
            [weakself uploadImage];
            
        }
        else {
            [SVProgressHUD showWithStatus:@"图片上传中"];
            requestCount ++;
            [weakself postFiles];
        }
        
    }];
}

-(void)getData{
    if (self.imageUrlArray.count == 0){
        [self showText:@"请上传图片"];
    }else{
        NSLog(@"选中的图片 ==== %@",self.imageUrlArray);
        NSString *string = [self.imageUrlArray componentsJoinedByString:@","];
        [RequestManager addUserFeedbackWithId:[[UserInfoManager shared] getUserID] contact:self.contactTextField.text picUrls:string title:self.titleTextField.text views:self.contentTextView.text withSuccess:^(id  _Nullable response) {
            NSLog(@"成功 ==== %@",response);
            if ([response[@"status"] integerValue] == 200) {
                [self.navigationController popViewControllerAnimated:YES];
                [self showText:@"反馈成功"];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
    
}


- (void)photoViewDidCancel:(HXPhotoView *)photoView {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)photoViewDidAddCellClick:(HXPhotoView *)photoView {
    [self.view endEditing:YES];
}



- (HXPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [[HXPhotoView alloc] initWithManager:self.photoManager scrollDirection:UICollectionViewScrollDirectionVertical];
        _photoView.delegate = self;
        _photoView.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
//        _photoView.spacing = (KSW-30-kScaleSize(108)*2)/2;
        _photoView.spacing = 15;
        _photoView.outerCamera = NO;
        _photoView.addImageName = @"upload_sj";
        _photoView.lineCount = 3;
        _photoView.backgroundColor = UIColor.whiteColor;
        _photoView.previewStyle = HXPhotoViewPreViewShowStyleDark;
        _photoView.layer.cornerRadius = 12;
        _photoView.clipsToBounds = YES;
    }
    return _photoView;
}
- (HXPhotoManager *)photoManager {
    if (!_photoManager) {
        _photoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        //黑色主题 视频或者图片 最多九张
        _photoManager.configuration.type = HXConfigurationTypeWXChat;
        _photoManager.configuration.videoMaxNum = 1;
        _photoManager.configuration.selectTogether = NO;
        _photoManager.configuration.videoMaximumDuration = 15;
        _photoManager.configuration.videoMaximumSelectDuration = 15;
        _photoManager.configuration.photoMaxNum = 3;
        _photoManager.configuration.showOriginalBytes = YES;
        _photoManager.configuration.showOriginalBytesLoading = YES;
        _photoManager.configuration.requestImageAfterFinishingSelection = YES;
        _photoManager.configuration.exportVideoURLForHighestQuality = NO;
    }
    return _photoManager;
}

#pragma mark -------

- (void)requestMediaSourceWith:(HXPhotoModel *)model success:(HttpResponseSuccessBlock)success{
    // 如果将_manager.configuration.requestImageAfterFinishingSelection 设为YES，
    // 那么在选择完成的时候就会获取图片和视频地址
    // 如果选中了原图那么获取图片时就是原图
    // 获取视频时如果设置 exportVideoURLForHighestQuality 为YES，则会去获取高等质量的视频。其他情况为中等质量的视频
    // 个人建议不在选择完成的时候去获取，因为每次选择完都会去获取。获取过程中可能会耗时过长
    // 可以在要上传的时候再去获取
        // 数组里装的是所有类型的资源，需要判断
        // 先判断资源类型
        if (model.subType == HXPhotoModelMediaSubTypePhoto) {
            // 当前为图片
            if (model.photoEdit) {
                // 如果有编辑数据，则说明这张图篇被编辑过了
                // 需要这样才能获取到编辑之后的图片
                UIImage *image = model.photoEdit.editPreviewImage;
                success(image);
            }
            // 再判断具体类型
            else {
                // 到这里就是手机相册里的图片了 model.asset PHAsset对象是有值的
                // 如果需要上传 Gif 或者 LivePhoto 需要具体判断
                if (model.type == HXPhotoModelMediaTypePhoto) {
                    // 普通的照片，如果不可以查看和livePhoto的时候，这就也可能是GIF或者LivePhoto了，
                    // 如果你的项目不支持动图那就不要取NSData或URL，因为如果本质是动图的话还是会变成动图传上去
                    // 这样判断是不是GIF model.photoFormat == HXPhotoModelFormatGIF
                    
                    // 如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.previewPhoto 或者 model.thumbPhoto 在选择完成时候已经获取并且赋值了
                    // 获取image
                    // size 就是获取图片的质量大小，原图的话就是 PHImageManagerMaximumSize，其他质量可设置size来获取
                    CGSize size;
                    if (self.isOriginal) {
                        size = PHImageManagerMaximumSize;
                    }else {
                        size = CGSizeMake(model.imageSize.width * 0.5, model.imageSize.height * 0.5);
                    }
                    
                    
                    [model requestPreviewImageWithSize:size startRequestICloud:^(PHImageRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        // 如果图片是在iCloud上的话会先走这个方法再去下载
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        // iCloud的下载进度
                    } success:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        // image
                        success(image);
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }else if (model.type == HXPhotoModelMediaTypePhotoGif) {
                    // 动图，如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.imageURL。因为在选择完成的时候已经获取了不用再去获取
//                    model.imageURL;
                    // 上传动图时，不要直接拿image上传哦。可以获取url或者data上传
                    // 获取data
                    NSLog(@"modelURL==%@",model.imageURL);
                    [model requestImageURLStartRequestICloud:^(PHContentEditingInputRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        
                    } success:^(NSURL * _Nullable imageURL, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        NSLog(@"URL==%@",imageURL);
                        
                        success(imageURL);
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        
                    }];
                }else if (model.type == HXPhotoModelMediaTypeLivePhoto) {
                    // LivePhoto，requestImageAfterFinishingSelection = YES 时没有处理livephoto，需要自己处理
                    // 如果需要上传livephoto的话，需要上传livephoto里的图片和视频
                    // 展示的时候需要根据图片和视频生成livephoto
                    [model requestLivePhotoAssetsWithSuccess:^(NSURL * _Nullable imageURL, NSURL * _Nullable videoURL, BOOL isNetwork, HXPhotoModel * _Nullable model) {
                        // imageURL - LivePhoto里的照片封面地址
                        // videoURL - LivePhoto里的视频地址
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }
                // 也可以不用上面的判断和方法获取，自己根据 model.asset 这个PHAsset对象来获取想要的东西
//                PHAsset *asset = model.asset;
            }
            
        }
}

- (NSMutableArray *)assetArray{
    if (!_assetArray) {
        _assetArray = [[NSMutableArray alloc]init];
    }
    return _assetArray;
}

@end
