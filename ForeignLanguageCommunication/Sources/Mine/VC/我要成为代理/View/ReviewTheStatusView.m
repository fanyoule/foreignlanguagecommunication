//
//  ReviewTheStatusView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "ReviewTheStatusView.h"

@implementation ReviewTheStatusView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self=  [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.resultsLabel];
    [self addSubview:self.reviewImageV];
    [self addSubview:self.whyLabel];
    [self.reviewImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self).offset(25);
    }];
    [self.resultsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.reviewImageV.mas_bottom).offset(10);
    }];
    [self.whyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.resultsLabel.mas_bottom).offset(5);
    }];
}

- (void)setModel:(TheAgentInformationModel *)model{
    if (model.status == -1) {
        self.reviewImageV.image = kImage(@"审核-失败");
        self.resultsLabel.text = @"审核失败";
        self.whyLabel.text = [NSString stringWithFormat:@"拒绝原因：%@",model.refuseReason];
        
    }else if (model.status == 0){
        self.reviewImageV.image = kImage(@"审核-正在审核");
        self.resultsLabel.text = @"审核中，等待平台联系你";
        self.whyLabel.hidden = YES;
    }
    
    
}


- (UIImageView *)reviewImageV{
    if (!_reviewImageV) {
        _reviewImageV = [[UIImageView alloc]init];
        _reviewImageV.image = [UIImage imageNamed:@"审核-正在审核"];
    }
    return _reviewImageV;
}

- (UILabel *)resultsLabel{
    if (!_resultsLabel) {
        _resultsLabel = [[UILabel alloc]init];
        _resultsLabel.text = @"审核中，等待平台联系你";
        _resultsLabel.textColor = RGBA(24, 24, 24, 1);
        _resultsLabel.font = kFont_Medium(14);
        _resultsLabel.textAlignment = 1;
    }
    return _resultsLabel;
}

- (UILabel *)whyLabel{
    if (!_whyLabel) {
        _whyLabel = [[UILabel alloc]init];
        _whyLabel.text = @"拒绝原因：原因原因原因";
        _whyLabel.textColor = RGBA(153, 153, 153, 1);
        _whyLabel.font = kFont_Medium(13);
        _whyLabel.textAlignment = 1;
    }
    return _whyLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
