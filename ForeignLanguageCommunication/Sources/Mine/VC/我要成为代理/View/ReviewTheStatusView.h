//
//  ReviewTheStatusView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import <UIKit/UIKit.h>
#import "TheAgentInformationModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReviewTheStatusView : UIView
/** 审核图片*/
@property (nonatomic, strong)UIImageView *reviewImageV;
/** 审核结果*/
@property (nonatomic, strong)UILabel *resultsLabel;
/** 拒绝原因*/
@property (nonatomic, strong)UILabel *whyLabel;


@property (nonatomic, strong)TheAgentInformationModel *model;
@end

NS_ASSUME_NONNULL_END
