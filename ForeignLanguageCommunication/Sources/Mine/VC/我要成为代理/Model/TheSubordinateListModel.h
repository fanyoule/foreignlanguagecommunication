//
//  TheSubordinateListModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TheSubordinateListModel : NSObject
/** 头像*/
@property (nonatomic, strong)NSString *avatar;
/** 昵称*/
@property (nonatomic, strong)NSString *nickname;
/** 可添加下属数量*/
@property (nonatomic, assign)NSInteger num;
/** 总收益*/
@property (nonatomic, assign)NSInteger totalProfit;
/** 下属id*/
@property (nonatomic, assign)NSInteger userId;

@property (nonatomic)BOOL choose;
@end

NS_ASSUME_NONNULL_END
