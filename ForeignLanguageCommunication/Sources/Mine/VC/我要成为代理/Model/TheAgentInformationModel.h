//
//  TheAgentInformationModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TheAgentInformationModel : NSObject
/** 代理区域*/
@property (nonatomic, strong)NSString *agentArea;
/** 代理等级*/
@property (nonatomic, assign)NSInteger agentLevel;
/** 经验简介*/
@property (nonatomic, strong)NSString *content;
/** 直系收益*/
@property (nonatomic, assign)NSInteger directProfit;
/** 代理邀请码*/
@property (nonatomic, strong)NSString *inviteCode;
/** 手机*/
@property (nonatomic, strong)NSString *phone;
/** 真实姓名*/
@property (nonatomic, strong)NSString *realName;
/** 拒绝理由*/
@property (nonatomic, strong)NSString *refuseReason;
/** 二级收益*/
@property (nonatomic, assign)NSInteger secProfit;
/** 性别0：女，1：男*/
@property (nonatomic, assign)NSInteger sex;
/** 状态，-1,：拒绝，0：待审核，1：正常，2：取消代理*/
@property (nonatomic, assign)NSInteger status;
/** 下属人数*/
@property (nonatomic, assign)NSInteger subordinateNum;
/** 今日绑定用户数量*/
@property (nonatomic, assign)NSInteger todayBind;
/** 今日收益*/
@property (nonatomic, assign)NSInteger todayProfit;
/** 总绑定用户数量*/
@property (nonatomic, assign)NSInteger totalBind;
/** 总收益*/
@property (nonatomic, assign)NSInteger totalProfit;
/** 用户id*/
@property (nonatomic, assign)NSInteger userId;
/** 已提现*/
@property (nonatomic, assign)NSInteger withdraw;



@end

NS_ASSUME_NONNULL_END
