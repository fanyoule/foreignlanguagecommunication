//
//  WantTheAgentViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//  我要成为代理

#import "WantTheAgentViewController.h"
#import "TheNewCurriculumTableViewCell.h"
#import "NewCourseSelectionTableViewCell.h"

#import "TheGrayBarTableViewCell.h"
#import "PersonalProfileTableViewCell.h"
#import "ReviewTheStatusView.h"// 审核状态View
#import "CitiyPickerView.h"
@interface WantTheAgentViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
/** 姓名*/
@property (nonatomic, strong)NSString *nameStr;
/** 手机号*/
@property (nonatomic, strong)NSString *phoneStr;
/** 简介*/
@property (nonatomic, strong)NSString *introductionStr;
/** 代理区域*/
@property (nonatomic, strong)NSString *areaStr;
@end

@implementation WantTheAgentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"我要成为代理";
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    // Do any additional setup after loading the view.
}

- (void)setInformationModel:(TheAgentInformationModel *)informationModel{
    _informationModel = informationModel;
    [self.mainTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 6;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ( indexPath.row == 2|| indexPath.row == 4) {
        return 10;
    }else if (indexPath.row == 5){
        return 200;
    }else{
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    kWeakSelf(self)
    if ( indexPath.row == 2|| indexPath.row == 4) {
        TheGrayBarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheGrayBarTableViewCell"];
        if (!cell) {
            cell = [[TheGrayBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheGrayBarTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }
    if (indexPath.row == 5) {
        PersonalProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalProfileTableViewCell"];
        if (!cell) {
            cell = [[PersonalProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonalProfileTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        cell.promptLabel.text = @"你的经验简介~";
        cell.profileLabel.text = @"经验简介";
        cell.textView.delegate = self;
        cell.promptLabel.tag = 800;
        if (self.isJudge == 2) {
            cell.textView.editable = YES;
        }else{
            cell.promptLabel.hidden = YES;
            cell.textView.text = self.informationModel.content;
            if (self.informationModel.status == 0) {
                cell.textView.editable = NO;
                
            }else{
                cell.textView.editable = YES;
                self.introductionStr = self.informationModel.content;
            }
            
        }
        
    return cell;
    }
    
    if (indexPath.row == 3) {
        NewCourseSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewCourseSelectionTableViewCell"];
        if (!cell) {
            cell = [[NewCourseSelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NewCourseSelectionTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        
        [cell whenTapped:^{
            [weakself.view endEditing:YES];
            [CitiyPickerView showPickerViewWithComplete:^(NSString * _Nonnull province, NSString * _Nonnull city, NSString * _Nonnull area) {
                    NSLog(@"%@%@%@",province,city,area);
                cell.label.text = [NSString stringWithFormat:@"%@-%@-%@",province,city,area];
                cell.label.textColor = RGBA(24, 24, 24, 1);
                
                weakself.areaStr = [NSString stringWithFormat:@"%@%@%@",province,city,area];
            }];
        }];
        if (self.isJudge == 2) {
            cell.titleImageV.image = [UIImage imageNamed:@"代理 - 区域"];
            cell.label.text = @"请填写代理区域";
        }else{
            cell.label.textColor = RGBA(24, 24, 24, 1);
            cell.label.text = self.informationModel.agentArea;
            if (self.informationModel.status == 0) {
                [cell whenTapped:^{
                    
                }];
                
            }else{
                self.areaStr = self.informationModel.agentArea;
            }
            
        }
        
        
        return cell;
    }
    
    TheNewCurriculumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheNewCurriculumTableViewCell"];
        if (!cell) {
            cell = [[TheNewCurriculumTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheNewCurriculumTableViewCell"];
        }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    cell.isJudge = 1;
    
    if (indexPath.row == 0) {
        cell.titleImageV.image = [UIImage imageNamed:@"代理 - 姓名"];
        cell.textField.placeholder = @"请填写真实姓名";
        if (self.isJudge == 2) {
            cell.textField.enabled = YES;
        }else{
            cell.textField.text = self.informationModel.realName;
            if (self.informationModel.status == 0) {
                cell.textField.enabled = NO;
                
            }else{
                cell.textField.enabled = YES;
                self.nameStr = self.informationModel.realName;
            }
            
        }
        cell.mobilePhone = ^(NSString *string) {
            weakself.nameStr = string;
        };
    }else if (indexPath.row == 1) {
        cell.titleImageV.image = [UIImage imageNamed:@"代理 - 手机号"];
        cell.textField.placeholder = @"请填写手机号";
        
        if (self.isJudge == 2) {
            cell.textField.enabled = YES;
        }else{
            cell.textField.text = self.informationModel.phone;
            if (self.informationModel.status == 0) {
                cell.textField.enabled = NO;
                
            }else{
                cell.textField.enabled = YES;
                self.phoneStr = self.informationModel.phone;
            }
            
        }
        
        cell.mobilePhone = ^(NSString *string) {
            weakself.phoneStr = string;
        };
    }
    
        return cell;
    
    
        
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ReviewTheStatusView *view = [[ReviewTheStatusView alloc]init];
    view.model = self.informationModel;
    view.backgroundColor = RGBA(245, 245, 245, 1);
    
    UIView *emptyView = UIView.new;
    emptyView.backgroundColor = UIColor.clearColor;
    
    
    // 根据状态改变header头的高度显示审核状态
    if (self.isJudge == 2) {
        return emptyView;
    }else{
        return view;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    if (self.isJudge == 2) {
        return 10;
    }else{
        return 110;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.clearColor;
    
    UIButton *button = [[UIButton alloc]init];
    button.backgroundColor = RGBA(52, 120, 245, 1);
    button.layer.cornerRadius = 6;
    button.clipsToBounds = YES;
    [button setTitle:@"提交" forState:(UIControlStateNormal)];
    [button setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
    button.titleLabel.font = kFont_Bold(15);
    [button addTarget:self action:@selector(clickTheSubmit:) forControlEvents:(UIControlEventTouchDown)];
    [view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.left.equalTo(view).offset(15);
        make.right.equalTo(view).offset(-15);
        make.height.equalTo(@50);
    }];
    
    
    // 根据状态来 隐藏提交按钮
    if (self.isJudge == 2) {
        button.hidden = NO;
    }else{
        if (self.informationModel.status == 0) {
            button.hidden = YES;
        }else if (self.informationModel.status == -1){
            [button setTitle:@"重新提交" forState:(UIControlStateNormal)];
            button.hidden = NO;
        }
    }
    
    
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 110;
}

-(void)clickTheSubmit:(UIButton *)button{
    NSLog(@"点击了提交");
    if (self.nameStr.length <= 0) {
        [self showText:@"请填写真实姓名"];
    }else if (self.phoneStr.length <= 0){
        [self showText:@"请填写手机号"];
    }else if (self.areaStr.length <= 0){
        [self showText:@"请选择代理区域"];
    }else if (self.introductionStr.length <= 0){
        [self showText:@"请填写经验简介"];
    }else{
        [RequestManager addAgentWithId:[[UserInfoManager shared] getUserID] agentArea:self.areaStr content:self.introductionStr name:self.nameStr phone:self.phoneStr withSuccess:^(id  _Nullable response) {
            [self.navigationController popViewControllerAnimated:YES];
            
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
}



- (void)textViewDidChange:(UITextView *)textView{
    UILabel *label = (UILabel *)[self.view viewWithTag:800];
    label.hidden = YES;
    self.introductionStr = textView.text;
    //字数限制操作

    if (textView.text.length >= 200) {
    
        textView.text = [textView.text substringToIndex:10];
        
    }
    
    //取消按钮点击权限，并显示提示文字

    if (textView.text.length <= 0) {
        
        label.hidden = NO;
    
    }

}

@end
