//
//  WantTheAgentViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "BaseViewController.h"
#import "TheAgentInformationModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface WantTheAgentViewController : BaseViewController

@property (nonatomic, strong)TheAgentInformationModel *informationModel;
/** 判断是否申请过成为代理  1：申请过  2：没申请过*/
@property (nonatomic, assign)NSInteger isJudge;

@end

NS_ASSUME_NONNULL_END
