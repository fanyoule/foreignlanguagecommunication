//
//  SubordinateDetailsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//  下属详情

#import "SubordinateDetailsViewController.h"
#import "AgentContentTableViewCell.h"
#import "TotalRevenueTableViewCell.h"
#import "ReturnsDetailedTableViewCell.h"

#import "TheAgentInformationModel.h"
@interface SubordinateDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)TheAgentInformationModel *model;

@end

@implementation SubordinateDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"下属详情";
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    // Do any additional setup after loading the view.
}
- (void)setListModel:(TheSubordinateListModel *)listModel{
    _listModel = listModel;
    [self getData];
}

-(void)getData{
    [RequestManager agentInfoWithId:self.listModel.userId withSuccess:^(id  _Nullable response) {
        NSDictionary *dic = response[@"data"];
        
        if ([response[@"status"] integerValue] == 200) {
            
            self.model = [TheAgentInformationModel mj_objectWithKeyValues:dic];
            
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
- (TheAgentInformationModel *)model{
    if (!_model) {
        _model = [[TheAgentInformationModel alloc]init];
    }
    return _model;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 100;
    }else if (indexPath.row == 1){
        return 90;
    }else{
        return 65;
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        AgentContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AgentContentTableViewCell"];
        if (!cell) {
            cell = [[AgentContentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AgentContentTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = RGBA(245, 245, 245, 1);
        cell.titleLabel.hidden = YES;
        cell.model  =self.model;
        return cell;
    }else if (indexPath.row == 1){
        TotalRevenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TotalRevenueTableViewCell"];
        if (!cell) {
            cell = [[TotalRevenueTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TotalRevenueTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = RGBA(255, 255, 255, 1);
        cell.detailBtn.hidden = YES;
        cell.numberLabel.text = [NSString stringWithFormat:@"+%ld",self.model.totalProfit];
        return cell;
    }else{
        ReturnsDetailedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReturnsDetailedTableViewCell"];
        if (!cell) {
            cell = [[ReturnsDetailedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReturnsDetailedTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = RGBA(255, 255, 255, 1);
        if (indexPath.row == 2) {
            cell.EImageV1.hidden = YES;
            cell.EImageV2.hidden = YES;
            cell.leftNumberLabel.text = [NSString stringWithFormat:@"%ld",self.model.todayBind];
            cell.rightNumberLabel.text = [NSString stringWithFormat:@"%ld",self.model.totalBind];
        }else if (indexPath.row == 3){
            cell.leftTitleLabel.text = @"今日收益";
            cell.rightTitleLabel.text = @"直系收益";
            cell.leftNumberLabel.text = [NSString stringWithFormat:@"%ld",self.model.todayProfit];
            cell.rightNumberLabel.text = [NSString stringWithFormat:@"%ld",self.model.directProfit];
        }
        return cell;
    }
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
