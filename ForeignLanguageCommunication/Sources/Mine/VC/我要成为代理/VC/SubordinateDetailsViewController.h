//
//  SubordinateDetailsViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "BaseViewController.h"
#import "TheSubordinateListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SubordinateDetailsViewController : BaseViewController

@property (nonatomic, strong)TheSubordinateListModel *listModel;

@end

NS_ASSUME_NONNULL_END
