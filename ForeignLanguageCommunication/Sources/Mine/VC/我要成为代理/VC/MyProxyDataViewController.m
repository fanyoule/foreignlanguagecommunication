//
//  MyProxyDataViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//  我的代理数据

#import "MyProxyDataViewController.h"
#import "AgentContentTableViewCell.h"
#import "TotalRevenueTableViewCell.h"
#import "ReturnsDetailedTableViewCell.h"
#import "MyStaffTableViewCell.h"
#import "TheWalletDetailsViewController.h"// 明细
#import "WithdrawalViewController.h"// 提现
#import "AgentThatViewController.h"// 说明
#import "ManagementSubordinatesViewController.h"// 管理下属
#import "TheAgentInformationModel.h"
#import "TheSubordinateListModel.h"
#import "DetailsOfMyWalletModel.h"
@interface MyProxyDataViewController ()<UITableViewDelegate,UITableViewDataSource>
/** 通知背景*/
@property (nonatomic, strong)UIView *backView;
/** 通知文字*/
@property (nonatomic, strong)UILabel *noticeLabel;

@property (nonatomic, strong)NSMutableArray *listArray;
@property (nonatomic, strong)TheAgentInformationModel *informationModel;


@property (nonatomic, assign)NSInteger balance;
@end

@implementation MyProxyDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"我的代理数据";
    [self addRightBtnWith:@"说明"];
    [self.rightBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
//    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    
    [self getTheBalance];
    [self getData];
}
#pragma mark 添加顶部View
-(void)addTopView{
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.noticeLabel];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.height.equalTo(@35);
        make.left.right.equalTo(self.view);
    }];
    [self.noticeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.backView);
    }];
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(243, 229, 229, 1);
    }
    return _backView;
}
- (UILabel *)noticeLabel{
    if (!_noticeLabel) {
        _noticeLabel = [[UILabel alloc]init];
        _noticeLabel.text = @"你已被取消代理，请联系上级或官方.";
        _noticeLabel.textColor = RGBA(230, 86, 86, 1);
        _noticeLabel.font = kFont_Medium(14);
        _noticeLabel.textAlignment = 1;
    }
    return _noticeLabel;
}
- (TheAgentInformationModel *)informationModel{
    if (!_informationModel) {
        _informationModel = [[TheAgentInformationModel alloc]init];
    }
    return _informationModel;
}
-(void)getData{
    
    [RequestManager agentInfoWithId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSDictionary *dic = response[@"data"];
        NSArray *array = response[@"data"][@"subordinate"];
        if ([response[@"status"] integerValue] == 200) {
            
            self.informationModel = [TheAgentInformationModel mj_objectWithKeyValues:dic];
            
            for (int i = 0; i < array.count; i++) {
                TheSubordinateListModel *model = [TheSubordinateListModel mj_objectWithKeyValues:array[i]];
                [self.listArray addObject:model];
            }
            self.backView.hidden = YES;// 根据数据来判断是否隐藏
        }else if ([response[@"status"] integerValue] == 2){
            [self addTopView];//添加顶部View
            [self.mainTableView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.backView.mas_bottom);
            }];
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

- (NSMutableArray *)listArray{
    if (!_listArray) {
        _listArray = [[NSMutableArray alloc]init];
    }
    return _listArray;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 5;
    }else if (section == 1){
        return self.listArray.count;
    }else{
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 100;
        }else if (indexPath.row == 1){
            return 95;
        }else{
            return 65;
        }
    }else if (indexPath.section == 1){
        return 65;
    }else{
        return 0;
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            AgentContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AgentContentTableViewCell"];
            if (!cell) {
                cell = [[AgentContentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AgentContentTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = RGBA(245, 245, 245, 1);
            cell.model = self.informationModel;
            return cell;
        }else if (indexPath.row == 1){
            TotalRevenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TotalRevenueTableViewCell"];
            if (!cell) {
                cell = [[TotalRevenueTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TotalRevenueTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = RGBA(255, 255, 255, 1);
            [cell.detailBtn addTarget:self action:@selector(clickTheDetails) forControlEvents:(UIControlEventTouchDown)];
            
            cell.numberLabel.text = [NSString stringWithFormat:@"+%ld",self.informationModel.totalProfit];
            
            return cell;
        }else{
            ReturnsDetailedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReturnsDetailedTableViewCell"];
            if (!cell) {
                cell = [[ReturnsDetailedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReturnsDetailedTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = RGBA(255, 255, 255, 1);
            if (indexPath.row == 2) {
                cell.EImageV1.hidden = YES;
                cell.EImageV2.hidden = YES;
                cell.leftNumberLabel.text = [NSString stringWithFormat:@"%ld",self.informationModel.todayBind];
                cell.rightNumberLabel.text = [NSString stringWithFormat:@"%ld",self.informationModel.totalBind];
            }else if (indexPath.row == 3){
                cell.leftTitleLabel.text = @"今日收益";
                cell.rightTitleLabel.text = @"直系收益";
                cell.leftNumberLabel.text = [NSString stringWithFormat:@"%ld",self.informationModel.todayProfit];
                cell.rightNumberLabel.text = [NSString stringWithFormat:@"%ld",self.informationModel.directProfit];
            }else if (indexPath.row == 4){
                cell.leftTitleLabel.text = @"二级收益";
                cell.rightTitleLabel.text = @"已提现";
                cell.leftNumberLabel.text = [NSString stringWithFormat:@"%ld",self.informationModel.secProfit];
                cell.rightNumberLabel.text = [NSString stringWithFormat:@"%ld",self.informationModel.withdraw];
            }
            return cell;
        }
    }else{
        MyStaffTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyStaffTableViewCell"];
        if (!cell) {
            cell = [[MyStaffTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyStaffTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = RGBA(255, 255, 255, 1);
        TheSubordinateListModel *model = [[TheSubordinateListModel alloc]init];
        model = self.listArray[indexPath.row];
        cell.model = model;
        
        
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView *view = UIView.new;
    view.backgroundColor = RGBA(245, 245, 245, 1);
    UILabel *label = [[UILabel alloc]init];
    label.text = @"下属人数(3人)";
    label.textColor = RGBA(24, 24, 24, 1);
    label.font = kFont_Bold(15);
    label.textAlignment = 0;
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.left.equalTo(view).offset(15);
    }];
    UIButton *button = [[UIButton alloc]init];
    [button setTitle:@"管理下属" forState:(UIControlStateNormal)];
    [button setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
    button.titleLabel.font = kFont_Medium(14);
    [button addTarget:self action:@selector(clickManageSubordinate) forControlEvents:(UIControlEventTouchDown)];
    [view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view);
        make.right.equalTo(view).offset(-15);
        make.height.equalTo(view);
    }];
    
    
    UIView *withdrawalView = UIView.new;
    withdrawalView.backgroundColor = UIColor.clearColor;
    UIButton *withdrawalBtn = [[UIButton alloc]init];
    withdrawalBtn.backgroundColor = RGBA(52, 120, 245, 1);
    withdrawalBtn.layer.cornerRadius = 6;
    withdrawalBtn.clipsToBounds = YES;
    [withdrawalBtn setTitle:@"提现" forState:(UIControlStateNormal)];
    [withdrawalBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
    withdrawalBtn.titleLabel.font = kFont_Bold(15);
    [withdrawalBtn addTarget:self action:@selector(clickTheSubmit:) forControlEvents:(UIControlEventTouchDown)];
    withdrawalBtn.hidden = YES;
    [withdrawalView addSubview:withdrawalBtn];
    [withdrawalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(withdrawalView);
        make.left.equalTo(withdrawalView).offset(15);
        make.right.equalTo(withdrawalView).offset(-15);
        make.height.equalTo(@50);
    }];
    
    if (self.informationModel.agentLevel == 1) {
        label.text = [NSString stringWithFormat:@"下属人数(%ld人)",self.listArray.count];
    }else{
        label.text = @"上级代理";
        button.hidden = YES;
    }
    
    if (section == 0) {
        return nil;
    }else if (section == 1) {
        return view;
    }else{
        return withdrawalView;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else if (section == 1){
        return 40;
    }else{
        return 110;
    }
    
}

// MARK: 管理下属
-(void)clickManageSubordinate{
    NSLog(@"点击了管理下属");
    ManagementSubordinatesViewController *vc = [[ManagementSubordinatesViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
// MARK: 提现
-(void)clickTheSubmit:(UIButton *)button{
    NSLog(@"点击了提现");
    WithdrawalViewController *vc = [[WithdrawalViewController alloc]init];
    vc.balance = self.balance;
    [self.navigationController pushViewController:vc animated:YES];
}
// MARK: 明细
-(void)clickTheDetails{
    NSLog(@"点击了明细");
    TheWalletDetailsViewController *vc = [[TheWalletDetailsViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)rightClick:(UIButton *)sender{
    AgentThatViewController *vc = [[AgentThatViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)getTheBalance{
    [RequestManager getWalletByUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"钱包详情 === %@",response);
        if ([response[@"status"] integerValue] == 200) {
            
            DetailsOfMyWalletModel *model = [DetailsOfMyWalletModel mj_objectWithKeyValues:response[@"data"][@"locationConfig"]];
            self.balance = model.balance;
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
