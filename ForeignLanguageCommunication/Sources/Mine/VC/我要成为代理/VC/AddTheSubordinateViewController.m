//
//  AddTheSubordinateViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//  添加下属

#import "AddTheSubordinateViewController.h"
#import "AddSubordinateTableViewCell.h"

#import "TheSubordinateListModel.h"
#import "YKSearchTextField.h"
@interface AddTheSubordinateViewController ()<UITableViewDelegate,UITableViewDataSource,SearchViewDelegate>


@property (nonatomic, strong)NSMutableArray *dataArray;

/** 选中的数据*/
@property (nonatomic, strong)NSMutableArray *selectedArray;

@end

@implementation AddTheSubordinateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"添加下属";
    [self addRightBtnWith:@"确定"];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(238, 238, 238, 1);
    [self.navView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.left.equalTo(self.navView);
        make.height.equalTo(@0.5);
    }];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(255, 255, 255, 1);
//    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    [self getData];
}
// MARK: 添加下属列表
-(void)getData{
    [RequestManager addSubordinateListWithId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                TheSubordinateListModel *model = [TheSubordinateListModel mj_objectWithKeyValues:array[i]];
                [self.dataArray addObject:model];
            }
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


- (NSMutableArray *)selectedArray{
    if (!_selectedArray) {
        _selectedArray = [[NSMutableArray alloc]init];
    }
    return _selectedArray;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = UIColor.clearColor;
    YKSearchTextField *textField = [[YKSearchTextField alloc] init];
    textField.frame = CGRectMake(25, 10, APPSIZE.width-50, 35);
    textField.placeHolder = @"搜索ID";
    textField.placeHolderPosition = SearchViewPlaceHolderPositionCenter;
    textField.layer.cornerRadius = 35/2;
    textField.delegate = self;
    [view addSubview:textField];
    
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 65;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddSubordinateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddSubordinateTableViewCell"];
    if (!cell) {
        cell = [[AddSubordinateTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddSubordinateTableViewCell"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = RGBA(255, 255, 255, 1);
    
    TheSubordinateListModel *model = [[TheSubordinateListModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    
    [cell.chooseBtn whenTapped:^{
        cell.chooseBtn.selected = !cell.chooseBtn.isSelected;
        model.choose = cell.chooseBtn.isSelected;
        if (cell.chooseBtn.selected) {
            [self.selectedArray addObject:@(model.userId)];
        }else{
            [self.selectedArray removeObject:@(model.userId)];
        }
        if (self.selectedArray != nil && ![self.selectedArray isKindOfClass:[NSNull class]] && (self.selectedArray.count != 0)) {
            [self.rightBtn setTitle:[NSString stringWithFormat:@"确定(%ld)",self.selectedArray.count] forState:(UIControlStateSelected)];
            
        }else{
            [self.rightBtn setTitle:@"确定" forState:(UIControlStateSelected)];
        }
        
    }];
    
    return cell;
    
    
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//
//}

- (void)rightClick:(UIButton *)sender{
    NSString *ids = [self.selectedArray componentsJoinedByString:@","];
    if (self.selectedArray != nil && ![self.selectedArray isKindOfClass:[NSNull class]] && (self.selectedArray.count != 0)) {
        [RequestManager addSubordinateWithId:[[UserInfoManager shared] getUserID] subordinateIds:ids withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self.navigationController popViewControllerAnimated:YES];
                if ([self.delegate respondsToSelector:@selector(clickAddTheSubordinateViewController)]) {
                    [self.delegate clickAddTheSubordinateViewController];
                }
                
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }else{
        [self showText:@"请选择下属"];
    }
    
}

- (void)searchViewDIdChangeText:(UITextField *)textField{
    NSLog(@"输入的字4 === %@",textField.text);
    NSMutableArray *nameArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < self.dataArray.count; i++) {
        TheSubordinateListModel *model = [[TheSubordinateListModel alloc]init];
        model = self.dataArray[i];
        if ([model.nickname containsString:textField.text]) {
            [nameArray addObject:model];
        }
    }
    if (textField.text.length <= 0) {
        [self.dataArray removeAllObjects];
        [self getData];
    }else{
        self.dataArray = nameArray;
        [self.mainTableView reloadData];
        
    }
    
    NSLog(@"全部下属名字 == %@",nameArray);
    
}
@end
