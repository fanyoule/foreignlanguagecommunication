//
//  AddTheSubordinateViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@protocol AddTheSubordinateViewControllerDelegate<NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)clickAddTheSubordinateViewController;

@end
@interface AddTheSubordinateViewController : BaseViewController

@property (nonatomic, weak) id<AddTheSubordinateViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
