//
//  AgentThatViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//  代理说明

#import "AgentThatViewController.h"
#import <WebKit/WebKit.h>
@interface AgentThatViewController ()<WKUIDelegate>

@end

@implementation AgentThatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"说明";
    
    self.navView.backgroundColor = [UIColor whiteColor];
    // 初始化配置对象
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    // 默认是NO，这个值决定了用内嵌HTML5播放视频还是用本地的全屏控制
    configuration.allowsInlineMediaPlayback = YES;
    // 自动播放, 不需要用户采取任何手势开启播放
    if (@available(iOS 10.0, *)) {
      // WKAudiovisualMediaTypeNone 音视频的播放不需要用户手势触发, 即为自动播放
        configuration.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypeAudio;
    } else {
        configuration.mediaTypesRequiringUserActionForPlayback = NO;
    }
    WKWebView *webView = [[WKWebView alloc]initWithFrame:CGRectMake(0,self.navView.size.height, APPSIZE.width, APPSIZE.height-TabbarSafeMargin) configuration:configuration];
    webView.UIDelegate =self;
    webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    webView.contentMode = UIViewContentModeRedraw;
    webView.opaque = YES;
    [self.view addSubview:webView];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://pic.sxsyingyu.com/gwugwbfhj8u7lejqykoa.html"]]];
    // Do any additional setup after loading the view.
}

@end
