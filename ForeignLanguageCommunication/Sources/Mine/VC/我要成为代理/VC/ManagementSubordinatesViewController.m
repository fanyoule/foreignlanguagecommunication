//
//  ManagementSubordinatesViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//  管理下属

#import "ManagementSubordinatesViewController.h"
#import "MyStaffTableViewCell.h"
#import "SubordinateDetailsViewController.h"// 下属详情
#import "AddTheSubordinateViewController.h"// 添加下属

#import "TheSubordinateListModel.h"
@interface ManagementSubordinatesViewController ()<UITableViewDelegate,UITableViewDataSource,AddTheSubordinateViewControllerDelegate>

/** 通知背景*/
@property (nonatomic, strong)UIView *backView;
/** 通知文字*/
@property (nonatomic, strong)UILabel *noticeLabel;

@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation ManagementSubordinatesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"管理下属";
    [self addRightBtnWith:@"添加下属"];
    [self addTopView];
    
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(255, 255, 255, 1);
//    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    [self getData];
    
}
#pragma mark 添加顶部View
-(void)addTopView{
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.noticeLabel];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.height.equalTo(@35);
        make.left.right.equalTo(self.view);
    }];
    [self.noticeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.backView);
    }];
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(214, 228, 253, 1);
    }
    return _backView;
}
- (UILabel *)noticeLabel{
    if (!_noticeLabel) {
        _noticeLabel = [[UILabel alloc]init];
        _noticeLabel.text = @"添加下属：必须是通过你的二维码下载或你的好友";
        _noticeLabel.textColor = RGBA(52, 120, 245, 1);
        _noticeLabel.font = kFont_Medium(14);
        _noticeLabel.textAlignment = 1;
    }
    return _noticeLabel;
}
// MARK: 下属列表
-(void)getData{
    [self.dataArray removeAllObjects];
    [RequestManager subordinateListWithId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"下属列表 ==== %@",response);
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                TheSubordinateListModel *model = [TheSubordinateListModel mj_objectWithKeyValues:array[i]];
                [self.dataArray addObject:model];
            }
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 65;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyStaffTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyStaffTableViewCell"];
    if (!cell) {
        cell = [[MyStaffTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyStaffTableViewCell"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = RGBA(255, 255, 255, 1);
    TheSubordinateListModel *model = [[TheSubordinateListModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    
        MGSwipeButton *deleteButton=[MGSwipeButton buttonWithTitle:@"解除代理" backgroundColor:[UIColor colorWithRed:255/255.0 green:53/255.0 blue:82/255.0 alpha:1.0]];
        deleteButton.buttonWidth = 70;
        deleteButton.layer.cornerRadius = 0;
        deleteButton.titleLabel.font =  [UIFont systemFontOfSize: 12.0];
        deleteButton.callback = ^BOOL(MGSwipeTableCell * _Nonnull cell) {
            
            
            UIAlertController *controller=[UIAlertController alertControllerWithTitle:@"确定解除代理" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *act2=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                UITextField *newNameTextField = controller.textFields.firstObject;
                NSLog(@"newNameTextField ==== %@",newNameTextField.text);
                [self showSVP];
                [RequestManager removeSubordinateWithId:model.userId withSuccess:^(id  _Nullable response) {
                    [self showText:@"解除成功"];
                    [self getData];
                    
                } withFail:^(NSError * _Nullable error) {
                    [self dissSVP];
                }];
            }];
            UIAlertAction *act1=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                     
            }];
            [controller addAction:act1];
            [controller addAction:act2];
            [self presentViewController:controller animated:YES completion:^{
                                    
            }];

            return YES;
        };
        cell.rightButtons = @[deleteButton];
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
        cell.rightSwipeSettings.bottomMargin =0;//距离底部
        cell.rightSwipeSettings.topMargin = 0;//button距离t顶部
        cell.rightSwipeSettings.offset = 0;//最右侧button距离边框的距离
        cell.rightSwipeSettings.buttonsDistance = 0;//button的间距
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TheSubordinateListModel *model = [[TheSubordinateListModel alloc]init];
    model = self.dataArray[indexPath.row];
    SubordinateDetailsViewController *vc = [[SubordinateDetailsViewController alloc]init];
    vc.listModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)rightClick:(UIButton *)sender{
    AddTheSubordinateViewController *vc = [[AddTheSubordinateViewController alloc]init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)clickAddTheSubordinateViewController{
    [self getData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
