//
//  AgentContentTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import <UIKit/UIKit.h>
#import "TheAgentInformationModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface AgentContentTableViewCell : UITableViewCell
/** 邀请码*/
@property (nonatomic, strong)UILabel *inviteCodeLabel;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;



@property (nonatomic, strong)TheAgentInformationModel *model;
@end

NS_ASSUME_NONNULL_END
