//
//  ReturnsDetailedTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "ReturnsDetailedTableViewCell.h"
@interface ReturnsDetailedTableViewCell ()
/** 线*/
@property (nonatomic, strong)UIView *lineView1;
@property (nonatomic, strong)UIView *lineView2;



@end
@implementation ReturnsDetailedTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.lineView1];
    [self.contentView addSubview:self.lineView2];
    [self.contentView addSubview:self.leftTitleLabel];
    [self.contentView addSubview:self.leftNumberLabel];
    [self.contentView addSubview:self.rightTitleLabel];
    [self.contentView addSubview:self.rightNumberLabel];
    [self.contentView addSubview:self.EImageV1];
    [self.contentView addSubview:self.EImageV2];
    
    [self.lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
    [self.lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.centerX.equalTo(self.contentView);
        make.width.equalTo(@0.5);
    }];
    [self.leftTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.leftNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.top.equalTo(self.leftTitleLabel.mas_bottom).offset(9);
    }];
    [self.rightTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView.mas_centerX).offset(15);
    }];
    [self.rightNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.rightTitleLabel.mas_bottom).offset(9);
        make.left.equalTo(self.contentView.mas_centerX).offset(15);
    }];
    [self.EImageV1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftTitleLabel);
        make.left.equalTo(self.leftTitleLabel.mas_right).offset(5);
        make.width.height.equalTo(@20);
    }];
    [self.EImageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.rightTitleLabel);
        make.left.equalTo(self.rightTitleLabel.mas_right).offset(5);
        make.width.height.equalTo(@20);
    }];
    
}
- (UIView *)lineView1{
    if (!_lineView1) {
        _lineView1 = [[UIView alloc]init];
        _lineView1.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView1;
}
- (UIView *)lineView2{
    if (!_lineView2) {
        _lineView2 = [[UIView alloc]init];
        _lineView2.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView2;
}

- (UILabel *)leftTitleLabel{
    if (!_leftTitleLabel) {
        _leftTitleLabel = [[UILabel alloc]init];
        _leftTitleLabel.text = @"今日下载量(人)";
        _leftTitleLabel.textColor = RGBA(24, 24, 24, 1);
        _leftTitleLabel.font = kFont_Medium(15);
        _leftTitleLabel.textAlignment = 0;
    }
    return _leftTitleLabel;
}
- (UILabel *)leftNumberLabel{
    if (!_leftNumberLabel) {
        _leftNumberLabel = [[UILabel alloc]init];
        _leftNumberLabel.text = @"0";
        _leftNumberLabel.textColor = RGBA(52, 120, 245, 1);
        _leftNumberLabel.textAlignment = 0;
        _leftNumberLabel.font = kFont_Medium(13);
    }
    return _leftNumberLabel;
}

- (UILabel *)rightTitleLabel{
    if (!_rightTitleLabel) {
        _rightTitleLabel = [[UILabel alloc]init];
        _rightTitleLabel.text = @"总下载量(人)";
        _rightTitleLabel.textColor = RGBA(24, 24, 24, 1);
        _rightTitleLabel.font = kFont_Medium(15);
        _rightTitleLabel.textAlignment = 0;
    }
    return _rightTitleLabel;
}
- (UILabel *)rightNumberLabel{
    if (!_rightNumberLabel) {
        _rightNumberLabel = [[UILabel alloc]init];
        _rightNumberLabel.text = @"0";
        _rightNumberLabel.textColor = RGBA(52, 120, 245, 1);
        _rightNumberLabel.textAlignment = 0;
        _rightNumberLabel.font = kFont_Medium(13);
    }
    return _rightNumberLabel;
}

- (UIImageView *)EImageV1{
    if (!_EImageV1) {
        _EImageV1 = [[UIImageView alloc]init];
        _EImageV1.image = [UIImage imageNamed:@"钱包-钱"];
        _EImageV1.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _EImageV1;
}
- (UIImageView *)EImageV2{
    if (!_EImageV2) {
        _EImageV2 = [[UIImageView alloc]init];
        _EImageV2.image = [UIImage imageNamed:@"钱包-钱"];
        _EImageV2.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _EImageV2;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
