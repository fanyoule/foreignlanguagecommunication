//
//  TotalRevenueTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TotalRevenueTableViewCell : UITableViewCell
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 明细按钮*/
@property (nonatomic, strong)UIButton *detailBtn;
/** 数量*/
@property (nonatomic, strong)UILabel *numberLabel;
@end

NS_ASSUME_NONNULL_END
