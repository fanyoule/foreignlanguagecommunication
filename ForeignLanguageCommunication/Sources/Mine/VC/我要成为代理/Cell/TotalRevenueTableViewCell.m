//
//  TotalRevenueTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "TotalRevenueTableViewCell.h"
@interface TotalRevenueTableViewCell ()
@property (nonatomic ,strong)UIImageView *imageV;

@end
@implementation TotalRevenueTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.detailBtn];
    [self.contentView addSubview:self.numberLabel];
    [self.contentView addSubview:self.imageV];
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.contentView).offset(-20);
        make.width.height.equalTo(@50);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_centerY).offset(0);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.left.equalTo(self.titleLabel.mas_right);
        make.width.height.equalTo(@20);
        
    }];
    self.detailBtn.hidden = YES;
    
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"总收益";
        _titleLabel.textColor = RGBA(153, 153, 153, 1);
        _titleLabel.font = kFont_Medium(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UIImageView *)imageV{
    if (!_imageV) {
        _imageV = [[UIImageView alloc]init];
        _imageV.image = [UIImage imageNamed:@"钱包-钱"];
    }
    return _imageV;
}

- (UIButton *)detailBtn{
    if (!_detailBtn) {
        _detailBtn = [[UIButton alloc]init];
        [_detailBtn setTitle:@"明细" forState:(UIControlStateNormal)];
        [_detailBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _detailBtn.titleLabel.font = kFont_Medium(14);
    }
    return _detailBtn;
}
- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.text = @"+9,000";
        _numberLabel.textColor = RGBA(202, 88, 86, 1);
        _numberLabel.font = kFont_Bold(24);
        _numberLabel.textAlignment = 0;
    }
    return _numberLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
