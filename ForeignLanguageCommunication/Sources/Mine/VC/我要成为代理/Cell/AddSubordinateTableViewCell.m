//
//  AddSubordinateTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "AddSubordinateTableViewCell.h"

@implementation AddSubordinateTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.chooseBtn];
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.chooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.chooseBtn.mas_right).offset(15);
        make.width.height.equalTo(@44);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@0.5);
        make.left.equalTo(self.portraitImageV);
        make.right.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
    }];
}

-(void)setModel:(TheSubordinateListModel *)model{
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.avatar]];
    self.nameLabel.text = model.nickname;
    
}

- (UIButton *)chooseBtn{
    if (!_chooseBtn) {
        _chooseBtn = [[UIButton alloc]init];
        [_chooseBtn setImage:[UIImage imageNamed:@"未选"] forState:(UIControlStateNormal)];
        [_chooseBtn setImage:[UIImage imageNamed:@"选中"] forState:(UIControlStateSelected)];
    }
    return _chooseBtn;
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像2"];
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Medium(15);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
