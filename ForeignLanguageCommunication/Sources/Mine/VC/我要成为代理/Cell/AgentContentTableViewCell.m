//
//  AgentContentTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "AgentContentTableViewCell.h"
@interface AgentContentTableViewCell ()

@end
@implementation AgentContentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.inviteCodeLabel];
    [self.contentView addSubview:self.titleLabel];
    [self.inviteCodeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.contentView).offset(20);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(-10);
        make.left.equalTo(self.contentView).offset(15);
    }];
}

-(void)setModel:(TheAgentInformationModel *)model{
    _model = model;
    self.inviteCodeLabel.text = model.inviteCode;
    if (model.agentLevel == 1) {
        self.titleLabel.text = @"一级代理";
    }else{
        self.titleLabel.text = @"二级代理";
    }
    
}

- (UILabel *)inviteCodeLabel{
    if (!_inviteCodeLabel) {
        _inviteCodeLabel = [[UILabel alloc]init];
        _inviteCodeLabel.text = @"12322";
        _inviteCodeLabel.textColor = RGBA(52, 120, 245, 1);
        _inviteCodeLabel.font = kFont_Bold(40);
        _inviteCodeLabel.textAlignment = 1;
    }
    return _inviteCodeLabel;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"一级代理";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Bold(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
