//
//  ReturnsDetailedTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReturnsDetailedTableViewCell : UITableViewCell
/** 左边的标题*/
@property (nonatomic, strong)UILabel *leftTitleLabel;
/** 左边的数量*/
@property (nonatomic, strong)UILabel *leftNumberLabel;

/** 右边的标题*/
@property (nonatomic, strong)UILabel *rightTitleLabel;
/** 右边的数量*/
@property (nonatomic, strong)UILabel *rightNumberLabel;
/** E币图片*/
@property (nonatomic, strong)UIImageView *EImageV1;
@property (nonatomic, strong)UIImageView *EImageV2;
@end

NS_ASSUME_NONNULL_END
