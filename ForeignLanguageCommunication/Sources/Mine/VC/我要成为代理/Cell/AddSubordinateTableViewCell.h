//
//  AddSubordinateTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import <UIKit/UIKit.h>
#import "TheSubordinateListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddSubordinateTableViewCell : UITableViewCell
/** 选择按钮*/
@property (nonatomic, strong)UIButton *chooseBtn;
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;


@property (nonatomic, strong)TheSubordinateListModel *model;
@end

NS_ASSUME_NONNULL_END
