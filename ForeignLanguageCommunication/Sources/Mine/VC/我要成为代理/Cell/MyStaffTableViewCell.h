//
//  MyStaffTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import <MGSwipeTableCell/MGSwipeTableCell.h>
#import "TheSubordinateListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyStaffTableViewCell : MGSwipeTableCell

@property (nonatomic, strong)TheSubordinateListModel *model;

@end

NS_ASSUME_NONNULL_END
