//
//  MyStaffTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "MyStaffTableViewCell.h"
@interface MyStaffTableViewCell ()
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/** 钻石*/
@property (nonatomic, strong)UILabel *diamondLabel;

@property (nonatomic, strong)UIImageView *ImageV;
@end
@implementation MyStaffTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.ImageV];
    [self.contentView addSubview:self.diamondLabel];
    
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@44);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    
    [self.ImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
        make.width.height.equalTo(@20);
    }];
    [self.diamondLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.ImageV.mas_left).offset(-3);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@0.5);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView);
    }];
}

-(void)setModel:(TheSubordinateListModel *)model{
    _model = model;
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.avatar]];
    self.nameLabel.text = model.nickname;
    self.diamondLabel.text = [NSString stringWithFormat:@"%ld",model.totalProfit];
    
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像2"];
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Medium(15);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}
- (UILabel *)diamondLabel{
    if (!_diamondLabel) {
        _diamondLabel = [[UILabel alloc]init];
        _diamondLabel.text = @"100";
        _diamondLabel.textColor = RGBA(24, 24, 24, 1);
        _diamondLabel.font = kFont_Medium(15);
        _diamondLabel.textAlignment = 2;
    }
    return _diamondLabel;
}
- (UIImageView *)ImageV{
    if (!_ImageV) {
        _ImageV = [[UIImageView alloc]init];
        _ImageV.image = [UIImage imageNamed:@"钱包-钱"];
        _ImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _ImageV;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
