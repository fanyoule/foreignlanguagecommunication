//
//  FriendInvitationViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//  我的邀请码

#import "FriendInvitationViewController.h"
#import "InviteCodeView.h"
#import "InvitedMembersTableViewCell.h"

#import "InviteCodeModel.h"
@interface FriendInvitationViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign)int paging;
@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation FriendInvitationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"好友邀请";
    self.view.backgroundColor = RGBA(255, 255, 255, 1);
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(255, 255, 255, 1);
    
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    self.paging = 1;
    
    // MARK: 下拉刷新
        self.mainTableView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        
        // MARK: 上拉加载
    self.mainTableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopic)];
    [self getData];
}

// MARK: 下拉刷新回调
-(void)loadNewData{
    self.paging = 1;
    [self.dataArray removeAllObjects];
    [self getData];
}
// MARK: 上拉加载
-(void)loadMoreTopic{
    self.paging++;
    [self getData];
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

-(void)getData{
    kWeakSelf(self)
    [RequestManager inviteLogWithUserId:[[UserInfoManager shared] getUserID] pageNum:self.paging pageSize:10 withSuccess:^(id  _Nullable response) {
        NSLog(@"邀请记录 === %@",response);
        NSArray *array = response[@"data"][@"list"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                InviteCodeModel *model = [InviteCodeModel mj_objectWithKeyValues:array[i]];
                [self.dataArray addObject:model];
            }
        }
        [self.mainTableView reloadData];
        [weakself.mainTableView.mj_header endRefreshing];
        [weakself.mainTableView.mj_footer endRefreshing];
    } withFail:^(NSError * _Nullable error) {
        [weakself.mainTableView.mj_header endRefreshing];
        [weakself.mainTableView.mj_footer endRefreshing];
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
        return self.dataArray.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        return 55;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        InvitedMembersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvitedMembersTableViewCell"];
        if (!cell) {
            cell = [[InvitedMembersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InvitedMembersTableViewCell"];
        }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    InviteCodeModel *model = [[InviteCodeModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
      
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    InviteCodeView *view = [[InviteCodeView alloc]init];
    view.backgroundColor = UIColor.whiteColor;
    view.invitationLabel.text = [NSString stringWithFormat:@"邀请人数:%ld",self.dataArray.count];
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
        return 420;
    
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//
//    UIView *view = UIView.new;
//    view.backgroundColor = UIColor.clearColor;
//
//    if (section == 0) {
//        return nil;
//    }else{
//        return view;
//    }
//
//
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (section == 0) {
//        return 0;
//    }else{
//        return 40;
//    }
//
//}

@end
