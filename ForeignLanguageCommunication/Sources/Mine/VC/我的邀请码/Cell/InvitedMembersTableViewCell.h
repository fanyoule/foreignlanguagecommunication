//
//  InvitedMembersTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import <UIKit/UIKit.h>
#import "InviteCodeModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface InvitedMembersTableViewCell : UITableViewCell
@property (nonatomic, strong)InviteCodeModel *model;
@end

NS_ASSUME_NONNULL_END
