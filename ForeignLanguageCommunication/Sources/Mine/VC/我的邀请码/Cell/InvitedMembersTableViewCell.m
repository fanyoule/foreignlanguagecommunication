//
//  InvitedMembersTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "InvitedMembersTableViewCell.h"
@interface InvitedMembersTableViewCell ()
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/** 时间*/
@property (nonatomic, strong)UILabel *timeLabel;
/** 金币数量*/
@property (nonatomic, strong)UILabel *numberLabel;
/** 金币*/
@property (nonatomic, strong)UILabel *moneyLabel;
@end
@implementation InvitedMembersTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.numberLabel];
    [self.contentView addSubview:self.moneyLabel];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@44);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV).offset(3);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.portraitImageV).offset(-5);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.moneyLabel.mas_left).offset(-3);
    }];
}

- (void)setModel:(InviteCodeModel *)model{
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.avatar]];
    self.nameLabel.text = model.nickname;
    self.timeLabel.text = model.createTime;
    self.numberLabel.text = [NSString stringWithFormat:@"+%ld",model.gold];
    
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像2"];
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.clipsToBounds = YES;
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _portraitImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"番茄小号";
        _nameLabel.textColor = RGBA(34, 34, 34, 1);
        _nameLabel.font = kFont_Medium(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.text = @"2020-05-11";
        _timeLabel.textColor = RGBA(153, 153, 153, 1);
        _timeLabel.font = kFont_Medium(12);
        _timeLabel.textAlignment = 0;
    }
    return _timeLabel;
}
- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.text = @"+300";
        _numberLabel.textColor = RGBA(24, 24, 24, 1);
        _numberLabel.font = kFont_Bold(18);
        _numberLabel.textAlignment = 2;
    }
    return _numberLabel;
}
- (UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel =[[UILabel alloc]init];
        _moneyLabel.text = @"金币";
        _moneyLabel.textColor = RGBA(34, 34, 34, 1);
        _moneyLabel.font = kFont_Medium(12);
        _moneyLabel.textAlignment = 2;
    }
    return _moneyLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
