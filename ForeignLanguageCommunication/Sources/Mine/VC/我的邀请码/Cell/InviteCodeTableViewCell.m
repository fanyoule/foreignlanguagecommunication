//
//  InviteCodeTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "InviteCodeTableViewCell.h"
@interface InviteCodeTableViewCell ()
/** 邀请好友图片*/
@property (nonatomic ,strong)UIImageView *invitationImageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 邀请码*/
@property (nonatomic, strong)UILabel *inviteCodelabel;
/** 复制*/
@property (nonatomic, strong)UIButton *button;
/** 左边的线*/
@property (nonatomic, strong)UIView *leftLine;
/** 右边的线*/
@property (nonatomic, strong)UIView *rightLine;
@end
@implementation InviteCodeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.invitationImageV];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.inviteCodelabel];
    [self.contentView addSubview:self.button];
    [self.contentView addSubview:self.leftLine];
    [self.contentView addSubview:self.rightLine];
    [self.invitationImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.contentView);
        
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.invitationImageV.mas_bottom).offset(20);
    }];
    [self.inviteCodelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(35);
    }];
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.inviteCodelabel.mas_bottom).offset(35);
        make.height.equalTo(@44);
        make.left.equalTo(self.contentView).offset(84);
        make.right.equalTo(self.contentView).offset(-84);
    }];
    [self.leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.titleLabel.mas_left).offset(-17);
        make.left.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.left.equalTo(self.titleLabel.mas_right).offset(17);
        make.right.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
    
    
}

- (UIImageView *)invitationImageV{
    if (!_invitationImageV) {
        _invitationImageV = [[UIImageView alloc]init];
        _invitationImageV.image = [UIImage imageNamed:@"我的-邀请好友"];
        _invitationImageV.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _invitationImageV;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"你的专属邀请码";
        _titleLabel.textColor = RGBA(153, 153, 153, 1);
        _titleLabel.font = kFont_Medium(14);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UILabel *)inviteCodelabel{
    if (!_inviteCodelabel) {
        _inviteCodelabel = [[UILabel alloc]init];
        _inviteCodelabel.text = @"6VG768";
        _inviteCodelabel.textColor = RGBA(52, 120, 245, 1);
        _inviteCodelabel.font = kFont_Bold(40);
        _inviteCodelabel.textAlignment = 1;
    }
    return _inviteCodelabel;
}

- (UIButton *)button{
    if (!_button) {
        _button = [[UIButton alloc]init];
        [_button setTitle:@"复制" forState:(UIControlStateNormal)];
        [_button setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _button.backgroundColor = RGBA(52, 120, 245, 1);
        _button.layer.cornerRadius = 44/2;
        _button.clipsToBounds = YES;
    }
    return _button;
}

- (UIView *)leftLine{
    if (!_leftLine) {
        _leftLine = [[UIView alloc]init];
        _leftLine.backgroundColor = RGBA(204, 204, 204, 1);
    }
    return _leftLine;
}
- (UIView *)rightLine{
    if (!_rightLine) {
        _rightLine = [[UIView alloc]init];
        _rightLine.backgroundColor = RGBA(204, 204, 204, 1);
    }
    return _rightLine;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
