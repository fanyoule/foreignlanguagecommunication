//
//  InviteCodeModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InviteCodeModel : NSObject
/** 是否代理邀请0：否，1：是*/
@property (nonatomic, assign)NSInteger agent;
/** 头像*/
@property (nonatomic, strong)NSString *avatar;
/** 邀请时间*/
@property (nonatomic, strong)NSString *createTime;
/** 金币*/
@property (nonatomic, assign)NSInteger gold;
/** 邀请码*/
@property (nonatomic, strong)NSString *inviteCode;
/** 被邀请用户id*/
@property (nonatomic, assign)NSInteger inviteUserId;
/** 被邀请用户昵称*/
@property (nonatomic, strong)NSString *nickname;
/** 用户id*/
@property (nonatomic, assign)NSInteger userId;



@end

NS_ASSUME_NONNULL_END
