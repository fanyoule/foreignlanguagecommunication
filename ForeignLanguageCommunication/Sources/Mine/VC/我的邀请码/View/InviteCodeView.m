//
//  InviteCodeView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import "InviteCodeView.h"
@interface InviteCodeView ()
/** 邀请好友图片*/
@property (nonatomic ,strong)UIImageView *invitationImageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 邀请码*/
@property (nonatomic, strong)UILabel *inviteCodelabel;
/** 复制*/
@property (nonatomic, strong)UIButton *button;
/** 左边的线*/
@property (nonatomic, strong)UIView *leftLine;
/** 右边的线*/
@property (nonatomic, strong)UIView *rightLine;

/** 分割线*/
@property (nonatomic, strong)UIView *lineView;
@end
@implementation InviteCodeView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.invitationImageV];
    [self addSubview:self.titleLabel];
    [self addSubview:self.inviteCodelabel];
    [self addSubview:self.button];
    [self addSubview:self.leftLine];
    [self addSubview:self.rightLine];
    [self addSubview:self.invitationLabel];
    [self addSubview:self.lineView];
    [self.invitationImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.invitationImageV.mas_bottom).offset(20);
    }];
    [self.inviteCodelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(35);
    }];
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.inviteCodelabel.mas_bottom).offset(35);
        make.height.equalTo(@44);
        make.left.equalTo(self).offset(84);
        make.right.equalTo(self).offset(-84);
    }];
    [self.leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.titleLabel.mas_left).offset(-17);
        make.left.equalTo(self);
        make.height.equalTo(@0.5);
    }];
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.left.equalTo(self.titleLabel.mas_right).offset(17);
        make.right.equalTo(self);
        make.height.equalTo(@0.5);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.button.mas_bottom).offset(24);
        make.left.right.equalTo(self);
        make.height.equalTo(@0.5);
    }];
    [self.invitationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(18);
        make.left.equalTo(self).offset(15);
    }];
    
    self.inviteCodelabel.text = [[UserInfoManager shared] inviteCode];
}

- (UIImageView *)invitationImageV{
    if (!_invitationImageV) {
        _invitationImageV = [[UIImageView alloc]init];
        _invitationImageV.image = [UIImage imageNamed:@"我的-邀请好友"];
        _invitationImageV.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _invitationImageV;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"你的专属邀请码";
        _titleLabel.textColor = RGBA(153, 153, 153, 1);
        _titleLabel.font = kFont_Medium(14);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UILabel *)inviteCodelabel{
    if (!_inviteCodelabel) {
        _inviteCodelabel = [[UILabel alloc]init];
        _inviteCodelabel.text = @"6VG768";
        _inviteCodelabel.textColor = RGBA(52, 120, 245, 1);
        _inviteCodelabel.font = kFont_Bold(40);
        _inviteCodelabel.textAlignment = 1;
    }
    return _inviteCodelabel;
}

- (UIButton *)button{
    if (!_button) {
        _button = [[UIButton alloc]init];
        [_button setTitle:@"复制" forState:(UIControlStateNormal)];
        [_button setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _button.backgroundColor = RGBA(52, 120, 245, 1);
        _button.layer.cornerRadius = 44/2;
        _button.clipsToBounds = YES;
        [_button addTarget:self action:@selector(hitCopy) forControlEvents:(UIControlEventTouchDown)];
    }
    return _button;
}

- (UIView *)leftLine{
    if (!_leftLine) {
        _leftLine = [[UIView alloc]init];
        _leftLine.backgroundColor = RGBA(204, 204, 204, 1);
    }
    return _leftLine;
}
- (UIView *)rightLine{
    if (!_rightLine) {
        _rightLine = [[UIView alloc]init];
        _rightLine.backgroundColor = RGBA(204, 204, 204, 1);
    }
    return _rightLine;
}
- (UILabel *)invitationLabel{
    if (!_invitationLabel) {
        _invitationLabel = [[UILabel alloc]init];
        _invitationLabel.text = @"邀请人数：2";
        _invitationLabel.textColor = RGBA(34, 34, 34, 1);
        _invitationLabel.font = kFont_Medium(16);
        _invitationLabel.textAlignment = 0;
    }
    return _invitationLabel;
}
-(UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}


-(void)hitCopy{
    NSLog(@"点击了复制");
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.inviteCodelabel.text;
//    pab.string = self.inviteCodelabel.text;
    [[self hx_viewController] showText:@"复制成功"];
}

@end
