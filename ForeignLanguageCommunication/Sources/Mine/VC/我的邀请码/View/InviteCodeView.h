//
//  InviteCodeView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InviteCodeView : UIView
/** 邀请人数*/
@property (nonatomic, strong)UILabel *invitationLabel;
@end

NS_ASSUME_NONNULL_END
