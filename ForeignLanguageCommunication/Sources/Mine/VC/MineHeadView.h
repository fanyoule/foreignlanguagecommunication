//
//  MineHeadView.h
//  VoiceLive
//
//  Created by mac on 2020/8/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyPageModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^goToInfoBock)(void);
//typedef void(^goSetUpBock)(void);
typedef void(^goToNumberInfoBlock)(NSInteger index);
@interface MineHeadView : UIView

@property (nonatomic, copy) goToInfoBock gotoInfoBlock;
//@property (nonatomic, copy) goSetUpBock goSetUpBock;
@property (nonatomic, copy) goToNumberInfoBlock goToNumberInfoBlock;

//需要一个数据模型
#pragma mark-TODO DataModel-
/** model */
@property (nonatomic, strong) MyPageModel *model;
@end

NS_ASSUME_NONNULL_END
