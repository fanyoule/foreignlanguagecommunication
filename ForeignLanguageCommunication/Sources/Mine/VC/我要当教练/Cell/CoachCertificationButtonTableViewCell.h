//
//  CoachCertificationButtonTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoachCertificationButtonTableViewCell : UITableViewCell
/** 勾选须知*/
@property (nonatomic, strong)UIButton *chooseBtn;
/** 须知*/
@property (nonatomic, strong)UILabel *instructionsLabel;
/** 须知内容*/
@property (nonatomic, strong)UIButton *contentBtn;
/** 认证按钮*/
@property (nonatomic, strong)UIButton *certificationBtn;
@end

NS_ASSUME_NONNULL_END
