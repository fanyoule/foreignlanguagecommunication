//
//  CoachCertificationButtonTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//

#import "CoachCertificationButtonTableViewCell.h"

@implementation CoachCertificationButtonTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.chooseBtn];
    [self.contentView addSubview:self.instructionsLabel];
    [self.contentView addSubview:self.contentBtn];
    [self.contentView addSubview:self.certificationBtn];
    [self.certificationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(-20).priority(600);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@50);
    }];
    [self.instructionsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(25);
        make.right.equalTo(self.contentView.mas_centerX).offset(-20);
        make.bottom.equalTo(self.certificationBtn.mas_top).offset(-10);
    }];
    [self.chooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.instructionsLabel);
        make.right.equalTo(self.instructionsLabel.mas_left).offset(-5);
    }];
    [self.contentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.instructionsLabel);
        make.left.equalTo(self.instructionsLabel.mas_right).offset(5);
    }];
}

- (UIButton *)chooseBtn{
    if (!_chooseBtn) {
        _chooseBtn = [[UIButton alloc]init];
        [_chooseBtn setImage:[UIImage imageNamed:@"认证-未勾选"] forState:(UIControlStateNormal)];
        [_chooseBtn setImage:[UIImage imageNamed:@"认证-勾选"] forState:(UIControlStateSelected)];
        
    }
    return _chooseBtn;
}
- (UILabel *)instructionsLabel{
    if (!_instructionsLabel) {
        _instructionsLabel = [[UILabel alloc]init];
        _instructionsLabel.text = @"同意";
        _instructionsLabel.textColor = RGBA(24, 24, 24, 1);
        _instructionsLabel.font = kFont_Medium(13);
        _instructionsLabel.textAlignment = 1;
    }
    return _instructionsLabel;
}
- (UIButton *)contentBtn{
    if (!_contentBtn) {
        _contentBtn = [[UIButton alloc]init];
        [_contentBtn setTitle:@"《须知》" forState:(UIControlStateNormal)];
        [_contentBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _contentBtn.titleLabel.font = kFont_Medium(13);
        
    }
    return _contentBtn;
}
- (UIButton *)certificationBtn{
    if (!_certificationBtn) {
        _certificationBtn = [[UIButton alloc]init];
        _certificationBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _certificationBtn.layer.cornerRadius = 12;
        _certificationBtn.clipsToBounds = YES;
        [_certificationBtn setTitle:@"认证" forState:(UIControlStateNormal)];
        _certificationBtn.titleLabel.font = kFont_Bold(15);
        [_certificationBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
    }
    return _certificationBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
