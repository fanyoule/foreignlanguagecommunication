//
//  PersonalProfileTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "PersonalProfileTableViewCell.h"
@interface PersonalProfileTableViewCell ()

@end
@implementation PersonalProfileTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.profileLabel];
    [self.contentView addSubview:self.textView];
    [self.textView addSubview:self.promptLabel];
//    [self.contentView addSubview:self.limitLabel];
    
    [self.profileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView).offset(15);
    }];
    
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.profileLabel.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-5).priority(600);
        make.height.equalTo(@140);
    }];
    
    [self.promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView).offset(8);
        make.left.equalTo(self.textView).offset(5);
    }];
    
//    [self.limitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.textView.mas_bottom).offset(5);
//        make.right.equalTo(self.contentView).offset(-15);
//        make.bottom.equalTo(self.contentView).offset(-10);
//    }];
    
    
}
- (UILabel *)profileLabel{
    if (!_profileLabel) {
        _profileLabel = [[UILabel alloc]init];
        _profileLabel.text = @"个人简介";
        _profileLabel.textColor = RGBA(24, 24, 24, 1);
        _profileLabel.font = kFont_Medium(16);
        _profileLabel.textAlignment = 0;
    }
    return _profileLabel;
}

- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.font = kFont_Medium(14);
        
    }
    return _textView;
}
- (UILabel *)promptLabel{
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc]init];
        _promptLabel.text = @"";
        _promptLabel.textColor = RGBA(153, 153, 153, 1);
        _promptLabel.font = kFont_Medium(14);
        _promptLabel.textAlignment = 0;
    }
    return _promptLabel;
}

- (UILabel *)limitLabel{
    if (!_limitLabel) {
        _limitLabel =[[ UILabel alloc]init];
        _limitLabel.text = @"仅限300字";
        _limitLabel.textColor = RGBA(153, 153, 153, 1);
        _limitLabel.font = kFont(11);
        _limitLabel.textAlignment = 2;
    }
    return _limitLabel;
}







- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
