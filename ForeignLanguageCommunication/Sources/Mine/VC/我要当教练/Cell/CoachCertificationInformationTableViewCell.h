//
//  CoachCertificationInformationTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoachCertificationInformationTableViewCell : UITableViewCell

/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 内容*/
@property (nonatomic, strong)UITextField *textField;
/** 线*/
@property (nonatomic, strong)UIView *lineView;

@end

NS_ASSUME_NONNULL_END
