//
//  PersonalProfileTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import <UIKit/UIKit.h>
//typedef void(^introductionToTheBlock)(NSString *string);
NS_ASSUME_NONNULL_BEGIN

@interface PersonalProfileTableViewCell : UITableViewCell
/** 个人简介*/
@property (nonatomic, strong)UILabel *profileLabel;
/** 内容*/
@property (nonatomic, strong)UITextView *textView;
/** 提示*/
@property (nonatomic, strong)UILabel *promptLabel;
/** 限制提示*/
@property (nonatomic, strong)UILabel *limitLabel;


//@property (nonatomic, strong)introductionToTheBlock introductionToThe;
@end

NS_ASSUME_NONNULL_END
