//
//  CoachCertificationViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//  教练认证

#import "CoachCertificationViewController.h"
#import "TheNewCurriculumTableViewCell.h"

#import "CoachCertificationButtonTableViewCell.h"
#import "PersonalProfileTableViewCell.h"

#import "CoachCertificationButtonTableViewCell.h"
#import "NewCourseSelectionTableViewCell.h"

#import "MineDataPickerModel.h"

#import "MineDataPickerViewController.h"// 弹窗
#import "ServiceAgreementViewController.h"// 用户协议
@interface CoachCertificationViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>

/** 判断姓名是否可输入 2: 可输入 ， 1：不可输入*/
@property (nonatomic, assign)NSInteger isJudge;

@property (nonatomic, strong)InstructorVerificationPriorCertificationModel *certificationModel;

/** 选中的语种id*/
@property (nonatomic, strong)NSString *languagesID;
/** 选中的教学内容id*/
@property (nonatomic, strong)NSString *teachingID;
/** 选中的适应人群id*/
@property (nonatomic, strong)NSString *crowdID;
/** 个人简介*/
@property (nonatomic, strong)NSString *introductionStr;
/** 手机号*/
@property (nonatomic, strong)NSString *mobilePhoneStr;

/** 姓名*/
@property (nonatomic, strong)NSString *nameStr;
/** 性别*/
@property (nonatomic, strong)NSString *sexStr;
@end

@implementation CoachCertificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"教练认证";
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    [self getData];
    [self setUpForDismissKeyboard];
}
// MARK: 教练认证前验证
-(void)getData{
    
    [RequestManager coachVerifyWithId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"是否是教练 === %@",response);
        if ([response[@"status"] integerValue] == 200) {
            NSDictionary *dic = response[@"data"];
        
            if (response[@"data"] == nil) {
                self.isJudge = 2;
            }else{
                self.isJudge = 1;
                self.certificationModel = [InstructorVerificationPriorCertificationModel mj_objectWithKeyValues:dic];
                NSLog(@"性别 === %@",response[@"data"][@"sex"]);
            }
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
- (InstructorVerificationPriorCertificationModel *)certificationModel{
    if (!_certificationModel) {
        _certificationModel = [[InstructorVerificationPriorCertificationModel alloc]init];
    }
    return _certificationModel;
}


#pragma mark - 回收任何空白区域键盘事件
- (void)setUpForDismissKeyboard {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    UITapGestureRecognizer *singleTapGR =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tapAnywhereToDismissKeyboard:)];
    NSOperationQueue *mainQuene =[NSOperationQueue mainQueue];
    [nc addObserverForName:UIKeyboardWillShowNotification
                    object:nil
                     queue:mainQuene
                usingBlock:^(NSNotification *note){
                    [self.view addGestureRecognizer:singleTapGR];
                }];
    [nc addObserverForName:UIKeyboardWillHideNotification
                    object:nil
                     queue:mainQuene
                usingBlock:^(NSNotification *note){
                    [self.view removeGestureRecognizer:singleTapGR];
                }];
}
 
- (void)tapAnywhereToDismissKeyboard:(UIGestureRecognizer *)gestureRecognizer {
    //此method会将self.view里所有的subview的first responder都resign掉
    [self.view endEditing:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 3;
    }else if (section == 1){
        return 3;
    }else{
        return 2;
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            NewCourseSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewCourseSelectionTableViewCell"];
            if (!cell) {
                cell = [[NewCourseSelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NewCourseSelectionTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
            
            cell.titleImageV.image = [UIImage imageNamed:@"代理 - 性别"];
            
            if (self.certificationModel.sex == nil) {
                cell.label.text = @"请选择性别";
                cell.label.textColor = RGBA(196, 196, 196, 1);
                [cell whenTapped:^{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"选择性别" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                        UIAlertAction *maleAction = [UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            //选择男
                            NSLog(@"男");
                            cell.label.text = @"男";
                            cell.label.textColor = RGBA(24, 24, 24, 1);
                            self.sexStr = @"1";
                        }];
                        UIAlertAction *femaleAction = [UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                            cell.label.text = @"女";
                            cell.label.textColor = RGBA(24, 24, 24, 1);
                            self.sexStr = @"0";
                            NSLog(@"女");
                        }];
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
                        [alert addAction:maleAction];
                        [alert addAction:femaleAction];
                        [alert addAction:cancelAction];
                        [self presentViewController:alert animated:YES completion:nil];
                }];
                
                
            }else{
                if ([self.certificationModel.sex isEqualToString:@"0"]) {
                    cell.label.text = @"女";
                }else{
                    cell.label.text = @"男";
                }
                cell.label.textColor = RGBA(24, 24, 24, 1);
                
                [cell whenTapped:^{
                    
                }];
            }
            
            
        return cell;
        }
        kWeakSelf(self)
        TheNewCurriculumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheNewCurriculumTableViewCell"];
            if (!cell) {
                cell = [[TheNewCurriculumTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheNewCurriculumTableViewCell"];
            }
            cell.backgroundColor = [UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.isJudge = 1;
        if (indexPath.row == 0) {
            cell.titleImageV.image = [UIImage imageNamed:@"代理 - 姓名"];
            cell.textField.placeholder = @"请填写真实姓名,不可更改";
            if (self.isJudge == 1) {
                cell.textField.enabled = NO;
                cell.textField.text = self.certificationModel.name;
            }else{
                cell.textField.enabled = YES;
                cell.mobilePhone = ^(NSString *string) {
                    weakself.nameStr = string;
                };
            }
        }else if (indexPath.row == 2){
            cell.titleImageV.image = [UIImage imageNamed:@"代理 - 手机号"];
            cell.textField.placeholder = @"请填写手机号";
            cell.textField.text = self.certificationModel.phone;
            cell.mobilePhone = ^(NSString *string) {
                weakself.mobilePhoneStr = string;
                
            };
        }
        return cell;
    }else if (indexPath.section == 1){
           
        kWeakSelf(self)
            NewCourseSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewCourseSelectionTableViewCell"];
            if (!cell) {
                cell = [[NewCourseSelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NewCourseSelectionTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.row == 0) {
            if (self.certificationModel.languageList != nil && ![self.certificationModel.languageList isKindOfClass:[NSNull class]] && (self.certificationModel.languageList.count != 0)) {
                
                NSMutableArray *array = [NSMutableArray array];
                for (int i = 0; i < self.certificationModel.languageList.count; i++) {
                    MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:self.certificationModel.languageList[i]];
                    [array addObject:model.name];
                        
                }
                NSString *string = [array componentsJoinedByString:@"、"];
                if (string != nil) {
                    cell.label.text = string;
                    cell.label.textColor = RGBA(24, 24, 24, 1);

                }else{
                    cell.label.text = @"请填写你的语种";
                    cell.label.textColor = RGBA(196, 196, 196, 1);
                }
            }else{
                cell.label.text = @"请填写你的语种";
                cell.label.textColor = RGBA(196, 196, 196, 1);
            }
            
            
            cell.titleImageV.image = [UIImage imageNamed:@"认证-语种"];
            [cell whenTapped:^{
                MineDataPickerViewController *VC = [MineDataPickerViewController new];
                VC.isJudge = 3;
                VC.showSingle = NO;
                VC.needShowColor = NO;
                VC.titleString = @"语种";
                HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                popController.backgroundAlpha = 0.1;//背景色透明度
                popController.animationDuration = 0.3;
                popController.popPosition = 2;
                popController.popType = 5;
                popController.dismissType = 5;
                popController.shouldDismissOnBackgroundTouch = YES;
                [popController presentInViewController:self];
                VC.confirmBlock = ^(NSArray *selectModelArray) {
                    NSMutableArray *arrayID = [NSMutableArray array];
                    NSMutableArray *array = [NSMutableArray array];
                    for (int i = 0; i < selectModelArray.count; i++) {
                        MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:selectModelArray[i]];
                        [array addObject:model.name];
                        [arrayID addObject:@(model.ID)];
                    }
                    weakself.languagesID = [arrayID componentsJoinedByString:@","];
                    cell.label.text = [array componentsJoinedByString:@"、"];
                    cell.label.textColor = RGBA(24, 24, 24, 1);

                };
            }];
            
            
            
        }else if (indexPath.row == 1){
            if (self.certificationModel.teachList != nil && ![self.certificationModel.teachList isKindOfClass:[NSNull class]] && (self.certificationModel.teachList.count != 0)) {
                NSMutableArray *array = [NSMutableArray array];
                for (int i = 0; i < self.certificationModel.teachList.count; i++) {
                    MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:self.certificationModel.teachList[i]];
                    [array addObject:model.name];
                    
                }
                NSString *string = [array componentsJoinedByString:@"、"];
                if (string != nil) {
                    cell.label.text = string;
                    cell.label.textColor = RGBA(24, 24, 24, 1);
                }else{
                    cell.label.text = @"请填写教学内容,可多选";
                    cell.label.textColor = RGBA(196, 196, 196, 1);
                }
            }else{
                cell.label.text = @"请填写教学内容,可多选";
                cell.label.textColor = RGBA(196, 196, 196, 1);
            }
            
            cell.titleImageV.image = [UIImage imageNamed:@"认证-教学内容"];
            
            [cell whenTapped:^{
                MineDataPickerViewController *VC = [MineDataPickerViewController new];
                VC.isJudge = 4;
                VC.showSingle = NO;
                VC.needShowColor = NO;
                VC.titleString = @"教学内容";
                HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                popController.backgroundAlpha = 0.1;//背景色透明度
                popController.animationDuration = 0.3;
                popController.popPosition = 2;
                popController.popType = 5;
                popController.dismissType = 5;
                popController.shouldDismissOnBackgroundTouch = YES;
                [popController presentInViewController:self];
                VC.confirmBlock = ^(NSArray *selectModelArray) {
                    NSMutableArray *arrayID = [NSMutableArray array];
                    NSMutableArray *array = [NSMutableArray array];
                    for (int i = 0; i < selectModelArray.count; i++) {
                        MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:selectModelArray[i]];
                        [array addObject:model.name];
                        [arrayID addObject:@(model.ID)];
                    }
                    weakself.teachingID = [arrayID componentsJoinedByString:@","];
                    cell.label.text = [array componentsJoinedByString:@"、"];
                    cell.label.textColor = RGBA(24, 24, 24, 1);

                };
            }];
            
        }else if (indexPath.row == 2){
            if (self.certificationModel.applyList != nil && ![self.certificationModel.applyList isKindOfClass:[NSNull class]] && (self.certificationModel.applyList.count != 0)) {
                NSMutableArray *array = [NSMutableArray array];
                for (int i = 0; i < self.certificationModel.applyList.count; i++) {
                    MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:self.certificationModel.applyList[i]];
                    [array addObject:model.name];
                }
                NSString *string = [array componentsJoinedByString:@"、"];
                if (string != nil) {
                    cell.label.text = [array componentsJoinedByString:@"、"];
                    cell.label.textColor = RGBA(24, 24, 24, 1);
                }else{
                    cell.label.text = @"请填写适应人群,可多选";
                    cell.label.textColor = RGBA(196, 196, 196, 1);
                }
            }else{
                cell.label.text = @"请填写适应人群,可多选";
                cell.label.textColor = RGBA(196, 196, 196, 1);
            }
            
            cell.titleImageV.image = [UIImage imageNamed:@"认证-适应人群"];
            
            [cell whenTapped:^{
                MineDataPickerViewController *VC = [MineDataPickerViewController new];
                VC.isJudge = 5;
                VC.showSingle = NO;
                VC.needShowColor = NO;
                VC.titleString = @"适应人群";
                HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                popController.backgroundAlpha = 0.1;//背景色透明度
                popController.animationDuration = 0.3;
                popController.popPosition = 2;
                popController.popType = 5;
                popController.dismissType = 5;
                popController.shouldDismissOnBackgroundTouch = YES;
                [popController presentInViewController:self];
                VC.confirmBlock = ^(NSArray *selectModelArray) {
                    NSMutableArray *arrayID = [NSMutableArray array];
                    NSMutableArray *array = [NSMutableArray array];
                    for (int i = 0; i < selectModelArray.count; i++) {
                        MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:selectModelArray[i]];
                        [array addObject:model.name];
                        [arrayID addObject:@(model.ID)];
                    }
                    weakself.crowdID = [arrayID componentsJoinedByString:@","];
                    cell.label.text = [array componentsJoinedByString:@"、"];
                    cell.label.textColor = RGBA(24, 24, 24, 1);

                };
            }];
        }
        return cell;
    }else{
        if (indexPath.row == 0) {
            PersonalProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalProfileTableViewCell"];
            if (!cell) {
                cell = [[PersonalProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonalProfileTableViewCell"];
            }
            if (self.certificationModel.content != nil) {
                cell.textView.text = self.certificationModel.content;
                
                cell.promptLabel.hidden = YES;
            }else{
                cell.promptLabel.text = @"仅限300字";
            }
            
            cell.textView.delegate = self;
            cell.promptLabel.tag = 800;
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
        }
        CoachCertificationButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoachCertificationButtonTableViewCell"];
        if (!cell) {
            cell = [[CoachCertificationButtonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CoachCertificationButtonTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor clearColor];
        if (self.isJudge == 2) {
            [cell.certificationBtn setTitle:@"认证" forState:(UIControlStateNormal)];
            [cell.certificationBtn addTarget:self action:@selector(clickCertification) forControlEvents:(UIControlEventTouchDown)];
        }else{
            [cell.certificationBtn setTitle:@"确认修改" forState:(UIControlStateNormal)];
            [cell.certificationBtn addTarget:self action:@selector(clickModify) forControlEvents:(UIControlEventTouchDown)];
        }
        self.certificationModel.selected = cell.chooseBtn.selected;
        [cell.chooseBtn whenTapped:^{
            cell.chooseBtn.selected = !cell.chooseBtn.isSelected;
            self.certificationModel.selected = cell.chooseBtn.selected;
        }];
        [cell.contentBtn whenTapped:^{
            ServiceAgreementViewController *vc = [[ServiceAgreementViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
    
    return cell;
        
        
    }
    
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *view = UIView.new;
    view.backgroundColor = UIColor.clearColor;
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 10;
    
}

- (void)textViewDidChange:(UITextView *)textView{
    UILabel *label = (UILabel *)[self.view viewWithTag:800];
    label.hidden = YES;
    
    self.introductionStr = textView.text;
    //字数限制操作

    if (textView.text.length >= 300) {
    
        textView.text = [textView.text substringToIndex:10];
        
    }
    
    //取消按钮点击权限，并显示提示文字

    if (textView.text.length <= 0) {
        
        label.hidden = NO;
    
    }

}

// MARK: 修改
-(void)clickModify{
    NSLog(@"修改");
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(self.certificationModel.userId) forKey:@"userId"];
    if (self.mobilePhoneStr != nil) {
        [dic setObject:self.mobilePhoneStr forKey:@"phone"];
    }else{
        
    }
    if (self.introductionStr != nil) {
        [dic setObject:self.introductionStr forKey:@"content"];
    }else{
        
    }
    if (self.crowdID != nil) {
        [dic setObject:self.crowdID forKey:@"applyCrowd"];
    }else{
        
    }
    if (self.teachingID != nil) {
        NSLog(@"教练内容  ====  %@",self.teachingID);
        [dic setObject:self.teachingID forKey:@"teachingContent"];
    }else{
        
    }
    if (self.languagesID != nil) {
        [dic setObject:self.languagesID forKey:@"languages"];
    }else{
        
    }
    if (self.certificationModel.selected == NO) {
        [self showText:@"请勾选须知"];
    }else{
        [RequestManager updateCoachWithMutableDictionary:dic withSuccess:^(id  _Nullable response) {
            NSLog(@"修改 === %@",response);
            if ([response[@"status"] integerValue] == 200) {
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
    
    
}
///** 选中的语种id*/
//@property (nonatomic, strong)NSString *languagesID;
///** 选中的教学内容id*/
//@property (nonatomic, strong)NSString *teachingID;
///** 选中的适应人群id*/
//@property (nonatomic, strong)NSString *crowdID;
///** 个人简介*/
//@property (nonatomic, strong)NSString *introductionStr;
// mobilePhoneStr

// MARK: 认证
-(void)clickCertification{
    NSLog(@"认证");
    if (self.nameStr == nil){
        [self showText:@"请输入真实姓名"];
    }else if (self.sexStr == nil){
        [self showText:@"请选择性别"];
    }else if (self.mobilePhoneStr == nil){
        [self showText:@"请输入手机号"];
    }else if (self.languagesID == nil) {
        [self showText:@"请选择语种"];
    }else if (self.teachingID == nil){
        [self showText:@"请选择教学内容"];
    }else if (self.crowdID == nil){
        [self showText:@"请选择适应人群"];
    }else if (self.introductionStr == nil){
        [self showText:@"请输入个人简介"];
    }else if (self.certificationModel.selected == NO) {
        [self showText:@"请勾选须知"];
    }else{
        [RequestManager addCoachWithId:[[UserInfoManager shared] getUserID] applyCrowd:self.crowdID content:self.introductionStr languages:self.languagesID name:self.nameStr phone:self.mobilePhoneStr sex:self.sexStr teachingContent:self.teachingID withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self.navigationController popViewControllerAnimated:YES];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshMyPageForDataNotifications" object:nil];
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
    
    
}

@end
