//
//  MyWorkViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "BaseViewController.h"
#import "ThePersonalDataModel.h"
#import "CoachDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyWorkViewController : BaseViewController
/** 判断 1：我的作品  2：她/他的作品*/
@property (nonatomic, assign)NSInteger isJudge;

/** */
@property (nonatomic, strong)ThePersonalDataModel *dataModel;

/** 教练详情model*/
@property (nonatomic, strong)CoachDetailsModel *detailsModel;
@end

NS_ASSUME_NONNULL_END
