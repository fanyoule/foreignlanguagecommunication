//
//  MyWorkViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "MyWorkViewController.h"
#import "PersonalInformationWorksCollectionViewCell.h"
#import "MyWorkModel.h"
#import "WorldDetailViewController.h"// 浏览作品

#import "WorldEventsDetailsModel.h"
@interface MyWorkViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong)UICollectionView *collectionView;
/** 选中的数组*/
@property (nonatomic, strong)NSMutableArray *selectedArray;
/** 数据源*/
@property (nonatomic, strong)NSMutableArray *dataArray;

/** 分页*/
@property (nonatomic, assign)int paging;


/** 是否显示播放量 1:不显示  其他显示*/
@property (nonatomic, assign)NSInteger according;
@end

@implementation MyWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    
    self.paging = 1;
    [self getData];
    self.collectionView.mj_footer.hidden = YES;
    
}
-(void)creatUI{
    UIView *lineView = [[UIView alloc]init];
    self.dataArray = [NSMutableArray array];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.navView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.navView);
        make.height.equalTo(@0.5);
    }];
    if (self.isJudge == 1) {
        self.navTitleString = @"我的作品";
        [self addRightBtnWith:@"选择"];
        [self.rightBtn setTitle:@"删除" forState:(UIControlStateSelected)];
        [self.rightBtn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateNormal)];
        [self.rightBtn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateSelected)];
    }else if (self.isJudge == 2) {
        if (self.dataModel.isMe) {
            self.navTitleString = @"我的作品";
        }else{
            self.navTitleString = @"他/她的作品";
        }
    }
    self.navView.backgroundColor = UIColor.whiteColor;
    
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    [self.collectionView registerClass:[PersonalInformationWorksCollectionViewCell class] forCellWithReuseIdentifier:@"PersonalInformationWorksCollectionViewCell"];

    // MARK: 下拉刷新
    self.collectionView.mj_header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    // MARK: 上拉加载
    self.collectionView.mj_footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopic)];
    
    [self.collectionView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.collectionView);
    }];
}


// MARK: 下拉刷新回调
-(void)loadNewData{
    self.paging = 1;
    [self getData];
}
// MARK: 上拉加载
-(void)loadMoreTopic{
    [self getData];
}


- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = ColorRGBA(255, 255, 255, 1);
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.allowsMultipleSelection = YES;
    }
    return _collectionView;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
// MARK: 解析教练作品
- (void)setDetailsModel:(CoachDetailsModel *)detailsModel{
    _detailsModel = detailsModel;
//    for (int i = 0; i < detailsModel.works.count; i++) {
//        MyCollectionWorksModel *model = [MyCollectionWorksModel mj_objectWithKeyValues:detailsModel.works[i]];
//
//            [self.dataArray addObject:model];
//
//    }
//    self.according = 1;
//    [self.collectionView reloadData];
}

-(void)setDataModel:(ThePersonalDataModel *)dataModel{
    _dataModel = dataModel;
//    self.according = 1;
//    for (int i = 0; i < dataModel.publishs.count; i++) {
//        MyCollectionWorksModel *model = [MyCollectionWorksModel mj_objectWithKeyValues:dataModel.publishs[i]];
//
//            [self.dataArray addObject:model];
//
//    }
//    [self.collectionView reloadData];
}
// MARK: 我的作品
-(void)getData{
    kWeakSelf(self)
    long userId = 0;
    if (self.isJudge == 1) {
        userId = [[UserInfoManager shared] getUserID];
    } else {
        if (self.dataModel) {
            userId = self.dataModel.ID;
        } else {
            userId = self.detailsModel.userId;
        }
    }
    [RequestManager getMyPublishListWithUserId:userId pageNum:self.paging pageSize:20 withSuccess:^(id  _Nullable response) {
        int lastPage = [response[@"data"][@"lastPage"] intValue];
        NSArray *tempArr = [MyCollectionWorksModel mj_objectArrayWithKeyValuesArray:response[@"data"][@"list"]];
        if (weakself.paging == 1) {
            [weakself.dataArray removeAllObjects];
        }
        [weakself stopLoading:!(weakself.paging < lastPage)];
        weakself.paging++;
        [weakself.dataArray addObjectsFromArray:tempArr];
        if (weakself.dataArray.count < 20) {
            weakself.collectionView.mj_footer.hidden = YES;
        } else {
            weakself.collectionView.mj_footer.hidden = NO;
        }
        weakself.nullView.hidden = weakself.dataArray.count;
        [weakself.collectionView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself stopLoading:NO];
    }];
}
- (void)stopLoading:(BOOL)isNoData {
    if ([self.collectionView.mj_header isRefreshing]) {
        [self.collectionView.mj_header endRefreshing];
        if (!isNoData) {
            self.collectionView.mj_footer.state = MJRefreshStateIdle;
        }
    }
    if ([self.collectionView.mj_footer isRefreshing]) {
        if (isNoData) {
            [self.collectionView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.collectionView.mj_footer endRefreshing];
        }
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PersonalInformationWorksCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PersonalInformationWorksCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    
    cell.shouldEdit = self.rightBtn.isSelected;
    MyCollectionWorksModel *Worksmodel = [[MyCollectionWorksModel alloc]init];
    Worksmodel = self.dataArray[indexPath.row];
    cell.workmodel = Worksmodel;
    
    MyWorkModel *model = [[MyWorkModel alloc]init];
    
    [cell.selector whenTapped:^{
        cell.selector.selected = !cell.selector.isSelected;
        model.shouldDelete = cell.selector.isSelected;
//        NSLog(@"选中的 ===== %@",model.pid);
        if (cell.selector.selected) {
            [self.selectedArray addObject:@(Worksmodel.ID)];
        }else{
            [self.selectedArray removeObject:@(Worksmodel.ID)];
        }
        
        if (self.selectedArray != nil && ![self.selectedArray isKindOfClass:[NSNull class]] && (self.selectedArray.count != 0)) {
            [self.rightBtn setTitle:[NSString stringWithFormat:@"删除(%ld)",self.selectedArray.count] forState:(UIControlStateSelected)];
            
        }else{
            [self.rightBtn setTitle:@"删除" forState:(UIControlStateSelected)];
        }
        
        NSLog(@"选中的数组 ==== %@",self.selectedArray);
    }];
    
    if (self.according == 1) {
        cell.playImageV.hidden = YES;
        cell.playLabel.hidden = YES;
    }else{
        cell.playImageV.hidden = NO;
        cell.playLabel.hidden = NO;
    }
    
    
        return cell;
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    return CGSizeMake(KSW/3-20, KSW/3-20);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 10, 15);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MyCollectionWorksModel *Worksmodel = self.dataArray[indexPath.row];
    WorldDetailViewController *vc = [[WorldDetailViewController alloc]init];
    vc.worksID = Worksmodel.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSMutableArray *)selectedArray{
    if (!_selectedArray) {
        _selectedArray = [[NSMutableArray alloc]init];
    }
    return _selectedArray;
}

-(void)rightClick:(UIButton *)sender{
    sender.selected = !sender.selected;
    
    if (!sender.selected) {
        
        if (self.selectedArray != nil && ![self.selectedArray isKindOfClass:[NSNull class]] && (self.selectedArray.count != 0)) {
            UIAlertController *controller=[UIAlertController alertControllerWithTitle:@"" message:@"你确定要删除作品吗" preferredStyle:UIAlertControllerStyleAlert];
                
            UIAlertAction *act1=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            }];
            UIAlertAction *act2=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                NSString *pidList = [self.selectedArray componentsJoinedByString:@","];
                NSLog(@"选中的id = %@",pidList);
                [RequestManager deletePublishsByIds:pidList withSuccess:^(id  _Nullable response) {
                    if ([response[@"status"] integerValue] == 200) {
                        [self showText:@"删除成功"];
                        [self deletaLocolSelectedCell];
                    }else{
                        [self showText:response[@"msg"]];
                    }
                } withFail:^(NSError * _Nullable error) {
                    
                }];
            }];
            [controller addAction:act1];
            [controller addAction:act2];
            
            [self presentViewController:controller animated:YES completion:^{
                
            }];
        }else{
            
            
    }
    }
  [self.collectionView reloadData];
}

-(void)deletaLocolSelectedCell{
    NSMutableArray * arr_selected = [NSMutableArray array];
    for (MyCollectionWorksModel *Worksmodel in self.dataArray) {
        for (int i = 0;i<self.selectedArray.count ;i++ ) {
            NSString * ids = [NSString stringWithFormat:@"%@",self.selectedArray[i]];
            if (Worksmodel.ID == ids.integerValue) {
                [arr_selected addObject:Worksmodel];
            }
            
        }
    }
    [self.selectedArray removeAllObjects];
    [self.dataArray removeObjectsInArray:arr_selected];
    self.nullView.hidden = self.dataArray.count;
    [self.rightBtn setTitle:@"删除" forState:(UIControlStateSelected)];
    [self.collectionView reloadData];
}


@end
