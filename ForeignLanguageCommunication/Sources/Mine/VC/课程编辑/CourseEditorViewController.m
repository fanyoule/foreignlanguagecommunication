//
//  CourseEditorViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "CourseEditorViewController.h"
#import <JXCategoryView/JXCategoryView.h>

#import "HasBeenOnViewController.h"// 已上架
#import "HasBeenOffTheShelvesViewController.h"// 已下架
#import "TheNewCurriculumViewController.h"// 新建
@interface CourseEditorViewController ()<UIScrollViewDelegate,JXCategoryViewDelegate>
@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;
@property (nonatomic, strong) UIView *contentView;
@end

@implementation CourseEditorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"课程编辑";
    [self addRightBtnWith:@"新建"];
    [self setUpNavView];
    [self setUpSubView];
    [self.view bringSubviewToFront:self.categoryTitleView];
    // Do any additional setup after loading the view.
}
- (void)setUpNavView {
    self.navView.backgroundColor = UIColor.clearColor;
    
   
    [self.view addSubview:self.categoryTitleView];
    self.categoryTitleView.frame = CGRectMake(0, self.navView.size.height, KSW, 27);
    
 
    
}

- (void)setUpSubView {
    
    // 已上架  已下架
    
    //scrollview 加载三个VC.view
    //  vc.view addsubview  tableview
    //  vc.tableview add  scrillview
        //vc.scrollview add collectionview
    
    [self.view addSubview:self.mainScrollView];
    
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.bounces = NO;
    [self.mainScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(SNavBarHeight);
        make.left.right.mas_offset(0);
        make.bottom.mas_offset(0);
    }];
    [self.mainScrollView addSubview:self.contentView];
    
    self.mainScrollView.contentSize = CGSizeMake(KSW * 2, 0);
    self.mainScrollView.delegate = self;
    self.contentView.frame = CGRectMake(0, 0, KSW*2, ScreenHeight -TabbarHeight);
    
    
    HasBeenOnViewController *classVC = [[HasBeenOnViewController alloc] init];
    [self addChildViewController:classVC];
    HasBeenOffTheShelvesViewController *hasVC = [[HasBeenOffTheShelvesViewController alloc] init];
    [self addChildViewController:hasVC];
    
    
    [self scrollViewDidEndScrollingAnimation:self.mainScrollView];
    
}

#pragma mark scrollview delegate

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    NSInteger index = scrollView.contentOffset.x / KSW;
    UIViewController *newVC = self.childViewControllers[index];
    if (newVC.isViewLoaded) {
        return;
    }
    [self.contentView addSubview:newVC.view];
    newVC.view.frame = CGRectMake(KSW * index, 0, KSW, self.contentView.height);
    [newVC didMoveToParentViewController:self];
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger index = scrollView.contentOffset.x / KSW;
    [self.categoryTitleView selectItemAtIndex:index];
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

//MARK: categorytitle delegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.mainScrollView.contentOffset = CGPointMake(KSW * index, 0);
    [self scrollViewDidEndScrollingAnimation:self.mainScrollView];
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
//MARK: Getter
- (JXCategoryTitleView *)categoryTitleView {
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titles = @[@"已上架",@"已下架"];
        _categoryTitleView.titleColor = RGBA(24, 24, 24, 1);
        _categoryTitleView.titleSelectedColor = RGBA(52, 120, 245, 1);
        _categoryTitleView.titleFont = kFont(14);
        _categoryTitleView.titleSelectedFont = kFont_Bold(14);
        _categoryTitleView.cellSpacing = 30;
        _categoryTitleView.cellWidth = 35;
        _categoryTitleView.collectionView.scrollEnabled = NO;
        _categoryTitleView.backgroundColor = UIColor.clearColor;
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorColor = RGBA(52, 120, 245, 1);
        lineView.indicatorWidth = 20;
        lineView.indicatorHeight = 3;
        lineView.indicatorCornerRadius = 1.5;
        _categoryTitleView.indicators = @[lineView];
        _categoryTitleView.delegate = self;
    }
    return _categoryTitleView;
}
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = UIView.new;
        _contentView.backgroundColor = UIColor.clearColor;
    }
    return _contentView;
}

-(void)rightClick:(UIButton *)sender{
    TheNewCurriculumViewController *vc = [[TheNewCurriculumViewController alloc]init];
    vc.isJudge = 1;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
