//
//  CourseDetailsCommentDetailsModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourseDetailsCommentDetailsModel : NSObject
/** 评论内容*/
@property (nonatomic, strong)NSString *content;
/** 课程id*/
@property (nonatomic, assign)NSInteger courseId;
/** 评价时间*/
@property (nonatomic, strong)NSString *createTime;
/** 1:好评，2：中评，3：差评*/
@property (nonatomic, assign)NSInteger evaluation;
/** 评价人昵称*/
@property (nonatomic, strong)NSString *nickname;
/** 评价人头像*/
@property (nonatomic, strong)NSString *url;
/** 评价人id*/
@property (nonatomic, assign)NSInteger userId;

@property(nonatomic,assign)CGFloat cellHeight;
@end

NS_ASSUME_NONNULL_END
