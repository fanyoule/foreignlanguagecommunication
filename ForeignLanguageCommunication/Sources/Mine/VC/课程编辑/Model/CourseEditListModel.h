//
//  CourseEditListModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/20.
//  课程编辑列表

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourseEditListModel : NSObject
/** 购买者id*/
@property (nonatomic, assign)NSInteger buyerId;
/** 课程图片*/
@property (nonatomic, strong)NSString *coursePic;
/** 创建时间*/
@property (nonatomic, strong)NSString *createTime;
/** 是否评论*/
@property (nonatomic)BOOL evaluation;
/** 课程id*/
@property (nonatomic, assign)NSInteger  ID;
/** 课程名称*/
@property (nonatomic, strong)NSString * name;
/** 教练昵称*/
@property (nonatomic, strong)NSString * nickname;
/** 已售*/
@property (nonatomic, assign)NSInteger  orders;
/** 价格*/
@property (nonatomic, assign)NSInteger  price;
/** 课程状态*/
@property (nonatomic, strong)NSString * status;
/** 课程时间*/
@property (nonatomic, assign)NSInteger time;
/** 教练id*/
@property (nonatomic, assign)NSInteger userId;
/** 教练头像*/
@property (nonatomic, strong)NSString * userUrl;


@end

NS_ASSUME_NONNULL_END
