//
//  CourseDetailsCommentsModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourseDetailsCommentsModel : NSObject


/** 类型*/
@property (nonatomic, strong)NSString *type;
/** 评论详情*/
@property (nonatomic, strong)NSArray *evaluation;
/** 评论数量*/
@property (nonatomic, assign)NSInteger num;
@end

NS_ASSUME_NONNULL_END
