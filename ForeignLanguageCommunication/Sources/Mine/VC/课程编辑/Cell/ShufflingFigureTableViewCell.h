//
//  ShufflingFigureTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
NS_ASSUME_NONNULL_BEGIN

@interface ShufflingFigureTableViewCell : UITableViewCell

@property (nonatomic, strong)SDCycleScrollView *circleView;

@end

NS_ASSUME_NONNULL_END
