//
//  NewCourseSelectionTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "NewCourseSelectionTableViewCell.h"

@implementation NewCourseSelectionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.titleImageV];
    [self.contentView addSubview:self.label];
    [self.contentView addSubview:self.lineView];
    
    [self.titleImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@17);
    }];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.titleImageV.mas_right).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.bottom.equalTo(self.contentView).offset(-15).priority(600);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@1);
        
    }];
    
}

- (UIImageView *)titleImageV{
    if (!_titleImageV) {
        _titleImageV = [[UIImageView alloc]init];
        _titleImageV.image = [UIImage imageNamed:@"新建课程-适应人群"];
        _titleImageV.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _titleImageV;
}

- (UILabel *)label{
    if (!_label) {
        _label = [[UILabel alloc]init];
        _label.text = @"请选择标签适应人群，可多选";
        _label.textColor = RGBA(196, 196, 196, 1);
        _label.font = kFont(14);
        _label.textAlignment = 0;
    }
    return _label;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(245, 245, 245, 1);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
