//
//  TheNewCurriculumTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "TheNewCurriculumTableViewCell.h"
@interface TheNewCurriculumTableViewCell ()<UITextFieldDelegate>


@property (nonatomic, strong)UIView *lineView;

@end
@implementation TheNewCurriculumTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.titleImageV];
    [self.contentView addSubview:self.textField];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.ELabel];
    [self.titleImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@17);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.titleImageV.mas_right).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.height.equalTo(@1);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.ELabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
    }];
    
}

- (UIImageView *)titleImageV{
    if (!_titleImageV) {
        _titleImageV = [[UIImageView alloc]init];
        _titleImageV.image = [UIImage imageNamed:@"新建课程-标题"];
        _titleImageV.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _titleImageV;
}

-(UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc]init];
        _textField.backgroundColor = UIColor.clearColor;
        _textField.tag = 900;
        _textField.placeholder = @"请填写课程名称（15个字符）";
        _textField.font = kFont(14);
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    }
    return _textField;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(245, 245, 245, 1);
    }
    return _lineView;
}

- (UILabel *)ELabel{
    if (!_ELabel) {
        _ELabel = [[UILabel alloc]init];
        _ELabel.hidden = YES;
        _ELabel.text = @"(500E币)";
        _ELabel.textColor = RGBA(153, 153, 153, 1);
        _ELabel.font = kFont(14);
        _ELabel.textAlignment = 2;
        _ELabel.tag = 500;
    }
    return _ELabel;
}

- (void)textFieldDidChange:(UITextField *)textField {
    int kMaxLength = 15;
    if (self.mobilePhone) {
        self.mobilePhone(textField.text);
    }
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入方式
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
    if (self.isJudge == 1) {
        self.ELabel.hidden = YES;
    }else{
        if (textField.text.length <= 0) {
            self.ELabel.hidden = YES;
        }else{
            self.ELabel.hidden = NO;
            self.ELabel.text = [NSString stringWithFormat:@"(%ldE币)",[textField.text integerValue]*100];
        }
    }
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
