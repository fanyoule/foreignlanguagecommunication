//
//  TheCoachBusinessCardTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/8.
//

#import <UIKit/UIKit.h>
#import "CourseDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TheCoachBusinessCardTableViewCell : UITableViewCell


@property (nonatomic, strong)CourseDetailsModel *model;
@end

NS_ASSUME_NONNULL_END
