//
//  NewCourseSelectionTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewCourseSelectionTableViewCell : UITableViewCell
@property (nonatomic, strong)UIImageView *titleImageV;
@property (nonatomic, strong)UILabel *label;
@property (nonatomic, strong)UIView *lineView;
@end

NS_ASSUME_NONNULL_END
