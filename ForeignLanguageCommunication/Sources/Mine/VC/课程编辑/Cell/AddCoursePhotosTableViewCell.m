//
//  AddCoursePhotosTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "AddCoursePhotosTableViewCell.h"
#import "PersonalInformationWorksCollectionViewCell.h"
#import "TZImagePickerController.h"
@interface AddCoursePhotosTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource,TZImagePickerControllerDelegate>

@property (nonatomic, strong)UICollectionView *collectionView;

@property (nonatomic, strong)NSArray *selectedArray;

@property (nonatomic, strong)NSMutableArray *selectedImageArray;


@end
@implementation AddCoursePhotosTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.rulesLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(17);
        make.left.equalTo(self.contentView).offset(15);
    }];
    
    
    [self.contentView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.right.bottom.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView).offset(-30);
//        make.bottom.equalTo(self.contentView).offset(-30).priority(1000);
//        make.height.equalTo(@160);
    }];
   
    
    
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.top.equalTo(self.collectionView.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10).priority(600);
    }];
    
    [self.collectionView registerClass:[PersonalInformationWorksCollectionViewCell class] forCellWithReuseIdentifier:@"PersonalInformationWorksCollectionViewCell"];
    
}

// MARK: 课程图片数组
- (void)setCoursePicArray:(NSArray *)coursePicArray{
    _coursePicArray = coursePicArray;
    NSLog(@"图片数组 ==== %@",coursePicArray);
    
    self.selectedImageArray = [NSMutableArray arrayWithArray:coursePicArray];
    
    
    [self.collectionView reloadData];
    
}


- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"添加课程照片";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(16);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"仅可上传6张照片";
        _rulesLabel.textColor = RGBA(153, 153, 153, 1);
        _rulesLabel.font = kFont_Regular(12);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = ColorRGBA(255, 255, 255, 1);
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.allowsMultipleSelection = YES;
    }
    return _collectionView;
}

- (NSMutableArray *)selectedImageArray{
    if (!_selectedImageArray) {
        _selectedImageArray = [[NSMutableArray alloc]init];
    }
    return _selectedImageArray;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.selectedImageArray.count == 0) {
        return 1;
    }else{
        return self.selectedImageArray.count;
    }
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    PersonalInformationWorksCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PersonalInformationWorksCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    cell.selector.hidden = YES;
    cell.playImageV.hidden = YES;
    cell.playLabel.hidden = YES;
    if (self.selectedImageArray.count == 0) {
        cell.imageV.image = kImage(@"添加照片");
    }else{
        [cell.imageV sd_setImageWithURL:[NSURL URLWithString:self.selectedImageArray[indexPath.row]]];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (self.selectedImageArray.count >= 6) {
        
    }else{
        
//        NSInteger i = 6 - self.selectedImageArray.count;
        NSInteger i = (self.selectedImageArray.count == 0) ? (6 - self.selectedImageArray.count) : (6 - self.selectedImageArray.count + 1);
        UIViewController *currentVC = [Util currentVC];
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:i columnNumber:4 delegate:self pushPhotoPickerVc:YES];

        imagePickerVc.allowTakeVideo = NO;
        imagePickerVc.allowPickingVideo = NO;
        imagePickerVc.showSelectBtn = NO;
        imagePickerVc.allowCrop = NO;
        imagePickerVc.allowPreview = YES;
    // You can get the photos by block, the same as by delegate.
    // 你可以通过block或者代理，来得到用户选择的照片.
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            self.selectedArray = photos;
            
            [self uploadImage];
            if (self.selectedImageArray.count > 0) {
                [self.selectedImageArray removeObject:self.selectedImageArray[indexPath.row]];
            }
        }];
        if (iosSystemVersion>=13.0) {
            imagePickerVc.modalPresentationStyle = 0;
        }
        [currentVC presentViewController:imagePickerVc animated:YES completion:nil];
    }
}

- (void)uploadImage {
    [RequestManager uploadImage:self.selectedArray withSuccess:^(id  _Nullable response) {
        NSArray *data = response[@"data"];
        
        for (int i = 0; i < data.count; i ++) {
            NSString *url = data[i][@"url"];
            [self.selectedImageArray addObject:url];
            
        }
        if (self.uploadPhotos) {
            self.uploadPhotos(self.selectedImageArray);
        }
        [self.collectionView reloadData];
        NSLog(@"images==%@",data);
        NSLog(@"selectedImageArray==%@",self.selectedImageArray);
    } withFail:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{


    return CGSizeMake(KSW/3-20, KSW/3-20);

}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 10, 15);
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
