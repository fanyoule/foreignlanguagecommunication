//
//  CoursePriceDetailsTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/8.
//

#import "CoursePriceDetailsTableViewCell.h"
#import "TYTagView.h"
#import "MineDataPickerModel.h"
@interface CoursePriceDetailsTableViewCell ()
/** ¥*/
@property (nonatomic, strong)UILabel *moneyLabel;
/** 价格*/
@property (nonatomic, strong)UILabel *priceLabel;
/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;
/** 标签*/
@property (nonatomic, strong)UIScrollView *scrollV;
/** 内容*/
@property (nonatomic, strong)UILabel *contentLabel;
/** 已售*/
@property (nonatomic, strong)UILabel *soldLabel;
/** 收藏按钮*/
@property (nonatomic, strong)UIButton *collectionBtn;
/** 收藏文字*/
@property (nonatomic, strong)UILabel *collectionLabel;
/** 适应人群 内容*/
@property (nonatomic, strong)TYTagView *tYTagView;
@end
@implementation CoursePriceDetailsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.moneyLabel];
    [self.contentView addSubview:self.priceLabel];
    [self.contentView addSubview:self.rulesLabel];
    [self.contentView addSubview:self.scrollV];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.soldLabel];
    [self.contentView addSubview:self.collectionLabel];
    [self.contentView addSubview:self.collectionBtn];
    
    
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(25);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.moneyLabel);
        make.left.equalTo(self.moneyLabel.mas_right).offset(3);
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.priceLabel);
        make.left.equalTo(self.priceLabel.mas_right).offset(2);
    }];
    
    
    [self.collectionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(25);
        make.height.mas_equalTo(50);
        make.top.equalTo(self.priceLabel.mas_bottom).offset(30);
        make.right.mas_equalTo(-15);
    }];
    [self.collectionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.collectionBtn);
        make.centerY.mas_equalTo(self.collectionBtn.mas_bottom);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.equalTo(self.collectionBtn.mas_left).offset(-5);
        make.top.equalTo(self.priceLabel.mas_bottom).offset(30);
    }];
    [self.soldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.contentLabel.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView).offset(-15).priority(600);
    }];
    
    [self.contentView addSubview:self.tYTagView];
    [self.tYTagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.rulesLabel.mas_bottom).offset(5);
        make.left.equalTo(self.moneyLabel);
        make.right.equalTo(self.contentView);
    }];
    
    
    
}

- (void)setModel:(CourseDetailsModel *)model{
    _model = model;

    NSMutableArray *nameArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < model.applyCrowd.count; i++) {
        MineDataPickerModel *PickerModel = [MineDataPickerModel mj_objectWithKeyValues:model.applyCrowd[i]];
        [nameArray addObject:PickerModel.name];
    }
    self.tYTagView.items = nameArray;

    self.priceLabel.text = [NSString stringWithFormat:@"%ld",model.price];
    self.rulesLabel.text = [NSString stringWithFormat:@"/%ld分钟/一节课",model.time];
    self.contentLabel.text = model.name;
    self.soldLabel.text = [NSString stringWithFormat:@"已售%ld",model.orders];
    
    if (model.isCollect == 1) {
        self.collectionBtn.selected = YES;
        _collectionLabel.text = @"已收藏";
    }else{
        self.collectionBtn.selected = NO;
        _collectionLabel.text = @"收藏";
    }
    
}
- (void)collectionAction {
    if (self.block) {
        self.block(self.model);
    }
}

- (TYTagView *)tYTagView{
    if (!_tYTagView) {
        _tYTagView = [[TYTagView alloc]init];
        _tYTagView.backgroundColor = UIColor.clearColor;
        _tYTagView.top = 5;
        _tYTagView.margin = 5;
        _tYTagView.tagHeight = 15;
    }
    return _tYTagView;
}

- (UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc]init];
        _moneyLabel.text = @"¥";
        _moneyLabel.textColor = RGBA(52, 120, 245, 1);
        _moneyLabel.font = kFont_Medium(11);
        _moneyLabel.textAlignment = 0;
    }
    return _moneyLabel;
}

- (UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.text = @"3600";
        _priceLabel.textColor = RGBA(52, 120, 245, 1);
        _priceLabel.font = kFont_Bold(24);
        _priceLabel.textAlignment = 0;
    }
    return _priceLabel;
}

- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"/90分钟/一节课";
        _rulesLabel.textColor = RGBA(52, 120, 245, 1);
        _rulesLabel.font = kFont_Medium(11);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"青少年课程口语1小时练习青少年课程口语1小时练习青少年课程口语1小时练习";
        _contentLabel.textColor = RGBA(24, 24, 24, 1);
        _contentLabel.font = kFont_Medium(14);
        _contentLabel.textAlignment = 0;
        _contentLabel.numberOfLines = 2;
    }
    return _contentLabel;
}

- (UILabel *)soldLabel{
    if (!_soldLabel) {
        _soldLabel = [[UILabel alloc]init];
        _soldLabel.text = @"已售1545";
        _soldLabel.textColor = RGBA(102, 102, 102, 1);
        _soldLabel.font = kFont_Medium(11);
        _soldLabel.textAlignment = 0;
    }
    return _soldLabel;
}

- (UIButton *)collectionBtn{
    if (!_collectionBtn) {
        _collectionBtn = [[UIButton alloc]init];
        [_collectionBtn setImage:[UIImage imageNamed:@"icon--未收藏"] forState:(UIControlStateNormal)];
        [_collectionBtn setImage:[UIImage imageNamed:@"icon--收藏"] forState:(UIControlStateSelected)];
        [_collectionBtn addTarget:self action:@selector(collectionAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _collectionBtn;
}

- (UILabel *)collectionLabel{
    if (!_collectionLabel) {
        _collectionLabel = [[UILabel alloc]init];
        _collectionLabel.text = @"收藏";
        _collectionLabel.textAlignment = 1;
        _collectionLabel.textColor = RGBA(24, 24, 24, 1);
        _collectionLabel.font = kFont_Medium(12);
    }
    return _collectionLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
