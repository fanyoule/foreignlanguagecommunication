//
//  CoursePriceDetailsTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/8.
//

#import <UIKit/UIKit.h>
#import "CourseDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^CoursePriceDetailsTableViewCellBlock)(CourseDetailsModel *model);
@interface CoursePriceDetailsTableViewCell : UITableViewCell


@property (nonatomic, strong)CourseDetailsModel *model;
@property (nonatomic, copy) CoursePriceDetailsTableViewCellBlock block;
@end

NS_ASSUME_NONNULL_END
