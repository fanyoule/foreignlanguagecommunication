//
//  CourseIntroductionTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "CourseIntroductionTableViewCell.h"
@interface CourseIntroductionTableViewCell ()
@property (nonatomic, strong)UIView *blueView;
@property (nonatomic, strong)UILabel *titleLabel;
/** 简介*/
@property (nonatomic, strong)UILabel *introductionlabel;
@end
@implementation CourseIntroductionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    
    [self.contentView addSubview:self.blueView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.introductionlabel];
    [self.blueView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.left.equalTo(self.contentView).offset(15);
        make.height.equalTo(@14);
        make.width.equalTo(@5);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.blueView);
        make.left.equalTo(self.blueView.mas_right).offset(5);
    }];
    [self.introductionlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.bottom.equalTo(self.contentView).offset(-10).priority(600);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(15);
    }];
    
}
- (void)setModel:(CourseDetailsModel *)model{
    self.introductionlabel.text = model.content;
}

- (UIView *)blueView{
    if (!_blueView) {
        _blueView = [[UIView alloc]init];
        _blueView.backgroundColor = RGBA(52, 120, 245, 1);
        _blueView.layer.cornerRadius = 2.5;
        _blueView.clipsToBounds = YES;
    }
    return _blueView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"课程简介";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Bold(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UILabel *)introductionlabel{
    if (!_introductionlabel) {
        _introductionlabel = [[UILabel alloc]init];
        _introductionlabel.text = @"简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介简介";
        _introductionlabel.textColor = RGBA(24, 24, 24, 1);
        _introductionlabel.font = kFont_Medium(15);
        _introductionlabel.textAlignment = 0;
        _introductionlabel.numberOfLines = 0;
    }
    return _introductionlabel;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
