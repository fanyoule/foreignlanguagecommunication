//
//  CourseEditorCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "CourseEditorCollectionViewCell.h"
@interface CourseEditorCollectionViewCell ()
/** 封面*/
@property (nonatomic, strong)UIImageView *coverImageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 价格图标*/
@property (nonatomic, strong)UILabel *priceIcon;
/** 价格*/
@property (nonatomic, strong)UILabel *priceLabel;
/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;
/** 已售*/
@property (nonatomic, strong)UILabel *soldLabel;
@end

@implementation CourseEditorCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.coverImageV];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.priceIcon];
    [self.contentView addSubview:self.priceLabel];
    [self.contentView addSubview:self.rulesLabel];
    [self.contentView addSubview:self.soldLabel];
    [self.contentView addSubview:self.shelvesBtn];
    [self.contentView addSubview:self.removeBtn];
    [self.contentView addSubview:self.editorBtn];
    
    [self.coverImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@80);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.coverImageV);
        make.left.equalTo(self.coverImageV.mas_right).offset(10);
        make.right.equalTo(self.contentView).offset(-20);
    }];
    [self.priceIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverImageV.mas_right).offset(10);
        make.bottom.equalTo(self.coverImageV).offset(-5);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceIcon.mas_right);
        make.bottom.equalTo(self.priceIcon).offset(5);
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceLabel.mas_right).offset(5);
        make.bottom.equalTo(self.priceIcon);
    }];
    [self.soldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.rulesLabel);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
    [self.shelvesBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(-20);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
    [self.editorBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.shelvesBtn);
        make.right.equalTo(self.shelvesBtn.mas_left).offset(-10);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
    [self.removeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.editorBtn);
        make.right.equalTo(self.editorBtn.mas_left).offset(-10);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
}

-(void)setModel:(CourseEditListModel *)model{
    [self.coverImageV sd_setImageWithURL:[NSURL URLWithString:model.coursePic]];
    self.titleLabel.text = model.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld",model.price];
    self.rulesLabel.text = [NSString stringWithFormat:@"/%ld分钟/一节课",model.time];
    self.soldLabel.text = [NSString stringWithFormat:@"已售%ld",model.orders];
    
    
}


- (UIImageView *)coverImageV{
    if (!_coverImageV) {
        _coverImageV = [[UIImageView alloc]init];
        _coverImageV.image = [UIImage imageNamed:@"测试头像2"];
        _coverImageV.layer.cornerRadius = 6;
        _coverImageV.clipsToBounds = YES;
        _coverImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _coverImageV;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"青少年课程口语1小时练习青少年课程口语1小时练习青少年课程口语...小时练习";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(14);
        _titleLabel.textAlignment = 0;
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}
- (UILabel *)priceIcon{
    if (!_priceIcon) {
        _priceIcon = [[UILabel alloc]init];
        _priceIcon.text = @"¥";
        _priceIcon.textColor = RGBA(52, 120, 245, 1);
        _priceIcon.font = kFont_Medium(11);
        _priceIcon.textAlignment = 0;
    }
    return _priceIcon;
}
- (UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.text = @"600";
        _priceLabel.textColor = RGBA(52, 120, 245, 1);
        _priceLabel.font = kFont_Bold(24);
        _priceLabel.textAlignment = 0;
    }
    return _priceLabel;
}
- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"/90分钟/一节课";
        _rulesLabel.textColor = RGBA(52, 120, 245, 1);
        _rulesLabel.font = kFont_Medium(11);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}
- (UIButton *)shelvesBtn{
    if (!_shelvesBtn) {
        _shelvesBtn = [[UIButton alloc]init];
        [_shelvesBtn setTitle:@"下架" forState:(UIControlStateNormal)];
        [_shelvesBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _shelvesBtn.layer.cornerRadius = 6;
        _shelvesBtn.clipsToBounds = YES;
        _shelvesBtn.layer.borderWidth = 0.5;
        _shelvesBtn.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
        _shelvesBtn.titleLabel.font = kFont_Medium(14);
    }
    return _shelvesBtn;
}
- (UIButton *)removeBtn{
    if (!_removeBtn) {
        _removeBtn = [[UIButton alloc]init];
        [_removeBtn setTitle:@"删除" forState:(UIControlStateNormal)];
        [_removeBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        _removeBtn.titleLabel.font = kFont_Medium(14);
        _removeBtn.layer.cornerRadius = 6;
        _removeBtn.clipsToBounds = YES;
        _removeBtn.layer.borderWidth = 0.5;
        _removeBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
    }
    return _removeBtn;
}
- (UIButton *)editorBtn{
    if (!_editorBtn) {
        _editorBtn = [[UIButton alloc]init];
        [_editorBtn setTitle:@"编辑" forState:(UIControlStateNormal)];
        [_editorBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        _editorBtn.titleLabel.font = kFont_Medium(14);
        _editorBtn.layer.cornerRadius = 6;
        _editorBtn.clipsToBounds = YES;
        _editorBtn.layer.borderWidth = 0.5;
        _editorBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
    }
    return _editorBtn;
}

- (UILabel *)soldLabel{
    if (!_soldLabel) {
        _soldLabel = [[UILabel alloc]init];
        _soldLabel.text = @"已售1000";
        _soldLabel.textColor = RGBA(102, 102, 102, 1);
        _soldLabel.font = kFont_Medium(11);
        _soldLabel.textAlignment = 0;
    }
    return _soldLabel;
}
@end
