//
//  TheNewCurriculumTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import <UIKit/UIKit.h>

typedef void(^mobilePhoneNoBlock)(NSString *string);

NS_ASSUME_NONNULL_BEGIN

@interface TheNewCurriculumTableViewCell : UITableViewCell

@property (nonatomic, strong)UIImageView *titleImageV;

@property (nonatomic, strong)UITextField *textField;
/** E币*/
@property (nonatomic, strong)UILabel *ELabel;

@property (nonatomic, strong)mobilePhoneNoBlock mobilePhone;


/** 判断显不显示E币 1: 不显示  0：显示*/
@property (nonatomic, assign)NSInteger isJudge;
@end

NS_ASSUME_NONNULL_END
