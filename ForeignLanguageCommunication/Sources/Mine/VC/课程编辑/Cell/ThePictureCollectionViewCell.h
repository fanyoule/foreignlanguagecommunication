//
//  ThePictureCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThePictureCollectionViewCell : UICollectionViewCell
/** 图片*/
@property (nonatomic, strong)UIImageView *imageV;
/** 删除图片*/
@property (nonatomic, strong)UIImageView *deleteImageV;

@end

NS_ASSUME_NONNULL_END
