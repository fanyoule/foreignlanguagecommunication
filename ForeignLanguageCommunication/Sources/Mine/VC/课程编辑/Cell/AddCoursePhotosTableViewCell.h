//
//  AddCoursePhotosTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import <UIKit/UIKit.h>

typedef void(^clickUploadPhotos)(NSArray *urlString);

NS_ASSUME_NONNULL_BEGIN

@interface AddCoursePhotosTableViewCell : UITableViewCell
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 图片*/
@property (nonatomic, strong)UIImageView *imageV;
/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;

/** 课程图片*/
@property (nonatomic, strong)NSArray *coursePicArray;

@property (nonatomic, strong)clickUploadPhotos uploadPhotos;
@end

NS_ASSUME_NONNULL_END
