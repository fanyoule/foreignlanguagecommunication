//
//  ShufflingFigureTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import "ShufflingFigureTableViewCell.h"
@interface ShufflingFigureTableViewCell ()<SDCycleScrollViewDelegate>

@end
@implementation ShufflingFigureTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    
    /**初始化  默认图片*/
        self.circleView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:kImage(@"icon_home_banner")];
        /**图片加载模式*/
        self.circleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:self.circleView];
        /**本地轮播图*/
        self.circleView.localizationImageNamesGroup = @[@"测试头像",@"测试头像2"];
        self.circleView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
        self.circleView.layer.shadowOffset = CGSizeMake(1, 1);
        self.circleView.layer.shadowRadius = 4;
        self.circleView.layer.shadowOpacity = 0.5;
    
    
    [self.contentView addSubview:self.circleView];
    [self.circleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).priority(600);
        make.height.equalTo(@(kScaleSize(375)));
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
