//
//  CommentsTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import <UIKit/UIKit.h>
#import "CourseDetailsCommentDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CommentsTableViewCell : UITableViewCell
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字 */
@property (nonatomic, strong)UILabel *nameLabel;
/** 内容*/
@property (nonatomic, strong)UILabel *contentLabel;
/** 时间*/
@property (nonatomic, strong)UILabel *timeLabel;

@property (nonatomic, strong)CourseDetailsCommentDetailsModel *model;
@end

NS_ASSUME_NONNULL_END
