//
//  CommentsTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import "CommentsTableViewCell.h"
@interface CommentsTableViewCell ()

@end
@implementation CommentsTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(7.5);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@(35));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV).offset(0);
        make.left.equalTo(self.portraitImageV.mas_right).offset(11);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(5);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel);
        make.top.equalTo(self.contentLabel.mas_bottom).offset(0);
        make.bottom.equalTo(self.contentView).offset(-5).priority(600);
    }];
}

- (void)setModel:(CourseDetailsCommentDetailsModel *)model{
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.url]];
    self.nameLabel.text = model.nickname;
    self.contentLabel.text = model.content;
    self.timeLabel.text = model.createTime;
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像2"];
        _portraitImageV.layer.cornerRadius = 35/2;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(153, 153, 153, 1);
        _nameLabel.font = kFont_Medium(14);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好效果很好";
        _contentLabel.textColor = RGBA(24, 24, 24, 1);
        _contentLabel.font = kFont_Medium(14);
        _contentLabel.textAlignment = 0;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}

- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.text = @"04-29";
        _timeLabel.textColor = RGBA(153, 153, 153, 1);
        _timeLabel.font = kFont_Medium(11);
        _timeLabel.textAlignment = 0;
    }
    return _timeLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
