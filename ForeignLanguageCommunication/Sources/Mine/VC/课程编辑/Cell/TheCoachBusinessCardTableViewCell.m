//
//  TheCoachBusinessCardTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/8.
//

#import "TheCoachBusinessCardTableViewCell.h"
@interface TheCoachBusinessCardTableViewCell ()

/** 背景*/
@property (nonatomic ,strong)UIView *backView;
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/** 等级图片*/
@property (nonatomic, strong)UIImageView *levelImageV;

@end
@implementation TheCoachBusinessCardTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.portraitImageV];
    [self.backView addSubview:self.nameLabel];
    [self.backView addSubview:self.levelImageV];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.bottom.mas_equalTo(-10);
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.height.equalTo(@65);
    }];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.backView);
        make.left.mas_equalTo(15);
        make.width.height.mas_equalTo(44);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.mas_equalTo(self.portraitImageV.mas_right).offset(5);
    }];
    [self.levelImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.backView);
        make.left.mas_equalTo(self.nameLabel.mas_right).offset(10);
    }];
    
    
}

- (void)setModel:(CourseDetailsModel *)model{
    self.nameLabel.text = model.realName;
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.userUrl]];
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.cornerRadius = 6;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像"];
        _portraitImageV.layer.cornerRadius = 44/2;
        _portraitImageV.clipsToBounds = YES;
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _portraitImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"张三";
        _nameLabel.textColor = RGBA(51, 51, 51, 1);
        _nameLabel.font = kFont_Medium(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UIImageView *)levelImageV{
    if (!_levelImageV) {
        _levelImageV = [[UIImageView alloc]init];
        _levelImageV.image = [UIImage imageNamed:@"icon--认证"];
    }
    return _levelImageV;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
