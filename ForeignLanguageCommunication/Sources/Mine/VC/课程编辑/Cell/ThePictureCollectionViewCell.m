//
//  ThePictureCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "ThePictureCollectionViewCell.h"

@implementation ThePictureCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.imageV];
    [self.contentView addSubview:self.deleteImageV];
    [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [self.deleteImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.imageV);
        make.width.height.equalTo(@15);
    }];
    
}

- (UIImageView *)imageV{
    if (!_imageV) {
        _imageV = UIImageView.new;
        _imageV.image = [UIImage imageNamed:@"添加照片"];
        _imageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _imageV;
}

- (UIImageView *)deleteImageV{
    if (!_deleteImageV) {
        _deleteImageV = UIImageView.new;
        _deleteImageV.image = [UIImage imageNamed:@"评价-删除"];
    }
    return _deleteImageV;
}




@end
