//
//  CourseEditorCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import <UIKit/UIKit.h>
#import "CourseEditListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CourseEditorCollectionViewCell : UICollectionViewCell
/** 下架*/
@property (nonatomic, strong)UIButton *shelvesBtn;
/** 删除*/
@property (nonatomic, strong)UIButton *removeBtn;
/** 编辑*/
@property (nonatomic, strong)UIButton *editorBtn;

@property (nonatomic, strong)CourseEditListModel *model;
@end

NS_ASSUME_NONNULL_END
