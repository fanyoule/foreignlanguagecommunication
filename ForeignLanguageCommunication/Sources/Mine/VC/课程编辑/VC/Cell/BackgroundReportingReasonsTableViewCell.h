//
//  BackgroundReportingReasonsTableViewCell.h
//  VoiceLive
//
//  Created by 申修智 on 2020/10/24.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol BackgroundReportingReasonsTableViewCellDelegate<NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)clickBackgroundReportingReasonsTableViewCell:(NSInteger)integer;

@end
@interface BackgroundReportingReasonsTableViewCell : UITableViewCell
@property(nonatomic, strong)UIView *backView;

@property (nonatomic, weak) id<BackgroundReportingReasonsTableViewCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
