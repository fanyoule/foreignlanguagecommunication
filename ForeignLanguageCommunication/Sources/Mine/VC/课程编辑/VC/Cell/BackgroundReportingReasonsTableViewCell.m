//
//  BackgroundReportingReasonsTableViewCell.m
//  VoiceLive
//
//  Created by 申修智 on 2020/10/24.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "BackgroundReportingReasonsTableViewCell.h"
#import "ReportReasonOptionsTableViewCell.h"

@interface BackgroundReportingReasonsTableViewCell ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong)UITableView *tableView;


@property(nonatomic, strong)NSIndexPath *lastSelectIndexPath;
@end

@implementation BackgroundReportingReasonsTableViewCell

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]init];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    }
    return _tableView;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        
    }
    return self;
}


-(void)addControls{
    [self.contentView addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
    [self.backView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.right.and.left.and.bottom.equalTo(self.backView);
    }];
    
}

- (UIView *)backView{
    if (!_backView) {
        _backView =[UIView init];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 6;
        _backView.layer.masksToBounds = YES;
    }
    return _backView;
}

// 返回多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
        return 7;
    
   
    
}

// cell 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    return 50;
    
    
}

// 放入cell的地方
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *ident = @"ReportReasonOptionsTableViewCell";
    
    ReportReasonOptionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        cell = [[ReportReasonOptionsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ident];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        
    }
    if (indexPath.row == 1) {
        cell.label.text = @"人身攻击";
    }
    if (indexPath.row == 2) {
        cell.label.text = @"淫秽色情";
    }
    if (indexPath.row == 3) {
        cell.label.text = @"垃圾广告";
    }
    if (indexPath.row == 4) {
        cell.label.text = @"敏感信息";
    }
    if (indexPath.row == 5) {
        cell.label.text = @"侵权";
    }
    if (indexPath.row == 6) {
        cell.label.text = @"其他";
    }
    if([_lastSelectIndexPath isEqual:indexPath]){

    //设置选中图片

    cell.checkImage.image = [UIImage imageNamed:@"选中"];

    }else {

    //设置未选中图片

    cell.checkImage.image = [UIImage imageNamed:@"未选"];

    }

    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;{
    ReportReasonOptionsTableViewCell * lastSelectCell = [tableView cellForRowAtIndexPath: _lastSelectIndexPath];
    if (lastSelectCell != nil) {
        lastSelectCell.checkImage.image = [UIImage imageNamed:@"未选"];
        
    }
    ReportReasonOptionsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.checkImage.image = [UIImage imageNamed:@"选中"];
    _lastSelectIndexPath = indexPath;
    
    NSLog(@"111111 ======= %ld ",_lastSelectIndexPath.row);
    if ([self.delegate respondsToSelector:@selector(clickBackgroundReportingReasonsTableViewCell:)]) {
        [self.delegate clickBackgroundReportingReasonsTableViewCell:_lastSelectIndexPath.row];
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
