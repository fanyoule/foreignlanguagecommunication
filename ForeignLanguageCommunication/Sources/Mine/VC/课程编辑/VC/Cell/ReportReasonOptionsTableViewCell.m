//
//  ReportReasonOptionsTableViewCell.m
//  VoiceLive
//
//  Created by 申修智 on 2020/10/24.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ReportReasonOptionsTableViewCell.h"

@implementation ReportReasonOptionsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.label];
    [self.contentView addSubview:self.checkImage];
    [self.contentView addSubview:self.lineView];
    
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(20.5);
    }];
    [self.checkImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
    }];
    
    
    
}


- (UILabel *)label{
    if (!_label) {
        _label = [UILabel init];
        _label.text = @"泄露隐私";
        _label.textColor = ColorRGB(51, 51, 51);
        _label.font = kFont_Medium(16);
        _label.textAlignment = 0;
    }
    return _label;
}

- (UIImageView *)checkImage{
    if (!_checkImage) {
        _checkImage = [UIImageView init];
        
    }
    return _checkImage;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView init];
        _lineView.backgroundColor = ColorRGB(238, 238, 238);
    }
    return _lineView;
}

-(void)gouxuan:(UIButton *)button{
    if (button.selected == NO) {
        button.selected = YES;
    }else{
        button.selected = NO;
    }
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
