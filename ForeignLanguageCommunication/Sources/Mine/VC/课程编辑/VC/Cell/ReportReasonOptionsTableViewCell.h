//
//  ReportReasonOptionsTableViewCell.h
//  VoiceLive
//
//  Created by 申修智 on 2020/10/24.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportReasonOptionsTableViewCell : UITableViewCell
@property(nonatomic, strong)UILabel *label;
@property(nonatomic, strong)UIImageView *checkImage;
@property(nonatomic, strong)UIView *lineView;
@end

NS_ASSUME_NONNULL_END
