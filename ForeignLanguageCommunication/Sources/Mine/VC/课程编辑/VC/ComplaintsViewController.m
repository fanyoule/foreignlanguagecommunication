//
//  ComplaintsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//  投诉

#import "ComplaintsViewController.h"
#import "ThePictureCollectionViewCell.h"
#import "BackgroundReportingReasonsTableViewCell.h"
typedef void(^HttpResponseSuccessBlock)(id _Nullable response);
@interface ComplaintsViewController ()<HXPhotoViewDelegate,HXPhotoViewControllerDelegate,HXCustomNavigationControllerDelegate,HXPhotoViewCellCustomProtocol,UITableViewDelegate,UITableViewDataSource,BackgroundReportingReasonsTableViewCellDelegate>

@property (nonatomic, strong) YYTextView *yyTextView;
/**图片管理器*/
@property (nonatomic, strong) HXPhotoManager *photoManager;
/**选择图片View*/
@property (nonatomic, strong) HXPhotoView *photoView;

/**视频封面*/
@property (nonatomic, copy) NSString *videoCoverUrl;
/**视频封面图片*/
@property (nonatomic, strong) UIImage *videoImage;
/**视频URL*/
@property (nonatomic, copy) NSString *videoUrl;
/**图片URL集合*/
@property (nonatomic, strong) NSArray *imageUrlArray;

/**存储图片/视频 model*/
@property (nonatomic, copy) NSArray *allPhotoArray;
/**图片Image*/
@property (nonatomic, strong) NSMutableArray *assetArray;
/**是否原图*/
@property (nonatomic) BOOL isOriginal;
/**判断是否视频还是图片*/
@property (nonatomic, assign) BOOL isPhotos;
//MARK: 判断当前是否可以点击发布按钮
//发布条件 文字/图片/视频/语音/  音频 audioPath判断    tagModel 判断
@property (nonatomic) BOOL hasText;
@property (nonatomic) BOOL hasPhoto;


// 举报类型
@property (nonatomic, assign)int type;
@end

@implementation ComplaintsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isJudge == 1) {
        self.navTitleString = @"举报";
        
    }else{
        self.navTitleString = @"投诉";
        
    }
    [self addRightBtnWith:@"提交"];
    
    self.navView.backgroundColor = UIColor.whiteColor;
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    
    [self setUpUI];
}
- (void)setUpUI {
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    
    
    UIView *backView = [[UIView alloc]init];
    backView.backgroundColor = UIColor.clearColor;
    
    [self.mainTableView addSubview:backView];
    
    
    UIView *bg = [[UIView alloc] init];
    bg.backgroundColor = UIColor.whiteColor;
    bg.layer.cornerRadius = 12;
    bg.layer.masksToBounds = YES;
    [backView addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backView).offset(15);
        make.right.equalTo(backView).offset(-15);
        make.top.equalTo(@20);
        make.height.equalTo(@340);
    }];

    [bg addSubview:self.yyTextView];
    [self.yyTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(bg).offset(20);
        make.right.equalTo(bg.mas_right).offset(-10);
        make.left.equalTo(@10);
    }];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    if (self.isJudge == 1) {
        titleLabel.text = @"请选择你要举报的原因";
    }else{
        titleLabel.text = @"请选择你要投诉的原因";
    }
    titleLabel.textColor = ColorRGB(153, 153, 153);
    titleLabel.font = kFont(14);
    titleLabel.textAlignment = 0;
    [backView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backView).offset(15);
        make.top.equalTo(bg.mas_bottom).offset(10);
    }];
    
    
    [bg addSubview:self.photoView];
    self.photoView.frame = CGRectMake(0, 220, KSW, kScaleSize(130));
    
    self.mainTableView.tableHeaderView = backView;
    backView.frame = CGRectMake(0, SNavBarHeight, KSW, 400);
}
- (void)backClick {
    [self.view endEditing:YES];
    if (self.hasPhoto || self.yyTextView.text.length > 0) {
        kWeakSelf(self)
        [self alertView:@"" msg:@"确定放弃编辑内容" btnArrTitles:@[@"确定"] showCacel:YES style:UIAlertControllerStyleAlert block:^(int index) {
            [weakself.navigationController popViewControllerAnimated:YES];
        }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
// MARK: 提交
- (void)rightClick:(UIButton *)sender {
    if (self.type == 0) {
        if (self.isJudge == 1) {
            [self showText:@"请选择举报原因"];
        }else{
            [self showText:@"请选择投诉原因"];
        }
    }else{
        if (self.hasPhoto&&self.yyTextView.text.length > 0) {
            [self.assetArray removeAllObjects];
            requestCount = 0;
            [SVProgressHUD showWithStatus:@""];
            [self postFiles];
        } else {
            if (self.isJudge == 1) {
                [self showText:@"请填写举报原因或上传照片"];
            }else{
                [self showText:@"请填写投诉原因或上传照片"];
            }
            
        }
    }
    
}
- (NSMutableArray *)assetArray{
    if (!_assetArray) {
        _assetArray = [[NSMutableArray alloc]init];
    }
    return _assetArray;
}

- (void)requestMediaSourceWith:(HXPhotoModel *)model success:(HttpResponseSuccessBlock)success{
    
    // 如果将_manager.configuration.requestImageAfterFinishingSelection 设为YES，
    // 那么在选择完成的时候就会获取图片和视频地址
    // 如果选中了原图那么获取图片时就是原图
    // 获取视频时如果设置 exportVideoURLForHighestQuality 为YES，则会去获取高等质量的视频。其他情况为中等质量的视频
    // 个人建议不在选择完成的时候去获取，因为每次选择完都会去获取。获取过程中可能会耗时过长
    // 可以在要上传的时候再去获取
        // 数组里装的是所有类型的资源，需要判断
        // 先判断资源类型
        if (model.subType == HXPhotoModelMediaSubTypePhoto) {
            // 当前为图片
            if (model.photoEdit) {
                // 如果有编辑数据，则说明这张图篇被编辑过了
                // 需要这样才能获取到编辑之后的图片
                UIImage *image = model.photoEdit.editPreviewImage;
                success(image);
            }
            // 再判断具体类型
            else {
                // 到这里就是手机相册里的图片了 model.asset PHAsset对象是有值的
                // 如果需要上传 Gif 或者 LivePhoto 需要具体判断
                if (model.type == HXPhotoModelMediaTypePhoto) {
                    // 普通的照片，如果不可以查看和livePhoto的时候，这就也可能是GIF或者LivePhoto了，
                    // 如果你的项目不支持动图那就不要取NSData或URL，因为如果本质是动图的话还是会变成动图传上去
                    // 这样判断是不是GIF model.photoFormat == HXPhotoModelFormatGIF
                    
                    // 如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.previewPhoto 或者 model.thumbPhoto 在选择完成时候已经获取并且赋值了
                    // 获取image
                    // size 就是获取图片的质量大小，原图的话就是 PHImageManagerMaximumSize，其他质量可设置size来获取
                    CGSize size;
                    if (self.isOriginal) {
                        size = PHImageManagerMaximumSize;
                    }else {
                        size = CGSizeMake(model.imageSize.width * 0.5, model.imageSize.height * 0.5);
                    }
                    
                    
                    [model requestPreviewImageWithSize:size startRequestICloud:^(PHImageRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        // 如果图片是在iCloud上的话会先走这个方法再去下载
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        // iCloud的下载进度
                    } success:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        // image
                        success(image);
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }else if (model.type == HXPhotoModelMediaTypePhotoGif) {
                    // 动图，如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.imageURL。因为在选择完成的时候已经获取了不用再去获取
//                    model.imageURL;
                    // 上传动图时，不要直接拿image上传哦。可以获取url或者data上传
                    // 获取data
                    NSLog(@"modelURL==%@",model.imageURL);
                    [model requestImageURLStartRequestICloud:^(PHContentEditingInputRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        
                    } success:^(NSURL * _Nullable imageURL, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        NSLog(@"URL==%@",imageURL);
                        
                        success(imageURL);
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        
                    }];
                    
//                    [model requestImageDataStartRequestICloud:nil progressHandler:nil success:^(NSData * _Nullable imageData, UIImageOrientation orientation, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
//
//
//                        success(model.imageURL);
//                    } failed:nil];
                }else if (model.type == HXPhotoModelMediaTypeLivePhoto) {
                    // LivePhoto，requestImageAfterFinishingSelection = YES 时没有处理livephoto，需要自己处理
                    // 如果需要上传livephoto的话，需要上传livephoto里的图片和视频
                    // 展示的时候需要根据图片和视频生成livephoto
                    [model requestLivePhotoAssetsWithSuccess:^(NSURL * _Nullable imageURL, NSURL * _Nullable videoURL, BOOL isNetwork, HXPhotoModel * _Nullable model) {
                        // imageURL - LivePhoto里的照片封面地址
                        // videoURL - LivePhoto里的视频地址
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }
                // 也可以不用上面的判断和方法获取，自己根据 model.asset 这个PHAsset对象来获取想要的东西
//                PHAsset *asset = model.asset;
            }
            
        }else if (model.subType == HXPhotoModelMediaSubTypeVideo) {
            // 当前为视频
            //因为在获取的时候已经requestImageAfterFinishingSelection = YES  直接获取
            self.videoImage = model.thumbPhoto;
            success(model.videoURL.absoluteString);
        }
    
}
//MARK: --------------Photo View delegate------------
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {

    self.allPhotoArray = [NSArray arrayWithArray:photos];
    [self.assetArray removeAllObjects];

    self.isOriginal = isOriginal;
    if (self.allPhotoArray.count > 0) {
        self.hasPhoto = YES;
        
    }else {
        self.hasPhoto = NO;
    }
}

//MARK: -----------------net work--------------------
- (void)uploadImage {
    [SVProgressHUD dismiss];
    [RequestManager uploadImage:self.assetArray withSuccess:^(id  _Nullable response) {
        NSArray *data = response[@"data"];
        NSMutableArray *muArr = [NSMutableArray array];
        for (int i = 0; i < data.count; i ++) {
            NSString *url = data[i][@"url"];
            [muArr addObject:url];
        }
        self.imageUrlArray = muArr;
        NSLog(@"images==%@",data);
            [self getData];
    } withFail:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
    }];
}

//MARK: ---------获取图片或者视频的数据/文件地址------------
static int requestCount = 0;
- (void)postFiles {
    kWeakSelf(self);
    NSLog(@"总数 === %@",self.allPhotoArray);
    [self requestMediaSourceWith:self.allPhotoArray[requestCount] success:^(id  _Nullable response) {
       //成功 继续获取下一个
        //图片
        [weakself.assetArray insertObject:response atIndex:requestCount];
        if (requestCount == weakself.allPhotoArray.count-1) {
            //结束
            //开始上传
            [SVProgressHUD showWithStatus:@"图片上传中"];
            
            [weakself uploadImage];
            
        }
        else {
            [SVProgressHUD showWithStatus:@"图片上传中"];
            requestCount ++;
            [weakself postFiles];
        }
        
    }];
}

- (void)setReportID:(NSInteger)reportID{
    _reportID = reportID;
}

-(void)getData{
    if (self.imageUrlArray.count == 0){
        [self showText:@"请上传图片"];
    }else{
        NSLog(@"选中的图片 ==== %@",self.imageUrlArray);
        NSString *stringImageV = [self.imageUrlArray componentsJoinedByString:@","];
        
        [RequestManager report:[[UserInfoManager shared] getUserID] reportId:self.reportID reportType:self.type type:2 imageUrl:stringImageV message:self.yyTextView.text withSuccess:^(id  _Nullable response) {
            NSLog(@"投诉 ==== %@",response);
            if ([response[@"status"] integerValue] == 200) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
        
    }
    
}


- (void)photoViewDidCancel:(HXPhotoView *)photoView {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)photoViewDidAddCellClick:(HXPhotoView *)photoView {
    [self.view endEditing:YES];
}


- (YYTextView *)yyTextView {
    if (!_yyTextView) {
        _yyTextView = [[YYTextView alloc] init];
        _yyTextView.font = kFont_Medium(15);
        if (self.isJudge == 1) {
            _yyTextView.placeholderText = @"请输入举报原因";
        }else{
            _yyTextView.placeholderText = @"我要投诉~";
        }
        _yyTextView.textColor = UIColor.blackColor;
        _yyTextView.backgroundColor = UIColor.whiteColor;
        _yyTextView.placeholderTextColor = RGBA(153, 153, 153, 1);
    }
    return _yyTextView;
}
- (HXPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [[HXPhotoView alloc] initWithManager:self.photoManager scrollDirection:UICollectionViewScrollDirectionVertical];
        _photoView.delegate = self;
        _photoView.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 45);
//        _photoView.spacing = (KSW-30-kScaleSize(108)*2)/2;
        _photoView.spacing = 15;
        _photoView.outerCamera = NO;
        _photoView.addImageName = @"upload_sj";
        _photoView.deleteImageName = @"评价-删除";
        _photoView.lineCount = 3;
        _photoView.backgroundColor = UIColor.whiteColor;
        _photoView.previewStyle = HXPhotoViewPreViewShowStyleDark;
        _photoView.cellCustomProtocol = self;
    }
    return _photoView;
}
- (HXPhotoManager *)photoManager {
    if (!_photoManager) {
        _photoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        //黑色主题 视频或者图片 最多九张
        _photoManager.configuration.type = HXPhotoManagerSelectedTypePhoto;
        _photoManager.configuration.videoMaxNum = 1;
        _photoManager.configuration.selectTogether = NO;
        _photoManager.configuration.videoMaximumDuration = 15;
        _photoManager.configuration.videoMaximumSelectDuration = 15;
        _photoManager.configuration.photoMaxNum = 3;
        _photoManager.configuration.showOriginalBytes = YES;
        _photoManager.configuration.showOriginalBytesLoading = YES;
        _photoManager.configuration.requestImageAfterFinishingSelection = YES;
        _photoManager.configuration.exportVideoURLForHighestQuality = NO;
    }
    return _photoManager;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
    
}
// cell 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 350;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BackgroundReportingReasonsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BackgroundReportingReasonsTableViewCell"];
        if (!cell) {
            cell = [[BackgroundReportingReasonsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BackgroundReportingReasonsTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor clearColor];
    cell.delegate = self;
        return cell;
}

- (void)clickBackgroundReportingReasonsTableViewCell:(NSInteger)integer{
    self.type = (int)integer+1;
    NSLog(@"选择的原因 === %d",self.type);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
