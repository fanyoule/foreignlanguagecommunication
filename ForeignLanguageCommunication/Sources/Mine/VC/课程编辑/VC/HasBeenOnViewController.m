//
//  HasBeenOnViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//  已上架

#import "HasBeenOnViewController.h"
#import "CourseEditorCollectionViewCell.h"
#import "CourseDetailsViewController.h"// 课程详情

#import "CourseEditListModel.h"
@interface HasBeenOnViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong)UIView *backView;
@property (nonatomic, strong)UILabel *rulesLabel;

@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation HasBeenOnViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addTopRules];
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    [self.mainCollectionView registerClass:[CourseEditorCollectionViewCell class] forCellWithReuseIdentifier:@"CourseEditorCollectionViewCell"];
    
    // MARK: 下拉刷新
    self.mainCollectionView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
}
// MARK: 下拉刷新回调
-(void)loadNewData{
    [self getData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getData];
}
// MARK： 头部规则
-(void)addTopRules{
    [RequestManager getQueryBusinessConfigurationWithSuccess:^(id  _Nullable response) {
        NSLog(@"查询业务配置 === %@",response);
        CGFloat courseBrokerageExApple = [response[@"data"][@"courseBrokerageExApple"] floatValue];
        self.rulesLabel.text = [NSString stringWithFormat:@"每节课平台会收取%0.1lf%%平台费.",courseBrokerageExApple];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
    [self.view addSubview:self.backView];
    [self.view addSubview:self.rulesLabel];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@30);
        make.top.equalTo(self.view).offset(30);
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(15);
    }];
}

-(void)getData{
    kWeakSelf(self)
    [self.dataArray removeAllObjects];
    [RequestManager coachCourseWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"课程编辑 === %@",response);
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                CourseEditListModel *model = [CourseEditListModel mj_objectWithKeyValues:array[i]];
                if ([model.status isEqualToString:@"上架"]) {
                    [self.dataArray addObject:model];
                }
            }
           
        }
        [self.mainCollectionView reloadData];
        [weakself.mainCollectionView.mj_header endRefreshing];
        [weakself.mainCollectionView.mj_footer endRefreshing];
    } withFail:^(NSError * _Nullable error) {
        [weakself.mainCollectionView.mj_header endRefreshing];
        [weakself.mainCollectionView.mj_footer endRefreshing];
    }];
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(214, 228, 253, 1);
    }
    return _backView;
}
- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"每节课平台会收取2%平台费.";
        _rulesLabel.textColor = RGBA(52, 120, 245, 1);
        _rulesLabel.font = kFont_Medium(14);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseEditorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CourseEditorCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    
    cell.editorBtn.hidden = YES;
    cell.removeBtn.hidden = YES;
    
    CourseEditListModel *model = [[CourseEditListModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    
    [cell.shelvesBtn whenTapped:^{
        // 下架
        [RequestManager changeStatusWithId:model.ID status:2 withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self.dataArray removeObject:model];
            }else{
                [self showText:response[@"msg"]];
            }
            [self.mainCollectionView reloadData];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }];
    
    return cell;
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    return CGSizeMake(KSW-30, 163);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CourseEditListModel *model = [[CourseEditListModel alloc]init];
    model = self.dataArray[indexPath.row];
    CourseDetailsViewController *vc = [[CourseDetailsViewController alloc]init];
    vc.listModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
