//
//  CourseDetailsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/8.
//  课程详情

#import "CourseDetailsViewController.h"
#import "CoursePriceDetailsTableViewCell.h"
#import "TheCoachBusinessCardTableViewCell.h"
#import "CourseIntroductionTableViewCell.h"
#import "ShufflingFigureTableViewCell.h"// 轮播图
#import "CommentsTableViewCell.h"// 评论Cell

#import "CourseDetailsModel.h"
#import "TYTagView.h"

#import "CourseDetailsCommentsModel.h"//评论Model
#import "CourseDetailsCommentDetailsModel.h"// 评论详情Model
#import "PurchaseCourseOrderDetailsViewController.h"// 课程详情
@interface CourseDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, strong)CourseDetailsModel *detailsModel;

@property (nonatomic, strong)UIButton *selectedBtn;


/** 全部评论数量*/
@property (nonatomic, assign)NSInteger totalNumber;
@property (nonatomic, strong)NSMutableArray *totalArray;
/** 好评数量*/
@property (nonatomic, assign)NSInteger favorableNumber;
@property (nonatomic, strong)NSMutableArray *favorableArray;
/** 中评数量*/
@property (nonatomic, assign)NSInteger evaluationNumber;
@property (nonatomic, strong)NSMutableArray *evaluationArray;
/** 差评数量*/
@property (nonatomic, assign)NSInteger badNumber;
@property (nonatomic, strong)NSMutableArray *badArray;

/** */
@property (nonatomic, strong)NSMutableArray *dataArray;

/** 购买按钮*/
@property (nonatomic, strong)UIButton *purchaseBtn;
@end

@implementation CourseDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"课程详情";
    self.view.backgroundColor = RGBA(255, 255, 255, 1);
    
    
    [self.view addSubview:self.purchaseBtn];
    [self.purchaseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@44);
        make.bottom.equalTo(self.view.mas_bottom).offset(-TabbarSafeMargin-5);
    }];
    
    
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.purchaseBtn.mas_top).offset(0);
    }];
    // Do any additional setup after loading the view.
}

- (void)setListModel:(CourseEditListModel *)listModel{
    _listModel = listModel;
    [self getData:listModel.ID];
}
- (void)setCourseModel:(InstructorDetailsCourseModel *)courseModel{
    _courseModel = courseModel;
    [self getData:courseModel.ID];
}
- (void)setModel:(CourseModel *)model{
    _model = model;
    [self getData:model.ID];
}

-(void)getData:(NSInteger)ID{
    [RequestManager getCourseById:ID userId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"课程详情 === %@",response);
        NSDictionary *dic = response[@"data"];
        NSArray *array = response[@"data"][@"evaluation"];
        if ([response[@"status"] integerValue] == 200) {
            self.detailsModel = [CourseDetailsModel mj_objectWithKeyValues:dic];
            for (int i = 0; i< array.count; i++) {
                CourseDetailsCommentsModel *commentsModel = [CourseDetailsCommentsModel mj_objectWithKeyValues:array[i]];
                if ([commentsModel.type isEqualToString:@"全部"]) {
                    NSArray *evaluation = commentsModel.evaluation;
                    for (int i = 0; i < evaluation.count; i++) {
                        CourseDetailsCommentDetailsModel *model = [CourseDetailsCommentDetailsModel mj_objectWithKeyValues:evaluation[i]];
                        [self.totalArray addObject:model];
                    }
                    self.totalNumber = commentsModel.num;
                }else if ([commentsModel.type isEqualToString:@"好评"]){
                    NSArray *evaluation = commentsModel.evaluation;
                    for (int i = 0; i < evaluation.count; i++) {
                        CourseDetailsCommentDetailsModel *model = [CourseDetailsCommentDetailsModel mj_objectWithKeyValues:evaluation[i]];
                        [self.favorableArray addObject:model];
                    }
                    self.favorableNumber = commentsModel.num;
                }else if ([commentsModel.type isEqualToString:@"中评"]){
                    NSArray *evaluation = commentsModel.evaluation;
                    for (int i = 0; i < evaluation.count; i++) {
                        CourseDetailsCommentDetailsModel *model = [CourseDetailsCommentDetailsModel mj_objectWithKeyValues:evaluation[i]];
                        [self.evaluationArray addObject:model];
                    }
                    self.evaluationNumber = commentsModel.num;
                }else if ([commentsModel.type isEqualToString:@"差评"]){
                    NSArray *evaluation = commentsModel.evaluation;
                    for (int i = 0; i < evaluation.count; i++) {
                        CourseDetailsCommentDetailsModel *model = [CourseDetailsCommentDetailsModel mj_objectWithKeyValues:evaluation[i]];
                        [self.badArray addObject:model];
                    }
                    self.badNumber = commentsModel.num;
                }
            }
            self.dataArray = self.totalArray;
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
    
    }];
}
- (CourseDetailsModel *)detailsModel{
    if (!_detailsModel) {
        _detailsModel = [[CourseDetailsModel alloc]init];
    }
    return _detailsModel;
}
- (NSMutableArray *)totalArray{
    if (!_totalArray) {
        _totalArray = [[NSMutableArray alloc]init];
    }
    return _totalArray;
}
- (NSMutableArray *)favorableArray{
    if (!_favorableArray) {
        _favorableArray = [[NSMutableArray alloc]init];
    }
    return _favorableArray;
}
- (NSMutableArray *)evaluationArray{
    if (!_evaluationArray) {
        _evaluationArray = [[NSMutableArray alloc]init];
    }
    return _evaluationArray;
}
- (NSMutableArray *)badArray{
    if (!_badArray) {
        _badArray = [[NSMutableArray alloc]init];
    }
    return _badArray;
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 4;
    }else{
        return self.dataArray.count;
    }
    
    
}
- (void)collectionAction:(CourseDetailsModel *)model {
    int isCollect = model.isCollect == 1 ? 0 : 1;
    [RequestManager addCollectWithId:[UserInfoManager shared].getUserID typeId:model.ID type:2 status:isCollect withSuccess:^(id  _Nullable response) {
        [self getData:model.ID];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            ShufflingFigureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShufflingFigureTableViewCell"];
            if (!cell) {
                cell = [[ShufflingFigureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ShufflingFigureTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];

            cell.circleView.localizationImageNamesGroup = self.detailsModel.coursePic;
            return cell;
        }else if (indexPath.row == 1) {
            CoursePriceDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoursePriceDetailsTableViewCell"];
                if (!cell) {
                    cell = [[CoursePriceDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CoursePriceDetailsTableViewCell"];
                }
            cell.block = ^(CourseDetailsModel * _Nonnull model) {
                [self collectionAction:model];
            };
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                cell.backgroundColor = [UIColor whiteColor];
            
            cell.model = self.detailsModel;
            
                return cell;
        }else if (indexPath.row == 2) {
            TheCoachBusinessCardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheCoachBusinessCardTableViewCell"];
                if (!cell) {
                    cell = [[TheCoachBusinessCardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheCoachBusinessCardTableViewCell"];
                }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = RGBA(245, 245, 245, 1);
            cell.model = self.detailsModel;
                return cell;
        }else{

            CourseIntroductionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CourseIntroductionTableViewCell"];
                if (!cell) {
                    cell = [[CourseIntroductionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CourseIntroductionTableViewCell"];
                }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
            cell.model = self.detailsModel;
                return cell;
        }
        
    }else{
        CommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentsTableViewCell"];
            if (!cell) {
                cell = [[CommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommentsTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
        CourseDetailsCommentDetailsModel *model = [[CourseDetailsCommentDetailsModel alloc]init];
        model = self.dataArray[indexPath.row];
        cell.model = model;
            return cell;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        
        return nil;
    }else{
        UIView *view = UIView.new;
        view.backgroundColor = UIColor.whiteColor;
//        UIView *backView = UIView.new;
//        backView.backgroundColor = UIColor.whiteColor;
//        [view addSubview:backView];
//        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(view).offset(10);
//            make.left.right.bottom.equalTo(view);
//        }];
        UILabel *label = [[UILabel alloc]init];
        label.text = @"评价";
        label.textColor = RGBA(24, 24, 24, 1);
        label.font = kFont_Bold(15);
        label.textAlignment = 0;
        [view addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view).offset(19);
            make.left.equalTo(view).offset(15);
        }];
        
        
            
//        TYTagView *YYTagView = [[TYTagView alloc]init];
//        YYTagView.backgroundColor = UIColor.clearColor;
//        YYTagView.top = 5;
//        YYTagView.margin = 10;
//        YYTagView.tagHeight = 15;
//        [view addSubview:YYTagView];
//        [YYTagView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(label.mas_bottom).offset(5);
//            make.left.equalTo(label);
//            make.right.equalTo(view);
//        }];
        //全部
        NSString *all = [NSString stringWithFormat:@"全部%ld",self.totalNumber];
        //好评
        NSString *favorable = [NSString stringWithFormat:@"好评%ld",self.favorableNumber];
        //中评
        NSString *evaluation = [NSString stringWithFormat:@"中评%ld",self.evaluationNumber];
        //差评
        NSString *review = [NSString stringWithFormat:@"差评%ld",self.badNumber];

        NSMutableArray *array = [NSMutableArray new];
        [array addObject:all];
        [array addObject:favorable];
        [array addObject:evaluation];
        [array addObject:review];
//        YYTagView.items = array;
        
        
        for (int i = 0; i < array.count; i ++) {
            UIButton * btn = [[UIButton alloc]init];
            btn.tag = 800+i;
            [btn setTitle:array[i] forState:(UIControlStateNormal)];
            [btn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateNormal)];
            [btn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateSelected)];
            
            btn.backgroundColor = RGBA(238, 238, 238, 1);
            btn.titleLabel.font = kFont_Medium(12);
            
            btn.layer.cornerRadius = 6;
            btn.clipsToBounds = YES;
            [view addSubview:btn];
            
            [btn sizeToFit];
            CGFloat w = btn.bounds.size.width + 10;
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(label.mas_bottom).offset(15);
                make.left.equalTo(view).offset(i*w + i*10 + 15);
                make.width.equalTo(@(w));
                make.height.equalTo(@25);
            }];
            
            [btn addTarget:self action:@selector(clickComments:) forControlEvents:(UIControlEventTouchDown)];
            
        }
        

        
        return view;
    }


}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    if (section == 0) {
        return 0;
    }else{
        return 90;
    }
}

-(void)clickComments:(UIButton *)button{
//    [self.dataArray removeAllObjects];
    if (!self.selectedBtn) {
        self.selectedBtn = button;
    }else{
        
        if (self.selectedBtn == button) {
            return;
        }
        
        self.selectedBtn.selected = NO;
        
        
        self.selectedBtn.backgroundColor = RGBA(238, 238, 238, 1);
        self.selectedBtn.layer.borderColor = RGBA(238, 238, 238, 1).CGColor;
        self.selectedBtn.layer.borderWidth = 0.5;
    }
    
    
    button.backgroundColor = RGBA(214, 228, 253, 1);
    button.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
    button.layer.borderWidth = 0.5;
    self.selectedBtn = button;
    self.selectedBtn.selected = YES;
    
    
    if (button.tag == 800) {
        self.dataArray = self.totalArray;
        
    }else if (button.tag == 801){
        self.dataArray = self.favorableArray;
        
    }else if (button.tag == 802){
        self.dataArray = self.evaluationArray;
        
    }else if (button.tag == 803){
        self.dataArray = self.badArray;
        
    }
//    [self.mainTableView reloadData];
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
    [self.mainTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (UIButton *)purchaseBtn{
    if (!_purchaseBtn) {
        _purchaseBtn = [[UIButton alloc]init];
        [_purchaseBtn setTitle:@"立即购买" forState:(UIControlStateNormal)];
        [_purchaseBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _purchaseBtn.titleLabel.font = kFont_Bold(15);
        _purchaseBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _purchaseBtn.layer.cornerRadius = 6;
        _purchaseBtn.clipsToBounds = YES;
        [_purchaseBtn addTarget:self action:@selector(clickPurchaseBtn) forControlEvents:(UIControlEventTouchDown)];
    }
    return _purchaseBtn;
}
-(void)clickPurchaseBtn {
    if (![UserInfoManager shared].isLogin) {
        [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            if (index == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
            }
        }];
        return;
    }
    PurchaseCourseOrderDetailsViewController *vc = [[PurchaseCourseOrderDetailsViewController alloc]init];
    vc.model = self.detailsModel;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
