//
//  ComplaintsViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ComplaintsViewController : BaseViewController


/** 判断  1:举报  ，2:投诉*/
@property (nonatomic, assign)NSInteger isJudge;

/** 被举报的ID*/
@property (nonatomic, assign)NSInteger reportID;


@end

NS_ASSUME_NONNULL_END
