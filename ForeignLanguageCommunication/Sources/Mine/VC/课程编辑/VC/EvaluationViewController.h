//
//  EvaluationViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//

#import "BaseViewController.h"
#import "MyOfCourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface EvaluationViewController : BaseViewController
@property (nonatomic, strong)MyOfCourseModel *model;
@end

NS_ASSUME_NONNULL_END
