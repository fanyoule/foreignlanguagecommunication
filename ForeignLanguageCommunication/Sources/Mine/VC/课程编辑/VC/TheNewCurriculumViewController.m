//
//  TheNewCurriculumViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//  新建课程

#import "TheNewCurriculumViewController.h"
#import "TheNewCurriculumTableViewCell.h"
#import "PersonalProfileTableViewCell.h"
#import "AddCoursePhotosTableViewCell.h"
#import "NewCourseSelectionTableViewCell.h"

#import "CourseDetailsModel.h"

#import "MineDataPickerModel.h"

#import "MineDataPickerViewController.h"// 弹窗
@interface TheNewCurriculumViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UITextViewDelegate
>

@property (nonatomic, strong)CourseDetailsModel *detailsModel;

/** 名字*/
@property (nonatomic, strong)NSString *nameStr;
/** 适应人群拼接id*/
@property (nonatomic, strong)NSString *adaptIDString;
/** 教学内容id*/
@property (nonatomic, assign)NSInteger teachingInteger;
/** 价格*/
@property (nonatomic, assign)NSInteger priceInteger;
/** 时间*/
@property (nonatomic, assign)int timeInteger;
/** 简介*/
@property (nonatomic, strong)NSString *introductionStr;
/** 图片url*/
@property (nonatomic, strong)NSString *urlString;
@end

@implementation TheNewCurriculumViewController
- (CourseDetailsModel *)detailsModel{
    if (!_detailsModel) {
        _detailsModel = [[CourseDetailsModel  alloc]init];
    }
    return _detailsModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isJudge == 1) {
        self.navTitleString = @"新建课程";
    }else{
        self.navTitleString = @"编辑课程";
    }
    [self creatUI];
    
  
}
-(void)creatUI{
    [self addRightBtnWith:@"完成"];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
}
- (void)setModel:(CourseEditListModel *)model{
    _model = model;
    [RequestManager getCourseById:model.ID userId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"课程详情 === %@",response);
        NSDictionary *dic = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            self.detailsModel = [CourseDetailsModel mj_objectWithKeyValues:dic];
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return 190;
    }else if (indexPath.section == 2){
        NSArray * arr = [self.urlString componentsSeparatedByString:@","];
        if (arr.count>3) {
            return 236+90;
        }
        return 236;
    }
    return 45;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 5;
    }else if (section == 1) {
        return 1;
    }else{
        return 1;
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        PersonalProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalProfileTableViewCell"];
        if (!cell) {
            cell = [[PersonalProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PersonalProfileTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        if (self.isJudge == 1) {
            cell.promptLabel.text = @"描述相关课程简介~";
            
        }else{
            cell.promptLabel.hidden = YES;
            cell.textView.text = self.detailsModel.content;
        }
        
        cell.profileLabel.text = @"课程简介";
        cell.textView.delegate = self;
        cell.promptLabel.tag = 700;
        
        return cell;
    }else if (indexPath.section == 2){
        AddCoursePhotosTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddCoursePhotosTableViewCell"];
        if (!cell) {
            cell = [[AddCoursePhotosTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddCoursePhotosTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        if (IS_VALID_STRING(self.urlString)) {
            NSArray * arr = [self.urlString componentsSeparatedByString:@","];
            cell.coursePicArray = arr;
        }else{
            cell.coursePicArray = self.detailsModel.coursePic;
        }
        
        kWeakSelf(self)
        cell.uploadPhotos = ^(NSArray *urlString) {
            weakself.urlString = [urlString componentsJoinedByString:@","];
            [weakself.mainTableView reloadSection:2 withRowAnimation:(UITableViewRowAnimationNone)];
        };
        
        return cell;
    }else{
        kWeakSelf(self)
        if (indexPath.row == 1||indexPath.row == 2) {
            NewCourseSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewCourseSelectionTableViewCell"];
            if (!cell) {
                cell = [[NewCourseSelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NewCourseSelectionTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
            if (indexPath.row == 1) {
                if (self.isJudge == 1) {
                    
                }else{
                    NSMutableArray *array = [[NSMutableArray alloc]init];
                    for (int i = 0; i < self.detailsModel.applyCrowd.count; i++) {
                        MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:self.detailsModel.applyCrowd[i]];
                        [array addObject:model.name];
                    }
                    cell.label.text = [array componentsJoinedByString:@"、"];
                    cell.label.textColor = RGBA(24, 24, 24, 1);
                }
                [cell whenTapped:^{
                    MineDataPickerViewController *VC = [MineDataPickerViewController new];
                    VC.isJudge = 5;
                    VC.showSingle = NO;
                    VC.needShowColor = NO;
                    VC.titleString = @"适应人群";
                    HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                    popController.backgroundAlpha = 0.1;//背景色透明度
                    popController.animationDuration = 0.3;
                    popController.popPosition = 2;
                    popController.popType = 5;
                    popController.dismissType = 5;
                    popController.shouldDismissOnBackgroundTouch = YES;
                    [popController presentInViewController:self];
                    VC.confirmBlock = ^(NSArray *selectModelArray) {
                        NSMutableArray *arrayID = [NSMutableArray array];
                        NSMutableArray *array = [NSMutableArray array];
                        for (int i = 0; i < selectModelArray.count; i++) {
                            MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:selectModelArray[i]];
                            [array addObject:model.name];
                            [arrayID addObject:@(model.ID)];
                        }
                        weakself.adaptIDString = [arrayID componentsJoinedByString:@","];
                        cell.label.text = [array componentsJoinedByString:@"、"];
                        cell.label.textColor = RGBA(24, 24, 24, 1);
                        
                    };
                }];
                
                
            }else if (indexPath.row == 2) {
                cell.titleImageV.image = [UIImage imageNamed:@"新建课程-内容"];
                
                if (self.isJudge == 1) {
                    cell.label.text = @"请选择教学内容标签，单选";
                }else{
                    if (self.detailsModel.teachingContent == nil) {
                        cell.label.text = @"请选择教学内容标签，单选";
                    }else{
                        cell.label.text = self.detailsModel.teachingContent[@"name"];
                        cell.label.textColor = RGBA(24, 24, 24, 1);
                    }
                }
                
                [cell whenTapped:^{
                    MineDataPickerViewController *VC = [MineDataPickerViewController new];
                    VC.isJudge = 4;
                    VC.showSingle = YES;
                    VC.needShowColor = NO;
                    VC.titleString = @"教学内容";
                    HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                    popController.backgroundAlpha = 0.1;//背景色透明度
                    popController.animationDuration = 0.3;
                    popController.popPosition = 2;
                    popController.popType = 5;
                    popController.dismissType = 5;
                    popController.shouldDismissOnBackgroundTouch = YES;
                    [popController presentInViewController:self];
                    VC.confirmBlock = ^(NSArray *selectModelArray) {
                        NSInteger ID = 0;
                        NSString *nameString = nil;
                        for (int i = 0; i < selectModelArray.count; i++) {
                            MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:selectModelArray[i]];
                            nameString = model.name;
                            ID = model.ID;
                        }
                        weakself.teachingInteger = ID;
                        cell.label.text = nameString;
                        cell.label.textColor = RGBA(24, 24, 24, 1);
                        
                    };
                }];
                
                
            }
            
            return cell;
        }
        
    TheNewCurriculumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheNewCurriculumTableViewCell"];
            if (!cell) {
                cell = [[TheNewCurriculumTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheNewCurriculumTableViewCell"];
            }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.row == 0) {
            cell.isJudge = 1;
            if (self.isJudge == 1) {
                
            }else{
                cell.textField.text = self.detailsModel.name;
            }
            cell.mobilePhone = ^(NSString *string) {
                self.nameStr = string;
            };
        }else if (indexPath.row == 3){
            if (self.isJudge == 1) {
                cell.titleImageV.image = [UIImage imageNamed:@"新建课程-价格"];
                cell.textField.placeholder = @"请填写单节课程价格";
            }else{
                cell.textField.text = [NSString stringWithFormat:@"%ld元",self.detailsModel.price];
                cell.ELabel.hidden = NO;
                cell.ELabel.text = [NSString stringWithFormat:@"(%ldE币)",self.detailsModel.price*100];
            }
            cell.mobilePhone = ^(NSString *string) {
                self.priceInteger = [string integerValue];
            };
        }else if (indexPath.row == 4){
            cell.isJudge = 1;
            if (self.isJudge == 1) {
                cell.titleImageV.image = [UIImage imageNamed:@"新建课程-时间"];
                cell.textField.placeholder = @"请填写单节课程时间";
            }else{
                cell.textField.text = [NSString stringWithFormat:@"%ld分钟",self.detailsModel.time];
            }
            cell.mobilePhone = ^(NSString *string) {
                self.timeInteger = [string intValue];
            };
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        if (indexPath.row == 1) {
//
//        }
//        if (indexPath.row == 2) {
//
//        }
//    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
        UIView *view = UIView.new;
        view.backgroundColor = UIColor.clearColor;
        return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 10;
    }else{
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
        UIView *view = UIView.new;
        view.backgroundColor = UIColor.clearColor;
        return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return 10;
    }else{
        return 10;
    }
}
- (void)textViewDidChange:(UITextView *)textView{
    UILabel *label = (UILabel *)[self.view viewWithTag:700];
    label.hidden = YES;
    
    //字数限制操作

    if (textView.text.length >= 300) {
    
        textView.text = [textView.text substringToIndex:10];
        
    }
    
    //取消按钮点击权限，并显示提示文字

    if (textView.text.length <= 0) {
        
        label.hidden = NO;
    
    }
    self.introductionStr = textView.text;

}

-(void)rightClick:(UIButton *)sender{
    if (self.isJudge == 1) {
        // 新建
        if (self.nameStr == nil) {
            [self showText:@"请填写课程名称"];
        }else if (self.adaptIDString == nil){
            [self showText:@"请选择适应人群"];
        }else if (self.teachingInteger == 0){
            [self showText:@"请选择教学内容"];
        }else if (self.priceInteger == 0){
            [self showText:@"请填写单节课程价格"];
        }else if (self.timeInteger == 0){
            [self showText:@"请填写单节课程时间"];
        }else if (self.introductionStr == nil){
            [self showText:@"请描述相关课程简介"];
        }else if (self.urlString == nil){
            [self showText:@"请添加课程图片"];
        }else{
            [RequestManager addCourseWithUserId:[[UserInfoManager shared] getUserID] applyCrowd:self.adaptIDString content:self.introductionStr name:self.nameStr pics:self.urlString time:self.timeInteger teachingContent:self.teachingInteger price:self.priceInteger withSuccess:^(id  _Nullable response) {
                if ([response[@"status"] integerValue] == 200) {
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [self showText:response[@"msg"]];
                }
            } withFail:^(NSError * _Nullable error) {
                
            }];
        }
        
        
        
        
    }else{
        // 编辑
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@(self.detailsModel.ID) forKey:@"id"];
        if (self.nameStr == nil) {
            
        }else{
            [dic setObject:self.nameStr forKey:@"name"];
        }
        
        if (self.adaptIDString == nil){
            
        }else{
            [dic setObject:self.adaptIDString forKey:@"applyCrowd"];
        }
        
        if (self.teachingInteger == 0){
            
        }else{
            [dic setObject:@(self.teachingInteger) forKey:@"teachingContent"];
        }
        
        if (self.priceInteger == 0){
            
        }else{
            [dic setObject:@(self.priceInteger) forKey:@"price"];
        }
        
        if (self.timeInteger == 0){
            
        }else{
            [dic setObject:@(self.timeInteger) forKey:@"time"];
        }
        
        if (self.introductionStr == nil){
            
        }else{
            [dic setObject:self.introductionStr forKey:@"content"];
        }
        
        if (self.urlString == nil) {
            NSString *string = [self.detailsModel.coursePic componentsJoinedByString:@","];
            [dic setObject:string forKey:@"pics"];
        }else{
            [dic setObject:self.urlString forKey:@"pics"];
        }
        if (self.nameStr == nil && self.adaptIDString == nil && self.teachingInteger == 0 && self.priceInteger == 0 && self.timeInteger == 0 && self.introductionStr == nil) {
            [self showText:@"请修改课程信息"];
            return;
        }
        
        NSLog(@"传输的字典 === %@",dic);
        
        [RequestManager updateCourseWithModifyThe:dic withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"更改成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [self showText:response[@"msg"]];
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
    
}
    


@end
