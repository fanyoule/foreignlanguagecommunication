//
//  CourseDetailsViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/8.
//

#import "BaseViewController.h"
#import "CourseEditListModel.h"
#import "InstructorDetailsCourseModel.h"
#import "CourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CourseDetailsViewController : BaseViewController
 
@property (nonatomic, strong)CourseEditListModel *listModel;

@property (nonatomic, strong)InstructorDetailsCourseModel *courseModel;

@property (nonatomic, strong)CourseModel *model;
@end

NS_ASSUME_NONNULL_END
