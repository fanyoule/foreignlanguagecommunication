//
//  HasBeenOffTheShelvesViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//  已下架

#import "HasBeenOffTheShelvesViewController.h"
#import "CourseEditorCollectionViewCell.h"
#import "CourseDetailsViewController.h"// 课程详情
#import "CourseEditListModel.h"

#import "TheNewCurriculumViewController.h"// 编辑
@interface HasBeenOffTheShelvesViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation HasBeenOffTheShelvesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(30);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    [self.mainCollectionView registerClass:[CourseEditorCollectionViewCell class] forCellWithReuseIdentifier:@"CourseEditorCollectionViewCell"];
    
    // MARK: 下拉刷新
    self.mainCollectionView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
}
// MARK: 下拉刷新回调
-(void)loadNewData{
    [self getData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getData];
}

-(void)getData{
    kWeakSelf(self)
    [self.dataArray removeAllObjects];
    [RequestManager coachCourseWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"课程编辑 === %@",response);
        NSArray *array = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                CourseEditListModel *model = [CourseEditListModel mj_objectWithKeyValues:array[i]];
                if ([model.status isEqualToString:@"下架"]) {
                    [self.dataArray addObject:model];
                }
            }
           
        }
        [self.mainCollectionView reloadData];
        [weakself.mainCollectionView.mj_header endRefreshing];
        [weakself.mainCollectionView.mj_footer endRefreshing];
    } withFail:^(NSError * _Nullable error) {
        [weakself.mainCollectionView.mj_header endRefreshing];
        [weakself.mainCollectionView.mj_footer endRefreshing];
        
    }];
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CourseEditListModel *model = [[CourseEditListModel alloc]init];
    model = self.dataArray[indexPath.row];
    CourseDetailsViewController *vc = [[CourseDetailsViewController alloc]init];
    vc.listModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseEditorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CourseEditorCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    
    [cell.shelvesBtn setTitle:@"上架" forState:(UIControlStateNormal)];
    CourseEditListModel *model = [[CourseEditListModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    
    [cell.shelvesBtn whenTapped:^{
        // 上架
        [RequestManager changeStatusWithId:model.ID status:1 withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self.dataArray removeObject:model];
            }else{
                [self showText:response[@"msg"]];
            }
            [self.mainCollectionView reloadData];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }];
    [cell.removeBtn whenTapped:^{
        // 删除
        [RequestManager deleteCourseById:model.ID userId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self.dataArray removeObject:model];
            }else{
                [self showText:response[@"msg"]];
            }
            [self.mainCollectionView reloadData];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }];
    [cell.editorBtn whenTapped:^{
        TheNewCurriculumViewController *vc = [[TheNewCurriculumViewController alloc]init];
        vc.model = model;
        vc.isJudge = 2;
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
    
    return cell;
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    return CGSizeMake(KSW-30, 163);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}

//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//
//    UIView *view = UIView.new;
//    view.backgroundColor = UIColor.clearColor;
//    UILabel *label = [[UILabel alloc]init];
//    label.text = @"没有更多了哦";
//    label.textColor = RGBA(153, 153, 153, 1);
//    label.font = kFont_Medium(12);
//    label.textAlignment = 1;
//    [view addSubview:label];
//    [label mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.center.equalTo(view);
//    }];
//    return view;
//
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//
//    return 30;
//
//}

@end
