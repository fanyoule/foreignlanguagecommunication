//
//  EvaluationViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/12.
//  评价

#import "EvaluationViewController.h"
#import "ThePictureCollectionViewCell.h"
@interface EvaluationViewController ()<UITextViewDelegate,HXPhotoViewDelegate>
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 分割线*/
@property (nonatomic, strong)UIView *lineView;
/** 输入框*/
@property (nonatomic, strong)UITextView *textView;
/** 提示语*/
@property (nonatomic, strong)UILabel *placeHolder;

/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;

@property (nonatomic, strong)UIButton *butt;
/**什么评论*/
@property (nonatomic, assign)int comments;

/**图片管理器*/
@property (nonatomic, strong) HXPhotoManager *photoManager;
/**选择图片View*/
@property (nonatomic, strong) HXPhotoView *photoView;

/**图片URL集合*/
@property (nonatomic, strong) NSArray *imageUrlArray;

/**存储图片/视频 model*/
@property (nonatomic, copy) NSArray *allPhotoArray;
/**图片Image*/
@property (nonatomic, strong) NSMutableArray *assetArray;
/**是否原图*/
@property (nonatomic) BOOL isOriginal;
//MARK: 判断当前是否可以点击发布按钮
//发布条件 文字/图片/视频/语音/  音频 audioPath判断    tagModel 判断
@property (nonatomic) BOOL hasText;
@property (nonatomic) BOOL hasPhoto;
@end

@implementation EvaluationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navTitleString = @"评价";
    [self addRightBtnWith:@"提交"];
    
    self.navView.backgroundColor = UIColor.whiteColor;
    
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    [self addChooseEvaluation];
    
    
    self.photoView.hidden = YES;
    self.rulesLabel.hidden = YES;
    
}

-(void)addChooseEvaluation{
    
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.lineView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom).offset(15);
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@390);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).offset(20);
        make.left.equalTo(self.backView).offset(10);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(15);
        make.left.equalTo(self.backView).offset(15);
        make.right.equalTo(self.backView).offset(-15);
        make.height.equalTo(@0.5);
    }];
    [self.backView layoutIfNeeded];
    [self.backView layoutSubviews];
    
    CGFloat w = (self.backView.bounds.size.width - 45)/3;
    
    NSArray *titleArray = @[@"差评",@"中评",@"好评"];
    NSArray *imageVArray = @[@"icon--差评--未选中",@"icon--中评--未选中",@"icon--好评--未选中"];
    
    
    for (int i = 0; i < 3; i++) {
        
        UIButton *btn = [[UIButton alloc]init];
        btn.backgroundColor = RGBA(245, 245, 245, 1);
        btn.layer.cornerRadius = 6;
        btn.clipsToBounds = YES;
        btn.tag = 600+i;
        [btn whenTapped:^{
            [self clickComments:btn number:i];
        }];
        [self.backView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.lineView.mas_bottom).offset(15);
            make.left.equalTo(self.backView).offset(i*w + i*12 +10);
            make.width.equalTo(@(w));
            make.height.equalTo(@35);
        }];
        
        UIImageView *imageV = [[UIImageView alloc]init];
        imageV.tag = 500+i;
        imageV.image = [UIImage imageNamed:imageVArray[i]];
        [btn addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(btn);
            make.right.equalTo(btn.mas_centerX).offset(-8);
        }];
        
        UILabel *label = [[UILabel alloc]init];
        label.tag = 400+i;
        label.text = titleArray[i];
        label.textColor = RGBA(153, 153, 153, 1);
        label.font = kFont_Medium(14);
        label.textAlignment = 0;
        [btn addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(btn);
            make.left.equalTo(btn.mas_centerX).offset(0);
        }];
         
    }
    
    [self.backView addSubview:self.textView];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(70);
        make.left.equalTo(self.backView).offset(15);
        make.right.equalTo(self.backView).offset(-15);
        make.height.equalTo(@90);
    }];
    [self.textView addSubview:self.placeHolder];
    [self.placeHolder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView).offset(8);
        make.left.equalTo(self.textView).offset(5);
    }];
    
    [self.backView addSubview:self.rulesLabel];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.backView).offset(-20);
        make.left.equalTo(self.backView).offset(15);
    }];
    
    [self.backView addSubview:self.photoView];
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(5);
        make.left.right.equalTo(self.backView);
        make.height.equalTo(@(kScaleSize(110)));
    }];
    
}


-(void)clickComments:(UIButton *)button number:(int)i{
    
    NSArray *arrayImageV = @[@"icon--差评--选中",@"icon--中评--选中",@"icon--好评--选中"];
    NSArray *arrayImageV2 = @[@"icon--差评--未选中",@"icon--中评--未选中",@"icon--好评--未选中"];
    
    if (!self.butt) {
        self.butt = button;
    }else{
        
        if (self.butt == button) {
            return;
        }
        
        self.butt.selected = NO;
        NSInteger index = self.butt.tag - 600;
        UIImageView *imageV = (UIImageView *)[self.view viewWithTag:(index+500)];
        imageV.image = kImage(arrayImageV2[i]);
        
        UILabel *label = (UILabel *)[self.view viewWithTag:(index+400)];
        label.textColor = RGBA(153, 153, 153, 1);
        self.butt.backgroundColor = RGBA(245, 245, 245, 1);
    }
    UIImageView *currentImageV = (UIImageView *)[self.view viewWithTag:(i+500)];
    currentImageV.image = kImage(arrayImageV[i]) ;
    
    UILabel *currentLabel = (UILabel *)[self.view viewWithTag:(i+400)];
    currentLabel.textColor = RGBA(52, 120, 245, 1);
    button.backgroundColor = RGBA(52, 120, 245, 0.5);
    
    self.butt = button;
    self.butt.selected = YES;
    
    self.comments = i+1;
    
}

-(void)rightClick:(UIButton *)sender{
    if (self.textView.text.length <= 0) {
        [self showText:@"请输入评论内容"];
    }else if (self.comments == 0){
        [self showText:@"请选择评价"];
    }else{
        [RequestManager addEvaluationWithId:[[UserInfoManager shared] getUserID] courseId:self.model.ID content:self.textView.text evaluation:self.comments withSuccess:^(id  _Nullable response) {
            NSLog(@"评价成功 === %@",response);
            [self.navigationController popViewControllerAnimated:YES];
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"评价失败 === %@",error);
        }];
    }
    
}




- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.cornerRadius = 6;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"您对课程满意吗？";
        _titleLabel.textColor = RGBA(34, 34, 34, 1);
        _titleLabel.font = kFont_Bold(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}

- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.delegate = self;
        _textView.font = kFont_Medium(15);
    }
    return _textView;
}
-(UILabel *)placeHolder{
    if (!_placeHolder) {
        _placeHolder = [[UILabel alloc]init];
        _placeHolder.text = @"分享您的体验~";
        _placeHolder.textColor = rgba(153, 153, 153, 1);
        _placeHolder.font = kFont_Medium(15);
        _placeHolder.textAlignment = 0;
    }
    return _placeHolder;
}

- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"仅可上传3张照片";
        _rulesLabel.textColor = RGBA(153, 153, 153, 1);
        _rulesLabel.font = kFont_Medium(12);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}

- (void)textViewDidChange:(UITextView *)textView{

    self.placeHolder.hidden = YES;
    
    //字数限制操作

    if (textView.text.length >= 300) {
    
        textView.text = [textView.text substringToIndex:300];
        
    }
    
    //取消按钮点击权限，并显示提示文字

    if (textView.text.length <= 0) {
        
        self.placeHolder.hidden = NO;
    
    }

}

- (HXPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [[HXPhotoView alloc] initWithManager:self.photoManager scrollDirection:UICollectionViewScrollDirectionVertical];
        _photoView.delegate = self;
        _photoView.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
//        _photoView.spacing = (KSW-30-kScaleSize(108)*2)/2;
        _photoView.spacing = 15;
        _photoView.outerCamera = NO;
        _photoView.addImageName = @"upload_sj";
        _photoView.lineCount = 3;
        _photoView.backgroundColor = UIColor.whiteColor;
        _photoView.previewStyle = HXPhotoViewPreViewShowStyleDark;
        _photoView.layer.cornerRadius = 12;
        _photoView.clipsToBounds = YES;
    }
    return _photoView;
}
- (HXPhotoManager *)photoManager {
    if (!_photoManager) {
        _photoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        //黑色主题 视频或者图片 最多九张
        _photoManager.configuration.type = HXConfigurationTypeWXChat;
        _photoManager.configuration.videoMaxNum = 1;
        _photoManager.configuration.selectTogether = NO;
        _photoManager.configuration.videoMaximumDuration = 15;
        _photoManager.configuration.videoMaximumSelectDuration = 15;
        _photoManager.configuration.photoMaxNum = 3;
        _photoManager.configuration.showOriginalBytes = YES;
        _photoManager.configuration.showOriginalBytesLoading = YES;
        _photoManager.configuration.requestImageAfterFinishingSelection = YES;
        _photoManager.configuration.exportVideoURLForHighestQuality = NO;
    }
    return _photoManager;
}

#pragma mark -------

- (void)requestMediaSourceWith:(HXPhotoModel *)model success:(HttpResponseSuccessBlock)success{
    // 如果将_manager.configuration.requestImageAfterFinishingSelection 设为YES，
    // 那么在选择完成的时候就会获取图片和视频地址
    // 如果选中了原图那么获取图片时就是原图
    // 获取视频时如果设置 exportVideoURLForHighestQuality 为YES，则会去获取高等质量的视频。其他情况为中等质量的视频
    // 个人建议不在选择完成的时候去获取，因为每次选择完都会去获取。获取过程中可能会耗时过长
    // 可以在要上传的时候再去获取
        // 数组里装的是所有类型的资源，需要判断
        // 先判断资源类型
        if (model.subType == HXPhotoModelMediaSubTypePhoto) {
            // 当前为图片
            if (model.photoEdit) {
                // 如果有编辑数据，则说明这张图篇被编辑过了
                // 需要这样才能获取到编辑之后的图片
                UIImage *image = model.photoEdit.editPreviewImage;
                success(image);
            }
            // 再判断具体类型
            else {
                // 到这里就是手机相册里的图片了 model.asset PHAsset对象是有值的
                // 如果需要上传 Gif 或者 LivePhoto 需要具体判断
                if (model.type == HXPhotoModelMediaTypePhoto) {
                    // 普通的照片，如果不可以查看和livePhoto的时候，这就也可能是GIF或者LivePhoto了，
                    // 如果你的项目不支持动图那就不要取NSData或URL，因为如果本质是动图的话还是会变成动图传上去
                    // 这样判断是不是GIF model.photoFormat == HXPhotoModelFormatGIF
                    
                    // 如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.previewPhoto 或者 model.thumbPhoto 在选择完成时候已经获取并且赋值了
                    // 获取image
                    // size 就是获取图片的质量大小，原图的话就是 PHImageManagerMaximumSize，其他质量可设置size来获取
                    CGSize size;
                    if (self.isOriginal) {
                        size = PHImageManagerMaximumSize;
                    }else {
                        size = CGSizeMake(model.imageSize.width * 0.5, model.imageSize.height * 0.5);
                    }
                    
                    
                    [model requestPreviewImageWithSize:size startRequestICloud:^(PHImageRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        // 如果图片是在iCloud上的话会先走这个方法再去下载
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        // iCloud的下载进度
                    } success:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        // image
                        success(image);
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }else if (model.type == HXPhotoModelMediaTypePhotoGif) {
                    // 动图，如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.imageURL。因为在选择完成的时候已经获取了不用再去获取
//                    model.imageURL;
                    // 上传动图时，不要直接拿image上传哦。可以获取url或者data上传
                    // 获取data
                    NSLog(@"modelURL==%@",model.imageURL);
                    [model requestImageURLStartRequestICloud:^(PHContentEditingInputRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        
                    } success:^(NSURL * _Nullable imageURL, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        NSLog(@"URL==%@",imageURL);
                        
                        success(imageURL);
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        
                    }];
                }else if (model.type == HXPhotoModelMediaTypeLivePhoto) {
                    // LivePhoto，requestImageAfterFinishingSelection = YES 时没有处理livephoto，需要自己处理
                    // 如果需要上传livephoto的话，需要上传livephoto里的图片和视频
                    // 展示的时候需要根据图片和视频生成livephoto
                    [model requestLivePhotoAssetsWithSuccess:^(NSURL * _Nullable imageURL, NSURL * _Nullable videoURL, BOOL isNetwork, HXPhotoModel * _Nullable model) {
                        // imageURL - LivePhoto里的照片封面地址
                        // videoURL - LivePhoto里的视频地址
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }
                // 也可以不用上面的判断和方法获取，自己根据 model.asset 这个PHAsset对象来获取想要的东西
//                PHAsset *asset = model.asset;
            }
            
        }
}

- (NSMutableArray *)assetArray{
    if (!_assetArray) {
        _assetArray = [[NSMutableArray alloc]init];
    }
    return _assetArray;
}
//MARK: --------------Photo View delegate------------
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {

    self.allPhotoArray = [NSArray arrayWithArray:photos];
    [self.assetArray removeAllObjects];

    self.isOriginal = isOriginal;
    if (self.allPhotoArray.count > 0) {
        self.hasPhoto = YES;
        
    }else {
        self.hasPhoto = NO;
    }
}

//MARK: -----------------net work--------------------
- (void)uploadImage {
    [RequestManager uploadImage:self.assetArray withSuccess:^(id  _Nullable response) {
        NSArray *data = response[@"data"];
        NSMutableArray *muArr = [NSMutableArray array];
        for (int i = 0; i < data.count; i ++) {
            NSString *url = data[i][@"url"];
            [muArr addObject:url];
        }
        self.imageUrlArray = muArr;
        NSLog(@"images==%@",data);
            [self getData];
    } withFail:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
    }];
}

//MARK: ---------获取图片或者视频的数据/文件地址------------
static int requestCount = 0;
- (void)postFiles {
    kWeakSelf(self);
    NSLog(@"总数 === %@",self.allPhotoArray);
    [self requestMediaSourceWith:self.allPhotoArray[requestCount] success:^(id  _Nullable response) {
       //成功 继续获取下一个
        //图片
        [weakself.assetArray insertObject:response atIndex:requestCount];
        if (requestCount == weakself.allPhotoArray.count-1) {
            //结束
            //开始上传
//            [SVProgressHUD showWithStatus:@"图片上传中"];
            [SVProgressHUD dismiss];
            [weakself uploadImage];
            
        }
        else {
            [SVProgressHUD showWithStatus:@"图片上传中"];
            requestCount ++;
            [weakself postFiles];
        }
        
    }];
}
- (void)setModel:(MyOfCourseModel *)model{
    _model = model;
}

-(void)getData{
    if (self.imageUrlArray.count == 0){
        [self showText:@"请上传图片"];
    }else{
        NSLog(@"选中的图片 ==== %@",self.imageUrlArray);
        NSString *string = [self.imageUrlArray componentsJoinedByString:@","];
        
    }
    
}


- (void)photoViewDidCancel:(HXPhotoView *)photoView {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)photoViewDidAddCellClick:(HXPhotoView *)photoView {
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
