//
//  TheNewCurriculumViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/5.
//

#import "BaseViewController.h"
#import "CourseEditListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TheNewCurriculumViewController : BaseViewController
/** 判断  1：新建 ， 2：编辑*/
@property (nonatomic, assign)NSInteger isJudge;

@property (nonatomic, strong)CourseEditListModel *model;
@end

NS_ASSUME_NONNULL_END
