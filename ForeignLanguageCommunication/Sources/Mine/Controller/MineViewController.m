//
//  MineViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/16.
//

#import "MineViewController.h"
#import "MineHeadView.h"
#import "MyPageTableViewCell.h"
#import "MyVIPOpenTableViewCell.h"


#import "VIPCenterViewController.h"// 会员中心
#import "MyPersonalInformationVC.h"//个人资料
#import "SetUpViewController.h"// 我的设置
#import "MyCollectionViewController.h"// 我的收藏
#import "MyAttentionViewController.h"// 我的关注
#import "MyFanViewController.h" // 我的粉丝
#import "MyCourseViewController.h"// 我的课程
#import "MyWorkViewController.h"// 我的作品
#import "CoachCertificationViewController.h"// 我要当教练
#import "CourseEditorViewController.h"// 课程编辑
#import "CourseManagementViewController.h"// 课程管理
#import "MyWalletViewController.h"// 我的钱包
#import "AppleWalletViewController.h"
#import "FriendInvitationViewController.h"// 我的邀请码
#import "WantTheAgentViewController.h"// 我要成为代理
#import "MyProxyDataViewController.h"// 我的代理数据
#import "FunctionGuideViewController.h"// 功能指南
#import "FeedbackViewController.h"// 问题反馈
#import "PayThePopupWindow.h"// 支付弹窗
#import <HWPopController/HWPop.h>
#import "ImprovePersonalDataViewController.h"// 完善资料
#import "MyPageModel.h"
#import "InstructorVerificationPriorCertificationModel.h"
/** 登录界面*/
#import "LoginVerifyViewController.h"
#import "CustomNavViewController.h"


#import "TheAgentInformationModel.h"
@interface MineViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong)UITableView *tableV;
@property (nonatomic, strong) UIButton *navRightBtn;
@property (nonatomic, strong) MineHeadView *headView;//头视图
@property (nonatomic, strong) NSArray *onePartitionArray;
@property (nonatomic, strong) NSArray *twoPartitionArray;
@property (nonatomic, strong) NSArray *threePartitionArray;
/** 判断是否需要登录 */
@property (nonatomic,assign) NSInteger  isLogin;

@property (nonatomic, strong)MyPageModel *myModel;


@end

@implementation MineViewController

-(UITableView *)tableV{
    if (!_tableV) {
        
        _tableV = [[UITableView alloc]initWithFrame:CGRectMake( 0, 0 , APPSIZE.width, ScreenHeight - 49 - TabbarSafeMargin) style:UITableViewStyleGrouped];
//        _tableV.backgroundColor = [UIColor blueColor];
//        _tableView.backgroundColor = [UIColor redColor];
        _tableV.delegate = self;
        _tableV.dataSource = self;
//        _tableV.allowsSelection = NO;
        _tableV.separatorStyle = UITableViewCellSelectionStyleNone;
        
        if (@available(iOS 11.0, *)) {
            _tableV.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else{
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    return _tableV;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.onePartitionArray = [[NSArray alloc]init];
    self.twoPartitionArray = [[NSArray alloc]init];
    self.threePartitionArray = [[NSArray alloc]init];
    
    
    self.twoPartitionArray = @[@"我的钱包",@"邀请好友",@"我要成为代理"];
    self.threePartitionArray = @[@"功能指南",@"问题反馈",@"设置"];
    // Do any additional setup after loading the view.
    
    if (@available(iOS 13.0, *)) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];//状态栏样式
       } else {
          //
           // Fallback on earlier versions
       }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataNotifications:) name:@"RefreshMyPageForDataNotifications" object:nil];

    
    
    [self getData];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    // 返回你所需要的状态栏样式
    return UIStatusBarStyleLightContent;
}
-(void)viewDidDisappear:(BOOL)animated{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

#pragma mark -create subViews-
//自定义的导航栏
- (void)setUpNavView{
    //隐藏系统导航栏 设置自定义的透明导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}
-(void)dataNotifications:(NSNotification *)notice{
    [self getData];
}

- (MyPageModel *)myModel{
    if (!_myModel) {
        _myModel = [[MyPageModel alloc]init];
    }
    return _myModel;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //每次进入，隐藏系统导航栏，以防其他控制器修改了这个状态
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self setUpSubViews];
    [self getData];
}
// MARK: 我的页面
-(void)getData{
    NSLog(@"用户ID ==== %ld",[[UserInfoManager shared] getUserID]);
    [RequestManager myPage:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"我的页面 === %@",response);
        if ([response[@"status"] integerValue] == 200) {
            self.myModel = [
                            MyPageModel mj_objectWithKeyValues:response[@"data"]];
        }else{
            [self showText:response[@"msg"]];
        }
        [self setUpSubViews];
        [self.tableV reloadData];
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"我的页面失败 === %@",error);
    }];
    
    // MARK: 教练认证前验证
    [RequestManager coachVerifyWithId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"是否是教练 === %@",response);
        if (response[@"data"] == nil) {
            self.onePartitionArray = @[@"我的课程",@"我的作品",@"我要当教练"];
        }else{
            self.onePartitionArray = @[@"我的课程",@"我的作品",@"我要当教练",@"课程编辑",@"课程管理"];
//            NSDictionary *dic = response[@"data"];
//            if ([response[@"status"] integerValue] == 200) {
//                self.certificationModel = [InstructorVerificationPriorCertificationModel mj_objectWithKeyValues:dic];
//            }
        }
        [self.tableV reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
//    [RequestManager]
    
}

//- (InstructorVerificationPriorCertificationModel *)certificationModel{
//    if (!_certificationModel) {
//        _certificationModel = [[InstructorVerificationPriorCertificationModel alloc]init];
//    }
//    return _certificationModel;
//}

- (void)setUpSubViews {
    //tableview
    [self.view addSubview:self.tableV];
    //table view 的头视图
    self.headView = [[MineHeadView alloc] init];
    self.headView.model = self.myModel;
    
    kWeakSelf(self);
    [self.headView whenTapped:^{
        if ([UserInfoManager shared].isLogin == NO) {
            // 登录弹窗
            [weakself loginWindowPops];
        }else{
            ImprovePersonalDataViewController *vc = [[ImprovePersonalDataViewController alloc]init];
            [weakself.navigationController pushViewController:vc animated:YES];
        }
    }];
    self.headView.gotoInfoBlock = ^{
        if ([UserInfoManager shared].isLogin == NO) {
            // 登录弹窗
            [weakself loginWindowPops];
        }else{
            // 个人中心
            MyPersonalInformationVC *VC = [[MyPersonalInformationVC alloc]init];
            VC.ID = [[UserInfoManager shared] getUserID];
            [weakself.navigationController pushViewController:VC animated:YES];
        }
    };
    self.headView.goToNumberInfoBlock = ^(NSInteger index) {
        if ([UserInfoManager shared].isLogin == NO) {
            // 登录弹窗
            [weakself loginWindowPops];
        }else{
            // 收藏夹 关注  粉丝
            [weakself goToNumberInfo:index];
        }
    };
    UIButton *setUpBtn = [[UIButton alloc]init];
    setUpBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [setUpBtn setImage:[UIImage imageNamed:@"my_rignht_top"] forState:(UIControlStateNormal)];
    
       
     setUpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.headView addSubview:setUpBtn];
    [setUpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.headView.mas_right);
        make.top.mas_equalTo(self.headView.mas_top).offset(TOP_SPACE20or44);
        make.height.mas_offset(20);
        make.width.mas_offset(35);
    }];
    [setUpBtn whenTapped:^{
       
    }];
    self.headView.frame = CGRectMake(0, -30, KSW, (KSW * 175 / 375 +77-20));

    self.tableV.tableHeaderView = self.headView;
    

}

// go to numbers VC
- (void)goToNumberInfo:(NSInteger)index {
    if (index == 0) {
        NSLog(@"点击了收藏夹");
        MyCollectionViewController *vc = [[MyCollectionViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (index == 1) {
        NSLog(@"点击了关注");
        MyAttentionViewController *vc = [[MyAttentionViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (index == 2) {
        NSLog(@"点击了粉丝");
        MyFanViewController *vc = [[MyFanViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 90;
    }else{
        return 57;
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
   UIView *view = UIView.new;
   view.backgroundColor = UIColor.clearColor;
   return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return 0;
    }else if (section == 1) {
        return 0;
    }else if (section == 2){
        return 0;
    }else{
        return 0;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0||section == 1) {
        return 0;
    }else{
        return 10;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1) {
        return self.onePartitionArray.count;
    }else if (section == 2) {
        return self.twoPartitionArray.count;
    }else {
        return self.threePartitionArray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        MyVIPOpenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyVIPOpenTableViewCell"];
        if (!cell) {
            cell = [[MyVIPOpenTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyVIPOpenTableViewCell"];
        }
        cell.backgroundColor = [UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if (self.myModel.isVIP == YES) {
            cell.openLabel.text = @"续费";
        }else{
            cell.openLabel.text = @"立即开通";
        }
        [cell.openLabel whenTapped:^{
            VIPCenterViewController *vc = [[VIPCenterViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
    return cell;
    }else{
        MyPageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyPageTableViewCell"];
        if (!cell) {
            cell = [[MyPageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyPageTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if (indexPath.section == 1) {
            cell.nameLabel.text = self.onePartitionArray[indexPath.row];
            cell.markImageV.image = [UIImage imageNamed:self.onePartitionArray[indexPath.row]];
        }else if (indexPath.section == 2){
            cell.nameLabel.text = self.twoPartitionArray[indexPath.row];
            cell.markImageV.image = [UIImage imageNamed:self.twoPartitionArray[indexPath.row]];
            if (indexPath.row == 2) {
                if (self.myModel.isAgent == YES) {
                    cell.nameLabel.text = @"我的代理数据";
                }else{
                    cell.nameLabel.text = self.twoPartitionArray[indexPath.row];
                }
            }
        }else if (indexPath.section == 3){
            cell.nameLabel.text = self.threePartitionArray[indexPath.row];
            cell.markImageV.image = [UIImage imageNamed:self.threePartitionArray[indexPath.row]];
        }
    
    return cell;
    }
        
        
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([UserInfoManager shared].isLogin == NO) {
        // 登录弹窗
        [self loginWindowPops];
        return;
    }
    
    if (indexPath.section == 0) {
//        NSLog(@"点击了会员");
//        VIPCenterViewController *vc = [[VIPCenterViewController alloc]init];
//        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            NSLog(@"点击了我的课程");
            MyCourseViewController *vc = [[MyCourseViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 1){
            NSLog(@"点击了我的作品");
            MyWorkViewController *vc = [[MyWorkViewController alloc]init];
            vc.isJudge = 1;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 2){
            NSLog(@"点击了我要当教练");
            CoachCertificationViewController *vc = [[CoachCertificationViewController alloc]init];
//            vc.model = self.certificationModel;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 3){
            NSLog(@"点击了课程编辑");
            CourseEditorViewController *vc = [[CourseEditorViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 4){
            NSLog(@"点击了课程管理");
            CourseManagementViewController *vc = [[CourseManagementViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (indexPath.section == 2){
        if (indexPath.row == 0) {
            NSLog(@"点击了我的钱包");
            AppleWalletViewController *vc = [[AppleWalletViewController alloc] init];
//            MyWalletViewController *vc = [[MyWalletViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 1){
            NSLog(@"点击了我的邀请码");
            FriendInvitationViewController *vc = [[FriendInvitationViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 2){
            NSLog(@"点击了我要成为代理");
            if (self.myModel.isAgent == YES) {
                MyProxyDataViewController *vc = [[MyProxyDataViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                [RequestManager agentInfoWithId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
                    NSDictionary *dic = response[@"data"];
                    NSLog(@"代理信息 === %@",response);
                    WantTheAgentViewController *vc = [[WantTheAgentViewController alloc]init];
                    if ([response[@"data"] isKindOfClass:[NSString class]]) {
                        vc.isJudge = 2;
                    }else{
                        TheAgentInformationModel *model = [TheAgentInformationModel mj_objectWithKeyValues:dic];
                        vc.isJudge = 1;
                        vc.informationModel = model;
                        
                    }
                    [self.navigationController pushViewController:vc animated:YES];
                    
                } withFail:^(NSError * _Nullable error) {
                    
                }];
                
                
            }
            
        }
    }else if (indexPath.section == 3){
        if (indexPath.row == 0) {
            NSLog(@"点击了功能指南");
            FunctionGuideViewController *vc = [[FunctionGuideViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 1){
            NSLog(@"点击了问题反馈");
            FeedbackViewController *vc = [[FeedbackViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 2){
            NSLog(@"点击了设置");
            SetUpViewController *vc = [[SetUpViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
}


/** 登录弹窗*/
-(void)loginWindowPops{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请您先登录" message:@"该功能需要登录过后才能继续使用" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"暂不登录" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"前往登录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            LoginVerifyViewController *loginVC = [[LoginVerifyViewController alloc] init];
            
            CustomNavViewController *customNav = [[CustomNavViewController alloc] initWithRootViewController:loginVC];
            customNav.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [self presentViewController:customNav animated:YES completion:nil];
            
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
}


@end
