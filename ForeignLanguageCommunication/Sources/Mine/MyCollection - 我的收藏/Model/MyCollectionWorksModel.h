//
//  MyCollectionWorksModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyCollectionWorksModel : NSObject
/** 浏览的数量*/
@property (nonatomic, assign) NSInteger browseNum;
/** 收藏ID*/
@property (nonatomic, assign) NSInteger collectId;
/** 动态内容*/
@property (nonatomic, strong) NSString *content;
/** 发布时间*/
@property (nonatomic, strong) NSString *createTime;
/** 图片url，多张已逗号分隔*/
@property (nonatomic, strong) NSString *picUrl;
/** 动态ID*/
@property (nonatomic, assign) NSInteger ID;
/** 动态类型：1 文字+图片、2 文字+视频*/
@property (nonatomic, assign) NSInteger type;
/** 视频封面url*/
@property (nonatomic, strong) NSString *videoCoverUrl;
/** 视频url*/
@property (nonatomic, strong) NSString *videoUrl;

@end

NS_ASSUME_NONNULL_END
