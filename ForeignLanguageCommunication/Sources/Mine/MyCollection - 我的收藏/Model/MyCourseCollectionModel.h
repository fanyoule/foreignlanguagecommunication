//
//  MyCourseCollectionModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyCourseCollectionModel : NSObject
/** 用户头像*/
@property (nonatomic, strong) NSString *avatarUrl;
/** 教练名字*/
@property (nonatomic, strong) NSString *coachName;
/** 收藏ID*/
@property (nonatomic, assign) NSInteger collectId;
/** 课程简介*/
@property (nonatomic, strong) NSString *content;
/** 课程ID*/
@property (nonatomic, assign) NSInteger courseId;
/** 课程名字*/
@property (nonatomic, strong) NSString *courseName;
/** 课程图片*/
@property (nonatomic, strong) NSString *courseUrl;
/** 用户名*/
@property (nonatomic, strong) NSString *nickname;
/** 价格*/
@property (nonatomic, assign) NSInteger price;
/** 课程时间*/
@property (nonatomic, assign) NSInteger time;
/** 用户id*/
@property (nonatomic, assign) NSInteger userId;
@end

NS_ASSUME_NONNULL_END
