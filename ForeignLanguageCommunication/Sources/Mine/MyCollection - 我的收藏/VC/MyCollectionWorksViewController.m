//
//  MyCollectionWorksViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//  作品

#import "MyCollectionWorksViewController.h"
#import "PersonalInformationWorksCollectionViewCell.h"
#import "WorldDetailViewController.h"
#import "MyCollectionWorksModel.h"
@interface MyCollectionWorksViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong)UICollectionView *collectionView;

@property (nonatomic, strong)NSMutableArray *dataArray;

@property (nonatomic, assign)int paging;
@end

@implementation MyCollectionWorksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(30);
        make.left.right.bottom.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    [self.collectionView registerClass:[PersonalInformationWorksCollectionViewCell class] forCellWithReuseIdentifier:@"PersonalInformationWorksCollectionViewCell"];
    
    self.paging = 1;
    
    // MARK: 下拉刷新
        self.collectionView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        
        // MARK: 上拉加载
    self.collectionView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopic)];
    [self getData];
}

// MARK: 下拉刷新回调
-(void)loadNewData{
    self.paging = 1;
    [self.dataArray removeAllObjects];
    [self getData];
}
// MARK: 上拉加载
-(void)loadMoreTopic{
    self.paging++;
    [self getData];
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

// MARK: 分页查询作品收藏列表
-(void)getData{
    kWeakSelf(self)
    [RequestManager getPublishCollectListPages:self.paging userId:[[UserInfoManager shared] getUserID] pageSize:20 withSuccess:^(id  _Nullable response) {
        NSLog(@"分页查询作品收藏列表 === %@",response);
        NSArray *listArray = response[@"data"][@"list"];
        for (int i = 0; i < listArray.count; i++) {
            MyCollectionWorksModel *model = [MyCollectionWorksModel mj_objectWithKeyValues:listArray[i]];
            
            [self.dataArray addObject:model];
        }
        [self.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView.mj_footer endRefreshing];
    } withFail:^(NSError * _Nullable error) {
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView.mj_footer endRefreshing];
    }];
}


- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = ColorRGBA(255, 255, 255, 1);
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.allowsMultipleSelection = YES;
    }
    return _collectionView;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PersonalInformationWorksCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PersonalInformationWorksCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    cell.layer.masksToBounds = YES;
    cell.selector.hidden = YES;
    MyCollectionWorksModel *model = [[MyCollectionWorksModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.workmodel = model;
    
        return cell;
  
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    
    return CGSizeMake(KSW/3-20, KSW/3-20);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 10, 15);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MyCollectionWorksModel *model = self.dataArray[indexPath.item];
    WorldDetailViewController *vc = [[WorldDetailViewController alloc]init];
    vc.worksID = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
