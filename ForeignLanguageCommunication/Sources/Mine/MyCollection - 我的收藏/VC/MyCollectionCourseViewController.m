//
//  MyCollectionCourseViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/6.
//  课程

#import "MyCollectionCourseViewController.h"
#import "MyCourseIsVersatileCollectionViewCell.h"
#import "CoachDetailsViewController.h"

#import "MyCourseCollectionModel.h"
@interface MyCollectionCourseViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;

@property (nonatomic, assign)int paging;
@end

@implementation MyCollectionCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(255, 255, 255, 1);
    
    
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(30);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    
    [self.mainCollectionView registerClass:[MyCourseIsVersatileCollectionViewCell class] forCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell"];
    
    
    self.paging = 1;
    
    // MARK: 下拉刷新
        self.mainCollectionView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        
        // MARK: 上拉加载
    self.mainCollectionView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopic)];
    [self getData];
}


// MARK: 下拉刷新回调
-(void)loadNewData{
    self.paging = 1;
    [self.dataArray removeAllObjects];
    [self getData];
}
// MARK: 上拉加载
-(void)loadMoreTopic{
    self.paging++;
    [self getData];
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
// MARK: 分页查询课程收藏列表
-(void)getData{
    kWeakSelf(self)
    [RequestManager getCourseCollectListPages:self.paging userId:[[UserInfoManager shared] getUserID] pageSize:10 withSuccess:^(id  _Nullable response) {
        NSLog(@"分页查询课程收藏列表 === %@",response);
        NSArray *array = response[@"data"][@"list"];
        if ([response[@"status"] integerValue] == 200) {
            for (int i = 0; i < array.count; i++) {
                MyCourseCollectionModel *model = [MyCourseCollectionModel mj_objectWithKeyValues:array[i]];
                [self.dataArray addObject:model];
            }
        }
        [self.mainCollectionView reloadData];
        [weakself.mainCollectionView.mj_header endRefreshing];
        [weakself.mainCollectionView.mj_footer endRefreshing];
    } withFail:^(NSError * _Nullable error) {
        [weakself.mainCollectionView.mj_header endRefreshing];
        [weakself.mainCollectionView.mj_footer endRefreshing];
    }];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MyCourseIsVersatileCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCourseIsVersatileCollectionViewCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 12;
    
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    cell.layer.shadowOpacity = 0.5f;
    cell.classLabel.hidden = YES;
    cell.blueBtn.hidden = YES;
    cell.grayBtn.hidden = YES;
    
    cell.courseModel = self.dataArray[indexPath.row];
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(KSW-30, 155);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{


    return UIEdgeInsetsMake(10, 15, 5, 15);
}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
//{
//
//    return 20;//上下间隙
//}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//
//    return 15;//左右间隙
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MyCourseCollectionModel *m = self.dataArray[indexPath.row];
    CoachWithCourseModel *model = [[CoachWithCourseModel alloc] init];
    model.userId = m.userId;
    CoachDetailsViewController *vc = [[CoachDetailsViewController alloc]init];
    vc.courseModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
