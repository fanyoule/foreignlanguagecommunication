//
//  PerfectInformationView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "PerfectInformationView.h"

@implementation PerfectInformationView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.portraitImageV];
    [self addSubview:self.label];
    
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.centerX.equalTo(self);
        make.width.height.equalTo(@60);
    }];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.portraitImageV);
        make.top.equalTo(self.portraitImageV.mas_bottom).offset(10);
    }];
    
}


- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"头像"];
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.layer.masksToBounds = YES;
    }
    return _portraitImageV;
}

- (UILabel *)label{
    if (!_label) {
        _label = [[UILabel alloc]init];
        _label.text = @"上传头像";
        _label.textColor = RGBA(24, 24, 24, 1);
        _label.font = kFont_Medium(12);
        _label.textAlignment = 1;
    }
    return _label;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
