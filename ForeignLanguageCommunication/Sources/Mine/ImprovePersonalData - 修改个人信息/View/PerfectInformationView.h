//
//  PerfectInformationView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PerfectInformationView : UIView
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 上传*/
@property (nonatomic, strong)UILabel *label;
@end

NS_ASSUME_NONNULL_END
