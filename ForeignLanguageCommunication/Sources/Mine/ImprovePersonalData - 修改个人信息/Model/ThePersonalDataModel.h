//
//  ThePersonalDataModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/15.
//  个人资料Model

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThePersonalDataModel : NSObject
/** 生日*/
@property (nonatomic, strong)NSString *birthday;
/** 城市*/
@property (nonatomic, strong)NSString *city;
/** 与我距离*/
@property (nonatomic, strong)NSString *distance;
/** 头像*/
@property (nonatomic, strong)NSString *headUrl;
/** 身高*/
@property (nonatomic, assign)NSInteger height;
/** 爱好*/
@property (nonatomic, strong)NSArray *hobby;
/** 用户id*/
@property (nonatomic, assign)NSInteger ID;
/** 个人介绍*/
@property (nonatomic, strong)NSString *introduce;
/** 邀请码*/
@property (nonatomic, strong)NSString *inviteCode;
/** 是否被拉黑*/
@property (nonatomic)BOOL isBlack;
/** 是否关注*/
@property (nonatomic)BOOL isFollow;
/** 是否本人*/
@property (nonatomic)BOOL isMe;
/** 是否会员*/
@property (nonatomic)BOOL isVIP;
/** 是否被投票*/
@property (nonatomic)BOOL isVoted;
/** 昵称*/
@property (nonatomic, strong)NSString *nickName;
/** 职业*/
@property (nonatomic, strong)NSString *occupation;
/** 手机*/
@property (nonatomic, strong)NSString *phone;
/** 我的作品*/
@property (nonatomic, strong)NSArray *publishs;
/** 性别：0 女，1 男*/
@property (nonatomic, assign)NSInteger sex;
/** 账号*/
@property (nonatomic, assign)NSString *sysId;
/** 体重*/
@property (nonatomic, assign)NSInteger weight;


@end

NS_ASSUME_NONNULL_END
