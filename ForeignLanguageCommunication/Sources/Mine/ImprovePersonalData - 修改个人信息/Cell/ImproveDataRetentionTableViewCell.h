//
//  ImproveDataRetentionTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImproveDataRetentionTableViewCell : UITableViewCell
/** 按钮*/
@property (nonatomic, strong)UIButton *saveBtn;
@end

NS_ASSUME_NONNULL_END
