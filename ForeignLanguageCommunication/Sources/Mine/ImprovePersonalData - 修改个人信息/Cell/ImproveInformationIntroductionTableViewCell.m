//
//  ImproveInformationIntroductionTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "ImproveInformationIntroductionTableViewCell.h"
@interface ImproveInformationIntroductionTableViewCell ()<UITextViewDelegate>

@end

@implementation ImproveInformationIntroductionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.textView];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.promptLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.bottom.equalTo(self.contentView).offset(-15);
        make.left.equalTo(self.contentView).offset(100);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@100);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
    
    [self.promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView).offset(0);
        make.left.equalTo(self.textView).offset(5);
    }];
    
}



- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"个人介绍";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(16);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.delegate = self;
        _textView.font = kFont_Medium(15);
        _textView.textContainerInset = UIEdgeInsetsMake(0, 0, 10, 10);
        
    }
    return _textView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}
- (UILabel *)promptLabel{
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc]init];
        _promptLabel.text = @"介绍一下自己";
        _promptLabel.textColor = RGBA(198, 198, 198, 1);
        _promptLabel.font = kFont_Medium(15);
        _promptLabel.textAlignment = 0;
    }
    return _promptLabel;
}

-(void)textViewDidChange:(UITextView*)textView
{
    if([textView.text length] == 0){
      self.promptLabel.text = @"介绍一下自己";

    }else{
       self.promptLabel.text = @"";//这里给空

    }

    //计算剩余字数   不需要的也可不写

    NSString *nsTextCotent = textView.text;

    NSInteger existTextNum = [nsTextCotent length];

    
    if (self.clickPersonalProfileBlock) {
        self.clickPersonalProfileBlock(textView.text);
    }
    

}

//设置超出最大字数（140字）即不可输入 也是textview的代理方法

-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text{
    if ([text isEqualToString:@"\n"]) {     //这里"\n"对应的是键盘的 return 回收键盘之用
        
       [textView resignFirstResponder];

       return YES;

    }
    if (range.location >= 30){
     
            return NO;

    }else{
       return YES;

    }

}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
