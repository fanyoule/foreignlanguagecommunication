//
//  PerfectInformationTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^ClickNicknameBlock)(NSString *nameTitle);

@interface PerfectInformationTableViewCell : UITableViewCell
/** 星星*/
@property (nonatomic, strong)UILabel *starsLabel;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 输入框*/
@property (nonatomic, strong)UITextField *textField;
/** 线*/
@property (nonatomic, strong)UIView *lineView;

/** 昵称*/
@property (nonatomic, copy)ClickNicknameBlock clickNicknameBlock;
@end

NS_ASSUME_NONNULL_END
