//
//  ImproveDataSelectionTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImproveDataSelectionTableViewCell : UITableViewCell
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 内容*/
@property (nonatomic, strong)UILabel *contentLabel;

/** 线*/
@property (nonatomic, strong)UIView *lineView;
@end

NS_ASSUME_NONNULL_END
