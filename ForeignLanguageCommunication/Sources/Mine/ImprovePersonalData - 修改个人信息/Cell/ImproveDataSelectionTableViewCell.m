//
//  ImproveDataSelectionTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "ImproveDataSelectionTableViewCell.h"

@implementation ImproveDataSelectionTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.lineView];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.bottom.equalTo(self.contentView).offset(-15).priority(600);
        make.left.equalTo(self.contentView).offset(105);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
    
    
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"常驻城市";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(16);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"可以多选";
        _contentLabel.textColor = rgba(198, 198, 198, 1);
        _contentLabel.font = kFont_Medium(14);
        _contentLabel.textAlignment = 0;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
