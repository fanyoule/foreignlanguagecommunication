//
//  ImproveInformationIntroductionTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^ClickPersonalProfileBlock)(NSString *personalProfileStr);
@interface ImproveInformationIntroductionTableViewCell : UITableViewCell
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 输入框*/
@property (nonatomic, strong)UITextView *textView;
/** 提示*/
@property (nonatomic, strong)UILabel *promptLabel;
/** 线*/
@property (nonatomic, strong)UIView *lineView;

/** 个人简介*/
@property (nonatomic, copy)ClickPersonalProfileBlock clickPersonalProfileBlock;
@end

NS_ASSUME_NONNULL_END
