//
//  ImproveDataRetentionTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "ImproveDataRetentionTableViewCell.h"
@interface ImproveDataRetentionTableViewCell ()

/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;

@end
@implementation ImproveDataRetentionTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.saveBtn];
    [self.contentView addSubview:self.rulesLabel];
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.bottom.equalTo(self.contentView).offset(-20);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@44);
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.saveBtn.mas_bottom).offset(10);
    }];
    
    
}

- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [[UIButton alloc]init];
        [_saveBtn setTitle:@"保存" forState:(UIControlStateNormal)];
        [_saveBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _saveBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _saveBtn.layer.cornerRadius = 6;
        _saveBtn.layer.masksToBounds = YES;
    }
    return _saveBtn;
}

- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"选择性别后无法修改";
        _rulesLabel.textColor = RGBA(153, 153, 153, 1);
        _rulesLabel.font = kFont_Medium(12);
        _rulesLabel.textAlignment = 1;
    }
    return _rulesLabel;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
