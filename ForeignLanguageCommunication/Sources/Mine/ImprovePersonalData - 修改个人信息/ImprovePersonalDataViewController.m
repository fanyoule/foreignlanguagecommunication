//
//  ImprovePersonalDataViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//  修改个人信息

#import "ImprovePersonalDataViewController.h"
#import "PerfectInformationTableViewCell.h"
#import "ImproveDataSelectionTableViewCell.h"
#import "ImproveInformationIntroductionTableViewCell.h"
#import "ImproveDataRetentionTableViewCell.h"

#import "MineDataPickerModel.h"


#import "PerfectInformationView.h"// 头部

#import "CitiyPickerView.h"// 城市选择器
#import "CiryAndJobViewController.h"// 职业选择器

#import "MineDataPickerViewController.h"//体重身高选择器
#import "DatePickerView.h"// 生日选择器
#import "ThePersonalDataModel.h"//个人资料Model
@interface ImprovePersonalDataViewController ()<UITableViewDelegate,UITableViewDataSource, HXPhotoViewDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic ,strong)PerfectInformationView *headView;
///**图片管理器*/
//@property (nonatomic, strong) HXPhotoManager *photoManager;
///**选择图片View*/
//@property (nonatomic, strong) HXPhotoView *photoView;

/** 职业id*/
@property (nonatomic, assign)NSInteger professionalID;
/** 省*/
@property (nonatomic, strong)NSString *provinceStr;
/** 市*/
@property (nonatomic, strong)NSString *cityStr;
/** 区*/
@property (nonatomic, strong)NSString *areaStr;
/** 生日*/
@property (nonatomic, strong)NSString *birthdayStr;
/** 个人简介*/
@property (nonatomic, strong)NSString *personalProfileStr;
/** 身高*/
@property (nonatomic, assign)NSInteger heightInteger;
/** 体重*/
@property (nonatomic, assign)NSInteger weightInteger;
/** 日常爱好*/
@property (nonatomic, strong)NSString *hobbyStrID;
/** 名字*/
@property (nonatomic, strong)NSString *nameStr;
/** 头像url*/
@property (nonatomic, strong)NSString *avatarUrlStr;

/** 个人资料Model*/
@property (nonatomic, strong)ThePersonalDataModel *model;

@end

@implementation ImprovePersonalDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navTitleString = @"修改个人信息";
    
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
//    self.mainTableView.scrollEnabled = NO;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    self.headView.frame = CGRectMake(0, SNavBarHeight, KSW, 126);

    self.mainTableView.tableHeaderView = self.headView;
    
    [self.headView.portraitImageV whenTapped:^{
        NSLog(@"头像");
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self selectAction:1];
        }];
        
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"从相册中选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self selectAction:2];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [cancelAction setValue:RGBA(153, 153, 153, 1) forKey:@"titleTextColor"];
        [alert addAction:action1];
        [alert addAction:action2];
        [alert addAction:cancelAction];
        
        [self presentViewController:alert animated:YES completion:nil];
//        self.avatarUrlStr = self.model.headUrl;
    }];
    
    [self getData];
}
- (void)selectAction:(int)a {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;//编辑模式  但是编辑框是正方形的
     // 设置可用的媒体类型、默认只包含kUTTypeImage
    imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
    imagePicker.modalPresentationStyle = UIModalPresentationOverFullScreen;
    if (a == 1) {
        //设置照片来源
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:nil];
    } else {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}
#pragma mark - =======UIImagePickerControllerDelegate=========
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    [RequestManager uploadImage:@[image] withSuccess:^(id  _Nullable response) {
        NSArray *data = response[@"data"];
        if (data.count > 0) {
            NSString *url = data[0][@"url"];
            self.avatarUrlStr = url;
            self.headView.portraitImageV.image = image;
//            [self.headView.portraitImageV sd_setImageWithURL:[NSURL URLWithString:url]];
        }
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
// 取消图片选择调用此方法
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (PerfectInformationView *)headView{
    if (!_headView) {
        _headView = [[PerfectInformationView alloc]init];
        _headView.backgroundColor = RGBA(245, 245, 245, 1);
    }
    return _headView;
}
- (ThePersonalDataModel *)model{
    if (!_model) {
        _model = [[ThePersonalDataModel alloc]init];
    }
    return _model;
}

// MARK: 个人资料
-(void)getData{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@([[UserInfoManager shared] getUserID]) forKey:@"toUserId"];
    [dic setObject:@([[UserInfoManager shared] getUserID]) forKey:@"userId"];
    
    [RequestManager postObtainUrl:@"/api/user/getUserById" incomingParameter:dic withSuccess:^(id  _Nullable response) {
        NSLog(@"用户资料 === %@",response);
        NSDictionary *dictionary = [[NSDictionary alloc]init];
        dictionary = response[@"data"];
        if ([response[@"status"] intValue] == 200) {
            self.model = [ThePersonalDataModel mj_objectWithKeyValues:dictionary];
            [self.headView.portraitImageV sd_setImageWithURL:[NSURL URLWithString:self.model.headUrl]];
            
        }
        
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 9;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    kWeakSelf(self)
    if (indexPath.row == 0) {
        
        PerfectInformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PerfectInformationTableViewCell"];
            if (!cell) {
                cell = [[PerfectInformationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PerfectInformationTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.backgroundColor = [UIColor whiteColor];
        cell.textField.text = self.model.nickName;
        
        cell.clickNicknameBlock = ^(NSString * _Nonnull nameTitle) {
            weakself.nameStr = nameTitle;
        };
        NSLog(@"名字 === %@",cell.textField.text);
            return cell;
    }else if (indexPath.row == 7){
        ImproveInformationIntroductionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImproveInformationIntroductionTableViewCell"];
        if (!cell) {
            cell = [[ImproveInformationIntroductionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ImproveInformationIntroductionTableViewCell"];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textView.text = self.model.introduce;
        if (self.model.introduce.length <= 0) {
            cell.promptLabel.hidden = YES;
        }
        
        cell.clickPersonalProfileBlock = ^(NSString * _Nonnull personalProfileStr) {
            weakself.personalProfileStr = personalProfileStr;
        };
        
        return cell;
    }else if (indexPath.row == 8){
        ImproveDataRetentionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImproveDataRetentionTableViewCell"];
        if (!cell) {
            cell = [[ImproveDataRetentionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ImproveDataRetentionTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor clearColor];
        [cell.saveBtn addTarget:self action:@selector(clickSave) forControlEvents:(UIControlEventTouchDown)];
        return cell;
    }else{
        
        ImproveDataSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImproveDataSelectionTableViewCell"];
        if (!cell) {
            cell = [[ImproveDataSelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ImproveDataSelectionTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.backgroundColor = [UIColor whiteColor];
        
        if (indexPath.row == 1) {
            // 常驻城市
            if (self.model.city == nil) {
                cell.contentLabel.text = @"可以多选";
            }else{
                cell.contentLabel.text = self.model.city;
                cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
            }
            
            [cell whenTapped:^{
                NSLog(@"常驻城市");
                [CitiyPickerView showPickerViewWithComplete:^(NSString * _Nonnull province, NSString * _Nonnull city, NSString * _Nonnull area) {
                        NSLog(@"%@%@%@",province,city,area);
                    cell.contentLabel.text = city;
                    cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
                    weakself.provinceStr = province;
                    weakself.cityStr = city;
                    weakself.areaStr = area;
                }];
            }];
            
        }else if (indexPath.row == 2){
            cell.titleLabel.text = @"生日";
            if (self.model.birthday == nil) {
                cell.contentLabel.text = @"请选择";
            }else{
                cell.contentLabel.text = self.model.birthday;
                cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
            }
            
            [cell whenTapped:^{
                NSLog(@"生日");
                DatePickerView *dateView = [[DatePickerView alloc] init];
                    [dateView show];
                    dateView.timeBlock = ^(NSDate * _Nonnull date, NSString * _Nonnull dateString) {
                      //选择时间
                        NSLog(@"%@",dateString);
                        cell.contentLabel.text = dateString;
                        cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
                        weakself.birthdayStr = dateString;
                    };
            }];
        }else if (indexPath.row == 3){
            cell.titleLabel.text = @"职业";
            if (self.model.occupation == nil) {
                cell.contentLabel.text = @"请选择";
            }else{
                cell.contentLabel.text = self.model.occupation;
                cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
            }
            
            
            [cell whenTapped:^{
                NSLog(@"职业");
                CiryAndJobViewController *vc= [[CiryAndJobViewController alloc]init];
                vc.titleStr = @"职业";
                [self.navigationController pushViewController:vc animated:YES];
                vc.confirmBlock = ^(CityAndJobModel *cityAndJobModel) {
                    cell.contentLabel.text = cityAndJobModel.name;
                    cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
                    weakself.professionalID = cityAndJobModel.ID;
                };
                
            }];
        }else if (indexPath.row == 4){
            cell.titleLabel.text = @"日常爱好";
            if (self.model.hobby.count == 0) {
                cell.contentLabel.text = @"可以多选";
            }else{
                NSMutableArray *arrayName = [NSMutableArray array];
                
                for (int i = 0; i < self.model.hobby.count; i++) {
                    [arrayName addObject:self.model.hobby[i][@"name"]];
                    
                }
                NSString *nameStr = [arrayName componentsJoinedByString:@","];
                
                cell.contentLabel.text = nameStr;
                cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
            }
            
            [cell whenTapped:^{
                NSLog(@"日常爱好");
                MineDataPickerViewController *VC = [MineDataPickerViewController new];
                VC.isJudge = 2;
                VC.showSingle = NO;
                VC.needShowColor = NO;
                VC.titleString = @"日常爱好";
                HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                popController.backgroundAlpha = 0.1;//背景色透明度
                popController.animationDuration = 0.3;
                popController.popPosition = 2;
                popController.popType = 5;
                popController.dismissType = 5;
                popController.shouldDismissOnBackgroundTouch = YES;
                [popController presentInViewController:self];
                VC.confirmBlock = ^(NSArray *selectModelArray) {
                    NSMutableArray *arrayID = [NSMutableArray array];
                    NSMutableArray *array = [NSMutableArray array];
                    if (selectModelArray != nil && ![selectModelArray isKindOfClass:[NSNull class]] && (selectModelArray.count != 0)) {
                        for (int i = 0; i < selectModelArray.count; i++) {
                            MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:selectModelArray[i]];
                            [array addObject:model.name];
                            [arrayID addObject:@(model.ID)];
                        }
                        weakself.hobbyStrID = [arrayID componentsJoinedByString:@","];
                        cell.contentLabel.text = [array componentsJoinedByString:@"、"];
                        cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
                    }
                };
            }];
        }else if (indexPath.row == 5){
            cell.titleLabel.text = @"身高";
            if (self.model.height == 0) {
                cell.contentLabel.text = @"不显示";
                cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
            }else{
                cell.contentLabel.text = [NSString stringWithFormat:@"%ldcm",self.model.height];
                cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
            }
            
            [cell whenTapped:^{
                NSLog(@"身高");
                MineDataPickerViewController *VC = [MineDataPickerViewController new];
                VC.isJudge = 0;
                VC.showSingle = YES;
                VC.needShowColor = NO;
                VC.titleString = @"身高";
                HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                popController.backgroundAlpha = 0.1;//背景色透明度
                popController.animationDuration = 0.3;
                popController.popPosition = 2;
                popController.popType = 5;
                popController.dismissType = 5;
                popController.shouldDismissOnBackgroundTouch = YES;
                [popController presentInViewController:self];
                VC.confirmBlock = ^(NSArray *selectModelArray) {
                    NSMutableArray *array = [NSMutableArray array];
                    if (selectModelArray != nil && ![selectModelArray isKindOfClass:[NSNull class]] && (selectModelArray.count != 0)) {
                        for (int i = 0; i < selectModelArray.count; i++) {
                            MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:selectModelArray[i]];
                            [array addObject:model.name];
                        }
                        cell.contentLabel.text = [array componentsJoinedByString:@""];
                        cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
                        
                        if ([cell.contentLabel.text isEqualToString:@"不显示"]) {
                            weakself.heightInteger = 0;
                        }else{
                            weakself.heightInteger = [[cell.contentLabel.text substringToIndex:3] integerValue];
                        }
                    }
                    
                };
                
            }];
        }else if (indexPath.row == 6){
            cell.titleLabel.text = @"体重";
            if (self.model.weight == 0) {
                cell.contentLabel.text = @"不显示";
                cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
            }else{
                cell.contentLabel.text = [NSString stringWithFormat:@"%ldkg",self.model.weight];
                cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
            }
            
            [cell whenTapped:^{
                NSLog(@"体重");
                MineDataPickerViewController *VC = [MineDataPickerViewController new];
                VC.isJudge = 1;
                VC.showSingle = YES;
                VC.needShowColor = NO;
                VC.titleString = @"体重";
                HWPopController *popController = [[HWPopController alloc] initWithViewController:VC];
                popController.backgroundAlpha = 0.1;//背景色透明度
                popController.animationDuration = 0.3;
                popController.popPosition = 2;
                popController.popType = 5;
                popController.dismissType = 5;
                popController.shouldDismissOnBackgroundTouch = YES;
                [popController presentInViewController:self];
                VC.confirmBlock = ^(NSArray *selectModelArray) {
                    NSMutableArray *array = [NSMutableArray array];
                    if (selectModelArray != nil && ![selectModelArray isKindOfClass:[NSNull class]] && (selectModelArray.count != 0)) {
                        for (int i = 0; i < selectModelArray.count; i++) {
                            MineDataPickerModel *model = [MineDataPickerModel mj_objectWithKeyValues:selectModelArray[i]];
                            [array addObject:model.name];
                        }
                        cell.contentLabel.text = [array componentsJoinedByString:@""];
                        cell.contentLabel.textColor = RGBA(24, 24, 24, 1);
                        if ([cell.contentLabel.text isEqualToString:@"不显示"]) {
                            weakself.weightInteger = 0;
                        }else{
                            weakself.weightInteger = [[cell.contentLabel.text substringToIndex:2] integerValue];
                        }
                    }else{
                        
                    }
                    
                };
            }];
        }
        
        
        
        return cell;
    }
    
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//   
//}

//MARK: 修改个人资料
-(void)clickSave{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@([[UserInfoManager shared] getUserID]) forKey:@"id"];//用户id
    if (self.avatarUrlStr == nil) {
        [dic setObject:self.model.headUrl forKey:@"avatar"];//头像url
    }else{
        [dic setObject:self.avatarUrlStr forKey:@"avatar"];//头像url
    }
    
    if (self.nameStr == nil) {
        [dic setObject:self.model.nickName forKey:@"nickname"];//昵称
    }else{
        [dic setObject:self.nameStr forKey:@"nickname"];//昵称
    }
    
    if (self.birthdayStr == nil) {
        
    }else{
        [dic setObject:self.birthdayStr forKey:@"birthday"];//生日
    }
    
    if (self.weightInteger == 0) {
        
    }else{
        [dic setObject:@(self.weightInteger) forKey:@"weight"];//体重
    }
    
    if (self.heightInteger == 0) {
        
    }else{
        [dic setObject:@(self.heightInteger) forKey:@"height"];//身高
    }
    
    if (self.hobbyStrID == nil) {
        
    }else{
        [dic setObject:self.hobbyStrID forKey:@"hobby"];//爱好，id拼接
    }
    
    if (self.personalProfileStr == nil) {
        
    }else{
        [dic setObject:self.personalProfileStr forKey:@"introduce"];//个人简介
    }
    
    if (self.professionalID == 0) {
        
    }else{
        [dic setObject:@(self.professionalID) forKey:@"occupation"];//职业id
    }
    
    if (self.cityStr == nil) {
        
    }else{
        [dic setObject:self.provinceStr forKey:@"province"];//省
        [dic setObject:self.cityStr forKey:@"city"];//市
        [dic setObject:self.areaStr forKey:@"area"];//区
    }
    NSLog(@"字典 === %@",dic);
    
    [RequestManager postObtainUrl:@"/api/user/updUser" incomingParameter:dic withSuccess:^(id  _Nullable response) {
        if ([response[@"status"] integerValue] == 200) {
            [self showText:@"修改成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self showText:response[@"msg"]];
        }
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
    NSLog(@"保存");
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
