//
//  CiryAndJobViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import "CiryAndJobViewController.h"
#import "CityAndjobTableViewCell.h"


@interface CiryAndJobViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *leftTableview;
@property (nonatomic, strong) UITableView *rightTableview;
/**左 数据*/
@property (nonatomic, strong) NSMutableArray *leftDataArray;
/**右 数据*/
@property (nonatomic, strong) NSMutableArray *rightDataArray;


@property (nonatomic, strong) CityAndJobModel *leftModel;
@property (nonatomic, strong) CityAndJobModel *rightModel;
@end

@implementation CiryAndJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitleString = self.titleStr;
    self.navView.backgroundColor = UIColor.whiteColor;
    [self addRightBtnWith:@"确定"];
    [self.rightBtn setTitleColor:CustomColor(@"#3478F5") forState:UIControlStateNormal];
    self.view.backgroundColor = CustomColor(@"#F1F1F1");
    [self setupSubview];
    [self getData];
}
- (void)rightClick:(UIButton *)sender{
    //确定选择
    //返回 选中的leftmodel   rightmodel 
    if (self.confirmBlock) {
//        NSString *str= [NSString stringWithFormat:@"%@",self.rightModel.name];
        self.confirmBlock(self.rightModel);
    }
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"职业名字 === %@",self.rightModel.name);
}


- (void)setupSubview {
    [self.view addSubview:self.leftTableview];
    self.leftTableview.contentInset = UIEdgeInsetsMake(0, 0, TabbarSafeMargin, 0);
    [self.view addSubview:self.rightTableview];
    self.rightTableview.contentInset = UIEdgeInsetsMake(0, 0, TabbarSafeMargin, 0);
    [self.leftTableview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_offset(SNavBarHeight);
            make.bottom.mas_offset(0);
            make.left.mas_offset(0);
            make.width.mas_equalTo(KSW/3);
    }];
    
    [self.rightTableview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(KSW/3 +1);
            make.right.mas_offset(0);
            make.bottom.top.mas_equalTo(self.leftTableview);
            
    }];
}
- (UITableView *)leftTableview {
    if (!_leftTableview) {
        _leftTableview = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _leftTableview.delegate = self;
        _leftTableview.dataSource = self;
        _leftTableview.showsVerticalScrollIndicator = NO;
        [_leftTableview setTableFooterView:[[UIView alloc] init]];
    }
    return _leftTableview;
}

- (UITableView *)rightTableview {
    if (!_rightTableview) {
        _rightTableview = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _rightTableview.delegate = self;
        _rightTableview.dataSource = self;
        _rightTableview.backgroundColor = UIColor.clearColor;
        _rightTableview.showsVerticalScrollIndicator = NO;
        [_rightTableview setTableFooterView:[[UIView alloc] init]];
    }
    return _rightTableview;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.leftTableview) {
        return self.leftDataArray.count;
    }else{
        //根据当前选中的leftmodel 来决定右边的数量
        return self.rightDataArray.count;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.leftTableview) {
        CityAndjobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"left"];
        if (!cell) {
            cell = [[CityAndjobTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"left"];
        }
        cell.type = CityAndjobTableViewCellTypeLeft;
        CityAndJobModel *model = [[CityAndJobModel alloc]init];
        model = self.leftDataArray[indexPath.row];
        cell.model = model;
        return cell;
    }else{
        //根据左边选中的model来决定右边子项的种类&数量
        CityAndjobTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"right"];
        if (!cell) {
            cell = [[CityAndjobTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"right"];
        }
        cell.type = CityAndjobTableViewCellTypeRight;
        CityAndJobModel *model = [[CityAndJobModel alloc]init];
        model = self.rightDataArray[indexPath.row];
        cell.model = model;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.leftTableview) {
        CityAndJobModel *model = self.leftDataArray[indexPath.row];
        if (self.leftModel == model) {
            return;
        }
        [self.rightDataArray removeAllObjects];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@(model.ID) forKey:@"industryId"];
        [RequestManager getDonLogIn:@"/api/industry/getProfession" incomingParameter:dic withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                NSLog(@"职业信息 === %@",response);
                NSArray *array = response[@"data"];
                for (int i = 0; i < array.count; i++) {
                    CityAndJobModel *model = [CityAndJobModel mj_objectWithKeyValues:array[i]];
                    [self.rightDataArray addObject:model];
                }
            }
            [self.rightTableview reloadData];
        } withFail:^(NSError * _Nullable error) {
            
        }];
        
        if (self.leftModel) {
            
            self.leftModel.selected = NO;
        }
        
        self.leftModel = model;
        self.leftModel.selected = YES;
        [self.leftTableview reloadData];
        [self.rightTableview reloadData];
        
    }else{
        CityAndJobModel *model = self.rightDataArray[indexPath.row];
        if (self.rightModel == model) {
            return;
        }
        if (self.rightModel) {
            self.rightModel.selected = NO;
        }
        self.rightModel = model;
        self.rightModel.selected = YES;
        [self.rightTableview reloadData];
    }
}

- (NSMutableArray *)leftDataArray{
    if (!_leftDataArray) {
        _leftDataArray = [NSMutableArray array];
    }
    return _leftDataArray;
}
- (NSMutableArray *)rightDataArray {
    if (!_rightDataArray) {
        _rightDataArray = [NSMutableArray array];
    }
    return _rightDataArray;
}

- (void)getData {
    // MAKR: 获取行业信息
    [RequestManager getDonLogIn:@"/api/industry/getIndustry" withSuccess:^(id  _Nullable response) {
        NSLog(@"行业信息 === %@",response);
        NSArray *array = response[@"data"];
        for (int i = 0; i < array.count; i++) {
            CityAndJobModel *model = [CityAndJobModel mj_objectWithKeyValues:array[i]];
            NSLog(@"model.name--%@",model.name);
            [self.leftDataArray addObject:model];
        }
        [self.leftTableview reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
    [RequestManager getDonLogIn:@"/api/industry/getProfession" incomingParameter:@{@"industryId":@(1)} withSuccess:^(id  _Nullable response) {
        if ([response[@"status"] integerValue] == 200) {
            NSLog(@"职业信息 === %@",response);
            NSArray *array = response[@"data"];
            for (int i = 0; i < array.count; i++) {
                CityAndJobModel *model = [CityAndJobModel mj_objectWithKeyValues:array[i]];
                NSLog(@"model.name--%@",model.name);
                [self.rightDataArray addObject:model];
            }
        }
        [self.rightTableview reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
    [self.leftTableview reloadData];
    [self.rightTableview reloadData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
