//
//  CityAndjobTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import <UIKit/UIKit.h>
#import "CityAndJobModel.h"


typedef NS_ENUM(NSUInteger, CityAndjobTableViewCellType) {
    CityAndjobTableViewCellTypeLeft,
    CityAndjobTableViewCellTypeRight,
};

NS_ASSUME_NONNULL_BEGIN

@interface CityAndjobTableViewCell : UITableViewCell
@property (nonatomic, strong) CityAndJobModel *model;
@property (nonatomic, assign) CityAndjobTableViewCellType type;
@end

NS_ASSUME_NONNULL_END
