//
//  CiryAndJobViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import "BaseViewController.h"
#import "CityAndJobModel.h"
typedef void(^clickConfirmBlock)(CityAndJobModel *cityAndJobModel);

NS_ASSUME_NONNULL_BEGIN

@interface CiryAndJobViewController : BaseViewController
@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) clickConfirmBlock confirmBlock;
@end

NS_ASSUME_NONNULL_END
