//
//  CityAndJobModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CityAndJobModel : NSObject
/** 行业id ||  职业id*/
@property (nonatomic, assign)NSInteger ID;

@property (nonatomic, copy) NSString *name;
/**存储该名称下的子项*/
@property (nonatomic, strong) NSArray *infoarray;
@property (nonatomic, assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
