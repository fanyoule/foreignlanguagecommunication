//
//  CitiyPickerView.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//
#import "ProvinceModel.h"
#import "CityModel.h"
#import "CitiyPickerView.h"

@implementation CitiyPickerView


+ (void)showPickerViewWithComplete:(void (^)(NSString * _Nonnull, NSString * _Nonnull, NSString * _Nonnull))complete {
    BRStringPickerView *stringPicker = [[BRStringPickerView alloc] init];
    stringPicker.pickerMode = BRStringPickerComponentLinkage;
    stringPicker.dataSourceArr = [self getAreaArray];
    stringPicker.title = @"选择常驻地区";
    
    BRPickerStyle *customStyle = [[BRPickerStyle alloc]init];
    customStyle.selectRowTextFont = [UIFont boldSystemFontOfSize:20.0f];
    customStyle.cancelBtnTitle = @"取消";
    customStyle.doneBtnTitle = @"确定";
    customStyle.cancelTextColor = rgba(39, 93, 220, 1);
    customStyle.doneTextColor = rgba(39, 93, 220, 1);
    customStyle.selectRowTextColor = rgba(39, 93, 220, 1);
    stringPicker.pickerStyle = customStyle;
    [stringPicker show];
    stringPicker.resultModelArrayBlock = ^(NSArray<BRResultModel *> * _Nullable resultModelArr) {
        BRResultModel *province = resultModelArr[0];
        BRResultModel *city = resultModelArr[1];
        BRResultModel *area = resultModelArr[2];
        
        NSString *pName = province.value;
        NSString *cName = city.value;
        NSString *aName = area.value;
        
        if (complete) {
            complete(pName,cName,aName);
        }
        
    };
    
    
}
+ (NSArray <BRResultModel *> *)getAreaArray {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"city" ofType:@"plist"];
    NSArray *array = [[NSArray alloc] initWithContentsOfFile:path];
    NSMutableArray *allModel = [NSMutableArray array];
    for (int i = 0; i < array.count; i ++) {
        
        ProvinceModel *province = [ProvinceModel mj_objectWithKeyValues:array[i]];
        
        BRResultModel *prM = [[BRResultModel alloc] init];
        prM.parentValue = @"";
        prM.parentKey = @"-1";
        prM.key = [NSString stringWithFormat:@"%@-1",province.name];
        prM.value = province.name;
        
        [allModel addObject:prM];
        
        for (int j = 0; j < province.city.count; j ++) {
            CityModel *city = [CityModel mj_objectWithKeyValues:province.city[j]];
            BRResultModel *crM = [[BRResultModel alloc] init];
            crM.parentKey = [NSString stringWithFormat:@"%@-1",province.name];
            crM.parentValue = @"";
            crM.key = [NSString stringWithFormat:@"%@%@",province.name,city.name];
            crM.value = city.name;
            [allModel addObject:crM];
            for (int k = 0; k < city.area.count; k ++) {
                NSString *area = city.area[k];
                
                BRResultModel *arM = [[BRResultModel alloc] init];
                arM.parentKey = [NSString stringWithFormat:@"%@%@",province.name,city.name];
                arM.parentValue = @"";
                arM.key = area;
                arM.value = area;
                [allModel addObject:arM];
            }
        }
        
    }
    return allModel;
}
@end
