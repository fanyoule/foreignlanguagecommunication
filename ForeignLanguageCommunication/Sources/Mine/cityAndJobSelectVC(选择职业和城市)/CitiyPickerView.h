//
//  CitiyPickerView.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CitiyPickerView : NSObject
+ (void)showPickerViewWithComplete:(void(^)(NSString *province, NSString *city, NSString *area))complete;
@end

NS_ASSUME_NONNULL_END
