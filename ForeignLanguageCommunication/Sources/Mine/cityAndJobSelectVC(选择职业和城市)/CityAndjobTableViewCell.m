//
//  CityAndjobTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import "CityAndjobTableViewCell.h"

@interface CityAndjobTableViewCell ()
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *contentlabel;

@end

@implementation CityAndjobTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
    }
    return _backView;
}
- (UILabel *)contentlabel {
    if (!_contentlabel) {
        _contentlabel = [UILabel new];
        _contentlabel.textAlignment = NSTextAlignmentCenter;
        _contentlabel.font = kFont_Medium(16);
        _contentlabel.textColor = CustomColor(@"#181818");
    }
    return _contentlabel;
}

- (void)setupSubView {
    
    [self.contentView addSubview:self.backView];
    [self.contentView addSubview:self.contentlabel];
    self.contentView.backgroundColor = UIColor.whiteColor;
    self.contentlabel.backgroundColor = UIColor.clearColor;
    
    [self.contentlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
        make.height.equalTo(@50);
    }];
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubView];
    }
    return self;
}

- (void)setModel:(CityAndJobModel *)model{
    _model = model;
    self.contentlabel.text = model.name;
    if (model.selected) {
        if (self.type == CityAndjobTableViewCellTypeLeft) {
            self.contentlabel.textColor = UIColor.whiteColor;
            self.contentlabel.backgroundColor = CustomColor(@"#3478F5");
        }else{
            self.contentlabel.textColor = CustomColor(@"3478F5");
        }
        
        
    }else{
        
        if (self.type == CityAndjobTableViewCellTypeLeft) {
            self.contentlabel.textColor = CustomColor(@"#181818");
            self.contentlabel.backgroundColor = UIColor.clearColor;
        }
        else{
            self.contentlabel.textColor = CustomColor(@"#181818");
        }
    }
}
@end
