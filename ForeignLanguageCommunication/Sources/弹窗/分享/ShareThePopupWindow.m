//
//  ShareThePopupWindow.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import "ShareThePopupWindow.h"
#import <HWPopController/HWPop.h>
#import <UMShare/UMShare.h>
@interface ShareThePopupWindow ()
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 微信*/
@property (nonatomic, strong)UIImageView *weChatImageV;
/** 微信文字*/
@property (nonatomic, strong)UILabel *weChatLabel;
/** 微信按钮*/
@property (nonatomic, strong)UIButton *weChatBtn;

/** 朋友圈*/
@property (nonatomic, strong)UIImageView *circleFriendsImageV;
/** 朋友圈文字*/
@property (nonatomic, strong)UILabel *circleFriendsLabel;
/** 朋友圈按钮*/
@property (nonatomic, strong)UIButton *circleFriendsBtn;

/** 取消按钮*/
@property (nonatomic, strong)UIButton *cancelBtn;
@end

@implementation ShareThePopupWindow

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contentSizeInPop = [UIScreen mainScreen].bounds.size;
    self.popController.containerView.backgroundColor = [UIColor clearColor];
    
    UIView *returnView = [[UIView alloc]init];
    returnView.backgroundColor = [UIColor clearColor];
    returnView.frame = self.view.frame;
    [self.view addSubview:returnView];
    [returnView whenTapped:^{
        [self.popController dismiss];
    }];
    
    
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.weChatImageV];
    [self.backView addSubview:self.weChatLabel];
    [self.backView addSubview:self.weChatBtn];
    [self.backView addSubview:self.circleFriendsImageV];
    [self.backView addSubview:self.circleFriendsLabel];
    [self.backView addSubview:self.circleFriendsBtn];
    [self.backView addSubview:self.cancelBtn];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.equalTo(@(185 + TabbarSafeMargin));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.backView).offset(15);
    }];
    [self.weChatImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).offset(66);
        make.right.equalTo(self.backView.mas_centerX).offset(-60);
        make.width.height.equalTo(@32);
    }];
    [self.weChatLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.weChatImageV);
        make.top.equalTo(self.weChatImageV.mas_bottom).offset(10);
    }];
    [self.weChatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.weChatImageV);
        make.bottom.equalTo(self.weChatLabel);
    }];
    [self.circleFriendsImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).offset(66);
        make.left.equalTo(self.backView.mas_centerX).offset(60);
    }];
    [self.circleFriendsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.circleFriendsImageV);
        make.top.equalTo(self.circleFriendsImageV.mas_bottom).offset(10);
    }];
    [self.circleFriendsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.circleFriendsImageV);
        make.bottom.equalTo(self.circleFriendsLabel);
    }];
    
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.backView);
        make.height.equalTo(@50);
        make.bottom.equalTo(self.backView).offset(-TabbarSafeMargin);
    }];
    
    
    
    
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(255, 255, 255, 1);
        _backView.layer.cornerRadius = 12;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"分享到";
        _titleLabel.textColor = RGBA(51, 51, 51, 1);
        _titleLabel.font = kFont(15);
        _titleLabel.textAlignment = 1;
    }
    return _titleLabel;
}

- (UIImageView *)weChatImageV{
    if (!_weChatImageV) {
        _weChatImageV = [[UIImageView alloc]init];
        _weChatImageV.image = [UIImage imageNamed:@"分享-微信"];
    }
    return _weChatImageV;
}

- (UILabel *)weChatLabel{
    if (!_weChatLabel) {
        _weChatLabel = [[UILabel alloc]init];
        _weChatLabel.text = @"微信";
        _weChatLabel.textColor = RGBA(51, 51, 51, 1);
        _weChatLabel.font = kFont(14);
        _weChatLabel.textAlignment = 1;
    }
    return _weChatLabel;
}

- (UIButton *)weChatBtn{
    if (!_weChatBtn) {
        _weChatBtn = [[UIButton alloc]init];
        _weChatBtn.backgroundColor = UIColor.clearColor;
        [_weChatBtn addTarget:self action:@selector(clicTheWeChat) forControlEvents:(UIControlEventTouchDown)];
    }
    return _weChatBtn;
}


- (UIImageView *)circleFriendsImageV{
    if (!_circleFriendsImageV) {
        _circleFriendsImageV = [[UIImageView alloc]init];
        _circleFriendsImageV.image = [UIImage imageNamed:@"朋友圈"];
    }
    return _circleFriendsImageV;
}

- (UILabel *)circleFriendsLabel{
    if (!_circleFriendsLabel) {
        _circleFriendsLabel = [[UILabel alloc]init];
        _circleFriendsLabel.text = @"朋友圈";
        _circleFriendsLabel.textColor = RGBA(51, 51, 51, 1);
        _circleFriendsLabel.font = kFont(14);
        _circleFriendsLabel.textAlignment = 1;
    }
    return _circleFriendsLabel;
}

- (UIButton *)circleFriendsBtn{
    if (!_circleFriendsBtn) {
        _circleFriendsBtn = [[UIButton alloc]init];
        _circleFriendsBtn.backgroundColor = UIColor.clearColor;
        [_circleFriendsBtn addTarget:self action:@selector(clickTheCircleFriends) forControlEvents:(UIControlEventTouchDown)];
    }
    return _circleFriendsBtn;
}

- (UIButton *)cancelBtn{
    if (!_cancelBtn) {
        _cancelBtn = [[UIButton alloc]init];
        [_cancelBtn setTitle:@"取消" forState:(UIControlStateNormal)];
        [_cancelBtn setTitleColor:RGBA(51, 51, 51, 1) forState:(UIControlStateNormal)];
        _cancelBtn.titleLabel.font = kFont_Medium(15);
        _cancelBtn.backgroundColor = RGBA(238, 238, 238, 1);
        [_cancelBtn addTarget:self action:@selector(dismiss) forControlEvents:(UIControlEventTouchDown)];
        
    }
    return _cancelBtn;
}
-(void)dismiss{
    [self.popController dismiss];
}

-(void)setModel:(ShareModel *)model{
    _model = model;
}

- (void)clicTheWeChat {
    NSLog(@"点击了分享微信");
    //分享到微信
    //创建分享消息对象
    
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:self.model.title
                       descr:self.model.content
                   thumImage:[UIImage imageNamed:@"AppIcon"]];
    //设置网页地址
    shareObject.webpageUrl =@"http://quyangapp.en.com";
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:UMSocialPlatformType_WechatSession messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            NSLog(@"************Share fail with error %@*********",error);
        }else{
            NSLog(@"response data is %@",data);
        }
    }];
}
- (void)clickTheCircleFriends {
    NSLog(@"点击了分享朋友圈");
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    //设置文本
    messageObject.text = self.model.title;
    //创建图片内容对象
    UMShareImageObject *shareObject = [[UMShareImageObject alloc] init];
    //如果有缩略图，则设置缩略图
    shareObject.thumbImage = [UIImage imageNamed:@"AppIcon"];
    [shareObject setShareImage:@"https://www.umeng.com/img/index/demo/1104.4b2f7dfe614bea70eea4c6071c72d7f5.jpg"];
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:UMSocialPlatformType_WechatTimeLine messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            NSLog(@"************Share fail with error %@*********",error);
        }else{
            NSLog(@"response data is %@",data);
        }
    }];
}

@end
