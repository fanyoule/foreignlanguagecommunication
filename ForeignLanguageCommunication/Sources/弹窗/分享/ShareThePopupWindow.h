//
//  ShareThePopupWindow.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import <UIKit/UIKit.h>
#import "ShareModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShareThePopupWindow : UIViewController
@property (nonatomic, strong) ShareModel *model;
@end

NS_ASSUME_NONNULL_END
