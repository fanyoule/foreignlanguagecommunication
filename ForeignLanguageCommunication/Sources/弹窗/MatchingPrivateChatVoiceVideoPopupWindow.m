//
//  MatchingPrivateChatVoiceVideoPopupWindow.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//  匹配弹窗

#import "MatchingPrivateChatVoiceVideoPopupWindow.h"
#import <HWPopController/HWPop.h>
@interface MatchingPrivateChatVoiceVideoPopupWindow ()
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 解锁*/
@property (nonatomic, strong)UIButton *unlockBtn;
/** 成为会员*/
@property (nonatomic, strong)UIButton *becomeBtn;

@end

@implementation MatchingPrivateChatVoiceVideoPopupWindow

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.contentSizeInPop = [UIScreen mainScreen].bounds.size;
    self.popController.containerView.backgroundColor = [UIColor clearColor];
    
    UIView *returnView = [[UIView alloc]init];
    returnView.backgroundColor = [UIColor clearColor];
    returnView.frame = self.view.frame;
    [self.view addSubview:returnView];
    [returnView whenTapped:^{
        [self dismissViewControllerAnimated:YES completion:^{

        }];
    }];
    
    
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.unlockBtn];
    [self.backView addSubview:self.becomeBtn];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(70);
        make.right.equalTo(self.view).offset(-70);
        make.height.equalTo(@230);
        make.center.equalTo(self.view);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.backView).offset(25);
        make.left.equalTo(self.backView).offset(35);
        make.right.equalTo(self.backView).offset(-35);
    }];
    [self.unlockBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(25);
        make.width.equalTo(@215);
        make.height.equalTo(@40);
    }];
    [self.becomeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.unlockBtn.mas_bottom).offset(25);
        make.width.height.equalTo(self.unlockBtn);
    }];
    
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.cornerRadius = 12;
        _backView.layer.masksToBounds = YES;
    }
    return _backView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"今日匹配次数已达到上限，请购买次数（非会员10次）";
        _titleLabel.textColor = RGBA(51, 51, 51, 1);
        _titleLabel.font = kFont_Medium(15);
        _titleLabel.textAlignment = 1;
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}
- (UIButton *)unlockBtn{
    if (!_unlockBtn) {
        _unlockBtn = [[UIButton alloc]init];
        [_unlockBtn setTitle:@"解锁匹配（10元）" forState:(UIControlStateNormal)];
        [_unlockBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _unlockBtn.titleLabel.font = kFont_Medium(15);
        _unlockBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _unlockBtn.layer.cornerRadius = 6;
        _unlockBtn.clipsToBounds = YES;
    }
    return _unlockBtn;
}

- (UIButton *)becomeBtn{
    if (!_becomeBtn) {
        _becomeBtn = [[UIButton alloc]init];
        [_becomeBtn setTitle:@"成为会员，提高每日次数" forState:(UIControlStateNormal)];
        [_becomeBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _becomeBtn.titleLabel.font = kFont_Medium(15);
        
        _becomeBtn.layer.cornerRadius = 6;
        _becomeBtn.clipsToBounds = YES;
        _becomeBtn.layer.borderColor = RGBA(103, 207, 206, 1).CGColor;
        _becomeBtn.layer.borderWidth = 0.5;
    }
    return _becomeBtn;
}

@end
