//
//  PayThePopupWindow.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import <UIKit/UIKit.h>
#import "CourseDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^clickConfirmThePayment)(NSInteger judge);
@interface PayThePopupWindow : UIViewController

@property (nonatomic, assign)CGFloat discount;

@property (nonatomic, strong)CourseDetailsModel *model;

@property (nonatomic, copy)clickConfirmThePayment payment;
@end

NS_ASSUME_NONNULL_END
