//
//  ApplePayViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/4.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApplePayViewController : UIViewController
/** 选中的金额*/
@property (nonatomic, strong) NSString *selectAmount;
@property (nonatomic, copy) NSString *applePayId;
@end

NS_ASSUME_NONNULL_END
