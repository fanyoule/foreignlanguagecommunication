//
//  ApplePayViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/4.
//

#import "ApplePayViewController.h"
#import "PayTheAmountTableViewCell.h"
#import "AlipayWeChatMethodPaymentTableViewCell.h"
#import <HWPopController/HWPop.h>
#import <StoreKit/StoreKit.h>

@interface ApplePayViewController ()<UITableViewDelegate, UITableViewDataSource, SKPaymentTransactionObserver, SKProductsRequestDelegate>
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 叉*/
@property (nonatomic, strong)UIButton *forkBtn;
/** 确认按钮*/
@property (nonatomic, strong)UIButton *confirmBtn;

@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, assign) int payIndex;

@end

@implementation ApplePayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.payIndex = 0;
    self.contentSizeInPop = [UIScreen mainScreen].bounds.size;
    self.popController.containerView.backgroundColor = [UIColor clearColor];
    
    UIView *returnView = [[UIView alloc]init];
    returnView.backgroundColor = [UIColor clearColor];
    returnView.frame = self.view.frame;
    [self.view addSubview:returnView];
    [returnView whenTapped:^{
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }];
    
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.forkBtn];
    
    [self.backView addSubview:self.tableView];
    
    [self.backView addSubview:self.confirmBtn];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.equalTo(@312);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.backView).offset(15);
    }];
    [self.forkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.backView).offset(-15);
        make.width.height.equalTo(@30);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.left.right.equalTo(self.backView);
        make.bottom.equalTo(self.confirmBtn.mas_top);
    }];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(15);
        make.right.equalTo(self.backView).offset(-15);
        make.height.equalTo(@50);
        make.bottom.equalTo(self.backView).offset(-10-TabbarSafeMargin);;
    }];
}

#pragma mark -delegate datasource-
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 175;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"ApplePayTheAmountTableViewCell";
    PayTheAmountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PayTheAmountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.rulesLabel.text = @"";
    cell.backgroundColor = RGBA(248, 248, 248, 1);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (self.selectAmount == nil) {
        cell.amountLabel.text = @"0";
    } else {
        cell.amountLabel.text = self.selectAmount;
    }
    
    cell.EImageV.hidden = YES;
    return cell;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]init];
        _tableView.backgroundColor = RGBA(248, 248, 248, 1);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else self.automaticallyAdjustsScrollViewInsets = NO;
    }
    return _tableView;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(248, 248, 248, 1);
        _backView.layer.cornerRadius = 10;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = UILabel.new;
        _titleLabel.text = @"支付";
        _titleLabel.textColor = RGBA(34, 34, 34, 1);
        _titleLabel.font = kFont_Bold(17);
        _titleLabel.textAlignment = 1;
    }
    return _titleLabel;
}

- (UIButton *)forkBtn {
    if (!_forkBtn) {
        _forkBtn = UIButton.new;
        [_forkBtn setImage:[UIImage imageNamed:@"关  闭"] forState:(UIControlStateNormal)];
        [_forkBtn  addTarget:self action:@selector(shutDown) forControlEvents:(UIControlEventTouchDown)];
    }
    return _forkBtn;
}

- (UIButton *)confirmBtn {
    if (!_confirmBtn) {
        _confirmBtn = UIButton.new;
        [_confirmBtn setTitle:@"确认支付" forState:(UIControlStateNormal)];
        [_confirmBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _confirmBtn.titleLabel.font = kFont_Bold(16);
        _confirmBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _confirmBtn.layer.cornerRadius = 7;
        _confirmBtn.clipsToBounds = YES;
        [_confirmBtn addTarget:self action:@selector(determineThePayment) forControlEvents:(UIControlEventTouchDown)];
    }
    return _confirmBtn;
}

- (void)shutDown {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

// MARK: 确定支付
- (void)determineThePayment {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"充值E币" message:[NSString stringWithFormat:@"你确定花费%@元购买吗？",self.selectAmount] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"付费" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([SKPaymentQueue canMakePayments]) {
            //监听购买结果
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            [self requestProductData];
        } else {
            NSLog(@"不允许程序内付费");
        }
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
}
//去苹果服务器请求商品
- (void)requestProductData {
    NSArray *product = [[NSArray alloc] initWithObjects:self.applePayId, nil];
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:nsset];
    request.delegate = self;
    [request start];
}
//收到产品返回信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"--------------收到产品反馈消息---------------------");
    NSArray *product = response.products;
    NSLog(@"productID:%@", response.invalidProductIdentifiers);
    if(product.count == 0){
        [self showError:@"查找不到商品信息"];
        return;
    }
    [SVProgressHUD showWithStatus:@"支付中"];
    SKProduct *p = nil;
    for(SKProduct *pro in product) {
        NSLog(@"%@", [pro description]);
        NSLog(@"%@", [pro localizedTitle]);
        NSLog(@"%@", [pro localizedDescription]);
        NSLog(@"%@", [pro price]);
        NSLog(@"%@", [pro productIdentifier]);
        
        if([pro.productIdentifier isEqualToString: self.applePayId]){
            p = pro;
        }
    }
    SKPayment *payment = [SKPayment paymentWithProduct:p];
    NSLog(@"发送购买请求");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
//请求失败
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self showError:@"支付失败"];
}

- (void)requestDidFinish:(SKRequest *)request {
    [SVProgressHUD dismiss];
}

//监听购买结果
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transaction{
    [SVProgressHUD dismiss];
    for(SKPaymentTransaction *tran in transaction){
        
        switch(tran.transactionState) {
            case SKPaymentTransactionStatePurchased:{
                [self verifyPurchaseWithPaymentTransaction:tran];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"buyed"];
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStatePurchasing:
                break;
            case SKPaymentTransactionStateRestored:{
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStateFailed:{
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
                [self showError:@"购买失败"];
            }
                break;
            default:
                break;
        }
    }
}

//交易结束
- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"交易结束");
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)dealloc {
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)verifyPurchaseWithPaymentTransaction:(SKPaymentTransaction *)transaction {
    [SVProgressHUD dismiss];
    // 验证凭据，获取到苹果返回的交易凭据
    // appStoreReceiptURL iOS7.0增加的，购买交易完成后，会将凭据存放在该地址
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    // 从沙盒中获取到购买凭据
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    //向自己的服务器验证购买凭证
    //最好将返回的数据转换成 base64再传给后台,后台再转换回来;以防返回字符串中有特字符传给后台显示空
    NSString *resu=[[NSString alloc]initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
    NSString *environment = [self environmentForReceipt:resu];
    
    // 发送网络POST请求，对购买凭据进行验证
    //测试验证地址:https://sandbox.itunes.apple.com/verifyReceipt
    //正式验证地址:https://buy.itunes.apple.com/verifyReceipt
    NSURL *url = [NSURL URLWithString:AppStore];
    if ([environment isEqualToString:@"environment=Sandbox"]) {
        url = [NSURL URLWithString:SANDBOX];
    }
    NSMutableURLRequest *urlRequest =
    [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f];
    urlRequest.HTTPMethod = @"POST";
    NSString *encodeStr = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    NSString *payload = [NSString stringWithFormat:@"{\"receipt-data\" : \"%@\"}", encodeStr];
    NSData *payloadData = [payload dataUsingEncoding:NSUTF8StringEncoding];
    urlRequest.HTTPBody = payloadData;
    // 提交验证请求，并获得官方的验证JSON结果 iOS9后更改了另外的一个方法
    NSData *result = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error:nil];
    // 官方验证结果为空
    if (result == nil) {
        [self showError:@"购买失败"];
        return;
    }
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingAllowFragments error:nil];
    if (dict != nil) {
        // 比对字典中以下信息基本上可以保证数据安全
        NSLog(@"字典%@",dict);
        NSLog(@"验证成功！购买的商品是：%@  %@", [dict objectForKey:@"product_id"], [dict objectForKey:@"productName"]);
        NSArray *array = dict[@"receipt"][@"in_app"];
//        NSDictionary *inAppDict = [array lastObject];
        [self showText:@"购买成功"];
        dispatch_async(dispatch_get_main_queue(), ^{
//            [g_server iOSAsynChronous:self.resultObject recepit:encodeStr toView:self];
        });
        
    } else {
        [self showError:@"购买失败"];
    }
}
//收据的环境判断；
- (NSString *)environmentForReceipt:(NSString * )str {
    str= [str stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    str=[str stringByReplacingOccurrencesOfString:@" " withString:@""];
    str=[str stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSArray * arr=[str componentsSeparatedByString:@";"];
    //存储收据环境的变量
    NSString * environment=arr[2];
    return environment;
}
@end
