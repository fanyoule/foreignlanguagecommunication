//
//  ECoinMethodPaymentTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import "ECoinMethodPaymentTableViewCell.h"
@interface ECoinMethodPaymentTableViewCell ()
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** E币*/
@property (nonatomic, strong)UIImageView *EImageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;


/** 折扣背景*/
@property (nonatomic, strong)UIImageView *discountBackView;

@end

@implementation ECoinMethodPaymentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self.contentView addSubview:self.backView];
    [self.contentView addSubview:self.EImageV];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.chooseBtn];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(5);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@68);
    }];
    [self.EImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(15);
        make.width.height.equalTo(@23);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.EImageV.mas_right).offset(5);
    }];
    [self.chooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.right.equalTo(self.backView).offset(-15);
    }];
    
    [self.backView addSubview:self.discountBackView];
    [self.backView addSubview:self.discountLabel];
    [self.discountBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView).offset(3);
        make.left.equalTo(self.titleLabel.mas_right).offset(5);
    }];
    [self.discountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.discountBackView);
        make.centerY.equalTo(self.discountBackView).offset(-3);
    }];
    
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [[UILabel alloc]init];
        _backView.backgroundColor = RGBA(255, 255, 255, 1);
        _backView.layer.cornerRadius = 10;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UIImageView *)EImageV{
    if (!_EImageV) {
        _EImageV = [[UIImageView alloc]init];
        _EImageV.image = [UIImage imageNamed:@"钱包-钱"];
    }
    return _EImageV;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"E币支付";
        _titleLabel.textColor = RGBA(34, 34, 34, 1);
        _titleLabel.font = kFont(16);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UIButton *)chooseBtn{
    if (!_chooseBtn) {
        _chooseBtn = [[UIButton alloc]init];
        [_chooseBtn setImage:[UIImage imageNamed:@"未选"] forState:(UIControlStateNormal)];
        [_chooseBtn setImage:[UIImage imageNamed:@"选中"] forState:(UIControlStateSelected)];
    }
    return _chooseBtn;
}

- (UIImageView *)discountBackView{
    if (!_discountBackView) {
        _discountBackView = [[UIImageView alloc]init];
        _discountBackView.image = [UIImage imageNamed:@"圆角矩形 5"];
    }
    return _discountBackView;
}

- (UILabel *)discountLabel{
    if (!_discountLabel) {
        _discountLabel = [[UILabel alloc]init];
        _discountLabel.text = @"7.6折";
        _discountLabel.textColor = RGBA(255, 255, 255, 1);
        _discountLabel.font = kFont(11);
        _discountLabel.textAlignment = 1;
    }
    return _discountLabel;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
