//
//  PayTheAmountTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import "PayTheAmountTableViewCell.h"
@interface PayTheAmountTableViewCell ()
/** 背景*/
@property (nonatomic, strong)UIView *backView;



@end
@implementation PayTheAmountTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.backView];
    [self.contentView addSubview:self.moneyLabel];
    [self.contentView addSubview:self.amountLabel];
    [self.contentView addSubview:self.rulesLabel];
    [self.contentView addSubview:self.EImageV];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(-5);
        make.height.equalTo(@150);
    }];
    
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.backView).offset(40);
        
    }];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.amountLabel);
        make.right.equalTo(self.amountLabel.mas_left);
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.moneyLabel.mas_bottom).offset(25);
    }];
    [self.EImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.amountLabel);
        make.left.equalTo(self.amountLabel.mas_right).offset(5);
        make.height.width.equalTo(@15);
    }];
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.cornerRadius = 10;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc]init];
        _moneyLabel.text = @"￥";
        _moneyLabel.textColor = RGBA(34, 34, 34, 1);
        _moneyLabel.font = kFont_Bold(24);
        _moneyLabel.textAlignment = 0;
    }
    return _moneyLabel;
}

- (UILabel *)amountLabel{
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc]init];
        _amountLabel.text = @"100.00";
        _amountLabel.textColor = RGBA(34, 34, 34, 1);
        _amountLabel.font = kFont_Bold(35);
        _amountLabel.textAlignment = 0;
    }
    return _amountLabel;
}
- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"支付成后请耐心等待老师与您联系";
        _rulesLabel.textColor = RGBA(153, 153, 153, 1);
        _rulesLabel.font = kFont(12);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}

- (UIImageView *)EImageV{
    if (!_EImageV) {
        _EImageV = [[UIImageView alloc]init];
        _EImageV.image = kImage(@"钱包-钱");
    }
    return _EImageV;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
