//
//  AlipayWeChatMethodPaymentTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^AlipayWeChatMethodPaymentTableViewCellBlock)(int);
@interface AlipayWeChatMethodPaymentTableViewCell : UITableViewCell
/** 支付宝选择按钮*/
@property (nonatomic, strong)UIButton *alipayBtn;
/** 微信选择按钮*/
@property (nonatomic, strong)UIButton *weChatBtn;
@property (nonatomic, copy) AlipayWeChatMethodPaymentTableViewCellBlock block;
@end

NS_ASSUME_NONNULL_END
