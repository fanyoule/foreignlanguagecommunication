//
//  AlipayWeChatMethodPaymentTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import "AlipayWeChatMethodPaymentTableViewCell.h"
@interface AlipayWeChatMethodPaymentTableViewCell ()
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 微信图标*/
@property (nonatomic, strong)UIImageView *weChatImageV;
/** 微信标题*/
@property (nonatomic, strong)UILabel *weChatLabel;


/** 支付宝图标*/
@property (nonatomic, strong)UIImageView *alipayImageV;
/** 支付宝标题*/
@property (nonatomic, strong)UILabel *alipayLabel;


@end
@implementation AlipayWeChatMethodPaymentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.weChatImageV];
    [self.backView addSubview:self.weChatLabel];
    [self.backView addSubview:self.weChatBtn];
    [self.backView addSubview:self.alipayImageV];
    [self.backView addSubview:self.alipayLabel];
    [self.backView addSubview:self.alipayBtn];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(5);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(-5);
    }];
    [self.weChatImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.backView.mas_centerY).offset(-15);
        make.left.equalTo(self.backView).offset(15);
        make.width.height.equalTo(@23);
    }];
    [self.weChatLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.weChatImageV);
        make.left.equalTo(self.weChatImageV.mas_right).offset(5);
    }];
    [self.weChatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.weChatImageV);
        make.right.equalTo(self.backView).offset(-15);
    }];
    [self.alipayImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView.mas_centerY).offset(15);
        make.left.equalTo(self.backView).offset(15);
        make.width.height.equalTo(@23);
    }];
    [self.alipayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.alipayImageV);
        make.left.equalTo(self.alipayImageV.mas_right).offset(5);
    }];
    [self.alipayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.alipayImageV);
        make.right.equalTo(self.backView).offset(-15);
    }];
    
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(255, 255, 255, 1);
        _backView.layer.cornerRadius = 10;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UIImageView *)weChatImageV{
    if (!_weChatImageV) {
        _weChatImageV = [[UIImageView alloc]init];
        _weChatImageV.image = [UIImage imageNamed:@"微信"];
    }
    return _weChatImageV;
}

- (UILabel *)weChatLabel{
    if (!_weChatLabel) {
        _weChatLabel = [[UILabel alloc]init];
        _weChatLabel.text = @"微信支付";
        _weChatLabel.textColor = RGBA(34, 34, 34, 1);
        _weChatLabel.font = kFont(16);
        _weChatLabel.textAlignment = 0;
    }
    return _weChatLabel;
}
- (UIButton *)weChatBtn{
    if (!_weChatBtn) {
        _weChatBtn = [[UIButton alloc]init];
        [_weChatBtn setImage:[UIImage imageNamed:@"未选"] forState:(UIControlStateNormal)];
        [_weChatBtn setImage:[UIImage imageNamed:@"选中"] forState:(UIControlStateSelected)];
        [_weChatBtn addTarget:self action:@selector(weChatBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _weChatBtn;
}

- (void)weChatBtnClick {
    self.weChatBtn.selected = YES;
    self.alipayBtn.selected = NO;
    if (self.block) {
        self.block(1);
    }
}
- (void)aliPayBtnClick {
    self.weChatBtn.selected = NO;
    self.alipayBtn.selected = YES;
    if (self.block) {
        self.block(2);
    }
}
- (UIImageView *)alipayImageV{
    if (!_alipayImageV) {
        _alipayImageV = [[UIImageView alloc]init];
        _alipayImageV.image = [UIImage imageNamed:@"支付宝"];
    }
    return _alipayImageV;
}

- (UILabel *)alipayLabel{
    if (!_alipayLabel) {
        _alipayLabel = [[UILabel alloc]init];
        _alipayLabel.text = @"支付宝支付";
        _alipayLabel.textColor = RGBA(34, 34, 34, 1);
        _alipayLabel.font = kFont(16);
        _alipayLabel.textAlignment = 0;
    }
    return _alipayLabel;
}
- (UIButton *)alipayBtn{
    if (!_alipayBtn) {
        _alipayBtn = [[UIButton alloc]init];
        [_alipayBtn setImage:[UIImage imageNamed:@"未选"] forState:(UIControlStateNormal)];
        [_alipayBtn setImage:[UIImage imageNamed:@"选中"] forState:(UIControlStateSelected)];
        [_alipayBtn addTarget:self action:@selector(aliPayBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _alipayBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
