//
//  ECoinMethodPaymentTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ECoinMethodPaymentTableViewCell : UITableViewCell
/** 折扣*/
@property (nonatomic, strong)UILabel *discountLabel;
/** 选择*/
@property (nonatomic, strong)UIButton *chooseBtn;
@end

NS_ASSUME_NONNULL_END
