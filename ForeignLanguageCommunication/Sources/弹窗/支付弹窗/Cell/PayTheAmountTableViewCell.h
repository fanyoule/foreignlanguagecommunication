//
//  PayTheAmountTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PayTheAmountTableViewCell : UITableViewCell
/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;
/** 钱*/
@property (nonatomic, strong)UILabel *moneyLabel;
/** 金额*/
@property (nonatomic, strong)UILabel *amountLabel;
/** E币*/
@property (nonatomic, strong)UIImageView *EImageV;
@end

NS_ASSUME_NONNULL_END
