//
//  TopUpPopupWindow.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopUpPopupWindow : UIViewController

/** 选中的金额*/
@property (nonatomic, strong)NSString *selectAmount;
@end

NS_ASSUME_NONNULL_END
