//
//  TopUpPopupWindow.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/14.
//  支付弹窗 --- 不带E币支付

#import "TopUpPopupWindow.h"
#import "PayTheAmountTableViewCell.h"
#import "AlipayWeChatMethodPaymentTableViewCell.h"
#import <HWPopController/HWPop.h>
#import <StoreKit/StoreKit.h>


@interface TopUpPopupWindow ()<UITableViewDelegate,UITableViewDataSource,SKProductsRequestDelegate,SKPaymentTransactionObserver>
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 叉*/
@property (nonatomic, strong)UIButton *forkBtn;
/** 确认按钮*/
@property (nonatomic, strong)UIButton *confirmBtn;

@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, assign) int payIndex;

@end

@implementation TopUpPopupWindow






- (void)viewDidLoad {
    [super viewDidLoad];
    self.payIndex = 0;
    self.contentSizeInPop = [UIScreen mainScreen].bounds.size;
    self.popController.containerView.backgroundColor = [UIColor clearColor];
    
    
    UIView *returnView = [[UIView alloc]init];
    returnView.backgroundColor = [UIColor clearColor];
    returnView.frame = self.view.frame;
    [self.view addSubview:returnView];
    [returnView whenTapped:^{
        [self dismissViewControllerAnimated:YES completion:^{

        }];
    }];
    
    
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.forkBtn];
    
    [self.backView addSubview:self.tableView];
    
    [self.backView addSubview:self.confirmBtn];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
//        make.height.equalTo(@458);
        make.height.equalTo(@312);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.backView).offset(15);
    }];
    [self.forkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.backView).offset(-15);
        make.width.height.equalTo(@30);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.left.right.equalTo(self.backView);
        make.bottom.equalTo(self.confirmBtn.mas_top);
    }];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(15);
        make.right.equalTo(self.backView).offset(-15);
        make.height.equalTo(@50);
        make.bottom.equalTo(self.backView).offset(-10-TabbarSafeMargin);;
    }];
    
    
}

#pragma mark -delegate datasource-
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
//    return 2;
}
// cell 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 175;
    }else{
        return 146;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        PayTheAmountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PayTheAmountTableViewCell"];
            if (!cell) {
                cell = [[PayTheAmountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PayTheAmountTableViewCell"];
            }
        cell.backgroundColor = RGBA(248, 248, 248, 1);
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if (self.selectAmount == nil) {
            cell.amountLabel.text = @"0";
        }else{
            cell.amountLabel.text = self.selectAmount;
        }
        
        cell.EImageV.hidden = YES;
        return cell;
    }else{
        AlipayWeChatMethodPaymentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AlipayWeChatMethodPaymentTableViewCell"];
        if (!cell) {
            cell = [[AlipayWeChatMethodPaymentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AlipayWeChatMethodPaymentTableViewCell"];
        }
        cell.backgroundColor = RGBA(248, 248, 248, 1);
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        kWeakSelf(self)
        cell.block = ^(int index) {
            weakself.payIndex = index;
        };
        return cell;
        
    }
    
}


- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]init];
        _tableView.backgroundColor = RGBA(248, 248, 248, 1);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    return _tableView;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(248, 248, 248, 1);
        _backView.layer.cornerRadius = 10;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = UILabel.new;
        _titleLabel.text = @"支付";
        _titleLabel.textColor = RGBA(34, 34, 34, 1);
        _titleLabel.font = kFont_Bold(17);
        _titleLabel.textAlignment = 1;
    }
    return _titleLabel;
}

- (UIButton *)forkBtn{
    if (!_forkBtn) {
        _forkBtn = UIButton.new;
        
        [_forkBtn setImage:[UIImage imageNamed:@"关  闭"] forState:(UIControlStateNormal)];
        [_forkBtn  addTarget:self action:@selector(shutDown) forControlEvents:(UIControlEventTouchDown)];
    }
    return _forkBtn;
}

- (UIButton *)confirmBtn{
    if (!_confirmBtn) {
        _confirmBtn = UIButton.new;
        [_confirmBtn setTitle:@"确认支付" forState:(UIControlStateNormal)];
        [_confirmBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _confirmBtn.titleLabel.font = kFont_Bold(16);
        _confirmBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _confirmBtn.layer.cornerRadius = 7;
        _confirmBtn.clipsToBounds = YES;
        [_confirmBtn addTarget:self action:@selector(determineThePayment) forControlEvents:(UIControlEventTouchDown)];
    }
    return _confirmBtn;
}

-(void)shutDown{
    
    [self dismissViewControllerAnimated:YES completion:^{

    }];
    
}

// MARK: 确定支付
-(void)determineThePayment {
//    if (self.payIndex == 0) {
//        [self showText:@"请选择支付方式"];
//        return;
//    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"充值E币" message:[NSString stringWithFormat:@"你确定花费%@元购买吗？",self.selectAmount] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"付费" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//    [IHUtility addWaitingView:@"支付中"];
    //监听购买结果
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        if([SKPaymentQueue canMakePayments]){
            [self requestProductData:@"com.IntelligentNetwork.QuickIdentificationDiagram.purchase1"];
        }else{
            NSLog(@"不允许程序内付费");
        }
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    // 弹出对话框
    [self presentViewController:alert animated:true completion:nil];
}
//去苹果服务器请求商品
- (void)requestProductData:(NSString *)type{
    
    NSArray *product = [[NSArray alloc] initWithObjects:type,nil];
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:nsset];
    request.delegate = self;
    [request start];
}
//收到产品返回信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    
    NSLog(@"--------------收到产品反馈消息---------------------");
    NSArray *product = response.products;
    NSLog(@"productID:%@", response.invalidProductIdentifiers);
    if(product.count == 0){
        [self showError:@"查找不到商品信息"];
//        [WHToast showMessage:@"查找不到商品信息"  duration:1 finishHandler:^{
//        }];
        return;
    }
    [SVProgressHUD showWithStatus:@"支付中"];
//    [IHUtility addWaitingView:@"支付中"];
    SKProduct *p = nil;
    for(SKProduct *pro in product) {
        NSLog(@"%@", [pro description]);
        NSLog(@"%@", [pro localizedTitle]);
        NSLog(@"%@", [pro localizedDescription]);
        NSLog(@"%@", [pro price]);
        NSLog(@"%@", [pro productIdentifier]);
        
        if([pro.productIdentifier isEqualToString: @"com.IntelligentNetwork.QuickIdentificationDiagram.purchase1"]){
            p = pro;
        }
    }
    SKPayment *payment = [SKPayment paymentWithProduct:p];
    NSLog(@"发送购买请求");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
//请求失败
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    [SVProgressHUD dismiss];
//    [IHUtility removeWaitingView];
    [self showError:@"支付失败"];
//    [WHToast showMessage:@"支付失败"  duration:1 finishHandler:^{
//    }];
}

- (void)requestDidFinish:(SKRequest *)request{
    [SVProgressHUD dismiss];
//    [IHUtility removeWaitingView];
}

//监听购买结果
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transaction{
    [SVProgressHUD dismiss];
//    [IHUtility removeWaitingView];
    for(SKPaymentTransaction *tran in transaction){
        [self verifyPurchaseWithPaymentTransaction:[NSNumber numberWithInt:tran.transactionState]];
        switch(tran.transactionState) {
            case SKPaymentTransactionStatePurchased:{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"buyed"];
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStatePurchasing:
                break;
            case SKPaymentTransactionStateRestored:{
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStateFailed:{
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
                [self showError:@"购买失败"];
            }
                break;
            default:
                break;
        }
    }
}

//交易结束
- (void)completeTransaction:(SKPaymentTransaction *)transaction{
    NSLog(@"交易结束");
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
}


- (void)dealloc{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

-(void)verifyPurchaseWithPaymentTransaction:(NSNumber*)resultState{
    //从沙盒中获取交易凭证并且拼接成请求体数据
    NSURL *receiptUrl=[[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receiptData=[NSData dataWithContentsOfURL:receiptUrl];
}

@end
