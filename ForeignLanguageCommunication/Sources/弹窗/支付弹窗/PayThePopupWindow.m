//
//  PayThePopupWindow.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//  支付弹窗 --- 带E币支付

#import "PayThePopupWindow.h"
#import "PayTheAmountTableViewCell.h"
#import "ECoinMethodPaymentTableViewCell.h"
#import "AlipayWeChatMethodPaymentTableViewCell.h"
#import <HWPopController/HWPop.h>

@interface PayThePopupWindow ()<UITableViewDelegate,UITableViewDataSource> 
/** 背景*/
@property (nonatomic, strong)UIView *backView;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 叉*/
@property (nonatomic, strong)UIButton *forkBtn;
/** 确认按钮*/
@property (nonatomic, strong)UIButton *confirmBtn;

@property (nonatomic, strong)UITableView *tableView;

@property (nonatomic, strong)UIButton *optionsBtn;

/** 判断显示的是e币 还是钱*/
@property (nonatomic, assign)NSInteger judge;
@end

@implementation PayThePopupWindow

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentSizeInPop = [UIScreen mainScreen].bounds.size;
    self.popController.containerView.backgroundColor = [UIColor clearColor];
    
    
    UIView *returnView = [[UIView alloc]init];
    returnView.backgroundColor = [UIColor clearColor];
    returnView.frame = self.view.frame;
    [self.view addSubview:returnView];
    [returnView whenTapped:^{
        [self dismissViewControllerAnimated:YES completion:^{

        }];
    }];
    
    
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.titleLabel];
    [self.backView addSubview:self.forkBtn];
    
    [self.backView addSubview:self.tableView];
    
    [self.backView addSubview:self.confirmBtn];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.equalTo(@518);
//        make.height.equalTo(@392);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.backView).offset(15);
    }];
    [self.forkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self.backView).offset(-15);
        make.width.height.equalTo(@30);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.left.right.equalTo(self.backView);
        make.bottom.equalTo(self.confirmBtn.mas_top);
    }];
    
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).offset(15);
        make.right.equalTo(self.backView).offset(-15);
        make.height.equalTo(@50);
        make.bottom.equalTo(self.backView).offset(-10-TabbarSafeMargin);;
    }];
    
    self.judge = 1;
}

#pragma mark -delegate datasource-
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
//    return 2;
}
// cell 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 175;
    }else if (indexPath.row == 1) {
        return 78;
    }else{
        return 146;
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        PayTheAmountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PayTheAmountTableViewCell"];
            if (!cell) {
                cell = [[PayTheAmountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PayTheAmountTableViewCell"];
            }
        cell.backgroundColor = RGBA(248, 248, 248, 1);
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if (self.judge == 1) {
            cell.amountLabel.text = [NSString stringWithFormat:@"%0.0lf",(self.model.price * 100)*self.discount/10];
            cell.moneyLabel.hidden = YES;
            cell.EImageV.hidden = NO;
        }else{
            cell.amountLabel.text = [NSString stringWithFormat:@"%ld",self.model.price];
            cell.EImageV.hidden = YES;
            cell.moneyLabel.hidden = NO;
        }
        
        
        cell.EImageV.tag = 700;
        cell.amountLabel.tag = 800;
        cell.moneyLabel.tag = 900;
        return cell;
    }else if (indexPath.row == 1) {
        ECoinMethodPaymentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ECoinMethodPaymentTableViewCell"];
            if (!cell) {
                cell = [[ECoinMethodPaymentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ECoinMethodPaymentTableViewCell"];
            }
        cell.backgroundColor = RGBA(248, 248, 248, 1);
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.discountLabel.text = [NSString stringWithFormat:@"%0.1lf折",self.discount];
        [cell.chooseBtn addTarget:self action:@selector(selectPaymentMethod:) forControlEvents:(UIControlEventTouchDown)];
        cell.chooseBtn.tag = 500;
        if (self.judge == 1) {
            cell.chooseBtn.selected = YES;
        }else{
            cell.chooseBtn.selected = NO;
        }
        
        return cell;
    }else{
        AlipayWeChatMethodPaymentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AlipayWeChatMethodPaymentTableViewCell"];
        if (!cell) {
            cell = [[AlipayWeChatMethodPaymentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AlipayWeChatMethodPaymentTableViewCell"];
        }
        cell.backgroundColor = RGBA(248, 248, 248, 1);
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [cell.alipayBtn addTarget:self action:@selector(selectPaymentMethod:) forControlEvents:(UIControlEventTouchDown)];
        
        [cell.weChatBtn addTarget:self action:@selector(selectPaymentMethod:) forControlEvents:(UIControlEventTouchDown)];
        cell.alipayBtn.tag = 501;
        cell.weChatBtn.tag = 502;
        if (self.judge == 2) {
            cell.alipayBtn.selected = YES;
        }else{
            cell.alipayBtn.selected = NO;
        }
        if (self.judge == 3) {
            cell.weChatBtn.selected = YES;
        }else{
            cell.weChatBtn.selected = NO;
        }
        return cell;
        
    }
    
}
-(void)selectPaymentMethod:(UIButton *)button{
    if (button.tag == 500) {
        self.judge = 1;
    }else if (button.tag == 501){
        self.judge = 2;
    }else if (button.tag == 502){
        self.judge = 3;
    }
    [self.tableView reloadData];
}


- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]init];
        _tableView.backgroundColor = RGBA(248, 248, 248, 1);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    return _tableView;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(248, 248, 248, 1);
        _backView.layer.cornerRadius = 10;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = UILabel.new;
        _titleLabel.text = @"支付";
        _titleLabel.textColor = RGBA(34, 34, 34, 1);
        _titleLabel.font = kFont_Bold(17);
        _titleLabel.textAlignment = 1;
    }
    return _titleLabel;
}

- (UIButton *)forkBtn{
    if (!_forkBtn) {
        _forkBtn = UIButton.new;
        
        [_forkBtn setImage:[UIImage imageNamed:@"关  闭"] forState:(UIControlStateNormal)];
        [_forkBtn  addTarget:self action:@selector(shutDown) forControlEvents:(UIControlEventTouchDown)];
    }
    return _forkBtn;
}

- (UIButton *)confirmBtn{
    if (!_confirmBtn) {
        _confirmBtn = UIButton.new;
        [_confirmBtn setTitle:@"确认支付" forState:(UIControlStateNormal)];
        [_confirmBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _confirmBtn.titleLabel.font = kFont_Bold(16);
        _confirmBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _confirmBtn.layer.cornerRadius = 7;
        _confirmBtn.clipsToBounds = YES;
        [_confirmBtn addTarget:self action:@selector(determineThePayment) forControlEvents:(UIControlEventTouchDown)];
    }
    return _confirmBtn;
}

- (void)setModel:(CourseDetailsModel *)model{
    _model = model;
    [self.tableView reloadData];
}

- (void)setDiscount:(CGFloat)discount{
    _discount = discount;
    [self.tableView reloadData];
}
#pragma mark -- 确认支付
-(void)determineThePayment{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    if (self.judge == 1) {
        [RequestManager buyCourseByGoldWithUserId:[[UserInfoManager shared] getUserID] courseId:self.model.ID withSuccess:^(id  _Nullable response) {
            NSLog(@"E币支付 === %@",response);
            if (self.payment) {
                self.payment(1);
            }
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"失败 === %@",error);
            [self showText:@"余额不足,请充值"];
            if (self.payment) {
                self.payment(2);
            }
        }];
    } else if (self.judge == 2) { // 支付宝支付
        [RequestManager aliBuyCourseCourseId:self.model.ID userId:[[UserInfoManager shared] getUserID] success:^(id  _Nullable response) {
            [self tuningAlipayWithOrder:response[@"data"]];
        } withFail:^(NSError * _Nullable error) {
            [self showText:error.localizedDescription];
        }];
    } else if (self.judge == 3) { // 微信支付
        [RequestManager wxBuyCourseCourseId:self.model.ID userId:[[UserInfoManager shared] getUserID] success:^(id  _Nullable response) {
            [self tuningWxWith:response[@"data"]];
        } withFail:^(NSError * _Nullable error) {
            [self showText:error.localizedDescription];
        }];
        
    }
}
- (void)tuningWxWith:(NSDictionary *)dict {
    PayReq *req = [[PayReq alloc] init];
    req.partnerId = [dict objectForKey:@"partnerid"];
    req.prepayId = [dict objectForKey:@"prepayid"];
    req.nonceStr = [dict objectForKey:@"noncestr"];
    req.timeStamp = [[dict objectForKey:@"timestamp"] intValue];
    req.package = @"Sign=WXPay";//[dict objectForKey:@"package"];
    req.sign = [dict objectForKey:@"sign"];
    [WXApi sendReq:req completion:nil];
}

- (void)tuningAlipayWithOrder:(NSString *)signedString {
    // NOTE: 如果加签成功，则继续执行支付
    if (signedString != nil) {
        //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
        NSString *appScheme = @"sayEnglishapp";
        // NOTE: 调用支付结果开始支付
        [[AlipaySDK defaultService] payOrder:signedString fromScheme:appScheme callback:^(NSDictionary *resultDic) {

            SBJsonParser * resultParser = [[SBJsonParser alloc] init] ;
            NSDictionary *resultObject = [resultParser objectWithString:resultDic[@"result"]];
            NSDictionary *response = [resultObject objectForKey:@"alipay_trade_app_pay_response"];
            if ([response[@"msg"] isEqualToString:@"Success"]) {
                if (self.payment) {
                    self.payment(1);
                }
            } else {
                [self showText:@"支付失败"];
                if (self.payment) {
                    self.payment(2);
                }
            }
        }];
    }

}

-(void)shutDown{
    
    [self dismissViewControllerAnimated:YES completion:^{

    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
