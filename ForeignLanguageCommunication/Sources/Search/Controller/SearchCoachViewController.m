//
//  SearchCoachViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/16.
//  找教练

#import "SearchCoachViewController.h"
#import "FindCoachSortingFilterView.h"
#import "FindCoachCollectionViewCell.h"
#import "CoachWithCourseTableViewCell.h"
#import "CoachFilterView.h"
#import "CoachSearchViewController.h"
#import "MMComboBox.h"
#import "CoachDetailsViewController.h"// 教练详情
#import "CourseDetailsViewController.h"// 课程详情

@interface SearchCoachViewController ()
<UITableViewDelegate,
UITableViewDataSource,
UICollectionViewDelegate,
UICollectionViewDataSource,
MMComBoBoxViewDataSource,
MMComBoBoxViewDelegate,
JXCategoryViewDelegate,
JXCategoryListContainerViewDelegate
>
/**搜索框*/
@property (nonatomic, strong)UIView *searchView;
/**筛选  排序View*/
@property (nonatomic, strong)FindCoachSortingFilterView *findCoachSortingFilterView;

@property (nonatomic, strong)MMComBoBoxView *comBoBoxView;


@property (nonatomic, strong) NSMutableArray *coachModelArray;

@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;
@property (nonatomic, strong) JXCategoryIndicatorLineView *indicateView;
@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;

@property (nonatomic, strong) CoachFilterView *filterView;

@property (nonatomic, strong) NSMutableArray *filterDataArray;

@property (nonatomic, strong) NSDictionary *searchDic;

@property (nonatomic) int lastPage;

@property (nonatomic) int page;
@end

@implementation SearchCoachViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = CustomColor(@"#F8F8F8");
    
    self.page = 1;
    [self theSearchBox];
    [self addFindCoachSortingFilterView];
    [self setupSubView];
    [self getData];
    
}
- (void)setupSubView {
    [self.view addSubview:self.mainTableView];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    kWeakSelf(self);
    self.mainTableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        weakself.page = 1;
        [weakself getData];
    }];
    
    self.mainTableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
        [weakself getData];
    }];
    
    
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.navView.mas_bottom).offset(40);
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
    }];
    [self.mainTableView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.mainTableView);
    }];
    
}


- (JXCategoryTitleView *)categoryTitleView {
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titleColor = CustomColor(@"#EFEFEF");
        _categoryTitleView.titleSelectedColor = CustomColor(@"#EFEFEF");
        _categoryTitleView.titleFont = kFont_Bold(16);
        _categoryTitleView.indicators = @[self.indicateView];
        _categoryTitleView.delegate = self;
        _categoryTitleView.titleSelectedFont = kFont_Bold(23);
        
    }
    return _categoryTitleView;
}

- (JXCategoryIndicatorLineView *)indicateView{
    if (!_indicateView) {
        _indicateView = [[JXCategoryIndicatorLineView alloc] init];
        _indicateView.lineStyle = JXCategoryIndicatorLineStyle_Normal;
        _indicateView.indicatorWidth = 17;
        _indicateView.indicatorHeight = 4;
        
        _indicateView.indicatorColor = CustomColor(@"#EFEFEF");
    }
    return _indicateView;
}
/**搜索框*/
-(void)theSearchBox{
    self.searchView = [[UIView alloc]init];
    self.searchView.backgroundColor = RGBA(52, 120, 245, 0.1);
    self.searchView.layer.cornerRadius = 30/2;
    [self.navView addSubview:self.searchView];
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_offset(-7);
        make.left.equalTo(self.navView).offset(60);
        make.right.equalTo(self.navView).offset(-15);
        make.height.equalTo(@30);
    }];
    UIImageView *magnifyingImageV = [[UIImageView alloc]init];
    magnifyingImageV.image = [UIImage imageNamed:@"icon--搜索"];
    [self.searchView addSubview:magnifyingImageV];
    [magnifyingImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(12);
        make.centerY.mas_offset(0);
        make.height.width.mas_equalTo(22);
    }];
    UILabel *coachLabel = [[UILabel alloc]init];
    coachLabel.text = @"搜索教练";
    coachLabel.textColor = RGBA(102, 102, 102, 1);
    coachLabel.font = kFont_Medium(15);
    coachLabel.textAlignment = 0;
    [self.searchView addSubview:coachLabel];
    [coachLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.searchView);
        make.left.equalTo(magnifyingImageV.mas_right).offset(6);
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToSearchCoach)];
    [self.searchView addGestureRecognizer:tap];
    
}

/** 筛选 排序View*/
-(void)addFindCoachSortingFilterView{

    [self.view addSubview:self.findCoachSortingFilterView];
    [self.findCoachSortingFilterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@40);
    }];
    kWeakSelf(self);
    
    self.findCoachSortingFilterView.screeningBlock = ^(BOOL isShow) {
        if (isShow) {
            //获取数据
            [weakself getLabelData];
        }else{
            //关闭
            if (weakself.filterView) {
                [weakself.filterView close];
            }
            
        }
    };



}

- (FindCoachSortingFilterView *)findCoachSortingFilterView{
    if (!_findCoachSortingFilterView) {
        _findCoachSortingFilterView = [[FindCoachSortingFilterView alloc]init];
        _findCoachSortingFilterView.backgroundColor = [UIColor whiteColor];
    }
    return _findCoachSortingFilterView;
}

- (NSMutableArray *)coachModelArray {
    if (!_coachModelArray) {
        _coachModelArray = [NSMutableArray array];
    }
    return _coachModelArray;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.coachModelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CoachWithCourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Coach"];
    
    
    if (!cell) {
        cell = [[CoachWithCourseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Coach"];
    }
    cell.model = self.coachModelArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.courseBlock = ^(CourseModel *courseModel) {
      //课程详情
        CourseDetailsViewController *vc = [[CourseDetailsViewController alloc]init];
        vc.model = courseModel;
        [self.navigationController pushViewController:vc animated:YES];
    };
//    cell.followBlock = ^(CoachWithCourseModel *model) {
//      //关注
//    };
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CoachWithCourseModel *model = self.coachModelArray[indexPath.row];
    
    return model.height;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"点击教练");
    CoachWithCourseModel *model = self.coachModelArray[indexPath.row];
    CoachDetailsViewController *vc = [[CoachDetailsViewController alloc]init];
    vc.courseModel = model;
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)goToSearchCoach{
    
    NSLog(@"搜索教练");
    CoachSearchViewController *coachSVC = [[CoachSearchViewController alloc] init];
    [self.navigationController pushViewController:coachSVC animated:YES];
}

- (void)getData{
    
    if (self.page>1 && self.page>self.lastPage) {
        [self.mainTableView.mj_header endRefreshing];
        
        [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        return;
    }
    NSInteger crowdID = 0;
    NSInteger contentID = 0;
    NSString *price = @"";
    NSString *name = @"";
    
    
    if (self.searchDic) {
        if (self.searchDic.count > 0) {
            if (self.searchDic[@"适用人群"]) {
                CourseModel *model = self.searchDic[@"适用人群"];
                crowdID = model.ID;
                NSLog(@"model = %@",model);
            }
            if (self.searchDic[@"教学内容"]) {
                CourseModel *model = self.searchDic[@"教学内容"];
                contentID = model.ID;
            }
            if (self.searchDic[@"价格"]) {
                CourseModel *model = self.searchDic[@"价格"];
                if ([model.name isEqualToString:@"不限"]) {
                    price = nil;
                    return;
                }
//                NSArray *parray = [model.name componentsSeparatedByString:@","];
//                price = [parray componentsJoinedByString:@","];
                if ([model.name containsString:@"以下"]) {
                    price = [model.name stringByReplacingOccurrencesOfString:@"以下" withString:@""];
                    return;
                }
                if ([model.name containsString:@"以上"]) {
                    price = [model.name stringByReplacingOccurrencesOfString:@"以上" withString:@""];
                    return;
                }
                price = [model.name stringByReplacingOccurrencesOfString:@"-" withString:@","];
            }
        }
    }
    NSLog(@"crowdID === %ld",(long)crowdID);
    NSLog(@"price === %@",price);
    NSLog(@"name === %@",name);
    NSLog(@"self.page === %d",self.page);
    NSLog(@"contentID === %ld",(long)contentID);
    
    [RequestManager searchCoachWithId:[[UserInfoManager shared] getUserID] applyCrowd:crowdID price:price name:name pageNum:self.page pageSize:10 teachingContent:contentID withSuccess:^(id  _Nullable response) {
        NSLog(@"筛选 === %@", response);
        if (self.page == 1) {
            [self.coachModelArray removeAllObjects];
        }
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        NSArray *array = response[@"data"][@"list"];
        self.lastPage = [response[@"data"][@"lastPage"] intValue];
                                             
        for (int i = 0; i < array.count; i ++) {
            CoachWithCourseModel *model = [CoachWithCourseModel mj_objectWithKeyValues:array[i]];
            [self.coachModelArray addObject:model];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.nullView.hidden = self.coachModelArray.count;
            [self.mainTableView reloadData];
//            if (self.page == 1) {
//                self.mainTableView.contentOffset = CGPointMake(0, 0);
//            }
        });
        
        self.page ++;
        
        
        
    } withFail:^(NSError * _Nullable error) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        [self stopLoading];
        [self showText:error.localizedDescription];
    }];
    
}
- (void)getLabelData{
    //2 适用人群  1 教学内容
    [self.filterDataArray removeAllObjects];
    [self showSVP];
    [RequestManager getLabelByType:2 withSuccess:^(id  _Nullable response) {
        
        NSLog(@"适用人群 %@",response[@"data"]);
        
        NSArray *array = response[@"data"];
        NSMutableArray *groupArr = [NSMutableArray array];
        for (int i = 0; i < array.count; i ++) {
            CourseCrowdModel *model = [CourseCrowdModel mj_objectWithKeyValues:array[i]];
            [groupArr addObject:model];
        }
        NSDictionary *dic1 = @{@"title":@"适用人群",@"data":groupArr};
        [self.filterDataArray addObject:dic1];
        
        [RequestManager getLabelByType:1 withSuccess:^(id  _Nullable response) {
            [self dissSVP];
            NSLog(@"教学内容 %@",response[@"data"]);
            NSArray *array1 = response[@"data"];
            NSMutableArray *groupArr1 = [NSMutableArray array];
            for (int i = 0; i < array1.count; i ++) {
                CourseCrowdModel *model = [CourseCrowdModel mj_objectWithKeyValues:array1[i]];
                [groupArr1 addObject:model];
            }
            NSDictionary *dic2 = @{@"title":@"教学内容",@"data":groupArr1};
            [self.filterDataArray addObject:dic2];
            NSMutableArray *priceA = [NSMutableArray array];
            NSArray *pArray = @[@"不限",@"100以下",@"100-500",@"500-1000",@"1000以上"];
            for (int i = 0; i < pArray.count; i ++) {
                CourseCrowdModel *cmodel = [[CourseCrowdModel alloc] init];
                cmodel.name = pArray[i];
                [priceA addObject:cmodel];
            }
            
            NSDictionary *dic3 = @{@"title":@"价格",@"data":priceA};
            [self.filterDataArray addObject:dic3];
            
            [self showFilterView];
            
        } withFail:^(NSError * _Nullable error) {
            [self dissSVP];
            [self showText:error.localizedDescription];
        }];
        
    } withFail:^(NSError * _Nullable error) {
        [self dissSVP];
        [self showText:error.localizedDescription];
    }];
}
- (void)stopLoading{
    [self dissSVP];
    
}
- (NSMutableArray *)filterDataArray{
    if (!_filterDataArray) {
        _filterDataArray = [NSMutableArray array];
    }
    return _filterDataArray;
}

- (void)showFilterView{
    if (self.filterView) {
        [self.filterView close];
    }
    CoachFilterView *filterV = [[[NSBundle mainBundle] loadNibNamed:@"CoachFilterView" owner:self options:nil] lastObject];
     filterV.frame = CGRectMake(0, SNavBarHeight+40, KSW, ScreenHeight-SNavBarHeight-40);
    filterV.dataArray = self.filterDataArray;
    
    [self.view addSubview:filterV];
    [filterV refresh];
    kWeakSelf(self);
    filterV.confirmBlock = ^(NSDictionary *conditionInfo) {
        // 确认 刷新数据源
        weakself.searchDic = conditionInfo;
        weakself.page = 1;
        [weakself getData];
        weakself.findCoachSortingFilterView.screeningBtn.selected = NO;
    };
    self.filterView = filterV;
    
}
@end
