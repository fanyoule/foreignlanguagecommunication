//
//  CoachSearchViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/28.
//

#import "CoachSearchViewController.h"
#import "CoachWithCourseTableViewCell.h"
#import "YKSearchBar.h"
#import "CoachDetailsViewController.h"// 教练详情
#import "CourseDetailsViewController.h"// 课程详情
@interface CoachSearchViewController ()<YKSearchBarDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) YKSearchBar *searchBar;
@property (nonatomic) int page;
@property (nonatomic) int lastPage;
@property (nonatomic, copy) NSString *searchContent;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation CoachSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavView];
    self.searchContent = @"";
    self.page = 1;
    self.lastPage = 0;
    [self setupSubview];
}

- (void)setupNavView{
    self.navView.hidden = NO;
    self.backBtn.hidden = YES;
    self.navBottomLine.hidden = NO;
    [self.navView addSubview:self.searchBar];
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.right.mas_offset(-15);
            make.bottom.mas_offset(-7);
            make.height.mas_equalTo(30);
    }];
}
- (void)setupSubview {
    [self.view addSubview:self.mainTableView];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_offset(0);
            make.top.mas_offset(SNavBarHeight);
    }];
    kWeakSelf(self);
    self.mainTableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [weakself getData];
    }];
    self.mainTableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
        [weakself getData];
    }];
    
}




- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (YKSearchBar *)searchBar{
    if (!_searchBar) {
        _searchBar = [[YKSearchBar alloc]init];
        
        _searchBar.alwaysShowFinish = YES;
        [_searchBar.finishButton setTitle:@"取消" forState:UIControlStateNormal];
        [_searchBar.finishButton setTitleColor:CustomColor(@"#181818") forState:UIControlStateNormal];
        _searchBar.finishButton.titleLabel.font = kFont_Bold(15);
        _searchBar.delegate = self;
        _searchBar.paddingToButton = 10;
        _searchBar.searchTextView.layer.cornerRadius = 15;
        _searchBar.searchTextView.placeHolderPosition = SearchViewPlaceHolderPositionDefault;
        _searchBar.searchTextView.placeHolder = @"搜索教练";
        _searchBar.searchTextView.placeHolderFont = kFont_Medium(15);
        _searchBar.searchTextView.font = kFont_Medium(15);
        _searchBar.searchTextView.textColor = CustomColor(@"#181818");
    }
    return _searchBar;
}
- (void)finishSearch:(UITextField *)textField{
    NSLog(@"点击结束");
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)searchBarDidEndEdit:(UITextField *)textField{
    NSLog(@"结束输入");
    
    self.searchContent = textField.text;
    
    [self getData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CoachWithCourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"coach"];
    if (!cell) {
        cell = [[CoachWithCourseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"coach"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.dataArray[indexPath.row];
    cell.courseBlock = ^(CourseModel *courseModel) {
      //课程详情
        CourseDetailsViewController *vc = [[CourseDetailsViewController alloc]init];
        vc.model = courseModel;
        [self.navigationController pushViewController:vc animated:YES];
    };
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CoachWithCourseModel *model = self.dataArray[indexPath.row];
    return model.height;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CoachWithCourseModel *model = self.dataArray[indexPath.row];
    CoachDetailsViewController *vc = [[CoachDetailsViewController alloc]init];
    vc.courseModel = model;
    [self.navigationController pushViewController:vc animated:YES];
    //跳转 教练详情
}

- (void)getData{
    if (self.lastPage > 0 && self.page > self.lastPage) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        return;
    }
    
    [RequestManager searchCoachWithId:[[UserInfoManager shared] getUserID] applyCrowd:0 price:@"" name:self.searchContent pageNum:self.page pageSize:10 teachingContent:0 withSuccess:^(id  _Nullable response) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        
         self.lastPage = [response[@"data"][@"lastPage"] intValue];
        if (self.page == 1) {
            [self.dataArray removeAllObjects];
        }
        
        NSArray *array = response[@"data"][@"list"];
        for (int i = 0; i < array.count; i ++) {
            CoachWithCourseModel *model = [CoachWithCourseModel mj_objectWithKeyValues:array[i]];
            [self.dataArray addObject:model];
        }
        
        [self.mainTableView reloadData];
        
    } withFail:^(NSError * _Nullable error) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
