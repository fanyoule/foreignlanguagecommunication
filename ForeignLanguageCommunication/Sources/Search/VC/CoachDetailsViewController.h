//
//  CoachDetailsViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "BaseViewController.h"
#import "CoachWithCourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CoachDetailsViewController : BaseViewController

@property (nonatomic, strong)CoachWithCourseModel *courseModel;

@end

NS_ASSUME_NONNULL_END
