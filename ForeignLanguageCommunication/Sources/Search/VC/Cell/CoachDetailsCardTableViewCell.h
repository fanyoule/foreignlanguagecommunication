//
//  CoachDetailsCardTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>
#import "CoachDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CoachDetailsCardTableViewCell : UITableViewCell
@property (nonatomic, strong)CoachDetailsModel *detailsModel;
@end

NS_ASSUME_NONNULL_END
