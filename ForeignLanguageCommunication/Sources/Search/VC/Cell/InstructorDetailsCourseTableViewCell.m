//
//  InstructorDetailsCourseTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "InstructorDetailsCourseTableViewCell.h"

#import "TYTagView.h"
#import "CoachTeachingModel.h"
@interface InstructorDetailsCourseTableViewCell ()
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 已售*/
@property (nonatomic, strong)UILabel *soldLabel;
/** 购买*/
@property (nonatomic, strong)UIButton *buyBtn;
/** 钱*/
@property (nonatomic, strong)UILabel *moneyLabel;
/** 价格*/
@property (nonatomic, strong)UILabel *priceLabel;
/** 规则*/
@property (nonatomic, strong)UILabel *rulesLabel;

/** 标签*/
@property (nonatomic, strong)TYTagView *tYTagView;
@end
@implementation InstructorDetailsCourseTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

- (void)setCourseModel:(InstructorDetailsCourseModel *)courseModel{
    if (courseModel.coursePic.count > 0) {
        [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:courseModel.coursePic[0][@"url"]]];
    }
    
    self.titleLabel.text = courseModel.name;
    self.soldLabel.text = [NSString stringWithFormat:@"已售%ld",courseModel.orders];
    self.priceLabel.text = [NSString stringWithFormat:@"%ld",courseModel.price];
    self.rulesLabel.text = [NSString stringWithFormat:@"/%ld分钟/一节课",courseModel.time];
    
    NSMutableArray *dataArray = [NSMutableArray array];
    for (int i = 0; i < courseModel.applyCrowd.count; i++) {
        CoachTeachingModel *model = [CoachTeachingModel mj_objectWithKeyValues:courseModel.applyCrowd[i]];
        [dataArray addObject:model.name];
    }
    self.tYTagView.items = dataArray;
    
    
}

// MARK: 点击购买
-(void)clickBuy{
    if (self.purchase) {
        self.purchase();
    }
}

-(void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.tYTagView];
    
    [self.contentView addSubview:self.soldLabel];
    [self.contentView addSubview:self.buyBtn];
    [self.contentView addSubview:self.moneyLabel];
    [self.contentView addSubview:self.priceLabel];
    [self.contentView addSubview:self.rulesLabel];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@80);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
    [self.tYTagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.left.equalTo(self.titleLabel);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tYTagView.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView).offset(-15).priority(800);
        make.left.equalTo(self.portraitImageV.mas_right).offset(20);
        make.height.equalTo(@18);
    }];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.priceLabel);
        make.right.equalTo(self.priceLabel.mas_left);
    }];
    [self.rulesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.priceLabel);
        make.left.equalTo(self.priceLabel.mas_right);
    }];
    [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.priceLabel);
        make.width.equalTo(@60);
        make.height.equalTo(@20);
    }];
    [self.soldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.buyBtn);
        make.bottom.equalTo(self.buyBtn.mas_top).offset(-5);
    }];
}
- (TYTagView *)tYTagView{
    if (!_tYTagView) {
        _tYTagView = [[TYTagView alloc]init];
        _tYTagView.backgroundColor = UIColor.clearColor;
        _tYTagView.top = 5;
        _tYTagView.margin = 5;
        _tYTagView.tagHeight = 15;
    }
    return _tYTagView;
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像"];
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"青少年课程口语1小时练习青少年课程口语1小时练习青少年课程口语1小时少";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(14);
        _titleLabel.textAlignment = 0;
        _titleLabel.numberOfLines = 2;
    }
    return _titleLabel;
}
- (UILabel *)soldLabel{
    if (!_soldLabel) {
        _soldLabel = [[UILabel alloc]init];
        _soldLabel.text = @"已售156";
        _soldLabel.textColor = RGBA(153, 153, 153, 1);
        _soldLabel.font = kFont_Medium(11);
        _soldLabel.textAlignment = 2;
    }
    return _soldLabel;
}
- (UIButton *)buyBtn{
    if (!_buyBtn) {
        _buyBtn = [[UIButton alloc]init];
        [_buyBtn setTitle:@"购买" forState:(UIControlStateNormal)];
        [_buyBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _buyBtn.titleLabel.font = kFont_Medium(14);
        _buyBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _buyBtn.layer.cornerRadius = 6;
        _buyBtn.clipsToBounds = YES;
        [_buyBtn addTarget:self action:@selector(clickBuy) forControlEvents:(UIControlEventTouchDown)];
    }
    return _buyBtn;
}
- (UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc]init];
        _moneyLabel.text = @"¥";
        _moneyLabel.textColor = RGBA(52, 120, 245, 1);
        _moneyLabel.font = kFont_Medium(11);
    }
    return _moneyLabel;
}
- (UILabel *)priceLabel{
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc]init];
        _priceLabel.text = @"3900";
        _priceLabel.textColor = RGBA(52, 120, 245, 1);
        _priceLabel.font = kFont_Bold(24);
        _priceLabel.textAlignment = 0;
    }
    return _priceLabel;
}
- (UILabel *)rulesLabel{
    if (!_rulesLabel) {
        _rulesLabel = [[UILabel alloc]init];
        _rulesLabel.text = @"/90分钟/一节课";
        _rulesLabel.textColor = RGBA(52, 120, 245, 1);
        _rulesLabel.font = kFont_Medium(11);
        _rulesLabel.textAlignment = 0;
    }
    return _rulesLabel;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
