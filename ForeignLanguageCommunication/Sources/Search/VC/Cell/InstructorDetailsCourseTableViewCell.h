//
//  InstructorDetailsCourseTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>
#import "InstructorDetailsCourseModel.h"
typedef void(^clickPurchaseCourseBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface InstructorDetailsCourseTableViewCell : UITableViewCell

@property (nonatomic, strong)InstructorDetailsCourseModel *courseModel;

@property(nonatomic, copy)clickPurchaseCourseBlock purchase;
@end

NS_ASSUME_NONNULL_END
