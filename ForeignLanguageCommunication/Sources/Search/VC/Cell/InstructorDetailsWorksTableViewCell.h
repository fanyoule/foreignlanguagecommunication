//
//  InstructorDetailsWorksTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import <UIKit/UIKit.h>
#import "CoachDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol UnionInstructorDetailsWorksTableViewCellDelegate <NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)clickInstructorDetailsWorksTableViewCell;

@end

@interface InstructorDetailsWorksTableViewCell : UITableViewCell

@property (nonatomic, strong)CoachDetailsModel *detailsModel;

@property(nonatomic, weak) id<UnionInstructorDetailsWorksTableViewCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
