//
//  InstructorDetailsWorksTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "InstructorDetailsWorksTableViewCell.h"
#import "InstructorDetailsWorksModel.h"
@interface InstructorDetailsWorksTableViewCell ()

@property (nonatomic, strong)UIView *titleView;
@property (nonatomic, strong)UILabel *titleLabel;



@property (nonatomic, strong)NSMutableArray *dataArray;
@end
@implementation InstructorDetailsWorksTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}

- (void)setDetailsModel:(CoachDetailsModel *)detailsModel{
    _detailsModel = detailsModel;
    NSArray *works = detailsModel.works;
    
    for (int i = 0; i < works.count; i++) {
        InstructorDetailsWorksModel *model = [InstructorDetailsWorksModel mj_objectWithKeyValues:works[i]];
        [self.dataArray addObject:model];
    }
    CGFloat w = KSW/4-10;
    
    for (int i = 0; i < self.dataArray.count; i++) {
        InstructorDetailsWorksModel *model = [[InstructorDetailsWorksModel alloc]init];
        model = self.dataArray[i];
        
        NSArray *imgaeArray = [model.picUrl componentsSeparatedByString:@","];
        
        UIImageView *imageV = [[UIImageView alloc]init];
        if (model.type == 1) {
            if (imgaeArray.count != 0) {
                [imageV sd_setImageWithURL:[NSURL URLWithString:imgaeArray[0]]];
            }
        }else{
            [imageV sd_setImageWithURL:[NSURL URLWithString:model.videoCoverUrl]];
        }
        imageV.userInteractionEnabled = YES;
        
        if (i <= 3) {
            [self.contentView addSubview:imageV];
            [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
                make.left.equalTo(self.contentView).offset(w*i + i*2 +15);
                make.width.height.equalTo(@(w));
                make.bottom.equalTo(self.contentView).offset(-10);
            }];
        }else{
            
        }
        if (self.dataArray.count > 4) {
            UIButton *arrowBtn = [[UIButton alloc]init];
            [arrowBtn setImage:[UIImage imageNamed:@"＞＞"] forState:(UIControlStateNormal)];
            [arrowBtn addTarget:self action:@selector(moreAndMore) forControlEvents:(UIControlEventTouchDown)];
            arrowBtn.backgroundColor = RGBA(0, 0, 0, 0.3);
            if (i == 3) {
                [imageV addSubview:arrowBtn];
                [arrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(imageV);
                }];
                
            }
            
        }
    }
    
}

-(void)addControls{
    [self.contentView addSubview:self.titleView];
    [self.contentView addSubview:self.titleLabel];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.left.equalTo(self.contentView).offset(15);
        make.height.equalTo(@14);
        make.width.equalTo(@5);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleView);
        make.left.equalTo(self.titleView.mas_right).offset(5);
    }];
    

    

}

-(void)moreAndMore{
    
    
    if ([self.delegate respondsToSelector:@selector(clickInstructorDetailsWorksTableViewCell)]) {
        [self.delegate clickInstructorDetailsWorksTableViewCell];
    }
    
}


- (UIView *)titleView{
    if (!_titleView) {
        _titleView = [[UIView alloc]init];
        _titleView.backgroundColor = RGBA(52, 120, 245, 1);
        _titleView.layer.cornerRadius = 2.5;
        _titleView.clipsToBounds = YES;
    }
    return _titleView;
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"作品";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Bold(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
