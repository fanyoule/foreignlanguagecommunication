//
//  CoachDetailsCardTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//

#import "CoachDetailsCardTableViewCell.h"
#import "TYTagView.h"
@interface CoachDetailsCardTableViewCell ()
@property (nonatomic, strong)UIView *backView;

/** 头像 */
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/** 关注*/
@property (nonatomic, strong)UIButton *focusBtn;
/** 适应人群*/
@property (nonatomic, strong)UILabel *adaptLabel;
/** 适应人群 内容*/
@property (nonatomic, strong)TYTagView *tYTagView;

/** 教学内容*/
@property (nonatomic, strong)UILabel *teachingLabel;
/** 教学内容  内容*/
@property (nonatomic, strong)TYTagView *teachingContentLabel;

/** 好评*/
@property (nonatomic, strong)UILabel *commentLabel;
/** 好评率*/
@property (nonatomic, strong)UILabel *favorableRatingLabel;
/** 购买量*/
@property (nonatomic, strong)UILabel *buyQuantityLabel;

/** 购买*/
@property (nonatomic, strong)UILabel *buyNumberLabel;
/** 作品*/
@property (nonatomic, strong)UILabel *worksNumberLabel;
/** 粉丝*/
@property (nonatomic, strong)UILabel *fansNumberLabel;

/** 线*/
@property (nonatomic, strong)UIView *lineView;
/** 简介*/
@property (nonatomic, strong)UILabel *introductionLabel;
/** 简介内容*/
@property (nonatomic, strong)UILabel *introductionContentLabel;

@property (nonatomic, strong)NSArray *adaptArray;

@end
@implementation CoachDetailsCardTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.focusBtn.hidden = YES;
        [self addControls];
    }
    return self;
}

- (void)setDetailsModel:(CoachDetailsModel *)detailsModel{
    _detailsModel = detailsModel;
    
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:detailsModel.avatar]];
    self.nameLabel.text = detailsModel.realName;
    self.favorableRatingLabel.text = [NSString stringWithFormat:@"%@%%",detailsModel.goodRate];
    self.buyQuantityLabel.text = [NSString stringWithFormat:@"%ld人购买",detailsModel.orderNum];
    self.buyNumberLabel.text = [NSString stringWithFormat:@"%ld",detailsModel.orderNum];
    self.worksNumberLabel.text = [NSString stringWithFormat:@"%ld",detailsModel.workNum];
    self.fansNumberLabel.text = [NSString stringWithFormat:@"%ld",detailsModel.fansNum];
    
    self.introductionContentLabel.text = detailsModel.content;
    
    
    self.adaptArray = [detailsModel.applyCrowd componentsSeparatedByString:@","];
    self.tYTagView.items = self.adaptArray;
    
    
    self.teachingContentLabel.items = [detailsModel.teachingContent componentsSeparatedByString:@","];
    NSLog(@"适应人群 === %@",self.adaptArray);
    NSLog(@"教学内容 === %@",detailsModel.teachingContent);
}

-(void)addControls{
    
    
    [self.contentView addSubview:self.backView];
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.focusBtn];
    [self.contentView addSubview:self.adaptLabel];
    [self.contentView addSubview:self.teachingLabel];
    [self.contentView addSubview:self.teachingContentLabel];
    
    [self.contentView addSubview:self.commentLabel];
    [self.contentView addSubview:self.favorableRatingLabel];
    [self.contentView addSubview:self.buyQuantityLabel];
    
    [self.contentView addSubview:self.buyNumberLabel];
    [self.contentView addSubview:self.worksNumberLabel];
    [self.contentView addSubview:self.fansNumberLabel];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.introductionLabel];
    [self.contentView addSubview:self.introductionContentLabel];
    
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.contentView).offset(15);
        make.bottom.right.equalTo(self.contentView).offset(-15);
    }];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).offset(20);
        make.left.equalTo(self.backView).offset(15);
        make.width.height.equalTo(@55);
        
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV).offset(10);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    [self.focusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).offset(20);
        make.right.equalTo(self.backView).offset(-15);
        make.width.equalTo(@60);
        make.height.equalTo(@28);
    }];
    
    [self.adaptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(25);
        make.width.equalTo(@50);
        
    }];
    
//    self.adaptArray = @[@"日常口语",@"日常",@"小学生",@"日常",@"小学生",@"日常",@"小学生"];
    
    [self.contentView addSubview:self.tYTagView];
    [self.tYTagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.adaptLabel).offset(-5);
        make.left.equalTo(self.adaptLabel.mas_right);
        make.right.equalTo(self.backView);
    }];
    
    
    
    [self.teachingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tYTagView.mas_bottom).offset(15);
        make.left.equalTo(self.adaptLabel);
        make.width.equalTo(@50);
        make.height.equalTo(@15);
    }];
    
    
    [self.teachingContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.teachingLabel).offset(-5);
        make.left.equalTo(self.teachingLabel.mas_right);
        make.right.equalTo(self.backView);
    }];
    [self.commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.teachingContentLabel.mas_bottom).offset(20);
        make.left.equalTo(self.teachingLabel);
        make.width.equalTo(@40);
        make.height.equalTo(@15);
    }];
    [self.favorableRatingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.commentLabel);
        make.left.equalTo(self.commentLabel.mas_right);
    }];
    [self.buyQuantityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.commentLabel);
        make.right.equalTo(self.backView).offset(-15);
    }];
    
    [self.backView layoutIfNeeded];
    CGFloat w = self.backView.bounds.size.width/3;
    UILabel *buyLabel = [[UILabel alloc]init];
    buyLabel.text = @"购买";
    buyLabel.textColor = RGBA(153, 153, 153, 1);
    buyLabel.font = kFont_Medium(12);
    buyLabel.textAlignment = 1;
    [self.backView addSubview:buyLabel];
    
    
    UILabel *worksLabel = [[UILabel alloc]init];
    worksLabel.text = @"作品";
    worksLabel.textColor = RGBA(153, 153, 153, 1);
    worksLabel.font = kFont_Medium(12);
    worksLabel.textAlignment = 1;
    [self.backView addSubview:worksLabel];
    
    
    UILabel *fansLabel = [[UILabel alloc]init];
    fansLabel.text = @"粉丝";
    fansLabel.textColor = RGBA(153, 153, 153, 1);
    fansLabel.font = kFont_Medium(12);
    fansLabel.textAlignment = 1;
    [self.backView addSubview:fansLabel];
    
    
    
    [self.buyNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView);
        make.top.equalTo(self.commentLabel.mas_bottom).offset(20);
        make.width.equalTo(@(w));
        make.height.equalTo(@20);
    }];
    [buyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyNumberLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.buyNumberLabel);
        make.width.equalTo(@(w));
        make.height.equalTo(@15);
    }];
    [self.worksNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView);
        make.top.equalTo(self.buyNumberLabel);
        make.width.equalTo(@(w));
        make.height.equalTo(@20);
    }];
    [worksLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.worksNumberLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.worksNumberLabel);
        make.width.equalTo(@(w));
        make.height.equalTo(@15);
    }];
    [self.fansNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView);
        make.top.equalTo(self.buyNumberLabel);
        make.width.equalTo(@(w));
        make.height.equalTo(@20);
    }];
    [fansLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.fansNumberLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.fansNumberLabel);
        make.width.equalTo(@(w));
        make.height.equalTo(@15);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(buyLabel.mas_bottom).offset(20);
        make.height.equalTo(@0.5);
        make.left.equalTo(self.backView).offset(15);
        make.right.equalTo(self.backView).offset(-15);
    }];
    
    [self.introductionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView).offset(10);
        make.left.equalTo(self.backView).offset(15);
        make.width.equalTo(@80);
    }];
    //80+15+15+15+15
    [self.introductionContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.introductionLabel.mas_right);
        make.top.equalTo(self.introductionLabel);
        make.right.equalTo(self.backView).offset(-15);
        make.bottom.equalTo(self.backView).offset(-20);
//        make.height.mas_equalTo(@20);
    }];
//    [self.backView layoutIfNeeded];
//    [self.backView layoutSubviews];
    NSLog(@"高 === %lf",self.backView.size.height);
    
}

- (TYTagView *)tYTagView{
    if (!_tYTagView) {
        _tYTagView = [[TYTagView alloc]init];
        _tYTagView.backgroundColor = UIColor.clearColor;
        _tYTagView.top = 5;
        _tYTagView.margin = 5;
        _tYTagView.tagHeight = 15;
    }
    return _tYTagView;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = RGBA(255, 255, 255, 1);
        _backView.layer.cornerRadius = 6;
        
        _backView.layer.shadowColor = [UIColor blackColor].CGColor;
        _backView.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
        _backView.layer.shadowOpacity = 0.5f;
    }
    return _backView;
}
- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像2"];
        _portraitImageV.contentMode = UIViewContentModeScaleAspectFill;
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(51, 51, 51, 1);
        _nameLabel.font = kFont_Bold(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}
- (UIButton *)focusBtn{
    if (!_focusBtn) {
        _focusBtn = [[UIButton alloc]init];
        [_focusBtn setTitle:@"关注" forState:(UIControlStateNormal)];
        [_focusBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _focusBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _focusBtn.layer.cornerRadius = 6;
        _focusBtn.layer.masksToBounds = YES;
        [_focusBtn addTarget:self action:@selector(clickFocus) forControlEvents:(UIControlEventTouchDown)];
    }
    return _focusBtn;
}
- (UILabel *)adaptLabel{
    if (!_adaptLabel) {
        _adaptLabel = [[UILabel alloc]init];
        _adaptLabel.text = @"适应人群";
        _adaptLabel.textColor = RGBA(153, 153, 153, 1);
        _adaptLabel.font = kFont_Medium(12);
        _adaptLabel.textAlignment = 0;
    }
    return _adaptLabel;
}
- (UILabel *)teachingLabel{
    if (!_teachingLabel) {
        _teachingLabel = [[UILabel alloc]init];
        _teachingLabel.text = @"教学内容";
        _teachingLabel.textColor = RGBA(153, 153, 153, 1);
        _teachingLabel.font = kFont_Medium(12);
        _teachingLabel.textAlignment = 0;
    }
    return _teachingLabel;
}
- (TYTagView *)teachingContentLabel{
    if (!_teachingContentLabel) {
        _teachingContentLabel = [[TYTagView alloc]init];
        _teachingContentLabel.backgroundColor = UIColor.clearColor;
        _teachingContentLabel.top = 5;
        _teachingContentLabel.margin = 5;
        _teachingContentLabel.tagHeight = 15;
    }
    return _teachingContentLabel;
}


- (UILabel *)commentLabel{
    if (!_commentLabel) {
        _commentLabel = [[UILabel alloc]init];
        _commentLabel.text = @"好评：";
        _commentLabel.textColor = RGBA(153, 153, 153, 1);
        _commentLabel.font = kFont_Medium(12);
        _commentLabel.textAlignment = 0;
    }
    return _commentLabel;
}
- (UILabel *)favorableRatingLabel{
    if (!_favorableRatingLabel) {
        _favorableRatingLabel = [[UILabel alloc]init];
        _favorableRatingLabel.text = @"0.60%";
        _favorableRatingLabel.textColor = RGBA(213, 160, 104, 1);
        _favorableRatingLabel.font = kFont_Medium(12);
        _favorableRatingLabel.textAlignment = 0;
    }
    return _favorableRatingLabel;
}
- (UILabel *)buyQuantityLabel{
    if (!_buyQuantityLabel) {
        _buyQuantityLabel = [[UILabel alloc]init];
        _buyQuantityLabel.text = @"289846人购买";
        _buyQuantityLabel.textColor = RGBA(153, 153, 153, 1);
        _buyQuantityLabel.font = kFont_Medium(12);
        _buyQuantityLabel.textAlignment = 2;
    }
    return _buyQuantityLabel;
}
- (UILabel *)buyNumberLabel{
    if (!_buyNumberLabel) {
        _buyNumberLabel = [[UILabel alloc]init];
        _buyNumberLabel.text = @"56565";
        _buyNumberLabel.textColor = RGBA(51, 51, 51, 1);
        _buyNumberLabel.font = kFont_Bold(18);
        _buyNumberLabel.textAlignment = 1;
    }
    return _buyNumberLabel;
}
- (UILabel *)worksNumberLabel{
    if (!_worksNumberLabel) {
        _worksNumberLabel = [[UILabel alloc]init];
        _worksNumberLabel.text = @"16516";
        _worksNumberLabel.textColor = RGBA(51, 51, 51, 1);
        _worksNumberLabel.font = kFont_Bold(18);
        _worksNumberLabel.textAlignment = 1;
    }
    return _worksNumberLabel;
}
- (UILabel *)fansNumberLabel{
    if (!_fansNumberLabel) {
        _fansNumberLabel = [[UILabel alloc]init];
        _fansNumberLabel.text = @"845851";
        _fansNumberLabel.textColor = RGBA(51, 51, 51, 1);
        _fansNumberLabel.font = kFont_Bold(18);
        _fansNumberLabel.textAlignment = 1;
    }
    return _fansNumberLabel;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}
- (UILabel *)introductionLabel{
    if (!_introductionLabel) {
        _introductionLabel = [[UILabel alloc]init];
        _introductionLabel.text = @"教练介绍:";
        _introductionLabel.textColor = RGBA(24, 24, 24, 1);
        _introductionLabel.font = kFont_Bold(15);
        _introductionLabel.textAlignment = 0;
       
    }
    return _introductionLabel;
}
- (UILabel *)introductionContentLabel{
    if (!_introductionContentLabel) {
        _introductionContentLabel = [[UILabel alloc]init];
        _introductionContentLabel.text = @"毕业于毕业大学大学，曾多次赴韩国和日本学习，是中韩医学交流学者。他艺术...";
        _introductionContentLabel.textColor = RGBA(24, 24, 24, 1);
        _introductionContentLabel.font = kFont_Medium(14);
        _introductionContentLabel.textAlignment = 0;
        _introductionContentLabel.numberOfLines = 0;
    }
    return _introductionContentLabel;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
