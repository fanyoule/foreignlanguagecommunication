//
//  CoachDetailsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/13.
//  教练详情

#import "CoachDetailsViewController.h"
#import "CoachDetailsCardTableViewCell.h"
#import "InstructorDetailsWorksTableViewCell.h"
#import "InstructorDetailsCourseTableViewCell.h"
#import "CommentsTableViewCell.h"
#import "CoachDetailsModel.h"
#import "InstructorDetailsCourseModel.h"

#import "CourseDetailsCommentsModel.h"

#import "ShareThePopupWindow.h"// 分享弹窗
#import "MyWorkViewController.h"// 他人作品
#import "CourseDetailsViewController.h"// 课程详情

@interface CoachDetailsViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
UnionInstructorDetailsWorksTableViewCellDelegate
>

@property (nonatomic, strong)UIButton *selectedBtn;

@property (nonatomic, strong)CoachDetailsModel *detailsModel;

@property (nonatomic, strong)NSMutableArray *courseArray;

/** 全部评论数量*/
@property (nonatomic, assign)NSInteger totalNumber;
@property (nonatomic, strong)NSMutableArray *totalArray;
/** 好评数量*/
@property (nonatomic, assign)NSInteger favorableNumber;
@property (nonatomic, strong)NSMutableArray *favorableArray;
/** 中评数量*/
@property (nonatomic, assign)NSInteger evaluationNumber;
@property (nonatomic, strong)NSMutableArray *evaluationArray;
/** 差评数量*/
@property (nonatomic, assign)NSInteger badNumber;
@property (nonatomic, strong)NSMutableArray *badArray;

@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation CoachDetailsViewController
- (NSMutableArray *)totalArray{
    if (!_totalArray) {
        _totalArray = [[NSMutableArray alloc]init];
    }
    return _totalArray;
}
- (NSMutableArray *)favorableArray{
    if (!_favorableArray) {
        _favorableArray = [[NSMutableArray alloc]init];
    }
    return _favorableArray;
}
- (NSMutableArray *)evaluationArray{
    if (!_evaluationArray) {
        _evaluationArray = [[NSMutableArray alloc]init];
    }
    return _evaluationArray;
}
- (NSMutableArray *)badArray{
    if (!_badArray) {
        _badArray = [[NSMutableArray alloc]init];
    }
    return _badArray;
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (NSMutableArray *)courseArray{
    if (!_courseArray) {
        _courseArray = [[NSMutableArray alloc]init];
    }
    return _courseArray;
}
- (CoachDetailsModel *)detailsModel{
    if (!_detailsModel) {
        _detailsModel = [[CoachDetailsModel alloc]init];
    }
    return _detailsModel;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"教练详情";
    [self creatUI];

    // Do any additional setup after loading the view.
}
-(void)creatUI{
    
    [self addRightBtnWith:@"···"];
    self.rightBtn.titleLabel.font = kFont_Bold(20);
    [self.rightBtn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateNormal)];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.navView.mas_bottom);
            make.left.right.equalTo(self.view);
            make.bottom.equalTo(self.view).offset( -TabbarSafeMargin);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.navView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.left.equalTo(self.navView);
        make.height.equalTo(@0.5);
    }];
    
}
- (void)setCourseModel:(CoachWithCourseModel *)courseModel{
    _courseModel = courseModel;
    [self getData];
}
-(void)getData{
    
    [RequestManager coachDetailWithId:[[UserInfoManager shared] getUserID] coachId:self.courseModel.userId withSuccess:^(id  _Nullable response) {
        NSLog(@"教练详情 === %@",response);
        NSDictionary *dic = response[@"data"];
        NSArray *course = dic[@"course"];
        NSArray *evaluation = dic[@"evaluation"];
        if ([response[@"status"] integerValue] == 200) {
            self.detailsModel = [CoachDetailsModel mj_objectWithKeyValues:dic];
            if (IS_VALID_STRING(self.detailsModel.content)) {
                CGFloat contectHeight = [self.detailsModel.content heightForFont:kFont_Medium(14) width:YL_kScreenWidth-140];
                self.detailsModel.cellheight = 290+contectHeight;
            }else{
                self.detailsModel.cellheight = 290;
            }
            
            for (int i = 0; i< course.count; i++) {
                InstructorDetailsCourseModel *courseModel = [InstructorDetailsCourseModel mj_objectWithKeyValues:course[i]];
                [self.courseArray addObject:courseModel];
            }
            
            for (int i = 0; i< evaluation.count; i++) {
                CourseDetailsCommentsModel *commentsModel = [CourseDetailsCommentsModel mj_objectWithKeyValues:evaluation[i]];
                if ([commentsModel.type isEqualToString:@"全部"]) {
                    NSArray *evaluation = commentsModel.evaluation;
                    for (int i = 0; i < evaluation.count; i++) {
                        CourseDetailsCommentDetailsModel *model = [CourseDetailsCommentDetailsModel mj_objectWithKeyValues:evaluation[i]];
                        [self.totalArray addObject:model];
                    }
                    self.totalNumber = commentsModel.num;
                }else if ([commentsModel.type isEqualToString:@"好评"]){
                    NSArray *evaluation = commentsModel.evaluation;
                    for (int i = 0; i < evaluation.count; i++) {
                        CourseDetailsCommentDetailsModel *model = [CourseDetailsCommentDetailsModel mj_objectWithKeyValues:evaluation[i]];
                        [self.favorableArray addObject:model];
                    }
                    self.favorableNumber = commentsModel.num;
                }else if ([commentsModel.type isEqualToString:@"中评"]){
                    NSArray *evaluation = commentsModel.evaluation;
                    for (int i = 0; i < evaluation.count; i++) {
                        CourseDetailsCommentDetailsModel *model = [CourseDetailsCommentDetailsModel mj_objectWithKeyValues:evaluation[i]];
                        [self.evaluationArray addObject:model];
                    }
                    self.evaluationNumber = commentsModel.num;
                }else if ([commentsModel.type isEqualToString:@"差评"]){
                    NSArray *evaluation = commentsModel.evaluation;
                    for (int i = 0; i < evaluation.count; i++) {
                        CourseDetailsCommentDetailsModel *model = [CourseDetailsCommentDetailsModel mj_objectWithKeyValues:evaluation[i]];
                        [self.badArray addObject:model];
                    }
                    self.badNumber = commentsModel.num;
                }
            }
            
            self.dataArray = self.totalArray;
    
        }
        [self.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
        return 2;
    }else if (section == 1){
        return self.courseArray.count;
    }else{
        return self.dataArray.count;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    
    if (section == 0) {
        
    }else if (section == 1){
        view.backgroundColor = UIColor.whiteColor;
        
        
        UIView *titleView = [[UIView alloc]init];
        titleView.backgroundColor = RGBA(52, 120, 245, 1);
        titleView.layer.cornerRadius = 2.5;
        titleView.clipsToBounds = YES;
        [view addSubview:titleView];
        [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view).offset(10);
            make.left.equalTo(view).offset(15);
            make.width.equalTo(@5);
            make.height.equalTo(@14);
        }];
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = @"课程";
        titleLabel.textColor = RGBA(24, 24, 24, 1);
        titleLabel.font = kFont_Bold(15);
        titleLabel.textAlignment = 0;
        [view addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(titleView);
            make.left.equalTo(titleView.mas_right).offset(5);
        }];
        
        
    }else if (section == 2){
        view.backgroundColor = RGBA(245, 245, 245, 1);
        UIView *backView = [[UIView alloc]init];
        backView.backgroundColor = UIColor.whiteColor;
        [view addSubview:backView];
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view).offset(10);
            make.left.right.bottom.equalTo(view);
        }];
        
        UIView *titleView = [[UIView alloc]init];
        titleView.backgroundColor = RGBA(52, 120, 245, 1);
        titleView.layer.cornerRadius = 2.5;
        titleView.clipsToBounds = YES;
        [backView addSubview:titleView];
        [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view).offset(20);
            make.left.equalTo(view).offset(15);
            make.width.equalTo(@5);
            make.height.equalTo(@14);
        }];
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = @"评价";
        titleLabel.textColor = RGBA(24, 24, 24, 1);
        titleLabel.font = kFont_Bold(15);
        titleLabel.textAlignment = 0;
        [backView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(titleView);
            make.left.equalTo(titleView.mas_right).offset(5);
        }];
        //全部
        NSString *all = [NSString stringWithFormat:@"全部%ld",self.totalNumber];
        //好评
        NSString *favorable = [NSString stringWithFormat:@"好评%ld",self.favorableNumber];
        //中评
        NSString *evaluation = [NSString stringWithFormat:@"中评%ld",self.evaluationNumber];
        //差评
        NSString *review = [NSString stringWithFormat:@"差评%ld",self.badNumber];

        NSMutableArray *array = [NSMutableArray new];
        [array addObject:all];
        [array addObject:favorable];
        [array addObject:evaluation];
        [array addObject:review];


        for (int i = 0; i< array.count; i++) {
            UIButton * btn = [UIButton new];
            btn.tag = 800+i;
            [btn setTitle:array[i] forState:(UIControlStateNormal)];
            [btn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateNormal)];
            [btn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateSelected)];
            btn.backgroundColor = RGBA(238, 238, 238, 1);
            btn.titleLabel.font = kFont_Medium(12);
//            btn.layer.borderWidth = 1;
//            btn.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;

            btn.layer.cornerRadius = 6;
            btn.clipsToBounds = YES;
            [backView addSubview:btn];
            [btn sizeToFit];
            CGFloat w = btn.bounds.size.width + 15;
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(titleLabel.mas_bottom).offset(15);
                make.width.equalTo(@(w));
                make.left.equalTo(backView).offset((w*i)+(10*i)+15);

            }];
            [btn addTarget:self action:@selector(clickComments:) forControlEvents:(UIControlEventTouchDown)];
            
        }
        
        
        
    }
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else if (section ==1){
        return 40;
    }else{
        return 102;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            CoachDetailsCardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoachDetailsCardTableViewCell"];
            
            if (!cell) {
                cell = [[CoachDetailsCardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CoachDetailsCardTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.detailsModel = self.detailsModel;
            return cell;
        }else{
            InstructorDetailsWorksTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InstructorDetailsWorksTableViewCell"];
            
            if (!cell) {
                cell = [[InstructorDetailsWorksTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InstructorDetailsWorksTableViewCell"];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.delegate = self;
            cell.detailsModel = self.detailsModel;
            return cell;
        }
    }else if (indexPath.section == 1){
        InstructorDetailsCourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InstructorDetailsCourseTableViewCell"];
        
        if (!cell) {
            cell = [[InstructorDetailsCourseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InstructorDetailsCourseTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        InstructorDetailsCourseModel *model = [[InstructorDetailsCourseModel alloc]init];
        model = self.courseArray[indexPath.row];
        cell.courseModel = model;
        
        cell.purchase = ^{
          // 购买课程
            CourseDetailsViewController *vc = [[CourseDetailsViewController alloc]init];
            vc.courseModel = self.courseArray[indexPath.row];
            [self.navigationController pushViewController:vc animated:YES];
            
        };
        
        return cell;
    }else{
        CommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentsTableViewCell"];
        
        if (!cell) {
            cell = [[CommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommentsTableViewCell"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        CourseDetailsCommentDetailsModel *model = [[CourseDetailsCommentDetailsModel alloc]init];
        model = self.dataArray[indexPath.row];
        cell.model = model;
        
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        CourseDetailsViewController *vc = [[CourseDetailsViewController alloc]init];
        vc.courseModel = self.courseArray[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            return self.detailsModel.cellheight;
        }else{
            return 153;
        }
    }else if (indexPath.section == 1){
        return 100;
    }
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

-(void)clickComments:(UIButton *)button{

    if (!self.selectedBtn) {
        self.selectedBtn = button;
    }else{
        
        if (self.selectedBtn == button) {
            return;
        }
        
        self.selectedBtn.selected = NO;
        
        
        self.selectedBtn.backgroundColor = RGBA(238, 238, 238, 1);
        self.selectedBtn.layer.borderColor = RGBA(238, 238, 238, 1).CGColor;
        self.selectedBtn.layer.borderWidth = 0.5;
    }
    
    
    button.backgroundColor = RGBA(214, 228, 253, 1);
    button.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
    button.layer.borderWidth = 0.5;
    self.selectedBtn = button;
    self.selectedBtn.selected = YES;
    
    
    if (button.tag == 800) {
        self.dataArray = self.totalArray;

    }else if (button.tag == 801){
        self.dataArray = self.favorableArray;

    }else if (button.tag == 802){
        self.dataArray = self.evaluationArray;

    }else if (button.tag == 803){
        self.dataArray = self.badArray;

    }
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:2];
    [self.mainTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)rightClick:(UIButton *)sender{
    CoachDetailsModel *m = self.detailsModel;
    ShareModel *model = [[ShareModel alloc] init];
    model.image = m.avatar;
    model.title = @"教练详情";
    model.content = m.content;
    ShareThePopupWindow *vc = [ShareThePopupWindow new];
//    vc.model = self.detailsModel;
    vc.model = model;
    [vc popupWithPopType:HWPopTypeShrinkIn dismissType: HWDismissTypeNone];
}
// MARK: 点击了更多作品
-(void)clickInstructorDetailsWorksTableViewCell{
    NSLog(@"作品");
    MyWorkViewController *vc = [[MyWorkViewController alloc]init];
    vc.isJudge = 2;
    vc.detailsModel = self.detailsModel;
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
