//
//  FindCoachCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import <UIKit/UIKit.h>
#import "LookingCoachCourseView.h"
NS_ASSUME_NONNULL_BEGIN

@interface FindCoachCollectionViewCell : UICollectionViewCell
/**头像*/
@property (nonatomic , strong)UIImageView *portraitImageV;
/**名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/**级别*/
@property (nonatomic, strong)UIImageView *levelImageV;
/**关注*/
@property (nonatomic, strong)UILabel *focusLabel;
/**标签1*/
@property (nonatomic, strong)UILabel *titleLabel1;
/**标签2*/
@property (nonatomic, strong)UILabel *titleLabel2;
/**好评*/
@property (nonatomic, strong)UILabel *praiseLabel;
/**好评百分比*/
@property (nonatomic, strong)UILabel *percentageLabel;
/**购买*/
@property (nonatomic, strong)UILabel *buyLabel;
/**课程View*/
@property (nonatomic, strong)LookingCoachCourseView *courseView1;
/**课程View*/
@property (nonatomic, strong)LookingCoachCourseView *courseView2;
@end

NS_ASSUME_NONNULL_END
