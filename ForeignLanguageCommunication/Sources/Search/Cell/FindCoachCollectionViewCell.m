//
//  FindCoachCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "FindCoachCollectionViewCell.h"

@interface FindCoachCollectionViewCell ()
@property (nonatomic, strong)UIView *lineView1;
@property (nonatomic, strong)UIView *lineView2;
@property (nonatomic, strong)UIView *lineView3;

@end

@implementation FindCoachCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.levelImageV];
    [self.contentView addSubview:self.focusLabel];
    [self.contentView addSubview:self.titleLabel1];
    [self.contentView addSubview:self.titleLabel2];
    [self.contentView addSubview:self.praiseLabel];
    [self.contentView addSubview:self.percentageLabel];
    [self.contentView addSubview:self.buyLabel];
    [self.contentView addSubview:self.courseView1];
    [self.contentView addSubview:self.courseView2];
    [self.contentView addSubview:self.lineView1];
    [self.contentView addSubview:self.lineView2];
    [self.contentView addSubview:self.lineView3];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@50);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV).offset(4);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    [self.levelImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.left.equalTo(self.nameLabel.mas_right).offset(7);
    }];
    [self.focusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@25);
        make.width.equalTo(@60);
    }];
    [self.titleLabel1 sizeToFit];
    CGFloat w = self.titleLabel1.bounds.size.width;
    CGFloat h = self.titleLabel1.bounds.size.height;
    self.titleLabel1.layer.cornerRadius = h/2;
    self.titleLabel1.clipsToBounds = YES;
    [self.titleLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.portraitImageV).offset(-5);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
        make.width.equalTo(@(w + 15));
        make.height.equalTo(@(h + 4));
    }];
    [self.titleLabel2 sizeToFit];
    CGFloat w2 = self.titleLabel2.bounds.size.width;
    CGFloat h2 = self.titleLabel2.bounds.size.height;
    self.titleLabel2.layer.cornerRadius = h2/2;
    self.titleLabel2.clipsToBounds = YES;
    [self.titleLabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.titleLabel1);
        make.left.equalTo(self.titleLabel1.mas_right).offset(10);
        make.width.equalTo(@(w2 + 15));
        make.height.equalTo(@(h2 + 4));
    }];
    [self.praiseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV.mas_bottom);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    [self.percentageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.praiseLabel);
        make.left.equalTo(self.praiseLabel.mas_right);
    }];
    [self.lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.percentageLabel);
        make.left.equalTo(self.percentageLabel.mas_right).offset(10);
        make.width.equalTo(@1);
        make.height.equalTo(@15);
    }];
    [self.buyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.praiseLabel);
        make.left.equalTo(self.lineView1.mas_right).offset(10);
    }];
    [self.lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
        make.top.equalTo(self.praiseLabel.mas_bottom).offset(9.5);
    }];
    [self.courseView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.lineView2.mas_bottom);
        make.height.equalTo(@90);
    }];
    [self.lineView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.courseView1.mas_bottom);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
    }];
    [self.courseView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.lineView3.mas_bottom);
        make.height.equalTo(@90);
    }];
    
    
}
- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像2"];
        _portraitImageV.layer.cornerRadius = 50/2;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"张三";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Bold(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UIImageView *)levelImageV{
    if (!_levelImageV) {
        _levelImageV = [[UIImageView alloc]init];
        _levelImageV.image = [UIImage imageNamed:@"找教练-级别"];
    }
    return _levelImageV;
}
- (UILabel *)focusLabel{
    if (!_focusLabel) {
        _focusLabel = [[UILabel alloc]init];
        _focusLabel.backgroundColor = RGBA(52, 120, 245, 1);
        _focusLabel.text = @"关注";
        _focusLabel.textColor = RGBA(255, 255, 255, 1);
        _focusLabel.font = kFont_Medium(14);
        _focusLabel.textAlignment = 1;
        _focusLabel.layer.cornerRadius = 25/2;
        _focusLabel.clipsToBounds = YES;
    }
    return _focusLabel;
}
- (UILabel *)titleLabel1{
    if (!_titleLabel1) {
        _titleLabel1 = [[UILabel alloc]init];
        _titleLabel1.backgroundColor = RGBA(214, 228, 253, 1);
        _titleLabel1.text = @"小学生";
        _titleLabel1.textColor = RGBA(52, 120, 245, 1);
        _titleLabel1.font = kFont_Medium(12);
        _titleLabel1.textAlignment = 1;
        
        
    }
    return _titleLabel1;
}
- (UILabel *)titleLabel2{
    if (!_titleLabel2) {
        _titleLabel2 = [[UILabel alloc]init];
        _titleLabel2.backgroundColor = RGBA(214, 228, 253, 1);
        _titleLabel2.text = @"日常口语";
        _titleLabel2.textColor = RGBA(52, 120, 245, 1);
        _titleLabel2.font = kFont_Medium(12);
        _titleLabel2.textAlignment = 1;
        
        
    }
    return _titleLabel2;
}

- (UILabel *)praiseLabel{
    if (!_praiseLabel) {
        _praiseLabel = [[UILabel alloc]init];
        _praiseLabel.text = @"好评：";
        _praiseLabel.textColor = RGBA(153, 153, 153, 1);
        _praiseLabel.font = kFont(12);
        _praiseLabel.textAlignment = 0;
    }
    return _praiseLabel;
}
- (UILabel *)percentageLabel{
    if (!_percentageLabel) {
        _percentageLabel = [[UILabel alloc]init];
        _percentageLabel.text = @"99%";
        _percentageLabel.textColor = RGBA(153, 153, 153, 1);
        _percentageLabel.font = kFont(12);
        _percentageLabel.textAlignment = 0;
    }
    return _percentageLabel;
}
- (UILabel *)buyLabel{
    if (!_buyLabel) {
        _buyLabel = [[UILabel alloc]init];
        _buyLabel.text = @"999人购买";
        _buyLabel.textColor = RGBA(153, 153, 153, 1);
        _buyLabel.font = kFont(12);
        _buyLabel.textAlignment = 0;
    }
    return _buyLabel;
}
- (UIView *)lineView1{
    if (!_lineView1) {
        _lineView1 = [[UIView alloc]init];
        _lineView1.backgroundColor = RGBA(153, 153, 153, 1);
    }
    return _lineView1;
}
- (UIView *)lineView2{
    if (!_lineView2) {
        _lineView2 = [[UIView alloc]init];
        _lineView2.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView2;
}
- (UIView *)lineView3{
    if (!_lineView3) {
        _lineView3 = [[UIView alloc]init];
        _lineView3.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView3;
}

- (LookingCoachCourseView *)courseView1{
    if (!_courseView1) {
        _courseView1 = [[LookingCoachCourseView alloc]init];
        _courseView1.backgroundColor = UIColor.whiteColor;
    }
    return _courseView1;
}
- (LookingCoachCourseView *)courseView2{
    if (!_courseView2) {
        _courseView2 = [[LookingCoachCourseView alloc]init];
        _courseView2.backgroundColor = UIColor.whiteColor;
    }
    return _courseView2;
}


@end
