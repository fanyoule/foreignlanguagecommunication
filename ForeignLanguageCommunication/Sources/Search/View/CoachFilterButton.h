//
//  CoachFilterButton.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoachFilterButton : UIButton
/**选中时背景色*/
@property (nonatomic, strong) UIColor *selectedBackColor;
/**普通背景色*/
@property (nonatomic, strong) UIColor *normalBackColor;
/**边框颜色*/
@property (nonatomic, strong) UIColor *normalBorderColor;
/**选中边框颜色*/
@property (nonatomic, strong) UIColor *selectedBorderColor;
/**添加到按钮上的选中图片*/
@property (nonatomic, strong) UIImage *selectedImage;
@end

NS_ASSUME_NONNULL_END
