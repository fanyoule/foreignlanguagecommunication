//
//  TagsLabel.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/13.
//

#import "TagsLabel.h"


@implementation TagsLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setTagsArray:(NSArray *)tagsArray{
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] init];
    for (int i = 0; i < tagsArray.count; i ++) {
        NSString *tagStr = tagsArray[i];
        NSMutableAttributedString *tagAtt = [[NSMutableAttributedString alloc] initWithString:tagStr];
        [tagAtt yy_insertString:@"     " atIndex:0];
        [tagAtt yy_appendString:@"     "];
        tagAtt.yy_font = self.font;
        tagAtt.yy_color = self.textcolor;
        
        
        [tagAtt yy_setTextBinding:[YYTextBinding bindingWithDeleteConfirm:NO] range:tagAtt.yy_rangeOfAll];
        YYTextBorder *border = [YYTextBorder new];
        border.fillColor = self.backColor;
        border.strokeColor = self.backColor;
        border.strokeWidth = 2;
        border.cornerRadius = 10;
        border.insets = UIEdgeInsetsMake(-5, -10, -5, -10);
        [tagAtt yy_setTextBackgroundBorder:border range:[tagAtt.string rangeOfString:tagStr]];
        [text appendAttributedString:tagAtt];
       
    }
//    self.textContainerInset = UIEdgeInsetsMake(10, 5, 10, 5);
    
    text.yy_lineSpacing = 20;
    text.yy_lineBreakMode = NSLineBreakByWordWrapping;
    self.preferredMaxLayoutWidth = 200;
    self.numberOfLines = 0;
    self.attributedText = text;
}

@end
