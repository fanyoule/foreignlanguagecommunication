//
//  LookingCoachCourseView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "LookingCoachCourseView.h"

@implementation LookingCoachCourseView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
        
//        self.dropMenu = dropMenu;
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.courseImageV];
    [self addSubview:self.courseContentLabel];
    [self addSubview:self.moneyLabel];
    [self addSubview:self.moneyNumberLabel];
    [self addSubview:self.soldLabel];
    [self.courseImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(15);
        make.width.height.equalTo(@60);
    }];
    [self.courseContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.courseImageV);
        make.left.equalTo(self.courseImageV.mas_right).offset(11);
        make.right.equalTo(self).offset(-25);
    }];
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.courseImageV);
        make.left.equalTo(self.courseImageV.mas_right).offset(10);
        
    }];
    [self.moneyNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.moneyLabel);
        make.left.equalTo(self.moneyLabel.mas_right).offset(1);
    }];
    [self.soldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.courseImageV);
        make.right.equalTo(self).offset(-15.5);
    }];
}

- (UIImageView *)courseImageV{
    if (!_courseImageV) {
        _courseImageV = [[UIImageView alloc]init];
        _courseImageV.image = [UIImage imageNamed:@"测试照片2"];
        _courseImageV.layer.cornerRadius = 6;
        _courseImageV.clipsToBounds = YES;
    }
    return _courseImageV;
}

- (UILabel *)courseContentLabel{
    if (!_courseContentLabel) {
        _courseContentLabel = [[UILabel alloc]init];
        _courseContentLabel.text = @"青少年课程口语1小时练习青少年课程口语1小时练习青少年课程口语1小...";
        _courseContentLabel.textColor = RGBA(24, 24, 24, 1);
        _courseContentLabel.font = kFont(14);
        _courseContentLabel.textAlignment = 0;
        _courseContentLabel.numberOfLines = 2;
    }
    return _courseContentLabel;
}

- (UILabel *)moneyLabel{
    if (!_moneyLabel) {
        _moneyLabel = [[UILabel alloc]init];
        _moneyLabel.text = @"¥";
        _moneyLabel.textColor = RGBA(52, 120, 245, 1);
        _moneyLabel.font = kFont(10);
        _moneyLabel.textAlignment = 0;
    }
    return _moneyLabel;
}

- (UILabel *)moneyNumberLabel{
    if (!_moneyNumberLabel) {
        _moneyNumberLabel = [[UILabel alloc]init];
        _moneyNumberLabel.text = @"3900";
        _moneyNumberLabel.textColor = RGBA(52, 120, 245, 1);
        _moneyNumberLabel.font = kFont_Bold(18);
        _moneyNumberLabel.textAlignment = 0;
    }
    return _moneyNumberLabel;
}

- (UILabel *)soldLabel{
    if (!_soldLabel) {
        _soldLabel = [[UILabel alloc]init];
        _soldLabel.text = @"已售3654";
        _soldLabel.textColor = RGBA(153, 153, 153, 1);
        _soldLabel.font = kFont(10);
        _soldLabel.textAlignment = 0;
    }
    return _soldLabel;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
