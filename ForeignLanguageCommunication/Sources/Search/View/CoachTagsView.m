//
//  CoachTagsView.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/19.
//

#import "CoachTagsView.h"
#import "CoachTagButton.h"

@interface CoachTagsView ()
@property (nonatomic, strong) NSMutableArray *tagViewArray;
@end
@implementation CoachTagsView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (NSMutableArray *)tagViewArray{
    if (!_tagViewArray) {
        _tagViewArray = [NSMutableArray array];
    }
    return _tagViewArray;
}
- (void)setTagArray:(NSArray *)tagArray{
    
    _tagArray = tagArray;
    
    CGFloat lastY = self.insetPadding;
    CGFloat lastX = self.insetMargin;
    
    for (int i = 0; i < tagArray.count; i ++) {
        CoachTagButton *tagb = [CoachTagButton buttonWithType:UIButtonTypeCustom];
        
        [self addSubview:tagb];
        if (self.backColor) {
            tagb.backColor = self.backColor;
        }
        
        if (self.backImage) {
            [tagb setBackgroundImage:self.backImage forState:UIControlStateNormal];
        }
        
        if (self.textColor) {
            [tagb setTitleColor:self.textColor forState:UIControlStateNormal];
        }
        if (self.textFont) {
            tagb.textFont = self.textFont;
        }
        tagb.layer.cornerRadius = self.cornerR;
        tagb.innerSpace = self.innerSpace;
        if (self.tagH > 0) {
            tagb.maxH = self.tagH;
        }
        [tagb setTitle:tagArray[i] forState:UIControlStateNormal];
        
        CGSize size = tagb.size;
        
        CGFloat x = lastX + self.tagMargin + size.width;
        if (lastX+size.width+self.insetMargin > self.maxW) {
            //huanhang
            lastX = self.insetMargin;
            lastY += size.height+self.tagMargin;
        }
        tagb.frame = CGRectMake(lastX, lastY, size.width, size.height);
        
        lastX += size.width+self.tagMargin;
        
        if (i == tagArray.count-1) {
            lastY += size.height+self.insetPadding;
        }
    }
    
    self.size = CGSizeMake(self.maxW, lastY);
    
}
@end
