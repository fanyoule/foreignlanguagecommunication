//
//  LookingCoachCourseView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LookingCoachCourseView : UIView
/**课程图片*/
@property (nonatomic, strong)UIImageView *courseImageV;
/**课程内容*/
@property (nonatomic, strong)UILabel *courseContentLabel;
/**钱*/
@property (nonatomic, strong)UILabel *moneyLabel;
/**钱数*/
@property (nonatomic, strong)UILabel *moneyNumberLabel;
/**已售*/
@property (nonatomic, strong)UILabel *soldLabel;
@end

NS_ASSUME_NONNULL_END
