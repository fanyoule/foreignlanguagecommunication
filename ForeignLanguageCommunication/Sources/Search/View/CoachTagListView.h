//
//  CoachTagListView.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/27.
//

#import <UIKit/UIKit.h>
#import "CoachTagView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CoachTagListView : UIView

/**文字*/
@property (nonatomic, strong) NSArray *textArray;
/**字体*/
@property (nonatomic, strong) UIFont *font;
/**字体颜色*/
@property (nonatomic, strong) UIColor *textColor;
/**背景色*/
@property (nonatomic, strong) UIColor *backColor;
/**背景图片*/
@property (nonatomic, copy) NSString *backImage;
/**左右宽度*/
@property (nonatomic) CGFloat marginSpace;
/**固定高度*/
@property (nonatomic) CGFloat fixableHeight;
/**tag之间宽度*/
@property (nonatomic) CGFloat spaceW;
/**每行之间高度*/
@property (nonatomic) CGFloat spaceH;
/**最终高度*/
@property (nonatomic, readonly) CGFloat latestH;
/**最大宽度*/
@property (nonatomic) CGFloat maxW;

@end

NS_ASSUME_NONNULL_END
