//
//  CoachWithCourseTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/7.
//

#import <UIKit/UIKit.h>
#import "CoachWithCourseModel.h"

typedef void(^clickFollowBlock)(CoachWithCourseModel *model);
typedef void(^clickCourseBlock)(CourseModel *courseModel);

NS_ASSUME_NONNULL_BEGIN

@interface CoachWithCourseTableViewCell : UITableViewCell
@property (nonatomic, strong) CoachWithCourseModel *model;

@property (nonatomic, copy) clickFollowBlock followBlock;

@property (nonatomic, copy) clickCourseBlock courseBlock;
@end

NS_ASSUME_NONNULL_END
