//
//  CoachFilterTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/8.
//

#define kConditionSpace   10

#import "CoachFilterTableViewCell.h"
#import "CoachFilterButton.h"

@interface CoachFilterTableViewCell ()
@property (nonatomic, strong) UILabel *titleLabel;
/**选择条件*/
@property (nonatomic, strong) NSMutableArray *conditionArray;

@property (nonatomic, assign) CGFloat filterBtnW;

@property (nonatomic, strong) CoachFilterButton *selectedBtn;
@end

@implementation CoachFilterTableViewCell


- (CGFloat)filterBtnW{
    return (KSW - 50) / 3;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (NSMutableArray *)conditionArray {
    if (!_conditionArray) {
        _conditionArray = [NSMutableArray array];
    }
    return _conditionArray;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = CustomColor(@"#181818");
        _titleLabel.font = kFont_Medium(14);
    }
    return _titleLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubview];
    }
    return self;
}
- (void)setupSubview {
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(16);
            make.top.mas_offset(5);
    }];
}

- (void)setConditionDic:(NSDictionary *)conditionDic {
    _conditionDic = conditionDic;
    for (CoachFilterButton *btn in self.conditionArray) {
        [btn removeFromSuperview];
    }
    [self.conditionArray removeAllObjects];
    NSArray *array = conditionDic[@"data"];
    NSString *title = conditionDic[@"title"];
    self.titleLabel.text = title;
    
    CGFloat maxY = 0;
    for (int i = 0; i < array.count; i ++) {
        CourseCrowdModel *model = array[i];
        
        CoachFilterButton *btn = [self createFilterButton:model.name];
        
        NSInteger row = i / 3;
        NSInteger col = i % 3;
        //没有默认选中
        btn.frame = CGRectMake(15+col*(self.filterBtnW+kConditionSpace), 35+row*(35+kConditionSpace), self.filterBtnW, 35);
        if (i == array.count -1) {
            maxY = btn.bottom+5;
        }
        btn.tag = i;
        [self.contentView addSubview:btn];
        [self.conditionArray addObject:btn];
    }
    self.maxH = maxY;
    
}




- (CoachFilterButton *)createFilterButton:(NSString *)text {
    CoachFilterButton *btn = [CoachFilterButton buttonWithType:UIButtonTypeCustom];
    btn.normalBorderColor = CustomColor(@"#999999");
    btn.layer.borderWidth = 1.0;
    btn.layer.cornerRadius = 6.0;
    btn.clipsToBounds = YES;
    btn.normalBackColor = UIColor.whiteColor;
    btn.selectedImage = kImage(@"icon_filter_yes");
    btn.selectedBackColor = CustomColor(@"#EAF1FE");
    btn.selectedBorderColor = CustomColor(@"#3478F5");
    [btn setTitle:text forState:UIControlStateNormal];
    [btn setTitleColor:CustomColor(@"#181818") forState:UIControlStateNormal];
    btn.titleLabel.font = kFont_Medium(12);
    [btn addTarget:self action:@selector(selectConditionWith:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}



- (void)selectConditionWith:(CoachFilterButton *)sender {
    //选择了条件
    if (self.selectedBtn == sender) {
        return;
    }
    
    if (self.selectedBtn) {
        self.selectedBtn.selected = NO;
    }
    self.selectedBtn = sender;
    self.selectedBtn.selected = YES;
    
    NSArray *array = self.conditionDic[@"data"];
    CourseCrowdModel *model = array[sender.tag];
    //选择了条件
    if (self.conditionBlock) {
        self.conditionBlock(model,self.titleLabel.text);
    }
}


@end
