//
//  CoachFilterView.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/8.
// 筛选页面  距离底部 120距离 点击收起？

#import <UIKit/UIKit.h>

typedef void(^clickConfirmBlock)(NSDictionary *conditionInfo);
typedef void(^clickResetBlock)(void);
NS_ASSUME_NONNULL_BEGIN

@interface CoachFilterView : UIView
@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, copy) clickConfirmBlock confirmBlock;
@property (nonatomic, copy) clickResetBlock resetBlock;
- (void)close;
- (void)refresh;
@end

NS_ASSUME_NONNULL_END



