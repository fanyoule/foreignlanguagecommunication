//
//  CoachTagView.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**标签视图  文字 字体 颜色 背景色  背景图片  左右宽度 固定高度  圆角*/
@interface CoachTagView : UIView
@property (nonatomic, strong, readonly) UIButton *tagBtn;
/**文字*/
@property (nonatomic, copy) NSString *text;
/**字体*/
@property (nonatomic, strong) UIFont *font;
/**字体颜色*/
@property (nonatomic, strong) UIColor *textColor;
/**背景色*/
@property (nonatomic, strong) UIColor *backColor;
/**背景图片*/
@property (nonatomic, copy) NSString *backImage;
/**左右宽度*/
@property (nonatomic) CGFloat marginSpace;
/**固定高度*/
@property (nonatomic) CGFloat fixableHeight;
/**最后的宽度*/
@property (nonatomic, readonly) CGFloat latestW;

@end

NS_ASSUME_NONNULL_END
