//
//  CoachFilterTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/8.
//
//筛选教练的条件cell
#import <UIKit/UIKit.h>

#import "CourseCrowdModel.h"

typedef void(^clickConditionBlock)(CourseCrowdModel *model,NSString *key);


NS_ASSUME_NONNULL_BEGIN

@interface CoachFilterTableViewCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *conditionDic;

@property (nonatomic, copy) clickConditionBlock conditionBlock;

@property (nonatomic) CGFloat maxH;

@end

NS_ASSUME_NONNULL_END
