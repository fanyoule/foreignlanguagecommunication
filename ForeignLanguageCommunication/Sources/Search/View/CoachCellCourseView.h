//
//  CoachCellCourseView.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/6.
//
//教练 课程 view 顶部有line
#import <UIKit/UIKit.h>
#import "CourseModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^clickCourseContentBlock)(CourseModel *model);

@interface CoachCellCourseView : UIView
@property (nonatomic, strong) CourseModel *model;
@property (nonatomic, copy) clickCourseContentBlock courseContentBlock;

@end

NS_ASSUME_NONNULL_END
