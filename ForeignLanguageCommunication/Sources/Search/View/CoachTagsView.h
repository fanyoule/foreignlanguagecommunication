//
//  CoachTagsView.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoachTagsView : UIView
@property (nonatomic, strong) UIImage *backImage;
@property (nonatomic, strong) UIColor *backColor;
@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, strong) UIFont *textFont;

@property (nonatomic) CGFloat maxW;

@property (nonatomic) CGFloat innerSpace;
@property (nonatomic) CGFloat tagH;
/**标签圆角*/
@property (nonatomic) CGFloat cornerR;
/**标签间隔*/
@property (nonatomic) CGFloat tagMargin;
/**左右间隔*/
@property (nonatomic) CGFloat insetMargin;
/**上下间隔*/
@property (nonatomic)CGFloat insetPadding;

@property (nonatomic, strong) NSArray *tagArray;
@end

NS_ASSUME_NONNULL_END
