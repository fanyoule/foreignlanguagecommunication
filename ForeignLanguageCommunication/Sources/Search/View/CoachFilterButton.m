//
//  CoachFilterButton.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import "CoachFilterButton.h"

@interface CoachFilterButton ()
@property (nonatomic, strong) UIImageView *selectedImageV;
@end

@implementation CoachFilterButton

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        [self setBackgroundColor:self.selectedBackColor];
        self.layer.borderColor = self.selectedBorderColor.CGColor;
        self.selectedImageV.hidden = NO;
        
    }else{
        [self setBackgroundColor:self.normalBackColor];
        self.layer.borderColor = self.normalBorderColor.CGColor;
        self.selectedImageV.hidden = YES;
    }
}

- (void)setNormalBackColor:(UIColor *)normalBackColor {
    _normalBackColor = normalBackColor;
    [self setBackgroundColor:normalBackColor];
}
- (void)setNormalBorderColor:(UIColor *)normalBorderColor{
    _normalBorderColor = normalBorderColor;
    self.layer.borderColor = normalBorderColor.CGColor;
}
- (void)setSelectedImage:(UIImage *)selectedImage{
    _selectedImage = selectedImage;
    self.selectedImageV.image = selectedImage;
    
}

- (UIImageView *)selectedImageV {
    if (!_selectedImageV) {
        _selectedImageV = [UIImageView new];
        [self addSubview:_selectedImageV];
        [_selectedImageV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.bottom.mas_offset(0);
                    make.width.height.mas_equalTo(15);
        }];
        _selectedImageV.hidden = YES;
    }
    return _selectedImageV;
}
@end
