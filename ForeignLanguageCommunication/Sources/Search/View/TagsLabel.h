//
//  TagsLabel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/13.
//

#import <YYText/YYText.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface TagsLabel : YYLabel
@property (nonatomic, strong) NSArray *tagsArray;

@property (nonatomic, strong) UIColor *backColor;
@property (nonatomic, strong) UIColor *textcolor;


@end

NS_ASSUME_NONNULL_END
