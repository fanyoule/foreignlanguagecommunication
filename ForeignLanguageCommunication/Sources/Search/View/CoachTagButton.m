//
//  CoachTagButton.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/19.
//

#import "CoachTagButton.h"

@implementation CoachTagButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)setTextFont:(UIFont *)textFont{
    self.titleLabel.font = textFont;
}
- (void)setBackColor:(UIColor *)backColor {
    [self setBackgroundColor:backColor];
}
- (void)setTitle:(NSString *)title forState:(UIControlState)state{
    [super setTitle:title forState:state];
    
    [self sizeToFit];
    CGSize size = self.size;
    size.width += 2*self.innerSpace;
    size.height = self.maxH;
    self.size = size;
}

@end
