//
//  FindCoachSortingFilterView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "FindCoachSortingFilterView.h"

@interface FindCoachSortingFilterView ()
@property (nonatomic, strong)UIView *lineView1;
@property (nonatomic, strong)UIView *lineView2;



@end

@implementation FindCoachSortingFilterView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.IntelligentSortingBtn];
    [self addSubview:self.screeningBtn];
    [self addSubview:self.lineView1];
    [self addSubview:self.lineView2];
    [self.IntelligentSortingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self);
        make.right.equalTo(self.mas_centerX);
    }];
    [self.screeningBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(self);
        make.left.equalTo(self.mas_centerX);
    }];
    [self.lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@1);
        make.height.equalTo(@15);
        make.center.equalTo(self);
    }];
    [self.lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(@0.5);
    }];
}

- (UIView *)lineView1{
    if (!_lineView1) {
        _lineView1 = [[UIView alloc]init];
        _lineView1.backgroundColor = RGBA(204, 204, 204, 1);
    }
    return _lineView1;
}

- (UIView *)lineView2{
    if (!_lineView2) {
        _lineView2 = [[UIView alloc]init];
        _lineView2.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView2;
}

- (UIButton *)IntelligentSortingBtn{
    if (!_IntelligentSortingBtn) {
        _IntelligentSortingBtn = [[UIButton alloc]init];
        
        [_IntelligentSortingBtn setTitle:@"智能排序∨" forState:(UIControlStateNormal)];
        [_IntelligentSortingBtn setTitleColor:RGBA(51, 51, 51, 1) forState:(UIControlStateNormal)];
        [_IntelligentSortingBtn setTitle:@"智能排序∧" forState:(UIControlStateSelected)];
        [_IntelligentSortingBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateSelected)];
        _IntelligentSortingBtn.titleLabel.font = kFont(14);
        [_IntelligentSortingBtn addTarget:self action:@selector(IntelligentSortingBtn:) forControlEvents:(UIControlEventTouchDown)];
    }
    return _IntelligentSortingBtn;
}

- (UIButton *)screeningBtn{
    if (!_screeningBtn) {
        _screeningBtn = [[UIButton alloc]init];
        [_screeningBtn setTitle:@"筛选∨" forState:(UIControlStateNormal)];
        [_screeningBtn setTitleColor:RGBA(51, 51, 51, 1) forState:(UIControlStateNormal)];
        [_screeningBtn setTitle:@"筛选∧" forState:(UIControlStateSelected)];
        [_screeningBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateSelected)];
        _screeningBtn.titleLabel.font = kFont(14);
        [_screeningBtn addTarget:self action:@selector(screening:) forControlEvents:(UIControlEventTouchDown)];
    }
    return _screeningBtn;
}

-(void)IntelligentSortingBtn:(UIButton *)button{
    button.selected = !button.selected;
}

-(void)screening:(UIButton *)button{
    button.selected = !button.selected;
    if (self.screeningBlock) {
        self.screeningBlock(button.isSelected);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
