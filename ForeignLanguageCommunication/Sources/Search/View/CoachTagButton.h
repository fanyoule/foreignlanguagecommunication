//
//  CoachTagButton.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoachTagButton : UIButton

@property (nonatomic) CGFloat innerSpace;
/**固定高度*/
@property (nonatomic) CGFloat maxH;
@property (nonatomic, strong) UIColor *backColor;
@property (nonatomic, strong) UIFont *textFont;

@end

NS_ASSUME_NONNULL_END
