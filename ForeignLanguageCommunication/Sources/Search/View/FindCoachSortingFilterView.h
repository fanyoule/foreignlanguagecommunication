//
//  FindCoachSortingFilterView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import <UIKit/UIKit.h>

typedef void(^clickScreeningBlock)(BOOL isShow);

NS_ASSUME_NONNULL_BEGIN

@interface FindCoachSortingFilterView : UIView
/**智能排序*/
@property (nonatomic, strong)UIButton *IntelligentSortingBtn;
/**筛选*/
@property (nonatomic, strong)UIButton *screeningBtn;

@property (nonatomic, copy) clickScreeningBlock screeningBlock;
@end

NS_ASSUME_NONNULL_END
