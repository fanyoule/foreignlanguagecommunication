//
//  CoachWithCourseTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/7.
//

#import "CoachWithCourseTableViewCell.h"
#import "CoachCellCourseView.h"

#import "CoachTagListView.h"

@interface CoachWithCourseTableViewCell ()
/**背景圆角*/
@property (nonatomic, strong) UIView *backView;
/**头像*/
@property (nonatomic, strong) UIImageView *avatarView;
/**姓名*/
@property (nonatomic, strong) UILabel *nameLabel;
/**认证icon 没有使用*/
@property (nonatomic, strong) UIImageView *iconView;
/**关注按钮 关注过的隐藏 */
@property (nonatomic, strong) UIButton *followBtn;
/**好评率 百分比*/
@property (nonatomic, strong) UILabel *goodPercentLabel;
/**购买数 XX人购买*/
@property (nonatomic, strong) UILabel *countLabel;
/**分割线 没有使用*/
@property (nonatomic, strong) UIView *lineVIew;
/**课程视图数组 最多两个 */
@property (nonatomic, strong) NSMutableArray *courseViewArray;
/**标签 数组 不使用*/
@property (nonatomic, strong) NSMutableArray *tagLabelArray;


/**适用人群*/
@property (nonatomic, strong) UILabel *targetLabel;
/**教学内容*/
@property (nonatomic, strong) UILabel *courseContentLabel;
/**好评率 标题*/
@property (nonatomic, strong) UILabel *goodTitleLabel;
/**顶部背景视图 没有使用 */
@property (nonatomic, strong) UIView *topbackView;

// 标题高度
@property (nonatomic, assign) CGFloat nameHeight;
// 小标题高度
@property (nonatomic, assign) CGFloat subTitleHeight;
//小标题宽度
@property (nonatomic, assign) CGFloat subTitleWidth;

/**适应人群*/
@property (nonatomic, strong) CoachTagListView *crowdTagView;
/**教学内容*/
@property (nonatomic, strong) CoachTagListView *teachContentTagView;
@end

@implementation CoachWithCourseTableViewCell

- (CoachTagListView *)crowdTagView{
    if (!_crowdTagView) {
        _crowdTagView = [[CoachTagListView alloc] init];
        _crowdTagView.backImage = @"icon_tag_light_blue";
        _crowdTagView.textColor = CustomColor(@"#3478F5");
        _crowdTagView.font = kFont(11);
        _crowdTagView.fixableHeight = 20;
        _crowdTagView.marginSpace = 8;
        _crowdTagView.spaceW = 6;
        _crowdTagView.spaceH = 8;
        // model 中 设置 text MaxW
    }
    return _crowdTagView;
}
- (CoachTagListView *)teachContentTagView{
    if (!_teachContentTagView) {
        _teachContentTagView = [[CoachTagListView alloc] init];
        _teachContentTagView.backImage = @"icon_tag_light_blue";
        _teachContentTagView.textColor = CustomColor(@"#3478F5");
        _teachContentTagView.font = kFont(11);
        _teachContentTagView.fixableHeight = 20;
        _teachContentTagView.marginSpace = 8;
        _teachContentTagView.spaceW = 6;
        _teachContentTagView.spaceH = 8;
        // model 中 设置 text MaxW
    }
    
    return _teachContentTagView;
}

/**课程 固定最多两个*/
- (NSMutableArray *)courseViewArray {
    if (!_courseViewArray) {
        _courseViewArray = [NSMutableArray array];
        for (int i = 0; i < 2; i ++) {
            CoachCellCourseView *courseView = [[CoachCellCourseView alloc] init];
            [_courseViewArray addObject:courseView];
            kWeakSelf(self);
            courseView.courseContentBlock = ^(CourseModel * _Nonnull model) {
              //点击了课程
                if (weakself.courseBlock) {
                    weakself.courseBlock(model);
                }
            };
        }
    }
    return _courseViewArray;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.layer.cornerRadius = 12.0;
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(1, 1);
        _backView.layer.shadowColor = rgba(0, 0, 0, 0.2).CGColor;
        _backView.layer.shadowRadius = 6.0;
        _backView.layer.shadowOpacity = 1;
    }
    return _backView;
}

- (UIImageView *)avatarView {
    if (!_avatarView) {
        _avatarView = [UIImageView new];
//        _avatarView.layer.cornerRadius = 6.0;
//        _avatarView.clipsToBounds = YES;
        _avatarView.layer.cornerRadius = 6.0;
        _avatarView.layer.borderColor = CustomColor(@"#E7E7E7").CGColor;
        _avatarView.layer.borderWidth  =1.0;
        _avatarView.contentMode = UIViewContentModeScaleAspectFill;
        _avatarView.clipsToBounds = YES;
    }
    return _avatarView;
}
- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] initWithImage:kImage(@"找教练-级别")];
        
    }
    return _iconView;
}
- (UIButton *)followBtn {
    if (!_followBtn) {
        _followBtn = [UIButton buttonWithType: UIButtonTypeCustom];
        [_followBtn setTitle:@"关注" forState:UIControlStateNormal];
        [_followBtn setBackgroundColor:CustomColor(@"#3478F5")];
        _followBtn.titleLabel.font = kFont_Medium(14);
        _followBtn.layer.cornerRadius = 6.0;
        [_followBtn addTarget: self action:@selector(clickFollow:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _followBtn;
}
- (UILabel *)goodTitleLabel {
    if (!_goodTitleLabel) {
        _goodTitleLabel = [UILabel new];
        _goodTitleLabel.text = @"好评：";
        _goodTitleLabel.font = kFont(12);
        _goodTitleLabel.textColor  = CustomColor(@"#666666");
        [_goodTitleLabel sizeToFit];
        self.subTitleHeight = _goodTitleLabel.size.height;
        self.subTitleWidth = _goodTitleLabel.size.width;
    }
    return _goodTitleLabel;
}
/**好评率*/
- (UILabel *)goodPercentLabel {
    if (!_goodPercentLabel) {
        _goodPercentLabel = [UILabel new];
         
        _goodPercentLabel.textColor = CustomColor(@"#FF8016");
        _goodPercentLabel.font = kFont(12);
    }
    return _goodPercentLabel;
}
//MARK: 已售
- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [UILabel new];
        _countLabel.textColor  = CustomColor(@"#AFB2C2");
        _countLabel.font = kFont(12);
    }
    return _countLabel;
}
- (UIView *)lineVIew {
    if (!_lineVIew) {
        _lineVIew = [UIView new];
        _lineVIew.backgroundColor = CustomColor(@"#999999");
        
    }
    return _lineVIew;
}
//MARK: 顶部视图
- (UIView *)topbackView{
    if (!_topbackView) {
        _topbackView = [UIView new];
        _topbackView.backgroundColor = UIColor.whiteColor;
    }
    return _topbackView;
}

- (UILabel *)targetLabel{
    if (!_targetLabel) {
        _targetLabel = [ UILabel new];
        _targetLabel.textColor = CustomColor(@"#666666");
        _targetLabel.font = kFont(12);
        _targetLabel.text = @"适用人群";
        [_targetLabel sizeToFit];
    }
    return _targetLabel;
}
- (UILabel *)courseContentLabel {
    if (!_courseContentLabel) {
        _courseContentLabel = [UILabel new];
        _courseContentLabel.textColor = CustomColor(@"#666666");
        _courseContentLabel.font = kFont(12);
        _courseContentLabel.text = @"教学内容";
        [_courseContentLabel sizeToFit];
        self.subTitleWidth = _courseContentLabel.size.width;
        self.subTitleHeight = _courseContentLabel.size.height;
    }
    return _courseContentLabel;
}


- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.textColor = CustomColor(@"#222222");
        _nameLabel.font = kFont_Bold(16);
        _nameLabel.text = @"未知用户";
        
        [_nameLabel sizeToFit];
        self.nameHeight = _nameLabel.size.height;
    }
    return _nameLabel;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubview];
    }
    return self;
}
- (void)setupSubview {
    
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.topbackView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_offset(5);
            make.left.mas_offset(15);
            make.right.mas_offset(-15);
            make.bottom.mas_offset(-10);
    }];
    
    [self.backView addSubview:self.avatarView];
    [self.backView addSubview:self.nameLabel];
    [self.backView addSubview:self.targetLabel];
    [self.backView addSubview:self.courseContentLabel];
    [self.backView addSubview:self.goodTitleLabel];
    [self.backView addSubview:self.goodPercentLabel];
    [self.backView addSubview:self.countLabel];
    [self.backView addSubview:self.followBtn];
    
    [self.backView addSubview:self.crowdTagView];
    [self.backView addSubview:self.teachContentTagView];
    
    for (CoachCellCourseView *courseView in self.courseViewArray) {
        courseView.hidden = YES;
        [self.backView addSubview:courseView];
    }
    
    
    [self.avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(10);
            make.top.mas_offset(20);
            make.width.height.mas_equalTo(88);
    }];
    
    [self.followBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(25);
        make.centerY.mas_equalTo(self.nameLabel);
        make.right.mas_offset(-15);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.avatarView.mas_top).mas_offset(5);
        make.left.mas_equalTo(self.avatarView.mas_right).mas_offset(@10);
        make.right.mas_equalTo(self.followBtn.mas_left).mas_offset(-10);
    }];
    
    [self.targetLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.nameLabel);
            make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(14);
    }];
}

- (void)clickFollow:(UIButton *)sender {
    //点击了关注
    NSLog(@"点击了关注");
    if (self.followBlock) {
        self.followBlock(self.model);
    }
}

- (void)setModel:(CoachWithCourseModel *)model {
    _model = model;
    CGFloat maxY = 25;
    [self.avatarView sd_setImageWithURL:[Util getQNMiniPicWithUrl:model.avatar]];
    
    //名字
    self.nameLabel.text = model.realName;
    [self.nameLabel sizeToFit];
    
    maxY += self.nameLabel.size.height + 14;
    
//    //关注按钮
//    self.followBtn.hidden = model.isFollow;
    self.followBtn.hidden = YES;
    
    //好评率
    self.goodPercentLabel.text =model.goodRate;
    //购买数
    self.countLabel.text = [NSString stringWithFormat:@"%ld人购买",model.orderNum];
    //标签  两种
    self.crowdTagView.hidden = YES;
    //计算 标签最大宽度
    
    CGFloat tagViewW = KSW - 30 - 10 - 88 - 10 - self.subTitleWidth - 10 - 15;
    
    NSMutableArray *crowdArray = [NSMutableArray arrayWithArray:[model.applyCrowd componentsSeparatedByString:@","]];
    if (crowdArray.count>0) {
        if (crowdArray.count>1) {
            [crowdArray removeLastObject];
        }
        self.crowdTagView.hidden = NO;
        self.crowdTagView.maxW = tagViewW;
        
        self.crowdTagView.textArray = crowdArray;
        CGFloat h = self.crowdTagView.latestH;
        
        [self.crowdTagView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(tagViewW);
                    make.height.mas_equalTo(h);
                    make.left.mas_equalTo(self.targetLabel.mas_right).mas_offset(10);
                    make.top.mas_equalTo(self.targetLabel.mas_top).mas_offset(-3);
        }];
        
        maxY += h-3;
        
        [self.courseContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.crowdTagView.mas_bottom).mas_offset(12);
                    make.left.mas_equalTo(self.targetLabel);
        }];
        maxY += 12;
        
    }else{
        //教学内容标题
        maxY += self.subTitleHeight + 16;
        
        [self.courseContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.targetLabel.mas_bottom).mas_offset(16);
                    make.left.mas_equalTo(self.targetLabel);
        }];
    }
    //教学内容
    NSMutableArray *teachArray = [NSMutableArray arrayWithArray:[model.teachingContent componentsSeparatedByString:@","]];
    
    self.teachContentTagView.hidden = YES;
    if (teachArray.count > 0) {
        //有标签
        if (teachArray.count>1) {
            [teachArray removeLastObject];
        }
       
        self.teachContentTagView.maxW = tagViewW;
        self.teachContentTagView.textArray = teachArray;
        self.teachContentTagView.hidden = NO;
        CGFloat h = self.teachContentTagView.latestH;
        [self.teachContentTagView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(tagViewW);
                    make.left.mas_equalTo(self.courseContentLabel.mas_right).mas_offset(10);
                    make.top.mas_equalTo(self.courseContentLabel.mas_top).mas_offset(-3);
                    make.height.mas_equalTo(h);
        }];
        // 标签底部
        maxY += h-3;
        
        [self.goodTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.teachContentTagView.mas_bottom).mas_offset(12);
                    make.left.mas_equalTo(self.courseContentLabel);
        }];
        //好评位置
        maxY += self.subTitleHeight + 12;
        
    }else{
        //没有教学内容标签
        maxY += self.subTitleHeight+16 + self.subTitleHeight;
        
        [self.goodTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(self.courseContentLabel.mas_bottom).mas_offset(16);
                    make.bottom.mas_offset(-10);
            make.left.mas_equalTo(self.courseContentLabel);
        }];
    }
    
    maxY += 10;
    [self.goodPercentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.goodTitleLabel);
            make.left.mas_equalTo(self.goodTitleLabel.mas_right);
    }];
    
    [self.countLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_offset(-15);
            make.centerY.mas_equalTo(self.goodTitleLabel);
    }];
    
    
    
    //设置 课程View
    for (CoachCellCourseView *courseView in self.courseViewArray) {
        courseView.hidden = YES;
    }
    if (model.course.count > 0 ) {
        
        for (int i = 0; i < model.course.count; i ++) {
            if (i < self.courseViewArray.count) {
                CoachCellCourseView *courseView = self.courseViewArray[i];
                courseView.hidden = NO;
                courseView.model = model.course[i];
                
            }
        }
        
        CoachCellCourseView *courseView1 = self.courseViewArray[0];
        CGFloat h = courseView1.model.height;
        courseView1.frame = CGRectMake(10, maxY, KSW - 30 -20, h);
//        [courseView1 mas_remakeConstraints:^(MASConstraintMaker *make) {
//                    make.left.mas_offset(10);
//                    make.right.mas_offset(-10);
//            make.top.mas_offset(maxY);
//                    make.height.mas_equalTo(h);
//        }];
        maxY += h+10;
        if (model.course.count == 2) {
            CoachCellCourseView *courseView2 = self.courseViewArray[1];
            CGFloat h = courseView2.model.height;
            courseView2.frame = CGRectMake(10, maxY, KSW-30-20, h);
//            [courseView2 mas_remakeConstraints:^(MASConstraintMaker *make) {
//                        make.left.mas_offset(10);
//                        make.right.mas_offset(-10);
//                        make.top.mas_equalTo(courseView1.mas_bottom).mas_offset(10);
//                        make.height.mas_equalTo(h);
//                make.bottom.mas_offset(-15);
//            }];
            maxY += h;
        }
        maxY += 15;
    }else{
        maxY += 15;
    }
    
    model.height = maxY;
    
    
    
}

@end
