//
//  CoachTagListView.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/27.
//

#import "CoachTagListView.h"

#import "CoachTagView.h"

@interface CoachTagListView ()
@property (nonatomic, strong) NSMutableArray *listArray;
@end
@implementation CoachTagListView

- (instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}

- (NSMutableArray *)listArray{
    if (!_listArray) {
        _listArray = [NSMutableArray array];
    }
    return _listArray;
}


- (void)setTextArray:(NSArray *)textArray {
    _textArray = textArray;
    for (CoachTagView *tagV in self.listArray) {
        [tagV removeFromSuperview];
    }
    [self.listArray removeAllObjects];
    
    CGFloat startX = 0;
    CGFloat startY = 0;
    
    for (int i = 0; i < textArray.count; i ++) {
        
        CoachTagView *tagV = [[CoachTagView alloc] init];
        tagV.backColor = self.backColor;
        tagV.backImage = self.backImage;
        tagV.font = self.font;
        tagV.textColor = self.textColor;
        tagV.marginSpace = self.marginSpace;
        tagV.fixableHeight = self.fixableHeight;
        
        tagV.text = textArray[i];
        //计算得到size
        CGSize size = tagV.size;
        
        if (startX+size.width>self.maxW) {
            //换行
            startX = 0;
            startY = startY + size.height + self.spaceH;
        }
        
        tagV.frame = CGRectMake(startX, startY, size.width, size.height);
        startX = startX+size.width+self.spaceW;
        [self addSubview:tagV];
        [self.listArray addObject:tagV];
        
    }
    //最后的高度
    _latestH = startY + self.fixableHeight;
    self.size = CGSizeMake(self.maxW, _latestH);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
