//
//  CoachCellCourseView.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/6.
//  左右与父视图空10距离
//上下 10 距离

#import "CoachCellCourseView.h"
#import "CoachTagListView.h"


@interface CoachCellCourseView ()

/**封面图*/
@property (nonatomic, strong) UIImageView *avatarView;
/**标题*/
@property (nonatomic, strong) UILabel *nameLabel;
/**价格*/
@property (nonatomic, strong) UILabel *priceLabel;
/**已售*/
@property (nonatomic, strong) UILabel *countLabel;
/**点击按钮*/
@property (nonatomic, strong) UIButton *courseBtn;
/**标签视图数组*/
@property (nonatomic, strong) NSMutableArray *tagArray;

/**标签集合视图*/
@property (nonatomic, strong) CoachTagListView *tagsView;

@property (nonatomic, strong) UIImageView *backImageV;
@end

@implementation CoachCellCourseView
- (UIImageView *)backImageV {
    if (!_backImageV) {
        _backImageV = [[UIImageView alloc] initWithImage:kImage(@"icon_coach_course_back")];
        
    }
    return _backImageV;
}

- (CoachTagListView *)tagsView{
    if (!_tagsView) {
        _tagsView = [[CoachTagListView alloc] init];
        _tagsView.backImage = @"icon_tag_light_green";
        _tagsView.font = kFont_Regular(11);
        _tagsView.textColor = CustomColor(@"#04BF5E");
        _tagsView.fixableHeight = 21;
        _tagsView.marginSpace = 6;
        _tagsView.spaceW = 12;
        _tagsView.spaceH = 10;
        
    }
    return _tagsView;
}

- (instancetype)init{
    if (self = [super init]) {
        [self setupSubview];
    }
    return self;
}

- (UIButton *)courseBtn {
    if (!_courseBtn) {
        _courseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _courseBtn.backgroundColor = UIColor.clearColor;
        [_courseBtn addTarget:self action:@selector(clickCourse:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _courseBtn;
}
- (void)clickCourse:(UIButton *)sender {
    if (self.courseContentBlock) {
        self.courseContentBlock(self.model);
    }
}

- (void)setupSubview {
    self.backgroundColor = rgba(250, 252, 255, 1);
    self.layer.cornerRadius = 6.0;
    [self addSubview:self.backImageV];
    [self addSubview:self.avatarView];
    [self addSubview:self.nameLabel];
    [self addSubview:self.priceLabel];
    [self addSubview:self.countLabel];
    
    [self addSubview:self.tagsView];
    [self addSubview:self.courseBtn];
    //背景图
    [self.backImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self);
    }];
  
    [self.avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(61.5);
            make.left.mas_offset(14);
        make.top.mas_offset(14);
        
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.avatarView);
            make.left.mas_equalTo(self.avatarView.mas_right).mas_offset(15);
            make.right.mas_offset(-15);
            
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(self.nameLabel);
    }];
    
    
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.priceLabel.mas_bottom).mas_offset(10);
        make.right.mas_equalTo(self.priceLabel);
    }];
    
//    [self.courseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.edges.mas_equalTo(self);
//    }];
    
    
    
    
}
- (NSMutableArray *)tagArray {
    if (!_tagArray) {
        _tagArray = [NSMutableArray array];
    }
    return _tagArray;
}



- (UIImageView *)avatarView {
    if (!_avatarView) {
        _avatarView = [UIImageView new];
        _avatarView.layer.cornerRadius = 6.0;
        _avatarView.clipsToBounds = YES;
        _avatarView.contentMode = UIViewContentModeScaleAspectFill;
        _avatarView.image= kImage(@"icon_coach_course");
    }
    return _avatarView;
}
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.numberOfLines = 1;
        _nameLabel.textColor = CustomColor(@"#222222");
        _nameLabel.font = kFont_Bold(14);
        _nameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    return _nameLabel;
}
- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [UILabel new];
        _priceLabel.textColor = CustomColor(@"#FF4320");
    }
    return _priceLabel;
}

- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [UILabel new];
        _countLabel.textColor = CustomColor(@"#B2B6BF");
        _countLabel.font = kFont(11);
    }
    return _countLabel;
}
- (void)setModel:(CourseModel *)model {
    _model = model;
    if (model.coursePic.count > 0) {
        NSDictionary *dic = model.coursePic[0];
        if (dic) {
            NSString *url = dic[@"url"];
            [self.avatarView sd_setImageWithURL:[Util getQNMiniPicWithUrl:url]];
        }
    }
    
    self.nameLabel.text = model.name;
    
    NSMutableAttributedString *priceAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%ld",model.price]];
    [priceAtt addAttributes:@{NSFontAttributeName:kFont_Bold(10)} range:NSMakeRange(0, 1)];
    [priceAtt addAttributes:@{NSFontAttributeName:kFont_Bold(18)} range:NSMakeRange(1, priceAtt.string.length - 1)];
    self.priceLabel.attributedText = priceAtt;
    
    [self.priceLabel sizeToFit];
    CGSize priceSize = self.priceLabel.size;
    self.countLabel.text = [NSString stringWithFormat:@"已售%ld",model.orders];
    /**最外面cell 左右空15 此View 左右空10 左边图片空14 右边文字空15*/
    //CGFloat w = KSW -30 -15 -85 - priceSize.width - 15;
    NSMutableArray *tagArray = [NSMutableArray array];
    CGFloat tagviewW = 130;
    for (int i = 0 ; i < model.applyCrowd.count; i ++) {
        CourseCrowdModel *coursemodel = model.applyCrowd[i];
        [tagArray addObject:coursemodel.name];
    }
    [tagArray addObject:model.teachingContent.name];
    
    self.tagsView.maxW = tagviewW;
    self.tagsView.textArray = tagArray;
    
    //布局
    self.tagsView.hidden = NO;
    
    CGFloat h = self.tagsView.latestH;
    [self.tagsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.avatarView.mas_right).mas_offset(10);
            make.top.mas_equalTo(self.priceLabel.mas_top).mas_offset(-1);
            make.width.mas_equalTo(tagviewW);
            make.height.mas_equalTo(h);
    }];
    if (h+53+10>106) {
        model.height = h+53+10;
    }else{
        model.height = 106;
    }
    self.courseBtn.frame = CGRectMake(0, 0, KSW-30-20, model.height);
}



@end
