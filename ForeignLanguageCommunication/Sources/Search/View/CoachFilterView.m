//
//  CoachFilterView.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/8.
//

#import "CoachFilterView.h"
#import "CoachFilterTableViewCell.h"
@interface CoachFilterView ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UITableView *myTableview;
@property (nonatomic, strong) NSMutableDictionary *selectDic;
@end

@implementation CoachFilterView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.myTableview.delegate = self;
    self.myTableview.dataSource = self;
    self.myTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
}
-(NSMutableDictionary *)selectDic{
    if (!_selectDic) {
        _selectDic = [NSMutableDictionary dictionary];
    }
    return _selectDic;
}
- (NSMutableArray *)dataArray {
    if (!_dataArray ) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CoachFilterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"filter"];
    if (!cell) {
        cell = [[CoachFilterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"filter"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.conditionDic = self.dataArray[indexPath.row];
    cell.conditionBlock = ^(CourseCrowdModel *model,NSString *key) {
        [self.selectDic setValue:model forKey:key];
    };
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CoachFilterTableViewCell *cell = [self tableView:self.myTableview cellForRowAtIndexPath:indexPath];
    return cell.maxH;
}

- (IBAction)resetContent:(id)sender {
    if (self.resetBlock) {
        self.resetBlock();
    }
    //重置所有选中
    [self.myTableview reloadData];
    [self.selectDic removeAllObjects];
    
}
- (IBAction)confirmContent:(id)sender {
    if (self.confirmBlock) {
        self.confirmBlock(self.selectDic);
    }
    [self close];
    
}
- (void)close{
//    [UIView animateWithDuration:0.25 animations:^{
//        self.height = 0;
//    } completion:^(BOOL finished) {
//        [self removeFromSuperview];
//    }];
    
    [self removeFromSuperview];
}

- (void)refresh{
    [self.myTableview reloadData];
}
@end




