//
//  CoachTagView.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/27.
//

#import "CoachTagView.h"

@interface CoachTagView ()
@property (nonatomic, strong) UIButton *tagBtn;

@property (nonatomic, strong) UIImageView *contentImageV;
@property (nonatomic, strong) UILabel *contentLabel;
@end

@implementation CoachTagView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init{
    if (self = [super init]) {
        
        [self addSubview:self.contentImageV];
        
        self.contentImageV.hidden = YES;
        [self addSubview:self.contentLabel];
//        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.center.mas_equalTo(self);
//        }];
    }
    return self;
}

- (UIButton *)tagBtn{
    if (!_tagBtn) {
        _tagBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _tagBtn.backgroundColor = UIColor.clearColor;
        
    }
    return _tagBtn;
}

- (UIImageView *)contentImageV{
    if (!_contentImageV) {
        _contentImageV = [UIImageView new];
        
    }
    return _contentImageV;
}
- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.backgroundColor = UIColor.clearColor;
        _contentLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _contentLabel;
}

- (void)setText:(NSString *)text{
    _text = text;
    //最后赋值  开始设置UI并计算
    
    if (self.backColor) {
        self.backgroundColor = self.backColor;
    }
    self.contentImageV.hidden = YES;
    if (self.backImage) {
        self.contentImageV.image = kImage(self.backImage);
        self.contentImageV.hidden = NO;
    }
    
    
    UIFont *fontT = self.font ? self.font : kFont(12);
    UIColor *colorT = self.textColor ? self.textColor : CustomColor(@"666666");
    
    self.contentLabel.textColor = colorT;
    self.contentLabel.font = fontT;
    self.contentLabel.text = text;
    [self.contentLabel sizeToFit];
    
    CGSize size = self.contentLabel.size;
   
    size.width = size.width+2*self.marginSpace;
    size.height = self.fixableHeight;
    //设置 view的size
    
    
    self.contentImageV.frame = CGRectMake(0, 0, size.width, size.height);
    self.contentLabel.frame = CGRectMake(0, 0, size.width, size.height);
    self.size = size;
    _latestW = size.width;
}
@end
