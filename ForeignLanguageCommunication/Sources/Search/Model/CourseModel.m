//
//  CourseModel.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/7.
//

#import "CourseModel.h"

@implementation CourseModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
}
+ (NSDictionary *)mj_objectClassInArray{
    return @{@"applyCrowd":[CourseCrowdModel class]};
}
@end
