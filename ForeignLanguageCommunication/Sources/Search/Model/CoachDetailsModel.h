//
//  CoachDetailsModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/27.
//  教练详情

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoachDetailsModel : NSObject
/** 适应人群*/
@property (nonatomic, strong)NSString *applyCrowd;
/** 头像*/
@property (nonatomic, strong)NSString *avatar;
/** 简介*/
@property (nonatomic, strong)NSString *content;
/** 课程*/
@property (nonatomic, strong)NSArray *course;
/** 评价*/
@property (nonatomic, strong)NSArray *evaluation;
/** 评价数*/
@property (nonatomic, assign)NSInteger evaluationNum;
/** 粉丝数*/
@property (nonatomic, assign)NSInteger fansNum;
/** 好评率*/
@property (nonatomic, strong)NSString *goodRate;
/** 是否关注*/
@property (nonatomic)BOOL isFollow;
/** 购买数*/
@property (nonatomic, assign)NSInteger orderNum;
/** 真实姓名*/
@property (nonatomic, strong)NSString *realName;
/** 性别0女，1男*/
@property (nonatomic, assign)NSInteger sex;
/** 教学内容*/
@property (nonatomic, strong)NSString *teachingContent;
/** 教练id*/
@property (nonatomic, assign)NSInteger userId;
/** 作品数*/
@property (nonatomic, assign)NSInteger workNum;
/** 作品*/
@property (nonatomic, strong)NSArray *works;

@property(nonatomic,assign)CGFloat cellheight;


@end

NS_ASSUME_NONNULL_END
