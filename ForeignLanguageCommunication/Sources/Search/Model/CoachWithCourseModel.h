//
//  CoachWithCourseModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/7.
//

#import <Foundation/Foundation.h>
#import "CourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CoachWithCourseModel : NSObject

//@property (nonatomic, copy) NSString *avatarUrl;
//@property (nonatomic, copy) NSString *nickName;
//@property (nonatomic, assign) float percent;
//@property (nonatomic, strong) NSArray *tagList;
//@property (nonatomic, copy) NSString *teachingTag;
//@property (nonatomic) BOOL isFan;
//
//@property (nonatomic) BOOL isVerify;
//@property (nonatomic) NSInteger sales;
//@property (nonatomic, strong) NSArray *courseList;
@property (nonatomic) CGFloat height;
//MARK:------------------------------------------
/**适应人群*/
@property (nonatomic, copy) NSString *applyCrowd;
/***/
@property (nonatomic, copy) NSString *avatar;
/**课程*/
@property (nonatomic, strong) NSArray <CourseModel *> *course;
/**好评率*/
@property (nonatomic, copy) NSString *goodRate;
/**是否关注*/
@property (nonatomic) BOOL isFollow;
/**购买数量*/
@property (nonatomic) NSInteger orderNum;
/**真实姓名*/
@property (nonatomic, copy) NSString *realName;
/**性别*/
@property (nonatomic) NSInteger sex;
/**教学内容*/
@property (nonatomic, copy) NSString *teachingContent;
@property (nonatomic) NSInteger userId;
/*
 {
         "applyCrowd": "",
         "avatar": "",
         "course": [],
         "goodRate": "",
         "isFollow": true,
         "orderNum": 0,
         "realName": "",
         "sex": 0,
         "teachingContent": "",
         "userId": 0
     }
 */

/*
 {
     "applyCrowd": [
         {
             "createTime": "",
             "kind": 0,
             "name": ""
         }
     ],
     "applyCrowd1": "",
     "content": "",
     "coursePic": [],
     "evaluation": [
         {
             "evaluation": [
                 {
                     "content": "",
                     "courseId": 0,
                     "createTime": "",
                     "evaluation": 0,
                     "nickname": "",
                     "url": "",
                     "userId": 0
                 }
             ],
             "num": 0,
             "type": ""
         }
     ],
     "id": 0,
     "isCollect": 0,
     "name": "",
     "orders": 0,
     "price": 0,
     "realName": "",
     "teachingContent": {
         "createTime": "",
         "kind": 0,
         "name": ""
     },
     "time": 0,
     "userId": 0,
     "userUrl": ""
 }
 */
@end

NS_ASSUME_NONNULL_END
