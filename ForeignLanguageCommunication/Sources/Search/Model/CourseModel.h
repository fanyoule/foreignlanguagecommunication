//
//  CourseModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/7.
//

#import <Foundation/Foundation.h>
#import "CoachTeachingModel.h"
#import "CourseCrowdModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CourseModel : NSObject

@property (nonatomic) CGFloat height;
//MARK: ----------
@property (nonatomic, strong) NSArray <CourseCrowdModel *> *applyCrowd;
@property (nonatomic, copy) NSString *applyCrowd1;
@property (nonatomic, strong) NSArray *coursePic;

@property (nonatomic) NSInteger ID;

@property (nonatomic, copy) NSString *name;
@property (nonatomic) NSInteger orders;
@property (nonatomic) NSInteger price;
@property (nonatomic, strong) CoachTeachingModel *teachingContent;
@property (nonatomic) NSInteger time;
@property (nonatomic) NSInteger userId;
/*
 applyCrowd =                     (
                             {
         createTime = "2020-11-30 16:09:13";
         id = 9;
         kind = 2;
         name = "\U521d\U4e2d\U751f";
     },
                             {
         createTime = "2020-11-30 16:09:20";
         id = 10;
         kind = 2;
         name = "\U9ad8\U4e2d\U751f";
     }
 );
 applyCrowd1 = "9,10";
 coursePic =                     (
                             {
         courseId = 38;
         id = 141;
         url = "http://pic.sxsyingyu.com/xr416ldbht5783wj3fu3.jpg";
     }
 );
 id = 38;
 name = "\U3014\U671f\U672b\U51b2\U523a\U3015\U82f1\U8bed\U53e3\U8bed\U4e0a\U8bfe\U51b2\U523a\U73ed";
 orders = 47;
 price = 1;
 teachingContent =                     {
     createTime = "2020-11-30 16:10:11";
     id = 14;
     kind = 1;
     name = "\U65e5\U5e38\U53e3\U8bed";
 };
 time = 45;
 userId = 40;
 */
@end

NS_ASSUME_NONNULL_END
