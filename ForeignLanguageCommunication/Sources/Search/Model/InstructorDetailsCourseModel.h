//
//  InstructorDetailsCourseModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InstructorDetailsCourseModel : NSObject

/** 适应人群*/
@property (nonatomic, strong)NSArray *applyCrowd;
/** 适应人群*/
@property (nonatomic, strong)NSString *applyCrowd1;
/** 课程简介*/
@property (nonatomic, strong)NSString *content;
/** 课程图片*/
@property (nonatomic, strong)NSArray *coursePic;
/** 课程评价*/
@property (nonatomic, strong)NSArray *evaluation;
/** 课程id*/
@property (nonatomic, assign)NSInteger ID;
/** 是否收藏：0取消 1点赞*/
@property (nonatomic, assign)NSInteger isCollect;
/** 课程名称*/
@property (nonatomic, strong)NSString *name;
/** 已售*/
@property (nonatomic, assign)NSInteger orders;
/** 价格*/
@property (nonatomic, assign)NSInteger price;
/** 教练姓名*/
@property (nonatomic, strong)NSString *realName;
/** 教学内容*/
@property (nonatomic, strong)NSDictionary *teachingContent;
/** 课程时间*/
@property (nonatomic, assign)NSInteger time;
/** 用户id*/
@property (nonatomic, assign)NSInteger userId;
/** 教练头像*/
@property (nonatomic, strong)NSString *userUrl;

@end

NS_ASSUME_NONNULL_END
