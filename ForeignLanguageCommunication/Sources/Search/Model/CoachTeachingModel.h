//
//  CoachTeachingModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/26.
//教学内容

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoachTeachingModel : NSObject
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic) NSInteger ID;
@property (nonatomic) NSInteger kind;
@property (nonatomic, copy) NSString *name;
@end

NS_ASSUME_NONNULL_END
