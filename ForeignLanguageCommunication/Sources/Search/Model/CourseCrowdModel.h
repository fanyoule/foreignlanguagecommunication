//
//  CourseCrowdModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/26.
//标签

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourseCrowdModel : NSObject
/** createTime = "2020-11-30 16:09:13";
 id = 9;
 kind = 2;
 name = "\U521d\U4e2d\U751f";*/

@property (nonatomic, copy) NSString *name;
@property (nonatomic) NSInteger ID;
@property (nonatomic) NSInteger kind;
@property (nonatomic, copy) NSString *createTime;
@end

NS_ASSUME_NONNULL_END
