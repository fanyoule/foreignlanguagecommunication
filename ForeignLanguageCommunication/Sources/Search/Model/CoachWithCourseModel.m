//
//  CoachWithCourseModel.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/7.
//

#import "CoachWithCourseModel.h"

@implementation CoachWithCourseModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{@"course":[CourseModel class]};
}
@end
