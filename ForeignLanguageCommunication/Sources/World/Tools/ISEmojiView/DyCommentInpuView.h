//
//  DyCommentInpuView.h
//  VoiceLive
//
//  Created by mac on 2020/12/2.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//功能 输入文字 emoji 选择图片
//out  展示用的小图片 上传进度
//in  上传图片  上传评论内容
//receive  type 一级评论 二级评论

#import <UIKit/UIKit.h>
#import "ISEmojiView.h"

@protocol DyCommentInputViewDelegate <NSObject>
- (void)commentInputViewDidChangeFrame;
- (void)commentInputViewBeganEdit;
- (void)sendMsg:(NSString *)msg;
@end

typedef void(^selectImageBlock)(UIImage * _Nullable image, NSURL * _Nullable url);

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, DyCommentInpuViewType) {
    /**可以选择图片*/
    DyCommentInpuViewTypeImage,
    /**不能选择图片*/
    DyCommentInpuViewTypeEmojiOnly,
};

@interface DyCommentInpuView : UIView
@property (nonatomic, strong) YYTextView *textView;
@property (nonatomic) BOOL editStatus;
@property (nonatomic) DyCommentInpuViewType type;
@property (nonatomic, weak) id <DyCommentInputViewDelegate> delegate;
@property (nonatomic, copy) NSString *placeHolder;
- (void)beginEdit;
@end

NS_ASSUME_NONNULL_END
