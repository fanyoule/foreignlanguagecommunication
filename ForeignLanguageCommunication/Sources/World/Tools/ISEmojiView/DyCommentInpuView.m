//
//  DyCommentInpuView.m
//  VoiceLive
//
//  Created by mac on 2020/12/2.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//  textview  + emojiButton  + imageButton
//
//change type time  publish over   text == @""  && end edit


#import "DyCommentInpuView.h"


@interface DyCommentInpuView ()<ISEmojiViewDelegate, YYTextViewDelegate>

@property (nonatomic, strong) UIButton *emojiBtn;
@property (nonatomic, strong) UIButton *sendBtn;
@property (nonatomic, strong) HXPhotoManager *photoManager;
@property (nonatomic) BOOL isOriginal;
@property (nonatomic, strong) HXPhotoModel *currentModel;

@property (nonatomic, strong) ISEmojiView *isEmojiView;

@property (nonatomic) BOOL showEmoji;
@property (nonatomic) CGRect originalFrame;
@end

@implementation DyCommentInpuView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithHexString:@"#1D1D1D"];
        _showEmoji = NO;
        [self setupSubView];
        [self addNotice];
        self.originalFrame = frame;
    }
    return self;
}

- (void)setupSubView {
    
    self.size = CGSizeMake(KSW, 49);
    [self addSubview:self.sendBtn];
    [self addSubview:self.textView];
    [self addSubview:self.emojiBtn];
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(44);
        make.right.mas_offset(-0.1);
        make.centerY.mas_offset(0);
    }];
    
    [self.emojiBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(18);
        make.right.mas_equalTo(self.sendBtn.mas_left).mas_offset(-10);
        make.centerY.mas_offset(0);
    }];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.right.mas_equalTo(self.emojiBtn.mas_left).mas_offset(-8);
        make.height.mas_equalTo(32);
        make.centerY.mas_offset(0);
    }];
    
    self.isEmojiView = [[ISEmojiView alloc] initWithFrame:CGRectMake(0, 0, KSW, 216+TabbarSafeMargin+39)];
    self.isEmojiView.delegate = self;
}
- (void)setPlaceHolder:(NSString *)placeHolder {
    _placeHolder = placeHolder;
    
    self.textView.placeholderText = placeHolder;
}
- (YYTextView *)textView {
    if (!_textView) {
        _textView = YYTextView.new;
        _textView.placeholderText = @"有爱评论，说点儿好听的~";
        _textView.placeholderTextColor = [UIColor colorWithWhite:1 alpha:0.8];;
        _textView.placeholderFont = kFont_Regular(14);
        _textView.font = kFont_Regular(14);
        _textView.textColor = UIColor.whiteColor;
        _textView.layer.cornerRadius = 16;
        _textView.backgroundColor = [UIColor colorWithHexString:@"#1D1D1D"];
        _textView.returnKeyType = UIReturnKeySend;
        _textView.delegate = self;
        
        _textView.contentInset = UIEdgeInsetsMake(5, 5, 5, 5);
        _textView.textContainerInset = UIEdgeInsetsMake(5, 6, 5, 6);
        _textView.showsHorizontalScrollIndicator = NO;
        
    }
    return _textView;
}

- (UIButton *)emojiBtn {
    if (!_emojiBtn) {
        _emojiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_emojiBtn setImage:kImage(@"icon_comment_emoji") forState:UIControlStateNormal];
        [_emojiBtn setImage:kImage(@"icon_keyboard") forState:UIControlStateSelected];
        [_emojiBtn addTarget:self action:@selector(clickToShowEmoji:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _emojiBtn;
}
- (UIButton *)sendBtn {
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_sendBtn setImage:kImage(@"icon_comment_emoji") forState:UIControlStateNormal];
        [_sendBtn addTarget:self action:@selector(sendComment) forControlEvents:UIControlEventTouchUpInside];
        [_sendBtn setTitle:@"发送" forState:UIControlStateNormal];
        [_sendBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        _sendBtn.titleLabel.font = kFont(15);
        [_sendBtn setBackgroundColor:UIColor.clearColor];
    }
    return _sendBtn;
}


- (HXPhotoManager *)photoManager {
    if (!_photoManager) {
        _photoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        
        _photoManager.configuration.type = HXConfigurationTypeWXChat;
        _photoManager.type = HXPhotoManagerSelectedTypePhoto;
        //        _photoManager.configuration.selectTogether = NO;
        _photoManager.configuration.maxNum = 1;
        _photoManager.configuration.showOriginalBytes = YES;
        _photoManager.configuration.showOriginalBytesLoading = YES;
        _photoManager.configuration.requestImageAfterFinishingSelection = YES;
        
    }
    return _photoManager;
}
- (void)setEditStatus:(BOOL)editStatus {
    _editStatus = editStatus;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentInputViewBeganEdit)]) {
        [self.delegate commentInputViewBeganEdit];
    }
}

- (void)clickToShowEmoji:(UIButton *)sender {
//    if (!self.editStatus) {
//        [self beginEdit];
//        sender.selected = NO;
//    }
    //切换emoji键盘和系统键盘
    //直接点击  emoji键盘 点击 切换
    // 收起键盘时监听 当前收起的是emoji键盘 再点击其他 应该是系统键盘 点击自己 是emoji
    //选中状态下 说明当前是emoji
//    if (_showEmoji) {
//        self.textView.inputView = nil;
//        [self.textView reloadInputViews];
//    } else {
//        self.textView.inputView = [self createEmojiBoard];
//        [self.textView reloadInputViews];
//    }
//    _showEmoji = !_showEmoji;
    
    if (sender.isSelected) {
        self.textView.inputView = nil;
        [self.textView reloadInputViews];
    }else {
        self.textView.inputView = self.isEmojiView; //[self createEmojiBoard];
        [self.textView reloadInputViews];
    }
    sender.selected = !sender.isSelected;
    [self.textView becomeFirstResponder];
}

- (ISEmojiView *)createEmojiBoard {
    ISEmojiView *emojiView = [[ISEmojiView alloc] initWithFrame:CGRectMake(0, 0, KSW, 216+TabbarSafeMargin+39)];
    emojiView.delegate = self;
    return emojiView;
}

- (void)addNotice {
    //监听键盘
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)keyboardDidShow:(NSNotification *)notice {
    self.editStatus = YES;
}

- (void)keyboardWillShow:(NSNotification *)notice {
    NSLog(@"will show");
    
    NSDictionary *dic =  notice.userInfo;
    NSLog(@"%@", dic[UIKeyboardFrameEndUserInfoKey]);
    CGRect rect = [dic[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat y = rect.origin.y;
    if (y>self.originalFrame.origin.y) {
        return;
    }
    [UIView animateWithDuration:0.25 animations:^{
        self.y = y-self.height;
    }];
}

- (void)keyboardWillHide:(NSNotification *) notice {
    NSLog(@"will hide");
    [UIView animateWithDuration:0.25 animations:^{
        self.frame = self.originalFrame;
    } completion:nil];
}
- (void)keyboardDidHide:(NSNotification *) notice {
    NSLog(@"did hide");
    self.editStatus = NO;
    
    self.textView.inputView = nil;
    [self.textView reloadInputViews];
    self.emojiBtn.selected = NO;
}
//MARK: -----发送
- (void)sendComment {
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendMsg:)]) {
        [self.delegate sendMsg:self.textView.text];
    }
}

//MARK: --------------textView delegate
- (BOOL)textView:(YYTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        
        //send comment
        if (textView.hasText || self.currentModel) {
            [self sendComment];
        }
//        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(YYTextView *)textView {
    return YES;
}
- (void)textViewDidEndEditing:(YYTextView *)textView {
    if (textView.text.length > 0) {
        return;
    }

    self.type = DyCommentInpuViewTypeImage;
}

//MARK: emoji delegate
- (void)emojiView:(ISEmojiView *)emojiView didSelectEmoji:(NSString *)emoji {
    
    //选择了emoji
    NSRange range = self.textView.selectedRange;
    NSMutableString *str = [NSMutableString stringWithString:self.textView.text];
    [str insertString:emoji atIndex:self.textView.selectedRange.location];
    
    self.textView.text = str;
    self.textView.selectedRange = NSMakeRange(range.location + emoji.length, 0);
}

- (void)emojiView:(ISEmojiView *)emojiView didPressDeleteButton:(UIButton *)deletebutton {
    //向前删除一个字符
    if (self.textView.text.length > 0) {
        if (self.textView.selectedRange.location == 0) {
            return;
        }
        NSRange lastRange = self.textView.selectedRange;
        
        NSRange textR = [self.textView.text rangeOfComposedCharacterSequenceAtIndex:lastRange.location-1];
        self.textView.text=[self.textView.text stringByReplacingCharactersInRange:textR withString:@""];
        self.textView.selectedRange = NSMakeRange(textR.location, 0);
    }
}

/**清理所选图片*/
- (void)clearImage {
    self.currentModel = nil;
    [self.photoManager cancelBeforeSelectedList];
    [self.photoManager clearSelectedList];
}
- (void)beginEdit {
    [self.textView becomeFirstResponder];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentInputViewDidChangeFrame)]) {
        [self.delegate commentInputViewDidChangeFrame];
    }
}

@end
