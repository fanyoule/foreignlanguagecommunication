//
//  EmojModel.h
//  VoiceLive
//
//  Created by mac on 2020/11/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmojModel : NSObject

@property (nonatomic, strong) NSArray *People;
@property (nonatomic, strong) NSArray *Places;
@property (nonatomic, strong) NSArray *Objects;
@property (nonatomic, strong) NSArray *Nature;
@end

NS_ASSUME_NONNULL_END
