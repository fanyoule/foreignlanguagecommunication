//
//  CommentTableHeader.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import "CommentTableHeader.h"

@implementation CommentTableHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = UIColor.blackColor;
    
    self.icon.layer.cornerRadius = 20;
    self.icon.layer.masksToBounds = YES;
    
    [self.content sizeToFit];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
    [self addGestureRecognizer:tap];
}
- (void)click {
    if ([self.delegate respondsToSelector:@selector(tableViewHeaderClick:)]) {
        [self.delegate tableViewHeaderClick:self.section];
    }
}

- (void)setModel:(CommentModel *)model {
    _model = model;
    
    [self.icon sd_setImageWithURL:[NSURL URLWithString:model.avatarUrl] placeholderImage:kImage(@"测试头像")];
    self.nickName.text = model.nickname;
    self.timeLab.text = model.createTime;
    self.content.text = model.comment;
}
@end
