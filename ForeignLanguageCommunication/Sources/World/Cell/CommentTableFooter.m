//
//  CommentTableFooter.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import "CommentTableFooter.h"

@interface CommentTableFooter ()
@property (nonatomic, strong) UILabel *lab;
@end

@implementation CommentTableFooter

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = UIColor.blackColor;
        [self setUpUI];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
        [self addGestureRecognizer:tap];
    }
    return self;
}
- (void)click {
    if ([self.delegate respondsToSelector:@selector(tableViewFooterClick:)]) {
        [self.delegate tableViewFooterClick:self.section];
    }
}
- (void)setUpUI {
    UILabel *lab = [[UILabel alloc] init];
    lab.font = kFont(15);
    lab.textColor = UIColor.whiteColor;
    [self.contentView addSubview:lab];
    self.lab = lab;
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(80 + 30));
        make.centerY.equalTo(self.mas_centerY);
    }];
}
- (void)setOpen:(BOOL)open {
    _open = open;
    
    if (open) {
        self.lab.text = @"收起▲";
    } else {
        self.lab.text = @"查看更多▼";
    }
}
@end
