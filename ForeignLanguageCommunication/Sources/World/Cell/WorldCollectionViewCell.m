//
//  WorldCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "WorldCollectionViewCell.h"

@interface WorldCollectionViewCell ()
/**展示图*/
@property (nonatomic, strong)YYAnimatedImageView *showFigureImageV;
/**内容背景*/
@property (nonatomic, strong)UIView *backView;
/**内容*/
@property (nonatomic, strong)UILabel *contentLabel;
/**头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/**名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/**阅读*/
@property (nonatomic, strong)UILabel *readingLabel;
/**阅读次数*/
@property (nonatomic, strong)UILabel *readingNumberLabel;

@end

@implementation WorldCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
        
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.showFigureImageV];
    self.showFigureImageV.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.backView];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.readingLabel];
    
    [self.showFigureImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.contentView);
        make.height.equalTo(@(self.width));
    }];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.showFigureImageV.mas_bottom);
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@75);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).offset(7);
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@23);
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.contentLabel.mas_bottom).offset(9);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.portraitImageV.mas_right).offset(5.5);
        make.centerY.equalTo(self.portraitImageV);
    }];
    [self.readingLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-5.5);
        make.centerY.equalTo(self.portraitImageV);
    }];
    
}
- (void)setModel:(WorldListModel *)model {
    _model = model;
    
    if (model.type == 1) {
        NSArray *arr = [model.picUrl componentsSeparatedByString:@","];
        if (arr.count > 0) {
            NSString *url = [NSString stringWithFormat:@"%@?imageView2/2/w/400/h/200",arr[0]];
            [self.showFigureImageV sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:kImage(@"icon_photo")];
        } else {
            self.showFigureImageV.image = kImage(@"icon_photo");
        }
    } else {
        NSString *url;
        if (model.videoCoverUrl.length>0) {
            url = [NSString stringWithFormat:@"%@?imageView2/2/w/400/h/200",model.videoCoverUrl];
        }else{
            NSArray *arr = [model.picUrl componentsSeparatedByString:@","];
            
            url = [NSString stringWithFormat:@"%@?imageView2/2/w/400/h/200",arr[0]];
        }
        
         
        [self.showFigureImageV sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:kImage(@"icon_photo")];
    }
    
    self.contentLabel.text = model.content;
    NSString *avatarUrl = [NSString stringWithFormat:@"%@?imageView2/2/w/400/h/200",model.avatarUrl];
    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:avatarUrl] placeholderImage:kImage(@"测试头像")];
    self.nameLabel.text = model.nickname;
    self.readingLabel.text = [NSString stringWithFormat:@"阅读%ld次", model.browseNum];
}

- (YYAnimatedImageView *)showFigureImageV{
    if (!_showFigureImageV) {
        _showFigureImageV = [YYAnimatedImageView new];
        _showFigureImageV.image = [UIImage imageNamed:@"icon_photo"];
    }
    return _showFigureImageV;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"美莱双眼皮/内眼角，无辜小鹿眼，睁眼迷人双...";
        _contentLabel.textColor = RGBA(24, 24, 24, 1);
        _contentLabel.font = kFont_Medium(13);
        _contentLabel.textAlignment = 0;
        _contentLabel.numberOfLines = 2;
    }
    return _contentLabel;
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像"];
        _portraitImageV.layer.cornerRadius = 23/2;
        _portraitImageV.layer.masksToBounds = YES;
    }
    return _portraitImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"名字";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Medium(12);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UILabel *)readingLabel{
    if (!_readingLabel) {
        _readingLabel = [[UILabel alloc]init];
        _readingLabel.text = @"阅读223次";
        _readingLabel.textColor = RGBA(153, 153, 153, 1);
        _readingLabel.font = kFont_Medium(10);
        _readingLabel.textAlignment = 0;
    }
    return _readingLabel;
}

- (UILabel *)readingNumberLabel{
    if (!_readingNumberLabel) {
        _readingNumberLabel = [[UILabel alloc]init];
    }
    return _readingNumberLabel;
}

@end
