//
//  ReplayTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import "ReplayTableViewCell.h"

@implementation ReplayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.authorLab.layer.cornerRadius = 4;
    self.authorLab.layer.masksToBounds = YES;
    
    self.icon.layer.cornerRadius = 12.5;
    self.icon.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ReplayModel *)model {
    _model = model;
    
    self.authorLab.hidden = YES;
    [self.icon sd_setImageWithURL:[NSURL URLWithString:model.uidAvatarUrl] placeholderImage:kImage(@"测试头像")];
    [self.nickName oneStr:model.uidNickname twoStr:model.touidNickname];
    self.timeLab.text = model.createDate;
    self.content.text = model.replyComment;
}
- (void)ss:(NSString *)string {
    // 1.创建一个富文本
    NSMutableAttributedString *attri =  [[NSMutableAttributedString alloc] initWithString:string];
    NSInteger len = string.length - 1;
    // 修改富文本中的不同文字的样式
    [attri addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, len)];
    [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, len)];

    // 2.添加表情图片
    NSTextAttachment *attch = [[NSTextAttachment alloc] init];
    // 表情图片
    attch.image = [UIImage imageNamed:@"pinglun_hf"];
    // 设置图片大小
    attch.bounds = CGRectMake(0, 0, 15, 15);

    // 创建带有图片的富文本
    NSAttributedString *str = [NSAttributedString attributedStringWithAttachment:attch];
    [attri insertAttributedString:str atIndex:3];// 插入某个位置
    // 用label的attributedText属性来使用富文本
    self.nickName.attributedText = attri;
}

@end
