//
//  ReplayTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import <UIKit/UIKit.h>
#import "ReplayModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReplayTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *authorLab;

@property (nonatomic, strong) ReplayModel *model;

@end

NS_ASSUME_NONNULL_END
