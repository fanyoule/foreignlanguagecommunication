//
//  ReportTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/12.
//

#import <UIKit/UIKit.h>
#import "ReportModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;
@property (weak, nonatomic) IBOutlet UIView *line;

@property (nonatomic, strong) ReportModel *model;
@end

NS_ASSUME_NONNULL_END
