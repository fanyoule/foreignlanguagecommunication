//
//  SYButton.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/12.
//

#import "SYButton.h"

@implementation SYButton

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect imageRect = self.imageView.frame;
    imageRect.size = CGSizeMake(30, 30);
    imageRect.origin.x = (self.frame.size.width - 30)/2;
    imageRect.origin.y = 5;
    
    CGRect titleRect = self.titleLabel.frame;
//    titleRect.origin.x = (self.frame.size.width - titleRect.size.width)/2;
//    titleRect.origin.y = 40;
    self.imageView.frame = imageRect;
//    self.titleLabel.frame = titleRect;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(0, 40, self.frame.size.width, 20);
}
@end
