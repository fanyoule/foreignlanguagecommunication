//
//  WorldCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import <UIKit/UIKit.h>
#import "WorldListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface WorldCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) WorldListModel *model;
@end

NS_ASSUME_NONNULL_END
