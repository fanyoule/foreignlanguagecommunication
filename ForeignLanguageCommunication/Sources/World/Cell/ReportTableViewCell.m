//
//  ReportTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/12.
//

#import "ReportTableViewCell.h"

@implementation ReportTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.line.backgroundColor = CellLineColor;
}
- (void)setModel:(ReportModel *)model {
    _model = model;
    
    self.titleLab.text = model.title;
    if (model.isSelect) {
        [self.stateBtn setImage:kImage(@"选中") forState:UIControlStateNormal];
    } else {
        [self.stateBtn setImage:kImage(@"未选中") forState:UIControlStateNormal];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
