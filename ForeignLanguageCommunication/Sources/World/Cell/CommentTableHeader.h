//
//  CommentTableHeader.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CommentTableHeaderDelegate <NSObject>

- (void)tableViewHeaderClick:(NSInteger)section;

@end

@interface CommentTableHeader : UITableViewHeaderFooterView
@property(nonatomic, weak) id<CommentTableHeaderDelegate> delegate;
@property (nonatomic, assign) NSInteger section;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@property (nonatomic, strong) CommentModel *model;

@end

NS_ASSUME_NONNULL_END
