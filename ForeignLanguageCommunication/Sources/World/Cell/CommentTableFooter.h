//
//  CommentTableFooter.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@protocol CommentTableFooterDelegate <NSObject>

- (void)tableViewFooterClick:(NSInteger)section;

@end

@interface CommentTableFooter : UITableViewHeaderFooterView
@property (nonatomic, assign) NSInteger section;
@property(nonatomic, weak) id<CommentTableFooterDelegate> delegate;
@property (nonatomic, assign) BOOL open;
@end

NS_ASSUME_NONNULL_END
