//
//  ReportModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportModel : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic) int type;
@property (nonatomic, assign) BOOL isSelect;
@end

NS_ASSUME_NONNULL_END
