//
//  WorldListModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WorldListModel : NSObject
@property (nonatomic, copy) NSString *avatarUrl;        // 发布的用户头像
@property (nonatomic) long browseNum;                   // 浏览的数量
@property (nonatomic, copy) NSString *content;          // 动态内容
@property (nonatomic, copy) NSString *createTime;       // 发布时间
@property (nonatomic) long Id;                          // 世界动态id
@property (nonatomic, copy) NSString *nickname;         // 发布的用户名
@property (nonatomic) int type;                         // 动态类型：1 文字+图片、2 文字+视频
@property (nonatomic) long userId;                      // 发布的用户ID
@property (nonatomic, copy) NSString *videoCoverUrl;    // 视频封面url
@property (nonatomic, copy) NSString *videoUrl;         // 视频url
@property (nonatomic, copy) NSString *picUrl;           // 图片
@end

NS_ASSUME_NONNULL_END
