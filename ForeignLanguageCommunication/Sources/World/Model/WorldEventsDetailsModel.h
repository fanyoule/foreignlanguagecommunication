//
//  WorldEventsDetailsModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/19.
//  世界动态详情

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WorldEventsDetailsModel : NSObject
/** 发布的用户头像*/
@property (nonatomic, strong)NSString * avatarUrl;
/** 评论的数量*/
@property (nonatomic, assign)NSInteger commentNum ;
/** 动态内容*/
@property (nonatomic, strong)NSString * content;
/** 发布时间*/
@property (nonatomic, strong)NSString * createTime;
/** 是否关注：0未关注 1已关注 2本人*/
@property (nonatomic, assign)NSInteger  isFocus;
/** 是否已经点赞*/
@property (nonatomic)BOOL isLike;
/** 点赞的数量*/
@property (nonatomic, assign)NSInteger likeNum ;
/** 发布的用户名*/
@property (nonatomic, strong)NSString * nickname;
/** 图片url，多张已逗号分隔*/
@property (nonatomic, strong)NSString * picUrl;
/** 动态类型：1 文字+图片、2 文字+视频*/
@property (nonatomic, assign)NSInteger type;
/** 发布的用户ID*/
@property (nonatomic, assign)NSInteger userId;
/** 视频封面url*/
@property (nonatomic, strong)NSString * videoCoverUrl;
/** 视频url*/
@property (nonatomic, strong)NSString * videoUrl;

@end

NS_ASSUME_NONNULL_END
