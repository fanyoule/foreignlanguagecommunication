//
//  CommentModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import <Foundation/Foundation.h>
#import "ReplayModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommentModel : NSObject
@property (nonatomic, copy) NSString *avatarUrl;        // 用户头像
@property (nonatomic, copy) NSString *comment;          // 评论内容
@property (nonatomic, copy) NSString *commentNum;       // 评论数量
@property (nonatomic, copy) NSString *commentPic;       // 评论图片url
@property (nonatomic, copy) NSString *commentPicName;   // 评论图片文件名
@property (nonatomic, copy) NSString *createTime;       // 评论时间
@property (nonatomic, copy) NSString *isAuthor;         // 是否是作者：0不是 1是
@property (nonatomic, copy) NSString *nickname;         // 用户名
@property (nonatomic, copy) NSString *publishId;        // 动态id
@property (nonatomic, copy) NSString *userId;           // 评论用户id
@property (nonatomic, copy) NSString *Id;               // 评论id
@property (nonatomic, strong) NSMutableArray <ReplayModel *>*replays;          // 动态评论回复列表

@property (nonatomic, assign) BOOL open;

@end

NS_ASSUME_NONNULL_END
