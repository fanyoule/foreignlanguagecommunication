//
//  WorldDetailModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WorldDetailModel : NSObject
@property (nonatomic, copy) NSString *avatarUrl;        // 发布的用户头像
@property (nonatomic) long commentNum;                  // 评论的数量
@property (nonatomic, copy) NSString *content;          // 动态内容
@property (nonatomic, copy) NSString *createTime;       // 发布时间
@property (nonatomic) int isFocus;                       // 是否关注：0未关注 1已关注 2本人
@property (nonatomic) BOOL isLike;                       // 是否已经点赞
@property (nonatomic) long likeNum;                     // 点赞的数量
@property (nonatomic, copy) NSString *nickname;         // 发布的用户名
@property (nonatomic, copy) NSString *picUrl;           // 图片url，多张已逗号分隔
@property (nonatomic) int type;                         // 动态类型：1 文字+图片、2 文字+视频
@property (nonatomic) long userId;                      // 发布的用户ID
@property (nonatomic, copy) NSString *videoCoverUrl;    // 视频封面url
@property (nonatomic, copy) NSString *videoUrl;         // 视频url
@property (nonatomic) long Id;                          // 世界动态id




@end

NS_ASSUME_NONNULL_END
