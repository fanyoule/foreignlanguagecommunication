//
//  CommentModel.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import "CommentModel.h"


@implementation CommentModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"replays":[ReplayModel class]};
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"Id": @"id"};
}
@end
