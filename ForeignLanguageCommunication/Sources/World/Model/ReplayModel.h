//
//  ReplayModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReplayModel : NSObject
@property (nonatomic, copy) NSString *Id;               // 回复评论id
@property (nonatomic, copy) NSString *commentsId;           // 评论总id
@property (nonatomic, copy) NSString *createDate;           // 回复时间
@property (nonatomic, copy) NSString *isCommentTouid;       // 被回复用户是否是作者 0：是 1：不是
@property (nonatomic, copy) NSString *isCommentUid;         // 回复用户是否是作者 0：是 1：不是
@property (nonatomic, copy) NSString *replyComment;         // 回复内容
@property (nonatomic, copy) NSString *replyCommentPic;      // 回复图片
@property (nonatomic, copy) NSString *replyCommentPicName;  // 回复图片名字
@property (nonatomic, copy) NSString *touid;                // 被回复用户id
@property (nonatomic, copy) NSString *touidAvatarUrl;       // 被回复用户头像
@property (nonatomic, copy) NSString *touidNickname;        // 被回复用户名
@property (nonatomic, copy) NSString *uid;                  // 回复用户ID
@property (nonatomic, copy) NSString *uidAvatarUrl;         // 回复用户头像
@property (nonatomic, copy) NSString *uidNickname;          // 回复用户名
@end

NS_ASSUME_NONNULL_END
