//
//  CommentViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import "CommentViewController.h"
#import "CommentModel.h"
#import "CommentTableHeader.h"
#import "ReplayTableViewCell.h"
#import "CommentTableFooter.h"
#import "DyCommentInpuView.h"

#define EmojViewH  (49)
@interface CommentViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
CommentTableHeaderDelegate,
CommentTableFooterDelegate,
DyCommentInputViewDelegate
>
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableBottomConstraint;

@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) DyCommentInpuView *commentInputView;

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) UIButton *coverBtn;
@property (nonatomic) int page;

@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *toUserId;
@property (nonatomic, copy) NSString *commentsId;
@property (nonatomic, copy) NSString *publishId;
@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.dataArr = [NSMutableArray array];
    
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColor.clearColor;
    
    self.tableBottomConstraint.constant = 8 + EmojViewH + TabbarSafeMargin;
    [self.view layoutIfNeeded];
    
    self.bgView.layer.cornerRadius = 8;
    self.bgView.layer.masksToBounds = YES;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = UIColor.blackColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.tableView registerNib:[UINib nibWithNibName:[CommentTableHeader toString] bundle:nil] forHeaderFooterViewReuseIdentifier:[CommentTableHeader toString]];
    [self.tableView registerClass:[CommentTableFooter class] forHeaderFooterViewReuseIdentifier:[CommentTableFooter toString]];
    [self.tableView registerNib:[UINib nibWithNibName:[ReplayTableViewCell toString] bundle:nil] forCellReuseIdentifier:[ReplayTableViewCell toString]];
    self.tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(pageDown)];
    self.tableView.mj_footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(pageUp)];
    self.tableView.mj_footer.hidden = YES;
    
    [self getData];
    [self setUpUI];
    
    self.coverBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.coverBtn.frame = CGRectMake(0, 0, KSW, KSH - 300);
    self.coverBtn.backgroundColor = UIColor.clearColor;
    [self.coverBtn addTarget:self action:@selector(hiddenCoverBtn) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:self.coverBtn];
    self.coverBtn.hidden = YES;
    [self resetIds];
    
}
- (void)resetIds {
    self.nickName = @"";
    self.toUserId = @"";
    self.publishId = @"";
    self.commentsId = @"";
    self.commentInputView.placeHolder = @"有爱评论，说点儿好听的~";
}
- (void)hiddenCoverBtn {
    self.coverBtn.hidden = YES;
    if (self.commentInputView.editStatus) {
        [self.commentInputView endEditing:YES];
    }
}
- (void)setUpUI {
    [self.view addSubview:self.bottomView];
    
    self.commentInputView = [[DyCommentInpuView alloc] initWithFrame:CGRectMake(0, ScreenHeight - TabbarSafeMargin - EmojViewH, KSW, EmojViewH)];
    self.commentInputView.type = DyCommentInpuViewTypeEmojiOnly;
    self.commentInputView.delegate = self;
    [self.view addSubview:self.commentInputView];
}
- (void)commentInputViewBeganEdit {
    self.coverBtn.hidden = !self.commentInputView.editStatus;
    if (!self.commentInputView.editStatus) {
        [self resetIds];
    }
}
- (void)pageDown {
    self.page = 1;
    [self getData];
}
- (void)pageUp {
    [self getData];
}
- (void)stopLoading:(BOOL)isNoData {
    if ([self.tableView.mj_header isRefreshing]) {
        [self.tableView.mj_header endRefreshing];
        if (!isNoData) {
            self.tableView.mj_footer.state = MJRefreshStateIdle;
        }
    }
    if ([self.tableView.mj_footer isRefreshing]) {
        if (isNoData) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
    }
}

- (void)getData {
    kWeakSelf(self)
    [RequestManager queryPublishCommentsWithId:self.model.Id pageNum:self.page pageSize:10 withSuccess:^(id  _Nullable response) {
        NSDictionary *data = response[@"data"];
        long total = [data[@"total"] longValue];
        weakself.titleLab.text = [NSString stringWithFormat:@"%ld条评论", total];
        int lastPage = [data[@"lastPage"] intValue];
        NSArray *tempArr = [CommentModel mj_objectArrayWithKeyValuesArray:data[@"list"]];
        if (weakself.page == 1) {
            [weakself.dataArr removeAllObjects];
        }
        [weakself stopLoading:!(weakself.page < lastPage)];
        weakself.page++;
        [weakself.dataArr addObjectsFromArray:tempArr];
        if (weakself.dataArr.count > 0) {
            weakself.tableView.mj_footer.hidden = NO;
        }
        [weakself.tableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself stopLoading:NO];
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    CommentModel *model = self.dataArr[section];
    if (model.open) {
        return model.replays.count;
    } else {
        if (model.replays.count > 3) {
            return 3;
        } else {
            return model.replays.count;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CommentModel *model = self.dataArr[section];
    CGSize size = [model.comment boundingRectWithSize:CGSizeMake(KSW - 80, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:kFont_Medium(14)} context:nil].size;
    return 58 + size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CommentTableHeader *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[CommentTableHeader toString]];
    header.model = self.dataArr[section];
    header.section = section;
    header.delegate = self;
    return header;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    CommentModel *model = self.dataArr[section];
    if (model.replays.count > 3) {
        return 30;
    } else {
        return 0.001;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    CommentModel *model = self.dataArr[section];
    if (model.replays.count > 3) {
        CommentTableFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[CommentTableFooter toString]];
        footer.open = model.open;
        footer.section = section;
        footer.delegate = self;
        return footer;
    } else {
        return [UIView new];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    125
//    return 80;
    CommentModel *model = self.dataArr[indexPath.section];
    ReplayModel *m = model.replays[indexPath.row];
    CGSize size = [m.replyComment boundingRectWithSize:CGSizeMake(KSW - 125, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:kFont_Medium(14)} context:nil].size;
    return 58 + size.height;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReplayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ReplayTableViewCell toString]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CommentModel *model = self.dataArr[indexPath.section];
    cell.model = model.replays[indexPath.row];
    return cell;
}
- (void)tableViewHeaderClick:(NSInteger)section {
    NSInteger  userID =[[UserInfoManager shared]getUserID] ;
    NSArray *arr = @[@"回复"];
    kWeakSelf(self)
    CommentModel *model = self.dataArr[section];
    if (model.userId.integerValue==userID) {
        arr = @[@"回复",@"删除"];
    }
    [self alertView:@"" msg:@"" btnArrTitles:arr showCacel:YES style:UIAlertControllerStyleActionSheet block:^(int index) {
        switch (index) {
            case 0:
            {
                
                [weakself addComment:model.userId name:model.nickname publishId:model.publishId commentsId:model.Id];
            }
                break;
            case 1://删除
            {
                [weakself deletaPublishCommentById:model.Id];
            }
                break;
                
            default:
                break;
        }
      
    }];
}
//删除
-(void)deletaPublishCommentById:(NSString *)Id{
    if (!IS_VALID_STRING(Id)) {
        return;
    }
    kWeakSelf(self)
    [RequestManager deletePublishCommentsById:Id.integerValue withSuccess:^(id  _Nullable response) {
        [weakself pageDown];
        [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshComment object:nil];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
//删除回复
-(void)deletePublishCommentsReplyById:(NSString *)ID{
    if (!IS_VALID_STRING(ID)) {
        return;
    }
    kWeakSelf(self)
    [RequestManager deletePublishCommentsReplyById:ID.integerValue withSuccess:^(id  _Nullable response){
        [weakself pageDown];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
    
}

// 回复
- (void)addComment:(NSString *)userId name:(NSString *)name publishId:(NSString *)publishId commentsId:(NSString *)commentsId {
    self.nickName = name;
    self.publishId = publishId;
    self.toUserId = userId;
    self.commentsId = commentsId;
    self.commentInputView.placeHolder = [NSString stringWithFormat:@"回复%@", self.nickName];
    [self.commentInputView.textView becomeFirstResponder];
}
- (void)sendMsg:(NSString *)msg {
    if (!msg || msg.length <= 0) {
        [self showText:@"输入内容不能为空"];
        return;
    }
    kWeakSelf(self)
    if (self.publishId.length > 0) {
        [RequestManager addPublishCommentsReplyWithTouid:[self.toUserId longValue] uid:[UserInfoManager shared].getUserID commentsId:[self.commentsId longValue] publishId:[self.publishId longValue] replyComment:msg replyCommentPic:@"" withSuccess:^(id  _Nullable response) {
            [weakself pageDown];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    } else {
        int isAuthor = self.model.userId == [UserInfoManager shared].getUserID ? 1 : 0;
        [RequestManager addPublishCommentsWithUserId:[UserInfoManager shared].getUserID isAuthor:isAuthor publishId:self.model.Id comment:msg commentPic:@"" withSuccess:^(id  _Nullable response) {
            [weakself pageDown];
            [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshComment object:nil];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
    [self.commentInputView endEditing:YES];
    self.commentInputView.textView.text = @"";
}

- (void)tableViewFooterClick:(NSInteger)section {
    CommentModel *model = self.dataArr[section];
    model.open = !model.open;
    [self.tableView reloadData];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger userId = [[UserInfoManager shared]getUserID];
    NSArray *arr = @[@"回复"];
    
    CommentModel *model = self.dataArr[indexPath.section];
    ReplayModel *replayModel = model.replays[indexPath.row];
    if (replayModel.uid.integerValue == userId) {
        arr = @[@"回复",@"删除"];
    }
    kWeakSelf(self)
    [self alertView:@"" msg:@"" btnArrTitles:arr showCacel:YES style:UIAlertControllerStyleActionSheet block:^(int index) {
        switch (index) {
            case 0:
            {
                [weakself addComment:replayModel.uid name:replayModel.uidNickname publishId:model.publishId commentsId:replayModel.commentsId];
            }
                break;
            case 1:
            {
                [weakself deletePublishCommentsReplyById:replayModel.Id];
            }
                break;
                
            default:
                break;
        }
       
    }];
}
- (IBAction)back:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight - EmojViewH - TabbarSafeMargin, KSW, EmojViewH + TabbarSafeMargin)];
        _bottomView.backgroundColor = [UIColor colorWithHexString:@"#1D1D1D"];
    }
    return _bottomView;
}

@end
