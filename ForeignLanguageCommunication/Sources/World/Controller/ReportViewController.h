//
//  ReportViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportViewController : BaseViewController
@property (nonatomic) long userId;
@end

NS_ASSUME_NONNULL_END
