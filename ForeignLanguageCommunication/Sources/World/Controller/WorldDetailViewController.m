//
//  WorldDetailViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/12.
//

#import "WorldDetailViewController.h"

#import <AVFoundation/AVFoundation.h>
#import "SYButton.h"
#import "ReportViewController.h"
#import "CommentViewController.h"
#import "WorldDetailModel.h"
#import "MyPersonalInformationVC.h"

@interface WorldDetailViewController ()<SDCycleScrollViewDelegate>
@property (nonatomic, strong) SDCycleScrollView *circleView;

@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *headerImgv;
@property (nonatomic, strong) UIButton *addImgv;
@property (nonatomic, strong) SYButton *commentBtn;
@property (nonatomic, strong) SYButton *zanBtn;
@property (nonatomic, strong) SYButton *reportBtn;
@property (nonatomic, strong) UILabel *desLab;
@property (nonatomic, strong) UILabel *nameLab;

@property (nonatomic, strong) UIView *videoView;
@property (nonatomic, strong) AVPlayer *player;//播放器对象
@property (nonatomic, strong) AVPlayerItem *currentPlayerItem;

@property (nonatomic) BOOL jumpOtherVcStop;

@property (nonatomic, strong) WorldDetailModel *detailModel;
@property (nonatomic, assign) BOOL isShow;
@end

@implementation WorldDetailViewController

- (void)viewDidLoad {
    _isShow = NO;
    _jumpOtherVcStop = NO;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo) name:kPlayVideoNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getData) name:kRefreshComment object:nil];
    [self getData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)getData {
    [self showSVP];
    kWeakSelf(self);
    long Id = self.model ? self.model.Id : self.worksID;
    [RequestManager getPublishsById:[UserInfoManager shared].getUserID Id:Id withSuccess:^(id  _Nullable response) {
        [weakself dissSVP];
        weakself.detailModel = [WorldDetailModel mj_objectWithKeyValues:response[@"data"]];
        [weakself showVideoOrImages];
    } withFail:^(NSError * _Nullable error) {
        [weakself dissSVP];
        [weakself showText:error.localizedDescription];
    }];
}
- (void)playVideo {
    if (_jumpOtherVcStop) {
        if (self.player) {
            [self play];
        }
    }
}
- (void)showVideoOrImages {
    if (self.detailModel.type == 2) {
        self.videoView.hidden = NO;
        self.circleView.hidden = YES;
    } else {
        self.videoView.hidden = YES;
        self.circleView.hidden = NO;
    }
    if (!_isShow) {
        _isShow = YES;
        if (self.detailModel.type == 2) {
            [self addVideoPlayer:self.detailModel.videoUrl];
        } else {
            NSArray *arr = [self.detailModel.picUrl componentsSeparatedByString:@","];
            self.circleView.localizationImageNamesGroup = arr;
        }
    }
    [self.headerImgv sd_setBackgroundImageWithURL:[NSURL URLWithString:self.detailModel.avatarUrl] forState:UIControlStateNormal];
    [self.zanBtn setTitle:[NSString longStrong:self.detailModel.likeNum] forState:UIControlStateNormal];
    [self.commentBtn setTitle:[NSString longStrong:self.detailModel.commentNum] forState:UIControlStateNormal];
    self.nameLab.text = [NSString stringWithFormat:@"@%@", self.detailModel.nickname];
    self.desLab.text = self.detailModel.content;
    self.addImgv.hidden = self.detailModel.isFocus != 0;
    self.zanBtn.selected = self.detailModel.isLike;
}
- (void)setUpUI {
    [self.view addSubview:self.videoView];
    [self.view addSubview:self.circleView];
//    if (self.model.type == 2) {
//        [self.view addSubview:self.videoView];
//    } else {
//        [self.view addSubview:self.circleView];
//    }
    // 返回按钮
    [self.view addSubview:self.backButton];
    
    [self.view addSubview:self.reportBtn];
    [self.reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.bottom.equalTo(@(-TabbarSafeMargin - 60));
        make.width.height.equalTo(@60);
    }];
    
    [self.view addSubview:self.zanBtn];
    [self.zanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.bottom.equalTo(self.reportBtn.mas_top).offset(-20);
        make.width.height.equalTo(@60);
    }];
    
    [self.view addSubview:self.commentBtn];
    [self.commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.bottom.equalTo(self.zanBtn.mas_top).offset(-20);
        make.width.height.equalTo(@60);
    }];
    
    [self.view addSubview:self.headerImgv];
    [self.headerImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.bottom.equalTo(self.commentBtn.mas_top).offset(-30);
        make.width.height.equalTo(@60);
    }];
    
    [self.view addSubview:self.addImgv];
    [self.addImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.headerImgv.mas_centerX);
        make.centerY.equalTo(self.headerImgv.mas_bottom);
        make.width.height.equalTo(@20);
    }];
    
    [self.view addSubview:self.desLab];
    [self.desLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.bottom.equalTo(@(-TabbarSafeMargin - 40));
        make.width.equalTo(@(KSW * 0.7));
    }];
    
    [self.view addSubview:self.nameLab];
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.bottom.equalTo(self.desLab.mas_top).offset(-5);
        make.width.equalTo(@(KSW * 0.7));
    }];
}

- (void)addVideoPlayer:(NSString *)url {
    NSString *webVideoPath = url;
//    NSString *webVideoPath = @"http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";
    NSURL *webVideoUrl = [NSURL URLWithString:webVideoPath];
    AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithURL:webVideoUrl];
    self.currentPlayerItem = playerItem;
    self.player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
    
    AVPlayerLayer *avLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    avLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    avLayer.frame = self.videoView.bounds;
    [self.videoView.layer addSublayer:avLayer];
    [self play];
    self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)playerItemDidReachEnd {
    [self.currentPlayerItem seekToTime:kCMTimeZero];
}
- (void)play {
    [self.player play];
}
- (void)pause {
    [self.player pause];
}
- (UIButton *)backButton {
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.frame = CGRectMake(20, SNavBarHeight - 44, 30, 44);
        UIImage *bImage = [[[UIImage imageNamed:@"icon_nav_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 0)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [_backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        _backButton.backgroundColor = [UIColor clearColor];
        _backButton.tintColor = UIColor.whiteColor;
        [_backButton setImage:bImage forState:UIControlStateNormal];
    }
    return _backButton;
}
- (void)back {
    if (self.player) {
        [self pause];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)tap {
    if (self.player.timeControlStatus == AVPlayerTimeControlStatusPlaying) {
        [self pause];
    } else {
        [self play];
    }
}
// 点击头像
- (void)headerClick {
    MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc] init];
    vc.ID = self.detailModel.userId;
    [self.navigationController pushViewController:vc animated:YES];
}
// 举报
- (void)reportBtnClick {
    if (self.player && self.player.timeControlStatus == AVPlayerTimeControlStatusPlaying) {
        self.jumpOtherVcStop = YES;
        [self pause];
    } else {
        self.jumpOtherVcStop = NO;
    }
    ReportViewController *vc = [[ReportViewController alloc] init];
    vc.userId = self.detailModel.userId;
    [self.navigationController pushViewController:vc animated:YES];
}
// 点赞
- (void)zanBtnClick {
    int status = self.detailModel.isLike ? 0 : 1;
    kWeakSelf(self)
    [RequestManager postPublishsLikeWithId:self.detailModel.Id likedPostId:[UserInfoManager shared].getUserID status:status withSuccess:^(id  _Nullable response) {
        [weakself getData];
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"***");
    }];
}
// 关注
- (void)addBtnClick {
    kWeakSelf(self)
    [RequestManager addFollowsMappingWithId:[UserInfoManager shared].getUserID followUserId:self.detailModel.userId withSuccess:^(id  _Nullable response) {
        weakself.addImgv.hidden = YES;
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
// 评论
- (void)commentBtnClick {
    CommentViewController *vc = [[CommentViewController alloc] init];
    if (self.model) {
        vc.model = self.model;
    } else {
        vc.model = [[WorldListModel alloc] init];
        vc.model.Id = self.detailModel.Id;
    }
    
    float version = [UIDevice currentDevice].systemVersion.floatValue;
    if (version < 8.0) { // iOS 7 实现的方式略有不同(设置self)
        self.modalPresentationStyle = UIModalPresentationCurrentContext;
        // iOS8以下必须使用rootViewController,否则背景会变黑
        [self.view.window.rootViewController presentViewController:vc animated:YES completion:^{
        }];
    } else { // iOS 8 以上实现（设置vc）
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext|UIModalPresentationFullScreen;
        //如果控制器属于navigationcontroller或者tababrControlelr子控制器,不使用UIModalPresentationFullScreen 的话, bar 会盖住你的modal出来的控制器
        [self presentViewController:vc animated:YES completion:^{
        // 也可以在这里做一些完成modal后需要做得事情
        }];
    }
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
        //轮播图滚动到第几个
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    //点击了第几个轮播图
}
- (UIView *)videoView {
    if (!_videoView) {
        _videoView = [[UIView alloc] initWithFrame:self.view.bounds];
        _videoView.backgroundColor = UIColor.blackColor;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
        [_videoView addGestureRecognizer:tap];
    }
    return _videoView;
}
- (SDCycleScrollView *)circleView {
    if (!_circleView) {
        _circleView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, KSW, ScreenHeight) delegate:self placeholderImage:[UIImage imageNamed:@""]];
        _circleView.clipsToBounds = YES;
        _circleView.backgroundColor = UIColor.blackColor;
        _circleView.autoScroll = NO;
        _circleView.showPageControl = YES;
        _circleView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
        _circleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFit;
        _circleView.pageControlBottomOffset = TabbarSafeMargin;
    }
    return _circleView;
}
- (SYButton *)reportBtn {
    if (!_reportBtn) {
        _reportBtn = [SYButton buttonWithType:UIButtonTypeCustom];
//        [_reportBtn setTitle:@"0" forState:UIControlStateNormal];
        _reportBtn.titleLabel.font = kFont(12);
        [_reportBtn addTarget:self action:@selector(reportBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_reportBtn setImage:kImage(@"report_sj") forState:UIControlStateNormal];
    }
    return _reportBtn;
}
- (SYButton *)zanBtn {
    if (!_zanBtn) {
        _zanBtn = [SYButton buttonWithType:UIButtonTypeCustom];
        [_zanBtn setTitle:@"0" forState:UIControlStateNormal];
        [_zanBtn addTarget:self action:@selector(zanBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _zanBtn.titleLabel.font = kFont(12);
        [_zanBtn setImage:kImage(@"zan_sj") forState:UIControlStateNormal];
        [_zanBtn setImage:kImage(@"zan_sj_sel") forState:UIControlStateSelected];
    }
    return _zanBtn;
}
- (SYButton *)commentBtn {
    if (!_commentBtn) {
        _commentBtn = [SYButton buttonWithType:UIButtonTypeCustom];
        [_commentBtn setTitle:@"0" forState:UIControlStateNormal];
        _commentBtn.titleLabel.font = kFont(12);
        [_commentBtn addTarget:self action:@selector(commentBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_commentBtn setImage:kImage(@"comment_sj") forState:UIControlStateNormal];
    }
    return _commentBtn;
}
- (UIButton *)headerImgv {
    if (!_headerImgv) {
        _headerImgv = [UIButton buttonWithType:UIButtonTypeCustom];
        [_headerImgv setBackgroundImage:kImage(@"消息-好友") forState:UIControlStateNormal];
        _headerImgv.layer.cornerRadius = 5;
        _headerImgv.layer.masksToBounds = YES;
        [_headerImgv addTarget:self action:@selector(headerClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _headerImgv;
}
- (UIButton *)addImgv {
    if (!_addImgv) {
        _addImgv = [[UIButton alloc] init];
        [_addImgv setImage:kImage(@"add_sj") forState:UIControlStateNormal];
        [_addImgv addTarget:self action:@selector(addBtnClick) forControlEvents:UIControlEventTouchUpInside];
//        _addImgv.image = kImage(@"add_sj");
    }
    return _addImgv;
}
- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.font = kFont(14);
        _nameLab.textColor = RGBA(243, 243, 243, 1);
        _nameLab.text = @"@五月天店铺";
    }
    return _nameLab;
}
- (UILabel *)desLab {
    if (!_desLab) {
        _desLab = [[UILabel alloc] init];
        _desLab.font = kFont(14);
        _desLab.textColor = RGBA(243, 243, 243, 1);
        _desLab.numberOfLines = 0;
        _desLab.text = @"巴黎圣母绿茶小吊带裙019新款吊带露背 连衣裙，长裙超级婊，";
    }
    return _desLab;
}
@end
