//
//  CommentViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/13.
//

#import "BaseViewController.h"
#import "WorldListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CommentViewController : BaseViewController
@property (nonatomic, strong) WorldListModel *model;
@end

NS_ASSUME_NONNULL_END
