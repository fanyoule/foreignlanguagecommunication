//
//  WorldViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/16.
//  世界

#import "WorldViewController.h"
#import "WorldCollectionViewCell.h"
#import "DynamicPublishViewController.h"
#import "WorldDetailViewController.h"

@interface WorldViewController ()<UICollectionViewDelegate,UICollectionViewDataSource> {
    CGFloat itemW;
}
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic) int page;
@end

@implementation WorldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self creatUI];
    [self getData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pageDown) name:kRefreshWorldList object:nil];
}
-(void)creatUI{
    itemW = (KSW - 30 - 15)/2;
    self.dataArr = [NSMutableArray array];
    self.page = 1;
    
    UIButton *dynamicBtn = [[UIButton alloc]init];
    dynamicBtn.backgroundColor =RGBA(0, 153, 255, 1);
    [dynamicBtn setTitle:@"发动态" forState:(UIControlStateNormal)];
    [dynamicBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
    dynamicBtn.titleLabel.font = kFont_Bold(15);
    dynamicBtn.layer.cornerRadius = (30/2);
    [dynamicBtn addTarget:self action:@selector(withTheDynamic) forControlEvents:(UIControlEventTouchDown)];
    [self.navView addSubview:dynamicBtn];
    [dynamicBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_offset(-12.5);
        make.bottom.mas_offset(-10);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(30);
    }];
    
    UILabel *worldLabel = [[UILabel alloc]init];
    worldLabel.text = @"世界";
    worldLabel.textColor = RGBA(24, 24, 24, 1);
    worldLabel.font = kFont_Bold(23);
    worldLabel.textAlignment = 0;
    [self.navView addSubview:worldLabel];
    [worldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.bottom.mas_offset(-10);
    }];
    
    
    [self.view addSubview:self.mainCollectionView];
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.equalTo(@0);
        make.bottom.equalTo(self.view).offset(-(49 + TabbarSafeMargin));
        make.width.equalTo(self.view);
    }];
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.mainCollectionView registerClass:[WorldCollectionViewCell class] forCellWithReuseIdentifier:@"WorldCollectionViewCell"];
    self.mainCollectionView.mj_header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(pageDown)];
    self.mainCollectionView.mj_footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(pageUp)];
    
    [self.mainCollectionView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.mainCollectionView);
    }];
    
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)pageDown {
    self.page = 1;
    [self getData];
}
- (void)pageUp {
    [self getData];
}
- (void)stopLoading:(BOOL)isNoData {
    [self dissSVP];
    if ([self.mainCollectionView.mj_header isRefreshing]) {
        [self.mainCollectionView.mj_header endRefreshing];
        if (!isNoData) {
            self.mainCollectionView.mj_footer.state = MJRefreshStateIdle;
        }
    }
    if ([self.mainCollectionView.mj_footer isRefreshing]) {
        if (isNoData) {
            [self.mainCollectionView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.mainCollectionView.mj_footer endRefreshing];
        }
    }
}
- (void)getData {
    [self showSVP];
    kWeakSelf(self)
    [RequestManager getPublishsListWithPage:self.page pageSize:10 withSuccess:^(id  _Nullable response) {
        NSDictionary *data = response[@"data"];
        int lastPage = [data[@"lastPage"] intValue];
        NSArray *tempArr = [WorldListModel mj_objectArrayWithKeyValuesArray:data[@"list"]];
        if (weakself.page == 1) {
            [weakself.dataArr removeAllObjects];
        }
        [weakself stopLoading:!(weakself.page < lastPage)];
        weakself.page++;
        [self.dataArr addObjectsFromArray:tempArr];
        weakself.nullView.hidden = weakself.dataArr.count;
        [weakself.mainCollectionView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself stopLoading:NO];
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WorldCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WorldCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
    cell.model = self.dataArr[indexPath.item];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(itemW, itemW + 75);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 15, 10, 15);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (![UserInfoManager shared].isLogin) {
        [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            if (index == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
            }
        }];
        return;
    }
    WorldDetailViewController *vc = [[WorldDetailViewController alloc] init];

//    vc.isVideo = indexPath.item % 2 == 0;

    vc.model = self.dataArr[indexPath.item];

    [self.navigationController pushViewController:vc animated:YES];
}

/**发动态*/
-(void)withTheDynamic{
    NSLog(@"点击了发动态");
    if (![UserInfoManager shared].isLogin) {
        [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            if (index == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
            }
        }];
        return;
    }
    DynamicPublishViewController *publishVC = [[DynamicPublishViewController alloc] init];
    [self.navigationController pushViewController:publishVC animated:YES];
}



@end
