//
//  ReportViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/12.
//

#import "ReportViewController.h"
#import "ReportTableViewCell.h"

#define ReporCellH 48
@interface ReportViewController ()<HXPhotoViewDelegate,HXPhotoViewControllerDelegate,HXCustomNavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) YYTextView *yyTextView;
/**图片管理器*/
@property (nonatomic, strong) HXPhotoManager *photoManager;
/**选择图片View*/
@property (nonatomic, strong) HXPhotoView *photoView;
/**存储图片/视频 model*/
@property (nonatomic, copy) NSArray *allPhotoArray;
/**图片Image*/
@property (nonatomic, strong) NSMutableArray *assetArray;
/**是否原图*/
@property (nonatomic) BOOL isOriginal;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) ReportModel *currentModel;
@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.navView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.navView);
        make.height.equalTo(@0.5);
    }];
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"举报";
    [self addRightBtnWith:@"发送"];
    [self.rightBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
    [self getData];
    [self setUpUI];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] postNotificationName:kPlayVideoNotification object:nil];
}
- (void)getData {
    self.dataArr = [NSMutableArray array];
    NSArray *titles = @[@"泄露隐私", @"人身攻击", @"淫秽色情", @"垃圾广告", @"敏感信息", @"侵权"];
    for (int i = 0; i < titles.count; i++) {
        NSString *title = titles[i];
        ReportModel *model = [[ReportModel alloc] init];
        model.title = title;
        model.type = i + 1;
        model.isSelect = NO;
        [self.dataArr addObject:model];
    }
    
}
- (void)setUpUI {
    [self.view addSubview:self.mainScrollView];
    self.mainScrollView.bounces = YES;
    self.mainScrollView.scrollEnabled = YES;
    self.mainScrollView.backgroundColor = RGBA(243, 243, 243, 1);
    self.mainScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.mainScrollView.frame = CGRectMake(0, SNavBarHeight, KSW, ScreenHeight-SNavBarHeight);

    CGFloat Y = 0;
    UIView *bg = [[UIView alloc] init];
    bg.backgroundColor = UIColor.whiteColor;
    [self.mainScrollView addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.navView);
        make.top.equalTo(@20);
        make.height.equalTo(@200);
    }];
    
    Y = 220;
    [bg addSubview:self.yyTextView];
    [self.yyTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(bg);
        make.right.equalTo(bg.mas_right).offset(-15);
        make.left.equalTo(@15);
    }];
    
    [self.mainScrollView addSubview:self.photoView];
    self.photoView.frame = CGRectMake(0, 220, KSW, kScaleSize(110));
    
    Y = Y + kScaleSize(110);
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = UIColor.whiteColor;
    [self.mainScrollView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.navView);
        make.top.equalTo(self.photoView.mas_bottom);
        make.height.equalTo(@10);
    }];
    
    UILabel *lab = [[UILabel alloc] init];
    lab.font = kFont(16);
    lab.text = @"选择你要举报的原因";
    lab.textColor = UIColor.grayColor;
    [self.mainScrollView addSubview:lab];
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.top.equalTo(bottomView.mas_bottom);
        make.height.equalTo(@50);
    }];
    
    Y = Y + 10 + 50;
    CGFloat tableH = self.dataArr.count * ReporCellH;
    [self.mainScrollView addSubview:self.mainTableView];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = UIColor.whiteColor;
    self.mainTableView.layer.cornerRadius = 8;
    self.mainTableView.layer.masksToBounds = YES;
//    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.scrollEnabled = NO;
    [self.mainTableView registerNib:[UINib nibWithNibName:@"ReportTableViewCell" bundle:nil] forCellReuseIdentifier:@"ReportTableViewCell"];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(self.navView.mas_right).offset(-15);
        make.top.equalTo(lab.mas_bottom);
//        make.bottom.equalTo(self.view.mas_bottom);
        make.height.equalTo(@(tableH));
    }];
    
    [self.mainTableView reloadData];
    
    Y = Y + tableH;
    if (Y > self.mainScrollView.height) {
        self.mainScrollView.contentSize = CGSizeMake(0, Y+20);
    } else {
        self.mainScrollView.contentSize = CGSizeMake(0, self.mainScrollView.height+20);
    }
}

- (void)rightClick:(UIButton *)sender {
    if (!self.yyTextView.text || self.yyTextView.text.length <= 0) {
        [self showText:@"请输入举报内容"];
        return;
    }
    if (!self.currentModel) {
        [self showText:@"请选择举报原因"];
        return;
    }
    if (self.allPhotoArray.count > 0) {
        self.assetArray = [NSMutableArray array];
        [SVProgressHUD showWithStatus:@""];
        [self postFiles];
    } else {
        [self report:@""];
    }
}
//MARK: ---------获取图片或者视频的数据/文件地址------------
static int requestCount = 0;
- (void)postFiles {
    kWeakSelf(self);
    [self requestMediaSourceWith:self.allPhotoArray[requestCount] success:^(id  _Nullable response) {
       //成功 继续获取下一个
        //图片
        [weakself.assetArray insertObject:response atIndex:requestCount];
        if (requestCount == weakself.allPhotoArray.count-1) {
            //结束
            //开始上传
//            [SVProgressHUD showWithStatus:@"图片上传中"];
            
            [weakself uploadImage];
        }
        else {
            requestCount ++;
            [weakself postFiles];
        }
    }];
}
- (void)report:(NSString *)imsgeStr {
    requestCount = 0;
    [self showSVP];
    kWeakSelf(self)
    [RequestManager report:[UserInfoManager shared].getUserID reportId:self.userId reportType:self.currentModel.type type:3 imageUrl:imsgeStr message:self.yyTextView.text withSuccess:^(id  _Nullable response) {
        [weakself dissSVP];
        [weakself alertView:@"举报成功" msg:@"" btnArrTitles:@[@"确定"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            [weakself.navigationController popViewControllerAnimated:YES];
        }];
    } withFail:^(NSError * _Nullable error) {
        [weakself dissSVP];
    }];
}
- (void)requestMediaSourceWith:(HXPhotoModel *)model success:(HttpResponseSuccessBlock)success{
    // 如果将_manager.configuration.requestImageAfterFinishingSelection 设为YES，
    // 那么在选择完成的时候就会获取图片和视频地址
    // 如果选中了原图那么获取图片时就是原图
    // 获取视频时如果设置 exportVideoURLForHighestQuality 为YES，则会去获取高等质量的视频。其他情况为中等质量的视频
    // 个人建议不在选择完成的时候去获取，因为每次选择完都会去获取。获取过程中可能会耗时过长
    // 可以在要上传的时候再去获取
        // 数组里装的是所有类型的资源，需要判断
        // 先判断资源类型
        if (model.subType == HXPhotoModelMediaSubTypePhoto) {
            // 当前为图片
            if (model.photoEdit) {
                // 如果有编辑数据，则说明这张图篇被编辑过了
                // 需要这样才能获取到编辑之后的图片
                UIImage *image = model.photoEdit.editPreviewImage;
                success(image);
            }
            // 再判断具体类型
            else {
                // 到这里就是手机相册里的图片了 model.asset PHAsset对象是有值的
                // 如果需要上传 Gif 或者 LivePhoto 需要具体判断
                if (model.type == HXPhotoModelMediaTypePhoto) {
                    // 普通的照片，如果不可以查看和livePhoto的时候，这就也可能是GIF或者LivePhoto了，
                    // 如果你的项目不支持动图那就不要取NSData或URL，因为如果本质是动图的话还是会变成动图传上去
                    // 这样判断是不是GIF model.photoFormat == HXPhotoModelFormatGIF
                    
                    // 如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.previewPhoto 或者 model.thumbPhoto 在选择完成时候已经获取并且赋值了
                    // 获取image
                    // size 就是获取图片的质量大小，原图的话就是 PHImageManagerMaximumSize，其他质量可设置size来获取
                    CGSize size;
                    if (self.isOriginal) {
                        size = PHImageManagerMaximumSize;
                    }else {
                        size = CGSizeMake(model.imageSize.width * 0.5, model.imageSize.height * 0.5);
                    }
                    
                    
                    [model requestPreviewImageWithSize:size startRequestICloud:^(PHImageRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        // 如果图片是在iCloud上的话会先走这个方法再去下载
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        // iCloud的下载进度
                    } success:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        // image
                        success(image);
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }else if (model.type == HXPhotoModelMediaTypePhotoGif) {
                    // 动图，如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.imageURL。因为在选择完成的时候已经获取了不用再去获取
//                    model.imageURL;
                    // 上传动图时，不要直接拿image上传哦。可以获取url或者data上传
                    // 获取data
                    NSLog(@"modelURL==%@",model.imageURL);
                    [model requestImageURLStartRequestICloud:^(PHContentEditingInputRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        
                    } success:^(NSURL * _Nullable imageURL, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        NSLog(@"URL==%@",imageURL);
                        
                        success(imageURL);
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        
                    }];
                }else if (model.type == HXPhotoModelMediaTypeLivePhoto) {
                    // LivePhoto，requestImageAfterFinishingSelection = YES 时没有处理livephoto，需要自己处理
                    // 如果需要上传livephoto的话，需要上传livephoto里的图片和视频
                    // 展示的时候需要根据图片和视频生成livephoto
                    [model requestLivePhotoAssetsWithSuccess:^(NSURL * _Nullable imageURL, NSURL * _Nullable videoURL, BOOL isNetwork, HXPhotoModel * _Nullable model) {
                        // imageURL - LivePhoto里的照片封面地址
                        // videoURL - LivePhoto里的视频地址
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }
                // 也可以不用上面的判断和方法获取，自己根据 model.asset 这个PHAsset对象来获取想要的东西
//                PHAsset *asset = model.asset;
            }
            
        }
}
//MARK: -----------------net work--------------------
- (void)uploadImage {
    requestCount = 0;
    [[YKHttpRequestManager shared] UploadImagesWithUrl:@"/api/storage/upload" withImages:self.assetArray withParams:nil withSuccess:^(id  _Nullable response) {
        [SVProgressHUD dismiss];
        if (response) {
            NSNumber *code = response[@"status"];
            if (code.intValue == 200) {
                //成功 获取URL
                NSArray *data = response[@"data"];
                NSString *tempString = @"";
                for (int i = 0; i < data.count; i ++) {
                    NSString *url = data[i][@"url"];
                    if (i == 0) {
                        tempString = url;
                    } else {
                        tempString = [NSString stringWithFormat:@"%@,%@", tempString, url];
                    }
                }
                [self report:tempString];
            }
        }
    } withFail:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
    } withProgress:^(int64_t bytesRead, int64_t totalBytes) {
        
    } withShouldLogin:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ReporCellH;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReportTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.dataArr[indexPath.row];
    if (indexPath.row == self.dataArr.count - 1) {
        cell.line.hidden = YES;
    } else {
        cell.line.hidden = NO;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    for (int i = 0; i < self.dataArr.count; i++) {
        ReportModel *model = self.dataArr[i];
        if (i == indexPath.row) {
            model.isSelect = YES;
        } else {
            model.isSelect = NO;
        }
    }
    self.currentModel = self.dataArr[indexPath.row];
    [self.mainTableView reloadData];
}
//MARK: --------------Photo View delegate------------
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {

    self.allPhotoArray = [NSArray arrayWithArray:allList];
    [self.assetArray removeAllObjects];
    self.isOriginal = isOriginal;
}


- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    //更新了视图的frame  下面的同步更新
    //newy-oldy = changy
//    self.photoViewBottom = photoView.bottom;
}
- (YYTextView *)yyTextView {
    if (!_yyTextView) {
        _yyTextView = [[YYTextView alloc] init];
        _yyTextView.font = kFont_Medium(15);
        _yyTextView.placeholderText = @"我想说~";
        _yyTextView.textColor = UIColor.blackColor;
        _yyTextView.backgroundColor = UIColor.whiteColor;
        _yyTextView.placeholderTextColor = RGBA(153, 153, 153, 1);
    }
    return _yyTextView;
}
- (HXPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [[HXPhotoView alloc] initWithManager:self.photoManager scrollDirection:UICollectionViewScrollDirectionVertical];
        _photoView.delegate = self;
        _photoView.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
        _photoView.spacing = (KSW-30-kScaleSize(108)*3)/2;
        _photoView.outerCamera = NO;
        _photoView.lineCount = 4;
        _photoView.addImageName = @"upload_sj";
        _photoView.backgroundColor = UIColor.whiteColor;
        _photoView.previewStyle = HXPhotoViewPreViewShowStyleDark;
    }
    return _photoView;
}
- (HXPhotoManager *)photoManager {
    if (!_photoManager) {
        _photoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        //黑色主题 视频或者图片 最多九张
        _photoManager.type = HXPhotoManagerSelectedTypePhoto;
        _photoManager.configuration.type = HXConfigurationTypeWXChat;
        _photoManager.configuration.videoMaxNum = 0;
        _photoManager.configuration.selectTogether = NO;
        _photoManager.configuration.photoMaxNum = 4;
        _photoManager.configuration.showOriginalBytes = YES;
        _photoManager.configuration.showOriginalBytesLoading = YES;
        _photoManager.configuration.requestImageAfterFinishingSelection = YES;
        _photoManager.configuration.exportVideoURLForHighestQuality = NO;
    }
    return _photoManager;
}
@end
