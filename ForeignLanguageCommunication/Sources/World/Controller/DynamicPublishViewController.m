//
//  DynamicPublishViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/12.
//

#import "DynamicPublishViewController.h"

typedef void(^HttpResponseSuccessBlock)(id _Nullable response);

@interface DynamicPublishViewController ()<HXPhotoViewDelegate,HXPhotoViewControllerDelegate,HXCustomNavigationControllerDelegate>
@property (nonatomic, strong) YYTextView *yyTextView;
/**图片管理器*/
@property (nonatomic, strong) HXPhotoManager *photoManager;
/**选择图片View*/
@property (nonatomic, strong) HXPhotoView *photoView;

/**视频封面*/
@property (nonatomic, copy) NSString *videoCoverUrl;
/**视频封面图片*/
@property (nonatomic, strong) UIImage *videoImage;
/**视频URL*/
@property (nonatomic, copy) NSString *videoUrl;
/**图片URL集合*/
@property (nonatomic, strong) NSArray *imageUrlArray;

/**存储图片/视频 model*/
@property (nonatomic, copy) NSArray *allPhotoArray;
/**图片Image*/
@property (nonatomic, strong) NSMutableArray *assetArray;
/**是否原图*/
@property (nonatomic) BOOL isOriginal;
/**判断是否视频还是图片*/
@property (nonatomic, assign) BOOL isPhotos;
//MARK: 判断当前是否可以点击发布按钮
//发布条件 文字/图片/视频/语音/  音频 audioPath判断    tagModel 判断
@property (nonatomic) BOOL hasText;
@property (nonatomic) BOOL hasPhoto;

@end

@implementation DynamicPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.navView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.navView);
        make.height.equalTo(@0.5);
    }];
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"发布动态";
    [self addRightBtnWith:@"发布"];
    [self.rightBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
    
    [self setUpUI];
}
- (void)setUpUI {
    [self.view addSubview:self.mainScrollView];
    self.mainScrollView.bounces = YES;
    self.mainScrollView.scrollEnabled = YES;
    self.mainScrollView.backgroundColor = kBackgroundColor;
    self.mainScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.mainScrollView.frame = CGRectMake(0, SNavBarHeight, KSW, ScreenHeight-SNavBarHeight);
    self.mainScrollView.contentSize = CGSizeMake(0, self.mainScrollView.height+20);
    
    UIView *bg = [[UIView alloc] init];
    bg.backgroundColor = UIColor.whiteColor;
    [self.mainScrollView addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.navView);
        make.top.equalTo(@20);
        make.height.equalTo(@200);
    }];

    [bg addSubview:self.yyTextView];
    [self.yyTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(bg);
        make.right.equalTo(bg.mas_right).offset(-10);
        make.left.equalTo(@10);
    }];
    
    [self.mainScrollView addSubview:self.photoView];
    self.photoView.frame = CGRectMake(0, 220, KSW, kScaleSize(110));
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = UIColor.whiteColor;
    [self.mainScrollView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.navView);
        make.top.equalTo(self.photoView.mas_bottom);
        make.height.equalTo(@50);
    }];
    
    UILabel *left = [[UILabel alloc] init];
    left.font = kFont(12);
    left.text = @"仅可上传6张照片,15s视频";
    left.textColor = RGBA(153, 153, 153, 1);
    [bottomView addSubview:left];
    [left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.centerY.equalTo(bottomView.mas_centerY).offset(-10);
    }];
    
    UILabel *right = [[UILabel alloc] init];
    right.font = kFont(12);
    right.text = @"不超过55字";
    right.textColor = RGBA(153, 153, 153, 1);
    [bottomView addSubview:right];
    [right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bottomView.mas_right).offset(-15);
        make.centerY.equalTo(bottomView.mas_centerY).offset(-8);
    }];
}
- (void)backClick {
    [self.view endEditing:YES];
    if (self.hasPhoto || self.yyTextView.text.length > 0) {
        kWeakSelf(self)
        [self alertView:@"" msg:@"确定放弃编辑内容" btnArrTitles:@[@"确定"] showCacel:YES style:UIAlertControllerStyleAlert block:^(int index) {
            [weakself.navigationController popViewControllerAnimated:YES];
        }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)rightClick:(UIButton *)sender {
    self.videoImage = nil;
    self.videoUrl = @"";
    self.videoCoverUrl = @"";
    self.imageUrlArray = nil;
    
    if (!self.yyTextView.text || self.yyTextView.text.length <= 0) {
        [self showText:@"请输入内容"];
        return;
    }
    
    if (self.hasPhoto) {
        self.assetArray = [NSMutableArray array];
        requestCount = 0;
//        [SVProgressHUD showWithStatus:@""];
        [self postFiles];
    } else {
        [self showText:@"照片/视频未上传，不能发布"];
    }
}
//MARK: ---------获取图片或者视频的数据/文件地址------------
static int requestCount = 0;
- (void)postFiles {
    kWeakSelf(self);
    [self requestMediaSourceWith:self.allPhotoArray[requestCount] success:^(id  _Nullable response) {
       //成功 继续获取下一个
        if (weakself.isPhotos) {
            //图片
            [weakself.assetArray insertObject:response atIndex:requestCount];
            if (requestCount == weakself.allPhotoArray.count-1) {
                //结束
                //开始上传
                [SVProgressHUD showWithStatus:@"图片上传中"];
                [weakself uploadImage];
            }
            else {
                requestCount ++;
                [weakself postFiles];
            }
        }
        else{
            NSString *videoPath = (NSString *)response;
            [weakself.assetArray insertObject:videoPath atIndex:0];
            //开始上传
            [weakself uploadVideoWithOption:^{
                [SVProgressHUD dismiss];
                [weakself publishDynamic];
            }];
        }
    }];
}
- (void)publishDynamic {
    NSString *picUrl = @"";
    int type = 0;
    if (self.isPhotos) {
        picUrl = [self.imageUrlArray componentsJoinedByString:@","];
        type = 1;
    }else {
        type = 2;
    }
    kWeakSelf(self);
    [SVProgressHUD showWithStatus:@""];
    [RequestManager addPublishsWithUserId:[UserInfoManager shared].getUserID type:type picUrl:picUrl content:self.yyTextView.text videoCoverUrl:self.videoCoverUrl videoUrl:self.videoUrl withSuccess:^(id  _Nullable response) {
        [SVProgressHUD dismiss];
        [weakself showText:@"发布成功"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshWorldList object:nil];
            [weakself.navigationController popViewControllerAnimated:YES];
        });
    } withFail:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
    }];
}
- (void)requestMediaSourceWith:(HXPhotoModel *)model success:(HttpResponseSuccessBlock)success{
    
    // 如果将_manager.configuration.requestImageAfterFinishingSelection 设为YES，
    // 那么在选择完成的时候就会获取图片和视频地址
    // 如果选中了原图那么获取图片时就是原图
    // 获取视频时如果设置 exportVideoURLForHighestQuality 为YES，则会去获取高等质量的视频。其他情况为中等质量的视频
    // 个人建议不在选择完成的时候去获取，因为每次选择完都会去获取。获取过程中可能会耗时过长
    // 可以在要上传的时候再去获取
        // 数组里装的是所有类型的资源，需要判断
        // 先判断资源类型
        if (model.subType == HXPhotoModelMediaSubTypePhoto) {
            // 当前为图片
            if (model.photoEdit) {
                // 如果有编辑数据，则说明这张图篇被编辑过了
                // 需要这样才能获取到编辑之后的图片
                UIImage *image = model.photoEdit.editPreviewImage;
                success(image);
            }
            // 再判断具体类型
            else {
                // 到这里就是手机相册里的图片了 model.asset PHAsset对象是有值的
                // 如果需要上传 Gif 或者 LivePhoto 需要具体判断
                if (model.type == HXPhotoModelMediaTypePhoto) {
                    // 普通的照片，如果不可以查看和livePhoto的时候，这就也可能是GIF或者LivePhoto了，
                    // 如果你的项目不支持动图那就不要取NSData或URL，因为如果本质是动图的话还是会变成动图传上去
                    // 这样判断是不是GIF model.photoFormat == HXPhotoModelFormatGIF
                    
                    // 如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.previewPhoto 或者 model.thumbPhoto 在选择完成时候已经获取并且赋值了
                    // 获取image
                    // size 就是获取图片的质量大小，原图的话就是 PHImageManagerMaximumSize，其他质量可设置size来获取
                    CGSize size;
                    if (self.isOriginal) {
                        size = PHImageManagerMaximumSize;
                    }else {
                        size = CGSizeMake(model.imageSize.width * 0.5, model.imageSize.height * 0.5);
                    }
                    
                    
                    [model requestPreviewImageWithSize:size startRequestICloud:^(PHImageRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        // 如果图片是在iCloud上的话会先走这个方法再去下载
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        // iCloud的下载进度
                    } success:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        // image
                        success(image);
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }else if (model.type == HXPhotoModelMediaTypePhotoGif) {
                    // 动图，如果 requestImageAfterFinishingSelection = YES 的话，直接取 model.imageURL。因为在选择完成的时候已经获取了不用再去获取
//                    model.imageURL;
                    // 上传动图时，不要直接拿image上传哦。可以获取url或者data上传
                    // 获取data
                    NSLog(@"modelURL==%@",model.imageURL);
                    [model requestImageURLStartRequestICloud:^(PHContentEditingInputRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
                        
                    } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                        
                    } success:^(NSURL * _Nullable imageURL, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                        NSLog(@"URL==%@",imageURL);
                        
                        success(imageURL);
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        
                    }];
                } else if (model.type == HXPhotoModelMediaTypeLivePhoto) {
                    // LivePhoto，requestImageAfterFinishingSelection = YES 时没有处理livephoto，需要自己处理
                    // 如果需要上传livephoto的话，需要上传livephoto里的图片和视频
                    // 展示的时候需要根据图片和视频生成livephoto
                    [model requestLivePhotoAssetsWithSuccess:^(NSURL * _Nullable imageURL, NSURL * _Nullable videoURL, BOOL isNetwork, HXPhotoModel * _Nullable model) {
                        // imageURL - LivePhoto里的照片封面地址
                        // videoURL - LivePhoto里的视频地址
                        
                    } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                        // 获取失败
                    }];
                }
                // 也可以不用上面的判断和方法获取，自己根据 model.asset 这个PHAsset对象来获取想要的东西
//                PHAsset *asset = model.asset;
            }
            
        }else if (model.subType == HXPhotoModelMediaSubTypeVideo) {
            // 当前为视频
            //因为在获取的时候已经requestImageAfterFinishingSelection = YES  直接获取
            self.videoImage = model.thumbPhoto;
            success(model.videoURL.absoluteString);
        }
    
}
//MARK: -----------------net work--------------------
- (void)uploadImage {
    [RequestManager uploadImage:self.assetArray withSuccess:^(id  _Nullable response) {
        NSArray *data = response[@"data"];
        NSMutableArray *muArr = [NSMutableArray array];
        for (int i = 0; i < data.count; i ++) {
            NSString *url = data[i][@"url"];
            [muArr addObject:url];
        }
        self.imageUrlArray = muArr;
        NSLog(@"images==%@",data);
            [self publishDynamic];
    } withFail:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
    }];
}
- (void)uploadVideoWithOption:(void(^)(void))opt {
    //上传封面
    [SVProgressHUD showWithStatus:@"上传视频中..."];
    [RequestManager uploadImage:@[self.videoImage] withSuccess:^(id  _Nullable response) {
        //成功 获取URL
        NSArray * data = response[@"data"];
        NSLog(@"videoimage==%@",data);
        self.videoCoverUrl = data[0][@"url"];
        [RequestManager postFileWithFilepath:self.assetArray[0] withName:@"video/mpeg4" withSuccess:^(id  _Nullable response) {
            NSLog(@"%@",response);
            NSArray * data = response[@"data"];
            NSLog(@"video==%@",data);
            self.videoUrl = data[0][@"url"];
            if (opt) {
                opt();
            }
            
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"video==%@",error);
            [SVProgressHUD dismiss];
        }];
    } withFail:^(NSError * _Nullable error) {
        [SVProgressHUD dismiss];
    }];
}

//MARK: --------------Photo View delegate------------
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {

    self.allPhotoArray = [NSArray arrayWithArray:allList];
    [self.assetArray removeAllObjects];
    
    self.isOriginal = isOriginal;
    if (self.allPhotoArray.count > 0) {
        self.hasPhoto = YES;
        if (photos.count > 0) {
            //全是图片
            NSLog(@"选择了图片");
            self.isPhotos = YES;
        }else {
            //一个视频
            NSLog(@"选择了视频");
            self.isPhotos = NO;
        }
    }else {
        self.hasPhoto = NO;
    }
}


- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    //更新了视图的frame  下面的同步更新
    //newy-oldy = changy
//    self.photoViewBottom = photoView.bottom;
    
}


- (void)photoViewDidCancel:(HXPhotoView *)photoView {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)photoViewDidAddCellClick:(HXPhotoView *)photoView {
    [self.view endEditing:YES];
}
- (void)dealloc {
    
}
- (void)textViewDidChangeNotification:(NSNotification *)obj {
    YYTextView *textView = (YYTextView *)obj.object;
    NSString *string = textView.text;
    NSInteger maxLength = 55;
    //获取高亮部分
    YYTextRange *selectedRange = [textView valueForKey:@"_markedTextRange"];
    NSRange range = [selectedRange asRange];
    NSString *realString = [string substringWithRange:NSMakeRange(0, string.length - range.length)];
    if (realString.length >= maxLength){
        textView.text = [realString substringWithRange:NSMakeRange(0, maxLength)];
    }
}

- (YYTextView *)yyTextView {
    if (!_yyTextView) {
        _yyTextView = [[YYTextView alloc] init];
        _yyTextView.font = kFont_Medium(15);
        _yyTextView.placeholderText = @"我想说~";
        _yyTextView.textColor = UIColor.blackColor;
        _yyTextView.backgroundColor = UIColor.whiteColor;
        _yyTextView.placeholderTextColor = RGBA(153, 153, 153, 1);
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidChangeNotification:) name:YYTextViewTextDidChangeNotification object:nil];
    }
    return _yyTextView;
}
- (HXPhotoView *)photoView {
    if (!_photoView) {
        _photoView = [[HXPhotoView alloc] initWithManager:self.photoManager scrollDirection:UICollectionViewScrollDirectionVertical];
        _photoView.delegate = self;
        _photoView.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
//        _photoView.spacing = (KSW-30-kScaleSize(108)*2)/2;
        _photoView.spacing = 15;
        _photoView.outerCamera = NO;
        _photoView.addImageName = @"upload_sj";
        _photoView.lineCount = 3;
        _photoView.backgroundColor = UIColor.whiteColor;
        _photoView.previewStyle = HXPhotoViewPreViewShowStyleDark;
    }
    return _photoView;
}
- (HXPhotoManager *)photoManager {
    if (!_photoManager) {
        _photoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        //黑色主题 视频或者图片 最多九张
        _photoManager.configuration.type = HXConfigurationTypeWXChat;
        _photoManager.configuration.videoMaxNum = 1;
        _photoManager.configuration.selectTogether = NO;
        _photoManager.configuration.videoMaximumDuration = 15;
        _photoManager.configuration.videoMaximumSelectDuration = 15;
        _photoManager.configuration.photoMaxNum = 6;
        _photoManager.configuration.showOriginalBytes = YES;
        _photoManager.configuration.showOriginalBytesLoading = YES;
        _photoManager.configuration.requestImageAfterFinishingSelection = YES;
        _photoManager.configuration.exportVideoURLForHighestQuality = NO;
    }
    return _photoManager;
}
@end
