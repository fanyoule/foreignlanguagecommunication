//
//  WorldDetailViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/12.
//

#import "BaseViewController.h"
#import "WorldListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorldDetailViewController : BaseViewController
@property (nonatomic, strong) WorldListModel *model;

@property (nonatomic, assign)NSInteger worksID;

@end

NS_ASSUME_NONNULL_END
