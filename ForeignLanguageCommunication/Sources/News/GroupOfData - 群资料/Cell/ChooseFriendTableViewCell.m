//
//  ChooseFriendTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "ChooseFriendTableViewCell.h"

@implementation ChooseFriendTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}


- (void)addControls{
    [self.contentView addSubview:self.avataImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.IDLabel];
    [self.contentView addSubview:self.chooseBtn];
    
    [self.avataImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.bottom.equalTo(self.contentView).offset(-15).priority(600);
        make.left.equalTo(self.contentView).offset(15);
        make.height.width.equalTo(@50);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.avataImageV).offset(2);
        make.left.equalTo(self.avataImageV.mas_right).offset(10);
    }];
    [self.IDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.avataImageV).offset(-2);
        make.left.equalTo(self.avataImageV.mas_right).offset(10);
    }];
    [self.chooseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
}
- (void)setModel:(CanChooseFriendModel *)model {
    _model = model;
    
    [self.avataImageV sd_setImageWithURL:model.url.mj_url];
    self.nameLabel.text = model.nickname;
    self.IDLabel.text = [NSString stringWithFormat:@"ID:%ld", model.Id];
    self.chooseBtn.selected = model.isSelect;
}
- (UIImageView *)avataImageV{
    if (!_avataImageV) {
        _avataImageV = [[UIImageView alloc]init];
        _avataImageV.image = [UIImage imageNamed:@"测试头像"];
        _avataImageV.contentMode = UIViewContentModeScaleAspectFill;
        _avataImageV.layer.cornerRadius = 6;
        _avataImageV.clipsToBounds = YES;
    }
    return _avataImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Medium(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}
- (UILabel *)IDLabel{
    if (!_IDLabel) {
        _IDLabel = [[UILabel alloc]init];
        _IDLabel.text = @"ID:634565846";
        _IDLabel.textColor = RGBA(153, 153, 153, 1);
        _IDLabel.font = kFont(12);
        _IDLabel.textAlignment = 0;
    }
    return _IDLabel;
}

- (UIButton *)chooseBtn{
    if (!_chooseBtn) {
        _chooseBtn = [[UIButton alloc]init];
        [_chooseBtn setImage:[UIImage imageNamed:@"未选"] forState:(UIControlStateNormal)];
        [_chooseBtn setImage:[UIImage imageNamed:@"选中"] forState:(UIControlStateSelected)];
    }
    return _chooseBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
