//
//  DissolutionTheGroupTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "DissolutionTheGroupTableViewCell.h"

@implementation DissolutionTheGroupTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
- (void)addControls{
    
    
    [self.contentView addSubview:self.button];
    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(30);
        make.bottom.equalTo(self.contentView).offset(-15).priority(600);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@44);
    }];
}

- (UIButton *)button{
    if (!_button) {
        _button = [[UIButton alloc]init];
        [_button setTitle:@"解散群组" forState:(UIControlStateNormal)];
        [_button setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _button.titleLabel.font = kFont_Medium(16);
        _button.backgroundColor = RGBA(255, 96, 93, 1);
        _button.layer.cornerRadius = 22;
        _button.clipsToBounds = YES;
        [_button addTarget:self action:@selector(buttonClickAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}

- (void)buttonClickAction {
    if (self.buttonclick) {
        self.buttonclick();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
