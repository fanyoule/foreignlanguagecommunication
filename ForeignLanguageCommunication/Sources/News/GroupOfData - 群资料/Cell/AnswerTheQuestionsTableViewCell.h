//
//  AnswerTheQuestionsTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AnswerTheQuestionsTableViewCell : UITableViewCell

@property (nonatomic, strong)UILabel *titleLabel;

@property (nonatomic, strong)UITextField *textField;


@end

NS_ASSUME_NONNULL_END
