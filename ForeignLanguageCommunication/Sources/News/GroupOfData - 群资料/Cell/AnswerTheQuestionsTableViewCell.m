//
//  AnswerTheQuestionsTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/3.
//

#import "AnswerTheQuestionsTableViewCell.h"

@implementation AnswerTheQuestionsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}


- (void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.textField];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.bottom.equalTo(self.contentView).offset(-15);
        make.left.equalTo(self.contentView).offset(15);
        make.width.equalTo(@50);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.right.equalTo(self.contentView);
        make.left.equalTo(self.titleLabel.mas_right).offset(5);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.equalTo(self.contentView);
        make.left.equalTo(self.textField);
        make.height.equalTo(@.5);
    }];
    
    
}
- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"问题:";
        _titleLabel.textColor = RGBA(116, 92, 240, 1);
        _titleLabel.font = kFont(16);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc]init];
//        _textField.keyboardType = UIKeyboardTypeDecimalPad;
        _textField.backgroundColor = UIColor.whiteColor;
        _textField.placeholder = @"请输入问题";
        _textField.font = kFont_Regular(16);
//        _textField.delegate = self;
       
    }
    return _textField;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
