//
//  GroupAnnouncementTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "GroupAnnouncementTableViewCell.h"
@interface GroupAnnouncementTableViewCell ()
/** 头像*/
@property (nonatomic, strong)UIImageView *avataImageV;
/** 昵称*/
@property (nonatomic, strong)UILabel *nickName;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;

/** 时间图片*/
@property (nonatomic, strong)UIImageView *timeImageV;
/** 时间*/
@property (nonatomic, strong)UILabel *timeLabel;


@end
@implementation GroupAnnouncementTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}


- (void)addControls{
    [self.contentView addSubview:self.avataImageV];
    [self.contentView addSubview:self.nickName];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.timeImageV];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.moreBtn];
    [self.avataImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.left.equalTo(self.contentView).offset(10);
        make.width.height.equalTo(@25);
    }];
    [self.nickName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.avataImageV);
        make.left.equalTo(self.avataImageV.mas_right).offset(5);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nickName);
        make.left.equalTo(self.nickName.mas_right).offset(5);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.avataImageV.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.timeImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentLabel.mas_bottom).offset(15);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@8);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.timeImageV);
        make.left.equalTo(self.timeImageV.mas_right).offset(5);
        make.bottom.equalTo(self.contentView).offset(-10).priority(600);
    }];
    [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-5);
        make.width.height.equalTo(@44);
    }];
    
}
- (void)setModel:(NoticeModel *)model {
    _model = model;
    
    self.contentLabel.text = model.content;
    self.nickName.text = model.nickname;
    [self.avataImageV sd_setImageWithURL:[NSURL URLWithString:model.avatar]];
    self.titleLabel.text = model.title;
    self.timeLabel.text = model.createTime;
}
- (UIImageView *)avataImageV{
    if (!_avataImageV) {
        _avataImageV = [[UIImageView alloc]init];
        _avataImageV.image = [UIImage imageNamed:@"测试头像2"];
        _avataImageV.layer.cornerRadius = 3;
        _avataImageV.clipsToBounds = YES;
        _avataImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _avataImageV;
}
- (UILabel *)nickName{
    if (!_nickName) {
        _nickName = [[UILabel alloc]init];
        _nickName.text = @"昵称";
        _nickName.textColor = RGBA(85, 85, 85, 1);
        _nickName.font = kFont(12);
        _nickName.textAlignment = 0;
    }
    return _nickName;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"标题";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont(16);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"内容内容内容内容";
        _contentLabel.textColor = RGBA(85, 85, 85, 1);
        _contentLabel.font = kFont(12);
        _contentLabel.textAlignment = 0;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}
- (UIImageView *)timeImageV{
    if (!_timeImageV) {
        _timeImageV = [[UIImageView alloc]init];
        _timeImageV.image = [UIImage imageNamed:@"时间-闹钟"];
    }
    return _timeImageV;
}
- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.text = @"1999-9-9 17：45";
        _timeLabel.textColor = RGBA(85, 85, 85, 1);
        _timeLabel.font = kFont(12);
        _timeLabel.textAlignment = 0;
    }
    return _timeLabel;
}
- (UIButton *)moreBtn{
    if (!_moreBtn) {
        _moreBtn = [[UIButton alloc]init];
        [_moreBtn setImage:[UIImage imageNamed:@"icon_menu_v"] forState:(UIControlStateNormal)];
        [_moreBtn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreBtn;
}
- (void)btnClick {
    if (self.block) {
        self.block(self.index);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
