//
//  GroupOfDataTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "GroupOfDataTableViewCell.h"

@implementation GroupOfDataTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}
- (void)addControls{
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.arrowImageV];
    [self.contentView addSubview:self.swi];
    [self.contentView addSubview:self.contentLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(25);
        make.bottom.equalTo(self.contentView).offset(-25).priority(600);
        make.left.equalTo(self.contentView).offset(15);
        make.width.equalTo(@140);
    }];
    [self.arrowImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
        make.width.equalTo(@8);
    }];
    [self.swi mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.arrowImageV.mas_left).offset(-5);
        make.left.equalTo(self.titleLabel.mas_right).offset(10);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
        make.bottom.equalTo(self.contentView);
    }];
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        
        _titleLabel.text = @"群介绍";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont(17);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UIImageView *)arrowImageV{
    if (!_arrowImageV) {
        _arrowImageV = [[UIImageView alloc]init];
        _arrowImageV.image = [UIImage imageNamed:@"个人资料-箭头"];
    }
    return _arrowImageV;
}
- (UISwitch *)swi {
    if (!_swi) {
        _swi = [[UISwitch alloc] init];
        [_swi setOnTintColor:RGBA(52, 120, 245, 1)];//开启时颜色
        [_swi setBackgroundColor:RGBA(255, 255, 255, 1)];
        [_swi addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    }
    return _swi;
}
- (void)switchAction:(UISwitch *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(switchClick:)]) {
        [self.delegate switchClick:self.index];
    }
}
- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"设置群简介，不能少于15个字";
        _contentLabel.textColor = RGBA(153, 153, 153, 1);
        _contentLabel.font = kFont(15);
        _contentLabel.textAlignment = 2;
    }
    return _contentLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
