//
//  GroupMembersTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "GroupMembersTableViewCell.h"

@implementation GroupMembersTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}


- (void)addControls{
    [self.contentView addSubview:self.avataImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.positionLabel];
    [self.contentView addSubview:self.moreBtn];
    [self.contentView addSubview:self.meLab];
    [self.avataImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.bottom.equalTo(self.contentView).offset(-15);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@44);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.avataImageV.mas_right).offset(10);
        
    }];
    [self.positionLabel sizeToFit];
    CGFloat w = self.positionLabel.bounds.size.width + 10;
    [self.positionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.left.equalTo(self.nameLabel.mas_right).offset(5);
        make.width.equalTo(@(w));
    }];
    [self.moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.meLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.contentView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel);
        make.bottom.right.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
    
}

- (UIImageView *)avataImageV{
    if (!_avataImageV) {
        _avataImageV = [[UIImageView alloc]init];
        _avataImageV.image = [UIImage imageNamed:@"测试头像"];
        _avataImageV.layer.cornerRadius = 6;
        _avataImageV.clipsToBounds = YES;
        _avataImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _avataImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"昵称";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Medium(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}
- (UILabel *)positionLabel{
    if (!_positionLabel) {
        _positionLabel = [[UILabel alloc]init];
        _positionLabel.text = @"群主";
        _positionLabel.textColor = RGBA(0, 122, 255, 1);
        _positionLabel.font = kFont(12);
        _positionLabel.textAlignment = 1;
        _positionLabel.layer.cornerRadius = 3;
        _positionLabel.clipsToBounds = YES;
        _positionLabel.layer.borderColor = RGBA(0, 122, 255, 1).CGColor;
        _positionLabel.layer.borderWidth = 0.5;
    }
    return _positionLabel;
}
- (UILabel *)meLab {
    if (!_meLab) {
        _meLab = [[UILabel alloc] init];
        _meLab.text = @"我";
        _meLab.font = kFont(14);
        _meLab.textColor = kColor(@"#999999");
    }
    return _meLab;
}
- (UIButton *)moreBtn{
    if (!_moreBtn) {
        _moreBtn = [[UIButton alloc]init];
        [_moreBtn setImage:[UIImage imageNamed:@"icon_menu_v"] forState:(UIControlStateNormal)];
    }
    return _moreBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
