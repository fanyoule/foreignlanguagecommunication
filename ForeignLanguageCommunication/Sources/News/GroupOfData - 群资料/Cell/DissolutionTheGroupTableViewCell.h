//
//  DissolutionTheGroupTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef void(^DissolutionTheGroupTableViewCellBlock)(void);

@interface DissolutionTheGroupTableViewCell : UITableViewCell
@property (nonatomic, strong)UIButton *button;

@property (nonatomic, copy) DissolutionTheGroupTableViewCellBlock buttonclick;
@end

NS_ASSUME_NONNULL_END
