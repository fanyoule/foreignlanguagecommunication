//
//  ChooseFriendTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import <UIKit/UIKit.h>
#import "CanChooseFriendModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChooseFriendTableViewCell : UITableViewCell
/** 头像*/
@property (nonatomic, strong)UIImageView *avataImageV;
/** 昵称*/
@property (nonatomic, strong)UILabel *nameLabel;
/** ID*/
@property (nonatomic, strong)UILabel *IDLabel;
/** 选择*/
@property (nonatomic, strong)UIButton *chooseBtn;
@property (nonatomic, strong) CanChooseFriendModel *model;
@end

NS_ASSUME_NONNULL_END
