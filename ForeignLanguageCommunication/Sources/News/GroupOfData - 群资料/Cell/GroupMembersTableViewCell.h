//
//  GroupMembersTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupMembersTableViewCell : UITableViewCell
/** 头像*/
@property (nonatomic, strong)UIImageView *avataImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/** 职位*/
@property (nonatomic, strong)UILabel *positionLabel;
/** 更多*/
@property (nonatomic, strong)UIButton *moreBtn;
@property (nonatomic, strong) UILabel *meLab;
@end

NS_ASSUME_NONNULL_END
