//
//  GroupOfDataTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol GroupOfDataTableViewCellDelegate <NSObject>

- (void)switchClick:(NSInteger)index;

@end
@interface GroupOfDataTableViewCell : UITableViewCell

@property (nonatomic, assign) NSInteger index;
/** 标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 箭头*/
@property (nonatomic, strong)UIImageView *arrowImageV;
/** 开关*/
@property (nonatomic, strong)UISwitch *swi;
/** 内容*/
@property (nonatomic, strong)UILabel *contentLabel;
@property (nonatomic, weak) id<GroupOfDataTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
