//
//  GroupAnnouncementTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import <UIKit/UIKit.h>
#import "NoticeModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^GroupAnnouncementTableViewCellBlock)(NSInteger index);
@interface GroupAnnouncementTableViewCell : UITableViewCell
/** 内容*/
@property (nonatomic, strong)UILabel *contentLabel;
@property (nonatomic, strong) NoticeModel *model;
/** 更多*/
@property (nonatomic, strong)UIButton *moreBtn;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, copy) GroupAnnouncementTableViewCellBlock block;
@end

NS_ASSUME_NONNULL_END
