//
//  ChatGroupProfileViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "ChatGroupProfileViewController.h"
#import "GroupOfDataHeadView.h"
#import "GroupOfDataTableViewCell.h"
#import "DissolutionTheGroupTableViewCell.h"
#import "TheGrayBarTableViewCell.h"// 灰色的条"
#import "GroupOfDataModel.h"
#import "GroupNameViewController.h"// 修改群名称"
#import "GroupMembersViewController.h" // 群成员
#import "MyPersonalInformationVC.h"// 个人资料
#import "ChooseFriendViewController.h"// 邀请好友
#import "GroupToIntroduceViewController.h"//群介绍
#import "GroupAnnouncementViewController.h"//群公告
#import "BannedListVC.h"// 禁言列表"
#import "TheGroupOfValidationViewController.h"// 入群验证
#import "GroupOfDataModel.h"
#import "CurrentGroupUserinfoModel.h"

@interface ChatGroupProfileViewController ()<UITableViewDelegate,UITableViewDataSource, GroupOfDataTableViewCellDelegate>
/** head头*/
@property (nonatomic, strong)GroupOfDataHeadView *headView;
@property (nonatomic, strong) GroupOfDataModel *groupDataModel;
@property (nonatomic, strong) CurrentGroupUserinfoModel *myInfoModel;
@end

@implementation ChatGroupProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"群资料";
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    self.mainTableView.mj_header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    [self addHeadView];
//    [self getData];
//    [self getUserInGroupInfo];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getUserInGroupInfo];
//    [self getData];
}
- (void)addHeadView {
    self.headView.model = self.groupDataModel;
    self.headView.frame = CGRectMake(0, 0, KSW, 200);
    self.headView.backgroundColor = UIColor.whiteColor;
    self.mainTableView.tableHeaderView = self.headView;
    
    kWeakSelf(self)
    /** 修改群名称*/
    self.headView.groupName = ^{
        GroupNameViewController *vc = [[GroupNameViewController alloc]init];
        vc.ID = weakself.ID;
        [weakself.navigationController pushViewController:vc animated:YES];
    };
    /** 查看所有成员*/
    self.headView.theMembers = ^{
        GroupMembersViewController *vc = [[GroupMembersViewController alloc]init];
        vc.ID = weakself.ID;
        [weakself.navigationController pushViewController:vc animated:YES];
    };
    /** 点击成员头像 查看个人资料*/
    self.headView.clickMembers = ^(GroupUserModel * _Nonnull model) {
        [weakself headerClick:model];
    };
    /** 邀请好友*/
    self.headView.addFriends = ^{
        ChooseFriendViewController *vc = [[ChooseFriendViewController alloc]init];
        vc.ID = self.ID;
        [weakself.navigationController pushViewController:vc animated:YES];
    };
}
// 点击成员头像
- (void)headerClick:(GroupUserModel *)user {
    MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc] init];
    vc.ID = user.userId;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)stopLoading {
    if (self.mainTableView.mj_header.isRefreshing) {
        [self.mainTableView.mj_header endRefreshing];
    }
}
- (void)getUserInGroupInfo {
    kWeakSelf(self)
    [RequestManager getUserClusterVoWithId:[UserInfoManager shared].getUserID clusterId:self.ID withSuccess:^(id  _Nullable response) {
        weakself.myInfoModel = [CurrentGroupUserinfoModel mj_objectWithKeyValues:response[@"data"]];
        if (weakself.myInfoModel.identity == 0) {
            weakself.headView.editorBtn.hidden = YES;
        }
        [self getData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)getData {
    kWeakSelf(self)
    [RequestManager getPersonalClusterById:[UserInfoManager shared].getUserID Id:self.ID withSuccess:^(id  _Nullable response) {
        weakself.groupDataModel = [GroupOfDataModel mj_objectWithKeyValues:response[@"data"]];
        [weakself refresh];
        SetCache(weakself.groupDataModel.isNoDisturbing == 1, ISNoDisturbingKey(weakself.ID));
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)refresh {
    [self stopLoading];
    self.headView.model = self.groupDataModel;
    [self.mainTableView reloadData];
}
- (GroupOfDataHeadView *)headView{
    if (!_headView) {
        _headView = [[GroupOfDataHeadView alloc]init];
    }
    return _headView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.groupDataModel) {
        if (self.myInfoModel && self.myInfoModel.identity == 2) {
            return 13;
        } else {
            return 10;
        }
    } else {
        return 0;
    }
}
- (void)switchClick:(NSInteger)index {
    if (self.myInfoModel && self.myInfoModel.identity == 2) {
        if (index == 5) {
            [self setTop];
        } else if (index == 6) {
            [self setChair];
        } else if (index == 7) {
            [self setNoDisturbing];
        } else { // 8
            [self setShield];
        }
    } else {
        if (index == 5) {
            [self setNoDisturbing];
        } else if (index == 6) {
            [self setShield];
        } else { // 4
            [self setTop];
        }
    }
}
// 置顶
- (void)setTop {
//    kWeakSelf(self)
//    [self showSVP];
    BOOL isTop = GetCache(ISTopKey(self.ID));
    NSInteger value = isTop == 1 ? 0 : 1;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    dic[@"fromId"]= @([UserInfoManager shared].getUserID);//自己的id
    dic[@"groupId"] = @(self.ID);//群id
    dic[@"type"]= @"2";//0个人，1.工会 2.群聊 3.讨论组
    dic[@"status"] = @(value);//1.置顶 0.取消
    kWeakSelf(self)
    [self showSVP];
    [RequestManager getIMDataWithParameters:dic IMHost:IMPostHost UrlString:@"api/messageHandler/updateSessionRoomIsTopFromGroup" Success:^(id  _Nullable response) {
        NSLog(@"responm===%@",response);
        [weakself dissSVP];
        if ([response[@"status"]intValue]==200) {
            SetCache(value == 1, ISTopKey(weakself.ID));
            [weakself.mainTableView reloadData];
        }
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
// 免打扰
- (void)setNoDisturbing {
    kWeakSelf(self)
    [self showSVP];
    long value = self.groupDataModel.isNoDisturbing == 1 ? 0 : 1;
    [RequestManager updatePersonalClusterWithId:self.ID userId:[UserInfoManager shared].getUserID isNoDisturbing:value withSuccess:^(id  _Nullable response) {
        [weakself dissSVP];
        [weakself getData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
// 屏蔽
- (void)setShield {
    kWeakSelf(self)
    [self showSVP];
    long value = self.groupDataModel.isShield == 1 ? 0 : 1;
    [RequestManager updatePersonalClusterWithId:self.ID userId:[UserInfoManager shared].getUserID isShield:value withSuccess:^(id  _Nullable response) {
        [weakself dissSVP];
        [weakself getData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
// 主席模式
- (void)setChair {
    kWeakSelf(self)
    [self showSVP];
    long value = self.groupDataModel.chair == 1 ? 0 : 1;
    [RequestManager updatePersonalClusterWithId:self.ID userId:[UserInfoManager shared].getUserID chair:value withSuccess:^(id  _Nullable response) {
        [weakself dissSVP];
        [weakself getData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)deleGroupHistory {
    [self alertView:@"提示" msg:@"确定删除聊天记录?" btnArrTitles:@[@"确定"] showCacel:YES style:UIAlertControllerStyleAlert block:^(int index) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic[@"fromId"]= @([UserInfoManager shared].getUserID);//自己的id
        dic[@"groupId"] = @(self.ID);//公会id
        dic[@"type"]= @"2";//0个人，1.工会 2.群聊 3.讨论组
        [RequestManager getIMDataWithParameters:dic IMHost:IMPostHost UrlString:@"api/messageHandler/delGroupAllMessageHistoryById" Success:^(id  _Nullable response) {
            NSLog(@"responm===%@",response);
            if ([response[@"status"]intValue]==200) {
                [self showText:@"删除成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteChatGroupMsgs" object:nil userInfo:nil];
            }
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"error===%@",error);
        }];
    }];
}
// 解散群组
- (void)deletGroup {
    kWeakSelf(self)
    [self showSVP];
    [RequestManager deletePersonalClusterById:self.ID withSuccess:^(id  _Nullable response) {
        [weakself showText:@"解散群组成功"];
        [self showText:@"删除成功"];
        NSString *idString = @"";
        for (GroupUserModel *model in self.groupDataModel.clusterMember) {
            if (idString.length <= 0) {
                idString = LongToString(model.userId);
            } else {
                idString = [NSString stringWithFormat:@"%@,%ld", idString, model.userId];
            }
        }
        NSMutableDictionary *IMDic = [[NSMutableDictionary alloc]init];
        IMDic[@"from"] =@([UserInfoManager shared].getUserID);
        IMDic[@"msgType"] = @(0);
        IMDic[@"chatType"] = @(7);
        IMDic[@"groupType"] = @(2);
        IMDic[@"group_id"] = @(self.ID);
        IMDic[@"content"] = idString;
        NSLog(@"imdic===%@",IMDic);
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:IMDic
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //发送消息方法
        [[ChatIMSend share]sendMessage:str];
        //pop到根视图控制器
        [self.navigationController popToRootViewControllerAnimated:YES];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
// 退出群
- (void)quitGroup {
    kWeakSelf(self)
    [RequestManager deletePersonalClusterMemberByIdWithId:self.ID userId:[UserInfoManager shared].getUserID withSuccess:^(id  _Nullable response) {
        [weakself showText:@"退出成功"];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic[@"from"] = @([UserInfoManager shared].getUserID);
        dic[@"msgType"] = @(0);
        dic[@"chatType"] = @(6);
        dic[@"groupType"] = @(2);
        dic[@"group_id"] = @(weakself.ID);
        dic[@"content"] = @([UserInfoManager shared].getUserID);
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject: dic
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
            
            NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            //发送消息方法
           // [self sendMessage:str cmd:11];
        [[ChatIMSend share]sendMessage:str];
        //pop到根视图控制器
//        [self.navigationController popToRootViewControllerAnimated:NO];
        [self.tabBarController setSelectedIndex:2];
        [self.navigationController popToRootViewControllerAnimated:NO];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.myInfoModel && self.myInfoModel.identity == 2) {
        if (indexPath.row == 0||indexPath.row == 4||indexPath.row == 10) {
            TheGrayBarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheGrayBarTableViewCell"];
                if (!cell) {
                    cell = [[TheGrayBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheGrayBarTableViewCell"];
                }
            cell.backgroundColor = UIColor.whiteColor;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row == 12) {
            DissolutionTheGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DissolutionTheGroupTableViewCell"];
            if (!cell) {
               cell = [[DissolutionTheGroupTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DissolutionTheGroupTableViewCell"];
            }
            cell.backgroundColor = RGBA(245, 245, 245, 1);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.button setTitle:@"解散群组" forState:UIControlStateNormal];
            cell.buttonclick = ^{
                [self alertView:@"提示" msg:@"确定解散群组" btnArrTitles:@[@"确定"] showCacel:YES style:UIAlertControllerStyleAlert block:^(int index) {
                    [self deletGroup];
                }];
            };
           return cell;
        }
        
        GroupOfDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupOfDataTableViewCell"];
        if (!cell) {
            cell = [[GroupOfDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupOfDataTableViewCell"];
        }
        cell.delegate = self;
        cell.index = indexPath.row;
        if (indexPath.row == 1) {
            cell.swi.hidden = YES;
            cell.titleLabel.text = @"群介绍";
            cell.contentLabel.text = self.groupDataModel.clusterIntro && self.groupDataModel.clusterIntro.length > 0 ? self.groupDataModel.clusterIntro : @"设置群简介，不能少于15个字";
            [cell whenTapped:^{
                GroupToIntroduceViewController *vc = [[GroupToIntroduceViewController alloc]init];
                vc.ID = self.ID;
                [self.navigationController pushViewController:vc animated:YES];
            }];
            
        }else if (indexPath.row == 2){
            cell.contentLabel.hidden = YES;
            cell.swi.hidden = YES;
            cell.titleLabel.text = @"公告历史";
            [cell whenTapped:^{
                GroupAnnouncementViewController *vc = [[GroupAnnouncementViewController alloc]init];
                vc.identity = self.myInfoModel.identity;
                vc.ID = self.ID;
                [self.navigationController pushViewController:vc animated:YES];
            }];
            
        }else if (indexPath.row == 3){
            cell.swi.hidden = YES;
            cell.titleLabel.text = @"禁言列表";
            cell.contentLabel.text = @"0人禁言中";
            [cell whenTapped:^{
                BannedListVC *vc = [[BannedListVC alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }];
        }else if (indexPath.row == 5){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.titleLabel.text = @"置顶该群";
            BOOL isTop = GetCache(ISTopKey(self.ID));
            cell.swi.on = isTop;
        }else if (indexPath.row == 6){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.titleLabel.text = @"主席模式";
            cell.swi.on = self.groupDataModel.chair == 1;
        }else if (indexPath.row == 7){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.titleLabel.text = @"消息免打扰";
            cell.swi.on = self.groupDataModel.isNoDisturbing == 1;
        }else if (indexPath.row == 8){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.titleLabel.text = @"消息屏蔽";
            cell.swi.on = self.groupDataModel.isShield == 1;
        }else if (indexPath.row == 9){
            cell.contentLabel.hidden = YES;
            cell.swi.hidden = YES;
            cell.titleLabel.text = @"入群验证";
            [cell whenTapped:^{
                TheGroupOfValidationViewController *vc = [[TheGroupOfValidationViewController alloc]init];
                vc.groupDataModel = self.groupDataModel;
                [self.navigationController pushViewController:vc animated:YES];
            }];
        }else if (indexPath.row == 11){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.swi.hidden = YES;
            cell.titleLabel.text = @"删除聊天记录";
            cell.titleLabel.textColor = RGBA(116, 92, 240, 1);
            [cell whenTapped:^{
                [self deleGroupHistory];
            }];
        }
        cell.backgroundColor = UIColor.whiteColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        if (indexPath.row == 0||indexPath.row == 3||indexPath.row == 7) {
            TheGrayBarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheGrayBarTableViewCell"];
                if (!cell) {
                    cell = [[TheGrayBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheGrayBarTableViewCell"];
                }
            cell.backgroundColor = UIColor.whiteColor;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row == 9) {
            DissolutionTheGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DissolutionTheGroupTableViewCell"];
               if (!cell) {
                   cell = [[DissolutionTheGroupTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DissolutionTheGroupTableViewCell"];
               }
            cell.backgroundColor = RGBA(245, 245, 245, 1);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.button setTitle:@"删除并退出" forState:UIControlStateNormal];
            cell.buttonclick = ^{
                [self alertView:@"提示" msg:@"确定删除并退出群组" btnArrTitles:@[@"确定"] showCacel:YES style:UIAlertControllerStyleAlert block:^(int index) {
                    [self quitGroup];
                }];
            };
           return cell;
        }
        
        GroupOfDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupOfDataTableViewCell"];
        if (!cell) {
            cell = [[GroupOfDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupOfDataTableViewCell"];
        }
        cell.delegate = self;
        cell.index = indexPath.row;
        if (indexPath.row == 1) {
            cell.swi.hidden = YES;
            cell.titleLabel.text = @"群介绍";
            cell.contentLabel.text = self.groupDataModel.clusterIntro && self.groupDataModel.clusterIntro.length > 0 ? self.groupDataModel.clusterIntro : @"设置群简介，不能少于15个字";
            [cell whenTapped:^{
                if (self.myInfoModel.identity != 2) {
                    return;
                }
                GroupToIntroduceViewController *vc = [[GroupToIntroduceViewController alloc]init];
                vc.ID = self.ID;
                [self.navigationController pushViewController:vc animated:YES];
            }];
            
        }else if (indexPath.row == 2){
            cell.contentLabel.hidden = YES;
            cell.swi.hidden = YES;
            cell.titleLabel.text = @"公告历史";
                
            [cell whenTapped:^{
                GroupAnnouncementViewController *vc = [[GroupAnnouncementViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }];
            
        }else if (indexPath.row == 4){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.titleLabel.text = @"置顶该群";
//            cell.swi.on = self.groupDataModel.isTop == 1;
            BOOL isTop = GetCache(ISTopKey(self.ID));
            cell.swi.on = isTop;
        }else if (indexPath.row == 5){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.titleLabel.text = @"消息免打扰";
            cell.swi.on = self.groupDataModel.isNoDisturbing == 1;
        }else if (indexPath.row == 6){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.titleLabel.text = @"消息屏蔽";
            cell.swi.on = self.groupDataModel.isShield == 1;
        }else if (indexPath.row == 8){
            cell.arrowImageV.hidden = YES;
            cell.contentLabel.hidden = YES;
            cell.swi.hidden = YES;
            cell.titleLabel.text = @"删除聊天记录";
            cell.titleLabel.textColor = RGBA(116, 92, 240, 1);
        }
        cell.backgroundColor = UIColor.whiteColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}
@end
