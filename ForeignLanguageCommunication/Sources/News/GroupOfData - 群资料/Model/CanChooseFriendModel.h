//
//  CanChooseFriendModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CanChooseFriendModel : NSObject
@property (nonatomic, assign) long Id;          // 用户id
@property (nonatomic, copy) NSString *nickname; // 昵称
@property (nonatomic, assign) long sex;         // 性别(0：女，1：男)
@property (nonatomic, copy) NSString *sysId;    // 系统id
@property (nonatomic, copy) NSString *url;      // 头像
@property (nonatomic, assign) BOOL isSelect;    

@end

NS_ASSUME_NONNULL_END
