//
//  GroupOfDataModel.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "GroupOfDataModel.h"

@implementation GroupOfDataModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"clusterMember": [GroupUserModel class]};
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"ID":@"id"};
}
@end
