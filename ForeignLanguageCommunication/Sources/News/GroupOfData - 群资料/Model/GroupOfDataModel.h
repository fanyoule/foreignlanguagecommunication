//
//  GroupOfDataModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import <Foundation/Foundation.h>
#import "GroupUserModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupOfDataModel : NSObject

@property (nonatomic, copy) NSString *auditAnswer;  // 入群验证答案
@property (nonatomic, copy) NSString *auditQuestion;// 入群验证问题
@property (nonatomic, copy) NSString *avatarUrl;    // 群头像
@property (nonatomic, assign) long chair;           // 是否开启主席模式（0关闭，1开启） 管理员能说话，其他人说不了话
@property (nonatomic, copy) NSString *clusterIntro; // 群介绍
@property (nonatomic, copy) NSString *clusterName;  // 群名
@property (nonatomic, copy) NSString *createTime;   // 创建时间
@property (nonatomic, assign) BOOL isAdmin;         // 是否管理员
@property (nonatomic, assign) BOOL isMaster;        // 是否群主
@property (nonatomic, assign) long isNoDisturbing;  // 是否免打扰：0关闭 1开启
@property (nonatomic, assign) long isShield;        // 是否屏蔽：0关闭 屏蔽 1开启 不屏蔽
@property (nonatomic, assign) long isTop;           // 是否置顶：0不置顶 1置顶
@property (nonatomic, assign) long masterId;        // 创建者id
@property (nonatomic, assign) long peopleCount;     // 群人数
@property (nonatomic, assign) long silenceCount;    // 禁言人数
@property (nonatomic, assign) long userId;          // 当前用户id
@property (nonatomic, assign) long avatar;          // 群头像id
@property (nonatomic, assign) long clusterSysId;    // 群组系统id
@property (nonatomic, assign) long audit;           // 入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题）
@property (nonatomic, assign) long ID;              // 群id

@property (nonatomic, strong) NSMutableArray<GroupUserModel *> *clusterMember; // 群成员列表

@end

NS_ASSUME_NONNULL_END
