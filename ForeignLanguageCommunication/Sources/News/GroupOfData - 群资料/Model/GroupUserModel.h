//
//  GroupUserModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/2/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupUserModel : NSObject

@property (nonatomic, copy) NSString *clusterAuditInfo; // 入群审核内容
@property (nonatomic, copy) NSString *createTime;       // 创建时间
@property (nonatomic, copy) NSString *nickname;         // 昵称
@property (nonatomic, copy) NSString *url;              // 头像
@property (nonatomic, assign) BOOL isMaster;            // 是否群主
@property (nonatomic, assign) BOOL isMe;                // 是否自己
@property (nonatomic, assign) BOOL isSilence;           // 是否禁言
@property (nonatomic, assign) long admin;               // 是否是管理员（0普通成员，1管理员）
@property (nonatomic, assign) long clusterId;           // 群id
@property (nonatomic, assign) long status;              // 是否通过审核 0：已审核 1：待审核
@property (nonatomic, assign) long userId;              // 用户id




@end

NS_ASSUME_NONNULL_END
