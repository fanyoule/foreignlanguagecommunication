//
//  CurrentGroupUserinfoModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/2/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CurrentGroupUserinfoModel : NSObject
@property (nonatomic, assign) long chair;           // 公会是否开启主席模式（0关闭，1开启）
@property (nonatomic, assign) long identity;        // 用户身份（0普通成员，1管理员，2群主）
@property (nonatomic, assign) long stopTalk;        // 用户是否被禁言（0无，1禁）
@property (nonatomic, copy) NSString *clusterName;  // 群名称
@end

NS_ASSUME_NONNULL_END
