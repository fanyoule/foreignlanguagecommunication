//
//  NoticeModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/2/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoticeModel : NSObject
@property (nonatomic, copy) NSString *avatar;       // 用户头像
@property (nonatomic, assign) long clusterId;       // 群id
@property (nonatomic, copy) NSString *content;      // 公告内容
@property (nonatomic, copy) NSString *createTime;   // 创建时间
@property (nonatomic, assign) long Id;              //
@property (nonatomic, assign) long idStick;         // 是否置顶：1置顶，0不置顶
@property (nonatomic, copy) NSString *nickname;     // 用户名称
@property (nonatomic, copy) NSString *sign;         // 用户签名
@property (nonatomic, copy) NSString *status;       // 用户状态 0：封禁中 1：解封中 null 无封禁记录
@property (nonatomic, copy) NSString *sysid;        // 用户编号
@property (nonatomic, copy) NSString *title;        // 公告标题
@property (nonatomic, assign) long userId;          // 发布人Id
@end

NS_ASSUME_NONNULL_END
