//
//  GroupOfDataHeadView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "GroupOfDataHeadView.h"
@interface GroupOfDataHeadView ()
/** 群头像*/
@property (nonatomic, strong)UIImageView *headImageV;
/** 群名*/
@property (nonatomic, strong)UILabel *nameLabel;

/** 群id*/
@property (nonatomic, strong)UILabel *groupIDLabel;
/** 群成员标题*/
@property (nonatomic, strong)UILabel *titleLabel;
/** 人数*/
@property (nonatomic, strong)UILabel *numberLabel;
/** 箭头*/
@property (nonatomic, strong)UIImageView *arrowImageV;
/** 邀请图片*/
@property (nonatomic, strong)UIImageView *invitationImageV;
/** 邀请文字*/
@property (nonatomic, strong)UILabel *invitationLabel;
/** 线*/
@property (nonatomic, strong)UIView *lineView;
/** 查看所有成员按钮*/
@property (nonatomic, strong)UIButton *membersBtn;
/** 添加好友*/
@property (nonatomic, strong)UIButton *addBuddiesBtn;
@property (nonatomic, strong) UIView *bgView;
@end
@implementation GroupOfDataHeadView


-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self addSubview:self.headImageV];
    [self addSubview:self.nameLabel];
    [self addSubview:self.editorBtn];
    [self addSubview:self.groupIDLabel];
    [self addSubview:self.lineView];
    [self addSubview:self.titleLabel];
    [self addSubview:self.numberLabel];
    [self addSubview:self.arrowImageV];
    [self addSubview:self.invitationImageV];
    [self addSubview:self.invitationLabel];
    [self addSubview:self.membersBtn];
    [self addSubview:self.addBuddiesBtn];
    [self addSubview:self.bgView];
    [self.headImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self).offset(15);
        make.height.width.equalTo(@50);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headImageV).offset(5);
        make.left.equalTo(self.headImageV.mas_right).offset(10);
    }];
    [self.editorBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel.mas_right).offset(2);
        make.top.equalTo(self.nameLabel);
    }];
    [self.groupIDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.headImageV).offset(-2);
        make.left.equalTo(self.headImageV.mas_right).offset(10);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headImageV.mas_bottom).offset(5);
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.height.equalTo(@0.5);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lineView.mas_bottom).offset(10);
        make.left.equalTo(self).offset(15);
    }];
    [self.arrowImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-15);
        make.centerY.equalTo(self.titleLabel);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.arrowImageV);
        make.right.equalTo(self.arrowImageV.mas_left);
    }];
    
    [self.invitationImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.width.height.equalTo(@55);
        make.left.equalTo(self).offset(10);
    }];
    [self.invitationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.invitationImageV.mas_bottom).offset(10);
        make.centerX.equalTo(self.invitationImageV);
    }];
    [self.membersBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.right.equalTo(self.arrowImageV);
        make.height.equalTo(@30);
        make.centerY.equalTo(self.numberLabel);
    }];
    [self.addBuddiesBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.invitationImageV);
        make.bottom.equalTo(self.invitationLabel);
    }];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.invitationImageV);
        make.height.equalTo(@100);
        make.left.equalTo(self.invitationImageV.mas_right);
        make.right.equalTo(self.mas_right);
    }];
    
}

- (void)setModel:(GroupOfDataModel *)model{
    if (!model) {
        return;
    }
    
    [self.bgView removeAllSubviews];
    self.numberLabel.text = [NSString stringWithFormat:@"%ld人", model.clusterMember.count];
    [self.headImageV sd_setImageWithURL:[NSURL URLWithString:model.avatarUrl]];
    self.nameLabel.text = model.clusterName;
    self.groupIDLabel.text = [NSString stringWithFormat:@"群ID:%ld", model.ID];
    
    CGFloat w = (KSW - 60) / 5;
    for (int i = 0 ; i < model.clusterMember.count; i++) {
        if (i > 3) {
            return;
        }
        GroupUserModel *user = model.clusterMember[i];
        UIButton *btn = [[UIButton alloc]init];
        btn.backgroundColor = UIColor.clearColor;
        [btn whenTapped:^{
            [self transmission:user];
        }];
        
        [self.bgView addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(w));
            make.left.equalTo(self.bgView).offset(i*w + i*10 + 10);
            make.height.equalTo(@100);
            make.top.equalTo(@0);
        }];
        
        UIImageView *imageV = [[UIImageView alloc]init];
        [imageV sd_setImageWithURL:[NSURL URLWithString:user.url] placeholderImage:kImage(@"测试头像")];
        imageV.layer.cornerRadius = 6;
        imageV.clipsToBounds = YES;
        [btn addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(btn);
            make.width.height.equalTo(@55);
            make.centerX.equalTo(btn);
        }];
        
        UILabel *label = [[UILabel alloc]init];
        label.text = user.nickname;
        label.textColor = RGBA(24, 24, 24, 1);
        label.font = kFont(14);
        label.textAlignment = 1;
        [btn addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(imageV.mas_bottom).offset(10);
            make.centerX.equalTo(btn);
        }];
        
        UILabel *managerLabel = [[UILabel alloc]init];
        managerLabel.text = @"群主";
        managerLabel.textColor = RGBA(255, 255, 255, 1);
        managerLabel.font = kFont(10);
        managerLabel.textAlignment = 1;
        managerLabel.backgroundColor = RGBA(52, 120, 245, 1);
        managerLabel.layer.cornerRadius = 15/2;
        managerLabel.clipsToBounds = YES;
        [btn addSubview:managerLabel];
        [managerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(imageV.mas_bottom);
            make.width.equalTo(@25);
            make.height.equalTo(@15);
            make.centerX.equalTo(btn);
        }];
        if (user.isMaster == YES) {
            managerLabel.hidden = NO;
        }else{
            managerLabel.hidden = YES;
        }
    }
}
/** 邀请好友*/
- (void)addBuddies{
    if (self.addFriends) {
        self.addFriends();
    }
}
/** 查看个人资料*/
- (void)transmission:(GroupOfDataModel *)model {
    if (self.clickMembers) {
        self.clickMembers(model);
    }
}
/** 编辑*/
- (void)clickEditor{
    
    if (self.groupName) {
        self.groupName();
    }
}
/** 查看好友成员*/
- (void)membersOfThe{
    if (self.theMembers) {
        self.theMembers();
    }
}


- (UIImageView *)headImageV{
    if (!_headImageV) {
        _headImageV = [[UIImageView alloc]init];
        _headImageV.image = kImage(@"测试头像");
        _headImageV.layer.cornerRadius = 6;
        _headImageV.clipsToBounds = YES;
    }
    return _headImageV;
}
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.backgroundColor = UIColor.clearColor;
    }
    return _bgView;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"群名字";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Medium(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UIButton *)editorBtn{
    if (!_editorBtn) {
        _editorBtn = [[UIButton alloc]init];
        [_editorBtn setTitle:@"(编辑)" forState:(UIControlStateNormal)];
        [_editorBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
        _editorBtn.titleLabel.font = kFont(12);
        [_editorBtn addTarget:self action:@selector(clickEditor) forControlEvents:(UIControlEventTouchDown)];
    }
    return _editorBtn;
}
- (UILabel *)groupIDLabel{
    if (!_groupIDLabel) {
        _groupIDLabel = [[UILabel alloc]init];
        _groupIDLabel.text = @"群组ID:";
        _groupIDLabel.textColor = RGBA(153, 153, 153, 1);
        _groupIDLabel.font = kFont(14);
        _groupIDLabel.textAlignment = 0;
    }
    return _groupIDLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"群成员";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont(15);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}
- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.text = @"0人";
        _numberLabel.textColor = RGBA(153, 153, 153, 1);
        _numberLabel.font = kFont(14);
        _numberLabel.textAlignment = 2;
    }
    return _numberLabel;
}
- (UIImageView *)arrowImageV{
    if (!_arrowImageV) {
        _arrowImageV = [[UIImageView alloc]init];
        _arrowImageV.image = [UIImage imageNamed:@"个人资料-箭头"];
    }
    return _arrowImageV;
}

- (UIImageView *)invitationImageV{
    if (!_invitationImageV) {
        _invitationImageV = [[UIImageView alloc]init];
        _invitationImageV.image = [UIImage imageNamed:@"群资料-邀请"];
    }
    return _invitationImageV;
}

- (UILabel *)invitationLabel{
    if (!_invitationLabel) {
        _invitationLabel = [[UILabel alloc]init];
        _invitationLabel.text = @"邀请";
        _invitationLabel.textColor = RGBA(153, 153, 153, 1);
        _invitationLabel.font = kFont(14);
        _invitationLabel.textAlignment = 1;
    }
    return _invitationLabel;
}

- (UIButton *)membersBtn{
    if (!_membersBtn) {
        _membersBtn = [[UIButton alloc]init];
        _membersBtn.backgroundColor = UIColor.clearColor;
        [_membersBtn addTarget:self action:@selector(membersOfThe) forControlEvents:(UIControlEventTouchDown)];
    }
    return _membersBtn;
}

- (UIButton *)addBuddiesBtn{
    if (!_addBuddiesBtn) {
        _addBuddiesBtn = [[UIButton alloc]init];
        
        _addBuddiesBtn.backgroundColor = UIColor.clearColor;
        [_addBuddiesBtn addTarget:self action:@selector(addBuddies) forControlEvents:(UIControlEventTouchDown)];
    }
    return _addBuddiesBtn;
}

@end
