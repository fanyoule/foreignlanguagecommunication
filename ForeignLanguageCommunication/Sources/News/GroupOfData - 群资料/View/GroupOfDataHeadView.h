//
//  GroupOfDataHeadView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import <UIKit/UIKit.h>
#import "GroupOfDataModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^clickChangeGroupName)(void);
typedef void(^clickCheckTheMembers)(void);
typedef void(^clickMembersTheHead)(GroupUserModel *model);
typedef void(^clickInviteFriends)(void);

@interface GroupOfDataHeadView : UIView

@property (nonatomic, strong)GroupOfDataModel *model;

@property (nonatomic, strong)clickChangeGroupName groupName;
@property (nonatomic, strong)clickCheckTheMembers theMembers;
@property (nonatomic, strong)clickMembersTheHead clickMembers;
@property (nonatomic, strong)clickInviteFriends addFriends;

/** 编辑*/
@property (nonatomic, strong)UIButton *editorBtn;
@end

NS_ASSUME_NONNULL_END
