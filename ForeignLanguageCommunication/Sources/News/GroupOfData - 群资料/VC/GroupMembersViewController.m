//
//  GroupMembersViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "GroupMembersViewController.h"
#import "GroupMembersTableViewCell.h"
#import "YKSearchTextField.h"
#import "ChooseFriendViewController.h"// 邀请好友
#import "GroupOfDataModel.h"
#import "MyPersonalInformationVC.h"

@interface GroupMembersViewController ()<UITableViewDelegate,UITableViewDataSource,SearchViewDelegate, UISearchBarDelegate> {
    BOOL isSearch;
}
@property (nonatomic, strong) GroupOfDataModel *groupDataModel;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray *searchArr;
@property (nonatomic, strong) NSMutableArray *dataArr;
@end

@implementation GroupMembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isSearch = NO;
    self.searchArr = [NSMutableArray array];
    self.dataArr = [NSMutableArray array];
    self.navTitleString = @"群成员";
    [self addRightBtnWith:@"添加"];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60)];
    self.searchBar.placeholder = @"搜索";
    self.mainTableView.tableHeaderView = self.searchBar;
    self.mainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.searchBar.delegate = self;
    // Do any additional setup after loading the view.
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchArr = [NSMutableArray array];
    if (searchText && searchText.length > 0) {
        isSearch = YES;
        for (GroupUserModel *model in self.dataArr) {
            if ([model.nickname containsString:searchText]) {
                [self.searchArr addObject:model];
            }
        }
    } else {
        isSearch = NO;
    }
    [self.mainTableView reloadData];
}
- (void)getData {
    kWeakSelf(self)
    [RequestManager getPersonalClusterById:[UserInfoManager shared].getUserID Id:self.ID withSuccess:^(id  _Nullable response) {
        weakself.groupDataModel = [GroupOfDataModel mj_objectWithKeyValues:response[@"data"]];
        weakself.dataArr = weakself.groupDataModel.clusterMember;
        [weakself.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)rightClick:(UIButton *)sender{
    ChooseFriendViewController *vc = [[ChooseFriendViewController alloc]init];
    vc.ID = self.ID;
    [self.navigationController pushViewController:vc animated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isSearch) {
        return self.searchArr.count;
    } else {
        return self.dataArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupMembersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupMembersTableViewCell"];
    if (!cell) {
        cell = [[GroupMembersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupMembersTableViewCell"];
    }
    cell.backgroundColor = UIColor.whiteColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    GroupUserModel *model = isSearch ? self.searchArr[indexPath.row] : self.dataArr[indexPath.row];
    if (model.isMaster) {
        cell.positionLabel.hidden = NO;
    } else {
        cell.positionLabel.hidden = YES;
    }
    cell.nameLabel.text = model.nickname;
    [cell.avataImageV sd_setImageWithURL:model.url.mj_url];
    cell.moreBtn.hidden = YES;
    if (model.userId == [UserInfoManager shared].getUserID) {
        cell.meLab.hidden = NO;
    } else {
        cell.meLab.hidden = YES;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupUserModel *model = isSearch ? self.searchArr[indexPath.row] : self.dataArr[indexPath.row];
    MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc] init];
    vc.ID = model.userId;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
