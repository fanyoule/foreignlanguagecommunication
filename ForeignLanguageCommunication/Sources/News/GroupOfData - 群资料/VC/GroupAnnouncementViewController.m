//
//  GroupAnnouncementViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "GroupAnnouncementViewController.h"
#import "GroupAnnouncementTableViewCell.h"
#import "ReleaseAnnouncementViewController.h"

@interface GroupAnnouncementViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArr;
@end

@implementation GroupAnnouncementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArr = [NSMutableArray array];
    self.navTitleString = @"群公告";
    if (self.identity == 2) {
        [self addRightBtnWith:@"发布"];
    }
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
}
- (void)getData {
    [self showSVP];
    kWeakSelf(self)
    [RequestManager getPersonalClusterNoticeWithId:self.ID withSuccess:^(id  _Nullable response) {
        weakself.dataArr = [NoticeModel mj_objectArrayWithKeyValuesArray:response[@"data"]];
        [weakself dissSVP];
        [weakself.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}

- (void)rightClick:(UIButton *)sender{
    ReleaseAnnouncementViewController *vc = [[ReleaseAnnouncementViewController alloc]init];
    vc.ID = self.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupAnnouncementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupAnnouncementTableViewCell"];
    if (!cell) {
        cell = [[GroupAnnouncementTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupAnnouncementTableViewCell"];
    }
    cell.backgroundColor = UIColor.whiteColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.dataArr[indexPath.row];
    cell.index = indexPath.row;
    kWeakSelf(self);
    cell.block = ^(NSInteger index) {
        [weakself moreBtnClick:index];
    };
    if (self.identity == 2) {
        cell.moreBtn.hidden = NO;
    } else {
        cell.moreBtn.hidden = YES;
    }
    return cell;
}
- (void)moreBtnClick:(NSInteger)row {
    NoticeModel *model = self.dataArr[row];
    NSString *title = model.idStick == 1 ? @"取消置顶" : @"置顶";
    [self alertView:@"" msg:@"" btnArrTitles:@[title, @"删除"] showCacel:YES style:UIAlertControllerStyleActionSheet block:^(int index) {
        if (index == 0) {
            [self setTop:row];
        } else {
            [self deletNotice:row];
        }
    }];
}
- (void)setTop:(NSInteger)index {
    NoticeModel *model = self.dataArr[index];
    int value = model.idStick == 1 ? 0 : 1;
    [self showSVP];
    kWeakSelf(self)
    [RequestManager updatePersonalClusterNoticeId:model.Id userId:model.userId idStick:value title:model.title content:model.content withSuccess:^(id  _Nullable response) {
        [weakself showText:@"设置成功"];
        [weakself getData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)deletNotice:(NSInteger)index {
    NoticeModel *model = self.dataArr[index];
    [self showSVP];
    kWeakSelf(self)
    [RequestManager deletePersonalClusterNoticeById:model.Id withSuccess:^(id  _Nullable response) {
        [weakself showText:@"删除成功"];
        [weakself.dataArr removeObject:model];
        [weakself.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
@end
