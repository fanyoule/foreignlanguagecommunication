//
//  ChooseFriendViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "BaseViewController.h"
#import "GroupOfDataModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChooseFriendViewController : BaseViewController
@property (nonatomic, strong) GroupOfDataModel *groupDataModel;
@property (nonatomic, assign) long ID;
@end

NS_ASSUME_NONNULL_END
