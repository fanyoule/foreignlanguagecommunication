//
//  BannedListVC.m
//  VoiceLive
//
//  Created by 申修智 on 2020/11/17.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "BannedListVC.h"
#import "YKSearchTextField.h"
#import "MembersBannedTableViewCell.h"
#import "MyPersonalInformationVC.h"// 个人资料
@interface BannedListVC ()<UITableViewDelegate,UITableViewDataSource,SearchViewDelegate>
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)NSMutableArray *dataArray;//列表数据源

@property(nonatomic, strong)NSArray *tempArray;
@property (nonatomic) int page;

@end

@implementation BannedListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.navTitleLabel.text = @"禁言列表";
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.and.bottom.and.right.equalTo(self.view);
    }];
    kWeakSelf(self)
    self.tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        weakself.page = 1;
        [weakself getData];
    }];
    self.tableView.mj_footer = [MJRefreshAutoGifFooter footerWithRefreshingBlock:^{
        [weakself getData];
    }];
    self.tableView.mj_footer.hidden = YES;
    // Do any additional setup after loading the view.
    [self getData];
}

- (void)getData {
    kWeakSelf(self)
    [RequestManager getRoomForbiddenList:self.ID pageNum:self.page pageSize:20 nickname:[UserInfoManager shared].getUserNickName withSuccess:^(id  _Nullable response) {
        int lastPage = [response[@"data"][@"lastPage"] intValue];
        NSArray *tempArr = [TheBlacklistModel mj_objectArrayWithKeyValuesArray:response[@"data"][@"list"]];
        if (weakself.page == 1) {
            [weakself.dataArray removeAllObjects];
        }
        [weakself stopLoading:!(weakself.page < lastPage)];
        weakself.page++;
        [weakself.dataArray addObjectsFromArray:tempArr];
        if (weakself.dataArray.count > 0) {
            weakself.tableView.mj_footer.hidden = NO;
        }
        [weakself.tableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself stopLoading:NO];
    }];
}
- (void)stopLoading:(BOOL)isNoData {
    if ([self.tableView.mj_header isRefreshing]) {
        [self.tableView.mj_header endRefreshing];
        if (!isNoData) {
            self.tableView.mj_footer.state = MJRefreshStateIdle;
        }
    }
    if ([self.tableView.mj_footer isRefreshing]) {
        if (isNoData) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.tableView.mj_footer endRefreshing];
        }
    }
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]init];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    }
    return _tableView;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


////分区头
//- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//   
//    UIView *contentView = UIView.new;
//    contentView.backgroundColor = RGBA(255, 255, 255, 1);
//
//    YKSearchTextField *textField = [[YKSearchTextField alloc]init];
//    textField.frame = CGRectMake(15, 10, APPSIZE.width-30, 35);
//    textField.placeHolder = @"输入昵称";
//    textField.placeHolderPosition = SearchViewPlaceHolderPositionCenter;
//    textField.layer.cornerRadius = 35/2;
//    textField.delegate = self;
//    [contentView addSubview:textField];
//    
//    return contentView;
//    
//}
//
////分区头高度
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//    return 60;
//    
//}
//返回多少分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}

// 返回多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
//    return 5;
    
}

// cell 的高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    return 64;


}

// 放入cell的地方
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    static NSString *ident = @"MembersBannedTableViewCell";
    MembersBannedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ident];
    if (!cell) {
        cell = [[MembersBannedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ident];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    }
    cell.model = self.dataArray[indexPath.row];
    [cell.removeBtn whenTapped:^{
        
    }];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)searchViewDIdChangeText:(UITextField *)textField{
    NSLog(@"输入的字 === %@",textField.text);
    if (textField.text.length <= 0) {
        [self.dataArray removeAllObjects];
//        [self getData];
    }else{
        [self.mainTableView reloadData];
    }
}

@end
