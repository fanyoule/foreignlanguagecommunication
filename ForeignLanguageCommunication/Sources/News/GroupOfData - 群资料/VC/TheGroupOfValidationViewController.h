//
//  TheGroupOfValidationViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/3.
//

#import "BaseViewController.h"
#import "GroupOfDataModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TheGroupOfValidationViewController : BaseViewController
@property (nonatomic, strong) GroupOfDataModel *groupDataModel;
@end

NS_ASSUME_NONNULL_END
