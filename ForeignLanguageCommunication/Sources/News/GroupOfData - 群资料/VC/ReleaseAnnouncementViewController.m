//
//  ReleaseAnnouncementViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/3.
//

#import "ReleaseAnnouncementViewController.h"

@interface ReleaseAnnouncementViewController ()<UITextFieldDelegate,UITextViewDelegate>

@property (nonatomic, strong)UIView *backView1;
@property (nonatomic, strong)UIView *backView2;

@property (nonatomic, strong)UITextField *textField;
@property (nonatomic, strong)UILabel *titleLimitLabel;

@property (nonatomic, strong)UITextView *textView;
@property (nonatomic, strong)UILabel *promptLabel;
@property (nonatomic, strong)UILabel *contentLimitLabel;

@end

@implementation ReleaseAnnouncementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"发布公告";
    
    [self addRightBtnWith:@"完成"];
    [self.rightBtn setTitleColor:RGBA(24, 24, 24, 1) forState:(UIControlStateNormal)];
    
    [self.view addSubview:self.backView1];
    [self.view addSubview:self.backView2];
    [self.view addSubview:self.textField];
    [self.view addSubview:self.titleLimitLabel];
    [self.view addSubview:self.textView];
    [self.view addSubview:self.promptLabel];
    [self.view addSubview:self.contentLimitLabel];
    
    [self.backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@80);
    }];
    [self.backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@50);
        make.centerY.equalTo(self.backView1);
        make.left.right.equalTo(self.backView1);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.right.equalTo(self.backView2);
        make.left.equalTo(self.backView2).offset(15);
    }];
    [self.titleLimitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView2);
        make.right.equalTo(self.backView2).offset(-10);
    }];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView1.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@200);
    }];
    [self.promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView).offset(10);
        make.left.equalTo(self.textView).offset(15);
    }];
    [self.contentLimitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(10);
        make.right.equalTo(self.view).offset(-10);
    }];
}
- (void)rightClick:(UIButton *)sender {
    if (!_textField.text || _textField.text.length <= 0) {
        [self showText:@"请输入公告标题"];
        return;
    }
    if (!_textView.text || _textView.text.length <= 0) {
        [self showText:@"请输入公告内容"];
        return;
    }
    kWeakSelf(self)
    [self showSVP];
    [RequestManager addPersonalClusterNoticeWithId:self.ID userId:[UserInfoManager shared].getUserID idStick:0 title:_textField.text content:_textView.text withSuccess:^(id  _Nullable response) {
        [weakself showText:@"发布成功"];
        [weakself.navigationController popViewControllerAnimated:YES];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}

- (UIView *)backView1{
    if (!_backView1) {
        _backView1 = [[UIView alloc]init];
        _backView1.backgroundColor = RGBA(245, 245, 245, 1);
    }
    return _backView1;
}
- (UIView *)backView2{
    if (!_backView2) {
        _backView2 = [[UIView alloc]init];
        _backView2.backgroundColor = UIColor.whiteColor;
    }
    return _backView2;
}
- (UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc]init];
//        _textField.keyboardType = UIKeyboardTypeDecimalPad;
        _textField.backgroundColor = UIColor.whiteColor;
        _textField.placeholder = @"请输入标题（必填）";
        _textField.font = kFont_Regular(16);
        _textField.delegate = self;
        
        [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    }
    return _textField;
}
- (UILabel *)titleLimitLabel{
    if (!_titleLimitLabel) {
        _titleLimitLabel = [[UILabel alloc]init];
        _titleLimitLabel.text = @"0/10";
        _titleLimitLabel.textColor = RGBA(153, 153, 153, 1);
        _titleLimitLabel.font = kFont(12);
        _titleLimitLabel.textAlignment = 2;
    }
    return _titleLimitLabel;
}
- (UITextView *)textView {
    if (!_textView) {
        _textView = UITextView.new;
        
        _textView.font = kFont_Regular(16);
        _textView.backgroundColor = UIColor.whiteColor;
        _textView.delegate = self;
//        _textView.contentInset = UIEdgeInsetsMake(2, 15, 5, 5);
//        _textView.textContainerInset = UIEdgeInsetsMake(5, 16, 5, 6);
        _textView.textContainerInset = UIEdgeInsetsMake(10, 11, 10, 11);
        _textView.showsHorizontalScrollIndicator = NO;
        
    }
    return _textView;
}
- (UILabel *)promptLabel{
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc]init];
        _promptLabel.text = @"请输入要发布的公告内容（必填）";
        _promptLabel.textColor = RGBA(153, 153, 153, 1);
        _promptLabel.font = kFont(16);
        _promptLabel.textAlignment = 0;
    }
    return _promptLabel;
}

- (UILabel *)contentLimitLabel{
    if (!_contentLimitLabel) {
        _contentLimitLabel = [[UILabel alloc]init];
        _contentLimitLabel.text = @"0/500";
        _contentLimitLabel.textColor = RGBA(153, 153, 153, 1);
        _contentLimitLabel.font = kFont(12);
        _contentLimitLabel.textAlignment = 2;
    }
    return _contentLimitLabel;
}

-(void)textViewDidChange:(UITextView*)textView
{
    if([textView.text length] == 0){
        self.promptLabel.hidden = NO;
    }else{
        self.promptLabel.hidden = YES;//这里给空
    }
    self.contentLimitLabel.text = [NSString stringWithFormat:@"%ld/500",textView.text.length];
    
}
//设置超出最大字数即不可输入 也是textview的代理方法
-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text{
    if ([text isEqualToString:@"\n"]) {     //这里"\n"对应的是键盘的 return 回收键盘之用
        
       [textView resignFirstResponder];

       return YES;

    }
    if (range.location >= 500){
     
            return NO;

    }else{
       return YES;

    }
    

}

- (void)textFieldDidChange:(UITextField *)textField {
    int kMaxLength = 10;
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UITextInputMode currentInputMode] primaryLanguage]; // 键盘输入方式
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        else{
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
    self.titleLimitLabel.text = [NSString stringWithFormat:@"%ld/10",textField.text.length];
    
}

@end
