//
//  GroupNameViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "GroupNameViewController.h"

@interface GroupNameViewController ()<UITextViewDelegate>

@property (nonatomic, strong)UITextView *textView;

@property (nonatomic, strong)UILabel *placeholderLabel;

@property (nonatomic, strong)UILabel *promptLabel;
@end

@implementation GroupNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"群名称";
    [self addRightBtnWith:@"完成"];
    [self.rightBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    [self.view addSubview:self.textView];
    [self.view addSubview:self.placeholderLabel];
    [self.view addSubview:self.promptLabel];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@150);
    }];
    [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView).offset(15);
        make.left.equalTo(self.textView).offset(18);
    }];
    [self.promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(15);
        make.right.equalTo(self.textView);
    }];
    // Do any additional setup after loading the view.
}
- (void)rightClick:(UIButton *)sender {
    if (!_textView.text || _textView.text.length <= 0) {
        [self showText:@"请输入群昵称"];
        return;
    }
    [self showSVP];
    kWeakSelf(self)
    [RequestManager updatePersonalClusterWithId:self.ID userId:[UserInfoManager shared].getUserID clusterName:_textView.text withSuccess:^(id  _Nullable response) {
        [weakself showText:@"修改成功"];
        [weakself.navigationController popViewControllerAnimated:YES];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.backgroundColor = UIColor.whiteColor;
        _textView.font = kFont(16);
        _textView.layer.cornerRadius = 12;
        _textView.clipsToBounds = YES;
        _textView.textContainerInset = UIEdgeInsetsMake(15, 15, 15, 15);
        _textView.delegate = self;
    }
    return _textView;
}
- (UILabel *)placeholderLabel{
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc]init];
        _placeholderLabel.text = @"写下你的群名称哦！";
        _placeholderLabel.textColor = RGBA(153, 153, 153, 1);
        _placeholderLabel.font = kFont(16);
        _placeholderLabel.textAlignment = 2;
    }
    return _placeholderLabel;
}
- (UILabel *)promptLabel{
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc]init];
        _promptLabel.text = @"0/15";
        _promptLabel.textColor = RGBA(0, 122, 255, 1);
        _promptLabel.font = kFont(12);
        _promptLabel.textAlignment = 2;
    }
    return _promptLabel;
}
- (void)textViewDidChange:(UITextView *)textView{

    self.placeholderLabel.hidden = YES;
     
        //实时显示字数
    self.promptLabel.text = [NSString stringWithFormat:@"%lu/15", (unsigned long)textView.text.length];
    
    
    //字数限制操作
    if (textView.text.length >= 15) {
        [self showText:@"最多不超过十五个字"];
        textView.text = [textView.text substringToIndex:10];
        self.promptLabel.text = @"10/15";
    }
    
    //取消按钮点击权限，并显示提示文字
    if (textView.text.length == 0) {

    self.placeholderLabel.hidden = NO;
        
    }
}

@end
