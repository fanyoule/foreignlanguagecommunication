//
//  ChooseFriendViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "ChooseFriendViewController.h"
#import "ChooseFriendTableViewCell.h"


@interface ChooseFriendViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *selectArr;
@end

@implementation ChooseFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArr    = [NSMutableArray array];
    self.selectArr  = [NSMutableArray array];
    self.navTitleString = @"选择好友";
    [self addRightBtnWith:@"完成"];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    
    // Do any additional setup after loading the view.
    [self getdata];
}
- (void)rightClick:(UIButton *)sender {
    if (self.dataArr.count <= 0) {
        [self showText:@"暂无可邀请的好友"];
        return;
    }
    NSString *userIds = @"";
    for (int i = 0; i < self.dataArr.count; i++) {
        CanChooseFriendModel *model = self.dataArr[i];
        if (model.isSelect) {
            if (userIds.length == 0) {
                userIds = [NSString stringWithFormat:@"%ld", model.Id];
            } else {
                userIds = [NSString stringWithFormat:@"%@,%ld", userIds, model.Id];
            }
        }
    }
    if (userIds.length <= 0) {
        [self showText:@"请选择好友"];
        return;
    }
    kWeakSelf(self)
    [self showSVP];
    [RequestManager inviteClusterWithId:self.ID userIds:userIds withSuccess:^(id  _Nullable response) {
        [weakself dissSVP];
        [weakself getdata];
        [weakself requstSendIm:[NSMutableDictionary dictionaryWithDictionary:@{@"content": userIds}]];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
-(void)requstSendIm:(NSMutableDictionary *)dic{
    dic[@"from"] = @([UserInfoManager shared].getUserID);
    dic[@"msgType"] = @(0);
    dic[@"chatType"] = @(4);
    dic[@"groupType"] = @(2);
    dic[@"group_id"] = @(self.ID);
    NSLog(@"dic====%@",dic);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: dic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //发送消息方法
    // [self sendMessage:str cmd:11];
    [[ChatIMSend share]sendMessage:str];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)getdata {
    kWeakSelf(self)
    [RequestManager getUserPerClusterAddListWithId:self.ID userId:[UserInfoManager shared].getUserID withSuccess:^(id  _Nullable response) {
        weakself.dataArr = [CanChooseFriendModel mj_objectArrayWithKeyValuesArray:response[@"data"]];
        [weakself.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChooseFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChooseFriendTableViewCell"];
    if (!cell) {
        cell = [[ChooseFriendTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChooseFriendTableViewCell"];
    }
    cell.backgroundColor = UIColor.whiteColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CanChooseFriendModel *model = self.dataArr[indexPath.row];
    if (model.isSelect) {
        model.isSelect = NO;
    } else {
        model.isSelect = YES;
    }
    [self.mainTableView reloadData];
}

@end
