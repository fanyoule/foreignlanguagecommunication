//
//  TheGroupOfValidationViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/3.
//  入群验证

#import "TheGroupOfValidationViewController.h"
#import "GroupOfDataTableViewCell.h"
#import "AnswerTheQuestionsTableViewCell.h"
#import "TheGrayBarTableViewCell.h"// 灰色的条"
@interface TheGroupOfValidationViewController ()<UITableViewDelegate,UITableViewDataSource, GroupOfDataTableViewCellDelegate>

@property (nonatomic, assign)NSInteger rows;

@property (nonatomic, copy) NSString *question;
@property (nonatomic, copy) NSString *answer;
@end

@implementation TheGroupOfValidationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.groupDataModel.audit == 0 || self.groupDataModel.audit == 1) {
        self.question = @"";
        self.answer = @"";
    } else {
        self.question = self.groupDataModel.auditQuestion ? self.groupDataModel.auditQuestion : @"";
        self.answer = self.groupDataModel.auditAnswer ? self.groupDataModel.auditAnswer : @"";
    }
    
    
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"入群验证";
    [self addRightBtnWith:@"完成"];
    
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.scrollEnabled = NO;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom).offset(10);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    self.rows = self.groupDataModel.audit;
}
- (void)rightClick:(UIButton *)sender {
    if (self.rows == 0 || self.rows == 1) {
        [self showSVP];
        kWeakSelf(self)
        [RequestManager updatePersonalClusterWithId:self.groupDataModel.ID userId:[UserInfoManager shared].getUserID audit:self.rows auditQuestion:@"" auditAnswer:@"" withSuccess:^(id  _Nullable response) {
            weakself.groupDataModel.audit = weakself.rows;
            [weakself showText:@"修改成功"];
            [weakself.navigationController popViewControllerAnimated:YES];
        } withFail:^(NSError * _Nullable error) {
            [weakself showText:error.localizedDescription];
        }];
    } else {
        if (self.question.length <= 0) {
            [self showText:@"请输入问题"];
            return;
        }
        if (self.answer.length <= 0) {
            [self showText:@"请输入答案"];
            return;
        }
        [self showSVP];
        kWeakSelf(self)
        [RequestManager updatePersonalClusterWithId:self.groupDataModel.ID userId:[UserInfoManager shared].getUserID audit:self.rows auditQuestion:self.question auditAnswer:self.answer withSuccess:^(id  _Nullable response) {
            weakself.groupDataModel.audit = weakself.rows;
            [weakself showText:@"修改成功"];
            [weakself.navigationController popViewControllerAnimated:YES];
        } withFail:^(NSError * _Nullable error) {
            [weakself showText:error.localizedDescription];
        }];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.rows == 2) {
        return 6;
    } else {
        return 3;
    }
}
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField.tag == 700) {
        self.question = textField.text;
    } else {
        self.answer = textField.text;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 4|| indexPath.row == 5) {
        AnswerTheQuestionsTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"AnswerTheQuestionsTableViewCell"];
        if (!cell) {
            cell = [[AnswerTheQuestionsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AnswerTheQuestionsTableViewCell"];
        }
        cell.backgroundColor = UIColor.whiteColor;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:(UIControlEventEditingChanged)];
        if (indexPath.row == 4) {
            cell.titleLabel.text = @"问题:";
            cell.textField.placeholder = @"请输入问题";
            cell.textField.text = self.question;
            cell.textField.tag = 700;
        } else {
            cell.titleLabel.text = @"答案:";
            cell.textField.placeholder = @"请输入答案";
            cell.textField.text = self.answer;
            cell.textField.tag = 701;
        }
        return cell;
        
    } else if (indexPath.row == 3){
        TheGrayBarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheGrayBarTableViewCell"];
        if (!cell) {
            cell = [[TheGrayBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheGrayBarTableViewCell"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    GroupOfDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupOfDataTableViewCell"];
    if (!cell) {
        cell = [[GroupOfDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupOfDataTableViewCell"];
    }
    cell.delegate = self;
    cell.index = indexPath.row;
    cell.backgroundColor = UIColor.whiteColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        cell.arrowImageV.hidden = YES;
        cell.contentLabel.hidden = YES;
        cell.titleLabel.text = @"允许任何人加群";
        cell.swi.on = self.rows == 0;
    }else if (indexPath.row == 1){
        cell.arrowImageV.hidden = YES;
        cell.contentLabel.hidden = YES;
        cell.titleLabel.text = @"需要发送验证消息";
        cell.swi.on = self.rows == 1;
    }else if (indexPath.row == 2){
        cell.arrowImageV.hidden = YES;
        cell.contentLabel.hidden = YES;
        cell.titleLabel.text = @"需要正确回答问题";
        cell.swi.on = self.rows == 2;
    }
    return cell;
}
- (void)switchClick:(NSInteger)index {
    if (index == 0 || index == 1) {
        self.question = @"";
        self.answer = @"";
    }
    self.rows = index;
    [self.mainTableView reloadData];
}

@end
