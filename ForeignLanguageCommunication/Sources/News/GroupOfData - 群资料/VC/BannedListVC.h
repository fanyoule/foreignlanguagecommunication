//
//  BannedListVC.h
//  VoiceLive
//
//  Created by 申修智 on 2020/11/17.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@protocol BannedListVCDelegate<NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)clickBannedListVC:(NSInteger)integer;

@end
@interface BannedListVC : BaseViewController

@property (nonatomic, assign) long ID;
@property (nonatomic, weak) id<BannedListVCDelegate> VCDelegate;
@end

NS_ASSUME_NONNULL_END
