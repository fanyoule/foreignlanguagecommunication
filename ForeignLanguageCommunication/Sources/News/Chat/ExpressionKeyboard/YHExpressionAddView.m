//
//  YHExpressionAddView.m
//  Expression
//
//  Created by samuelandkevin on 17/2/9.
//  Copyright © 2017年 samuelandkevin. All rights reserved.
//

#define DDLog(FORMAT, ...)   fprintf(stderr, "\n[%s]  function:%s line:%d content:%s\n", __TIME__, __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#import "YHExpressionAddView.h"
#import <YYModel/YYModel.h>
#import <YYCache/YYCache.h>
#import <YYWebImage/YYWebImage.h>
#import "YYImage.h"
#import <YYText.h>
#import "Masonry.h"
#import "YHExpressionHelper.h"
#import "YHModel.h"
#import <YYCategories/YYCategories.h>




@interface YHExpressionAddCell()
@property (nonatomic,strong) UIButton    *btnIcon;
@property (nonatomic,strong) UILabel     *lbName;
@end

@implementation YHExpressionAddCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    _btnIcon = [UIButton new];
    _btnIcon.userInteractionEnabled = NO;
    [self.contentView addSubview:_btnIcon];
    
    _lbName = [UILabel new];
    _lbName.font = [UIFont systemFontOfSize:14.0];
    _lbName.textAlignment = NSTextAlignmentCenter;
    _lbName.textColor     = [UIColor blackColor];
    [self.contentView addSubview:_lbName];
    
    [self layoutUI];
}

- (void)setModel:(YHExtraModel *)model{
    
    _model = model;
    
    [_btnIcon setImage:image(model.imageName) forState:UIControlStateNormal];
    [_btnIcon setImage:image(model.imageName) forState:UIControlStateSelected];
    _lbName.text = model.name;
}

- (void)layoutUI{
    __weak typeof(self) weakSelf = self;
    [_btnIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.contentView);
        make.width.height.mas_equalTo(50);
        make.top.equalTo(weakSelf.contentView).offset(10);
    }];
    
    
    [_lbName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.btnIcon.mas_bottom).offset(3);
        make.left.right.equalTo(weakSelf.contentView);
    }];
}

@end




#define kOneItemHeight 90
#define kOnePageCount  8

@interface YHExpressionAddView()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIView *pageControl;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation YHExpressionAddView


+ (instancetype)sharedView {
    static YHExpressionAddView *v;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        v = [self new];
    });
    return v;
}

- (instancetype)init {
    self = [super init];
    
    self.backgroundColor = RGBA(245, 245, 245, 1);
//    self.dataArray = [YHExpressionHelper extraModels];
    

    [self _initCollectionView];
   

    return self;
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}


- (void)_initCollectionView {
    NSArray *Array  = @[
    @{
        @"name" : @"相册",
        @"imageName" : @"键盘相册",
        },@{
        @"name" : @"拍照",
        @"imageName" : @"键盘拍照",
        },@{
        @"name" : @"全体禁言",
        @"imageName" : @"键盘全体禁言",
                    },@{
                    @"name" : @"全体禁言",
                    @"imageName" : @"键盘全体禁言",
                                },
    

    ];
    for (int i = 0; i < Array.count; i++) {
        NSDictionary *dic = Array[i];
        NSLog(@"dic===%@",dic);
        YHExtraModel *model = [YHExtraModel mj_objectWithKeyValues:dic];
         [self.dataArray addObject:model];
    }
    CGFloat itemWidth = APPSIZE.width  / 3.0;
    itemWidth = CGFloatPixelRound(itemWidth);
    CGFloat padding = (APPSIZE.width - 4 * itemWidth) / 2.0;
    CGFloat paddingLeft = CGFloatPixelRound(padding);
    CGFloat paddingRight =APPSIZE.width - paddingLeft - itemWidth * 4;
   
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(itemWidth,itemWidth);
    // 最小行间距，默认是0
    layout.minimumLineSpacing = 0;
    //最小左右间距，默认是10
    layout.minimumInteritemSpacing = 0;
    // 区域内间距，默认是 UIEdgeInsetsMake(0, 0, 0, 0)
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, APPSIZE.width, kOneItemHeight * 2) collectionViewLayout:layout];
    _collectionView.backgroundColor = RGBA(245, 245, 245, 1);
    [_collectionView registerClass:[YHExpressionAddCell class] forCellWithReuseIdentifier:@"cell1"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [self addSubview:_collectionView];
    
    
//    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, CGFloatFromPixel(1))];
//    line.backgroundColor = UIColorHex(BFBFBF);
//    [_collectionView addSubview:line];
    
    _pageControl = [UIView new];
    _pageControl.size = CGSizeMake(kScreenWidth, 20);
    _pageControl.top = _collectionView.bottom - 5;
    _pageControl.userInteractionEnabled = NO;
    //_pageControl.backgroundColor = [UIColor redColor];
    [self addSubview:_pageControl];
    
    //kun调试
//    _collectionView.backgroundColor = [UIColor orangeColor];
//    _pageControl.backgroundColor = [UIColor purpleColor];
    
}



#pragma mark - @protocol UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
      YHExtraModel *model = [self.dataArray objectAtIndex:indexPath.row];
    if (model && _delegate && [_delegate respondsToSelector:@selector(extraItemDidTap:)]) {
        DDLog(@"%@",model.name);
        [_delegate extraItemDidTap:model];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

#pragma mark - @protocol UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YHExtraModel *model = [self.dataArray objectAtIndex:indexPath.row];
    YHExpressionAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
    cell.model =model;
    return cell;
}

- (YHExtraModel *)_modelForIndexPath:(NSIndexPath *)indexPath {
  
    
    NSUInteger page  = 0;
    NSUInteger index = page * kOnePageCount + indexPath.row;
    
    // transpose line/row
    NSUInteger ip = index / kOnePageCount;
    NSUInteger ii = index % kOnePageCount;
    NSUInteger reIndex = (ii % 2) * 4 + (ii / 2);
    index = reIndex + ip * kOnePageCount;
    
    if (index < self.dataArray.count) {
       // return self.dataArray[index];
    } else {
        return nil;
    }
    return nil;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
