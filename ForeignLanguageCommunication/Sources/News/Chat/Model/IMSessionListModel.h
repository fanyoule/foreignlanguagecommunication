//
//  IMSessionListModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/21.
//

#import <Foundation/Foundation.h>

#import "IMSessionMessageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface IMSessionListModel : NSObject
@property (nonatomic, copy) NSString *avatarUrl;
@property (nonatomic) NSInteger chatType;
@property (nonatomic) NSInteger doNotDisturb;
@property (nonatomic) NSInteger groupId;
@property (nonatomic, strong) IMSessionMessageModel *message;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *sessionId;
@property (nonatomic) BOOL top;
@property (nonatomic) NSInteger status;
@property (nonatomic) NSInteger unReadNum;
@property (nonatomic) NSInteger userId;
/**
 avatarUrl = "http://xz.yueyuecms.com/h9oll7u5vta855whzm4r.jpg";
 chatType = 2;
 doNotDisturb = 0;
 groupId = 182;
 newMeg =     {
     content = "[\U5927\U7b11]";
     duration = 0;
     msgType = text;
     remoteRead = 0;
     time = 1611042187328;
 };
 nickName = ccc;
 sessionId = 60066a9fc66bf96dc1b1968c;
 status = 0;
 top = 0;
 unReadNum = 3;
 userId = 182;
 
 */
@end

NS_ASSUME_NONNULL_END
