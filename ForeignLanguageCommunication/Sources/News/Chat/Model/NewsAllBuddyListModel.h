//
//  NewsAllBuddyListModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/17.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsAllBuddyListModel : NSObject
/** 是否可跟随（0可跟随，1不可跟随） */
@property (nonatomic, assign) NSInteger follow;
/** 用户id */
@property (nonatomic, assign) NSInteger ID;
/**隐身对其可见/在线对其隐身人数*/
@property (nonatomic, assign) NSInteger invisibleNum;
/**是不是会员（0不是，1是）*/
@property (nonatomic, assign) NSInteger isVip;
/** 是不是会员 */
@property (nonatomic, strong) NSString *vipType;
/** 昵称 */
@property (nonatomic, strong) NSString  *nickname;
/**所在房间id(暂无字段)*/
@property (nonatomic, assign) NSInteger  roomId;
/**个性签名 */
@property (nonatomic, strong) NSString  *sign;
/**状态(1:在线,2:勿扰,3:游戏中,4:隐身)*/
@property (nonatomic, assign) NSInteger  status;
/**萌角号 */
@property (nonatomic, strong) NSString  *sysId;
/**头像 */
@property (nonatomic, strong) NSString  *url;
/**会员等级*/
@property (nonatomic, assign) NSInteger  vipLevel;
/**性别 0 女 1 男*/
@property (nonatomic, assign) NSInteger  sex;
//后添加字段，，是否关注 0:未关注 1：关注 
@property (nonatomic,assign)  NSInteger isFocusOn;
@end

NS_ASSUME_NONNULL_END
//返回的数据结构
//data = (
//{
//id = 154;
//status = 1;
//follow = 0;
//sysId = 30497216;
//sex = 0;
//url = http://xz.yueyuecms.com/5zl65tunhgj3jndaz7bw.jpeg;
//nickname = 丫头;
//online = 1;
//vipType = 非会员;
//sign = ;
//}
