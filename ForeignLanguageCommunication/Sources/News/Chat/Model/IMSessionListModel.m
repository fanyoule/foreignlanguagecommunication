//
//  IMSessionListModel.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/21.
//

#import "IMSessionListModel.h"

@implementation IMSessionListModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"message":@"newMeg"};
}
+ (NSDictionary *)mj_objectClassInArray{
    return @{@"message":[IMSessionMessageModel class]};
}
@end
