//
//  ChatPersonModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatPersonModel : NSObject
@property (nonatomic, assign) NSInteger Id;
@property (nonatomic, assign) NSInteger black;      // 是否拉黑 1：开启拉黑 0：关闭拉黑
@property (nonatomic, assign) NSInteger report;     // 是否举报 1: 举报 0：不举报
@property (nonatomic, assign) NSInteger silence;    // 是否免打扰 1：开启免打扰 0：关闭免打扰
@property (nonatomic, assign) NSInteger toUserId;   // 被操作用户id
@property (nonatomic, assign) NSInteger top;        // 是否置顶 1：置顶 0：不置顶
@property (nonatomic, assign) NSInteger userId;     // 用户id

@property (nonatomic, copy) NSString *toUserAvatar; // 被操作用户头像
@property (nonatomic, copy) NSString *toUserNickname;   // 被操作用户昵称
@property (nonatomic, copy) NSString *updateTime;   // 更新时间
@end

NS_ASSUME_NONNULL_END
