//
//  InteractiveMessageModel.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/19.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "InteractiveMessageModel.h"

@implementation InteractiveMessageModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"moreNewMeg":@"newMeg"};
}
@end
