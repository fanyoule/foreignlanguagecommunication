//
//  IMSessionMessageModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IMSessionMessageModel : NSObject
@property (nonatomic, copy) NSString *content;
@property (nonatomic) NSInteger duration;
@property (nonatomic, copy) NSString *text;
@property (nonatomic) NSInteger remoteRead;
@property (nonatomic) NSInteger time;
@end

NS_ASSUME_NONNULL_END
