//
//  InteractiveMessageModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/19.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InteractiveMessageMoreModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface InteractiveMessageModel : NSObject
/** 用户id */
@property (nonatomic,assign) NSInteger  userId;
/** 聊天类型 */
@property (nonatomic, assign) NSInteger  chatType;
/** 状态 */
@property (nonatomic, assign) NSInteger  status;
/** 详细信息 */
@property (nonatomic, strong) InteractiveMessageMoreModel *moreNewMeg;
/**  */
@property (nonatomic, assign) NSInteger  doNotDisturb;
/** <#属性注释#> */
@property (nonatomic, strong) NSString *sessionId;
/** <#属性注释#> */
@property (nonatomic, strong) NSString *avatarUrl;
/**  */
@property (nonatomic, assign) NSInteger  unReadNum;
/** 是否置顶 */
@property (nonatomic, assign) NSInteger  top;
/**  */
@property (nonatomic, assign) NSInteger  groupId;
/**  */
@property (nonatomic, strong) NSString * nickName;
@end

NS_ASSUME_NONNULL_END
