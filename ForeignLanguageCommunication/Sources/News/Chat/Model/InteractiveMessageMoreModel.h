//
//  InteractiveMessageMoreModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/27.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InteractiveMessageMoreModel : NSObject
/** <#属性注释#> */
@property (nonatomic, assign) NSInteger remoteRead ;
/** <#属性注释#> */
@property (nonatomic, strong) NSString *time ;
/** <#属性注释#> */
@property (nonatomic, assign) NSInteger duration   ;
/** <#属性注释#> */
@property (nonatomic, strong) NSString *msgType;
/** <#属性注释#> */
@property (nonatomic, strong) NSString *image;
/** 内容 */
@property (nonatomic, strong) NSString *content;
@end

NS_ASSUME_NONNULL_END
