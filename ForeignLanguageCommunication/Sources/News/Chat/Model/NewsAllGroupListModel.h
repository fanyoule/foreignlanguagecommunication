//
//  NewsAllGroupListModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsAllGroupListModel : NSObject
/** 群或讨论组id */
@property (nonatomic, assign) NSInteger ID;
/** 是否免打扰 */
@property (nonatomic, assign) BOOL isDisturb;
/** 是否屏蔽 */
@property (nonatomic, assign) BOOL isShield;
/** 是否总群 */
@property (nonatomic, assign) BOOL isMasterCluster;
/**总群id */
@property (nonatomic, assign) NSInteger masterClusterId;
/** 昵称 */
@property (nonatomic, strong) NSString *name;
/** 群类型（1：公会群，2：个人群，3：讨论组） 这里只有个人群*/
@property (nonatomic, assign) NSInteger type;
/** 头像*/
@property (nonatomic, strong) NSString  *url;
/** 公会ID */
@property (nonatomic, assign) NSInteger  orgId;

@end

NS_ASSUME_NONNULL_END
