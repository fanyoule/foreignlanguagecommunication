//
//  ChatIMSend.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/12/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//
#define TCP_AutoConnectCount  3    //自动重连次数


//自动重连次数
NSInteger autoConnectCount = TCP_AutoConnectCount;
#import "ChatIMSend.h"
#import "AsyncSocket.h"
#import "NSData+SwitchData.h"
#import "NSString+SwitchData.h"

#import <AudioToolbox/AudioToolbox.h>
@interface ChatIMSend ()
{
    //定义一个SystemSoundID
    //SystemSoundID soundID = 1000;//具体参数详情下面贴出来
}
@property(nonatomic,strong)AsyncSocket *tcpScoket;
/** 地址 */
@property (nonatomic, strong) NSString   *hostString;
/** 端口号*/
@property (nonatomic, strong) NSString    *portString;
/** 所有加入的公会和群组 */
@property (nonatomic, strong) NSMutableArray *allGroupArray;
/** 所有加入公会的组群 */
@property (nonatomic, strong) NSMutableDictionary *allGroupDic;


@end

@implementation ChatIMSend
/**
 *  包头自定义协格式  重要
 */
typedef struct {
    Byte version;
    Byte Mask;
    Byte cmdByte;
    Byte bodyLen[4];
}SendDataHead;

+(instancetype)share{
    static ChatIMSend *instance = nil;
    static dispatch_once_t onceToken;
   
    dispatch_once(&onceToken, ^{
        instance = [[ChatIMSend alloc]init];
     // [instance requsetGroupData];
       
    });
    return instance;
}
//初始化连接
- (void)initSocket{
    if (_tcpScoket) {
        return;
    }
    self.hostString =IMhost;
    self.portString = IMport;
    self.tcpScoket = [[AsyncSocket alloc]initWithDelegate:self];
    NSError *error;
    BOOL isConnect = [self.tcpScoket connectToHost: self.hostString onPort:[self.portString intValue] withTimeout:-1 error:&error];
    if (isConnect) {
        NSLog(@"连接成功");
    }else {
        NSLog(@"连接失败");
    }
    
    //[self.tcpScoket setRunLoopModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
    [self.tcpScoket readDataWithTimeout:-1 tag:0];
}
#pragma mark - 登陆
-(void)sendIMLogin{
    [self requsetGroupData];
}
#pragma mark - 登出了
-(void)disconnect{
  // [self.tcpScoket disconnect];
   // instance = nil;
   // onceToken = 0;
    self.tcpScoket = nil;
    self.tcpScoket.userData = SocketOfflineByUser;// 声明是由用户主动切断
    //自动重连次数 , 置为初始化
    autoConnectCount = TCP_AutoConnectCount;
}
//已经连接上
-(void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port{
    UserInfoModel *userModel = [[UserInfoManager shared] getUserDetailInfo];
   // [self addMessage:[NSString stringWithFormat:@"连接上%@\nlocal:%@",host,sock]];
    //[self.connectHostMuArr addObject:host];
    [self.tcpScoket readDataWithTimeout:-1 tag:0];
    NSMutableDictionary *contentDic = [[NSMutableDictionary alloc]init];
    contentDic[@"userId"] = @(userModel.ID);
    contentDic[@"password"] = @"";
    contentDic[@"userNcikName"] = userModel.nickname;
    contentDic[@"userAvatarUrl"] = userModel.avatar;
    contentDic[@"groupJsonStr"] =self.allGroupArray;
    NSLog(@"syoudezuquan===%@",self.allGroupArray);
    
    //[NSString stringWithFormat:@"{%@}",self.allGroupDic[@"data"]];
    //NSLog(@"dic====%@",contentDic);
    //NSLog(@"conter===%@",self.allGroupArray);
    //NSLog(@"ssss===%@",self.allGroupDic[@"data"]);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: contentDic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSString *content=@"{\"loginname\":\"hello_ios\",\"password\":\"123\"}";
    // NSLog(@"strrr===%@",str);
    //连接成功后，发送账号密码登录
    
    [self sendMessage:str cmd:0x05];
}
//发送socket消息
-(void)sendMessage:(NSString *)content{

    [self sendMessage:content cmd:(Byte)11];
}
//创建个人组群放松信息
-(void)CreatePersonalGroup:(NSString *)content{
    [self sendMessage:content cmd:(Byte)7];
}
//发送socket消息
-(void)sendMessage:(NSString *)content cmd:(Byte)cmd{
    //要发送的总体数据
    NSMutableData *mData = [[NSMutableData alloc] init];
    
    //消息体内容 转换成nsdata即可。连接上服务器后，首先需要登录，发送账号密码过去.json格式。
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    
    NSInteger datalenth =data.length;  //获取消息体内容转换成nsdata后的长度
   
    SendDataHead senddata;
    senddata.version=0x01; //协议版本号 目前为1
    senddata.Mask=0x81;    //mask数据。目前固定
    //senddata.cmdByte=0x05; //消息cmd命令。。根据消息类型不同而不同。例如登录操的cmd为0x05
    senddata.cmdByte=cmd;
    
    //以下是拼接 消息内容的长度 转换成字节放到包头里面 消息体长度占四个字节。是个int数据
    senddata.bodyLen[0]=(Byte)((datalenth & 0xFF000000)>>24);
    senddata.bodyLen[1]=(Byte)((datalenth & 0x00FF0000)>>16);
    senddata.bodyLen[2]=(Byte)((datalenth & 0x0000FF00)>>8);
    senddata.bodyLen[3]=(Byte)((datalenth & 0x000000FF));
    
    //将包头内容转换成nsddata
    NSData * headdata = [[NSData alloc]initWithBytes:&senddata length:sizeof(SendDataHead)];
    
    
    [mData appendData:headdata]; //拼放入包头
    [mData appendData:data];   //放入消息体
    
    [self.tcpScoket writeData:mData withTimeout:-1 tag:0];  //写入数据
}
-(void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    NSLog(@"data===%@",data);
    NSLog(@"tag===%ld",tag);
    NSData *head = [data subdataWithRange:NSMakeRange(0, 7)];//取得包头头部数据  包头目前长度是7位
    NSData *lengthData = [head subdataWithRange:NSMakeRange(3, 4)];//取得长度数据 第3-4位是消息体长度
    NSLog(@"读到的data数据：%@",lengthData);
    //以下是将包头的长度的四个字节转换成int数字。
     NSString* hexString = [lengthData convertDataToHexStr];
     NSInteger length = [[hexString hexToDecimal]integerValue];

    //从第8个字符节开始一直到length长度的数据，都是消息体正文。
    NSData *contentdata=[data subdataWithRange:NSMakeRange(7, length)];
   
    NSString *msg = [[NSString alloc] initWithData: contentdata encoding:NSUTF8StringEncoding];
    NSLog(@"读到的数据：%@",msg);
    [self dataReceived:msg];
    [self.tcpScoket readDataWithTimeout: -1 tag: 0];
}

//掉线从新链接
-(void)onSocketDidDisconnect:(AsyncSocket *)sock
{
    NSLog(@"sorry the connect is failure %ld",sock.userData);
 if (sock.userData == SocketOfflineByServer) {
        // 服务器掉线，重连
    
     //自动重连
     if (autoConnectCount) {
         [self sendIMLogin];
         NSLog(@"-------------第%ld次重连--------------",(long)autoConnectCount);
         autoConnectCount -- ;
     }else{
         NSLog(@"----------------重连次数已用完------------------");
     }
    }
   else if (sock.userData == SocketOfflineByUser) {
        // 如果由用户断开，不进行重连
        return;
    }
 
}

#pragma mark - 收到的数据
-(void)dataReceived:(NSString *)str{
   // NSLog(@"str===%@",str);
//    if ([self.chatIMDelelgete respondsToSelector:@selector(receivedChatIM:)]) {
//        [self.chatIMDelelgete receivedChatIM:str];
//    }
   
    NSDictionary *modelDic= [self dictionaryWithJsonString:str];
    NSLog(@"modeldic===%@",modelDic);
    NSString * stringo = [[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@-%@",modelDic[@"data"][@"from"],modelDic[@"data"][@"to"]]];
    NSLog(@"strin===%@",stringo);
    if ([modelDic[@"data"][@"groupType"]isEqualToString:@"1"]) {
        stringo = [[NSUserDefaults standardUserDefaults] valueForKey:modelDic[@"data"][@"groupId"]];
    }
    if ([modelDic[@"data"][@"groupType"]isEqualToString:@"2"]) {
        NSLog(@"data==%@",modelDic[@"data"][@"groupId"]);
        stringo = [[NSUserDefaults standardUserDefaults] valueForKey:modelDic[@"data"][@"groupId"]];
    }
    if ([modelDic[@"data"][@"groupType"]isEqualToString:@"3"]) {
        stringo = [[NSUserDefaults standardUserDefaults] valueForKey:modelDic[@"data"][@"groupId"]];
    }
    UserInfoModel *userModel = [[UserInfoManager shared] getUserDetailInfo];
    if ([modelDic[@"command"]intValue]== 11 && [modelDic[@"data"][@"from"]intValue] !=userModel.ID ) {
        NSString *key = ISNoDisturbingKey([modelDic[@"data"][@"from"] longValue]);
        BOOL isNoDisturb = GetCache(key);
        if (!isNoDisturb) {
            [self Playprompt:stringo];
        }
    }
    if ([modelDic[@"notityType"]isEqualToString:@"100"]) {//只要收到100 就做群绑定
        
        UserInfoModel *userModel = [[UserInfoManager shared] getUserDetailInfo];
        
        
        NSMutableDictionary *userDic = [[NSMutableDictionary alloc]init];
        userDic[@"avatar"] = userModel.avatar;
        userDic[@"id"] = @(userModel.ID);
        userDic[@"nick"] =userModel.nickname;
        NSArray *array = @[userDic];
        NSMutableDictionary *IMDic = [[NSMutableDictionary alloc]init];
        IMDic[@"groupType"] = modelDic[@"groupType"];
        IMDic[@"group_id"] =[NSString stringWithFormat:@"%@-%@",modelDic[@"groupType"],modelDic[@"groupId"]];
        IMDic[@"users"] =array;
        
        NSError *error;
        NSLog(@"imDic===%@",IMDic);
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:IMDic
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //发送消息方法
    //  [self sendMessage:str cmd:(Byte)11];
        [[ChatIMSend share] CreatePersonalGroup:str];
    }
    //收到礼物横屏通知
    if ([modelDic[@"notityType"]isEqualToString:@"110"]) {//110,房间横幅通知
//        {"avatarUrl":"http://xz.yueyuecms.com/udjbkn9h2dny23cxuv8n.png","configType":0,"consumeRoomId":231,"consumeRoomName":"唱歌嘛","consumeRoomNum":"364181","message":"用户7406在唱歌嘛房间本次送出礼物共计:250星钻","nickname":"7406","notityType":"110","orgId":0,"orgName":"","orgRoomIds":"","type":0,"userId":151}
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notice_RTC_PackupGiftRadio" object:nil userInfo:@{@"key":str}];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"noti" object:nil userInfo:@{@"key":str}];
}
#pragma mark - 播放提示音乐
-(void)Playprompt:(NSString *)str{
    if ([str isEqualToString:@"true"]) {
        return;
    }
        //音效文件路径
        NSString *path = [[NSBundle mainBundle] pathForResource:@"im_notify" ofType:@"mp3"];
        //组装并播放音效
        SystemSoundID soundID;
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
        AudioServicesPlaySystemSound(soundID);
}
#pragma mark - 获取我加入的群组工会
-(void)requsetGroupData{
    UserInfoModel *userModel = [[UserInfoManager shared] getUserDetailInfo];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    dic[@"userId"] = @(userModel.ID);
    //获取群组
    
    [RequestManager myJoinedClusterWithId:userModel.ID withSuccess:^(id  _Nullable response) {
        NSNumber *number = response[@"status"];
        if (number.intValue == 200) {
            NSLog(@"my group == %@",response[@"data"]);
            
            NSArray *array = response[@"data"];
            if (array.count > 0) {
                for (NSDictionary *dic in array) {
                    [self.allGroupArray addObject:dic];
                }
                
            }
            [self initSocket];
        }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    
//    [RequestManager getMyDataWithParameters:dic UrlString:@"/api/personalCluster/myJoinedCluster" Success:^(id  _Nullable response) {
//        //   NSLog(@"response==%@",response);
//        self.allGroupDic[@"data"]= response[@"data"];
//        if ([response[@"status"]intValue]==200) {
//            NSArray *array = response[@"data"];
//
//            if (array != nil && ![array isKindOfClass:[NSNull class]] && array.count != 0){
//                //执行array不为空时的操作
//                for (NSDictionary *dic in array) {
//                    [self.allGroupArray addObject:dic];
//                }
//
//            }else{
//
//            }
//        }
//        //MARK: 获取群组之后  登录
//        [self initSocket];
//        // NSLog(@"response==%@",response);
//    } withFail:^(NSError * _Nullable error) {
//        //NSLog(@"err==%@",error);
//    }];
}
//json格式字符串转字典：

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *err;
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                         
                                                        options:NSJSONReadingMutableContainers
                         
                                                          error:&err];
    
    if(err) {
        
        NSLog(@"json解析失败：%@",err);
        
        return nil;
        
    }
    
    return dic;
    
}
- (NSMutableDictionary *)allGroupDic {
    if (!_allGroupDic) {
        _allGroupDic = [[NSMutableDictionary alloc]init];
    }
    return _allGroupDic;
}
- (NSMutableArray *)allGroupArray {
    if (!_allGroupArray) {
        _allGroupArray = [[NSMutableArray alloc]init];
    }
    return _allGroupArray;
}
@end
