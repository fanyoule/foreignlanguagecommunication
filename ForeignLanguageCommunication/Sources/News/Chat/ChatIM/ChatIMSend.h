//
//  ChatIMSend.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/12/12.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//>>>>发送消息  接受消息

#import <Foundation/Foundation.h>

enum{
    SocketOfflineByServer,// 服务器掉线，默认为0
    SocketOfflineByUser,  // 用户主动cut
};
NS_ASSUME_NONNULL_BEGIN
@protocol ChatIMSendDelegate <NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
- (void)receivedChatIM:(NSString *)str;
@end


@interface ChatIMSend : NSObject
+(instancetype)share;
//发送socket消息
-(void)sendMessage:(NSString *)content;
//登陆
-(void)sendIMLogin;
//登出
-(void)disconnect;
//创建个人组群
-(void)CreatePersonalGroup:(NSString *)content;
@property(nonatomic, weak) id<ChatIMSendDelegate> chatIMDelelgete;
@end

NS_ASSUME_NONNULL_END
