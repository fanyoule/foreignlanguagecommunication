//
//  WLGifFooter.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/12/9.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <MJRefresh/MJRefresh.h>

NS_ASSUME_NONNULL_BEGIN

@interface WLGifFooter : MJRefreshBackGifFooter

@end

NS_ASSUME_NONNULL_END
