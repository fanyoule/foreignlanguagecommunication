//
//  YKAudioRecorder.m
//  recordAudio
//
//  Created by mac on 2020/10/21.
//  Copyright © 2020 mac. All rights reserved.
//音频录制 开始 停止 指定存储位置 存储名称  清除源文件 保留MP3

#import "YKAudioRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import "ConvertAudioFile.h"
#import "WBFileManager.h"

@interface YKAudioRecorder ()<AVAudioRecorderDelegate>
{
    NSString *aacPath;
    NSString *mp3Path;
}
@property (nonatomic, strong) NSTimer *myTimer;
@property (nonatomic, strong) AVAudioRecorder *recorder;

@end

@implementation YKAudioRecorder

static int currentTimeSecond = -1;
- (instancetype)init {
    if (self = [super init]) {
//        [self prepareSession];
        self.minDuration = 5;
        self.maxDuration = 60;
    }
    return self;
}

- (void)prepareSession {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if (error) {
        return;
    }
    //激活音频设备管理会话
}

//录音参数
- (NSMutableDictionary *)recordSettings {
    //录音参数设置
    
    NSMutableDictionary *dicM = [NSMutableDictionary dictionary];
    //直接录制PCM数据 编码格式caf
    [dicM setObject:@(kAudioFormatLinearPCM) forKey:AVFormatIDKey];
    //码率
    [dicM setObject:@(110025) forKey:AVSampleRateKey];
    //双声道
    [dicM setObject:@(2) forKey:AVNumberOfChannelsKey];
    
    [dicM setObject:@(16) forKey:AVLinearPCMBitDepthKey];
    //录音质量
    [dicM setObject:[NSNumber numberWithInt:AVAudioQualityMin] forKey:AVEncoderAudioQualityKey];
    
    return dicM;
}

//创建文件保存路径
- (NSURL *)filePathURL{
    
    WBFileManager *fileM = [[WBFileManager alloc] init];
    
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    path = fileM.tmpPath;
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"] ];
    [formater setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
    NSString *dateStr = [formater stringFromDate:[NSDate date]];
    aacPath = [path stringByAppendingFormat:@"%@.caf",dateStr];
    
    mp3Path = [path stringByAppendingFormat:@"%@.mp3",dateStr];
    return [NSURL fileURLWithPath:aacPath];
}


- (AVAudioRecorder *)recorder {
    if (!_recorder) {
        NSError *error;
        NSURL *pathURL = [self filePathURL];
        NSMutableDictionary *settings = [self recordSettings];
        _recorder = [[AVAudioRecorder alloc] initWithURL:pathURL settings:settings error:&error];
        [_recorder recordForDuration:self.maxDuration];//最多录音60s
        
    }
    return _recorder;
}
- (void)startRecord {
    
    //录音状态
    if (_recorder.isRecording) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(audioRecorderWillBeginSuccess:)]) {
            [self.delegate audioRecorderWillBeginSuccess:NO];
        }
        return;
    }
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if (error) {
        return;
    }
    
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    NSError *recoderError;
    NSURL *pathURL = [self filePathURL];
    NSMutableDictionary *settings = [self recordSettings];
    self.recorder = [[AVAudioRecorder alloc] initWithURL:pathURL settings:settings error:&recoderError];
    
//    self.recorder.delegate = self;
    
    
   BOOL isPrepare = [self.recorder prepareToRecord];
    if (isPrepare) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(audioRecorderWillBeginSuccess:)]) {
            [self.delegate audioRecorderWillBeginSuccess:YES];
        }
        
        self.currentDuration = 0;
        [self.recorder record];
        [self.myTimer fire];
        NSLog(@"%@",[NSThread currentThread]);
        [[NSRunLoop currentRunLoop] addTimer:self.myTimer forMode:NSRunLoopCommonModes];
    }
    
}
//停止录音
- (void)stopRecord {
    if (!_recorder) {
        return;
    }
    if (self.recorder.isRecording) {
        
        [self.recorder stop];
        
        
        NSURL *url = self.recorder.url;
        NSLog(@"preurl = %@",url);
//        mp3Path = self.recorder.url.absoluteString;
        
        
        if (self.currentDuration >= self.minDuration) {
            //大于5秒可以转换
            if (self.delegate && [self.delegate respondsToSelector:@selector(audioRecorderDidStopWithTime:)]) {
                [self.delegate audioRecorderDidStopWithTime:self.currentDuration];
            }
            [self convertCode];
        }else {
            //删除语音
            if (self.delegate && [self.delegate respondsToSelector:@selector(audioRecorderDidStopWithTime:)]) {
                [self.delegate audioRecorderDidStopWithTime:self.currentDuration];
            }
            WBFileManager *filemanager =  [[WBFileManager alloc]init] ;
            [filemanager deleteFileWithfileName:aacPath];
            
        }
        
        self.recorder = nil;
        if (self.myTimer.isValid) {
            [self.myTimer invalidate];
            self.myTimer = nil;
            currentTimeSecond = -1;
            
        }
    }
}
- (void)convertCode {
    [ConvertAudioFile conventToMp3WithCafFilePath:aacPath mp3FilePath:mp3Path sampleRate:110025 callback:^(BOOL result) {
        if (result) {
            //删除 aac文件
            WBFileManager *fileM = [[WBFileManager alloc] init];
            [fileM deleteFileWithfileName:self->aacPath];
            if (self.delegate && [self.delegate respondsToSelector:@selector(audioRecorderDidStopConvert:)]) {
                if ([fileM isSxistAtPath:self->mp3Path]) {
                    [self.delegate audioRecorderDidStopConvert:self->mp3Path];
                }
                
            }
        }else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(audioRecorderDidStopConvert:)]) {
                
                    [self.delegate audioRecorderDidStopConvert:nil];
                
                
            }
        }
        
    }];
}
- (NSURL *)fileURL {
    return [NSURL fileURLWithPath:mp3Path];
}
/**
 caf转MP3
 */
- (NSTimer *)myTimer {
    if (!_myTimer) {
        _myTimer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(upDateTime) userInfo:nil repeats:YES];
    }
    return _myTimer;
}
//定时器执行 时间+
- (void)upDateTime {
    currentTimeSecond ++;
    self.currentDuration = currentTimeSecond;
    NSLog(@"%d",currentTimeSecond);
    //子线程 执行
    if (self.delegate && [self.delegate respondsToSelector:@selector(audioRecorderCurrentTime:)]) {
        [self.delegate audioRecorderCurrentTime:self.currentDuration];
    }
    
}
- (BOOL)isRecording {
    NSLog(@"record ===%d",_recorder.isRecording);
    
    return _recorder.isRecording;
}
@end
