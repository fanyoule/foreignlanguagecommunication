//
//  YKAudioRecorder.h
//  recordAudio
//
//  Created by mac on 2020/10/21.
//  Copyright © 2020 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol YKAudioRecoderDelegate <NSObject>

//录音 时间
- (void)audioRecorderCurrentTime:(NSInteger)time;
- (void)audioRecorderWillBeginSuccess:(BOOL)success;
- (void)audioRecorderDidStopWithTime:(NSInteger)duration;
- (void)audioRecorderDidStopConvert:(NSString *_Nullable)mp3Path;


@end

NS_ASSUME_NONNULL_BEGIN

@interface YKAudioRecorder : NSObject

- (void)startRecord;
- (void)stopRecord;
@property (nonatomic, copy,readonly) NSURL *fileURL;
- (void)convertCode;
@property (nonatomic) NSInteger currentDuration;
@property (nonatomic, weak) id<YKAudioRecoderDelegate> delegate;
/**最小时间长度*/
@property (nonatomic) NSInteger minDuration;
/**最大录音时间*/
@property (nonatomic) NSInteger maxDuration;
@property (nonatomic, readonly) BOOL isRecording;
@end

NS_ASSUME_NONNULL_END
