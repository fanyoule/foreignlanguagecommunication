//
//  ChatPersonDetailViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/1.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatPersonDetailViewController : BaseViewController
@property (nonatomic, assign) long userId;

@end

NS_ASSUME_NONNULL_END
