//
//  ChatDetailsGroupVC.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/12/1.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//>>>>群组聊天

#import <UIKit/UIKit.h>
#import "NewsAllGroupListModel.h"
#import "ChatDetailsUserPoslitionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsGroupVC : UIViewController

/** mode */
@property (nonatomic, strong) NewsAllGroupListModel *buddyModel;
/** poslitionModel */
@property (nonatomic, strong) ChatDetailsUserPoslitionModel *politionModel;
@end

NS_ASSUME_NONNULL_END
