//
//  ChatDetailsUserPoslitionModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/12/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsUserPoslitionModel : NSObject
/** 公会是否开启主席模式（0关闭，1开启） */
@property (nonatomic, assign) NSInteger chair;
/** 群头像 */
@property (nonatomic, strong) NSString *clusterHeadUrl;
/** 群名称 */
@property (nonatomic, strong) NSString *clusterName;
/** 是否是公会总群（0是，1不是） */
@property (nonatomic, assign) NSInteger clusterType;
/** 用户身份（0普通成员，1管理员，2群主） */
@property (nonatomic, assign) NSInteger identity;
/** 公会总群id */
@property (nonatomic, assign) NSInteger  orgClusterId;
/** 公会头像 */
@property (nonatomic, strong) NSString *orgHeadUrl;
/** 公会名称 */
@property (nonatomic, strong) NSString *orgName;
/** 用户是否被禁言（0无，1禁） */
@property (nonatomic, assign) NSInteger  stopTalk;
@end

NS_ASSUME_NONNULL_END
