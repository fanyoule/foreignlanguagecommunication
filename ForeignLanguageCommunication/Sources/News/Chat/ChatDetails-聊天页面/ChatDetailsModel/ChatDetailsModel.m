//
//  ChatDetailsModel.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsModel.h"

@implementation ChatDetailsModel
+ (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    ChatDetailsModel *model;
    NSLog(@"tag===%@",dictionary[@"tag"]);
    if ([dictionary[@"tag"] isEqualToString:@"1"]) {
        //自己纯文字
        Class class = NSClassFromString(@"ChatTestModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"2"]){
        // 其他纯文字
        Class class = NSClassFromString(@"ChatNoticeModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"3"]){
        //其他图片
        Class class = NSClassFromString(@"ChatImageModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"4"]){
        //自己图片
        Class class = NSClassFromString(@"ChatDetailsImageRightModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"5"]){
        //公用，时间，撤销，通知等事件
        Class class = NSClassFromString(@"ChatDetailsPublicModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"6"]){
        //入群系统打招呼
        Class class = NSClassFromString(@"ChatDetailsIntroduceModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"7"]){
        //群公告消息
        Class class = NSClassFromString(@"ChatDetailsAnnouncementModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"8"]){
        //其他人语音
        Class class = NSClassFromString(@"ChatVioceLeftModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"9"]){
        //自己语音
        Class class = NSClassFromString(@"ChatVioceRightModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"10"]){
        //其他人发送的视频
        Class class = NSClassFromString(@"ChatVidoLeftModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"11"]){
        //自己发送的视频
        Class class = NSClassFromString(@"ChatVidoRightModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else if ([dictionary[@"tag"] isEqualToString:@"12"]){
        //撤回消息
        Class class = NSClassFromString(@"ChatDetailsWithdrawTheMessageModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    }else{
       Class  class = NSClassFromString(@"ChatDetailsModel");
        model =  [class mj_objectWithKeyValues:dictionary];
        return model;
    };
}

@end
