//
//  ChatDetailsHistoryModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/21.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsHistoryModel : NSObject
/** 唯一标识 */
@property (nonatomic, strong) NSString *uuid;
/** <#属性注释#> */
@property (nonatomic, assign) NSInteger remoteRead;
/** 内容 */
@property (nonatomic, strong) NSString *content;
/** 时间 */
@property (nonatomic, strong) NSString *time;
/** 聊天类型 */
@property (nonatomic, strong) NSString *msgType;
/** 头像 */
@property (nonatomic, strong) NSString *avatar;

/** 发送着id */
@property (nonatomic, assign) NSInteger sessionId;

/**  昵称*/
@property (nonatomic, strong) NSString *nickname;
/** out自己， in他人的 */
@property (nonatomic, strong) NSString *direct;
/** image 聊天记录里面的图片数据 */
@property (nonatomic, strong) NSString *image;
/** voice 聊天记录里面的语音 */
@property (nonatomic, strong) NSString *voice;
/** 语言时长 */
@property (nonatomic, assign) NSInteger duration;
/** 视频 */
@property (nonatomic, strong) NSString *video;
/** 表示符msgUuid */
@property (nonatomic, strong) NSString *msgUuid;
/** 发送的id */
@property (nonatomic, strong) NSString *group_id;
/** 是否是私聊 */
@property (nonatomic, assign) BOOL privateChat;
@end

NS_ASSUME_NONNULL_END
