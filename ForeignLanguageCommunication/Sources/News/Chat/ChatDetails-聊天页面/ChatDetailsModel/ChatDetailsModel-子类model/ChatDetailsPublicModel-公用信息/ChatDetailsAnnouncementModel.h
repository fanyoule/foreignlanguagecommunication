//
//  ChatDetailsAnnouncementModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsAnnouncementModel : ChatDetailsModel
/** 标题 */
@property (nonatomic, strong) NSString *title;
/** 内容 */
@property (nonatomic, strong) NSString *subTitle;
@end

NS_ASSUME_NONNULL_END
