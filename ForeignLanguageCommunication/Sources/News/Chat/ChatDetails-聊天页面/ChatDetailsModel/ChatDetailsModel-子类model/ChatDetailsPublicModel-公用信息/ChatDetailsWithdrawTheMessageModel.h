//
//  ChatDetailsWithdrawTheMessageModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/12/16.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsModel.h"
#import "ChatDetailsHistoryModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsWithdrawTheMessageModel : ChatDetailsModel
/** model */
@property (nonatomic, strong) ChatDetailsHistoryModel *historyModel;
@end

NS_ASSUME_NONNULL_END
