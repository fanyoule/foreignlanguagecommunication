//
//  ChatDetailsPublicModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsPublicModel : ChatDetailsModel
@property (nonatomic,copy)  NSString *content;
@end

NS_ASSUME_NONNULL_END
