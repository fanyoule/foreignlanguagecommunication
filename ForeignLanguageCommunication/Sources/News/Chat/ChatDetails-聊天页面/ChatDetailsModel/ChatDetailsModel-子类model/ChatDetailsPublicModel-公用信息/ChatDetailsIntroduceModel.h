//
//  ChatDetailsIntroduceModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsIntroduceModel : ChatDetailsModel
@property (nonatomic,copy)  NSString *content;
/** name */
@property (nonatomic, strong) NSString *name;
/** 头像 */
@property (nonatomic, strong) UIImageView *photo;
/** 欢迎 */
@property (nonatomic, strong) NSString *introduceString;
@end

NS_ASSUME_NONNULL_END
