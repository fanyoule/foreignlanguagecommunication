//
//  ChatNoticeModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsModel.h"
@class ChatDetailsHistoryModel;
NS_ASSUME_NONNULL_BEGIN

@interface ChatNoticeModel : ChatDetailsModel
@property (nonatomic,copy) NSString *tag;
@property (nonatomic,copy)  NSString *content;
/** model */
@property (nonatomic, strong) ChatDetailsHistoryModel *historyModel;
@end

NS_ASSUME_NONNULL_END
