//
//  ChatVidoLeftModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/24.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsModel.h"
#import "ChatDetailsHistoryModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatVidoLeftModel : ChatDetailsModel
/** model */
@property (nonatomic, strong) ChatDetailsHistoryModel *historyModel;
@end

NS_ASSUME_NONNULL_END
