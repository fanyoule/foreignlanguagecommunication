//
//  ChatDetailsImageRightModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsModel.h"
#import "ChatDetailsHistoryModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsImageRightModel : ChatDetailsModel
@property (nonatomic,copy) NSString *tag;
@property (nonatomic, strong) NSString *photoName;
@property (nonatomic,copy)  NSString *content;

/** model */
@property (nonatomic, strong) ChatDetailsHistoryModel *historyModel;
@end

NS_ASSUME_NONNULL_END
