//
//  ChatDetailsModel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatTextLayout.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsModel : NSObject
+ (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@property (nonatomic,strong) ChatTextLayout *layout;
//@property (nonatomic,copy)  NSString *content;
@end

NS_ASSUME_NONNULL_END
