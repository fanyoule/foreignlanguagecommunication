//
//  ChatDetailsCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsCell.h"
#import "ChatDetailsModel.h"
@implementation ChatDetailsCell

- (instancetype)initWithModel:(ChatDetailsModel *)model
{
    // 获取Model 类名
    NSString *modelName = [NSString stringWithUTF8String:object_getClassName(model)];
    ChatDetailsCell *cell = [[NSClassFromString(@"ChatDetailsCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    if ([modelName isEqualToString:@"ChatTestModel"]) {
        //自己文字的cell
        cell = [[NSClassFromString(@"ChatTextCell") alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatNoticeModel"]){
        //对方文字
        cell = [[NSClassFromString(@"ChatNoticeCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatImageModel"]){
        //对方图片
        cell = [[NSClassFromString(@"ChatImageCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatDetailsImageRightModel"]){
        //自己图片
        cell = [[NSClassFromString(@"ChatImageRightCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatDetailsPublicModel"]){
        //公用时间，群通知
        cell = [[NSClassFromString(@"ChatDetailsPublicCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    
    }else if ([modelName isEqualToString:@"ChatDetailsIntroduceModel"]){
       // NSLog(@"taopp===6");
        //入群简介
        cell = [[NSClassFromString(@"ChatDetailsInformationCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatDetailsAnnouncementModel"]){
       // NSLog(@"taopp===6");
        //群公告
        cell = [[NSClassFromString(@"ChatDetailsAnnouncementCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatVioceLeftModel"]){
        //NSLog(@"taopp===6");
        //其他人语音
        cell = [[NSClassFromString(@"ChatVioceLeftCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatVioceRightModel"]){
        //NSLog(@"taopp===6");
        //自己的语音
        cell = [[NSClassFromString(@"ChatVioceRightCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatVidoLeftModel"]){
       // NSLog(@"taopp===6");
        //其他人发送的视频
        cell = [[NSClassFromString(@"ChatVidoLeftCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatVidoRightModel"]){
      //  NSLog(@"taopp===6");
        //自己发送的视频
        cell = [[NSClassFromString(@"ChatVidoRightCell") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
    }else if ([modelName isEqualToString:@"ChatDetailsWithdrawTheMessageModel"]){
        //  NSLog(@"taopp===6");
          //撤回消息
          cell = [[NSClassFromString(@"ChatDetailsWithdrawTheMessage") alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:modelName];
      }
    return cell;
}
- (void)setDataWithModel:(ChatDetailsModel *)model
{
    
}
@end
