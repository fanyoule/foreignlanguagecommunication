//
//  ChatDetailsCell.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChatDetailsModel;

@protocol chatDetailsCellDelegate <NSObject>

@required // 必须实现的方法 默认

@optional // 可选实现的方法
//撤回
- (void)clickcellWithdraw:(UITableViewCell *)cell dictionary:(NSMutableDictionary *)dic;
//删除
-(void)clickCellDelete:(UITableViewCell *)cell dictionary:(NSMutableDictionary *)dic;

- (void)headerClick:(NSString *)userId;
@end

@interface ChatDetailsCell : UITableViewCell

- (instancetype)initWithModel:(ChatDetailsModel *)model;
- (void)setDataWithModel:(ChatDetailsModel *)model;
@property(nonatomic,assign)BOOL isInRoom;

@property(nonatomic, weak) id<chatDetailsCellDelegate> cellDelegate;
@end


