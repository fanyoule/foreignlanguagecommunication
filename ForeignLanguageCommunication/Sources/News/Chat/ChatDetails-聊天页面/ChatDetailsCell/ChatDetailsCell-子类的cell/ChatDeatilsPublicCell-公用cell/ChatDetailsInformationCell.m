//
//  ChatDetailsInformationCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsInformationCell.h"
#import "ChatDetailsIntroduceModel.h"
@interface ChatDetailsInformationCell ()
/** 背景view */
@property (nonatomic, strong) UIView *bgView;
/** 头像 */
@property (nonatomic, strong) UIImageView *photoImageV;
/** 名字 */
@property (nonatomic, strong) UILabel *nameLabel;
/** 介绍 */
@property (nonatomic, strong) YYLabel *introduceLable;
@end
@implementation ChatDetailsInformationCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
         self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.photoImageV];
        [self.bgView addSubview:self.nameLabel];
        [self.bgView addSubview:self.introduceLable];
        
       
     [self prepareSubviews];
       
    }
    
    return self;
}
-(void)prepareSubviews{
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_top).mas_offset(10);
        make.left.mas_equalTo(self.contentView.mas_left).mas_offset(62);
        make.right.mas_equalTo(self.contentView.mas_right).mas_offset(-62);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-10).priority(600);
    }];
    [self.photoImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_top).mas_offset(10);
        make.left.mas_equalTo(self.bgView.mas_left).mas_offset(12);
        make.height.mas_offset(@40);
        make.width.mas_offset(40);
        make.bottom.mas_equalTo(self.bgView.mas_bottom).mas_offset(-10);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageV.mas_right).offset(10);
        make.bottom.mas_equalTo(self.photoImageV.mas_centerY).offset(-4);
        make.height.mas_equalTo(16);
    }];
    [self.introduceLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.photoImageV.mas_right).offset(10);
        make.right.mas_equalTo(self.bgView.mas_right).offset(-10);
        make.top.mas_equalTo(self.photoImageV.mas_centerY).offset(4);
        make.height.mas_equalTo(16);
    
    }];
}
-(void)setDataWithModel:(ChatDetailsModel *)model{
  ChatDetailsIntroduceModel *noticModel = (ChatDetailsIntroduceModel *)model;
    self.nameLabel.text = noticModel.name;
    self.introduceLable.text = noticModel.introduceString;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (YYLabel *)introduceLable {
    if (!_introduceLable) {
        _introduceLable = [[YYLabel alloc]init];
        _introduceLable.textColor = rgba(153, 153, 153, 1);
        _introduceLable.font = [UIFont systemFontOfSize:15];
    }
    return _introduceLable;
}
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.layer.cornerRadius = 6;
        _bgView.backgroundColor = [UIColor whiteColor];
        
    }
    return _bgView;
}
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = RGBA(73, 199, 209, 1);
        _nameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    }
    return _nameLabel;
}
- (UIImageView *)photoImageV {
    if (!_photoImageV) {
        _photoImageV = [[UIImageView alloc]init];
        _photoImageV.layer.cornerRadius = 6.0;
        _photoImageV.clipsToBounds = YES;
        _photoImageV.image = image(@"测试头像2");
    }
    return _photoImageV;
}
@end
