//
//  ChatDetailsAnnouncementCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsAnnouncementCell.h"
#import "ChatDetailsAnnouncementModel.h"
@interface ChatDetailsAnnouncementCell ()
/** 背景 */
@property (nonatomic, strong) UIView *bgView;
/** 标题 */
@property(nonatomic,strong)UILabel *titleLabel;
/** 内容 */
@property (nonatomic, strong)UILabel *subLable;
/**查看按钮*/
@property (nonatomic, strong) UIButton *leftBtn;
/** 稍后按钮 */
@property (nonatomic, strong) UIButton *rightBtn;



@end
@implementation ChatDetailsAnnouncementCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.titleLabel];
        [self.bgView addSubview:self.subLable];
        [self.bgView addSubview:self.leftBtn];
        [self.bgView addSubview:self.rightBtn];
        
       [self prepareSubviews];
    }
    
    return self;
}
-(void)prepareSubviews{
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        make.left.mas_equalTo(self.contentView.mas_left).offset(38);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-38);
        make.height.mas_offset(@130);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-10).priority(600);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_top).offset(15);
        make.centerX.mas_equalTo(self.contentView.mas_centerX);
        make.height.mas_offset(@18);
        
    }];
    [self.subLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(6);
        make.centerX.mas_equalTo(self.titleLabel.mas_centerX);
     
    }];
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView.mas_left).offset(0);
        make.right.mas_equalTo(self.bgView.mas_centerX);
        make.bottom.mas_equalTo(self.bgView.mas_bottom);
        make.top.mas_equalTo(self.subLable.mas_bottom).offset(13);
    }];
    [_leftBtn whenTapped:^{
        NSLog(@"点击了查看");
    }];
    [_rightBtn whenTapped:^{
        NSLog(@"点击了稍后");
    }];
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView.mas_centerX);
        make.bottom.mas_equalTo(self.bgView.mas_bottom);
        make.top.mas_equalTo(self.subLable.mas_bottom).offset(13);
        make.right.mas_equalTo(self.bgView.mas_right);
        
    }];
    UILabel *lineLabl = [[UILabel alloc]init];
    lineLabl.backgroundColor = RGBA(223, 223, 223, 0.8);
    [self.bgView addSubview:lineLabl];
    UILabel *lineLabelTwo = [[UILabel alloc]init];
    lineLabelTwo.backgroundColor = RGBA(223, 223, 223, 1);
    [self.bgView addSubview:lineLabelTwo];
    //分割线
    [lineLabl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgView.mas_right).offset(0);
        make.left.mas_equalTo(self.bgView.mas_left).offset(0);
        make.top.mas_equalTo(self.subLable.mas_bottom).offset(10);
        make.height.mas_offset(1);
    }];
    [lineLabelTwo mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(lineLabl.mas_bottom).offset(0);
        make.centerX.mas_equalTo(self.bgView.mas_centerX);
        make.bottom.mas_equalTo(self.bgView.mas_bottom);
        make.width.mas_offset(1);

    }];

    
    
}
-(void)setDataWithModel:(ChatDetailsModel *)model{
    ChatDetailsAnnouncementModel *_model = (ChatDetailsAnnouncementModel *)model;
    self.titleLabel.text =_model.title;
    self.subLable.text = _model.subTitle;
}
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 4;
    }
    return _bgView;
}
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
        _titleLabel.textColor = RGBA(34, 34, 34, 1);
    }
    return _titleLabel;
}
- (UILabel *)subLable {
    if (!_subLable) {
        _subLable = [[UILabel alloc]init];
        
        _subLable.preferredMaxLayoutWidth =APPSIZE.width-160;
        _subLable.font = [UIFont systemFontOfSize:14];
        _subLable.textColor = RGBA(102, 102, 102, 1);
        _subLable.textAlignment = NSTextAlignmentCenter;
     [    _subLable setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
                _subLable.numberOfLines=0;
    }
    return _subLable;
}
- (UIButton*)leftBtn {
    if (!_leftBtn) {
        _leftBtn = [[UIButton alloc]init];
        _leftBtn.text = @"查看";
        _leftBtn.textColor =RGBA(116, 92, 240, 1);
        _leftBtn.fontSize = 15;
        
       
    }
    return _leftBtn;
}
- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [[UIButton alloc]init];
        _rightBtn.text = @"稍后";
        _rightBtn.textColor =RGBA(102, 102, 102, 1);
        _rightBtn.fontSize = 15;
    }
    return _rightBtn;
}
@end
