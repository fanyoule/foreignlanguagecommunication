//
//  ChatDetailsPublicCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsPublicCell.h"
#import "ChatDetailsPublicModel.h"
@interface ChatDetailsPublicCell()
/** 背景view */
@property (nonatomic, strong) UIView *bgView;
/** 现在信息的lab */
@property (nonatomic, strong) UILabel *informationLable;
@end
@implementation ChatDetailsPublicCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
       self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
        [self.contentView addSubview:self.bgView];
        
        [self.bgView addSubview:self.informationLable];
        [self prepareSubviews];
       
    }
    
    return self;
}
-(void)prepareSubviews{
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(self.contentView.mas_top).offset(8);
        make.bottom.mas_equalTo(self.contentView).offset(-8).priority(600);
        make.centerX.mas_equalTo(self.contentView.mas_centerX);
        make.left.mas_equalTo(self.informationLable.mas_left).offset(-8);
        make.right.mas_equalTo(self.informationLable.mas_right).offset(8);
    }];
    [self.informationLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_top).offset(6);
        make.centerX.mas_equalTo(self.bgView.mas_centerX);
        make.bottom.mas_equalTo(self.bgView.mas_bottom).offset(-6);
//        make.left.mas_equalTo(self.bgView.mas_left).offset(4);
//        make.right.mas_equalTo(self.bgView.mas_right).offset(-8);
    }];
    
}
-(void)setDataWithModel:(ChatDetailsModel *)model{
    ChatDetailsPublicModel *noticModel = (ChatDetailsPublicModel *)model;
    self.informationLable.text = noticModel.content;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (UILabel *)informationLable {
    if (!_informationLable) {
        _informationLable = [[UILabel alloc]init];
        
        _informationLable.font = [UIFont systemFontOfSize:12];
        _informationLable.textColor = RGBA(34, 34, 34, 0.7);
        _informationLable.preferredMaxLayoutWidth =APPSIZE.width;
        _informationLable.textAlignment = NSTextAlignmentCenter;
        [ _informationLable setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _informationLable.numberOfLines = 0;
    }
    return _informationLable;
}
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 3;
    }
    return _bgView;
}
@end
