//
//  ChatVioceLeftCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/24.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatVioceLeftCell.h"
#import "ChatVioceLeftModel.h"
#import "ChatDetailsIMImageView.h"
@interface ChatVioceLeftCell ()
/** 头像 */
@property(nonatomic,strong)UIImageView *iconImageView;
/** 姓名 */
@property(nonatomic,strong)UILabel *nameLabel;
/** 详情文字 */
@property(nonatomic,strong)UILabel *contentLabel;
/** 文字背景框 */
@property(nonatomic,strong)ChatDetailsIMImageView *bgImageView;
/** 图片 */
@property (nonatomic, strong) UIImageView *photoImageV;
/** 时长label */
@property (nonatomic, strong) UILabel *vioveTimeLabel;
/** 播放链接 */
@property (nonatomic, strong) NSString *vioceString;
/** 音乐播放 */
@property (nonatomic, strong) AVPlayer *player;
/** 语音时长 */
@property (nonatomic, strong) UILabel *vioceTimeLabel;
/** 播放动画 */
@property(nonatomic,strong)UIImageView *animationview;
/**  撤回 */
@property (nonatomic, strong) NSMutableDictionary *modeDic;
/** 删除某一条聊天记录 */
@property (nonatomic, strong) NSMutableDictionary *deleteDic;



@end
@implementation ChatVioceLeftCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
        
        [self prepareSubviews];
       
    }
    
    return self;
}

-(void)prepareSubviews{
    
    __weak typeof(self) weakSelf=self;
    [self.iconImageView whenTapped:^{
        NSLog(@"ppppp");
        if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(headerClick:)]) {
            [self.cellDelegate headerClick:self.modeDic[@"userId"]];
        }
    }];
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(weakSelf.contentView).offset(15);
        make.top.mas_equalTo(weakSelf.contentView).offset(15);
        make.width.height.equalTo(@60);
    }];
    
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.iconImageView);
        make.height.mas_equalTo(@20);
        make.width.mas_equalTo(@100);
        make.leading.mas_equalTo(weakSelf.iconImageView.mas_trailing).offset(8);
    }];
    
     [self.contentView addSubview:self.bgImageView];
    
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom).offset(10);
        make.height.mas_offset(50);
        make.width.mas_offset(100);
        make.bottom.mas_equalTo(weakSelf.contentView).offset(-8).priority(600);
        make.left.mas_equalTo(self.iconImageView.mas_right).offset(10);
    }];
    [self.bgImageView addSubview:self.vioceTimeLabel];
    [self.vioceTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bgImageView.mas_centerY);
        make.right.mas_equalTo(self.bgImageView.mas_right).offset(-15);
    }];
    [self.bgImageView addSubview:self.animationview];
    //播放动态效果
    _animationview.sd_layout
    .leftSpaceToView(_bgImageView, 20)
    .centerYEqualToView(_bgImageView)
    .widthIs(12)
    .heightIs(16);
    [self.bgImageView whenTapped:^{
        [self clickImage];
    }];
    self.bgImageView.withDrawBlock = ^(void) {
        //撤回
        NSLog(@"删除");
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickCellDelete:dictionary:)]) {
            [weakSelf.cellDelegate clickCellDelete:weakSelf dictionary:weakSelf.deleteDic];
        }
    };
}
- (void)setDataWithModel:(ChatVioceLeftModel*)model
{
    ChatVioceLeftModel *noticModel = (ChatVioceLeftModel *)model;
    self.nameLabel.text = noticModel.historyModel.nickname;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:noticModel.historyModel.avatar] placeholderImage:kImage(@"iconbg")];
    self.vioceString = noticModel.historyModel.content;
    NSLog(@"vioceString===%@",self.vioceString);
    self.vioceTimeLabel.text = [NSString stringWithFormat:@"%lu%@",noticModel.historyModel.duration,@"''"];
    NSLog(@"vioceStringtgext===%@", self.vioceTimeLabel.text);
    //放入值
    self.modeDic[@"msgUuid"] = noticModel.historyModel.uuid;
    self.modeDic[@"content"] =  noticModel.historyModel.content;
    self.modeDic[@"time"] =  noticModel.historyModel.time;
    self.modeDic[@"userId"] = @(noticModel.historyModel.sessionId);
    //删除聊天
    self.deleteDic[@"msgId"] =  noticModel.historyModel.uuid;
}
//点击播放
-(void)clickImage{
    NSString *path = self.vioceString;
    NSURL *urld = [NSURL URLWithString:self.vioceString];
    self.player = [[AVPlayer alloc]initWithURL:urld];
    [self.player play];
    __block NSInteger time = [self.vioceTimeLabel.text intValue]; //倒计时时间
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        
        if(time <= 0){ //倒计时结束，关闭
            
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置按钮的样式
                [self.animationview stopAnimating];//暂停
            });
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置按钮显示读秒效果
                [self.animationview startAnimating];//播放
            });
            
            time--;
        }
    });
    dispatch_resume(_timer);
}

-(UIImageView *)iconImageView{
    
    if (_iconImageView==nil) {
        _iconImageView=[UIImageView new];
        _iconImageView.layer.cornerRadius=6.0;
        _iconImageView.layer.masksToBounds=YES;
  
    }
    return _iconImageView;
}

-(UILabel *)nameLabel{
    
    if (_nameLabel==nil) {
        _nameLabel=[UILabel new];
        _nameLabel.text = @"Sun";
        _nameLabel.textColor=[UIColor lightGrayColor];
        _nameLabel.font=[UIFont systemFontOfSize:15];
        _nameLabel.textAlignment=NSTextAlignmentLeft;
    }
    return _nameLabel;
}

-(UILabel *)contentLabel{
    
    if (_contentLabel==nil) {
        _contentLabel=[UILabel new];
        _contentLabel.textColor = rgba(34, 34, 34, 1);
        _contentLabel.font=[UIFont systemFontOfSize:15];
        _contentLabel.text= @"我我我我我我我我我欧文";
        _contentLabel.preferredMaxLayoutWidth =APPSIZE.width-150;
           [ _contentLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _contentLabel.numberOfLines=0;
    }
    return _contentLabel;
}

-(ChatDetailsIMImageView *)bgImageView{
        
    if (_bgImageView==nil) {
        _bgImageView=[ChatDetailsIMImageView new];
        _bgImageView.image = [[UIImage imageNamed:@"chat_other_bg"]stretchableImageWithLeftCapWidth:22 topCapHeight:22];
        _bgImageView.isReceiver = YES;
        _bgImageView.userInteractionEnabled = YES;
    }
        return _bgImageView;
}
- (UILabel *)vioceTimeLabel {
    if (!_vioceTimeLabel) {
        _vioceTimeLabel = [[UILabel alloc]init];
        _vioceTimeLabel.textColor = rgba(153, 153, 153, 1);
        _vioceTimeLabel.textAlignment = NSTextAlignmentLeft;
        _vioceTimeLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:20];
    }
    return _vioceTimeLabel;
}
//动画的imageview
- (UIImageView *)animationview{
    if (!_animationview) {
        _animationview = [[UIImageView alloc]init];
        _animationview.image = [UIImage imageNamed:@"im_yuyin_blue_icon"];
        //进行动画效果的3张图片（按照播放顺序放置）
        _animationview.animationImages = [NSArray arrayWithObjects:
                                          [UIImage imageNamed:@"im_yuyin_blue_icon1"],
                                          [UIImage imageNamed:@"im_yuyin_blue_icon2"],
                                          [UIImage imageNamed:@"im_yuyin_blue_icon"],nil];
        //设置动画间隔
        _animationview.animationDuration = 1;
        _animationview.animationRepeatCount = 0;
        _animationview.userInteractionEnabled = NO;
        _animationview.backgroundColor = [UIColor clearColor];
    }
    return _animationview;
}
- (NSMutableDictionary *)modeDic {
    if (!_modeDic) {
        _modeDic = [[NSMutableDictionary alloc]init];
    }
    return _modeDic;
}
- (NSMutableDictionary *)deleteDic {
    if (!_deleteDic) {
        _deleteDic = [[NSMutableDictionary alloc]init];
    }
    return _deleteDic;
}
@end
