//
//  ChatNoticeCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//
#define kSetSystemFontSize @"setSystemFontSize"
#import "ChatNoticeCell.h"
#import "ChatNoticeModel.h"
#import "ChatDeatilsLabel.h"//长按效果
#import "ChatDetailsHistoryModel.h"
#import "Utility.h"
#import "ChatDeatilsIMLabel.h"
#define kContentFontSize        16.0f   //内容字体大小
#define kPadding                5.0f    //控件间隙
#define kEdgeInsetsWidth       20.0f   //内容内边距宽度
@interface ChatNoticeCell ()
/** 头像 */
@property(nonatomic,strong)UIImageView *iconImageView;
/** 姓名 */
@property(nonatomic,strong)UILabel *nameLabel;
/** 详情文字 */
@property(nonatomic,strong)ChatDeatilsIMLabel *contentLabel;
/** 文字背景框 */
@property(nonatomic,strong)UIImageView *bgImageView;
@property (nonatomic,strong) UILabel *lbContent;
/**  撤回 */
@property (nonatomic, strong) NSMutableDictionary *modeDic;
/** 删除某一条聊天记录 */
@property (nonatomic, strong) NSMutableDictionary *deleteDic;
@end
@implementation ChatNoticeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
       
        [self prepareSubviews];
       
    }
    
    return self;
}
- (void)setDataWithModel:(ChatNoticeModel *)model
{
    ChatNoticeModel *chatModel = (ChatNoticeModel *)model;
//    self.contentLabel.text = chatModel.historyModel.content;
    self.nameLabel.text = chatModel.historyModel.nickname;
       [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:chatModel.historyModel.avatar] placeholderImage:image(@"iconbg")];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:chatModel.historyModel.avatar] placeholderImage:image(@"iconbg")];
    // 1、计算文字的宽高
 float textMaxWidth = APPSIZE.width-180; //60是消息框体距离右侧或者左侧的距离
 NSMutableAttributedString *attrStr = [Utility emotionStrWithString:chatModel.historyModel.content plistName:@"emoticons.plist" y:-8];
    [attrStr addAttribute:NSFontAttributeName
                    value:[UIFont systemFontOfSize:kContentFontSize]
                    range:NSMakeRange(0, attrStr.length)];
   // NSLog(@"attstr===%@",attrStr);
    CGSize textSize = [attrStr boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                             context:nil].size;
    self.contentLabel.attributedText = attrStr;
    [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_offset(textSize.height+5);
        make.width.mas_offset(textSize.width+5);
    }];
    //放入值
    self.modeDic[@"msgUuid"] = chatModel.historyModel.uuid;
    self.modeDic[@"content"] = chatModel.historyModel.content;
    self.modeDic[@"time"] = chatModel.historyModel.time;
    self.modeDic[@"userId"] = @(chatModel.historyModel.sessionId);
    //删除聊天
    self.deleteDic[@"msgId"] = chatModel.historyModel.uuid;
}
-(void)prepareSubviews{
    
    __weak typeof(self) weakSelf=self;
    [self.iconImageView whenTapped:^{
        NSLog(@"ppppp");
        if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(headerClick:)]) {
            [self.cellDelegate headerClick:self.modeDic[@"userId"]];
        }
    }];
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(weakSelf.contentView).offset(15);
        make.top.mas_equalTo(weakSelf.contentView).offset(15);
        make.width.height.equalTo(@60);
    }];
    
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.iconImageView);
        make.height.mas_equalTo(@20);
        make.width.mas_equalTo(@100);
        make.leading.mas_equalTo(weakSelf.iconImageView.mas_trailing).offset(8);
    }];
    
     [self.contentView addSubview:self.bgImageView];
      [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
         
          make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom).offset(10);
          make.bottom.mas_equalTo(weakSelf.contentView).offset(-8).priority(600);
          make.left.mas_equalTo(self.iconImageView.mas_right).offset(10);
      }];
      
      [self.bgImageView addSubview:self.contentLabel];
      [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
          make.top.mas_equalTo(self.bgImageView.mas_top).offset(15);
          make.left.mas_equalTo(self.bgImageView.mas_left).offset(15);
          make.right.mas_equalTo(self.bgImageView.mas_right).offset(-20);
          make.bottom.mas_equalTo(self.bgImageView.mas_bottom).offset(-15);
      }];

    [_lbContent mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.equalTo(weakSelf.bgImageView.mas_left).offset(15);
         make.top.equalTo(weakSelf.bgImageView.mas_top).offset(5);
         make.bottom.equalTo(weakSelf.bgImageView.mas_bottom).offset(-5);
        make.width.mas_lessThanOrEqualTo(APPSIZE.width - 133);
     }];
    self.contentLabel.retweetBlock = ^(NSString * _Nonnull text) {
        //转发
        NSLog(@"撤回");
        
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickcellWithdraw:dictionary:)]) {
            [weakSelf.cellDelegate clickcellWithdraw:weakSelf dictionary:weakSelf.modeDic];
        }
    };
    self.contentLabel.withDrawBlock = ^(NSString * _Nonnull text) {
        //撤回
        NSLog(@"删除");
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickCellDelete:dictionary:)]) {
            [weakSelf.cellDelegate clickCellDelete:weakSelf dictionary:weakSelf.deleteDic];
        }
    };
}

-(UIImageView *)iconImageView{
    
    if (_iconImageView==nil) {
        _iconImageView=[UIImageView new];
        _iconImageView.layer.cornerRadius=6.0;
        _iconImageView.layer.masksToBounds=YES;
        _iconImageView.image=[UIImage imageNamed:@"屏幕快照 2017-08-10 下午4.05.01"];
    }
    return _iconImageView;
}

-(UILabel *)nameLabel{
    
    if (_nameLabel==nil) {
        _nameLabel=[UILabel new];
        _nameLabel.text = @"Sun";
        _nameLabel.textColor=[UIColor lightGrayColor];
        _nameLabel.font=[UIFont systemFontOfSize:15];
        _nameLabel.textAlignment=NSTextAlignmentLeft;
    }
    return _nameLabel;
}

-(ChatDeatilsIMLabel *)contentLabel{
    
    if (_contentLabel==nil) {
        _contentLabel=[ChatDeatilsIMLabel new];
   
        _contentLabel.textColor = rgba(34, 34, 34, 1);
        _contentLabel.font=[UIFont systemFontOfSize:15];
        _contentLabel.text= @"我我我我我我我我我欧文";
        _contentLabel.preferredMaxLayoutWidth =APPSIZE.width-150;
           [ _contentLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _contentLabel.numberOfLines=0;
        _contentLabel.isReceiver = YES;


        
    }
    return _contentLabel;
}

-(UIImageView *)bgImageView{
        
        if (_bgImageView==nil) {
            _bgImageView=[UIImageView new];
            _bgImageView.image = [[UIImage imageNamed:@"chat_other_bg"]stretchableImageWithLeftCapWidth:22 topCapHeight:22];
            _bgImageView.userInteractionEnabled = YES;
            
//            UIImage *bImage =[UIImage imageNamed:@"chat_recive_nor"];
//            bImage = [bImage resizableImageWithCapInsets:UIEdgeInsetsMake(bImage.size.width/2,bImage.size.width/2,bImage.size.width/2, bImage.size.width/2)];
//            _bgImageView.image = bImage;
//            _bgImageView.userInteractionEnabled = YES;
//            _bgImageView.contentMode =UIViewContentModeScaleToFill;
        }
        return _bgImageView;
    }
- (NSMutableDictionary *)modeDic {
    if (!_modeDic) {
        _modeDic = [[NSMutableDictionary alloc]init];
    }
    return _modeDic;
}
- (NSMutableDictionary *)deleteDic {
    if (!_deleteDic) {
        _deleteDic = [[NSMutableDictionary alloc]init];
    }
    return _deleteDic;
}
@end
