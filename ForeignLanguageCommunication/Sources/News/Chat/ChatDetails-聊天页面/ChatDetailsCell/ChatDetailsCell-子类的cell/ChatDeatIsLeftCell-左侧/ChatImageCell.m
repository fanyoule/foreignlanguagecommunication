//
//  ChatImageCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/23.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatImageCell.h"
#import "ChatImageModel.h"
#import "ChatDetailsIMImageView.h"
@interface ChatImageCell ()
/** 头像 */
@property(nonatomic,strong)UIImageView *iconImageView;
/** 姓名 */
@property(nonatomic,strong)UILabel *nameLabel;
/** 详情文字 */
@property(nonatomic,strong)UILabel *contentLabel;
/** 文字背景框 */
@property(nonatomic,strong)UIImageView *bgImageView;
/** 图片 */
@property (nonatomic, strong)ChatDetailsIMImageView *photoImageV;
/** 数据 */
@property (nonatomic, strong) NSMutableArray *dataArray;
/**  撤回 */
@property (nonatomic, strong) NSMutableDictionary *modeDic;
/** 删除某一条聊天记录 */
@property (nonatomic, strong) NSMutableDictionary *deleteDic;

@end
@implementation ChatImageCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
        
        [self prepareSubviews];
       
    }
    
    return self;
}
- (void)setDataWithModel:(ChatImageModel*)model
{
    [self.dataArray removeAllObjects];
    ChatImageModel *noticModel = (ChatImageModel *)model;
    self.nameLabel.text = noticModel.historyModel.nickname;
       [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:noticModel.historyModel.avatar] placeholderImage:image(@"iconbg")];
      [self.photoImageV sd_setImageWithURL:[NSURL URLWithString:noticModel.historyModel.content] placeholderImage:image(@"iconbg")];
    [self.dataArray removeAllObjects];
    YBImageBrowser *ybimageBrowser = [YBImageBrowser new];
   
    YBIBImageData *data = [[YBIBImageData alloc] init];
   data.imageURL = [NSURL URLWithString:noticModel.historyModel.content];
    [self.dataArray addObject:data];
    ybimageBrowser.dataSourceArray = self.dataArray;
   ybimageBrowser.currentPage = 0;
    [self.photoImageV whenTapped:^{
        NSLog(@"noticModel.historyModel.content==%@",noticModel.historyModel.content);
     
        [ybimageBrowser show];
    }];
    //放入值
    self.modeDic[@"msgUuid"] = noticModel.historyModel.uuid;
    self.modeDic[@"content"] = noticModel.historyModel.content;
    self.modeDic[@"time"]    = noticModel.historyModel.time;
    self.modeDic[@"userId"] = @(noticModel.historyModel.sessionId);
    //删除聊天
    self.deleteDic[@"msgId"] = noticModel.historyModel.uuid;
}
-(void)prepareSubviews{
    
    __weak typeof(self) weakSelf=self;
    [self.iconImageView whenTapped:^{
        NSLog(@"ppppp");
        if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(headerClick:)]) {
            [self.cellDelegate headerClick:self.modeDic[@"userId"]];
        }
    }];
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(weakSelf.contentView).offset(15);
        make.top.mas_equalTo(weakSelf.contentView).offset(15);
        make.width.height.equalTo(@60);
    }];
    
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.iconImageView);
        make.height.mas_equalTo(@20);
        make.width.mas_equalTo(@100);
        make.left.mas_equalTo(weakSelf.iconImageView.mas_right).offset(8);
    }];
    
     [self.contentView addSubview:self.photoImageV];
      [self.photoImageV mas_makeConstraints:^(MASConstraintMaker *make) {
         
          make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom).offset(10);
          make.bottom.mas_equalTo(weakSelf.contentView).offset(-8).priority(600);
          make.left.mas_equalTo(self.iconImageView.mas_right).offset(10);
          make.height.equalTo(@160);
          make.width.equalTo(@140);
      }];
      
    self.photoImageV.withDrawBlock = ^(void) {
        //撤回
        NSLog(@"删除");
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickCellDelete:dictionary:)]) {
            [weakSelf.cellDelegate clickCellDelete:weakSelf dictionary:weakSelf.deleteDic];
        }
    };
}

-(UIImageView *)iconImageView{
    
    if (_iconImageView==nil) {
        _iconImageView=[UIImageView new];
        _iconImageView.layer.cornerRadius=30;
        _iconImageView.layer.masksToBounds=YES;
        _iconImageView.image=[UIImage imageNamed:@""];
    }
    return _iconImageView;
}

-(UILabel *)nameLabel{
    
    if (_nameLabel==nil) {
        _nameLabel=[UILabel new];
        _nameLabel.text = @"Sun";
        _nameLabel.textColor=[UIColor lightGrayColor];
        _nameLabel.font=[UIFont systemFontOfSize:15];
        _nameLabel.textAlignment=NSTextAlignmentLeft;
    }
    return _nameLabel;
}

-(UILabel *)contentLabel{
    
    if (_contentLabel==nil) {
        _contentLabel=[UILabel new];
        _contentLabel.textColor = rgba(34, 34, 34, 1);
        _contentLabel.font=[UIFont systemFontOfSize:15];
        _contentLabel.text= @"我我我我我我我我我欧文";
        _contentLabel.preferredMaxLayoutWidth =APPSIZE.width-150;
           [ _contentLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _contentLabel.numberOfLines=0;
    }
    return _contentLabel;
}

-(UIImageView *)bgImageView{
        
        if (_bgImageView==nil) {
            _bgImageView=[UIImageView new];
            _bgImageView.image = [[UIImage imageNamed:@"chat_other_bg"]stretchableImageWithLeftCapWidth:22 topCapHeight:22];
            _bgImageView.userInteractionEnabled = YES;
            
//            UIImage *bImage =[UIImage imageNamed:@"chat_other_bg"];
//            bImage = [bImage resizableImageWithCapInsets:UIEdgeInsetsMake(bImage.size.width/2,bImage.size.width/2,bImage.size.width/2, bImage.size.width/2)];
//            _bgImageView.image = bImage;
//            _bgImageView.contentMode =UIViewContentModeScaleToFill;
        }
        return _bgImageView;
    }
- (ChatDetailsIMImageView *)photoImageV {
    if (!_photoImageV) {
        _photoImageV = [[ChatDetailsIMImageView alloc]init];
        _photoImageV.contentMode = UIViewContentModeScaleAspectFill;
        _photoImageV.layer.cornerRadius = 6.0;
        _photoImageV.clipsToBounds = YES;
        _photoImageV.isReceiver = YES;
    }
    return _photoImageV;
}
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (NSMutableDictionary *)modeDic {
    if (!_modeDic) {
        _modeDic = [[NSMutableDictionary alloc]init];
    }
    return _modeDic;
}
- (NSMutableDictionary *)deleteDic {
    if (!_deleteDic) {
        _deleteDic = [[NSMutableDictionary alloc]init];
    }
    return _deleteDic;
}
@end
