//
//  ChatTextCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatTextCell.h"
#import "ChatTestModel.h"
#import "ChatDeatilsLabel.h"
#import "ChatDetailsHistoryModel.h"
#import "Utility.h"
#import "ChatDeatilsIMLabel.h"
#define kContentFontSize        16.0f   //内容字体大小
#define kPadding                5.0f    //控件间隙
#define kEdgeInsetsWidth       20.0f   //内容内边距宽度
@interface ChatTextCell ()

@property(nonatomic,strong)UIImageView *iconImageView;

@property(nonatomic,strong)UILabel *nameLabel;
/** 是否查看 已读 送达*/
@property (nonatomic, strong) UILabel *remoteReadLabel;
@property(nonatomic,strong)ChatDeatilsIMLabel *contentLabel;

@property(nonatomic,strong)UIImageView *bgImageView;
/**  撤回 */
@property (nonatomic, strong) NSMutableDictionary *modeDic;
/** 删除某一条聊天记录 */
@property (nonatomic, strong) NSMutableDictionary *deleteDic;

@end
@implementation ChatTextCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
        [self.contentView addSubview:self.remoteReadLabel];
        
        [self prepareSubviews];
    }
    
    return self;
}

-(void)prepareSubviews{
    
    __weak typeof(self) weakSelf=self;
    [self.iconImageView whenTapped:^{
        NSLog(@"ppppp");
        if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(headerClick:)]) {
            [self.cellDelegate headerClick:self.modeDic[@"userId"]];
        }
    }];
    [self.contentView addSubview:self.iconImageView];

    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(weakSelf.contentView).offset(-15);
        make.top.mas_equalTo(weakSelf.contentView).offset(15);
        make.width.height.equalTo(@60);
    }];
    
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.iconImageView);
        make.trailing.mas_equalTo(weakSelf.iconImageView.mas_leading).offset(-8);
        make.height.mas_equalTo(@20);
        make.width.mas_equalTo(@100);
    }];
    
    [self.contentView addSubview:self.bgImageView];
    self.bgImageView.userInteractionEnabled = YES;
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom).offset(10);
        make.bottom.mas_equalTo(weakSelf.contentView).offset(-8).priority(600);
        make.right.mas_equalTo(self.iconImageView.mas_left).offset(-10);
    }];
    
    [self.bgImageView addSubview:self.contentLabel];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgImageView.mas_top).offset(15);
        make.left.mas_equalTo(self.bgImageView.mas_left).offset(15);
        make.right.mas_equalTo(self.bgImageView.mas_right).offset(-20);
        make.bottom.mas_equalTo(self.bgImageView.mas_bottom).offset(-15);
    }];
   // self.contentLabel.showMenuItemBlock();//显示
    self.contentLabel.showMenuItemBlock = ^{
        
    };
    self.contentLabel.retweetBlock = ^(NSString * _Nonnull text) {
        //转发
        NSLog(@"撤回");

        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickcellWithdraw:dictionary:)]) {
            [weakSelf.cellDelegate clickcellWithdraw:weakSelf dictionary:weakSelf.modeDic];
        }
    };
    self.contentLabel.withDrawBlock = ^(NSString * _Nonnull text) {
      
        NSLog(@"删除");
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickCellDelete:dictionary:)]) {
            [weakSelf.cellDelegate clickCellDelete:weakSelf dictionary:weakSelf.deleteDic];
        }
    };
    [self.remoteReadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgImageView.mas_left).offset(-5);
        make.top.mas_equalTo(self.bgImageView.mas_top).offset(-2.5);
        make.height.mas_offset(15);
        make.width.mas_offset(25);
    }];
}
- (void)setDataWithModel:(ChatTestModel *)model
{
   ChatTestModel *chatModel = (ChatTestModel *)model;
   // self.contentLabel.text = chatModel.historyModel.content;
    self.nameLabel.text = chatModel.historyModel.nickname;
//    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[chatModel.historyModel.content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    self.contentLabel.attributedText = attrStr;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:chatModel.historyModel.avatar] placeholderImage:image(@"iconbg")];
    // 1、计算文字的宽高
 float textMaxWidth = APPSIZE.width-180; //60是消息框体距离右侧或者左侧的距离
 NSMutableAttributedString *attrStr = [Utility emotionStrWithString:chatModel.historyModel.content plistName:@"emoticons.plist" y:-8];
    [attrStr addAttribute:NSFontAttributeName
                    value:[UIFont systemFontOfSize:kContentFontSize]
                    range:NSMakeRange(0, attrStr.length)];
   // NSLog(@"attstr===%@",attrStr);
    CGSize textSize = [attrStr boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                             context:nil].size;
    self.contentLabel.attributedText = attrStr;
    [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_offset(textSize.height+5);
        make.width.mas_offset(textSize.width+5);
    }];
    if (chatModel.historyModel.privateChat) {
        if (chatModel.historyModel.remoteRead == 1) {
            self.remoteReadLabel.text = @"已读";
            self.remoteReadLabel.backgroundColor = rgba(26, 196, 94, 1);
        }else{
            self.remoteReadLabel.text = @"送达";
            self.remoteReadLabel.backgroundColor = rgba(253, 184, 86, 1);
        }
    }else{
        [self.remoteReadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_offset(0);
        }];
    }
  
    //放入值
    self.modeDic[@"msgUuid"] = chatModel.historyModel.uuid;
    self.modeDic[@"content"] = chatModel.historyModel.content;
    self.modeDic[@"time"] = chatModel.historyModel.time;
    self.modeDic[@"userId"] = @(chatModel.historyModel.sessionId);
    //删除聊天
    self.deleteDic[@"msgId"] = chatModel.historyModel.uuid;
    
   //self.modeDic[@"group_id"]  = chatModel.historyModel.
    if (self.isInRoom) {
        self.remoteReadLabel.text = @"已读";
        self.remoteReadLabel.backgroundColor = rgba(26, 196, 94, 1);
    }
}
-(void)setIsInRoom:(BOOL)isInRoom{
    if (isInRoom) {
        self.remoteReadLabel.text = @"已读";
        self.remoteReadLabel.backgroundColor = rgba(26, 196, 94, 1);
    }
}
-(UIImageView *)iconImageView{
    
    if (_iconImageView==nil) {
        _iconImageView=[UIImageView new];
        _iconImageView.layer.cornerRadius=6.0;
        _iconImageView.layer.masksToBounds=YES;
        _iconImageView.image=[UIImage imageNamed:@""];
    }
    return _iconImageView;
}

-(UILabel *)nameLabel{
    
    if (_nameLabel==nil) {
        _nameLabel=[UILabel new];
        _nameLabel.textColor=[UIColor lightGrayColor];
        _nameLabel.text = @"me";
        _nameLabel.font=[UIFont systemFontOfSize:15];
        _nameLabel.textAlignment=NSTextAlignmentRight;
    }
    return _nameLabel;
}

-(ChatDeatilsIMLabel *)contentLabel{
    
    if (_contentLabel==nil) {
        _contentLabel=[ChatDeatilsIMLabel new];
        _contentLabel.textColor = [UIColor whiteColor];
        _contentLabel.font =  [UIFont fontWithName:@"PingFang-SC-Medium" size:16];
        _contentLabel.font=[UIFont systemFontOfSize:15];
      _contentLabel.preferredMaxLayoutWidth =200;
      [ _contentLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _contentLabel.numberOfLines=0;
    }
return _contentLabel;
}

-(UIImageView *)bgImageView{
    
    if (_bgImageView==nil) {
        _bgImageView=[UIImageView new];
      _bgImageView.image = [[UIImage imageNamed:@"chat_send_nor"]stretchableImageWithLeftCapWidth:22 topCapHeight:22];
        _bgImageView.userInteractionEnabled = YES;
//        UIImage *bImage =[UIImage imageNamed:@"chat_send_nor"];
//
//        bImage = [bImage resizableImageWithCapInsets:UIEdgeInsetsMake(bImage.size.width/2,bImage.size.width/2,bImage.size.width/2, bImage.size.width/2)];
//        _bgImageView.image = bImage;
//        _bgImageView.contentMode =UIViewContentModeScaleAspectFill;
//        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}
- (NSMutableDictionary *)modeDic {
    if (!_modeDic) {
        _modeDic = [[NSMutableDictionary alloc]init];
    }
    return _modeDic;
}
- (NSMutableDictionary *)deleteDic {
    if (!_deleteDic) {
        _deleteDic = [[NSMutableDictionary alloc]init];
    }
    return _deleteDic;
}
- (UILabel *)remoteReadLabel {
    if (!_remoteReadLabel) {
        _remoteReadLabel = [[UILabel alloc]init];
        _remoteReadLabel.cornerRadius = 3;
        _remoteReadLabel.textColor = [UIColor whiteColor];
        _remoteReadLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:11];
        _remoteReadLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _remoteReadLabel;
}
@end
