//
//  ChatDetailsActivityRightCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsActivityRightCell.h"

@interface ChatDetailsActivityRightCell ()
/** 背景 */
@property (nonatomic, strong) UIView *bgView;
/** 标题 */
@property(nonatomic,strong)UILabel *titleLabel;
/** 内容 */
@property (nonatomic, strong)UILabel *subLable;
/**查看按钮*/
@property (nonatomic, strong) UIButton *leftBtn;
/** 稍后按钮 */
@property (nonatomic, strong) UIButton *rightBtn;



@end
@implementation ChatDetailsActivityRightCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.titleLabel];
        [self.bgView addSubview:self.subLable];
        [self.bgView addSubview:self.leftBtn];
        [self.bgView addSubview:self.rightBtn];
        
       [self prepareSubviews];
    }
    
    return self;
}
-(void)prepareSubviews{
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        make.left.mas_equalTo(self.contentView.mas_left).offset(38);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-38);
        make.height.mas_offset(@130);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).mas_offset(-10).priority(600);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView.mas_top).offset(10);
        make.centerX.mas_equalTo(self.contentView.mas_centerX);
        make.height.mas_offset(@18);
        
    }];
    [self.subLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).offset(6);
        make.left.mas_equalTo(self.bgView.mas_left).offset(48);
        make.right.mas_equalTo(self.bgView.mas_right).offset(-48);
        make.height.mas_offset(@30);
    }];
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView.mas_left).offset(0);
        make.right.mas_equalTo(self.bgView.mas_centerX);
        make.bottom.mas_equalTo(self.bgView.mas_bottom);
        make.top.mas_equalTo(self.subLable.mas_bottom).offset(13);
    }];
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView.mas_centerX);
        make.bottom.mas_equalTo(self.bgView.mas_bottom);
        make.top.mas_equalTo(self.subLable.mas_bottom).offset(13);
        make.right.mas_equalTo(self.bgView.mas_right);
        
    }];
    
}
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.cornerRadius = 4;
    }
    return _bgView;
}
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
    }
    return _titleLabel;
}
- (UILabel *)subLable {
    if (!_subLable) {
        _subLable = [[UILabel alloc]init];
    }
    return _subLable;
}
- (UIButton*)leftBtn {
    if (!_leftBtn) {
        _leftBtn = [[UIButton alloc]init];
       
    }
    return _leftBtn;
}
- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [[UIButton alloc]init];
    }
    return _rightBtn;
}
@end
