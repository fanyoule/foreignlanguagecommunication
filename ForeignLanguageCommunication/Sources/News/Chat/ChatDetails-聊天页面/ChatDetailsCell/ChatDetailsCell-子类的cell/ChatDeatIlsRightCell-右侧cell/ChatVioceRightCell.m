//
//  ChatVioceRightCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/24.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatVioceRightCell.h"

#import "ChatDetailsImageRightModel.h"
#import "ChatDetailsIMImageView.h"
@interface ChatVioceRightCell ()
/** 头像 */
@property(nonatomic,strong)UIImageView *iconImageView;
/** 昵称 */
@property(nonatomic,strong)UILabel *nameLabel;

/** 是否查看 已读 送达*/
@property (nonatomic, strong) UILabel *remoteReadLabel;
@property(nonatomic,strong)ChatDetailsIMImageView *bgImageView;
/** 语音时长 */
@property (nonatomic, strong) UILabel *vioceTimeLabel;
/** 播放动画 */
@property(nonatomic,strong)UIImageView *animationview;
@property (nonatomic,strong)AVPlayer *player;
/** vioveString */
@property (nonatomic, strong) NSString *vioceString;
/**  撤回 */
@property (nonatomic, strong) NSMutableDictionary *modeDic;
/** 删除某一条聊天记录 */
@property (nonatomic, strong) NSMutableDictionary *deleteDic;


@end
@implementation ChatVioceRightCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
        [self.contentView addSubview:self.remoteReadLabel];
        [self prepareSubviews];
    }
    
    return self;
}

-(void)prepareSubviews{
    
    __weak typeof(self) weakSelf=self;
    [self.iconImageView whenTapped:^{
        NSLog(@"ppppp");
        if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(headerClick:)]) {
            [self.cellDelegate headerClick:self.modeDic[@"userId"]];
        }
    }];
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(weakSelf.contentView).offset(-15);
        make.top.mas_equalTo(weakSelf.contentView).offset(15);
        make.width.height.equalTo(@60);
    }];
    
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.iconImageView);
        make.trailing.mas_equalTo(weakSelf.iconImageView.mas_leading).offset(-8);
        make.height.mas_equalTo(@20);
        make.width.mas_equalTo(@100);
    }];
    [self.contentView addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_offset(50);
        make.width.mas_offset(100);
        make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom).offset(10);
        make.bottom.mas_equalTo(weakSelf.contentView).offset(-8).priority(600);
        make.right.mas_equalTo(self.iconImageView.mas_left).offset(-10);
    }];
    [self.bgImageView addSubview:self.vioceTimeLabel];
    [self.vioceTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bgImageView.mas_centerY);
        make.left.mas_equalTo(self.bgImageView.mas_left).offset(15);
    }];
    [self.bgImageView addSubview:self.animationview];
    //播放动态效果
    _animationview.sd_layout
    .rightSpaceToView(_bgImageView, 20)
    .centerYEqualToView(_bgImageView)
    .widthIs(12)
    .heightIs(16);
    [self.bgImageView whenTapped:^{
        [self clickImage];
    }];
    self.bgImageView.retweetBlock = ^(void) {
        //转发
        NSLog(@"撤回");
        
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickcellWithdraw:dictionary:)]) {
            [weakSelf.cellDelegate clickcellWithdraw:weakSelf dictionary:weakSelf.modeDic];
        }
    };
    self.bgImageView.withDrawBlock = ^(void) {
        //撤回
        NSLog(@"删除");
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickCellDelete:dictionary:)]) {
            [weakSelf.cellDelegate clickCellDelete:weakSelf dictionary:weakSelf.deleteDic];
        }
    };
    [self.remoteReadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgImageView.mas_left).offset(-5);
        make.top.mas_equalTo(self.bgImageView.mas_top).offset(-2.5);
        make.height.mas_offset(15);
        make.width.mas_offset(25);
    }];
}
- (void)setDataWithModel:(ChatDetailsImageRightModel *)model
{
    ChatDetailsImageRightModel *rightImageModel = (ChatDetailsImageRightModel *)model;
    self.nameLabel.text = rightImageModel.historyModel.nickname;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:rightImageModel.historyModel.avatar]];
    self.vioceTimeLabel.text = [NSString stringWithFormat:@"%lu%@",rightImageModel.historyModel.duration,@"''"];
    self.vioceString = rightImageModel.historyModel.content;
    //放入值
    self.modeDic[@"msgUuid"] = rightImageModel.historyModel.uuid;
    self.modeDic[@"content"] =  rightImageModel.historyModel.content;
    self.modeDic[@"time"] =  rightImageModel.historyModel.time;
    self.modeDic[@"userId"] = @(rightImageModel.historyModel.sessionId);
    //删除聊天
    self.deleteDic[@"msgId"] =  rightImageModel.historyModel.uuid;
    if (rightImageModel.historyModel.privateChat) {
        if (rightImageModel.historyModel.remoteRead == 1) {
            self.remoteReadLabel.text = @"已读";
            self.remoteReadLabel.backgroundColor = rgba(26, 196, 94, 1);
        }else{
            self.remoteReadLabel.text = @"送达";
            self.remoteReadLabel.backgroundColor = rgba(253, 184, 86, 1);
        }
    }else{
        [self.remoteReadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_offset(0);
        }];
    }
}
-(void)setIsInRoom:(BOOL)isInRoom{
    if (isInRoom) {
        self.remoteReadLabel.text = @"已读";
        self.remoteReadLabel.backgroundColor = rgba(26, 196, 94, 1);
    }
}
//点击播放
-(void)clickImage{
    NSString *path = self.vioceString;
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    self.player = [[AVPlayer alloc]initWithURL:url];
    [self.player play];
    __block NSInteger time = [self.vioceTimeLabel.text intValue]; //倒计时时间
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        
        if(time <= 0){ //倒计时结束，关闭
            
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置按钮的样式
                [self.animationview stopAnimating];//暂停
            });
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置按钮显示读秒效果
                [self.animationview startAnimating];//播放
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}
-(UIImageView *)iconImageView{
    
    if (_iconImageView==nil) {
        _iconImageView=[UIImageView new];
        _iconImageView.layer.cornerRadius=6.0;
        _iconImageView.layer.masksToBounds=YES;
        _iconImageView.image=[UIImage imageNamed:@"测试头像"];
    }
    return _iconImageView;
}

-(UILabel *)nameLabel{
    
    if (_nameLabel==nil) {
        _nameLabel=[UILabel new];
        _nameLabel.textColor=[UIColor lightGrayColor];
        _nameLabel.text = @"me";
        _nameLabel.font=[UIFont systemFontOfSize:15];
        _nameLabel.textAlignment=NSTextAlignmentRight;
    }
    return _nameLabel;
}


-(ChatDetailsIMImageView *)bgImageView{
    
    if (_bgImageView==nil) {
        _bgImageView=[ChatDetailsIMImageView new];
        _bgImageView.image = [[UIImage imageNamed:@"chat_send_nor"]stretchableImageWithLeftCapWidth:22 topCapHeight:22];
        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}
- (UILabel *)vioceTimeLabel {
    if (!_vioceTimeLabel) {
        _vioceTimeLabel = [[UILabel alloc]init];
        _vioceTimeLabel.textColor = [UIColor whiteColor];
        _vioceTimeLabel.textAlignment = NSTextAlignmentLeft;
        _vioceTimeLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:20];
    }
    return _vioceTimeLabel;
}
//动画的imageview
- (UIImageView *)animationview{
    if (!_animationview) {
        _animationview = [[UIImageView alloc]init];
        _animationview.image = [UIImage imageNamed:@"im_yuyin_white_icon"];
        //进行动画效果的3张图片（按照播放顺序放置）
        _animationview.animationImages = [NSArray arrayWithObjects:
                                          [UIImage imageNamed:@"im_yuyin_white_icon1"],
                                          [UIImage imageNamed:@"im_yuyin_white_icon2"],
                                          [UIImage imageNamed:@"im_yuyin_white_icon"],nil];
        //设置动画间隔
        _animationview.animationDuration = 1;
        _animationview.animationRepeatCount = 0;
        _animationview.userInteractionEnabled = NO;
        _animationview.backgroundColor = [UIColor clearColor];
    }
    return _animationview;
}
- (NSMutableDictionary *)modeDic {
    if (!_modeDic) {
        _modeDic = [[NSMutableDictionary alloc]init];
    }
    return _modeDic;
}
- (NSMutableDictionary *)deleteDic {
    if (!_deleteDic) {
        _deleteDic = [[NSMutableDictionary alloc]init];
    }
    return _deleteDic;
}
- (UILabel *)remoteReadLabel {
    if (!_remoteReadLabel) {
        _remoteReadLabel = [[UILabel alloc]init];
        _remoteReadLabel.cornerRadius = 3;
        _remoteReadLabel.textColor = [UIColor whiteColor];
        _remoteReadLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:11];
        _remoteReadLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _remoteReadLabel;
}
@end
