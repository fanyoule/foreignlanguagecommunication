//
//  ChatImageRightCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/25.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatImageRightCell.h"
#import "ChatDetailsImageRightModel.h"
#import "ChatDetailsIMImageView.h"
@interface ChatImageRightCell ()
/** 头像 */
@property(nonatomic,strong)UIImageView *iconImageView;
/** 昵称 */
@property(nonatomic,strong)UILabel *nameLabel;


@property(nonatomic,strong)UIImageView *bgImageView;
/** 是否查看 已读 送达*/
@property (nonatomic, strong) UILabel *remoteReadLabel;
/** 图片 */
@property (nonatomic, strong) ChatDetailsIMImageView *photoImageV;
/** 数据 */
@property (nonatomic, strong) NSMutableArray *dataArray;
/**  撤回 */
@property (nonatomic, strong) NSMutableDictionary *modeDic;
/** 删除某一条聊天记录 */
@property (nonatomic, strong) NSMutableDictionary *deleteDic;

@end
@implementation ChatImageRightCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor=RGBA(245, 245, 245, 1);
         [self.contentView addSubview:self.photoImageV];
        [self.contentView addSubview:self.remoteReadLabel];
        [self prepareSubviews];
    }
    
    return self;
}

-(void)prepareSubviews{
    
    __weak typeof(self) weakSelf=self;
    [self.iconImageView whenTapped:^{
        NSLog(@"ppppp");
        if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(headerClick:)]) {
            [self.cellDelegate headerClick:self.modeDic[@"userId"]];
        }
    }];
    [self.contentView addSubview:self.iconImageView];
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.mas_equalTo(weakSelf.contentView).offset(-15);
        make.top.mas_equalTo(weakSelf.contentView).offset(15);
        make.width.height.equalTo(@60);
    }];
    
    [self.contentView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.iconImageView);
        make.trailing.mas_equalTo(weakSelf.iconImageView.mas_leading).offset(-8);
        make.height.mas_equalTo(@20);
        make.width.mas_equalTo(@100);
    }];
    [self.photoImageV mas_makeConstraints:^(MASConstraintMaker *make) {
         
          make.top.mas_equalTo(weakSelf.nameLabel.mas_bottom).offset(10);
          make.bottom.mas_equalTo(weakSelf.contentView).offset(-8).priority(600);
          make.right.mas_equalTo(self.iconImageView.mas_left).offset(-10);
          make.height.equalTo(@160);
          make.width.equalTo(@140);
      }];
    [self.photoImageV whenTapped:^{
        NSLog(@"dddd");
    }];
    self.photoImageV.retweetBlock = ^(void) {
        //转发
        NSLog(@"撤回");
        
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickcellWithdraw:dictionary:)]) {
            [weakSelf.cellDelegate clickcellWithdraw:weakSelf dictionary:weakSelf.modeDic];
        }
    };
    self.photoImageV.withDrawBlock = ^(void) {
        //撤回
        NSLog(@"删除");
        if ([weakSelf.cellDelegate respondsToSelector:@selector(clickCellDelete:dictionary:)]) {
            [weakSelf.cellDelegate clickCellDelete:weakSelf dictionary:weakSelf.deleteDic];
        }
    };
    [self.remoteReadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.photoImageV.mas_left).offset(-5);
        make.top.mas_equalTo(self.photoImageV.mas_top).offset(-2.5);
        make.height.mas_offset(15);
        make.width.mas_offset(25);
    }];
}
- (void)setDataWithModel:(ChatDetailsImageRightModel *)model
{
    ChatDetailsImageRightModel *rightImageModel = (ChatDetailsImageRightModel *)model;
    self.nameLabel.text = rightImageModel.historyModel.nickname;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:rightImageModel.historyModel.avatar] placeholderImage:image(@"iconbg")];
   [self.photoImageV sd_setImageWithURL:[NSURL URLWithString:rightImageModel.historyModel.content] placeholderImage:image(@"iconbg")];
    [self.dataArray removeAllObjects];
    YBImageBrowser *ybimageBrowser = [YBImageBrowser new];
   
    YBIBImageData *data = [[YBIBImageData alloc] init];
    data.imageURL = [NSURL URLWithString:rightImageModel.historyModel.content];
    [self.dataArray addObject:data];
    ybimageBrowser.dataSourceArray = self.dataArray;
    ybimageBrowser.currentPage = 0;
    [self.photoImageV whenTapped:^{
        NSLog(@"noticModel.historyModel.content==%@",rightImageModel.historyModel.content);
  
        [ybimageBrowser show];
    }];
    //放入值
    self.modeDic[@"msgUuid"] = rightImageModel.historyModel.uuid;
    self.modeDic[@"content"] = rightImageModel.historyModel.content;
    self.modeDic[@"time"]    = rightImageModel.historyModel.time;
    self.modeDic[@"userId"] = @(rightImageModel.historyModel.sessionId);
    //删除聊天
    self.deleteDic[@"msgId"] = rightImageModel.historyModel.uuid;
    if (rightImageModel.historyModel.privateChat) {
        if (rightImageModel.historyModel.remoteRead == 1) {
            self.remoteReadLabel.text = @"已读";
            self.remoteReadLabel.backgroundColor = rgba(26, 196, 94, 1);
        }else{
            self.remoteReadLabel.text = @"送达";
            self.remoteReadLabel.backgroundColor = rgba(253, 184, 86, 1);
        }
    }else{
        [self.remoteReadLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_offset(0);
        }];
    }
}
-(void)setIsInRoom:(BOOL)isInRoom{
    if (isInRoom) {
        self.remoteReadLabel.text = @"已读";
        self.remoteReadLabel.backgroundColor = rgba(26, 196, 94, 1);
    }
}
-(UIImageView *)iconImageView{
    
    if (_iconImageView==nil) {
        _iconImageView=[UIImageView new];
        _iconImageView.layer.cornerRadius=6.0;
        _iconImageView.layer.masksToBounds=YES;
        _iconImageView.image=[UIImage imageNamed:@"测试头像"];
    }
    return _iconImageView;
}

-(UILabel *)nameLabel{
    
    if (_nameLabel==nil) {
        _nameLabel=[UILabel new];
        _nameLabel.textColor=[UIColor lightGrayColor];
        _nameLabel.text = @"me";
        _nameLabel.font=[UIFont systemFontOfSize:15];
        _nameLabel.textAlignment=NSTextAlignmentRight;
    }
    return _nameLabel;
}


-(UIImageView *)bgImageView{
    
    if (_bgImageView==nil) {
        _bgImageView=[UIImageView new];
        _bgImageView.image = [[UIImage imageNamed:@"chat_send_nor"]stretchableImageWithLeftCapWidth:22 topCapHeight:22];
//        UIImage *bImage =[UIImage imageNamed:@"chat_send_nor"];
//
//        bImage = [bImage resizableImageWithCapInsets:UIEdgeInsetsMake(bImage.size.width/2,bImage.size.width/2,bImage.size.width/2, bImage.size.width/2)];
//        _bgImageView.image = bImage;
//        _bgImageView.contentMode =UIViewContentModeScaleToFill;
//        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}
- (ChatDetailsIMImageView *)photoImageV {
    if (!_photoImageV) {
        _photoImageV = [[ChatDetailsIMImageView alloc]init];
        _photoImageV.contentMode = UIViewContentModeScaleAspectFill;
        _photoImageV.layer.cornerRadius = 5;
        _photoImageV.clipsToBounds = YES;
    }
    return _photoImageV;
}
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
- (NSMutableDictionary *)modeDic {
    if (!_modeDic) {
        _modeDic = [[NSMutableDictionary alloc]init];
    }
    return _modeDic;
}
- (NSMutableDictionary *)deleteDic {
    if (!_deleteDic) {
        _deleteDic = [[NSMutableDictionary alloc]init];
    }
    return _deleteDic;
}
- (UILabel *)remoteReadLabel {
    if (!_remoteReadLabel) {
        _remoteReadLabel = [[UILabel alloc]init];
        _remoteReadLabel.cornerRadius = 3;
        _remoteReadLabel.textColor = [UIColor whiteColor];
        _remoteReadLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:11];
    }
    return _remoteReadLabel;
}
@end
