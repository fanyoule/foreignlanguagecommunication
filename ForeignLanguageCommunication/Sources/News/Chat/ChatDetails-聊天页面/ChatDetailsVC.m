//
//  ChatDetailsVC.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDetailsVC.h"
#import "ChatDetailsCell.h"//父类的cell
#import "ChatDetailsModel.h"//父类的model
#import "YHExpressionKeyboard.h"//键盘
//#import "ChatDetailsSetVC.h"//聊天设置
//#import "ChatDetailsAnnouncementVC.h"//发布公告
#import "AsyncSocket.h"
#import "SwitchHeader.h"
#import "LLInputController.h"
#import "THeader.h"
#import <AVFoundation/AVFoundation.h>

#import "ChatDetailsHistoryModel.h"
#import <MJRefresh.h>
#import "MjWLHeader.h"
#import "WLGifFooter.h"
//#import "ChatDetalisPersonalSetVC.h"//个人聊天设置
#import "PlayerManager.h"
#import "XKLIMVoiceConverter.h"
#import "ChatIMSend.h"
#import "ChatPersonDetailViewController.h"
#import "MyPersonalInformationVC.h"


/**
 *  包头自定义协格式  重要
 */
typedef struct {
    Byte version;
    Byte Mask;
    Byte cmdByte;
    Byte bodyLen[4];
}SendDataHead;
@interface ChatDetailsVC ()<UITableViewDelegate,UITableViewDataSource,TInputControllerDelegate,TZImagePickerControllerDelegate,ETPlayerDelagate,HXPhotoViewControllerDelegate,HXCustomNavigationControllerDelegate
,UIImagePickerControllerDelegate, UINavigationControllerDelegate,ChatIMSendDelegate,chatDetailsCellDelegate>
@property (nonatomic,strong)UILabel    *labTitle;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic,strong)NSMutableDictionary *dic;
@property (nonatomic,strong)NSMutableArray *modelArray;
@property (nonatomic, strong) UITableView *chatTableV;
@property (nonatomic,strong) YHExpressionKeyboard *keyboard;
/** 地址 */
@property (nonatomic, strong) NSString   *hostString;
/** 端口号*/
@property (nonatomic, strong) NSString    *portString;
/** 所有加入的公会和群组 */
@property (nonatomic, strong) NSMutableArray *allGroupArray;
/** 所有加入公会的组群 */
@property (nonatomic, strong) NSMutableDictionary *allGroupDic;
@property(nonatomic,strong)AsyncSocket *tcpScoket;
@property (nonatomic,strong) LLInputController * inputController;
/** 页数 */
@property (nonatomic, assign) NSInteger   pagNumber;
/** 刷新动画 */
@property (nonatomic, strong) MjWLHeader *gifHeader;
/** 上拉 */
@property (nonatomic, strong) WLGifFooter *gifFooter;
/**
 数据库
 */
//@property (nonatomic,strong)FMDatabase *dataBase;
/**
 消息数组
 */
@property (nonatomic,strong)NSMutableArray *messageArray;
@property (nonatomic, weak) PlayerManager *playM;
/** <#属性注释#> */
@property (nonatomic, strong) AVPlayer *paly;
/**图片管理器*/
@property (nonatomic, strong) HXPhotoManager *photoManager;
/** 是否被拉黑 */
@property (nonatomic, assign) BOOL isBannend;
/** 是否在房间内 */
@property (nonatomic, assign) BOOL isInRoom;
/** 是否关注label */
@property (nonatomic, strong) UILabel *focusOnLabel;

@end

@implementation ChatDetailsVC
- (instancetype)init {
    if (self = [super init]) {
        
        self.playM = [PlayerManager sharedInstance];
        self.playM.delegate = self;
    }
    return self;
}
// 创建“导航栏”
- (void)createNav {
    // 在主线程异步加载，使下面的方法最后执行，防止其他的控件挡住了导航栏
    dispatch_async(dispatch_get_main_queue(), ^{
        UIView *nav = [UIView initX:0 y:0 width:APPSIZE.width height:TOP_SPACE88or64];
        nav.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:nav];
        self.labTitle = [UILabel initX:APPSIZE.width/2-60 y:TOP_SPACE20or44 width:120 height:44];
        self.labTitle.labFont([UIFont fontWithName:@"KohinoorTelugu-Medium" size:18])
        .labTextColor([UIColor blackColor])
        .labText(self.buddyModel.nickname)
        .labAlignmentCenter();
        [nav addSubview:self.labTitle];
        UIButton *btnLeft = [UIButton initX:5 y:TOP_SPACE20or44 width:80 height:44];
        btnLeft.imageView.contentMode = UIViewContentModeScaleAspectFit;
        btnLeft.btnImageUIControlStateNormal( image(@"icon_nav_back"))
        .btnImageEdgeInsets(1, 0,10, 40);
        btnLeft.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [nav addSubview:btnLeft];
        
        [btnLeft whenTapped:^{
           // [self InAChatRoom:0];//退出房间发送通知
            [self requstExitChat];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        UIButton *btnRight = [UIButton initX:APPSIZE.width-50 y:TOP_SPACE20or44 width:50 height:44];
        btnRight.imageView.contentMode =  UIViewContentModeScaleAspectFill;
        btnRight.btnImageUIControlStateNormal(image(@"个人资料-更多"))
        .btnImageEdgeInsets(0, 0, 0, 20)
        .btnTitleColorUIControlStateNormal([UIColor redColor]);
        btnRight.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [nav addSubview:btnRight];
        
        [btnRight whenTapped:^{
            NSLog(@"资料");
            ChatPersonDetailViewController *vc = [[ChatPersonDetailViewController alloc]init];
            vc.userId = self.buddyModel.ID;
            [self.navigationController pushViewController:vc animated:YES];
        }];
        [nav addSubview:self.focusOnLabel];
        [self.focusOnLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btnRight.mas_centerY);
            make.right.mas_equalTo(btnRight.mas_left).offset(-20);
            make.height.mas_offset(30);
            make.width.mas_offset(51);
        }];
        self.focusOnLabel.hidden = YES;
        [self.focusOnLabel whenTapped:^{
            NSLog(@"关注");
            [self clickFocusOn];
        }];
//        UIButton *bookBtn = [UIButton initX:APPSIZE.width-100 y:TOP_SPACE20or44 width:50 height:44];
//        bookBtn.imageView.contentMode =  UIViewContentModeScaleAspectFill;
//        bookBtn.btnImageUIControlStateNormal(image(@"发布"))
//        .btnImageEdgeInsets(1, 30, 0, 0);
//        bookBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//        [nav addSubview:bookBtn];
//
//        [bookBtn whenTapped:^{
//            NSLog(@"发布");
//            ChatDetalisPersonalSetVC *vc = [[ChatDetalisPersonalSetVC alloc]init];
//            [self.navigationController pushViewController:vc animated:YES];
//        }];
    });
    [self checkFollow];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //self.navigationController.navigationBarHidden = NO;
  //  [self requsetGroupData];
//    [self createNav];
    [self requsteShieldingData];//是否拉黑
    [self requstIMINChatRoom];//在聊天室内
    [self addSocket];
}
-(void)viewDidDisappear:(BOOL)animated{
    //[self InAChatRoom:0];//退出房间发送通知
}
- (NSMutableDictionary *)allGroupDic {
    if (!_allGroupDic) {
        _allGroupDic = [[NSMutableDictionary alloc]init];
    }
    return _allGroupDic;
}
- (NSMutableArray *)allGroupArray {
    if (!_allGroupArray) {
        _allGroupArray = [[NSMutableArray alloc]init];
    }
    return _allGroupArray;
}
- (NSMutableDictionary *)dic
{
    if (!_dic) {
        _dic = [[NSMutableDictionary alloc]init];
    }
    return _dic;
}

- (NSMutableArray *)modelArray
{
    if (!_modelArray) {
        _modelArray = [[NSMutableArray alloc]init];
    }
    return _modelArray;
}

-(UITableView *)chatTableV{
    if (!_chatTableV) {
        _chatTableV = [[UITableView alloc]initWithFrame:CGRectMake(0,TOP_SPACE88or64, APPSIZE.width, APPSIZE.height-TOP_SPACE88or64-BOTTOM_SPACE34or0-50) ];
        _chatTableV.backgroundColor=RGBA(245, 245, 245, 1);
        _chatTableV.rowHeight = UITableViewAutomaticDimension;
        self.chatTableV.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.chatTableV.scrollIndicatorInsets= UIEdgeInsetsMake(0, 0, 0, 0);
        self.chatTableV.tableFooterView = [[UIView alloc] init];
        self.chatTableV.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        if (@available(iOS 11.0, *)) {
            
            self.chatTableV.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }else {
            if (@available(iOS 13.0, *)) {
                self.chatTableV.automaticallyAdjustsScrollIndicatorInsets = NO;
            } else {
                // Fallback on earlier versions
            }
        }
        [  _chatTableV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _chatTableV.delegate = self;
        _chatTableV.dataSource =self;
        self.chatTableV.mj_header = self.gifHeader;
        self.chatTableV.mj_footer = self.gifFooter;

        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(tap)];
        tap.cancelsTouchesInView = NO; //切记,否则cell不能点击 ,collectionview同样如此
        [_chatTableV addGestureRecognizer:tap];
    }
    
    return _chatTableV;
}
-(void)addNav{
    
    UIView *nav = [UIView initX:0 y:0 width:APPSIZE.width height:TOP_SPACE88or64];
    nav.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:nav];
    self.labTitle = [UILabel initX:APPSIZE.width/2-60 y:TOP_SPACE20or44 width:120 height:44];
    self.labTitle.labFont([UIFont fontWithName:@"KohinoorTelugu-Medium" size:18])
    .labTextColor([UIColor blackColor])
    .labText(self.buddyModel.nickname)
    .labAlignmentCenter();
    [nav addSubview:self.labTitle];
    
    UIButton *btnLeft = [UIButton initX:5 y:TOP_SPACE20or44 width:80 height:44];
    btnLeft.imageView.contentMode = UIViewContentModeScaleAspectFit;
    btnLeft.btnImageUIControlStateNormal( image(@"icon_nav_back"))
    .btnImageEdgeInsets(1, 0,10, 40);
    btnLeft.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [nav addSubview:btnLeft];
    
    [btnLeft whenTapped:^{
        [self.keyboard resignFirstResponder];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    _rightBtn = [UIButton initX:APPSIZE.width-100 y:TOP_SPACE20or44 width:80 height:44];
    _rightBtn.btnTitleUIControlStateNormal(@"完成")
    .btnTitleFont(kFont(15))
    .btnTitleColorUIControlStateNormal(RGBA(154, 154, 154, 1));
    _rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [nav addSubview:  _rightBtn];
    [_rightBtn whenTapped:^{
        NSLog(@"完成");
    }];

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //http://xz.yueyuecms.com/2d9rbfcoski1bpe66jwh.m4a
    //    NSURL * url  = [NSURL URLWithString:@"http://xz.yueyuecms.com/var4viyspr5otqvezfl6.m4a"];
    //
    //    AVPlayerItem * songItem = [[AVPlayerItem alloc]initWithURL:url];
    //    self.paly = [[AVPlayer alloc]initWithPlayerItem:songItem];
    //    [self.paly play];
    //[self addNav];
    [self createNav];
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:self.chatTableV];
    UITapGestureRecognizer *tableViewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentTableViewTouchInSide)];
    tableViewGesture.numberOfTapsRequired = 1;
    tableViewGesture.cancelsTouchesInView = NO;
    [self.chatTableV addGestureRecognizer:tableViewGesture];
    // [self requestData];
    _inputController = [[LLInputController alloc] init];
    _inputController.view.frame = CGRectMake(0, self.view.frame.size.height - TTextView_Height - Bottom_SafeHeight, self.view.frame.size.width, TTextView_Height + Bottom_SafeHeight);
    _inputController.view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _inputController.delegate = self;
//    _inputController.isGroup = NO;
    _inputController.isGroup = YES;
    [_inputController.inputBar.moreButton setImage:kImage(@"icon_more") forState:UIControlStateNormal];
    [_inputController.inputBar.faceButton setImage:kImage(@"icon_emoj") forState:UIControlStateNormal];
    [_inputController.inputBar.keyboardButton setImage:kImage(@"tool_keyboard") forState:UIControlStateNormal];
    [_inputController.inputBar.micButton setImage:kImage(@"icon_yuyin") forState:UIControlStateNormal];
    [self addChildViewController:_inputController];
    [self.view addSubview:_inputController.view];
    //表情键盘
    //         YHExpressionKeyboard *keyboard = [[YHExpressionKeyboard alloc] initWithViewController:self aboveView:self.chatTableV];
    //        // keyboard.backgroundColor = [UIColor redColor];
    //         _keyboard = keyboard;
    [self requestHistoryList];
    self.pagNumber = 0;
    
//    [[ChatIMSend share]chatIMDelelgete];
//    ChatIMSend *im = [[ChatIMSend alloc]init];
//    im.chatIMDelelgete =self;
    ///获取通知中心,接收通知,调用方法

       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(text:) name:@"noti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleterChatPerson:) name:@"deleterChatPerson" object:nil];
   // [self InAChatRoom:1];//进入房间发送通知
}
#pragma mark - 音频播放代理
- (void)currentPlayerStatus:(ETPlayerStatus)playerStatus{
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.inputController reset];
}
#pragma mark - 键盘代理
- (void)inputController:(LLInputController *)inputController didChangeHeight:(CGFloat)height
{
    
    
    NSLog(@"hetig===%f",height);
    __weak typeof(self) ws = self;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.chatTableV.frame = CGRectMake(0,TOP_SPACE88or64-height+TTextView_Height + Bottom_SafeHeight, APPSIZE.width, APPSIZE.height-TOP_SPACE88or64-BOTTOM_SPACE34or0-50);
        CGRect msgFrame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.inputController.view.frame));
        msgFrame.size.height = ws.view.frame.size.height - height;
        CGRect inputFrame = ws.inputController.view.frame;
        inputFrame.origin.y = msgFrame.origin.y + msgFrame.size.height;
        inputFrame.size.height = height;
        ws.inputController.view.frame = inputFrame;
        
    } completion:nil];
}


#pragma mark - 发送文字或者语音消息
- (void)inputController:(LLInputController *)inputController didSendMessage:(id)msg {
    if (self.isBannend) {
        [self showText:@"您已被拉黑"];
        return;
    }
    if ([msg isKindOfClass:[NSString class]]) {
        NSLog(@"发送的文本消息 内容====%@",msg);
        UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
        //构造消息内容
        //    NSDate *datenow = [NSDate date];//现在时间
        //    NSString *timenow=[self getNowTimeTimestamp3];
        //    NSString *contenttemp=@"{\"from\": \"%@\",\"to\": \"%@\",\"cmd\":\"%@\",\"createTime\": \"%@\",\"msgType\": \"%@\",\"chatType\":\"%@\",\"group_id\":\"%@\",\"content\": \"%@\"}";
        NSString *uuid = [[NSUUID UUID] UUIDString];
        NSString *timeString = [self getNowTimeTimestamp];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic[@"avatarUrl"] =model.avatar;//头像
        dic[@"chatType"] = @(2);//聊天类型
        dic[@"content"] = msg;// 消息内xing
        dic[@"from"] = @(model.ID);//自己的id
        dic[@"msgType"] = @(0);//消息类型0:text、1:image、2:voice、3:vedio、4:Tips、5:news
        NSString *msgId =[NSString stringWithFormat:@"%@%@",uuid,timeString];//唯一标识
        dic[@"msgUuid"] = msgId;//唯一标识
        dic[@"nickName"] = model.nickname;//自己的昵称
        dic[@"to"] = @(self.buddyModel.ID);//接收者id
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject: dic
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //发送消息方法
        [[ChatIMSend share]sendMessage:str];
       // [self sendMessage:str cmd:(Byte)11];
        dic[@"time"] = timeString;
        [self addMessageWithDictionary:dic isSelf:YES];
        return;
    }
    if ([msg isKindOfClass:[AVURLAsset class]]) {
        AVURLAsset *audioAsset = msg;
        NSURL *assetUrl = audioAsset.URL;
        NSLog(@"adui===%@",audioAsset);
        int duration = (int)CMTimeGetSeconds(audioAsset.duration);
        int length = (int)[[[NSFileManager defaultManager] attributesOfItemAtPath:[audioAsset.URL path] error:nil] fileSize];
        NSLog(@"发送的语音消息 语音长度====%d 长度== %d",duration,length);
        NSString * urlStr = [assetUrl absoluteString];
        [RequestManager postFileWithFilepath:urlStr withName:@"video/mp3" withSuccess:^(id  _Nullable response) {
            NSLog(@"dddddd====%@",response);
            NSLog(@"repuhjh===%@",response[@"data"][0][@"url"]);
            if ([response[@"status"]intValue]== 200) {
                UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
                
                NSString *uuid = [[NSUUID UUID] UUIDString];
                NSString *timeString = [self getNowTimeTimestamp];
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                dic[@"avatarUrl"] =model.avatar;//头像
                dic[@"chatType"] = @(2);//聊天类型
                dic[@"content"] = response[@"data"][0][@"url"];// 消息内xing
                dic[@"from"] = @(model.ID);//自己的id
                dic[@"msgType"] = @(2);//消息类型0:text、1:image、2:voice、3:vedio、4:Tips、5:news
                dic[@"msgUuid"] = [NSString stringWithFormat:@"%@%@",uuid,timeString];//唯一标识
                dic[@"nickName"] = model.nickname;//自己的昵称
                dic[@"to"] = @(self.buddyModel.ID);//接收者id
                dic[@"duration"] = @(duration);
                
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject: dic
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
                
                NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                //发送消息方法
              //  [self sendMessage:str cmd:(Byte)11];
                //发送消息方法
                [[ChatIMSend share]sendMessage:str];
                dic[@"time"] = timeString;
                
                [self addVioceDic:dic];
            }
            
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"err===%@",error);
        }];
        
        return;
    }
}

- (void)inputController:(LLInputController *)inputController didSelectMoreCell:(LLInputMoreCell *)cell {
    if (self.isBannend) {
        [self showText:@"您已被拉黑"];
        return;
    }
    if ([cell.data.title isEqualToString:@"相片"]) {
        NSLog(@"点击了相册");
        __weak typeof(*&self) weakSelf = self;
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
        //默认为NO，为YES时可以多选视频/gif/图片，和照片共享最大可选张数maxImagesCount的限制
        imagePickerVc.allowPickingMultipleVideo = YES;//这样GIF的代理才会被执行
        //最大选取数
        imagePickerVc. maxImagesCount = 1;
        //是否可以选中动态图
        imagePickerVc.allowPickingGif = YES;
        
        //预览图片
        imagePickerVc.allowPreview = NO;
        // You can get the photos by block, the same as by delegate.
        // 你可以通过block或者代理，来得到用户选择的照片.
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            PHAsset *asset = assets[0];
            if ([[asset valueForKey:@"filename"] containsString:@"GIF"]){
                NSLog(@"选择了动图");
                [weakSelf fetchImageWithAsset:asset];
                
                
                
                return;
            }
            NSLog(@"assts===%@",assets);
            NSLog(@"photos===%@",photos);
            
            
            [RequestManager uploadImagesWith:photos withSuccess:^(id  _Nullable response) {
                NSLog(@"repon===%@",response);
                NSString *uuid = [[NSUUID UUID] UUIDString];
                NSString *timeString = [self getNowTimeTimestamp];
               NSString *msg =  [NSString stringWithFormat:@"%@%@",uuid,timeString];//唯一标识
                [self requsetImageData:response[@"data"][0][@"url"] msgID:msg];//发送图片
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                dic[@"tag"] = @"4";
                UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
                ChatDetailsHistoryModel *historyModel = [[ChatDetailsHistoryModel alloc]init];
                historyModel.avatar = model.avatar;
                historyModel.nickname = model.nickname;
                historyModel.content =response[@"data"][0][@"url"];//图片链接
                historyModel.time = timeString;
                historyModel.uuid = msg;
                historyModel.privateChat = YES;//是否是私聊
                if (self.isInRoom) {
                    historyModel.remoteRead = 1;
                }else{
                    historyModel.remoteRead = 0;
                }
                dic[@"historyModel"] = historyModel;
                ChatDetailsModel *imageModel = [ChatDetailsModel initWithDictionary:dic];
                [self.messageArray addObject:imageModel];
                
                
                //更新ui
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSIndexPath* insertion = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
                    [self.chatTableV reloadData];
                    [self tableviewScrollToRowWithIndex:insertion];
                });
                
            } fail:^(NSError * _Nullable error) {
                NSLog(@"errr==%@",error);
            } withProgress:^(int64_t bytesRead, int64_t totalBytes) {
                
            }];
            
        }];
        imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }
    if ([cell.data.title isEqualToString:@"拍摄"]) {
        NSLog(@"点击了拍照");
        // 创建UIImagePickerController实例
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        // 设置代理
        imagePickerController.delegate = self;
        // 是否显示裁剪框编辑（默认为NO），等于YES的时候，照片拍摄完成可以进行裁剪
        imagePickerController.allowsEditing = YES;
        // 设置照片来源为相机
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        // 设置进入相机时使用前置或后置摄像头
        imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        imagePickerController.modalPresentationStyle = UIModalPresentationFullScreen;
        // 展示选取照片控制器
        [self presentViewController:imagePickerController animated:YES completion:nil];
        return;
    }
    if ([cell.data.title isEqualToString:@"视频通话"]) {
        
    }
}
#pragma mark - UIImagePickerControllerDelegate
// 完成图片的选取后调用的方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // 选取完图片后跳转回原控制器
    [picker dismissViewControllerAnimated:YES completion:nil];
    /* 此处参数 info 是一个字典，下面是字典中的键值 （从相机获取的图片和相册获取的图片时，两者的info值不尽相同）
     * UIImagePickerControllerMediaType; // 媒体类型
     * UIImagePickerControllerOriginalImage; // 原始图片
     * UIImagePickerControllerEditedImage; // 裁剪后图片
     * UIImagePickerControllerCropRect; // 图片裁剪区域（CGRect）
     * UIImagePickerControllerMediaURL; // 媒体的URL
     * UIImagePickerControllerReferenceURL // 原件的URL
     * UIImagePickerControllerMediaMetadata // 当数据来源是相机时，此值才有效
     */
    // 从info中将图片取出，并加载到imageView当中
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [RequestManager uploadImagesWith:@[image] withSuccess:^(id  _Nullable response) {
        NSLog(@"repon===%@",response);
        NSString *uuid = [[NSUUID UUID] UUIDString];
        NSString *timeString = [self getNowTimeTimestamp];
    NSString *msg  =   [NSString stringWithFormat:@"%@%@",uuid,timeString];//唯一标识
        [self requsetImageData:response[@"data"][0][@"url"] msgID:msg];//发送图片
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic[@"tag"] = @"4";
        UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
        ChatDetailsHistoryModel *historyModel = [[ChatDetailsHistoryModel alloc]init];
        historyModel.avatar = model.avatar;
        historyModel.nickname = model.nickname;
        historyModel.content =response[@"data"][0][@"url"];//图片链接
        historyModel.time = timeString;
        historyModel.uuid = msg;
        historyModel.privateChat = YES;//是否是私聊
        if (self.isInRoom) {
            historyModel.remoteRead = 1;
        }else{
            historyModel.remoteRead = 0;
        }
        dic[@"historyModel"] = historyModel;
        ChatDetailsModel *imageModel = [ChatDetailsModel initWithDictionary:dic];
        [self.messageArray addObject:imageModel];
        
        
        //更新ui
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSIndexPath* insertion = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
            [self.chatTableV reloadData];
            [self tableviewScrollToRowWithIndex:insertion];
        });
        
    } fail:^(NSError * _Nullable error) {
        NSLog(@"errr==%@",error);
    } withProgress:^(int64_t bytesRead, int64_t totalBytes) {
        
    }];

//    // 创建保存图像时需要传入的选择器对象（回调方法格式固定）
//    SEL selectorToCall = @selector(image:didFinishSavingWithError:contextInfo:);
//    // 将图像保存到相册（第三个参数需要传入上面格式的选择器对象）
//    UIImageWriteToSavedPhotosAlbum(image, self, selectorToCall, NULL);
}

// 取消选取调用的方法
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}
/**
 通过资源获取图片的数据
 
 @param mAsset 资源文件
 @param imageBlock 图片数据回传
 */
- (void)fetchImageWithAsset:(PHAsset*)mAsset {
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];

     options.synchronous = YES;

     options.networkAccessAllowed = YES;

     options.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
  //  options.networkAccessAllowed = YES; 加上这句就好了，原因是 图片可能会在icloud上， 而你并没有允许网络获取icloud 图片功能！
    [[PHImageManager defaultManager] requestImageDataForAsset:mAsset options: options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        // 直接得到最终的 NSData 数据
        NSLog(@"info===%@",info);
        NSURL *fileURL = [info valueForKey:@"PHImageFileURLKey"];
        NSLog(@"fileURL===%@",fileURL);
        NSArray *array = @[imageData];
        [RequestManager uploadImagesWith:array withSuccess:^(id  _Nullable response) {
            NSLog(@"repson===%@",response);
            if ([response[@"status"]intValue]==200) {
                NSString *uuid = [[NSUUID UUID] UUIDString];
                NSString *timeString = [self getNowTimeTimestamp];
                NSString *msg = [NSString stringWithFormat:@"%@%@",uuid,timeString];//唯一标识
                [self requsetImageData:response[@"data"][0][@"url"] msgID:msg];//发送图片
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                dic[@"tag"] = @"4";
                UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
                ChatDetailsHistoryModel *historyModel = [[ChatDetailsHistoryModel alloc]init];
                historyModel.avatar = model.avatar;
                historyModel.nickname = model.nickname;
                historyModel.content =response[@"data"][0][@"url"];//图片链接
                historyModel.uuid = msg;
                historyModel.time = timeString;
                historyModel.privateChat = YES;//是否是私聊
                if (self.isInRoom) {
                    historyModel.remoteRead = 1;
                }else{
                    historyModel.remoteRead = 0;
                }
                dic[@"historyModel"] = historyModel;
                ChatDetailsModel *imageModel = [ChatDetailsModel initWithDictionary:dic];
                [self.messageArray addObject:imageModel];
                
                
                //更新ui
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSIndexPath* insertion = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
                    [self.chatTableV reloadData];
                    [self tableviewScrollToRowWithIndex:insertion];
                });
                
            }
        } fail:^(NSError * _Nullable error) {
            NSLog(@"err===%@",error);
        } withProgress:^(int64_t bytesRead, int64_t totalBytes) {
            
        }];
        
        
    }];
}

- (void)commentTableViewTouchInSide{
    [self.inputController reset];//键盘收起
}
#pragma mark - 相册选中视频的回调
-(void)imagePickerController:(TZImagePickerController *)picker
       didFinishPickingVideo:(UIImage *)coverImage
                sourceAssets:(PHAsset *)asset{
    [[TZImageManager manager] getVideoOutputPathWithAsset:asset presetName:AVAssetExportPresetLowQuality success:^(NSString *outputPath) {
        // NSData *data = [NSData dataWithContentsOfFile:outputPath];
        AVURLAsset* audioAsset =[AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:outputPath] options:nil];
          CMTime audioDuration = audioAsset.duration;
          float audioDurationSeconds = CMTimeGetSeconds(audioDuration);
        int audioTime = (int)audioDurationSeconds;
          NSLog(@"时长====%f",audioDurationSeconds);
        NSLog(@"视频导出到本地完成,沙盒路径为:%@",outputPath);
        //先上传服务器视频，拿到url
        [RequestManager postFileWithFilepath:outputPath withName:@"video/mpeg4" withSuccess:^(id  _Nullable response) {
            NSLog(@"repoiiiVideo===%@",response);
            if ([response[@"status"]intValue]==200) {
                [self requstVideoData:response[@"data"][0][@"url"] videoTime:audioTime];//上传url
            }
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"errr====%@",error);

        }];
        // Export completed, send video here, send by outputPath or NSData
        // 导出完成，在这里写上传代码，通过路径或者通过NSData上传
    } failure:^(NSString *errorMessage, NSError *error) {
        NSLog(@"视频导出失败:%@,error:%@",errorMessage, error);
    }];
    NSLog(@"选中了视频");
}
// 如果用户选择了一个gif图片且allowPickingMultipleVideo是NO，下面的代理方法会被执行
// 如果allowPickingMultipleVideo是YES，将会调用imagePickerController:didFinishPickingPhotos:sourceAssets:isSelectOriginalPhoto:
#pragma mark - GIF的回调
//如果allowPickingMultipleVideo是YES，将会调用imagePickerController:didFinishPickingPhotos:sourceAssets:isSelectOriginalPhoto:
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingGifImage:(UIImage *)animatedImage sourceAssets:(PHAsset *)asset {
    NSLog(@"git回调333");
    
}

-(void)requestData{
    NSArray *dataArray = @[
        @{
            @"tag" : @"1",
            @"content" : @"我是纯文本只有文字1",
        },
        @{
            @"tag" : @"2",
            @"image" : @"url",
            @"content" : @"我是纯文本只有文字1",
        },
        
        @{
            @"tag" : @"5",
            @"content" : @"5月13日 上午8:00",
        },
        @{
            @"tag" : @"5",
            @"image" : @"url",
            @"content" : @"恭喜！游戏兴趣群组创建成功，快邀请小伙伴们加入吧",
        },
        @{
            @"tag" : @"7",
            @"image" : @"url",
            @"title" : @"公告标题",
            @"subTitle" : @"恭喜！游戏兴趣群组创建成功，快邀请小伙伴们加入吧"
        },
        @{
            @"tag" : @"6",//6
            @"image" : @"url",
            @"name" : @"白茶",
            @"introduceString" : @"新人入群，跟大家Say Hello啦！",
            @"content" : @"我是纯文本只有文字1",
        },
        
        @{
            @"tag" : @"4",
            @"content" : @"我是纯文本只有文字3",
            @"photoName" : @"测试头像2",
        },
        @{
            @"tag" : @"3",
            @"photoName" : @"屏幕快照 2017-08-10 下午4.04.50",
            @"content" : @"我是纯文本只有文字1",
        },
        
    ];
    
    for (int i = 0; i < dataArray.count; i++) {
        NSDictionary *dic = dataArray[i];
        NSLog(@"dic===%@",dic);
        CGFloat addFontSize = [[[NSUserDefaults standardUserDefaults] valueForKey:@"setSystemFontSize"] floatValue];
        ChatDetailsModel *model = [ChatDetailsModel initWithDictionary:dic];
        ChatTextLayout *layout = [[ChatTextLayout alloc] init];
        [self.modelArray addObject:model];
    }
    
}
#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"count====%lu",(unsigned long)self.modelArray.count);
    return self.messageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatDetailsModel *model = self.messageArray[indexPath.row];
    NSString *modelName = [NSString stringWithUTF8String:object_getClassName(model)];
    ChatDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:modelName];
    if (cell == nil) {
        cell = [[ChatDetailsCell alloc]initWithModel:model];
    }
    [cell setDataWithModel:model];
    cell.cellDelegate = self;
    cell.isInRoom = self.isInRoom;
    //[self.dic setObject:cell forKey:@(indexPath.row)];
    return cell;
}
- (void)headerClick:(NSString *)userId {
    MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc] init];
    vc.ID = [userId longValue];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - @protocol UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [_keyboard endEditing];
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
    
}

#pragma mark - 点击了
-(void)tap{
    [self.keyboard endEditing];
    // [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
    //[self.view layoutIfNeeded];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //[self.keyboard endEditing];
    //   [self.keyboard resignFirstResponder];
    //    [self.inputController reset];//键盘收起
    
    // NSLog(@"scrollView");
}


#pragma mark - 获取我加入的群组工会
-(void)requsetGroupData{
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    dic[@"userId"] = @(model.ID);
    [RequestManager getMyDataWithParameters:dic UrlString:@"/api/personalCluster/myJoinedCluster" Success:^(id  _Nullable response) {
        //   NSLog(@"response==%@",response);
        self.allGroupDic[@"data"]= response[@"data"];
        if ([response[@"status"]intValue]==200) {
            NSArray *array = response[@"data"];
            
            if (array != nil && ![array isKindOfClass:[NSNull class]] && array.count != 0){
                //执行array不为空时的操作
                for (NSDictionary *dic in array) {
                    [self.allGroupArray addObject:dic];
                }
                
            }else{
                
            }
        }
        [self addSocket];
        // NSLog(@"response==%@",response);
    } withFail:^(NSError * _Nullable error) {
        //NSLog(@"err==%@",error);
    }];
}
#pragma mark - 长连接
-(void)addSocket{
    //线上
//    self.hostString = @"49.233.239.51";
//    self.portString = @"45000";
    self.hostString = IMhost;
    self.portString = @"45000";
    self.tcpScoket = [[AsyncSocket alloc]initWithDelegate:self];
    NSError *error;
    BOOL isConnect = [self.tcpScoket connectToHost: self.hostString onPort:[self.portString intValue] withTimeout:-1 error:&error];
    if (isConnect) {
        NSLog(@"连接成功");
    }else {
        NSLog(@"连接失败");
    }
    //[self.tcpScoket setRunLoopModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
    [self.tcpScoket readDataWithTimeout:-1 tag:0];
}
-(NSString *)getNowTimeTimestamp3{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss SSS"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    
    return timeSp;
}

//发送socket消息
-(void)sendMessage:(NSString *)content cmd:(Byte)cmd{
    //要发送的总体数据
    NSMutableData *mData = [[NSMutableData alloc] init];
    
    //消息体内容 转换成nsdata即可。连接上服务器后，首先需要登录，发送账号密码过去.json格式。
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    
    NSInteger datalenth =data.length;  //获取消息体内容转换成nsdata后的长度
    
    SendDataHead senddata;
    senddata.version=0x01; //协议版本号 目前为1
    senddata.Mask=0x81;    //mask数据。目前固定
    //senddata.cmdByte=0x05; //消息cmd命令。。根据消息类型不同而不同。例如登录操的cmd为0x05
    senddata.cmdByte=cmd;
    
    //以下是拼接 消息内容的长度 转换成字节放到包头里面 消息体长度占四个字节。是个int数据
    senddata.bodyLen[0]=(Byte)((datalenth & 0xFF000000)>>24);
    senddata.bodyLen[1]=(Byte)((datalenth & 0x00FF0000)>>16);
    senddata.bodyLen[2]=(Byte)((datalenth & 0x0000FF00)>>8);
    senddata.bodyLen[3]=(Byte)((datalenth & 0x000000FF));
    
    //将包头内容转换成nsddata
    NSData * headdata = [[NSData alloc]initWithBytes:&senddata length:sizeof(SendDataHead)];
    
    
    [mData appendData:headdata]; //拼放入包头
    [mData appendData:data];   //放入消息体
    
    [self.tcpScoket writeData:mData withTimeout:-1 tag:0];  //写入数据
}

#pragma mark - 收到的数据
-(void)dataReceived:(NSString *)str {
    NSDictionary *modelDic=  [self dictionaryWithJsonString:str];
    NSLog(@"pppp===%@",modelDic);
    NSLog(@"to===%@",modelDic[@"data"][@"to"]);
    NSLog(@"modeid===%ld",(long)self.buddyModel.ID);
    if ([modelDic[@"notityType"]isEqualToString:@"108"]) {
        self.isInRoom = YES;
        [self.chatTableV reloadData];
    }else if ([modelDic[@"notityType"]isEqualToString:@"109"]){
        self.isInRoom = NO;
    }
    
    if ([modelDic[@"userId"]intValue]==self.buddyModel.ID) {
        
        if ([modelDic[@"notityType"]isEqualToString:@"106"]) {//拉黑
            if ([modelDic[@"content"]isEqualToString:@"true"]) {
                self.isBannend = YES;
            }else{
                self.isBannend = NO;
            }
            
        }
    
    }
    if ([modelDic[@"chatType"]intValue]==18){
        self.pagNumber = 0;
        [self.messageArray removeAllObjects];
        [self requestHistoryList];
    }
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    if ([modelDic[@"data"][@"from"]intValue] == self.buddyModel.ID && [modelDic[@"data"][@"to"]intValue] == model.ID) {
       
        ChatDetailsHistoryModel *historyModel = [ChatDetailsHistoryModel mj_objectWithKeyValues:modelDic[@"data"]];
        historyModel.avatar =modelDic[@"data"][@"avatarUrl"];
        historyModel.nickname = modelDic[@"data"][@"nickName"];
        NSLog(@"modeUrl===%@",modelDic[@"avatarUrl"]);
        NSLog(@" modelDicname===%@", modelDic[@"nickName"]);
        NSLog(@"mstuyo===%@",historyModel.msgType);
        if ([historyModel.msgType isEqualToString:@"0"]) {//0.文字
            NSMutableDictionary *historyDic = [[NSMutableDictionary alloc]init];
            historyDic[@"tag"] = @"2";
            historyDic[@"historyModel"] = historyModel;
            
            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:historyDic];
            [self.messageArray addObject:chatModel];
            
        }else if ([historyModel.msgType isEqualToString:@"1"]){//1.图片
            NSMutableDictionary *historyDic = [[NSMutableDictionary alloc]init];
            historyDic[@"tag"] = @"3";
            historyDic[@"historyModel"] = historyModel;
            
            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:historyDic];
            [self.messageArray addObject:chatModel];
        }else if ([historyModel.msgType isEqualToString:@"2"]){//2.语音
            NSMutableDictionary *historyDic = [[NSMutableDictionary alloc]init];
            historyDic[@"tag"] = @"8";
            historyDic[@"historyModel"] = historyModel;
            
            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:historyDic];
            [self.messageArray addObject:chatModel];
        }else if ([historyModel.msgType isEqualToString:@"3"]){//3.视频
            NSMutableDictionary *historyDic = [[NSMutableDictionary alloc]init];
            historyDic[@"tag"] = @"10";
            historyDic[@"historyModel"] = historyModel;
            
            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:historyDic];
            [self.messageArray addObject:chatModel];
        }
        //更新ui
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.messageArray != nil && ![self.messageArray isKindOfClass:[NSNull class]] && self.messageArray.count != 0){
                //执行array不为空时的操作
                NSIndexPath* insertion = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
                [self.chatTableV reloadData];
                [self tableviewScrollToRowWithIndex:insertion];
            }else{
                
            }
         
        });
        
    }
    
    
    
    
}
-(void)addMessage:(NSString *)str{
    
    
    //   NSDictionary *ddd  = (NSDictionary *)str;
    //    ChatDetailsModel *mode = [ChatDetailsModel mj_objectWithKeyValues:str];
    //
    //NSDictionary *test =     [self dictionaryWithJsonString:str];
    //    NSLog(@"test===%@",test);
    //   // NSString *json = [self dictionaryToJson:ddd];
    //  //  NSLog(@"json===%@",json);
    //   // [self addMessageWithDictionary:test isSelf:NO];
    //
    ////    self.textV.text = [self.textV.text stringByAppendingFormat:@"%@\n\n\n",str];
    ////    [self.textV scrollRangeToVisible:[self.textV.text rangeOfString:str options:NSBackwardsSearch]];
}

- (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    //    NSRange range = {0,jsonString.length};
    
    
    
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"[]{}（#%-*+=_）\\|~(＜＞$%^&*)_+ "];
    NSString * hmutStr = [[mutStr componentsSeparatedByCharactersInSet: doNotWant]componentsJoinedByString: @""];
    
    NSLog(@"humStr is %@",hmutStr);
    
    return hmutStr;
}
//json格式字符串转字典：

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        
        return nil;
        
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *err;
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                         
                                                        options:NSJSONReadingMutableContainers
                         
                                                          error:&err];
    
    if(err) {
        
        NSLog(@"json解析失败：%@",err);
        
        return nil;
        
    }
    
    return dic;
    
}
#pragma mark -添加语言模型
-(void)addVioceDic:(NSMutableDictionary*)dic{
    
    dic[@"tag"]= @"9";
    ChatDetailsHistoryModel *historyModel = [[ChatDetailsHistoryModel alloc]init];
    historyModel.avatar = dic[@"avatarUrl"];
    historyModel.nickname =  dic[@"nickName"];
    historyModel.content =dic[@"content"];
    historyModel.duration = [dic[@"duration"]intValue];
    historyModel.uuid = dic[@"msgUuid"];
    historyModel.time = dic[@"time"];
    historyModel.privateChat = YES;//是否是私聊
    if (self.isInRoom) {
        historyModel.remoteRead = 1;
    }else{
        historyModel.remoteRead = 0;
    }
    dic[@"historyModel"] = historyModel;
    ChatDetailsModel *model = [ChatDetailsModel initWithDictionary:dic];
    [self.messageArray addObject:model];
    
    
    //更新ui
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSIndexPath* insertion = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
        [self.chatTableV reloadData];
        [self tableviewScrollToRowWithIndex:insertion];
    });
}
#pragma mark - 数据转换模型
-(void)addMessageWithDictionary:(NSDictionary *)messageDictionary isSelf:(BOOL)isSelf {
    //保存到库  目前不做本地存储
    //    NSString *idString = [NSString stringWithFormat:@"insert into ID%lu (name, category, message, isSelf) values (?, ?, ?, ?);",self.buddyModel.ID];
    //    NSLog(@"idstring===%@",idString);
    ////      NSString * sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (iid integer PRIMARY KEY AUTOINCREMENT, name text, message text, time text, category integer, isSelf bool)",idString];
    //    BOOL success = [_dataBase executeUpdate:idString, self.buddyModel.nickname ,@0, messageDictionary[@"content"], @0];
    //    if (success) {
    //                  NSLog(@"保存到数据库成功");
    //              }else {
    //                  NSLog(@"保存到数据库失败");
    //              }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    
    if (isSelf == YES) {
        dic[@"tag"]= @"1";
    }else{
        dic[@"tag"]= @"2";
    }
    ChatDetailsHistoryModel *historyModel = [[ChatDetailsHistoryModel alloc]init];
    historyModel.avatar = messageDictionary[@"avatarUrl"];
    historyModel.nickname = messageDictionary[@"nickName"];
    historyModel.content =messageDictionary[@"content"];
    historyModel.time = messageDictionary[@"time"];
    historyModel.uuid = messageDictionary[@"msgUuid"];
    historyModel.privateChat = YES;//是否是私聊
    if (self.isInRoom) {
        historyModel.remoteRead = 1;
    }else{
        historyModel.remoteRead = 0;
    }
    dic[@"content"] = messageDictionary[@"content"];
    dic[@"historyModel"] = historyModel;
    ChatDetailsModel *model = [ChatDetailsModel initWithDictionary:dic];
    [self.messageArray addObject:model];
    
    
    //更新ui
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSIndexPath* insertion = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
        [self.chatTableV reloadData];
        [self tableviewScrollToRowWithIndex:insertion];
    });
}
//滑动
-(void)tableviewScrollToRowWithIndex:(NSIndexPath *)lastIndex {
    
    [self.chatTableV scrollToRowAtIndexPath:lastIndex atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}
//AsyncSocketDelegate
//已经连接上
-(void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port{
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    [self addMessage:[NSString stringWithFormat:@"连接上%@\nlocal:%@",host,sock]];
    //[self.connectHostMuArr addObject:host];
    [self.tcpScoket readDataWithTimeout:-1 tag:0];
    NSMutableDictionary *contentDic = [[NSMutableDictionary alloc]init];
    contentDic[@"userId"] = @(model.ID);
    contentDic[@"password"] = @"";
    contentDic[@"userNcikName"] = model.nickname;
    contentDic[@"userAvatarUrl"] = model.avatar;
    contentDic[@"groupJsonStr"] =self.allGroupArray;
    
    //[NSString stringWithFormat:@"{%@}",self.allGroupDic[@"data"]];
    //    NSLog(@"dic====%@",contentDic);
    //    NSLog(@"conter===%@",self.allGroupArray);
    //    NSLog(@"ssss===%@",self.allGroupDic[@"data"]);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: contentDic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSString *content=@"{\"loginname\":\"hello_ios\",\"password\":\"123\"}";
    // NSLog(@"strrr===%@",str);
    //连接成功后，发送账号密码登录
    
    [self sendMessage:str cmd:0x05];
    
    
}

-(void)onSocketDidDisconnect:(AsyncSocket *)sock{
}

-(void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    NSLog(@"data===%@",data);
    NSString *result =[[ NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"string===%@",result);
    NSData *head = [data subdataWithRange:NSMakeRange(0, 7)];//取得包头头部数据  包头目前长度是7位
    NSData *lengthData = [head subdataWithRange:NSMakeRange(3, 4)];//取得长度数据 第3-4位是消息体长度  此处和发送包时拼接的头包的格式是一样的
    
    //以下是将包头的长度的四个字节转换成int数字。
    NSString* hexString = [lengthData convertDataToHexStr];
    NSInteger length = [[hexString hexToDecimal]integerValue];
    
    //从第8个字符节开始一直到length长度的数据，都是消息体正文。
    NSData *contentdata=[data subdataWithRange:NSMakeRange(7, length)];
    
    NSString *msg = [[NSString alloc] initWithData: contentdata encoding:NSUTF8StringEncoding];
    NSLog(@"读到的数据：%@",msg);
    [self dataReceived:msg];
    [self addMessage:[NSString stringWithFormat:@"收到数据：%@",msg]];
    [self.tcpScoket readDataWithTimeout: -1 tag: 0];
}


-(void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag{
    NSLog(@"发送了");
    //    [self addMessage:[NSString stringWithFormat:@"发送了"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/** 读取字节 */
- (int8_t)readRawByte:(NSData *)data headIndex:(int32_t *)index{
    
    if (*index >= data.length) return -1;
    
    *index = *index + 1;
    
    return ((int8_t *)data.bytes)[*index - 1];
}
#pragma mark - 获取时间戳
-(NSString *)getNowTimeTimestamp{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss SSS"]; // 设置想要的格式，hh与HH的区别:分别表示12小时制,24小时制
    
    //设置时区,这一点对时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    
    return timeSp;
    
}
#pragma mark - 创建数据库 目前不做本地存储
//-(void)createData {
//
//    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject];
//    NSString *fileName = [path stringByAppendingPathComponent:@"message.sqlite"];
//    NSLog(@"fileName = %@",fileName);
//
//    _dataBase = [FMDatabase databaseWithPath:fileName];
//
//    if ([_dataBase open]) {
//        NSLog(@"打开数据库成功");
//    }else {
//        NSLog(@"打开数据库失败");
//    }
//
//    NSString *idString = [NSString stringWithFormat:@"ID%ld",(long)self.buddyModel.ID];
//    NSString * sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (iid integer PRIMARY KEY AUTOINCREMENT, name text, message text, time text, category integer, isSelf bool)",idString];
//    //数据库建表
//    BOOL result = [_dataBase executeUpdate:sql];
//    // BOOL newResult = [_dataBase exec]
//    if (result) {
//        NSLog(@"创建表成功");
//    }else {
//        NSLog(@"创建表失败");
//    }
//}
#pragma mark - 读取数据库  目前不做本地存储
/*
-(NSArray *)readData {
    NSString *idString = [NSString stringWithFormat:@"select * from ID%ld",(long)self.buddyModel.ID];
    FMResultSet *resultSet = [_dataBase executeQuery:idString];
    
    if (resultSet) {
        NSMutableArray *nmArray = [NSMutableArray array];
        while ([resultSet next]) {
            NSString *name = [resultSet objectForColumn:@"name"];
            NSString *category = [resultSet objectForColumn:@"category"];
            NSString *message = [resultSet objectForColumn:@"message"];
            NSString *time = [resultSet objectForColumn:@"time"];
            NSString *isSelf = [resultSet objectForColumn:@"isSelf"];
            NSLog(@"name= %@, category= %@, message= %@ time= %@ isSelf= %@",name,category,message,time,isSelf);
            NSMutableDictionary *nmDictionary = [NSMutableDictionary dictionary];
            [nmDictionary setValue:name forKey:@"name"];
            [nmDictionary setValue:@([category integerValue]) forKey:@"tag"];
            [nmDictionary setValue:message forKey:@"content"];
            [nmDictionary setValue:time forKey:@"time"];
            [nmDictionary setValue:@([isSelf integerValue]) forKey:@"isSelf"];
            [nmArray addObject:nmDictionary];
        }
        return nmArray;
        
    }else {
        NSLog(@"查询失败");
    }
    return nil;
}
 */
#pragma mark - im代理
- (void)receivedChatIM:(NSString *)str{
    NSLog(@"str===%@",str);
}
- (MjWLHeader *)gifHeader {
    if (!_gifHeader) {
        _gifHeader = [MjWLHeader headerWithRefreshingBlock:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.pagNumber++;
                [self requestHistoryList];
            });
        }];
    }
    return _gifHeader;
}
- (WLGifFooter *)gifFooter {
    if (!_gifFooter) {
        _gifFooter = [WLGifFooter footerWithRefreshingBlock:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
             
                self.pagNumber = 0;
                [self.messageArray removeAllObjects];
                [self requestHistoryList];
            });
        }];
    }
    return _gifFooter;
}
#pragma mark - 获取聊天历史
-(void)requestHistoryList{
    
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    dic[@"fromId"] =@(model.ID) ;
    NSLog(@"foreimm===%lu",model.ID);
    dic[@"toId"] = @(self.buddyModel.ID);
    NSLog(@"topoo===%lu",self.buddyModel.ID);
    dic[@"startPageIndex"] =@(self.pagNumber);
    dic[@"length"] =@(50) ;
    //线上https://49.233.239.51:20443
    //线下https://192.168.31.106/
    [RequestManager getIMDataWithParameters:dic IMHost:IMPostHost UrlString:@"/api/messageHandler/getAllMessageByUserId" Success:^(id  _Nullable response) {
        [self.chatTableV.mj_header endRefreshing];
        [self.chatTableV.mj_footer endRefreshing];
            
        NSLog(@"repons历史记录==%@",response);
        if ([response[@"status"]intValue]== 200) {
            
            NSArray *array =response[@"data"];
            if (array != nil && ![array isKindOfClass:[NSNull class]] && array.count != 0){
                //执行array不为空时的操作
                // 倒序
                NSMutableArray *resultArr = (NSMutableArray *)[[array reverseObjectEnumerator] allObjects];
                NSLog(@"倒序：%@",resultArr); // 倒序的结果为：2,4,3,5
                for (NSDictionary *dic in resultArr) {
                    ChatDetailsHistoryModel *historyModel = [ChatDetailsHistoryModel mj_objectWithKeyValues:dic];
                    historyModel.privateChat = YES;//是否是私聊
                    if ([historyModel.msgType isEqualToString:@"revoke"]) {
                        NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                        historyModel.content = [NSString stringWithFormat:@"%@撤回了一条消息",historyModel.nickname];
                        historyModel.time =   [self ConvertStrToTime:historyModel.time];
                        messDic[@"historyModel"] = historyModel;
                        messDic[@"tag"] = @"12";
                        ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                        //                           [self.messageArray addObject:chatModel];
                        NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                        
                        [indexes addIndex:0];
                        [self.messageArray insertObject:chatModel atIndex:0];
                    }
                    if ([historyModel.direct isEqualToString:@"OUT"]) {//自己的消息
                        if ([historyModel.msgType isEqualToString:@"text"]) {
                            NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                            messDic[@"historyModel"] = historyModel;
                            messDic[@"tag"] = @"1";
                            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                            //                           [self.messageArray addObject:chatModel];
                            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                            
                            [indexes addIndex:0];
                            [self.messageArray insertObject:chatModel atIndex:0];
                        }else if ([historyModel.msgType isEqualToString:@"image"]){
                            historyModel.content = historyModel.image;
                            NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                            messDic[@"historyModel"] = historyModel;
                            messDic[@"tag"] = @"4";
                            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                            //                           [self.messageArray addObject:chatModel];
                            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                            
                            [indexes addIndex:0];
                            [self.messageArray insertObject:chatModel atIndex:0];
                        }else if ([historyModel.msgType isEqualToString:@"audio"]){//语音
                            NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                            historyModel.content = historyModel.voice;
                            messDic[@"historyModel"] = historyModel;
                            
                            messDic[@"tag"] = @"9";
                            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                            
                            [indexes addIndex:0];
                            [self.messageArray insertObject:chatModel atIndex:0];
                        }else if ([historyModel.msgType isEqualToString:@"video"]){//语音
                            NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                            historyModel.content = historyModel.video;
                            messDic[@"historyModel"] = historyModel;
                            
                            messDic[@"tag"] = @"11";
                            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                            
                            [indexes addIndex:0];
                            [self.messageArray insertObject:chatModel atIndex:0];
                        }
                    }else if ([historyModel.direct isEqualToString:@"IN"]){//别人的消息
                        if ([historyModel.msgType isEqualToString:@"text"]) {
                            NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                            messDic[@"historyModel"] = historyModel;
                            messDic[@"tag"] = @"2";
                            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                            
                            [indexes addIndex:0];
                            [self.messageArray insertObject:chatModel atIndex:0];
                        }else if ([historyModel.msgType isEqualToString:@"image"]){
                            NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                            historyModel.content = historyModel.image;
                            messDic[@"historyModel"] = historyModel;
                            messDic[@"tag"] = @"3";
                            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                            
                            [indexes addIndex:0];
                            [self.messageArray insertObject:chatModel atIndex:0];
                        }else if ([historyModel.msgType isEqualToString:@"audio"]){//语音
                            NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                            historyModel.content = historyModel.voice;
                            messDic[@"historyModel"] = historyModel;
                            
                            messDic[@"tag"] = @"8";
                            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                            
                            [indexes addIndex:0];
                            [self.messageArray insertObject:chatModel atIndex:0];
                        }else if ([historyModel.msgType isEqualToString:@"video"]){//语视频
                            NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
                            historyModel.content = historyModel.video;
                            messDic[@"historyModel"] = historyModel;
                            
                            messDic[@"tag"] = @"10";
                            ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
                            NSMutableIndexSet  *indexes = [NSMutableIndexSet indexSetWithIndex:0];
                            
                            [indexes addIndex:0];
                            [self.messageArray insertObject:chatModel atIndex:0];
                        }
                    }
                }
            }else{
                [self.chatTableV.mj_header endRefreshing];
            }
        }
        [self.chatTableV reloadData];
        if (self.messageArray != nil && ![self.messageArray isKindOfClass:[NSNull class]] && self.messageArray.count != 0){
            //执行array不为空时的操作
            if (self.pagNumber == 0) {
                //第一次进入
                NSIndexPath* insertion = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
                
                [self tableviewScrollToRowWithIndex:insertion];
            }
            
        }else{
            
        }
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"error聊天历史===%@",error);
        [self.chatTableV.mj_header endRefreshing];
        [self.chatTableV.mj_footer endRefreshing];
    }];
}
#pragma mark - 发送视频消息
-(void)requstVideoData:(NSString*)videoUrl videoTime:(int)time{
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    //构造消息内容
    //    NSDate *datenow = [NSDate date];//现在时间
    //    NSString *timenow=[self getNowTimeTimestamp3];
    //    NSString *contenttemp=@"{\"from\": \"%@\",\"to\": \"%@\",\"cmd\":\"%@\",\"createTime\": \"%@\",\"msgType\": \"%@\",\"chatType\":\"%@\",\"group_id\":\"%@\",\"content\": \"%@\"}";
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSString *timeString = [self getNowTimeTimestamp];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    dic[@"avatarUrl"] =model.avatar;//头像
    dic[@"chatType"] = @(2);//聊天类型,2.私聊
    dic[@"content"] = videoUrl;// 消息内xing
    dic[@"from"] = @(model.ID);//自己的id
    dic[@"msgType"] = @(3);//消息类型0:text、1:image、2:voice、3:vedio、4:Tips、5:news
    NSString *msg = [NSString stringWithFormat:@"%@%@",uuid,timeString];//唯一标识
    dic[@"msgUuid"] =msg;
    dic[@"nickName"] = model.nickname;//自己的昵称
    dic[@"to"] = @(self.buddyModel.ID);//接收者id
    dic[@"duration"] = @(time);//视频时长
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: dic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //发送消息方法
   // [self sendMessage:str cmd:(Byte)11];
    [[ChatIMSend share]sendMessage:str];
    //本地数组添加数据
    NSMutableDictionary *addDic = [[NSMutableDictionary alloc]init];
    addDic[@"tag"]= @"11";
 
    ChatDetailsHistoryModel *historyModel = [[ChatDetailsHistoryModel alloc]init];
    historyModel.avatar =model.avatar;
    historyModel.nickname =  model.nickname;//自己的昵称
    historyModel.content =videoUrl;// 消息内xing
    historyModel.time = timeString;
    historyModel.uuid = msg;
    historyModel.privateChat = YES;//是否是私聊
    if (self.isInRoom) {
        historyModel.remoteRead = 1;
    }else{
        historyModel.remoteRead = 0;
    }
    addDic[@"content"] = videoUrl;// 消息内xing
    addDic[@"historyModel"] = historyModel;
    ChatDetailsModel *addmodel = [ChatDetailsModel initWithDictionary:addDic];
    [self.messageArray addObject:addmodel];
    
    
    //更新ui
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSIndexPath* insertion = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
        [self.chatTableV reloadData];
        [self tableviewScrollToRowWithIndex:insertion];
    });
}
#pragma mark - 发送图片消息
-(void)requsetImageData:(NSString *)imageUrl msgID:(NSString *)msg{
    
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    //构造消息内容
    //    NSDate *datenow = [NSDate date];//现在时间
    //    NSString *timenow=[self getNowTimeTimestamp3];
    //    NSString *contenttemp=@"{\"from\": \"%@\",\"to\": \"%@\",\"cmd\":\"%@\",\"createTime\": \"%@\",\"msgType\": \"%@\",\"chatType\":\"%@\",\"group_id\":\"%@\",\"content\": \"%@\"}";
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSString *timeString = [self getNowTimeTimestamp];
    [NSString stringWithFormat:@"%@%@",uuid,timeString];//唯一标识
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    dic[@"avatarUrl"] =model.avatar;//头像
    dic[@"chatType"] = @(2);//聊天类型,2.私聊
    dic[@"content"] = imageUrl;// 消息内xing
    dic[@"from"] = @(model.ID);//自己的id
    dic[@"msgType"] = @(1);//消息类型0:text、1:image、2:voice、3:vedio、4:Tips、5:news
    dic[@"msgUuid"] = msg;
    dic[@"nickName"] = model.nickname;//自己的昵称
    dic[@"to"] = @(self.buddyModel.ID);//接收者id
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: dic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //发送消息方法
   // [self sendMessage:str cmd:(Byte)11];
    //发送消息方法
    [[ChatIMSend share]sendMessage:str];
}
#pragma mark - cell的代理
-(void)clickcellWithdraw:(UITableViewCell *)cell dictionary:(NSMutableDictionary *)dic{
    //时间戳
    NSString *beginTimestamp = dic[@"time"];
    NSString *endTimestamp = [self getTimestampSince1970];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
       [formatter setDateStyle:NSDateFormatterMediumStyle];
       [formatter setTimeStyle:NSDateFormatterShortStyle];
       [formatter setDateFormat:@"YYYY-MM-dd-HH:MM"];//@"yyyy-MM-dd-HHMMss"
       
       NSDate* date = [NSDate dateWithTimeIntervalSince1970:[beginTimestamp doubleValue]/1000];
       NSString *dateString = [formatter stringFromDate:date];
       NSLog(@"开始时间: %@", dateString);
       
       NSDate *date2 = [NSDate dateWithTimeIntervalSince1970:[endTimestamp doubleValue]];
       NSString *dateString2 = [formatter stringFromDate:date2];
       NSLog(@"结束时间: %@", dateString2);
       
       NSTimeInterval seconds = [date2 timeIntervalSinceDate:date];
       NSLog(@"两个时间相隔：%f", seconds);
    if (seconds>120) {
        [self showText:@"超过两分钟无法撤回"];
        return;
    }
      
//    NSString *newTime = [ChatDetailsGroupVC getCurrentTimes];
//    [self pleaseInsertStarTime:dic[@"time"] andInsertEndTime:newTime];
//    NSLog(@"nstime===%f",   [self pleaseInsertStarTime:dic[@"time"] andInsertEndTime:newTime]);
   
    NSIndexPath *indext = [self.chatTableV indexPathForCell:cell];

    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    //构造消息内容
    //    NSDate *datenow = [NSDate date];//现在时间
    //    NSString *timenow=[self getNowTimeTimestamp3];
    //    NSString *contenttemp=@"{\"from\": \"%@\",\"to\": \"%@\",\"cmd\":\"%@\",\"createTime\": \"%@\",\"msgType\": \"%@\",\"chatType\":\"%@\",\"group_id\":\"%@\",\"content\": \"%@\"}";
    NSString *uuid = [[NSUUID UUID] UUIDString];
    NSString *timeString = [self getNowTimeTimestamp];
    NSMutableDictionary *IMdic = [[NSMutableDictionary alloc]init];
    IMdic[@"from"] = @(model.ID);//自己的id
    IMdic[@"to"] = @(self.buddyModel.ID);//接收者id
    IMdic[@"msgType"] = @(5);//消息类型0:text、1:image、2:voice、3:vedio、4:Tips、5:news
    IMdic[@"msgUuid"] = dic[@"msgUuid"];//唯一标识符
    IMdic[@"groupType"] = @(0);//群组类型
    
    IMdic[@"chatType"] = @(18);//聊天类型1.群聊和讨论组
    IMdic[@"avatarUrl"] =model.avatar;//头像
    IMdic[@"nickName"] = model.nickname;//自己的昵称
    IMdic[@"content"] = dic[@"content"];// 消息内xing
   
    NSLog(@"dic===wenziziz%@",IMdic);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: IMdic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //发送消息方法
    //[self sendMessage:str cmd:(Byte)11];
    //发送消息方法
    [[ChatIMSend share]sendMessage:str];
    ChatDetailsHistoryModel *historyModel  = [[ChatDetailsHistoryModel alloc]init];
    historyModel.content = @"你撤回了一条消息";
    historyModel.time = [ChatDetailsVC getCurrentTimes];
    NSMutableDictionary *messDic = [[NSMutableDictionary alloc]init];
    NSLog(@"histroy===%@",historyModel.content);
    messDic[@"historyModel"] = historyModel;
    
    messDic[@"tag"] = @"12";
    ChatDetailsModel *chatModel = [ChatDetailsModel initWithDictionary:messDic];
  

   // [self.messageArray insertObject:chatModel atIndex:0];
    [self.messageArray replaceObjectAtIndex:indext.row withObject:chatModel];
    [self.chatTableV reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indext,nil] withRowAnimation:UITableViewRowAnimationNone];
    
    
}
-(void)clickCellDelete:(UITableViewCell *)cell dictionary:(NSMutableDictionary *)dic{
    NSLog(@"cellDelete");
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    dic[@"fromId"] = @(model.ID);
    dic[@"toId"] = @(self.buddyModel.ID);
    dic[@"type"] = @(0);
   
    NSLog(@"delegate===%@",dic);
//    [RequestManager getIMDataWithParameters:dic IMHost:@"https://192.168.31.222:20443/" UrlString:@"api/messageHandler/getAllGroupMessageByGroupIdAndType" Success:^(id  _Nullable response) {
    [RequestManager getIMDataWithParameters:dic IMHost:IMPostHost UrlString:@"api/messageHandler/delMessageByMsgId" Success:^(id  _Nullable response) {
        NSLog(@"rpost===%@",response);
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"errr==%@",error);
    }];
    NSIndexPath *indext = [self.chatTableV indexPathForCell:cell];
    NSLog(@"index==%lu",indext.row);
    NSLog(@"modelArray===%@",self.modelArray);
    [self.messageArray removeObjectAtIndex:indext.row];
    NSLog(@"modelArray===%@",self.modelArray);
    [self.chatTableV deleteRowsAtIndexPaths:@[indext]withRowAnimation:UITableViewRowAnimationFade];
}

- (NSString *)getTimeFromTimestamp:(NSString*)mtime{

 
    //将对象类型的时间转换为NSDate类型
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[mtime doubleValue]/1000];

        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];

        //指定输出的格式  格式:07.06 12:33

        [formatter setDateFormat:@"HH:ss"];

        NSString *timeString= [formatter stringFromDate:date];

        return timeString;

}
//获取当前的时间

+(NSString*)getCurrentTimes{

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制

    [formatter setDateFormat:@"HH:mm"];

    //现在时间,你可以输出来看下是什么格式

    NSDate *datenow = [NSDate date];

    //----------将nsdate按formatter格式转成nsstring

    NSString *currentTimeString = [formatter stringFromDate:datenow];

    NSLog(@"currentTimeString =  %@",currentTimeString);

    return currentTimeString;

}
//获取当前时间戳字符串 10位
- (NSString *)getTimestampSince1970
{
    NSDate *datenow = [NSDate date];//现在时间
    NSTimeInterval interval = [datenow timeIntervalSince1970];//13位的*1000
    NSString *timeSp = [NSString stringWithFormat:@"%.0f",interval];
    return timeSp;
}
//获取两个时间的间隔
- (NSTimeInterval)pleaseInsertStarTime:(NSString *)starTime andInsertEndTime:(NSString *)endTime{
    NSLog(@"startim===%@",starTime);
    NSLog(@"endTime===%@",endTime);
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"HH:mm"];//根据自己的需求定义格式
    NSDate* startDate = [formater dateFromString:starTime];
    NSDate* endDate = [formater dateFromString:endTime];
    NSTimeInterval time = [endDate timeIntervalSinceDate:startDate];
    NSLog(@"当前时间为%f", time);
    return time;
}
//时间戳变为格式时间13位
- (NSString *)ConvertStrToTime:(NSString *)timeStr

{

//long long time=[timeStr longLongValue];
//    如果服务器返回的是13位字符串，需要除以1000，否则显示不正确(13位其实代表的是毫秒，需要除以1000)
    long long time=[timeStr longLongValue] / 1000;

NSDate *date = [[NSDate alloc]initWithTimeIntervalSince1970:time];

NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    BOOL isToday = [[NSCalendar currentCalendar] isDateInToday:date];
    if (isToday) {
        [formatter setDateFormat:@"HH:mm:ss"];
    }else{
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }


NSString*timeString=[formatter stringFromDate:date];

return timeString;

}
///实现通知方法

- (void)text:(NSNotification *)noti {

///noti中的key,就可以取出值
    NSLog(@"chatNotion");
    NSString * name =  noti.userInfo[@"key"];
    [self dataReceived:name];
    NSLog(@"name===%@",name);

}
#pragma mark - 获取是否拉黑,获取是否关注
-(void)requsteShieldingData{
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    NSMutableDictionary *setDic = [[NSMutableDictionary alloc]init];
    setDic[@"userId"] =@(model.ID);//别人的id
    setDic[@"targetUserId"] = @(self.buddyModel.ID);
    NSLog(@"steDic===%@",setDic);
    
    [RequestManager getMyDataWithParameters:setDic UrlString:@"/api/user/imFocus" Success:^(id  _Nullable response) {
        NSLog(@"responselahei==%@",response);
        if ([response[@"status"]intValue]==200) {
           
            if (![self isBlankDictionary:response[@"data"]]) {
                if ([response[@"data"][@"isBlack"]intValue]== 1) {//是否被对方拉黑 ,true：被拉黑 false:未被拉
                    self.isBannend = YES;
                }
                if ([response[@"data"][@"isFollow"]intValue]== 1) {
                    self.focusOnLabel.hidden = YES;
                }else{
                    self.focusOnLabel.hidden = NO;
                }
            NSLog(@"有数据");
            }else{
                NSLog(@"无数据");
                self.isBannend = NO;
            }
        }

        // NSLog(@"response==%@",response);
    } withFail:^(NSError * _Nullable error) {
      NSLog(@"responselaheir==%@",error);
    }];
}
/**
 判断字典为空
 @param  dic 数组
 @return YES 空 NO
 */

- (BOOL)isBlankDictionary:(NSDictionary *)dic {
    if (!dic) {
        return YES;
    }
    if ([dic isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return YES;
    }
    if (!dic.count) {
        return YES;
    }
    if (dic == nil) {
        return YES;
    }
    if (dic == NULL) {
        return YES;
    }
    return NO;
}
#pragma mark - im私聊在聊天室
-(void)requstIMINChatRoom{
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    NSMutableDictionary *IMDic = [[NSMutableDictionary alloc]init];
    IMDic[@"fromId"] = @(model.ID);
    IMDic[@"toId"] = @(self.buddyModel.ID);
    IMDic[@"type"] = @(0);
    IMDic[@"status"] = @(1);//status->0:退出 1:进入
   
    [RequestManager getIMDataWithParameters:IMDic IMHost:IMPostHost UrlString:@"api/messageHandler/updateUserRoomLat" Success:^(id  _Nullable response) {
        NSLog(@"reponst==%@",response);
        if ([response[@"status"]intValue]==200) {
           
            [self InAChatRoom];
        }
        
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
-(void)InAChatRoom{
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    NSMutableDictionary *IMDic = [[NSMutableDictionary alloc]init];
    IMDic[@"from"] = @(model.ID);
    IMDic[@"to"] = @(self.buddyModel.ID);
    IMDic[@"chatType"] = @(19);
   
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:IMDic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [[ChatIMSend share]sendMessage:str];
}
- (void)checkFollow {
    [RequestManager getUserById:[UserInfoManager shared].getUserID toUserId:self.buddyModel.ID withSuccess:^(id  _Nullable response) {
        NSDictionary *data = response[@"data"];
        if ([data[@"isFollow"] intValue] == 1) {
            self.focusOnLabel.hidden = YES;
        }
    } withFail:^(NSError * _Nullable error) {
    }];
}
#pragma mark -  点击关注
- (void)clickFocusOn {
    [RequestManager addFollowsMappingWithId:[UserInfoManager shared].getUserID followUserId:self.buddyModel.ID withSuccess:^(id  _Nullable response) {
        self.focusOnLabel.hidden = YES;
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
#pragma mark - im 退出私聊
-(void)requstExitChat{
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    NSMutableDictionary *IMDic = [[NSMutableDictionary alloc]init];
    IMDic[@"fromId"] = @(model.ID);
    IMDic[@"toId"] = @(self.buddyModel.ID);
    IMDic[@"type"] = @(0);
    IMDic[@"status"] = @(0);//status->0:退出 1:进入
   
    [RequestManager getIMDataWithParameters:IMDic IMHost:IMPostHost UrlString:@"api/messageHandler/updateUserRoomLat" Success:^(id  _Nullable response) {
        NSLog(@"reponst==%@",response);
        if ([response[@"status"]intValue]==200) {
           
            [self outChatRoom];
        }
        
    } withFail:^(NSError * _Nullable error) {
        
    }];
}
-(void)outChatRoom{
    UserInfoModel *model = [[UserInfoManager shared]getUserDetailInfo];
    NSMutableDictionary *IMDic = [[NSMutableDictionary alloc]init];
    IMDic[@"from"] = @(model.ID);
    IMDic[@"to"] = @(self.buddyModel.ID);
    IMDic[@"chatType"] = @(19);
    IMDic[@"content"] = @"I'm left";
   
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:IMDic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [[ChatIMSend share]sendMessage:str];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 删除聊天记录通知
-(void)deleterChatPerson:(NSNotification *)notif {
    NSDictionary *dict = notif.userInfo;
    if ([dict[@"userId"] longValue] != self.buddyModel.ID) {
        return;
    }
    [self.messageArray removeAllObjects];
    [self.chatTableV reloadData];
}
///在调用结束的时候,要移除通知

 - (void)dealloc {

          //[super dealloc];  非ARC中需要调用此句

     [[NSNotificationCenter defaultCenter] removeObserver:self];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"noti" object:nil];

   }
- (NSMutableArray *)messageArray {
    if (!_messageArray) {
        _messageArray = [[NSMutableArray alloc]init];
    }
    return _messageArray;
}
- (HXPhotoManager *)photoManager {
    if (!_photoManager) {
        _photoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
        //黑色主题 视频或者图片 最多九张
        _photoManager.configuration.type = HXConfigurationTypeWXChat;
        _photoManager.configuration.videoMaxNum = 1;
        _photoManager.configuration.selectTogether = NO;
        _photoManager.configuration.photoMaxNum = 9;
        _photoManager.configuration.showOriginalBytes = YES;
        _photoManager.configuration.showOriginalBytesLoading = YES;
        _photoManager.configuration.requestImageAfterFinishingSelection = YES;
        _photoManager.configuration.exportVideoURLForHighestQuality = NO;
    }
    return _photoManager;
}
- (UILabel *)focusOnLabel {
    if (!_focusOnLabel) {
        _focusOnLabel = [[UILabel alloc]init];
        _focusOnLabel.backgroundColor = appColor;
        _focusOnLabel.cornerRadius = 6;
        _focusOnLabel.text = @"+关注";
        _focusOnLabel.textAlignment = NSTextAlignmentCenter;
        _focusOnLabel.font =  [UIFont fontWithName:@"PingFangSC-Regular" size:14];
        _focusOnLabel.textColor = [UIColor whiteColor];
    }
    return _focusOnLabel;
}
@end
