//
//  ChatDeatilsIMLabel.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/12/16.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatDeatilsIMLabel : UILabel
@property (nonatomic,  copy) void(^retweetBlock)(NSString *text);//转发
@property (nonatomic,  copy) void(^withDrawBlock)(NSString *text);//撤回
@property (nonatomic,  copy) void(^showMenuItemBlock)(void);//显示
@property (nonatomic,  copy) void(^bannedBlock)(void);//显示
@property (nonatomic,  copy) void(^kickedOutBlock)(void);//踢出群聊
@property (nonatomic,assign) BOOL isReceiver;
@property (nonatomic,assign)NSInteger  sentag;//1.群里自己2.群聊其他人 3.私聊自己 4.私聊其他人
@property (nonatomic,  copy) NSString *str;//不用self.text取文本，因为和YYLabel的ignoreCommonProperties有冲突。
@end

NS_ASSUME_NONNULL_END
