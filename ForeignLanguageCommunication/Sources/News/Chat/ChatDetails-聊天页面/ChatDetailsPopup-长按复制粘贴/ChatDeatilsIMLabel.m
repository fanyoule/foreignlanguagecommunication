//
//  ChatDeatilsIMLabel.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/12/16.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ChatDeatilsIMLabel.h"

@implementation ChatDeatilsIMLabel
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupTap];
    }
    return self;
}
/** 设置敲击手势 */
- (void)setupTap
{
    //已经在stroyboard设置了与用户交互,也可以用纯代码设置
    self.userInteractionEnabled = YES;
    
    //当前控件是label 所以是给label添加敲击手势
    [self addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)]];
    
   
}


- (void)longPress:(UILongPressGestureRecognizer *)gesture
{
    
  [self becomeFirstResponder]; //成为第一响应者
       UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setTargetRect:self.bounds inView:self];
      NSArray *menuItems =@[
      [[UIMenuItem alloc] initWithTitle:@"拷贝" action:@selector(customCopy:)],[[UIMenuItem alloc] initWithTitle:@"删除" action:@selector(withdraw:)],[[UIMenuItem alloc] initWithTitle:@"撤回" action:@selector(retweet:)]
      ];
    if (_isReceiver) {
        menuItems =@[
        [[UIMenuItem alloc] initWithTitle:@"拷贝" action:@selector(customCopy:)],[[UIMenuItem alloc] initWithTitle:@"删除" action:@selector(withdraw:)]
               ];
           }
    if (_showMenuItemBlock) {
               _showMenuItemBlock();
           }
       menu.menuItems = menuItems;
       menu.menuVisible = YES;
    
    
    
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
     if (
           ((action == @selector(customCopy:) || (action == @selector(retweet:)) || (action == @selector(withdraw:))|| (action == @selector(banned:))|| (action == @selector(kickedOut:)))  )
           )
           return YES;
       
       return NO;
}

#pragma mark - Super Method
#pragma mark - 监听MenuItem的点击事件/** 剪切 */
- (void)cut:(UIMenuController *)menu
{    //UIPasteboard 是可以在应用程序与应用程序之间共享的 \
    (应用程序:你的app就是一个应用程序 比如你的QQ消息可以剪切到百度查找一样)
    // 将label的文字存储到粘贴板
    [UIPasteboard generalPasteboard].string = self.text;
     self.str = [UIPasteboard generalPasteboard].string;
    // 清空文字
    self.text = nil;
   // [self finishChoosing];
}

//复制
- (void)customCopy:(UIMenuController *)menu
{
    // 将label的文字存储到粘贴板
    [UIPasteboard generalPasteboard].string = self.text;
   // [self finishChoosing];
}
//转发
- (void)retweet:(UIMenuController *)menu{

    kWeakSelf(self);
    if (self.retweetBlock) {
        weakself.retweetBlock(weakself.attributedText.string);
    }
   // [self finishChoosing];
}
//撤回
- (void)withdraw:(UIMenuController *)menu{
    
    kWeakSelf(self);
    if (self.withDrawBlock) {
        weakself.withDrawBlock(weakself.attributedText.string);
    }
    //[self finishChoosing];
}

//粘贴
- (void)paste:(UIMenuController *)menu
{
    // 将粘贴板的文字赋值给label
    self.str = [UIPasteboard generalPasteboard].string;
    //[self finishChoosing];
}
//禁言
- (void)banned:(UIMenuController *)menu
{
    kWeakSelf(self);
    if(self.bannedBlock){
        weakself.bannedBlock();
    }
    NSLog(@"禁言");
}
//踢出群聊
- (void)kickedOut:(UIMenuController *)menu
{
    kWeakSelf(self);
    if (self.kickedOutBlock) {
        weakself.kickedOutBlock();
    }
    NSLog(@"");
}
@end


