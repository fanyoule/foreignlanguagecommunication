//
//  ChatPersonDetailViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/1.
//

#import "ChatPersonDetailViewController.h"
#import "MyPersonalInformationVC.h"
#import "GroupOfDataTableViewCell.h"
#import "ChatPersonModel.h"

@interface ChatPersonDetailViewController ()<UITableViewDelegate,UITableViewDataSource, GroupOfDataTableViewCellDelegate>
@property (nonatomic, strong) UIImageView *headerImgv;
@property (nonatomic, strong) UILabel *nameLab;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) ChatPersonModel *personModel;
@end

@implementation ChatPersonDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navTitleString = @"聊天信息";
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = RGBA(245, 245, 245, 1);
    [self.view addSubview:self.mainTableView];
    self.mainTableView.tableHeaderView = self.headerView;
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    self.mainTableView.mj_header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
//    [self addHeadView];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
}

// 点击成员头像
- (void)headerClick {
    MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc] init];
    vc.ID = self.userId;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)stopLoading {
    if (self.mainTableView.mj_header.isRefreshing) {
        [self.mainTableView.mj_header endRefreshing];
    }
}
- (void)getData {
    kWeakSelf(self)
    [RequestManager getUserChatSetWithId:[UserInfoManager shared].getUserID toUserId:self.userId withSuccess:^(id  _Nullable response) {
        weakself.personModel = [ChatPersonModel mj_objectWithKeyValues:response[@"data"]];
        [weakself refresh];
        SetCache(weakself.personModel.silence == 1, ISNoDisturbingKey(weakself.userId));
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)refresh {
    [self.headerImgv sd_setImageWithURL:self.personModel.toUserAvatar.mj_url];
    self.nameLab.text = self.personModel.toUserNickname;
    [self.mainTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

// 免打扰
- (void)setNoDisturbing {
    kWeakSelf(self)
    [self showSVP];
    int value = self.personModel.silence == 1 ? 0 : 1;
    [RequestManager updateUserChatSetWithId:[UserInfoManager shared].getUserID toUserId:self.userId silence:value withSuccess:^(id  _Nullable response) {
        [weakself dissSVP];
        [weakself getData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)switchClick:(NSInteger)index {
    [self setNoDisturbing];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"chatPersonVcCell";
    GroupOfDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[GroupOfDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.delegate = self;
    cell.index = indexPath.row;
    if (indexPath.row == 0){
        cell.swi.hidden = NO;
        cell.contentLabel.hidden = YES;
        cell.titleLabel.text = @"消息免打扰";
        cell.swi.on = self.personModel.silence == 1;
    } else if (indexPath.row == 1) {
        cell.swi.hidden = YES;
        cell.contentLabel.hidden = YES;
        cell.titleLabel.text = @"删除聊天记录";
        [cell whenTapped:^{
            [self deletAllMsgHistory];
        }];
    }
    cell.backgroundColor = UIColor.whiteColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)deletAllMsgHistory {
    [self alertView:@"提示" msg:@"确定删除聊天记录?" btnArrTitles:@[@"确定"] showCacel:YES style:UIAlertControllerStyleAlert block:^(int index) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic[@"fromId"]= @([UserInfoManager shared].getUserID);//自己的id
        dic[@"toId"] = @(self.userId);
        dic[@"type"]= @"0"; // 0个人，1.工会 2.群聊 3.讨论组
        [RequestManager getIMDataWithParameters:dic IMHost:IMPostHost UrlString:@"api/messageHandler/delPersonalAllMessageHistoryById" Success:^(id  _Nullable response) {
            NSLog(@"responm===%@",response);
            if ([response[@"status"]intValue]==200) {
                [self showText:@"删除成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"deleterChatPerson" object:nil userInfo:@{@"userId": @(self.userId)}];
            }
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"error===%@",error);
        }];
    }];
}
- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 140)];
        _headerView.backgroundColor = UIColor.whiteColor;
        
        [_headerView addSubview:self.headerImgv];
        [_headerView addSubview:self.nameLab];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 130, kScreenWidth, 10)];
        line.backgroundColor = CellLineColor;
        [_headerView addSubview:line];
    }
    return _headerView;
}
- (UIImageView *)headerImgv {
    if (!_headerImgv) {
        _headerImgv = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 60, 60)];
        _headerView.layer.cornerRadius = 8;
        _headerView.layer.masksToBounds = YES;
        _headerView.userInteractionEnabled = YES;
        
        [_headerView whenTapped:^{
            [self headerClick];
        }];
    }
    return _headerImgv;
}
- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 80, 60, 20)];
        _nameLab.textAlignment = NSTextAlignmentCenter;
        _nameLab.font = kFont(13);
        _nameLab.textColor = UIColor.blackColor;
    }
    return _nameLab;
}
@end
