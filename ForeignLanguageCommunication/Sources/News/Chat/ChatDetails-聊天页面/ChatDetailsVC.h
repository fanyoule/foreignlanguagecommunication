//
//  ChatDetailsVC.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/9/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//>>>>个人聊天

#import <UIKit/UIKit.h>
#import "NewsAllBuddyListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatDetailsVC : UIViewController

/** mode */
@property (nonatomic, strong) NewsAllBuddyListModel *buddyModel;

@end

NS_ASSUME_NONNULL_END
