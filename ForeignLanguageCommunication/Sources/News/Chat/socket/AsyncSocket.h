//
//  AsyncSocket.h
//  
//  This class is in the public domain.
//  Originally created by Dustin Voss on Wed Jan 29 2003.
//  Updated and maintained by Deusty Designs and the Mac development community.
//
//  http://code.google.com/p/cocoaasyncsocket/
//

#import <Foundation/Foundation.h>

@class AsyncSocket;
@class AsyncReadPacket;
@class AsyncWritePacket;

extern NSString *const AsyncSocketException;
extern NSString *const AsyncSocketErrorDomain;

enum AsyncSocketError
{
	AsyncSocketCFSocketError = kCFSocketError,	// From CFSocketError enum.
	AsyncSocketNoError = 0,						// Never used.
	AsyncSocketCanceledError,					// onSocketWillConnect: returned NO.
	AsyncSocketConnectTimeoutError,
	AsyncSocketReadMaxedOutError,               // Reached set maxLength without completing
	AsyncSocketReadTimeoutError,
	AsyncSocketWriteTimeoutError
};
typedef enum AsyncSocketError AsyncSocketError;

@protocol AsyncSocketDelegate
@optional

/**
 * In the event of an error, the socket is closed.
 * You may call "unreadData" during this call-back to get the last bit of data off the socket.
 * When connecting, this delegate method may be called
 * before"onSocket:didAcceptNewSocket:" or "onSocket:didConnectToHost:".
 * 如果发生错误，套接字将被关闭。
 *在这个回调过程中，你可以调用“unreadData”来从套接字中获取最后的数据位。
 当连接时，这个委托方法可能被调用
 *在“onSocket:didAcceptNewSocket:”或“onSocket:didConnectToHost:”之前。
**/
- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err;

/**
 * Called when a socket disconnects with or without error.  If you want to release a socket after it disconnects,
 * do so here. It is not safe to do that during "onSocket:willDisconnectWithError:".
 * 
 * If you call the disconnect method, and the socket wasn't already disconnected,
 * this delegate method will be called before the disconnect method returns.
 * 当套接字断开连接时调用。如果您想在socket断开连接后释放它，
 *在这里这样做。在“onSocket:willDisconnectWithError:”期间这样做是不安全的。
 *
 *如果你调用disconnect方法，socket还没有断开连接，
 *这个委托方法将在disconnect方法返回之前被调用。
**/
- (void)onSocketDidDisconnect:(AsyncSocket *)sock;

/**
 * Called when a socket accepts a connection.  Another socket is spawned to handle it. The new socket will have
 * the same delegate and will call "onSocket:didConnectToHost:port:".
 * 当套接字接受连接时调用。派生另一个套接字来处理它。新插座会有毛病的
 *相同的委托，将调用“onSocket:didConnectToHost:port:”。
**/
- (void)onSocket:(AsyncSocket *)sock didAcceptNewSocket:(AsyncSocket *)newSocket;

/**
 * Called when a new socket is spawned to handle a connection.  This method should return the run-loop of the
 * thread on which the new socket and its delegate should operate. If omitted, [NSRunLoop currentRunLoop] is used.
 * 当产生一个新的套接字来处理连接时调用。类的运行循环应返回
 新套接字及其委托应操作的线程。如果省略，则使用[NSRunLoop currentRunLoop]。
**/
- (NSRunLoop *)onSocket:(AsyncSocket *)sock wantsRunLoopForNewSocket:(AsyncSocket *)newSocket;

/**
 * Called when a socket is about to connect. This method should return YES to continue, or NO to abort.
 * If aborted, will result in AsyncSocketCanceledError.
 * 
 * If the connectToHost:onPort:error: method was called, the delegate will be able to access and configure the
 * CFReadStream and CFWriteStream as desired prior to connection.
 *
 * If the connectToAddress:error: method was called, the delegate will be able to access and configure the
 * CFSocket and CFSocketNativeHandle (BSD socket) as desired prior to connection. You will be able to access and
 * configure the CFReadStream and CFWriteStream in the onSocket:didConnectToHost:port: method.
 * 当套接字将要连接时调用。此方法应返回YES以继续，或返回NO以中止。
 *如果中止，将导致AsyncSocketCanceledError。
 *
 *如果connectToHost:onPort:error:方法被调用，委托将能够访问和配置
 *连接之前的CFReadStream和CFWriteStream。
 *
 *如果connectToAddress:error:方法被调用，委托将能够访问和配置
 * CFSocket和CFSocketNativeHandle (BSD套接字)作为连接之前所需的。您将能够访问和
 在onSocket:didConnectToHost:port:方法中配置CFReadStream和CFWriteStream。
**/
- (BOOL)onSocketWillConnect:(AsyncSocket *)sock;

/**
 * Called when a socket connects and is ready for reading and writing.
 * The host parameter will be an IP address, not a DNS name.
 * *当套接字连接并准备读写时调用。
 主机参数是一个IP地址，而不是DNS名称。
**/
- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port;

/**
 * Called when a socket has completed reading the requested data into memory.
 * Not called if there is an error.
 * 当套接字完成将请求的数据读入内存时调用。
 *没有调用如果有一个错误。
**/
- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag;

/**
 * Called when a socket has read in data, but has not yet completed the read.
 * This would occur if using readToData: or readToLength: methods.
 * It may be used to for things such as updating progress bars.
 * 当套接字已读入数据，但尚未完成读入时调用。
 *这将发生如果使用readToData:或readToLength:方法。
 *它可以用于更新进度条之类的事情。
**/
- (void)onSocket:(AsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag;

/**
 * Called when a socket has completed writing the requested data. Not called if there is an error.
 * 当套接字完成写入请求的数据时调用。如果有错误则不调用。
**/
- (void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag;

/**
 * Called when a socket has written some data, but has not yet completed the entire write.
 * It may be used to for things such as updating progress bars.
 * 当套接字已写入一些数据，但尚未完成整个写入时调用。
 *它可以用于更新进度条之类的事情。
**/
- (void)onSocket:(AsyncSocket *)sock didWritePartialDataOfLength:(NSUInteger)partialLength tag:(long)tag;

/**
 * Called if a read operation has reached its timeout without completing.
 * This method allows you to optionally extend the timeout.
 * If you return a positive time interval (> 0) the read's timeout will be extended by the given amount.
 * If you don't implement this method, or return a non-positive time interval (<= 0) the read will timeout as usual.
 * 
 * The elapsed parameter is the sum of the original timeout, plus any additions previously added via this method.
 * The length parameter is the number of bytes that have been read so far for the read operation.
 * 
 * Note that this method may be called multiple times for a single read if you return positive numbers.
 * 当读操作达到超时而没有完成时调用。
 *此方法允许您选择延长超时。
 *如果你返回一个正的时间间隔(> 0)，读取的超时将被延长给定的数量。
 *如果你没有实现这个方法，或者返回一个非正的时间间隔(<= 0)，读取将像往常一样超时。
 *
 * elapsed参数是原始超时的总和，加上之前通过该方法添加的任何添加。
 length参数是到目前为止为read操作读取的字节数。
 *
 *注意，如果你返回正数，这个方法可能会被调用多次。
**/
- (NSTimeInterval)onSocket:(AsyncSocket *)sock
  shouldTimeoutReadWithTag:(long)tag
                   elapsed:(NSTimeInterval)elapsed
                 bytesDone:(NSUInteger)length;

/**
 * Called if a write operation has reached its timeout without completing.
 * This method allows you to optionally extend the timeout.
 * If you return a positive time interval (> 0) the write's timeout will be extended by the given amount.
 * If you don't implement this method, or return a non-positive time interval (<= 0) the write will timeout as usual.
 * 
 * The elapsed parameter is the sum of the original timeout, plus any additions previously added via this method.
 * The length parameter is the number of bytes that have been written so far for the write operation.
 * 
 * Note that this method may be called multiple times for a single write if you return positive numbers.
 * 当读操作达到超时而没有完成时调用。
 *此方法允许您选择延长超时。
 *如果你返回一个正的时间间隔(> 0)，读取的超时将被延长给定的数量。
 *如果你没有实现这个方法，或者返回一个非正的时间间隔(<= 0)，读取将像往常一样超时。
 *
 * elapsed参数是原始超时的总和，加上之前通过该方法添加的任何添加。
 length参数是到目前为止为read操作读取的字节数。
 *
 *注意，如果你返回正数，这个方法可能会被调用多次。
**/
- (NSTimeInterval)onSocket:(AsyncSocket *)sock
 shouldTimeoutWriteWithTag:(long)tag
                   elapsed:(NSTimeInterval)elapsed
                 bytesDone:(NSUInteger)length;

/**
 * Called after the socket has successfully completed SSL/TLS negotiation.
 * This method is not called unless you use the provided startTLS method.
 * 
 * If a SSL/TLS negotiation fails (invalid certificate, etc) then the socket will immediately close,
 * and the onSocket:willDisconnectWithError: delegate method will be called with the specific SSL error code.
 * 在套接字成功完成SSL/TLS协商后调用。
 除非使用提供的startTLS方法，否则不会调用此方法。
 *
 如果SSL/TLS协商失败(无效证书等)，socket将立即关闭，
 *和onSocket:willDisconnectWithError: delegate方法将被调用与特定的SSL错误代码。
**/
- (void)onSocketDidSecure:(AsyncSocket *)sock;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface AsyncSocket : NSObject
{
	CFSocketNativeHandle theNativeSocket4;
	CFSocketNativeHandle theNativeSocket6;
	
	CFSocketRef theSocket4;            // IPv4 accept or connect socket
	CFSocketRef theSocket6;            // IPv6 accept or connect socket
	
	CFReadStreamRef theReadStream;
	CFWriteStreamRef theWriteStream;

	CFRunLoopSourceRef theSource4;     // For theSocket4
	CFRunLoopSourceRef theSource6;     // For theSocket6
	CFRunLoopRef theRunLoop;
	CFSocketContext theContext;
	NSArray *theRunLoopModes;
	
	NSTimer *theConnectTimer;

	NSMutableArray *theReadQueue;
	AsyncReadPacket *theCurrentRead;
	NSTimer *theReadTimer;
	NSMutableData *partialReadBuffer;
	
	NSMutableArray *theWriteQueue;
	AsyncWritePacket *theCurrentWrite;
	NSTimer *theWriteTimer;

	id theDelegate;
	UInt16 theFlags;
	
	long theUserData;
}

- (id)init;
- (id)initWithDelegate:(id)delegate;
- (id)initWithDelegate:(id)delegate userData:(long)userData;

/* String representation is long but has no "\n". */
///*字符串表示是长，但没有“\n”。* /
- (NSString *)description;

/**
 * Use "canSafelySetDelegate" to see if there is any pending business (reads and writes) with the current delegate
 * before changing it.  It is, of course, safe to change the delegate before connecting or accepting connections.
**/
/// * *
//*使用"canSafelySetDelegate"查看是否有任何悬而未决的业务(读和写)与当前的委托
//*在更改之前。当然，在连接或接受连接之前更改委托是安全的。
//* * /
- (id)delegate;
- (BOOL)canSafelySetDelegate;
- (void)setDelegate:(id)delegate;

/* User data can be a long, or an id or void * cast to a long. */
///* User data可以是long类型，也可以是id或void类型。* /
- (long)userData;
- (void)setUserData:(long)userData;

/* Don't use these to read or write. And don't close them either! */
///*不要用这些来读或写。也不要关闭它们!* /
- (CFSocketRef)getCFSocket;
- (CFReadStreamRef)getCFReadStream;
- (CFWriteStreamRef)getCFWriteStream;

// Once one of the accept or connect methods are called, the AsyncSocket instance is locked in
// and the other accept/connect methods can't be called without disconnecting the socket first.
// If the attempt fails or times out, these methods either return NO or
// call "onSocket:willDisconnectWithError:" and "onSockedDidDisconnect:".

// When an incoming connection is accepted, AsyncSocket invokes several delegate methods.
// These methods are (in chronological order):
// 1. onSocket:didAcceptNewSocket:
// 2. onSocket:wantsRunLoopForNewSocket:
// 3. onSocketWillConnect:
// 
// Your server code will need to retain the accepted socket (if you want to accept it).
// The best place to do this is probably in the onSocket:didAcceptNewSocket: method.
// 
// After the read and write streams have been setup for the newly accepted socket,
// the onSocket:didConnectToHost:port: method will be called on the proper run loop.
// 
// Multithreading Note: If you're going to be moving the newly accepted socket to another run
// loop by implementing onSocket:wantsRunLoopForNewSocket:, then you should wait until the
// onSocket:didConnectToHost:port: method before calling read, write, or startTLS methods.
// Otherwise read/write events are scheduled on the incorrect runloop, and chaos may ensue.

/**
 * Tells the socket to begin listening and accepting connections on the given port.
 * When a connection comes in, the AsyncSocket instance will call the various delegate methods (see above).
 * The socket will listen on all available interfaces (e.g. wifi, ethernet, etc)
**/
//一旦其中一个accept或connect方法被调用，AsyncSocket实例被锁定
//其他accept/connect方法必须先断开套接字连接才能被调用。
//如果尝试失败或超时，这些方法返回NO或
//调用“onSocket:willDisconnectWithError:”和“onSockedDidDisconnect:”

//当一个传入的连接被接受时，AsyncSocket调用几个委托方法。
//这些方法是(按时间顺序):
/// / 1。onSocket: didAcceptNewSocket:
/// / 2。onSocket: wantsRunLoopForNewSocket:
/// / 3。onSocketWillConnect:
//
//您的服务器代码将需要保留已接受的套接字(如果您想接受它)。
//做这个的最佳位置可能是在onSocket:didAcceptNewSocket:方法中。
//
//为新接受的套接字设置读和写流后，
// onSocket:didConnectToHost:port:方法将在适当的运行循环中被调用。
//
//注意:如果您打算将新接受的套接字移动到另一个运行
//通过实现onSocket:wantsRunLoopForNewSocket:循环，然后你应该等待
// onSocket:didConnectToHost:port: method调用read、write或startTLS方法之前。
//否则读/写事件被安排在不正确的运行循环上，可能会导致混乱。

/// * *
//告诉套接字在给定端口上开始监听和接受连接。
//*当连接进来时，AsyncSocket实例将调用各种委托方法(见上面)。
//*插座将监听所有可用的接口(如wifi，以太网等)
//* * /
- (BOOL)acceptOnPort:(UInt16)port error:(NSError **)errPtr;

/**
 * This method is the same as acceptOnPort:error: with the additional option
 * of specifying which interface to listen on. So, for example, if you were writing code for a server that
 * has multiple IP addresses, you could specify which address you wanted to listen on.  Or you could use it
 * to specify that the socket should only accept connections over ethernet, and not other interfaces such as wifi.
 * You may also use the special strings "localhost" or "loopback" to specify that
 * the socket only accept connections from the local machine.
 * 
 * To accept connections on any interface pass nil, or simply use the acceptOnPort:error: method.
**/
/// * *
//*该方法与acceptOnPort:error:附加选项相同
//*指定监听哪个接口。举个例子，如果你在为一个服务器写代码
//*有多个IP地址，你可以指定你想要监听的地址。或者你可以用它
//*指定插座只接受以太网连接，不接受其他接口(如wifi)。
//*你也可以使用特殊字符串"localhost"或"loopback"来指定
//该套接字只接受本地机器的连接。
//*
//*在任何接口上接受连接传递nil，或简单地使用acceptOnPort:error:方法。
//* * /
- (BOOL)acceptOnInterface:(NSString *)interface port:(UInt16)port error:(NSError **)errPtr;

/**
 * Connects to the given host and port.
 * The host may be a domain name (e.g. "deusty.com") or an IP address string (e.g. "192.168.0.2")
**/
/// * *
//*连接到给定的主机和端口。
//*主机可能是一个域名(例如:"deusty.com")或IP地址字符串(例如:期间”“192.168.0.2)
//* * /
- (BOOL)connectToHost:(NSString *)hostname onPort:(UInt16)port error:(NSError **)errPtr;

/**
 * This method is the same as connectToHost:onPort:error: with an additional timeout option.
 * To not time out use a negative time interval, or simply use the connectToHost:onPort:error: method.
**/
/// * *
//*这个方法与connectToHost:onPort:error:相同，只是有一个额外的超时选项。
//*不超时使用负时间间隔，或简单地使用connectToHost:onPort:error:方法。
//* * /
- (BOOL)connectToHost:(NSString *)hostname
			   onPort:(UInt16)port
		  withTimeout:(NSTimeInterval)timeout
				error:(NSError **)errPtr;

/**
 * Connects to the given address, specified as a sockaddr structure wrapped in a NSData object.
 * For example, a NSData object returned from NSNetService's addresses method.
 * 
 * If you have an existing struct sockaddr you can convert it to a NSData object like so:
 * struct sockaddr sa  -> NSData *dsa = [NSData dataWithBytes:&remoteAddr length:remoteAddr.sa_len];
 * struct sockaddr *sa -> NSData *dsa = [NSData dataWithBytes:remoteAddr length:remoteAddr->sa_len];
**/
//连接到给定的地址，指定为包装在NSData对象中的sockaddr结构。
//*例如，一个NSNetService的地址方法返回的NSData对象。
//*
//*如果你有一个现有的sockaddr结构，你可以把它转换成NSData对象，像这样:
//* sockaddr sa -> NSData *dsa = [NSData dataWithBytes:&remoteAddr length: remotead .sa_len];
//* sockaddr *sa -> NSData *dsa = [NSData dataWithBytes:remoteAddr length:remoteAddr->sa_len];
//* * /
- (BOOL)connectToAddress:(NSData *)remoteAddr error:(NSError **)errPtr;

/**
 * This method is the same as connectToAddress:error: with an additional timeout option.
 * To not time out use a negative time interval, or simply use the connectToAddress:error: method.
**/
//* *
//*这个方法与connectToAddress:error:相同，只是有一个额外的超时选项。
//*不超时使用负时间间隔，或简单地使用connectToAddress:error:方法。
//* * /
- (BOOL)connectToAddress:(NSData *)remoteAddr withTimeout:(NSTimeInterval)timeout error:(NSError **)errPtr;

- (BOOL)connectToAddress:(NSData *)remoteAddr
     viaInterfaceAddress:(NSData *)interfaceAddr
             withTimeout:(NSTimeInterval)timeout
                   error:(NSError **)errPtr;

/**
 * Disconnects immediately. Any pending reads or writes are dropped.
 * If the socket is not already disconnected, the onSocketDidDisconnect delegate method
 * will be called immediately, before this method returns.
 * 
 * Please note the recommended way of releasing an AsyncSocket instance (e.g. in a dealloc method)
 * [asyncSocket setDelegate:nil];
 * [asyncSocket disconnect];
 * [asyncSocket release];
**/
//* *
//*立即断开。任何挂起的读或写都将被删除。
//*如果套接字还没有断开连接，则onSocketDidDisconnect委托方法
//*将在此方法返回之前立即被调用。
//*
//*请注意释放AsyncSocket实例的推荐方式(例如在dealloc方法中)
//* (asyncSocket setDelegate: nil);
//* (asyncSocket断开);
//* (asyncSocket释放);
//* * /
- (void)disconnect;

/**
 * Disconnects after all pending reads have completed.
 * After calling this, the read and write methods will do nothing.
 * The socket will disconnect even if there are still pending writes.
**/
//* *
//在所有挂起的读取完成后断开连接。
//在调用这个函数之后，读写方法将不做任何事情。
//*即使仍有挂起的写操作，套接字将断开连接。
//* * /
- (void)disconnectAfterReading;

/**
 * Disconnects after all pending writes have completed.
 * After calling this, the read and write methods will do nothing.
 * The socket will disconnect even if there are still pending reads.
**/
/// * *
//在所有挂起的写完成后断开连接。
//在调用这个函数之后，读写方法将不做任何事情。
//socket将断开连接，即使仍有读操作。
//* * /
- (void)disconnectAfterWriting;

/**
 * Disconnects after all pending reads and writes have completed.
 * After calling this, the read and write methods will do nothing.
**/
/// * *
//在所有挂起的读和写完成后断开连接。
//在调用这个函数之后，读写方法将不做任何事情。
//* * /
- (void)disconnectAfterReadingAndWriting;

/* Returns YES if the socket and streams are open, connected, and ready for reading and writing. */
///*如果套接字和流是打开的，连接的，并准备好读写。* /
- (BOOL)isConnected;

/**
 * Returns the local or remote host and port to which this socket is connected, or nil and 0 if not connected.
 * The host will be an IP address.
**/
/// * *
//返回套接字连接到的本地或远程主机和端口，如果没有连接，则返回nil和0。
//主机是一个IP地址。
//* * /
- (NSString *)connectedHost;
- (UInt16)connectedPort;

- (NSString *)localHost;
- (UInt16)localPort;

/**
 * Returns the local or remote address to which this socket is connected,
 * specified as a sockaddr structure wrapped in a NSData object.
 * 
 * See also the connectedHost, connectedPort, localHost and localPort methods.
**/
/// * *
//返回套接字连接到的本地或远程地址，
//*指定为包装在NSData对象中的sockaddr结构。
//*
//*参见connectedHost, connectedPort, localHost和localPort方法。
//* * /
- (NSData *)connectedAddress;
- (NSData *)localAddress;

/**
 * Returns whether the socket is IPv4 or IPv6.
 * An accepting socket may be both.
**/
/// * *
//*返回套接字是IPv4还是IPv6。
//一个可接受的套接字可能是两个。
//* * /
- (BOOL)isIPv4;
- (BOOL)isIPv6;

// The readData and writeData methods won't block (they are asynchronous).
// 
// When a read is complete the onSocket:didReadData:withTag: delegate method is called.
// When a write is complete the onSocket:didWriteDataWithTag: delegate method is called.
// 
// You may optionally set a timeout for any read/write operation. (To not timeout, use a negative time interval.)
// If a read/write opertion times out, the corresponding "onSocket:shouldTimeout..." delegate method
// is called to optionally allow you to extend the timeout.
// Upon a timeout, the "onSocket:willDisconnectWithError:" method is called, followed by "onSocketDidDisconnect".
// 
// The tag is for your convenience.
// You can use it as an array index, step number, state id, pointer, etc.

/**
 * Reads the first available bytes that become available on the socket.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
**/
// readData和writeData方法不会阻塞(它们是异步的)。
//
//当读取完成时，onSocket:didReadData:withTag: delegate方法被调用。
//当写入完成时，onSocket:didWriteDataWithTag: delegate方法被调用。
//
//你可以选择设置任何读/写操作的超时。(为了不超时，使用负时间间隔。)
//如果读/写操作超时，相应的"onSocket:shouldTimeout…"委托方法
//可选地允许您扩展超时。
//在超时时，“onSocket:willDisconnectWithError:”方法被调用，后面跟着“onSocketDidDisconnect”。
//
//标签是为了您的方便。
//你可以使用它作为数组索引，步骤号，状态id，指针等。

/// * *
//读取套接字上可用的第一个可用字节。
//*
//*如果超时值为负，读取操作将不使用超时。
//* * /
- (void)readDataWithTimeout:(NSTimeInterval)timeout tag:(long)tag;

/**
 * Reads the first available bytes that become available on the socket.
 * The bytes will be appended to the given byte buffer starting at the given offset.
 * The given buffer will automatically be increased in size if needed.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
 * If the buffer if nil, the socket will create a buffer for you.
 * 
 * If the bufferOffset is greater than the length of the given buffer,
 * the method will do nothing, and the delegate will not be called.
 * 
 * If you pass a buffer, you must not alter it in any way while AsyncSocket is using it.
 * After completion, the data returned in onSocket:didReadData:withTag: will be a subset of the given buffer.
 * That is, it will reference the bytes that were appended to the given buffer.
**/
/// * *
//读取套接字上可用的第一个可用字节。
//*字节将从给定的偏移量开始追加到给定的字节缓冲区。
//*给定的缓冲区将自动增加大小，如果需要。
//*
//*如果超时值为负，读取操作将不使用超时。
//*如果该缓冲区为nil，套接字将为你创建一个缓冲区。
//*
//*如果bufferOffset大于给定缓冲区的长度，
//*该方法将不做任何事情，并且不会被调用委托。
//*
//*如果你传递一个缓冲区，你不能以任何方式改变它，而AsyncSocket正在使用它。
//完成后，onSocket:didReadData:withTag:返回的数据将是给定缓冲区的子集。
//*也就是说，它将引用附加到给定缓冲区的字节。
//* * /
- (void)readDataWithTimeout:(NSTimeInterval)timeout
					 buffer:(NSMutableData *)buffer
			   bufferOffset:(NSUInteger)offset
						tag:(long)tag;

/**
 * Reads the first available bytes that become available on the socket.
 * The bytes will be appended to the given byte buffer starting at the given offset.
 * The given buffer will automatically be increased in size if needed.
 * A maximum of length bytes will be read.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
 * If the buffer if nil, a buffer will automatically be created for you.
 * If maxLength is zero, no length restriction is enforced.
 * 
 * If the bufferOffset is greater than the length of the given buffer,
 * the method will do nothing, and the delegate will not be called.
 * 
 * If you pass a buffer, you must not alter it in any way while AsyncSocket is using it.
 * After completion, the data returned in onSocket:didReadData:withTag: will be a subset of the given buffer.
 * That is, it will reference the bytes that were appended to the given buffer.
**/
/// * *
//读取套接字上可用的第一个可用字节。
//*字节将从给定的偏移量开始追加到给定的字节缓冲区。
//*给定的缓冲区将自动增加大小，如果需要。
//*最大长度字节将被读取。
//*
//*如果超时值为负，读取操作将不使用超时。
//*如果缓冲区为空，将自动为您创建一个缓冲区。
//*如果maxLength为零，则没有长度限制。
//*
//*如果bufferOffset大于给定缓冲区的长度，
//*该方法将不做任何事情，并且不会被调用委托。
//*
//*如果你传递一个缓冲区，你不能以任何方式改变它，而AsyncSocket正在使用它。
//完成后，onSocket:didReadData:withTag:返回的数据将是给定缓冲区的子集。
//*也就是说，它将引用附加到给定缓冲区的字节。
//* * /
- (void)readDataWithTimeout:(NSTimeInterval)timeout
                     buffer:(NSMutableData *)buffer
               bufferOffset:(NSUInteger)offset
                  maxLength:(NSUInteger)length
                        tag:(long)tag;

/**
 * Reads the given number of bytes.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
 * 
 * If the length is 0, this method does nothing and the delegate is not called.
**/
/// * *
//读取给定的字节数。
//*
//*如果超时值为负，读取操作将不使用超时。
//*
//*如果长度为0，这个方法不做任何事情，不会调用委托。
//* * /
- (void)readDataToLength:(NSUInteger)length withTimeout:(NSTimeInterval)timeout tag:(long)tag;

/**
 * Reads the given number of bytes.
 * The bytes will be appended to the given byte buffer starting at the given offset.
 * The given buffer will automatically be increased in size if needed.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
 * If the buffer if nil, a buffer will automatically be created for you.
 * 
 * If the length is 0, this method does nothing and the delegate is not called.
 * If the bufferOffset is greater than the length of the given buffer,
 * the method will do nothing, and the delegate will not be called.
 * 
 * If you pass a buffer, you must not alter it in any way while AsyncSocket is using it.
 * After completion, the data returned in onSocket:didReadData:withTag: will be a subset of the given buffer.
 * That is, it will reference the bytes that were appended to the given buffer.
**/
/// * *
//读取给定的字节数。
//*字节将从给定的偏移量开始追加到给定的字节缓冲区。
//*给定的缓冲区将自动增加大小，如果需要。
//*
//*如果超时值为负，读取操作将不使用超时。
//*如果缓冲区为空，将自动为您创建一个缓冲区。
//*
//*如果长度为0，这个方法不做任何事情，不会调用委托。
//*如果bufferOffset大于给定缓冲区的长度，
//*该方法将不做任何事情，并且不会被调用委托。
//*
//*如果你传递一个缓冲区，你不能以任何方式改变它，而AsyncSocket正在使用它。
//完成后，onSocket:didReadData:withTag:返回的数据将是给定缓冲区的子集。
//*也就是说，它将引用附加到给定缓冲区的字节。
//* * /
- (void)readDataToLength:(NSUInteger)length
             withTimeout:(NSTimeInterval)timeout
                  buffer:(NSMutableData *)buffer
            bufferOffset:(NSUInteger)offset
                     tag:(long)tag;

/**
 * Reads bytes until (and including) the passed "data" parameter, which acts as a separator.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
 * 
 * If you pass nil or zero-length data as the "data" parameter,
 * the method will do nothing, and the delegate will not be called.
 * 
 * To read a line from the socket, use the line separator (e.g. CRLF for HTTP, see below) as the "data" parameter.
 * Note that this method is not character-set aware, so if a separator can occur naturally as part of the encoding for
 * a character, the read will prematurely end.
**/
/// * *
//*读取字节，直到(包括)传递的“data”参数，它充当分隔符。
//*
//*如果超时值为负，读取操作将不使用超时。
//*
//*如果你传递nil或0长度的数据作为“data”参数，
//*该方法将不做任何事情，并且不会被调用委托。
//*
//*从socket读取一行，使用行分隔符(例如HTTP的CRLF，见下文)作为“data”参数。
//*注意此方法不支持字符集，因此如果一个分隔符可以自然地作为编码的一部分出现
//*一个字符，读会过早地结束。
//* * /
- (void)readDataToData:(NSData *)data withTimeout:(NSTimeInterval)timeout tag:(long)tag;

/**
 * Reads bytes until (and including) the passed "data" parameter, which acts as a separator.
 * The bytes will be appended to the given byte buffer starting at the given offset.
 * The given buffer will automatically be increased in size if needed.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
 * If the buffer if nil, a buffer will automatically be created for you.
 * 
 * If the bufferOffset is greater than the length of the given buffer,
 * the method will do nothing, and the delegate will not be called.
 * 
 * If you pass a buffer, you must not alter it in any way while AsyncSocket is using it.
 * After completion, the data returned in onSocket:didReadData:withTag: will be a subset of the given buffer.
 * That is, it will reference the bytes that were appended to the given buffer.
 * 
 * To read a line from the socket, use the line separator (e.g. CRLF for HTTP, see below) as the "data" parameter.
 * Note that this method is not character-set aware, so if a separator can occur naturally as part of the encoding for
 * a character, the read will prematurely end.
**/
//*读取字节，直到(包括)传递的“data”参数，它充当分隔符。
//*字节将从给定的偏移量开始追加到给定的字节缓冲区。
//*给定的缓冲区将自动增加大小，如果需要。
//*
//*如果超时值为负，读取操作将不使用超时。
//*如果缓冲区为空，将自动为您创建一个缓冲区。
//*
//*如果bufferOffset大于给定缓冲区的长度，
//*该方法将不做任何事情，并且不会被调用委托。
//*
//*如果你传递一个缓冲区，你不能以任何方式改变它，而AsyncSocket正在使用它。
//完成后，onSocket:didReadData:withTag:返回的数据将是给定缓冲区的子集。
//*也就是说，它将引用附加到给定缓冲区的字节。
//*
//*从socket读取一行，使用行分隔符(例如HTTP的CRLF，见下文)作为“data”参数。
//*注意此方法不支持字符集，因此如果一个分隔符可以自然地作为编码的一部分出现
//*一个字符，读会过早地结束。
- (void)readDataToData:(NSData *)data
           withTimeout:(NSTimeInterval)timeout
                buffer:(NSMutableData *)buffer
          bufferOffset:(NSUInteger)offset
                   tag:(long)tag;

/**
 * Reads bytes until (and including) the passed "data" parameter, which acts as a separator.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
 * 
 * If maxLength is zero, no length restriction is enforced.
 * Otherwise if maxLength bytes are read without completing the read,
 * it is treated similarly to a timeout - the socket is closed with a AsyncSocketReadMaxedOutError.
 * The read will complete successfully if exactly maxLength bytes are read and the given data is found at the end.
 * 
 * If you pass nil or zero-length data as the "data" parameter,
 * the method will do nothing, and the delegate will not be called.
 * If you pass a maxLength parameter that is less than the length of the data parameter,
 * the method will do nothing, and the delegate will not be called.
 * 
 * To read a line from the socket, use the line separator (e.g. CRLF for HTTP, see below) as the "data" parameter.
 * Note that this method is not character-set aware, so if a separator can occur naturally as part of the encoding for
 * a character, the read will prematurely end.
**/
/// * *
//*读取字节，直到(包括)传递的“data”参数，它充当分隔符。
//*
//*如果超时值为负，读取操作将不使用超时。
//*
//*如果maxLength为零，则没有长度限制。
//*否则，如果读取maxLength字节没有完成读取，
//*它被类似于超时处理-套接字被一个AsyncSocketReadMaxedOutError关闭。
//*读取将成功完成，如果恰好最大长度字节被读取，并在最后找到给定的数据。
//*
//*如果你传递nil或0长度的数据作为“data”参数，
//*该方法将不做任何事情，并且不会被调用委托。
//*如果你传递一个maxLength参数小于数据的长度，
//*该方法将不做任何事情，并且不会被调用委托。
//*
//*从socket读取一行，使用行分隔符(例如HTTP的CRLF，见下文)作为“data”参数。
//*注意此方法不支持字符集，因此如果一个分隔符可以自然地作为编码的一部分出现
//*一个字符，读会过早地结束。
//* * /
- (void)readDataToData:(NSData *)data withTimeout:(NSTimeInterval)timeout maxLength:(NSUInteger)length tag:(long)tag;

/**
 * Reads bytes until (and including) the passed "data" parameter, which acts as a separator.
 * The bytes will be appended to the given byte buffer starting at the given offset.
 * The given buffer will automatically be increased in size if needed.
 * A maximum of length bytes will be read.
 * 
 * If the timeout value is negative, the read operation will not use a timeout.
 * If the buffer if nil, a buffer will automatically be created for you.
 * 
 * If maxLength is zero, no length restriction is enforced.
 * Otherwise if maxLength bytes are read without completing the read,
 * it is treated similarly to a timeout - the socket is closed with a AsyncSocketReadMaxedOutError.
 * The read will complete successfully if exactly maxLength bytes are read and the given data is found at the end.
 * 
 * If you pass a maxLength parameter that is less than the length of the data parameter,
 * the method will do nothing, and the delegate will not be called.
 * If the bufferOffset is greater than the length of the given buffer,
 * the method will do nothing, and the delegate will not be called.
 * 
 * If you pass a buffer, you must not alter it in any way while AsyncSocket is using it.
 * After completion, the data returned in onSocket:didReadData:withTag: will be a subset of the given buffer.
 * That is, it will reference the bytes that were appended to the given buffer.
 * 
 * To read a line from the socket, use the line separator (e.g. CRLF for HTTP, see below) as the "data" parameter.
 * Note that this method is not character-set aware, so if a separator can occur naturally as part of the encoding for
 * a character, the read will prematurely end.
**/
//*读取字节，直到(包括)传递的“data”参数，它充当分隔符。
//*字节将从给定的偏移量开始追加到给定的字节缓冲区。
//*给定的缓冲区将自动增加大小，如果需要。
//*最大长度字节将被读取。
//*
//*如果超时值为负，读取操作将不使用超时。
//*如果缓冲区为空，将自动为您创建一个缓冲区。
//*
//*如果maxLength为零，则没有长度限制。
//*否则，如果读取maxLength字节没有完成读取，
//*它被类似于超时处理-套接字被一个AsyncSocketReadMaxedOutError关闭。
//*读取将成功完成，如果恰好最大长度字节被读取，并在最后找到给定的数据。
//*
//*如果你传递一个maxLength参数小于数据的长度，
//*该方法将不做任何事情，并且不会被调用委托。
//*如果bufferOffset大于给定缓冲区的长度，
//*该方法将不做任何事情，并且不会被调用委托。
//*
//*如果你传递一个缓冲区，你不能以任何方式改变它，而AsyncSocket正在使用它。
//完成后，onSocket:didReadData:withTag:返回的数据将是给定缓冲区的子集。
//*也就是说，它将引用附加到给定缓冲区的字节。
//*
//*从socket读取一行，使用行分隔符(例如HTTP的CRLF，见下文)作为“data”参数。
//*注意此方法不支持字符集，因此如果一个分隔符可以自然地作为编码的一部分出现
//*一个字符，读会过早地结束。
- (void)readDataToData:(NSData *)data
           withTimeout:(NSTimeInterval)timeout
                buffer:(NSMutableData *)buffer
          bufferOffset:(NSUInteger)offset
             maxLength:(NSUInteger)length
                   tag:(long)tag;

/**
 * Writes data to the socket, and calls the delegate when finished.
 * 
 * If you pass in nil or zero-length data, this method does nothing and the delegate will not be called.
 * If the timeout value is negative, the write operation will not use a timeout.
**/
/// * *
//*将数据写入套接字，并在完成后调用委托。
//*
//*如果你传入nil或0长度的数据，这个方法什么也不做，不会调用委托。
//*如果超时值是负的，写操作将不使用超时。
//* * /
- (void)writeData:(NSData *)data withTimeout:(NSTimeInterval)timeout tag:(long)tag;

/**
 * Returns progress of current read or write, from 0.0 to 1.0, or NaN if no read/write (use isnan() to check).
 * "tag", "done" and "total" will be filled in if they aren't NULL.
**/
/// * *
//返回当前读或写的进度，从0.0到1.0，如果没有读/写，返回NaN(使用isnan()检查)。
//*“tag”，“done”和“total”将被填充，如果他们不为空。
//* * /
- (float)progressOfReadReturningTag:(long *)tag bytesDone:(NSUInteger *)done total:(NSUInteger *)total;
- (float)progressOfWriteReturningTag:(long *)tag bytesDone:(NSUInteger *)done total:(NSUInteger *)total;

/**
 * Secures the connection using SSL/TLS.
 * 
 * This method may be called at any time, and the TLS handshake will occur after all pending reads and writes
 * are finished. This allows one the option of sending a protocol dependent StartTLS message, and queuing
 * the upgrade to TLS at the same time, without having to wait for the write to finish.
 * Any reads or writes scheduled after this method is called will occur over the secured connection.
 * 
 * The possible keys and values for the TLS settings are well documented.
 * Some possible keys are:
 * - kCFStreamSSLLevel
 * - kCFStreamSSLAllowsExpiredCertificates
 * - kCFStreamSSLAllowsExpiredRoots
 * - kCFStreamSSLAllowsAnyRoot
 * - kCFStreamSSLValidatesCertificateChain
 * - kCFStreamSSLPeerName
 * - kCFStreamSSLCertificates
 * - kCFStreamSSLIsServer
 * 
 * Please refer to Apple's documentation for associated values, as well as other possible keys.
 * 
 * If you pass in nil or an empty dictionary, the default settings will be used.
 * 
 * The default settings will check to make sure the remote party's certificate is signed by a
 * trusted 3rd party certificate agency (e.g. verisign) and that the certificate is not expired.
 * However it will not verify the name on the certificate unless you
 * give it a name to verify against via the kCFStreamSSLPeerName key.
 * The security implications of this are important to understand.
 * Imagine you are attempting to create a secure connection to MySecureServer.com,
 * but your socket gets directed to MaliciousServer.com because of a hacked DNS server.
 * If you simply use the default settings, and MaliciousServer.com has a valid certificate,
 * the default settings will not detect any problems since the certificate is valid.
 * To properly secure your connection in this particular scenario you
 * should set the kCFStreamSSLPeerName property to "MySecureServer.com".
 * If you do not know the peer name of the remote host in advance (for example, you're not sure
 * if it will be "domain.com" or "www.domain.com"), then you can use the default settings to validate the
 * certificate, and then use the X509Certificate class to verify the issuer after the socket has been secured.
 * The X509Certificate class is part of the CocoaAsyncSocket open source project.
**/
/// * *
//*使用SSL/TLS安全连接。
//*
//这个方法可以在任何时候被调用，TLS握手将在所有未决的读和写之后发生
//*完成。这允许用户选择发送依赖于协议的StartTLS消息并进行排队
//*同时升级到TLS，无需等待写入完成。
//调用此方法后的任何读或写操作都将在安全连接上进行。
//*
//* TLS设置的可能的键和值都有很好的文档说明。
//*一些可能的关键是:
//*——kCFStreamSSLLevel
//*——kCFStreamSSLAllowsExpiredCertificates
//*——kCFStreamSSLAllowsExpiredRoots
//*——kCFStreamSSLAllowsAnyRoot
//*——kCFStreamSSLValidatesCertificateChain
//*——kCFStreamSSLPeerName
//*——kCFStreamSSLCertificates
//*——kCFStreamSSLIsServer
//*
//*相关值请参考苹果文档，以及其他可能的密钥。
//*
//*如果你传入nil或空字典，将使用默认设置。
//*
//*默认设置将检查，以确保远程方的证书是由a签署的
//*可信的第三方证书代理机构(如verisign)，证书没有过期。
//*但是，它不会验证证书上的名称，除非您
//*给它一个名字来验证通过kCFStreamSSLPeerName键。
//*理解这其中的安全含义很重要。
//*假设你正在尝试创建一个MySecureServer.com的安全连接，
//*但你的socket被定向到MaliciousServer.com，因为一个黑客DNS服务器。
//*如果您只是使用默认设置，并且MaliciousServer.com有一个有效的证书，
//*默认设置不会检测到任何问题，因为证书是有效的。
//*要在此特定场景中妥善保护您的连接
//*应该设置kCFStreamSSLPeerName属性为“MySecureServer.com”。
//*如果您事先不知道远程主机的对等主机名(例如，您不确定)
//*如果它将是“domain.com”或“www.domain.com”)，那么您可以使用默认设置来验证
//*证书，然后在套接字得到保护后使用X509Certificate类验证颁发者。
//* X509Certificate类是CocoaAsyncSocket开源项目的一部分。
//* * /
- (void)startTLS:(NSDictionary *)tlsSettings;

/**
 * For handling readDataToData requests, data is necessarily read from the socket in small increments.
 * The performance can be much improved by allowing AsyncSocket to read larger chunks at a time and
 * store any overflow in a small internal buffer.
 * This is termed pre-buffering, as some data may be read for you before you ask for it.
 * If you use readDataToData a lot, enabling pre-buffering will result in better performance, especially on the iPhone.
 * 
 * The default pre-buffering state is controlled by the DEFAULT_PREBUFFERING definition.
 * It is highly recommended one leave this set to YES.
 * 
 * This method exists in case pre-buffering needs to be disabled by default for some unforeseen reason.
 * In that case, this method exists to allow one to easily enable pre-buffering when ready.
**/
/// * *
//*为了处理readDataToData请求，数据必须以小增量从套接字读取。
//*通过允许AsyncSocket一次读取更大的块，性能可以大大提高
//*在一个小的内部缓冲区中存储任何溢出。
//*这被称为预缓冲，因为有些数据可能在你请求它之前就已经为你读取了。
//*如果你经常使用readDataToData，启用预缓冲将会带来更好的性能，特别是在iPhone上。
//*
//默认预缓冲状态由DEFAULT_PREBUFFERING定义控制。
//*强烈建议将此设置保留为YES。
//*
//*此方法存在的情况下，预缓冲需要在默认情况下，由于某些不可预见的原因禁用。
//*在这种情况下，这个方法的存在是为了允许人们在准备好时轻松地启用预缓冲。
//* * /
- (void)enablePreBuffering;

/**
 * When you create an AsyncSocket, it is added to the runloop of the current thread.
 * So for manually created sockets, it is easiest to simply create the socket on the thread you intend to use it.
 * 
 * If a new socket is accepted, the delegate method onSocket:wantsRunLoopForNewSocket: is called to
 * allow you to place the socket on a separate thread. This works best in conjunction with a thread pool design.
 * 
 * If, however, you need to move the socket to a separate thread at a later time, this
 * method may be used to accomplish the task.
 * 
 * This method must be called from the thread/runloop the socket is currently running on.
 * 
 * Note: After calling this method, all further method calls to this object should be done from the given runloop.
 * Also, all delegate calls will be sent on the given runloop.
**/
/// * *
//当你创建一个AsyncSocket时，它被添加到当前线程的运行循环中。
//*对于手动创建的套接字，最简单的方法是在你打算使用它的线程上创建套接字。
//*
//*如果一个新的套接字被接受，委托方法onSocket:wantsRunLoopForNewSocket:被调用
//允许你把套接字放在一个单独的线程上。这最好与线程池设计结合使用。
//*
//*如果，然而，你需要移动套接字到一个单独的线程在以后的时间，这个
//方法可以用来完成任务。
//*
//*此方法必须从当前socket正在运行的线程/运行循环调用。
//*
//*注意:在调用此方法之后，所有对该对象的进一步方法调用都应该从给定的runloop中完成。
//*同样，所有的委托调用将被发送给给定的runloop。
//* * /
- (BOOL)moveToRunLoop:(NSRunLoop *)runLoop;

/**
 * Allows you to configure which run loop modes the socket uses.
 * The default set of run loop modes is NSDefaultRunLoopMode.
 * 
 * If you'd like your socket to continue operation during other modes, you may want to add modes such as
 * NSModalPanelRunLoopMode or NSEventTrackingRunLoopMode. Or you may simply want to use NSRunLoopCommonModes.
 * 
 * Accepted sockets will automatically inherit the same run loop modes as the listening socket.
 * 
 * Note: NSRunLoopCommonModes is defined in 10.5. For previous versions one can use kCFRunLoopCommonModes.
**/
/// * *
//*允许您配置套接字使用的运行循环模式。
//*运行循环模式的默认设置是NSDefaultRunLoopMode。
//*
//*如果您想让您的套接字在其他模式下继续操作，您可能需要添加以下模式
//* NSModalPanelRunLoopMode或NSEventTrackingRunLoopMode。或者您可能只是想使用NSRunLoopCommonModes。
//*
//被接受的套接字将自动继承与监听套接字相同的运行循环模式。
//*
//*注意:NSRunLoopCommonModes在10.5中定义。对于以前的版本，可以使用kCFRunLoopCommonModes。
//* * /
- (BOOL)setRunLoopModes:(NSArray *)runLoopModes;
- (BOOL)addRunLoopMode:(NSString *)runLoopMode;
- (BOOL)removeRunLoopMode:(NSString *)runLoopMode;

/**
 * Returns the current run loop modes the AsyncSocket instance is operating in.
 * The default set of run loop modes is NSDefaultRunLoopMode.
**/
/// * *
//*返回AsyncSocket实例正在运行的当前运行循环模式。
//*运行循环模式的默认设置是NSDefaultRunLoopMode。
//* * /
- (NSArray *)runLoopModes;

/**
 * In the event of an error, this method may be called during onSocket:willDisconnectWithError: to read
 * any data that's left on the socket.
**/
/// * *
//如果发生错误，可以在onSocket:willDisconnectWithError:期间调用该方法来读取
//*任何留在套接字上的数据。
//* * /
- (NSData *)unreadData;

/* A few common line separators, for use with the readDataToData:... methods. */
+ (NSData *)CRLFData;   // 0x0D0A
+ (NSData *)CRData;     // 0x0D
+ (NSData *)LFData;     // 0x0A
+ (NSData *)ZeroData;   // 0x00

@end
