//
//  InputMenuCell.m
//  UIKit
//
//  Created by kennethmiao on 2018/9/20.
//  Copyright © 2018年 Tencent. All rights reserved.
//

#import "LLMenuCell.h"
#import "THeader.h"
#import "LLUIKit.h"
#import "LLImageCache.h"
@implementation LLMenuCellData
@end

@implementation LLMenuCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self setupViews];
        [self defaultLayout];
    }
    return self;
}

- (void)setupViews
{
    self.backgroundColor = TMenuCell_UnSelected_Background_Color;
    _menu = [[UIImageView alloc] init];
    [self addSubview:_menu];
}

- (void)defaultLayout
{
}

- (void)setData:(LLMenuCellData *)data
{
    //set data动态。目前只有一种表情直接写死
//    _menu.image = [[LLImageCache sharedInstance] getFaceFromCache:data.path];
    _menu.image = kImage(@"emjib_name");
    if(data.isSelected){
        self.backgroundColor = TMenuCell_Selected_Background_Color;
    }
    else{
        self.backgroundColor = TMenuCell_UnSelected_Background_Color;
    }
    //update layout
    CGSize size = self.frame.size;
    _menu.frame = CGRectMake(TMenuCell_Margin, TMenuCell_Margin, size.width - 2 * TMenuCell_Margin, size.height - 2 * TMenuCell_Margin);
    _menu.contentMode = UIViewContentModeScaleAspectFit;

}
@end
