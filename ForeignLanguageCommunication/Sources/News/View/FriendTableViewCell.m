//
//  FriendTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/14.
//

#import "FriendTableViewCell.h"

@implementation FriendTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.icon.layer.masksToBounds = YES;
    self.icon.layer.cornerRadius = 20;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(FriendModel *)model {
    _model = model;
    
    self.nickName.text = model.nickname;
    [self.icon sd_setImageWithURL:model.avatar.mj_url];
    self.vipImgv.hidden = YES;
}
-(void)setModelGroupList:(GroupListModel *)model{
    
    if (IS_VALID_STRING(model.name)) {
        self.nickName.text = model.name;
    }
    if (IS_VALID_STRING(model.url)) {
        [self.icon sd_setImageWithURL:[NSURL URLWithString:model.url] placeholderImage:[UIImage imageNamed:@"测试照片3"]];
    }
    self.vipImgv.hidden = YES;
    
}
@end
