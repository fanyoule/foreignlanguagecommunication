//
//  FriendTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/14.
//

#import <UIKit/UIKit.h>
#import "FriendModel.h"
#import "GroupListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FriendTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UIImageView *vipImgv;

@property (nonatomic, strong) FriendModel *model;

-(void)setModelGroupList:(GroupListModel *)model;

@end

NS_ASSUME_NONNULL_END
