//
//  FriendViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/14.
//

#import "FriendViewController.h"
#import "FriendTableViewCell.h"
#import "DSCollectionViewIndex.h"
#import "UITableView+SCIndexView.h"
#import "SCIndexViewHeaderView.h"
#import "FriendModel.h"
#import "BMChineseSort.h"
#import "MyPersonalInformationVC.h"


@interface FriendViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    BOOL isSearch;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *titleArr;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSMutableArray *searchArr;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) SCIndexViewConfiguration *configuration;


@end

@implementation FriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isSearch = NO;
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"我的好友";

    self.dataArr = [NSMutableArray array];
    self.titleArr = [NSMutableArray array];
    self.array = [NSMutableArray array];
    self.searchArr = [NSMutableArray array];
    
    [self creatUI];
    [self getdata];
}
-(void)creatUI{
    self.topConstraint.constant = SNavBarHeight;
    [self.view layoutIfNeeded];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = kBackgroundColor;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:[FriendTableViewCell toString] bundle:nil] forCellReuseIdentifier:[FriendTableViewCell toString]];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60)];
    self.searchBar.placeholder = @"搜索";
    self.tableView.tableHeaderView = self.searchBar;
    self.searchBar.delegate = self;
    [self.tableView registerClass:SCIndexViewHeaderView.class forHeaderFooterViewReuseIdentifier:SCIndexViewHeaderView.reuseID];
    [self.tableView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.tableView);
    }];

}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchArr = [NSMutableArray array];
    if (searchText && searchText.length > 0) {
        isSearch = YES;
        for (FriendModel *model in self.array) {
            if ([model.nickname containsString:searchText]) {
                [self.searchArr addObject:model];
            }
        }
        [self.tableView setHiddenIndexView:YES];
    } else {
        isSearch = NO;
        [self.tableView setHiddenIndexView:NO];
    }
    [self.tableView reloadData];
}
- (void)addSCIndex {
    SCIndexViewConfiguration *configuration = [SCIndexViewConfiguration configuration];
    configuration.indexItemHeight = 18;
    configuration.indexItemSelectedBackgroundColor = kTHEMECOLOR;
    self.tableView.sc_indexViewConfiguration = configuration;
    self.tableView.sc_translucentForTableViewInNavigationBar = NO;
    NSMutableArray *arr = [NSMutableArray arrayWithArray:self.titleArr];
    [arr insertObject:UITableViewIndexSearch atIndex:0];
    self.tableView.sc_indexViewDataSource = arr.copy;
    self.tableView.sc_startSection = 0;
    self.configuration = configuration;
}
- (void)getdata {
    kWeakSelf(self)
    [RequestManager friendsListWithId:[UserInfoManager shared].getUserID withSuccess:^(id  _Nullable response) {
        weakself.array = [FriendModel mj_objectArrayWithKeyValuesArray:response[@"data"]];
        weakself.nullView.hidden = weakself.array.count;
        [weakself refresh];
    } withFail:^(NSError * _Nullable error) {
    }];
}
- (void)refresh {
    if (self.array.count > 0) {
        BMChineseSortSetting.share.sortMode = 2; // 1或2
        //排序 Person对象
        kWeakSelf(self)
        [BMChineseSort sortAndGroup:self.array key:@"nickname" finish:^(bool isSuccess, NSMutableArray *unGroupArr, NSMutableArray *sectionTitleArr, NSMutableArray<NSMutableArray *> *sortedObjArr) {
            if (isSuccess) {
                weakself.titleArr = sectionTitleArr;
                weakself.dataArr = sortedObjArr;
                [weakself.tableView reloadData];
                [weakself addSCIndex];
            }
        }];
    }
    NSLog(@"***");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (isSearch) {
        return 1;
    } else {
        return self.titleArr.count;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SCIndexViewHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SCIndexViewHeaderView.reuseID];
    if (isSearch) {
        [headerView configWithTitle:@""];
    } else {
        [headerView configWithTitle:self.titleArr[section]];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (isSearch) {
        return 0.001;
    } else {
        return SCIndexViewHeaderView.headerViewHeight;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isSearch) {
        return self.searchArr.count;
    } else {
        NSArray *arr = self.dataArr[section];
        return arr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[FriendTableViewCell toString]];
    if (isSearch) {
        cell.model = self.searchArr[indexPath.row];
    } else {
        NSArray *arr = self.dataArr[indexPath.section];
        cell.model = arr[indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isSearch) {
        [self headerClick:self.searchArr[indexPath.row]];
    } else {
        NSArray *arr = self.dataArr[indexPath.section];
        [self headerClick:arr[indexPath.row]];
    }
}
- (void)headerClick:(FriendModel *)model {
    MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc] init];
    vc.ID = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
