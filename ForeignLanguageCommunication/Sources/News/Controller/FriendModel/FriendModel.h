//
//  FriendModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/2/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FriendModel : NSObject
@property (nonatomic, copy) NSString *area;       // 区
@property (nonatomic, copy) NSString *avatar;     // 头像
@property (nonatomic, copy) NSString *birthday;   // 生日
@property (nonatomic, copy) NSString *city;       // 市
@property (nonatomic, copy) NSString *coachName;  // 教练名
@property (nonatomic, copy) NSString *createDate; // 创建时间
@property (nonatomic, assign) long height;        // 身高
@property (nonatomic, assign) long weight;        // 体重
@property (nonatomic, copy) NSString *hobby;      // 爱好
@property (nonatomic, copy) NSString *idCard;     // 身份证号
@property (nonatomic, copy) NSString *introduce;  // 个人介绍
@property (nonatomic, copy) NSString *inviteCode; // 邀请码
@property (nonatomic, copy) NSString *isCoach;    // 是否教练
@property (nonatomic, copy) NSString *isFollow;   // 是否关注
@property (nonatomic, copy) NSString *loginLat;   // 登陆纬度
@property (nonatomic, copy) NSString *loginLot;   // 登陆经度
@property (nonatomic, copy) NSString *nickname;   // 昵称
@property (nonatomic, copy) NSString *occupation; // 职业
@property (nonatomic, copy) NSString *passwd;     // 密码
@property (nonatomic, copy) NSString *phone;      // 手机
@property (nonatomic, copy) NSString *province;   // 省
@property (nonatomic, copy) NSString *qq;         // QQopenId
@property (nonatomic, copy) NSString *realName;   // 姓名
@property (nonatomic, copy) NSString *safemodePwd;    // 青少年模式密码
@property (nonatomic, assign) long safemodeStatus;    // 青少年模式开关：0关闭 1开启
@property (nonatomic, assign) long sex;               // 性别：0 女，1 男
@property (nonatomic, assign) long status;            // 状态
@property (nonatomic, assign) long ID;                // id
@property (nonatomic, copy) NSString *sysId;          // 系统用户id
@property (nonatomic, copy) NSString *wx;             // 微信openId
@end

NS_ASSUME_NONNULL_END
