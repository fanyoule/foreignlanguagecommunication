//
//  NewsViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/16.
//  消息

#import "NewsViewController.h"
#import <JXCategoryView/JXCategoryView.h>
#import "TheMessageViewController.h"//消息

#import "MessageChatViewController.h"//聊天
#import "SystemMessageViewController.h"//系统消息

//#import "TheRoomViewController.h"//房间
@interface NewsViewController ()<JXCategoryViewDelegate,UIGestureRecognizerDelegate,JXCategoryListContainerViewDelegate>
@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;


@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;

@property (nonatomic, strong) MessageChatViewController *messageVC;
@property (nonatomic, strong) SystemMessageViewController *systemVc;
@end

@implementation NewsViewController
- (instancetype)init {
    if (self = [super init]) {
        self.view.backgroundColor = [UIColor whiteColor];
        [self setUpNavView];
        [self setUpSubView];
        [self.view bringSubviewToFront:self.navView];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setBadgeNum) name:@"setBadgeNum" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin) name:@"didLogin" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin) name:@"logOut" object:nil];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.backgroundColor = [UIColor whiteColor];
//    [self setUpNavView];
//    [self setUpSubView];
//    [self.view bringSubviewToFront:self.navView];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setBadgeNum) name:@"setBadgeNum" object:nil];
}
- (void)didLogin {
    [self.messageVC refreshData];
    [self.systemVc refreshData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)setBadgeNum {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChatTotalUnread" object:@{@"badge": @(self.messageVC.unReadNum + self.systemVc.unReadNum)}];
}
- (void)setUpNavView {
    self.navView.backgroundColor = UIColor.clearColor;
    self.navTitleString = @"消息";
}

- (void)setUpSubView {
    [self.view addSubview:self.categoryTitleView];
    self.categoryTitleView.frame = CGRectMake(0, SNavBarHeight, KSW, 45);
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    [self.view addSubview:self.listContainerView];
    self.listContainerView.frame = CGRectMake(0, SNavBarHeight+50, KSW, ScreenHeight-SNavBarHeight-TabbarHeight-50);
    self.categoryTitleView.listContainer = self.listContainerView;
    
    self.messageVC = [[MessageChatViewController alloc] init];
    [self.messageVC loadDataSource];
    
    self.systemVc = [[SystemMessageViewController alloc]init];
    [self.systemVc loadDataSource];
}

//MARK: list container delegate
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView{
    return 2;
}
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    if (index == 0) {
        return self.messageVC;
    }
    return self.systemVc;
//    if (index == 0) {
//        MessageChatViewController *messageVC = [[MessageChatViewController alloc] init];
//        return messageVC;
//    }
//    return [[SystemMessageViewController alloc]init];
}


//MARK: categorytitle delegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
//MARK: Getter
- (JXCategoryTitleView *)categoryTitleView {
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titles = @[@"消息",@"系统消息"];
        _categoryTitleView.titleColor = RGBA(102, 102, 102, 1);
        _categoryTitleView.titleSelectedColor = CustomColor(@"#3478F5");
        _categoryTitleView.titleFont = kFont(14);
        _categoryTitleView.titleSelectedFont = kFont_Bold(14);
        _categoryTitleView.cellSpacing = 30;
        _categoryTitleView.cellWidth = 35;
        
        _categoryTitleView.collectionView.scrollEnabled = NO;
        _categoryTitleView.backgroundColor = UIColor.clearColor;
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorColor = CustomColor(@"#3478F5");
        lineView.indicatorWidth = 20;
        lineView.indicatorHeight = 3;
        lineView.indicatorCornerRadius = 1.5;
        _categoryTitleView.indicators = @[lineView];
        _categoryTitleView.delegate = self;
    }
    return _categoryTitleView;
}

@end
