//
//  SystemMessageViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "SystemMessageViewController.h"
#import "ChatInformationPersonTableViewCell.h"
#import "SystemMessageCommentsViewController.h"// 评论
#import "SystemMessageWalletViewController.h"// 钱包
#import "SystemMessageOfficialViewController.h"// 官方
#import "SystemMessageCourseViewController.h"// 课程
#import "SystemMegListModel.h"
#import "GroupHelpViewController.h"

@interface SystemMessageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation SystemMessageViewController
- (instancetype)init {
    if (self = [super init]) {
        
    }
    return self;
}
- (UIView *)listView{
    return self.view;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self getData];
    
    //self.dataArray = @[@"评论",@"官方",@"钱包",@"课程"].mutableCopy;
    // Do any additional setup after loading the view.
}
- (void)loadDataSource {
    self.unReadNum = 0;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
    self.mainTableView.mj_header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(getData)];
    
    [self getData];
}
- (void)refreshData {
    [self getData];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"systemMsgCellId";
    SystemMegListModel *model = self.dataArray[indexPath.row];
    ChatInformationPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[ChatInformationPersonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.numberLabel.hidden = YES;
    cell.sysModel = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
/** tableView 点击事件*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        // 评论
        SystemMessageCommentsViewController *vc = [SystemMessageCommentsViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1){
        // 官方
        SystemMessageOfficialViewController *vc = [SystemMessageOfficialViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 2){
        // 钱包
        SystemMessageWalletViewController *vc = [SystemMessageWalletViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 3){
        // 课程
        SystemMessageCourseViewController *vc = [SystemMessageCourseViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    } else{
        //群组 消息
        GroupHelpViewController *vc = [[GroupHelpViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)stopLoading {
    if (self.mainTableView.mj_header.isRefreshing) {
        [self.mainTableView.mj_header endRefreshing];
    }
}
- (void)getData{
    if (![UserInfoManager shared].isLogin) {
        self.unReadNum = 0;
        [self.dataArray removeAllObjects];
        [self stopLoading];
        [self.mainTableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"setBadgeNum" object:nil];
        return;
    }
    NSInteger userID = [[UserInfoManager shared] getUserID];
    [RequestManager indexWithUserId:userID withSuccess:^(id  _Nullable response) {
        [self stopLoading];
        NSArray *array = response[@"data"];
        [self.dataArray removeAllObjects];
        long num = 0;
        for (int i = 0; i < array.count; i ++) {
            SystemMegListModel *model = [SystemMegListModel mj_objectWithKeyValues:array[i]];
            [self.dataArray addObject:model];
            num = num + model.notReadNum;
        }
        self.unReadNum = num;
        [self.mainTableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"setBadgeNum" object:nil];
    } withFail:^(NSError * _Nullable error) {
        [self stopLoading];
    }];
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
