//
//  SystemMessageViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SystemMessageViewController : BaseViewController<JXCategoryListContentViewDelegate>
@property (nonatomic, assign) long unReadNum;

- (void)loadDataSource;
- (void)refreshData;
@end

NS_ASSUME_NONNULL_END
