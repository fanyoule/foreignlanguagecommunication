//
//  SystemMessageCourseViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//  课程

#import "SystemMessageCourseViewController.h"
#import "ChatInformationPersonTableViewCell.h"
@interface SystemMessageCourseViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic) int page;
@property (nonatomic) int lastPage;
@end

@implementation SystemMessageCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.lastPage = 0;
    self.navTitleString = @"课程";
    
    [self creatUI];
    [self getData];
}
-(void)creatUI{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGBA(238, 238, 238, 1);
    [self.navView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@0.5);
        make.left.right.equalTo(self.navView);
        make.bottom.equalTo(self.navView);
    }];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    [self.mainTableView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.mainTableView);
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
        ChatInformationPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatInformationPersonTableViewCell"];
        if (!cell) {
            cell = [[ChatInformationPersonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatInformationPersonTableViewCell"];
        }
    cell.numberLabel.hidden = YES;
    cell.courseModel = self.dataArray[indexPath.row];
         
    return cell;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSMutableArray *)dataArray{
   if (!_dataArray) {
       _dataArray = [NSMutableArray array];
   }
   return _dataArray;
}


- (void)getData {
   
   if (self.lastPage>0 && self.lastPage <= self.page) {
       [self stopRefresh];
       [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
       return;
   }
    
    [RequestManager coursesMessageWithUserId:[[UserInfoManager shared] getUserID] pageNum:self.page pageSize:10 withSuccess:^(id  _Nullable response) {
        [self stopRefresh];
        if (self.page == 1) {
            self.page ++;
            [self.dataArray removeAllObjects];
        }
        
        NSArray *array = response[@"data"][@"list"];
        for (int i = 0; i < array.count; i ++) {
            MessageCourseModel *model = [MessageCourseModel mj_objectWithKeyValues:array[i]];
            [self.dataArray addObject:model];
        }
        self.nullView.hidden = self.dataArray.count;
        [self.mainTableView reloadData];
        self.lastPage = [response[@"data"][@"lastPage"] intValue];
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
  
}

- (void)stopRefresh{
   [self.mainTableView.mj_header endRefreshing];
   [self.mainTableView.mj_footer endRefreshing];
}
@end
