//
//  MessageCourseModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/2/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageCourseModel : NSObject
/**{"userId":40,"userName":"187","headUrl":"http://pic.sxsyingyu.com/27m3ho1lwhf8v61dtxoo.jpeg","msgType":3,"coursesId":23,"coursesName":"大学英语精读","createTime":"2021-01-21"}*/
/**用户名*/
@property (nonatomic, copy) NSString *userName;
@property (nonatomic) NSInteger userId;
@property (nonatomic, copy) NSString *headUrl;
@property (nonatomic) NSInteger msgType;
@property (nonatomic) NSInteger coursesId;
@property (nonatomic, copy) NSString *coursesName;
@property (nonatomic, copy) NSString *createTime;

@end

NS_ASSUME_NONNULL_END
