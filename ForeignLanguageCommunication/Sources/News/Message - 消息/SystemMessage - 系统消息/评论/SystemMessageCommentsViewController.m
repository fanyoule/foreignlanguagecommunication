//
//  SystemMessageCommentsViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//  评论

#import "SystemMessageCommentsViewController.h"
#import "ChatInformationPersonTableViewCell.h"
#import "WorldDetailViewController.h"

@interface SystemMessageCommentsViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic) int page;
@property (nonatomic) int lastPage;
@end

@implementation SystemMessageCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.lastPage = 0;
    self.navTitleString = @"评论";
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGBA(238, 238, 238, 1);
    [self.navView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@0.5);
        make.left.right.equalTo(self.navView);
        make.bottom.equalTo(self.navView);
    }];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    [self getData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
        ChatInformationPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatInformationPersonTableViewCell"];
        if (!cell) {
            cell = [[ChatInformationPersonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatInformationPersonTableViewCell"];
        }
    cell.numberLabel.hidden = YES;
    cell.commentModel = self.dataArray[indexPath.row];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageCommentModel *model = self.dataArray[indexPath.row];
//    WorldListModel *model;
    //跳转动态
    WorldDetailViewController *vc = [[WorldDetailViewController alloc] init];
    vc.worksID = model.publishId;
    [self.navigationController pushViewController:vc animated:YES];
}


- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)getData {
    
    if (self.lastPage>0 && self.lastPage <= self.page) {
        [self stopRefresh];
        [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        return;
    }
    
    [RequestManager commentMessageWithUserId:[[UserInfoManager shared] getUserID] pageNum:self.page pageSize:10 withSuccess:^(id  _Nullable response) {
        [self stopRefresh];
        if (self.page == 1) {
            self.page ++;
            [self.dataArray removeAllObjects];
        }
        
        NSArray *array = response[@"data"][@"list"];
        for (int i = 0; i < array.count; i ++) {
            MessageCommentModel *model = [MessageCommentModel mj_objectWithKeyValues:array[i]];
            [self.dataArray addObject:model];
        }
        
        [self.mainTableView reloadData];
        self.lastPage = [response[@"data"][@"lastPage"] intValue];
        
        /*
         firstPage = 1;
         hasNextPage = 1;
         hasPreviousPage = 0;
         isFirstPage = 1;
         isLastPage = 0;
         lastPage = 2;
         */
        NSLog(@"%@",response);
    } withFail:^(NSError * _Nullable error) {
        [self stopRefresh];
    }];
}

- (void)stopRefresh{
    [self.mainTableView.mj_header endRefreshing];
    [self.mainTableView.mj_footer endRefreshing];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
