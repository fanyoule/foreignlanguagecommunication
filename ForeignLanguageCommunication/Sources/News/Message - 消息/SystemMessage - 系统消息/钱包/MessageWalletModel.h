//
//  MessageWalletModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/2/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageWalletModel : NSObject
//{"orderId":27,"coursesId":22,"status":1,"coursesName":"数学","msgType":1,"message":"支付成功","createTime":"2020-12-31"}
/**订单号*/
@property (nonatomic) NSInteger orderId;
//课程号
@property (nonatomic) NSInteger coursesId;
/**状态*/
@property (nonatomic) NSInteger status;
/**课程名*/
@property (nonatomic, copy) NSString *coursesName;
/**消息类型*/
@property (nonatomic) NSInteger msgType;
/**消息内容*/
@property (nonatomic, copy) NSString *message;
/**时间*/
@property (nonatomic, copy) NSString *createTime;

@end

NS_ASSUME_NONNULL_END
