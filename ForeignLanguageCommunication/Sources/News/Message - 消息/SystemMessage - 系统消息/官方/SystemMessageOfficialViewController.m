//
//  SystemMessageOfficialViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//  官方

#import "SystemMessageOfficialViewController.h"
#import "OfficialTableViewCell.h"
#import "ChatInformationPersonTableViewCell.h"
#import "HomeWebViewController.h"

@interface SystemMessageOfficialViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic) int page;
@property (nonatomic) int lastPage;
@end

@implementation SystemMessageOfficialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"官方";
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGBA(238, 238, 238, 1);
    [self.navView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@0.5);
        make.left.right.equalTo(self.navView);
        make.bottom.equalTo(self.navView);
    }];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    // Do any additional setup after loading the view.
    [self getData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatInformationPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatInformationPersonTableViewCell"];
    if (!cell) {
        cell = [[ChatInformationPersonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatInformationPersonTableViewCell"];
    }
    cell.numberLabel.hidden = YES;
    cell.officialModel = self.dataArray[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //网页
    MessageOfficialModel *model = self.dataArray[indexPath.row];
    HomeWebViewController *vc = [[HomeWebViewController alloc] init];
    vc.titleStr = model.topic;
    vc.url = model.content;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSMutableArray *)dataArray{
   if (!_dataArray) {
       _dataArray = [NSMutableArray array];
   }
   return _dataArray;
}

- (void)getData {
   
   if (self.lastPage>0 && self.lastPage <= self.page) {
       [self stopRefresh];
       [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
       return;
   }
    [RequestManager officialMessageWithPage:self.page pageSize:10 withSuccess:^(id  _Nullable response) {
        [self stopRefresh];
        
        NSLog(@"official==%@",response);
        [self stopRefresh];
        if (self.page == 1) {
            self.page ++;
            [self.dataArray removeAllObjects];
        }
        
        NSArray *array = response[@"data"][@"list"];
        for (int i = 0; i < array.count; i ++) {
            MessageOfficialModel *model = [MessageOfficialModel mj_objectWithKeyValues:array[i]];
            [self.dataArray addObject:model];
        }
        
        [self.mainTableView reloadData];
        self.lastPage = [response[@"data"][@"lastPage"] intValue];
    } withFail:^(NSError * _Nullable error) {
        [self stopRefresh];
    }];
  
}

- (void)stopRefresh{
   [self.mainTableView.mj_header endRefreshing];
   [self.mainTableView.mj_footer endRefreshing];
   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
