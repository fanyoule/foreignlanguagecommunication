//
//  MessageOfficialModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/2/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageOfficialModel : NSObject
//{"id":3,"topic":"一个左正蹬","imageUrl":"http://xz.yueyuecms.com/8x1g8m3e929vqve3ap0k.jpg","introduction":"一个右鞭腿","content":"https://baijiahao.baidu.com/s?id=1683791533627448322&wfr=spider&for=pc","createTime":"2020-12-04 10:54:29"}


@property (nonatomic) NSInteger ID;
/**名字 话题*/
@property (nonatomic, copy) NSString *topic;
/**头像*/
@property (nonatomic, copy) NSString *imageUrl;
/**简介*/
@property (nonatomic, copy) NSString *introduction;
/**网址*/
@property (nonatomic, copy) NSString *content;
/**创建时间*/
@property (nonatomic, copy) NSString *createTime;
@end

NS_ASSUME_NONNULL_END
