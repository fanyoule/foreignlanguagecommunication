//
//  OfficialTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import "OfficialTableViewCell.h"
@interface OfficialTableViewCell ()
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 标题*/
@property (nonatomic, strong)UILabel *titlelabel;
/** 时间*/
@property (nonatomic, strong)UILabel *timelabel;

/** 线*/
@property (nonatomic, strong)UIView *lineView;
@end
@implementation OfficialTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.titlelabel];
    [self.contentView addSubview:self.timelabel];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.lineView];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(20);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@44);
    }];
    [self.titlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV).offset(5);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
    }];
    [self.timelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titlelabel);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titlelabel.mas_bottom).offset(8);
        make.right.equalTo(self.contentView).offset(-20);
        make.left.equalTo(self.portraitImageV.mas_right).offset(10);
        make.bottom.equalTo(self.contentView).offset(-20).priority(600);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.height.equalTo(@0.5);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
    
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"官方"];
    }
    return _portraitImageV;
}

- (UILabel *)titlelabel{
    if (!_titlelabel) {
        _titlelabel = [[UILabel alloc]init];
        _titlelabel.text = @"官方";
        _titlelabel.textColor = RGBA(24, 24, 24, 1);
        _titlelabel.font = kFont_Medium(15);
        _titlelabel.textAlignment = 0;
    }
    return _titlelabel;
}

- (UILabel *)timelabel{
    if (!_timelabel) {
        _timelabel = [[UILabel alloc]init];
        _timelabel.text = @"10分钟前";
        _timelabel.textColor = RGBA(153, 153, 153, 1);
        _timelabel.font = kFont_Medium(11);
        _timelabel.textAlignment = 2;
    }
    return _timelabel;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"Hi，欢迎进入abc课程。Hi，欢迎进入abc课程。Hi，欢迎进入abc课程。";
        _contentLabel.textColor = RGBA(153, 153, 153, 1);
        _contentLabel.font = kFont_Medium(14);
        _contentLabel.textAlignment = 0;
        _contentLabel.numberOfLines = 0;
    }
    return _contentLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
