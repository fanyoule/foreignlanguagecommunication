//
//  OfficialTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OfficialTableViewCell : UITableViewCell
/** 内容*/
@property (nonatomic, strong)UILabel *contentLabel;
@end

NS_ASSUME_NONNULL_END
