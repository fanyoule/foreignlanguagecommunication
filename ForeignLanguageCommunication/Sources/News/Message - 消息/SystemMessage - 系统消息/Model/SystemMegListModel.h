//
//  SystemMegListModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SystemMegListModel : NSObject

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *name;
@property (nonatomic) NSInteger notReadNum;
@property (nonatomic, copy) NSString *updateTime;

@end

NS_ASSUME_NONNULL_END
