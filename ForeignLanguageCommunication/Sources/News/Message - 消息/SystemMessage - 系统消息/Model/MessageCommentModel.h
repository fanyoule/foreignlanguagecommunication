//
//  MessageCommentModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/2/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageCommentModel : NSObject
/**
 
 createTime = "2021-01-20";
 headUrl = "http://pic.sxsyingyu.com/4bhsetsvkv8ykycp0s80.jpeg";
 message = "\U79cb\U4e91\U56de\U590d\U4e86\U4f60";
 publishId = 53;
 userId = 35;
 userName = "\U79cb\U4e91";
 */

@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *headUrl;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic) NSInteger userId;
@property (nonatomic) NSInteger publishId;
@end

NS_ASSUME_NONNULL_END
