//
//  GroupHelpViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/2.
//

#import "GroupHelpViewController.h"
#import "GroupHelpTableViewCell.h"

@interface GroupHelpViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic) int page;
@property (nonatomic) int lastPage;

@end

@implementation GroupHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArray = [NSMutableArray array];
    self.navTitleString = @"群组消息助手";
    
    [self creatUI];
    
    
    // Do any additional setup after loading the view.
    [self getData];
}
-(void)creatUI{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGBA(238, 238, 238, 1);
    [self.navView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@0.5);
        make.left.right.equalTo(self.navView);
        make.bottom.equalTo(self.navView);
    }];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.backgroundColor = kBackgroundColor;
    [self.view addSubview:self.mainTableView];
    [self.mainTableView registerNib:[UINib nibWithNibName:@"GroupHelpTableViewCell" bundle:nil] forCellReuseIdentifier:@"GroupHelpTableViewCell"];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin);
    }];
    kWeakSelf(self)
    self.mainTableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        weakself.page = 1;
        [weakself getData];
    }];
    self.mainTableView.mj_footer = [MJRefreshAutoGifFooter footerWithRefreshingBlock:^{
        [weakself getData];
    }];
    self.mainTableView.mj_footer.hidden = YES;
    
    
    [self.mainTableView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.mainTableView);
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupHelpModel *model = self.dataArray[indexPath.row];
    if (model.msgTwoType == 0) {
        return 160;
    }
    return 200;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupHelpModel *model = self.dataArray[indexPath.row];
    GroupHelpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupHelpTableViewCell"];
    cell.model = model;
    cell.index = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    kWeakSelf(self)
    cell.agreeBlock = ^(NSInteger index, long type) {
        [weakself agreeAction:index type:type];
    };
    return cell;
}
- (void)agreeAction:(NSInteger)index type:(long)type {
    GroupHelpModel *model = self.dataArray[index];
    kWeakSelf(self)
    [RequestManager dealClusterAccessId:model.Id status:(int)type withSuccess:^(id  _Nullable response) {
        weakself.page = 1;
        [weakself getData];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (NSMutableArray *)dataArray{
   if (!_dataArray) {
       _dataArray = [NSMutableArray array];
   }
   return _dataArray;
}

- (void)getData {
    kWeakSelf(self)
    [RequestManager clusterWithUserId:[UserInfoManager shared].getUserID pageNum:self.page pageSize:20 withSuccess:^(id  _Nullable response) {
        int lastPage = [response[@"data"][@"lastPage"] intValue];
        NSArray *tempArr = [GroupHelpModel mj_objectArrayWithKeyValuesArray:response[@"data"][@"list"]];
        if (weakself.page == 1) {
            [weakself.dataArray removeAllObjects];
        }
        [weakself stopLoading:!(weakself.page < lastPage)];
        weakself.page++;
        [weakself.dataArray addObjectsFromArray:tempArr];
        if (weakself.dataArray.count > 0) {
            weakself.mainTableView.mj_footer.hidden = NO;
        }
        weakself.nullView.hidden = weakself.dataArray.count;
        [weakself.mainTableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself stopLoading:NO];
    }];
}
- (void)stopLoading:(BOOL)isNoData {
    if ([self.mainTableView.mj_header isRefreshing]) {
        [self.mainTableView.mj_header endRefreshing];
        if (!isNoData) {
            self.mainTableView.mj_footer.state = MJRefreshStateIdle;
        }
    }
    if ([self.mainTableView.mj_footer isRefreshing]) {
        if (isNoData) {
            [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.mainTableView.mj_footer endRefreshing];
        }
    }
}
@end
