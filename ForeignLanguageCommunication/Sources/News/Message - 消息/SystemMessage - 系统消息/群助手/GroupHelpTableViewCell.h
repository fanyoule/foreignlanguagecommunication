//
//  GroupHelpTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/2.
//

#import <UIKit/UIKit.h>
#import "GroupHelpModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^GroupHelpTableViewCellBlock)(NSInteger, long);
@interface GroupHelpTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *acountLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *yzLab;
@property (weak, nonatomic) IBOutlet UILabel *stateLab;
@property (weak, nonatomic) IBOutlet UIButton *disAgreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) GroupHelpModel *model;

@property (nonatomic, copy) GroupHelpTableViewCellBlock agreeBlock;
@end

NS_ASSUME_NONNULL_END
