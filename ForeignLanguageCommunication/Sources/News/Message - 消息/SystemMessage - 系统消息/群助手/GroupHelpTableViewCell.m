//
//  GroupHelpTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/2.
//

#import "GroupHelpTableViewCell.h"

@implementation GroupHelpTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = UIColor.clearColor;
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    
    self.disAgreeBtn.backgroundColor = UIColor.grayColor;
    self.disAgreeBtn.layer.cornerRadius = 3;
    self.disAgreeBtn.layer.masksToBounds = YES;
    
    self.agreeBtn.backgroundColor = kTHEMECOLOR;
    self.agreeBtn.layer.cornerRadius = 3;
    self.agreeBtn.layer.masksToBounds = YES;
    
    self.stateLab.textColor = kTHEMECOLOR;
}
- (IBAction)disAgreeBtnclick:(UIButton *)sender {
    if (self.agreeBlock) {
        self.agreeBlock(self.index, 1);
    }
}
- (IBAction)agreeBtnClick:(UIButton *)sender {
    if (self.agreeBlock) {
        self.agreeBlock(self.index, 0);
    }
}
- (void)setModel:(GroupHelpModel *)model {
    _model = model;
    
    self.timeLab.text = model.createTime;
    if (model.msgType == 1) {
        self.titleLab.text = @"退群通知";
        self.contentLab.text = @"退出了群组";
    } else if (model.msgType == 2) {
        self.titleLab.text = @"任命通知";
        self.contentLab.text = @"被设为管理员";
    } else {
        self.titleLab.text = @"入群申请";
        self.contentLab.text = @"申请加入群组";
    }
    self.acountLab.text = [NSString stringWithFormat:@"%@(ID:%@)", model.userName, model.userSysid];
    self.yzLab.text = model.message;
    
    if (model.result == 0) {
        self.stateLab.hidden = YES;
        self.disAgreeBtn.hidden = NO;
        self.agreeBtn.hidden = NO;
    } else if (model.result == 1) {
        self.stateLab.hidden = NO;
        self.disAgreeBtn.hidden = YES;
        self.agreeBtn.hidden = YES;
        self.stateLab.text = @"已同意";
    } else if (model.result == 2) {
        self.stateLab.hidden = NO;
        self.disAgreeBtn.hidden = YES;
        self.agreeBtn.hidden = YES;
        self.stateLab.text = @"已拒绝";
    } else {
        self.stateLab.hidden = YES;
        self.disAgreeBtn.hidden = YES;
        self.agreeBtn.hidden = YES;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
