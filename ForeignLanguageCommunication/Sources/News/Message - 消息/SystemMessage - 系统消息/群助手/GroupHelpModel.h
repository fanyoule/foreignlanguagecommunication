//
//  GroupHelpModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupHelpModel : NSObject
@property (nonatomic, copy) NSString *createTime;       // 创建时间
@property (nonatomic, assign) long handlerId;           // 处理者id
@property (nonatomic, assign) long Id;                  // 该消息id
@property (nonatomic, copy) NSString *handlerName;      // 处理者昵称
@property (nonatomic, copy) NSString *handlerSysid;     // 处理者账号
@property (nonatomic, copy) NSString *message;          // 处理消息
@property (nonatomic, assign) long msgTwoType;          // 消息的两种类型：0通知 1申请 申请有处理结果
@property (nonatomic, assign) long msgType;             // 消息类型（1退群通知，2任命通知，3入群申请）
@property (nonatomic, copy) NSString *nickname;         // 用户昵称
@property (nonatomic, assign) long orgId;               // 公会id
@property (nonatomic, assign) long result;              // 处理结果(0待处理，1同意/通过，2拒绝/不通过，通知为null)
@property (nonatomic, assign) long userId;              // 用户id
@property (nonatomic, copy) NSString *userName;         // 用户昵称
@property (nonatomic, copy) NSString *userSysid;        // 用户账号


@end

NS_ASSUME_NONNULL_END
