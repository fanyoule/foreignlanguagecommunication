//
//  TheMessageViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//  消息

#import "TheMessageViewController.h"
#import <JXCategoryView/JXCategoryView.h>
#import "MessageChatViewController.h"//聊天
#import "SystemMessageViewController.h"//系统消息


@interface TheMessageViewController ()<UIScrollViewDelegate,JXCategoryViewDelegate,UIGestureRecognizerDelegate>
@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;
@property (nonatomic, strong) UIView *contentView;
/**聊天 系统消息  切换*/
@property (nonatomic, strong) UIView *switchView;

@property (nonatomic, strong) MessageChatViewController *findTeamVC;
@property (nonatomic, strong) SystemMessageViewController *funVC;

@end

@implementation TheMessageViewController
- (instancetype)init {
    if (self = [super init]) {
        [self setUpNavView];
        [self setUpSubView];
        [self.view bringSubviewToFront:self.navView];
        [self.view bringSubviewToFront:self.categoryTitleView];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
//    [self setUpNavView];
//    [self setUpSubView];
//    [self.view bringSubviewToFront:self.categoryTitleView];
}

- (void)setUpNavView {
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"消息";
    self.navBottomLine.hidden = YES;
   
    [self.view addSubview:self.categoryTitleView];
//    self.categoryTitleView.frame = CGRectMake(0, self.navView.size.height, KSW, 27);
    self.categoryTitleView.frame = CGRectMake(0, SNavBarHeight, KSW, 45);
    
    
    UIView *lineView1 = [[UIView alloc] init];
    lineView1.backgroundColor = RGBA(204, 204, 204, 1);
    [self.categoryTitleView addSubview:lineView1];
    
    UIView *lineView2 = [[UIView alloc]init];
    lineView2.backgroundColor = RGBA(204, 204, 204, 1);
    [self.categoryTitleView addSubview:lineView2];
    
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.categoryTitleView);
        make.height.equalTo(@15);
        make.width.equalTo(@1);
    }];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.categoryTitleView);
        make.height.equalTo(@0.5);
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setBadgeNum) name:@"setBadgeNum" object:nil];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)setBadgeNum {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChatTotalUnread" object:@{@"badge": @(_findTeamVC.unReadNum + _funVC.unReadNum)}];
}
- (void)setUpSubView {
    //聊天 系统消息
    [self.view addSubview:self.mainScrollView];
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.bounces = NO;
    
    [self.mainScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(0);
        make.left.right.mas_offset(0);
        make.bottom.mas_offset(0);
    }];
    [self.mainScrollView addSubview:self.contentView];
    self.mainScrollView.scrollEnabled = NO;
    self.mainScrollView.contentSize = CGSizeMake(KSW*2, 0);
    self.mainScrollView.delegate = self;
//    self.contentView.frame = CGRectMake(0, 0, KSW*2, ScreenHeight -TabbarHeight);
    self.contentView.frame = CGRectMake(0, SNavBarHeight+50, KSW, ScreenHeight-SNavBarHeight-TabbarHeight-50);
    
    _findTeamVC = [[MessageChatViewController alloc] init];
    [self addChildViewController:_findTeamVC];
    _funVC = [[SystemMessageViewController alloc] init];
    [self addChildViewController:_funVC];
    [self.contentView addSubview:_findTeamVC.view];
    _findTeamVC.view.frame = CGRectMake(0, 0, kScreenWidth, self.contentView.height);
    [_findTeamVC loadDataSource];
    [self.contentView addSubview:_funVC.view];
    _funVC.view.frame = CGRectMake(kScreenWidth, 0, kScreenWidth, self.contentView.height);
    
    [self scrollViewDidEndScrollingAnimation:self.mainScrollView];
}

#pragma mark scrollview delegate
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//    NSInteger index = scrollView.contentOffset.x / KSW;
//    UIViewController *newVC = self.childViewControllers[index];
//    if (newVC.isViewLoaded) {
//        return;
//    }
//    [self.contentView addSubview:newVC.view];
//    newVC.view.frame = CGRectMake(KSW * index, 0, KSW, self.contentView.height);
//    [newVC didMoveToParentViewController:self];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger index = scrollView.contentOffset.x / KSW;
    [self.categoryTitleView selectItemAtIndex:index];
    [self scrollViewDidEndScrollingAnimation:scrollView];
}

//MARK: categorytitle delegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.mainScrollView.contentOffset = CGPointMake(KSW * index, 0);
    [self scrollViewDidEndScrollingAnimation:self.mainScrollView];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
//MARK: Getter
- (JXCategoryTitleView *)categoryTitleView {
//    if (!_categoryTitleView) {
//        _categoryTitleView = [[JXCategoryTitleView alloc] init];
//        _categoryTitleView.titles = @[@"聊天",@"系统消息"];
//        _categoryTitleView.titleColor = CustomColor(@"#222222");
//        _categoryTitleView.titleSelectedColor = CustomColor(@"#3478F5");
//        _categoryTitleView.titleFont = kFont_Regular(13);
//        _categoryTitleView.titleSelectedFont = kFont_Regular(13);
//        _categoryTitleView.cellSpacing = 0;
//        _categoryTitleView.cellWidth = KSW / 2;
//        _categoryTitleView.collectionView.scrollEnabled = NO;
//        _categoryTitleView.backgroundColor = UIColor.clearColor;
//        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
//        lineView.indicatorColor = CustomColor(@"#3478F5");
//        lineView.indicatorWidth = 20;
//        lineView.indicatorHeight = 3;
//        lineView.indicatorCornerRadius = 1.5;
//        _categoryTitleView.indicators = @[lineView];
//        _categoryTitleView.delegate = self;
//    }
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titles = @[@"消息",@"系统消息"];
        _categoryTitleView.titleColor = RGBA(102, 102, 102, 1);
        _categoryTitleView.titleSelectedColor = CustomColor(@"#3478F5");
        _categoryTitleView.titleFont = kFont(14);
        _categoryTitleView.titleSelectedFont = kFont_Bold(14);
        _categoryTitleView.cellSpacing = 30;
        _categoryTitleView.cellWidth = 35;
        
        _categoryTitleView.collectionView.scrollEnabled = NO;
        _categoryTitleView.backgroundColor = UIColor.clearColor;
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorColor = CustomColor(@"#3478F5");
        lineView.indicatorWidth = 20;
        lineView.indicatorHeight = 3;
        lineView.indicatorCornerRadius = 1.5;
        _categoryTitleView.indicators = @[lineView];
        _categoryTitleView.delegate = self;
    }
    return _categoryTitleView;
}
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = UIView.new;
        _contentView.backgroundColor = UIColor.clearColor;
    }
    return _contentView;
}

- (UIView *)switchView{
    UIView *sView = [UIView new];
    
    
    return sView;
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




@end
