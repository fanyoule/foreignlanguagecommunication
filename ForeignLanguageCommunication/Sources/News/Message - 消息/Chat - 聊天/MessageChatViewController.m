//
//  MessageChatViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//  收到消息 先判断消息

#import "MessageChatViewController.h"
#import "ChatMessageTableViewCell.h"
#import "ChatInformationPersonTableViewCell.h"
#import "FriendViewController.h"
#import "WLGifFooter.h"
#import "MjWLHeader.h"
#import "IMSessionListModel.h"
#import "InteractiveMessageModel.h"
#import "InteractIveCell.h"//会话列表cell

#import "NewsAllBuddyListModel.h"
#import "ChatDetailsVC.h"
#import "NewsAllGroupListModel.h"
#import "ChatDetailsGroupVC.h"

#import "ChatGroupProfileViewController.h"
#import "NewsAllGroupListVC.h"

@interface MessageChatViewController ()<UITableViewDelegate,UITableViewDataSource> 
@property (nonatomic,strong) NSMutableArray *dataArray;

@property (nonatomic, strong) WLGifFooter *refreshFooter;
@property (nonatomic, strong) MjWLHeader *refreshHeader;
/**当前需要加载的页数*/
@property (nonatomic) NSInteger page;


@property (nonatomic, strong) NSMutableArray *modelArray;
@end

@implementation MessageChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor whiteColor];
//    self.unReadNum = 0;
//    self.mainTableView.delegate = self;
//    self.mainTableView.dataSource = self;
//
//    [self.view addSubview:self.mainTableView];
//    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_offset(0);
//        make.left.right.bottom.equalTo(self.view);
//    }];
//    self.mainTableView.mj_header = self.refreshHeader;
//    self.mainTableView.mj_footer = self.refreshFooter;
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(text:) name:@"noti" object:nil];
}
- (void)loadDataSource {
    self.view.backgroundColor = [UIColor whiteColor];
    self.unReadNum = 0;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
    self.mainTableView.mj_header = self.refreshHeader;
    self.mainTableView.mj_footer = self.refreshFooter;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(text:) name:@"noti" object:nil];
    [self requestInitialData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestInitialData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"noti" object:nil];
}
- (UIView *)listView{
    
    [Util setCornerModelWithView:self.view model:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadius:6.0];
    return self.view;
}
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }
    return self.modelArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
    ChatMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatMessageTableViewCell"];
        if (!cell) {
            cell = [[ChatMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatMessageTableViewCell"];
        }
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                cell.nameLabel.text = @"我的群组";
                cell.pictureImageV.image = [UIImage imageNamed:@"消息-群组"];
            }else{
                cell.nameLabel.text = @"我的好友";
                cell.pictureImageV.image = [UIImage imageNamed:@"消息-好友"];
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    // 会话列表cell
    InteractiveMessageModel *model = [self.modelArray objectAtIndex:indexPath.row];
    InteractIveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InteractIveCell"];
       
    if (!cell) {
       cell = [[InteractIveCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"InteractIveCell"];
    }
    cell.model = model;
    NSString *isTop;
    if (model.top == 1) {
       isTop = @"取消置顶";
    }else{
       isTop = @"设置置顶";
    }
    MGSwipeButton *deleteButton=[MGSwipeButton buttonWithTitle:isTop backgroundColor:ColorRGBA(200, 198, 204, 1)];
    deleteButton.callback = ^BOOL(MGSwipeTableCell * _Nonnull cell) {
        if ([isTop isEqualToString:@"取消置顶"]) {
            NSMutableDictionary *IMDic = [[NSMutableDictionary alloc]init];
            IMDic[@"roomId"] = model.sessionId;
            IMDic[@"status"] = @(0);
            [self updateSessionRoomIsTop:IMDic];
        }else{
            NSMutableDictionary *IMDic = [[NSMutableDictionary alloc]init];
            IMDic[@"roomId"] = model.sessionId;
            IMDic[@"status"] = @(1);
            [self updateSessionRoomIsTop:IMDic];
        }
        return YES;
    };
    
    deleteButton.layer.cornerRadius = 0;
    deleteButton.buttonWidth = 70;
    deleteButton.titleLabel.font =  [UIFont systemFontOfSize: 12.0];
    
    MGSwipeButton *remindButton=[MGSwipeButton buttonWithTitle:@"删除" backgroundColor:[UIColor colorWithRed:255/255.0 green:53/255.0 blue:82/255.0 alpha:1.0]];
    remindButton.buttonWidth = 70;
    remindButton.layer.cornerRadius = 0;
     remindButton.titleLabel.font =  [UIFont systemFontOfSize: 12.0];
    remindButton.callback = ^BOOL(MGSwipeTableCell * _Nonnull cell) {
        NSMutableDictionary *deleDic = [[NSMutableDictionary alloc]init];
        deleDic[@"roomId"] = model.sessionId;
        deleDic[@"type"] = @(model.chatType);
        deleDic[@"userId"] = @(model.userId);
        [self delMessageByMsgId:deleDic didSelectRowAtIndexPath:indexPath];
      // [self setRemind:model indePath:indexPath];
        return YES;
    };
   
    cell.rightButtons = @[remindButton,deleteButton];
    cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    cell.rightSwipeSettings.bottomMargin =0;    //距离底部
    cell.rightSwipeSettings.topMargin = 0;      //button距离t顶部
    cell.rightSwipeSettings.offset = 0;         //最右侧button距离边框的距离
    cell.rightSwipeSettings.buttonsDistance = 0;//button的间距
//  cell.model = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;//取消点击cell变色
    return cell;
}
/** tableView 点击事件*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (![UserInfoManager shared].isLogin) {
            [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
                if (index == 1) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
                }
            }];
            return;
        }
        if (indexPath.row == 0) {
            //我的群组
            NewsAllGroupListVC *vc = [[NewsAllGroupListVC alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
            
        } else {
            //我的好友
            FriendViewController *vc = [[FriendViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else{
        
        InteractiveMessageModel *messModel = self.modelArray[indexPath.row];
        NewsAllBuddyListModel *lisModel = [[NewsAllBuddyListModel alloc]init];
        lisModel.ID = messModel.userId;
        lisModel.nickname = messModel.nickName;
        NewsAllGroupListModel *groupModel = [[NewsAllGroupListModel alloc]init];
        groupModel.ID = messModel.userId;
        groupModel.type = messModel.chatType;
        groupModel.name = messModel.nickName;
        if (messModel.chatType == 0) {
            ChatDetailsVC *vc = [[ChatDetailsVC alloc]init];
            vc.buddyModel = lisModel;
            [self.navigationController pushViewController:vc animated:NO];
        }else{
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            dic[@"id"] = @(messModel.userId);
//            dic[@"clusterId"] = @(messModel.userId);
//            dic[@"clusterType"] = @(messModel.chatType);
            dic[@"userId"] = @([UserInfoManager shared].getUserID); // @"/api/personalCluster/IMClusterInfo"
            [RequestManager getMyDataWithParameters:dic UrlString:@"/api/personalCluster/IMClusterInfo" Success:^(id  _Nullable response) {
                NSLog(@"respnpe===%@",response);
                NSMutableDictionary *dataDic = response[@"data"];
                 ChatDetailsUserPoslitionModel *poslitonModel = [ChatDetailsUserPoslitionModel mj_objectWithKeyValues:dataDic];
                 ChatDetailsGroupVC *vc = [[ChatDetailsGroupVC alloc]init];
                vc.buddyModel = groupModel;
                
                vc.politionModel = poslitonModel;
                [self.navigationController pushViewController:vc animated:NO];
            } withFail:^(NSError * _Nullable error) {
                NSLog(@"获取群信息失败");
            }];
        }
    }
}
#pragma mark - 接受消息的方法
- (void)text:(NSNotification *)noti {
    ///noti中的key,就可以取出值
    //  NSLog(@"chatNotion");
    NSString * name =  noti.userInfo[@"key"];
    NSLog(@"name===%@",name);
    NSDictionary *ImDic = [self dictionaryWithJsonString:name];
    if ([self isBlankString:ImDic[@"data"][@"from"]]) {
        return;
    }
    self.page = 0;
//    [self.modelArray removeAllObjects];
    [self requestChatData];
}
- (BOOL)isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}
//json格式字符串转字典：
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
- (NSMutableArray *)modelArray {
    if (!_modelArray) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}
- (void)refreshData {
    [self requestInitialData];
}
- (void)requestInitialData {
    if (![UserInfoManager shared].isLogin) {
        self.page = 0;
        [self.modelArray removeAllObjects];
        self.unReadNum = 0;
        [self stopLoading];
        [self.mainTableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"setBadgeNum" object:nil];
    } else {
        self.page = 0;
        [self requestChatData];
    }
}
- (void)stopLoading {
    if (self.mainTableView.mj_header.isRefreshing) {
        [self.mainTableView.mj_header endRefreshing];
    }
    if (self.mainTableView.mj_footer.isRefreshing) {
        [self.mainTableView.mj_footer endRefreshing];
    }
}
/**请求私聊 群组 消息列表*/
- (void)requestChatData{
    NSInteger userid = [[UserInfoManager shared] getUserID];
   NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
   dic[@"userId"] = @(userid);
   dic[@"startPageIndex"] = @(self.page);
   dic[@"length"] = @(30);
    
    [RequestManager getIMDataWithParameters:dic IMHost:IMPostHost UrlString:@"/api/messageHandler/getAllSessionHistoryByUserId" Success:^(id  _Nullable response) {
        [self stopLoading];
//        [self.mainTableView.mj_footer endRefreshing];
//        [self.mainTableView.mj_header endRefreshing];
        if ([response[@"status"] intValue] == 200) {
            NSArray *array = response[@"data"];
            if (self.page == 0) {
                [self.modelArray removeAllObjects];
            }
            if (array.count > 0) {
                long num = 0;
                for (NSDictionary *dic in array) {
                    NSLog(@"%@",dic);
                    InteractiveMessageModel *messageModel = [InteractiveMessageModel mj_objectWithKeyValues:dic];
                    [self.modelArray addObject:messageModel];
                    num = num + messageModel.unReadNum;
                    SetCache(messageModel.top == 1, ISTopKey(messageModel.userId));
                }
                [self.mainTableView reloadData];
                self.unReadNum = num;
            } else {
                self.unReadNum = 0;
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"setBadgeNum" object:nil];
        }
    } withFail:^(NSError * _Nullable error) {
        [self.mainTableView.mj_footer endRefreshing];
        [self.mainTableView.mj_header endRefreshing];
    }];
}

- (MjWLHeader *)refreshHeader{
    if (!_refreshHeader) {
        _refreshHeader = [MjWLHeader headerWithRefreshingBlock:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestInitialData];
                
            });
        }];
    }
    return _refreshHeader;
}
- (WLGifFooter *)refreshFooter{
    if (!_refreshFooter) {
        _refreshFooter = [WLGifFooter footerWithRefreshingBlock:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.page ++;
                [self requestChatData];
            });
        }];
    }
    return _refreshFooter;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//MARK: --------IM __________

#pragma mark - 设置置顶0。取消1.置顶;
-(void)updateSessionRoomIsTop:(NSMutableDictionary *)dic{
    [RequestManager postIMDataWithParameters:dic IMHost:IMPostHost UrlString:@"/api/messageHandler/updateSessionRoomIsTop" Success:^(id  _Nullable response) {
        if ([response[@"status"]intValue]==200) {
            self.page = 0;
            [self.modelArray removeAllObjects];
            [self requestInitialData];
        }
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"err===%@",error);
    }];
}

#pragma mark - 删除消息
-(void)delMessageByMsgId:(NSMutableDictionary *)dic didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserInfoModel *userM = [[UserInfoManager shared]getUserDetailInfo];
    //dic[@"userId"] = @(model.user.ID);
    NSLog(@"dic===%@",dic);
    
    
    [RequestManager postIMDataWithParameters:dic IMHost:IMPostHost UrlString:@"/api/messageHandler/delSessionById" Success:^(id  _Nullable response) {
        if ([response[@"status"]intValue]==200) {
            NSLog(@"modelArray===%lu",(unsigned long)self.modelArray.count);
            NSLog(@"modelArray===%ld",(long)indexPath.row);
            [self.modelArray removeObjectAtIndex:indexPath.row];
            [self.mainTableView deleteRowsAtIndexPaths:@[indexPath]withRowAnimation:UITableViewRowAnimationFade];
            [self.mainTableView reloadData];
        }
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"err===%@",error);
        [self requestInitialData];
    }];

}
@end
