//
//  ChatMessageTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatMessageTableViewCell : UITableViewCell
/**图片*/
@property (nonatomic, strong)UIImageView *pictureImageV;
/**名字*/
@property (nonatomic, strong)UILabel *nameLabel;
@end

NS_ASSUME_NONNULL_END
