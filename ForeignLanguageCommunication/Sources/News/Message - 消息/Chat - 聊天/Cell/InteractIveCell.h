//
//  InteractIveCell.h
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <MGSwipeTableCell/MGSwipeTableCell.h>
#import "InteractiveMessageModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface InteractIveCell : MGSwipeTableCell
/** model */
@property (nonatomic, strong) InteractiveMessageModel *model;

@end

NS_ASSUME_NONNULL_END
