//
//  ChatMessageTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "ChatMessageTableViewCell.h"



@interface ChatMessageTableViewCell ()

/** 线*/
@property (nonatomic, strong)UIView *lineView;

@end

@implementation ChatMessageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.pictureImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.lineView];
    
    
    
    [self.pictureImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.pictureImageV.mas_right).offset(10.5);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.pictureImageV.mas_left);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
        make.top.equalTo(self.contentView).offset(65);
        make.bottom.equalTo(self.contentView).offset(-0.5).priority(600);
    }];
    
}


- (UIImageView *)pictureImageV{
    if (!_pictureImageV) {
        _pictureImageV = [[UIImageView alloc]init];
        _pictureImageV.image = [UIImage imageNamed:@"消息-群组"];
    }
    return _pictureImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"我的群组";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Bold(15);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
