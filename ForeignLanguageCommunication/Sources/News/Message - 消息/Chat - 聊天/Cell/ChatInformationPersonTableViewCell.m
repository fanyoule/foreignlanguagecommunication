//
//  ChatInformationPersonTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import "ChatInformationPersonTableViewCell.h"
#import "NCGradientLabel.h"
#import "UIColor+Extend.h"
#import "UIView+HJViewStyle.h"


@interface ChatInformationPersonTableViewCell ()

/**消息*/
@property (nonatomic, strong)UILabel *messageLabel;
/**时间*/
@property (nonatomic, strong)UILabel *timeLabel;

/** 线*/
@property (nonatomic, strong)UIView *lineView;

@property (nonatomic, strong) UIView *badgeView;
@property (nonatomic, strong) UILabel *badgeLabel;



@end

@implementation ChatInformationPersonTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.pictureImageV];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.messageLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.numberLabel];
    [self.contentView addSubview:self.lineView];
    
    [self.contentView addSubview:self.badgeView];
    [self.badgeView addSubview:self.badgeLabel];
    
    [self.badgeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.pictureImageV.mas_top).offset(-5);
        make.right.mas_equalTo(self.pictureImageV.mas_right).offset(3.5);
        make.height.mas_offset(14);
        make.width.mas_offset(14);
    }];
    
    [self.badgeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.badgeView);
    }];
    
    [self.pictureImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(15);
        make.width.height.equalTo(@44);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.pictureImageV).offset(3);
        make.left.equalTo(self.pictureImageV.mas_right).offset(10.5);
    }];
    [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.pictureImageV).offset(-3);
        make.left.equalTo(self.pictureImageV.mas_right).offset(10.5);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.centerY.equalTo(self.messageLabel);
        make.width.height.equalTo(@15);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.pictureImageV.mas_left);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@0.5);
        make.top.equalTo(self.contentView).offset(65);
        make.bottom.equalTo(self.contentView).offset(-0.5).priority(600);
    }];
}

- (UIImageView *)pictureImageV{
    if (!_pictureImageV) {
        _pictureImageV = [[UIImageView alloc]init];
        _pictureImageV.image = [UIImage imageNamed:@"测试头像"];
        _pictureImageV.layer.cornerRadius = 6;
        _pictureImageV.layer.masksToBounds = YES;
    }
    return _pictureImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"我的群组";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Bold(15);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UILabel *)messageLabel{
    if (!_messageLabel) {
        _messageLabel = [[UILabel alloc]init];
        _messageLabel.text = @"看我的消息";
        _messageLabel.textColor = RGBA(153, 153, 153, 1);
        _messageLabel.font = kFont_Medium(14);
        _messageLabel.textAlignment = 0;
    }
    return _messageLabel;
}

- (UILabel *)timeLabel{
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.text = @"10分钟前";
        _timeLabel.textColor = RGBA(153, 153, 153, 1);
        _timeLabel.font = kFont(11);
        _timeLabel.textAlignment = 2;
    }
    return _timeLabel;
}

- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.backgroundColor = RGBA(241, 88, 95, 1);
        _numberLabel.text = @"2";
        _numberLabel.textColor = RGBA(254, 254, 254, 1);
        _numberLabel.font = kFont_Regular(10);
        _numberLabel.textAlignment = 1;
        _numberLabel.layer.cornerRadius = 15/2;
        _numberLabel.layer.masksToBounds = YES;
    }
    return _numberLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = RGBA(223, 223, 223, 1);
    }
    return _lineView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setSessionModel:(IMSessionListModel *)sessionModel{
    _sessionModel = sessionModel;
    [self.pictureImageV sd_setImageWithURL:[NSURL URLWithString:sessionModel.avatarUrl]];
    self.nameLabel.text = sessionModel.nickName;
    
}

- (void)setSysModel:(SystemMegListModel *)sysModel {
    _sysModel = sysModel;
    
    self.nameLabel.text = sysModel.name;
    if ([sysModel.name isEqualToString:@"群组"]) {
        self.pictureImageV.image = kImage(@"消息-群组");
    }else{
        self.pictureImageV.image = kImage(sysModel.name);
    }
    self.numberLabel.text = [NSString stringWithFormat:@"%ld",(long)sysModel.notReadNum];
    self.nameLabel.text = sysModel.name;
    if (sysModel.content) {
        self.messageLabel.text = sysModel.content;
        self.timeLabel.text = sysModel.updateTime;
        self.messageLabel.hidden = NO;
        self.timeLabel.hidden = NO;
        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.pictureImageV).offset(3);
            make.left.equalTo(self.pictureImageV.mas_right).offset(10.5);
        }];
    } else {
        self.messageLabel.hidden = YES;
        self.timeLabel.hidden = YES;
        [self.nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.pictureImageV.mas_centerY);
            make.left.equalTo(self.pictureImageV.mas_right).offset(10.5);
        }];
    }
    
    if (sysModel.notReadNum <= 0) {
        self.badgeView.hidden = YES;
    }else if (sysModel.notReadNum < 10) {
        self.badgeLabel.text = [NSString stringWithFormat:@"%ld",sysModel.notReadNum];
        [self.badgeView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(14);
        }];
        self.badgeView.hidden = NO;
    }else{
        self.badgeLabel.text = [NSString stringWithFormat:@"%ld",(long)sysModel.notReadNum];
        
        [self.badgeLabel sizeToFit];
        CGSize size = self.badgeLabel.size;
        size.width += 4;
        if (size.width<14) {
            size.width = 14;
        }
        [self.badgeView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(size.width);
        }];
        self.badgeView.hidden = NO;
    }
}

- (UIView *)badgeView{
    if (!_badgeView) {
        _badgeView = [UIView new];
        _badgeView.gradientStyle = 1;
        _badgeView.gradientAColor = RGBA(255, 165, 165, 1);
        _badgeView.gradientBColor = RGBA(255, 133, 133, 1);
        _badgeView.cornerRadius = 7;
    }
    return _badgeView;
}
- (UILabel *)badgeLabel{
    if (!_badgeLabel) {
        _badgeLabel = [UILabel new];
        _badgeLabel.font  = [UIFont systemFontOfSize:11];
       _badgeLabel.preferredMaxLayoutWidth =200;
       [_badgeLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _badgeLabel.text = @"10";
        _badgeLabel.textColor = [UIColor whiteColor];
        _badgeLabel.textAlignment = NSTextAlignmentCenter;

        _badgeLabel.numberOfLines =0;
    }
    return _badgeLabel;
}

- (void)setCommentModel:(MessageCommentModel *)commentModel {
    if (IS_VALID_STRING(commentModel.headUrl)) {
        [self.pictureImageV sd_setImageWithURL:[NSURL URLWithString:commentModel.headUrl] placeholderImage:kImage(@"测试头像") completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            self.pictureImageV.image = kImage(@"测试头像");
        }];
    }
//    [self.pictureImageV sd_setImageWithURL:[NSURL URLWithString:commentModel.headUrl]];
    self.nameLabel.text = commentModel.userName;
    self.messageLabel.text = commentModel.message;
    self.timeLabel.text = commentModel.createTime;
    self.badgeView.hidden = YES;
}

- (void)setOfficialModel:(MessageOfficialModel *)officialModel{
    _officialModel = officialModel;
//    [self.pictureImageV sd_setImageWithURL:[NSURL URLWithString:officialModel.imageUrl]];
    if (IS_VALID_STRING(officialModel.imageUrl)) {
        [self.pictureImageV sd_setImageWithURL:[NSURL URLWithString:officialModel.imageUrl] placeholderImage:kImage(@"测试头像") completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            self.pictureImageV.image = kImage(@"测试头像");
        }];
    }
    
    self.nameLabel.text = officialModel.topic;
    self.messageLabel.text = officialModel.introduction;
    self.timeLabel.text = officialModel.createTime;
    self.badgeView.hidden = YES;
}

- (void)setWalletModel:(MessageWalletModel *)walletModel{
    self.numberLabel.hidden = YES;
    self.badgeView.hidden =YES;
    _walletModel = walletModel;
    self.nameLabel.text = walletModel.coursesName;
    self.messageLabel.text = walletModel.message;
    self.timeLabel.text = walletModel.createTime;
}

- (void)setCourseModel:(MessageCourseModel *)courseModel{
    self.numberLabel.hidden = YES;
    self.badgeView.hidden =YES;
    _courseModel = courseModel;
    [self.pictureImageV sd_setImageWithURL:[NSURL URLWithString:courseModel.headUrl]];
    self.nameLabel.text = courseModel.userName;
    NSString *preStr = @"";
    
    switch (courseModel.msgType) {
        case 1:
            preStr = @"购买了 ";
            break;
        case 3:{
            preStr = @"申请退课 ";
        }
            break;
        default:
            break;
    }
    self.messageLabel.text = [NSString stringWithFormat:@"%@%@",preStr,courseModel.coursesName];
    self.timeLabel.text = courseModel.createTime;
}
@end
