//
//  InteractIveCell.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/11/18.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "InteractIveCell.h"

#import "NCGradientLabel.h"
#import "UIColor+Extend.h"
#import "UIView+HJViewStyle.h"
@interface InteractIveCell  ()
@property (nonatomic, strong) UIImageView *iimageView;

@property (nonatomic, strong) UILabel *nameLable;
@property (nonatomic, strong)  UILabel*stateLabel;//登录状态
@property (nonatomic, strong) UILabel *signatureLabel;//个性签名
/** 铃铛 */
@property (nonatomic, strong) UIImageView  *bellImageV;
/** 时间 */
@property (nonatomic, strong) UILabel  *timeLabel;
/** 消息条数提示 */
@property (nonatomic, strong) UILabel *messageNumberLabel;
/** 消息的背景view */
@property (nonatomic, strong) UIView *messageBgView;
/** 分割线 */
@property (nonatomic, strong) UILabel *messLineLabel;

@property (nonatomic, strong) UILabel *badgeLabel;
@end

@implementation InteractIveCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.iimageView];
        [self.contentView addSubview:self.nameLable];
        //[self.contentView addSubview:self.stateLabel];
        [self.contentView addSubview:self.signatureLabel];
        [self.contentView addSubview:self.timeLabel];
        [self.contentView addSubview:self.bellImageV];
        [self.contentView addSubview:self.messageBgView];
        [self.messageBgView addSubview:self.messageNumberLabel];
//        [self.contentView addSubview:self.badgeLabel];
        [self p_addMasonry];
    }
    return self;
}

#pragma mark - # Private Methods
- (void)p_addMasonry {
    [self.iimageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(11);
        make.bottom.mas_equalTo(self.contentView.mas_bottom).offset(-10);
        make.left.mas_equalTo(15);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(50);
    }];
    
    [self.nameLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iimageView.mas_right).offset(10);
        make.top.mas_equalTo(self.iimageView.mas_top).offset(0);
    }];
    [self.signatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iimageView.mas_right).offset(10);
        make.bottom.mas_equalTo(self.iimageView.mas_bottom).offset(0);
        make.right.mas_equalTo(self.timeLabel.mas_left).offset(-15);
    }];
    
    //时间
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_offset(-15);
        make.centerY.mas_equalTo(self.nameLable);
        make.width.mas_equalTo(90);
    }];
    
    [self.contentView addSubview:self.messLineLabel];
    
    [self.messLineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.signatureLabel.mas_left);
        make.left.mas_equalTo(self.iimageView);
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
        make.right.mas_offset(-15);
        make.height.mas_offset(0.6);
    }];
    
 
    [self.bellImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(20);
        make.top.mas_equalTo(self.timeLabel.mas_bottom).mas_offset(5);
            make.right.mas_offset(-15);
    }];
    [self.messageBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iimageView.mas_top).offset(-5);
        make.right.mas_equalTo(self.iimageView.mas_right).offset(3.5);
        make.height.mas_offset(14);
        make.width.mas_offset(14);
    }];
    
    [self.messageNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(self.messageBgView);
    }];

    
}
- (void)setModel:(InteractiveMessageModel *)model{
    [self.iimageView sd_setImageWithURL:[NSURL URLWithString:model.avatarUrl]];
    self.nameLable.text = model.nickName;
    self.timeLabel.text = [self getTimeFromTimestamp:model.moreNewMeg.time];
    self.signatureLabel.text = model.moreNewMeg.content;
    if ([model.moreNewMeg.msgType isEqualToString:@"video"]) {
        self.signatureLabel.text = @"[视频]";
    }else if ([model.moreNewMeg.msgType isEqualToString:@"image"]){
        self.signatureLabel.text = @"[图片]";
    }else if ([model.moreNewMeg.msgType isEqualToString:@"audio"]){
        self.signatureLabel.text = @"[语音]";
    }
    
    if (model.unReadNum <= 0) {
        self.messageBgView.hidden = YES;
    }else if (model.unReadNum < 10) {
        self.messageNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)model.unReadNum];
        [self.messageBgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(14);
        }];
        self.messageBgView.hidden = NO;
    }else{
        self.messageNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)model.unReadNum];
        
        [self.messageNumberLabel sizeToFit];
        CGSize size = self.messageNumberLabel.size;
        size.width += 4;
        if (size.width<14) {
            size.width = 14;
        }
        [self.messageBgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(size.width);
        }];
        self.messageBgView.hidden = NO;
    }
    BOOL isNoDist = GetCache(ISNoDisturbingKey(model.userId));
    self.bellImageV.hidden = !isNoDist;
//    if (model.doNotDisturb == 0) {
//        self.bellImageV.hidden = YES;
//    }else self.bellImageV.hidden = NO;
}
#pragma mark ---- 将时间戳转换成时间

- (NSString *)getTimeFromTimestamp:(NSString*)mtime{

 
    //将对象类型的时间转换为NSDate类型
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[mtime doubleValue]/1000];

        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];

        //指定输出的格式  格式:07.06 12:33

        [formatter setDateFormat:@"MM-dd HH:ss"];

        NSString *timeString= [formatter stringFromDate:date];

        return timeString;

}
#pragma mark - # Getter
- (UIImageView *)iimageView {
    if (!_iimageView) {
        _iimageView = [[UIImageView alloc] init];
        _iimageView.backgroundColor = [UIColor grayColor];
        _iimageView.cornerRadius = 6;
        _iimageView.clipsToBounds = YES;
    }
return _iimageView;
}

- (UILabel *)nameLable {
    if (!_nameLable) {
    _nameLable = [[UILabel alloc] init];
        _nameLable.textColor = CustomColor(@"#181818");
        
        //文字大小
    _nameLable.font = [UIFont fontWithName:@"PingFang-SC-Medium" size:15];
    }
    return _nameLable;
}

- (UILabel *)badgeLabel {
    if (!_badgeLabel) {
        _badgeLabel = [UILabel new];
        _badgeLabel.textAlignment = NSTextAlignmentCenter;
        _badgeLabel.font = kFont(11);
        _badgeLabel.backgroundColor = UIColor.redColor;
        
    }
    return _badgeLabel;
}
/** 登录状态 */
- (UILabel *)stateLabel {
    if (!_stateLabel) {
        _stateLabel = [[UILabel alloc]init];
        _stateLabel.textColor = RGBA(154, 154, 154, 1);
        _stateLabel.text = @"[离线]";
        _stateLabel.font = [UIFont systemFontOfSize:14];
    }
    return _stateLabel;
}
/** 个性签名 */
- (UILabel *)signatureLabel {
    if (!_signatureLabel) {
        _signatureLabel = [[UILabel alloc]init];
        _signatureLabel.textColor = CustomColor(@"#999999");
        _signatureLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
        _signatureLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _signatureLabel;
}
/** 铃铛 */
- (UIImageView *)bellImageV {
    if (!_bellImageV) {
        _bellImageV = [[UIImageView alloc]init];
        _bellImageV.contentMode = UIViewContentModeScaleAspectFill;
        _bellImageV.image = kImage(@"消息铃铛");
        _bellImageV.hidden = YES;
    }
    return _bellImageV;
}
- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.textColor = CustomColor(@"#999999");
        _timeLabel.font = [UIFont systemFontOfSize:11];
        _timeLabel.textAlignment = NSTextAlignmentRight;
    }
    return _timeLabel;
}
- (UILabel *)messageNumberLabel {
    if (!_messageNumberLabel) {
        _messageNumberLabel = [[UILabel alloc]init];
        _messageNumberLabel.font  = [UIFont systemFontOfSize:11];
       _messageNumberLabel.preferredMaxLayoutWidth =200;
       [_messageNumberLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        _messageNumberLabel.text = @"10";
        _messageNumberLabel.textColor = [UIColor whiteColor];

        _messageNumberLabel.numberOfLines =0;
       
        
    }
    return _messageNumberLabel;
}
- (UIView *)messageBgView {
    if (!_messageBgView) {
        _messageBgView = [[UIView alloc]init];
        
        _messageBgView.gradientStyle = 1;
        _messageBgView.gradientAColor = RGBA(255, 165, 165, 1);
        _messageBgView.gradientBColor = RGBA(255, 133, 133, 1);
        _messageBgView.cornerRadius = 7;
    }
    return _messageBgView;
}
- (UILabel *)messLineLabel {
    if (!_messLineLabel) {
        _messLineLabel = [[UILabel alloc]init];
        _messLineLabel.backgroundColor = rgba(223, 223, 223, 0.5);
    }
    return _messLineLabel;
}
@end
