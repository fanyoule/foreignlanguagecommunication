//
//  ChatInformationPersonTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/7.
//

#import <MGSwipeTableCell/MGSwipeTableCell.h>

#import "IMSessionListModel.h"
#import "SystemMegListModel.h"
#import "MessageCommentModel.h"
#import "MessageOfficialModel.h"
#import "MessageWalletModel.h"
#import "MessageCourseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ChatInformationPersonTableViewCell : MGSwipeTableCell
/** 头像*/
@property (nonatomic, strong) UIImageView *pictureImageV;
/** 名字*/
@property (nonatomic, strong) UILabel *nameLabel;
/** 数量*/
@property (nonatomic, strong) UILabel *numberLabel;



@property (nonatomic, strong) IMSessionListModel *sessionModel;

@property (nonatomic, strong) SystemMegListModel *sysModel;

@property (nonatomic, strong) MessageCommentModel *commentModel;

@property (nonatomic, strong) MessageOfficialModel *officialModel;
@property (nonatomic, strong) MessageWalletModel *walletModel;
@property (nonatomic, strong) MessageCourseModel *courseModel;
@end

NS_ASSUME_NONNULL_END
