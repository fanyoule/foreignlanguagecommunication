//
//  GroupListModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/2/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupListModel : NSObject
@property (nonatomic, assign) long Id;              // 群或讨论组id
@property (nonatomic, assign) long type;            // 群类型（1：公会群，2：个人群，3：讨论组）
@property (nonatomic, assign) long masterClusterId; // 总群id
@property (nonatomic, assign) BOOL isDisturb;       // 是否免打扰
@property (nonatomic, assign) BOOL isMasterCluster; // 是否总群
@property (nonatomic, assign) BOOL isShield;        // 是否屏蔽
@property (nonatomic, copy) NSString *name;         // 昵称
@property (nonatomic, copy) NSString *url;          // 头像
@end

NS_ASSUME_NONNULL_END
