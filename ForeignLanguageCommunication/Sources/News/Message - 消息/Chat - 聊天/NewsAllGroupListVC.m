//
//  NewsAllGroupListVC.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/2/26.
//

#import "NewsAllGroupListVC.h"
#import "FriendTableViewCell.h"
#import "MyPersonalInformationVC.h"
#import "GroupListModel.h"
#import "ChatDetailsUserPoslitionModel.h"
#import "ChatDetailsGroupVC.h"
#import "CreateGroupViewController.h"


@interface NewsAllGroupListVC ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    BOOL isSearch;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *searchArr;
@property (nonatomic, strong) UISearchBar *searchBar;
@end

@implementation NewsAllGroupListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    isSearch = NO;
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"我的群组";
    [self addRightBtnWith:@"创建"];
    self.dataArr = [NSMutableArray array];
    self.searchArr = [NSMutableArray array];
    
    [self creatUI];
    [self getdata];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createGroupSuccess) name:@"createNoti" object:nil];
}
-(void)creatUI{
    self.topConstraint.constant = SNavBarHeight;
    [self.view layoutIfNeeded];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = kBackgroundColor;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:[FriendTableViewCell toString] bundle:nil] forCellReuseIdentifier:[FriendTableViewCell toString]];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60)];
    self.searchBar.placeholder = @"搜索";
    self.tableView.tableHeaderView = self.searchBar;
    self.searchBar.delegate = self;
    [self.tableView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.tableView);
    }];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)createGroupSuccess {
    self.searchBar.text = @"";
    isSearch = NO;
    self.searchArr = [NSMutableArray array];
    [self getdata];
}
- (void)rightClick:(UIButton *)sender {
    CreateGroupViewController *vc = [[CreateGroupViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchArr = [NSMutableArray array];
    if (searchText && searchText.length > 0) {
        isSearch = YES;
        for (GroupListModel *model in self.dataArr) {
            if ([model.name containsString:searchText]) {
                [self.searchArr addObject:model];
            }
        }
    } else {
        isSearch = NO;
    }
    [self.tableView reloadData];
}
- (void)getdata {
    kWeakSelf(self)
    [RequestManager myJoinedClusterWithId:[UserInfoManager shared].getUserID withSuccess:^(id  _Nullable response) {
        weakself.dataArr = [GroupListModel mj_objectArrayWithKeyValuesArray:response[@"data"]];
        weakself.nullView.hidden = weakself.dataArr.count;
        [weakself.tableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isSearch) {
        return self.searchArr.count;
    } else {
        return self.dataArr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[FriendTableViewCell toString]];
    GroupListModel *model;
    if (isSearch) {
        model = self.searchArr[indexPath.row];
    } else {
        model = self.dataArr[indexPath.row];
    }
    [cell setModelGroupList:model];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupListModel *model;
    if (isSearch) {
        model = self.searchArr[indexPath.row];
    } else {
        model = self.dataArr[indexPath.row];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    dic[@"id"] = @(model.Id);
    dic[@"userId"] = @([UserInfoManager shared].getUserID);
    [RequestManager getMyDataWithParameters:dic UrlString:@"/api/personalCluster/IMClusterInfo" Success:^(id  _Nullable response) {
        NewsAllGroupListModel *groupModel = [[NewsAllGroupListModel alloc]init];
        groupModel.ID = model.Id;
        groupModel.type = model.type;
        groupModel.name = model.name;
        NSMutableDictionary *dataDic = response[@"data"];
         ChatDetailsUserPoslitionModel *poslitonModel = [ChatDetailsUserPoslitionModel mj_objectWithKeyValues:dataDic];
         ChatDetailsGroupVC *vc = [[ChatDetailsGroupVC alloc]init];
        vc.buddyModel = groupModel;
        
        vc.politionModel = poslitonModel;
        [self.navigationController pushViewController:vc animated:NO];
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"获取群信息失败");
    }];
}
@end
