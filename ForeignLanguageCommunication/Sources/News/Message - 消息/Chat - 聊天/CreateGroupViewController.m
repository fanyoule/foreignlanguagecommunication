//
//  CreateGroupViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/2/2.
//

#import "CreateGroupViewController.h"

@interface CreateGroupViewController ()<UITextViewDelegate>

@property (nonatomic, strong)UITextView *textView;

@property (nonatomic, strong)UILabel *placeholderLabel;

@property (nonatomic, strong)UILabel *promptLabel;
@end

@implementation CreateGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"创建群";
    self.navView.backgroundColor = UIColor.whiteColor;
    [self addRightBtnWith:@"完成"];
    [self.rightBtn setTitleColor:RGBA(153, 153, 153, 1) forState:(UIControlStateNormal)];
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    [self.view addSubview:self.textView];
    [self.view addSubview:self.placeholderLabel];
    [self.view addSubview:self.promptLabel];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@100);
    }];
    [self.placeholderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView).offset(15);
        make.left.equalTo(self.textView).offset(18);
    }];
    [self.promptLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(15);
        make.right.equalTo(self.textView);
    }];
}
- (void)rightClick:(UIButton *)sender {
    if (!self.textView.text || self.textView.text.length <= 0) {
        [self showText:@"请输入群昵称"];
        return;
    }
    kWeakSelf(self)
    [RequestManager addPersonalClusterWithId:[UserInfoManager shared].getUserID clusterName:self.textView.text withSuccess:^(id  _Nullable response) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"createNoti" object:nil];
//        [weakself showText:@"创建成功"];
//        [weakself.navigationController popViewControllerAnimated:YES];
        
        NSDictionary *data = response[@"data"];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        dic[@"group_id"] = data[@"id"];
        dic[@"avatarUrl"] = data[@"avatarUrl"];
        dic[@"nickName"] = data[@"clusterName"];
        
        dic[@"from"] = @([UserInfoManager shared].getUserID);
        dic[@"msgType"] = @(0);//消息类型0:text、1:image、2:voice、3:vedio、4:Tips、5:news
        dic[@"chatType"] = @(4);//
        dic[@"groupType"] = @(2);//群组类型
        dic[@"content"] = @([UserInfoManager shared].getUserID);;//入群者id
        [self requstChairman:dic];
        
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
#pragma mark - 发送消息给im创建群
-(void)requstChairman:(NSMutableDictionary*)dic{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: dic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString * str = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //发送消息方法
//  [self sendMessage:str cmd:(Byte)11];
    [[ChatIMSend share]sendMessage:str];
    [self showText:@"创建成功"];
   [self.navigationController popViewControllerAnimated:YES];
}

- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc]init];
        _textView.backgroundColor = UIColor.whiteColor;
        _textView.font = kFont(16);
        _textView.layer.cornerRadius = 12;
        _textView.clipsToBounds = YES;
        _textView.textContainerInset = UIEdgeInsetsMake(15, 15, 15, 15);
        _textView.delegate = self;
    }
    return _textView;
}
- (UILabel *)placeholderLabel{
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc]init];
        _placeholderLabel.text = @"请输入群昵称";
        _placeholderLabel.textColor = RGBA(153, 153, 153, 1);
        _placeholderLabel.font = kFont(16);
        _placeholderLabel.textAlignment = 2;
    }
    return _placeholderLabel;
}
- (UILabel *)promptLabel{
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc]init];
        _promptLabel.text = @"0/10";
        _promptLabel.textColor = RGBA(0, 122, 255, 1);
        _promptLabel.font = kFont(12);
        _promptLabel.textAlignment = 2;
    }
    return _promptLabel;
}
- (void)textViewDidChange:(UITextView *)textView{

    self.placeholderLabel.hidden = YES;
     
        //实时显示字数
    self.promptLabel.text = [NSString stringWithFormat:@"%lu/10", (unsigned long)textView.text.length];
    
    
    //字数限制操作
    if (textView.text.length >= 10) {
        [self showText:@"最多不超过10个字"];
        textView.text = [textView.text substringToIndex:10];
        self.promptLabel.text = @"10/10";
    }
    
    //取消按钮点击权限，并显示提示文字
    if (textView.text.length == 0) {

    self.placeholderLabel.hidden = NO;
        
    }
}
@end
