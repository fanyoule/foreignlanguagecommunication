//
//  TheRoomTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "TheRoomTableViewCell.h"

@interface TheRoomTableViewCell ()
/**被色背景*/
@property (nonatomic ,strong)UIView *backView;
/**头像*/
@property (nonatomic ,strong)UIImageView *portraitImageV;
/**房间名*/
@property (nonatomic, strong)UILabel *nameLabel;
/**内容*/
@property (nonatomic, strong)UILabel *contentLabel;
/**人员图片*/
@property (nonatomic, strong)UIImageView *personnelImageV;
/**人员数量*/
@property (nonatomic, strong)UILabel *personnelNumberLabel;
/**到期*/
@property (nonatomic, strong)UILabel *dueToLabel;
@end

@implementation TheRoomTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addControls];
        
    }
    return self;
}

-(void)addControls{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.portraitImageV];
    [self.backView addSubview:self.nameLabel];
    [self.backView addSubview:self.contentLabel];
    [self.backView addSubview:self.personnelImageV];
    [self.backView addSubview:self.personnelNumberLabel];
    [self.backView addSubview:self.dueToLabel];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(5);
       
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.height.equalTo(@90);
        make.bottom.equalTo(self.contentView).offset(-5).priority(600);
    }];
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(15);
        make.width.height.equalTo(@60);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.portraitImageV).offset(9);
        make.left.equalTo(self.portraitImageV.mas_right).offset(15.5);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.portraitImageV).offset(-10);
        make.left.equalTo(self.portraitImageV.mas_right).offset(15.5);
    }];
    [self.personnelNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.right.equalTo(self.backView).offset(-15.5);
    }];
    [self.personnelImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView);
        make.right.equalTo(self.personnelNumberLabel.mas_left).offset(-5);
    }];
    [self.dueToLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self.backView);
        make.height.equalTo (@20);
        make.width.equalTo(@50);
    }];
    
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc]init];
//        _backView.backgroundColor = [UIColor grayColor];
        _backView.backgroundColor = UIColor.whiteColor;
        _backView.layer.shadowOffset = CGSizeMake(0,1);
        _backView.layer.shadowRadius = 4.f;
        _backView.layer.shadowOpacity = 0.8f;
        _backView.layer.shadowColor = [UIColor grayColor].CGColor;
        _backView.layer.cornerRadius = 6;
        
    }
    return _backView;
}
- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = [UIImage imageNamed:@"测试头像"];
        _portraitImageV.layer.cornerRadius = 60/2;
        _portraitImageV.layer.masksToBounds = YES;
    }
    return _portraitImageV;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"房间名称名称";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Bold(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.text = @"房间交流内容房间交流内容";
        _contentLabel.textColor = RGBA(102, 102, 102, 1);
        _contentLabel.font = kFont_Medium(13);
        _contentLabel.textAlignment = 0;
    }
    return _contentLabel;
}

- (UIImageView *)personnelImageV{
    if (!_personnelImageV) {
        _personnelImageV = [[UIImageView alloc]init];
        _personnelImageV.image = [UIImage imageNamed:@"人数图片"];
    }
    return _personnelImageV;
}
- (UILabel *)personnelNumberLabel{
    if (!_personnelNumberLabel) {
        _personnelNumberLabel = [[UILabel alloc]init];
        _personnelNumberLabel.text = @"10";
        _personnelNumberLabel.textColor = RGBA(51, 51, 51, 1);
        _personnelNumberLabel.font = kFont_Medium(14);
        _personnelNumberLabel.textAlignment = 0;
    }
    return _personnelNumberLabel;
}

- (UILabel *)dueToLabel{
    if (!_dueToLabel) {
        _dueToLabel = [[UILabel alloc]init];
        _dueToLabel.backgroundColor = RGBA(241, 88, 95, 1);
        _dueToLabel.text = @"即将到期";
        _dueToLabel.textColor = RGBA(255, 255, 255, 1);
        _dueToLabel.font = kFont(11);
        _dueToLabel.textAlignment = 1;
    }
    return _dueToLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
