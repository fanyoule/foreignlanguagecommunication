//
//  RoomDivisionHeadView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//

#import "RoomDivisionHeadView.h"

@implementation RoomDivisionHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.pictureImageV];
    [self addSubview:self.nameLabel];
    [self addSubview:self.newLabel];
    [self.pictureImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(15);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.pictureImageV);
        make.left.equalTo(self.pictureImageV.mas_right).offset(4);
    }];
    [self.newLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-15);
    }];
}

- (UIImageView *)pictureImageV{
    if (!_pictureImageV) {
        _pictureImageV  =[[UIImageView alloc]init];
        _pictureImageV.image = [UIImage imageNamed:@"我的房间"];
    }
    return _pictureImageV;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"我的房间";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Bold(15);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

- (UILabel *)newLabel{
    if (!_newLabel) {
        _newLabel = [[UILabel alloc]init];
        _newLabel.text = @"+新建房间";
        _newLabel.textColor = RGBA(52, 120, 245, 1);
        _newLabel.font = kFont_Bold(14);
        _newLabel.textAlignment = 2;
    }
    return _newLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
