//
//  TheRoomViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2020/12/17.
//  房间

#import "TheRoomViewController.h"
#import "TheRoomTableViewCell.h"
#import "RoomDivisionHeadView.h"
@interface TheRoomViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation TheRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    
    [self.view addSubview:self.mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.bottom.right.left.equalTo(self.view);
    }];
    
    // Do any additional setup after loading the view.
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    RoomDivisionHeadView *view = [[RoomDivisionHeadView alloc]init];
    view.backgroundColor = [UIColor whiteColor];
    if (section == 1) {
        view.nameLabel.text = @"我加入的房间";
        view.pictureImageV.image = [UIImage imageNamed:@"我加入的房间"];
        view.newLabel.hidden = YES;
    }
    return view;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 34;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if (section == 0) {
//        return 2;
//    }
    return 6;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
        TheRoomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TheRoomTableViewCell"];
        if (!cell) {
            cell = [[TheRoomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TheRoomTableViewCell"];
        }
//    MGSwipeButton *deleteButton=[MGSwipeButton buttonWithTitle:@"删除" backgroundColor:[UIColor colorWithRed:255/255.0 green:53/255.0 blue:82/255.0 alpha:1.0]];
//    deleteButton.buttonWidth = 70;
//    deleteButton.layer.cornerRadius = 0;
//    deleteButton.titleLabel.font =  [UIFont systemFontOfSize: 12.0];
//    deleteButton.callback = ^BOOL(MGSwipeTableCell * _Nonnull cell) {
//
//        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
//        [dic setObject:@(model.channelMusicId) forKey:@"id"];
//        [UnionRequestManager deleteChannelSongIncomingParameter:dic withSuccess:^(id  _Nullable response) {
//            NSLog(@"删除成功 ==== %@",response);
//            if ([response[@"status"] intValue] == 200) {
//                [self showText:@"删除成功"];
//                [self.listArray removeObject:model];
////                    [self.listArray removeAllObjects];
////                    [self getData];
//                [self.tableV reloadData];
//                if (self.listArray != nil && ![self.listArray isKindOfClass:[NSNull class]] && (self.listArray.count != 0)) {
//
//                    [self addChannelSongs];
//                }else{
//                    [self channelNoData];
//
//                }
//                self.numberLabel.text = [NSString stringWithFormat:@"%ld",self.listArray.count];
//            }
//
//        } withFail:^(NSError * _Nullable error) {
//
//        }];
//        return YES;
//    };
//    cell.rightButtons = @[deleteButton];
//    cell.rightSwipeSettings.transition = MGSwipeTransition3D;
//    cell.rightSwipeSettings.bottomMargin =0;//距离底部
//    cell.rightSwipeSettings.topMargin = 0;//button距离t顶部
//    cell.rightSwipeSettings.offset = 0;//最右侧button距离边框的距离
//    cell.rightSwipeSettings.buttonsDistance = 0;//button的间距
    return cell;
}
/** tableView 点击事件*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
