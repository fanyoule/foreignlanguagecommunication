//
//  HomeMatchViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/13.
//

#import "BaseViewController.h"

#import "MatchRemainCountModel.h"
#import "HomeMatchTypeListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeMatchViewController : BaseViewController
@property (nonatomic) NSInteger matchType;
@property (nonatomic, strong) MatchRemainCountModel *countModel;
@property (nonatomic, strong) HomeMatchTypeListModel *matchModel;
@end

NS_ASSUME_NONNULL_END
