//
//  HomeMatchViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/13.
//

#import "HomeMatchViewController.h"

@interface HomeMatchViewController ()
/**标题提示*/
@property (strong, nonatomic) UILabel *titleLabel;
/**小标题提示*/
@property (strong, nonatomic) UILabel *subTitleLabel;
/**中间头像 自己的*/
@property (strong, nonatomic) UIImageView *avatarView;
/**取消*/
@property (strong, nonatomic) UIButton *cancelBtn;
/**剩余匹配次数*/
@property (nonatomic, strong) UILabel *remainCountLabel;

/**背景图*/
/**底部背景图*/
@property (nonatomic, strong) UIImageView *bottomImageV;
/**匹配中 或  匹配失败 背景图 三个圆*/
@property (nonatomic, strong) UIView *matchingView;


/**匹配成功*/
@property (nonatomic, strong) UIView *successView;

/**匹配成功 顶部*/
@property (nonatomic, strong) UIView *successTopView;
/**匹配成功 顶部头像  右边*/
@property (nonatomic, strong) UIImageView *topAvatarRightView;
/**匹配成功 顶部头像  左边*/
@property (nonatomic, strong) UIImageView *topAvatarLeftView;
/**匹配成功 提示*/
@property (nonatomic, strong) UILabel *successLabel;
/**匹配成功 对方的头像*/
@property (nonatomic, strong) UIImageView *successAvatarView;
/**匹配成功 对方名字*/
@property (nonatomic, strong) UILabel *successNameLabel;
/**匹配成功 打招呼按钮  没有点击事件*/
@property (nonatomic, strong) UIButton *successBtn;
/**匹配成功 中间视图 带背景*/
@property (nonatomic, strong) UIView *successMiddleView;


@end

@implementation HomeMatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupSubview];
    
    self.navView.backgroundColor = UIColor.clearColor;
    [self.backBtn setImage:kImage(@"icon_nav_back_white") forState:UIControlStateNormal];
    
}

- (void)setupSubview {
    [self.view addSubview:self.bottomImageV];
    [self.bottomImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_offset(0);
            make.height.mas_equalTo(kScaleSize(222));
    }];
    
    [self.view addSubview:self.matchingView];
    [self.matchingView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
    }];
    [self.view addSubview:self.successMiddleView];
    [self.successMiddleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_offset(0);
    }];
    self.successMiddleView.hidden = YES;
    
    [self.view addSubview:self.remainCountLabel];
    [self.remainCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_offset(0);
            make.top.mas_equalTo(self.cancelBtn.mas_bottom).mas_offset(10);
    }];
    
    
    self.remainCountLabel.text = [NSString stringWithFormat:@"今日剩余%ld次",self.countModel.remainCount];
    
    [self getData];
}

- (void)cancelMatch:(UIButton *)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//    [self matchSuccess];
    [self showSVP];
    kWeakSelf(self)
    [RequestManager cancelMatchWithId:[UserInfoManager shared].getUserID typeId:self.matchModel.ID chatType:3 withSuccess:^(id  _Nullable response) {
        [weakself dissSVP];
        [weakself.navigationController popViewControllerAnimated:YES];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}

- (UIImageView *)bottomImageV {
    if (!_bottomImageV) {
        _bottomImageV = [[UIImageView alloc] initWithImage:kImage(@"icon_match_cloud")];
        
    }
    return _bottomImageV;
}


//MARK: ------------匹配中-----------
- (UIView *)matchingView{
    if (!_matchingView) {
        _matchingView = [UIView new];
        _matchingView.backgroundColor = UIColor.clearColor;
        UIImageView *imageV1 = [[UIImageView alloc] initWithImage:kImage(@"icon_match_round_3")];
        [_matchingView addSubview:imageV1];
        UIImageView *imageV2 = [[UIImageView alloc] initWithImage:kImage(@"icon_match_round_2")];
        [_matchingView addSubview:imageV2];
        UIImageView *imageV3 = [[UIImageView alloc] initWithImage:kImage(@"icon_match_round_1")];
        [_matchingView addSubview:imageV3];
        
        [imageV1 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.height.mas_equalTo(KSW);
                    make.center.mas_equalTo(_matchingView);
        }];
        [imageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.center.mas_equalTo(imageV1);
                    make.width.height.mas_equalTo(kScaleSize(265));
        }];
        [imageV3 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.height.mas_equalTo(kScaleSize(150));
                    make.center.mas_equalTo(imageV1);
        }];
        
        
        [_matchingView addSubview:self.avatarView];
        [self.avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.height.mas_equalTo(kScaleSize(85));
                    make.center.mas_offset(0);
        }];
        
        [_matchingView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_offset(0);
                    make.top.mas_offset(SNavBarHeight+20);
                    
        }];
        
        [_matchingView addSubview:self.subTitleLabel];
        [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_offset(0);
                    make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(25);
        }];
        [_matchingView addSubview:self.cancelBtn];
        [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(185);
            make.height.mas_equalTo(45);
            make.centerX.mas_offset(0);
            make.bottom.mas_offset(-kScaleSize(58));
        }];
        
    }
    return _matchingView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.textColor = UIColor.whiteColor;
        _titleLabel.font = kFont_Bold(23);
        _titleLabel.text = @"开始匹配中...";
    }
    return _titleLabel;
}

- (UILabel *)subTitleLabel {
    if (!_subTitleLabel) {
        _subTitleLabel = [UILabel new];
        _subTitleLabel.textColor = CustomColor(@"#CEBAE5");
        _subTitleLabel.font = kFont(12);
        _subTitleLabel.textAlignment = NSTextAlignmentCenter;
        _subTitleLabel.text = @"正在努力帮您匹配中，请耐心等待！";
    }
    return _subTitleLabel;
}

- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelBtn setBackgroundImage:kImage(@"icon_match_btn") forState:UIControlStateNormal];
        [_cancelBtn setTitle:@"取消匹配" forState:UIControlStateNormal];
        [_cancelBtn setTitleColor:CustomColor(@"#6218A4") forState:UIControlStateNormal];
        _cancelBtn.titleLabel.font = kFont_Bold(15);
        [_cancelBtn addTarget:self action:@selector(cancelMatch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelBtn;
}


//MARK: 匹配成功
- (UIView *)successView{
    if (!_successView) {
        _successView = [UIView new];
        _successView.backgroundColor = UIColor.clearColor;
        
        [_successView addSubview:self.successTopView];
        [self.successTopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_offset(0);
            make.top.mas_offset(SNavBarHeight+20);
        }];
        [_successView addSubview:self.successMiddleView];
        [self.successMiddleView mas_makeConstraints:^(MASConstraintMaker *make) {
                    
                    make.left.right.mas_offset(0);
                    make.centerY.mas_offset(0);
        }];
        
        
    }
    return _successView;
}
//顶部 头像 匹配成功
- (UIView *)successTopView{
    if (!_successTopView) {
        _successTopView = [UIView new];
        [_successTopView addSubview:self.topAvatarLeftView];
        [_successTopView addSubview:self.topAvatarRightView];
        [_successTopView addSubview:self.successLabel];
        
        [self.topAvatarLeftView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_offset(5);
                    make.top.mas_offset(5);
                    make.bottom.mas_offset(-5);
                    make.width.height.mas_equalTo(36);
        }];
        [self.topAvatarRightView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.height.mas_equalTo(36);
                    make.centerY.mas_equalTo(self.topAvatarLeftView);
                    make.left.mas_equalTo(self.topAvatarLeftView.mas_right).mas_offset(-13);
        }];
        [self.successLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.mas_offset(0);
                    make.right.mas_offset(-5);
                    make.left.mas_equalTo(self.topAvatarRightView.mas_right).mas_offset(40);
        }];
    }
    return _successTopView;
}
- (UILabel *)successLabel{
    if (!_successLabel) {
        _successLabel = [UILabel new];
        _successLabel.font = kFont_Bold(23);
        _successLabel.text = @"匹配成功";
        _successLabel.textColor = UIColor.whiteColor;
    }
    return _successLabel;
}

- (UIImageView *)topAvatarLeftView {
    if (!_topAvatarLeftView) {
        _topAvatarLeftView = [UIImageView new];
        _topAvatarLeftView.layer.cornerRadius = 18.0;
    }
    return _topAvatarLeftView;
}
- (UIImageView *)topAvatarRightView{
    if (!_topAvatarRightView) {
        _topAvatarRightView = [UIImageView new];
        _topAvatarRightView.layer.cornerRadius = 18.0;
        _topAvatarRightView.clipsToBounds = YES;
    }
    return _topAvatarRightView;
}
//中间 头像 名字 背景图 打招呼按钮 下划线图

- (UIView *)successMiddleView {
    if (!_successMiddleView) {
        _successMiddleView = [UIView new];
        _successMiddleView.backgroundColor = UIColor.clearColor;
        
        UIImageView *backview = [[UIImageView alloc] initWithImage:kImage(@"icon_match_finish_back")];
        [_successMiddleView addSubview:backview];
        [backview mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_offset(0);
                    make.centerX.mas_offset(0);
                    make.width.mas_equalTo(215);
                    make.height.mas_equalTo(280);
        }];
        [_successMiddleView addSubview:self.successAvatarView];
        [self.successAvatarView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.width.height.mas_equalTo(85);
                    make.centerX.mas_offset(0);
                    make.top.mas_offset(0);
                    make.centerY.mas_equalTo(backview.mas_top);
        }];
        
        [_successMiddleView addSubview:self.successNameLabel];
        [self.successNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_offset(0);
                    make.top.mas_equalTo(self.successAvatarView.mas_bottom).mas_offset(15);
        }];
        
        UIImageView *line = [[UIImageView alloc] initWithImage:kImage(@"icon_match_finish_line")];
        [_successMiddleView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(3.5);
                    make.width.mas_equalTo(145);
                    make.centerX.mas_offset(0);
                    make.bottom.mas_offset(-30);
        }];
        
        [_successMiddleView addSubview:self.successBtn];
        [self.successBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_offset(0);
                    make.width.mas_equalTo(120);
                    make.height.mas_equalTo(34);
                    make.bottom.mas_equalTo(line.mas_top).mas_offset(-50);
        }];
        
        
    }
    return _successMiddleView;
}
- (UIImageView *)avatarView {
    if (!_avatarView) {
        _avatarView = [UIImageView new];
        _avatarView.layer.cornerRadius = 6.0;
        _avatarView.clipsToBounds = YES;
        _avatarView.layer.borderColor = CustomColor(@"#00FFFF").CGColor;
        _avatarView.layer.borderWidth = 2;
        
        [_avatarView sd_setImageWithURL:[[UserInfoManager shared] getUserAvatar].mj_url];
    }
    return _avatarView;
}
- (UILabel *)remainCountLabel{
    if (!_remainCountLabel) {
        _remainCountLabel = [UILabel new];
        _remainCountLabel.textAlignment = NSTextAlignmentCenter;
        _remainCountLabel.textColor = UIColor.whiteColor;
        _remainCountLabel.font = kFont(12);
    }
    return _remainCountLabel;
}
- (UIImageView *)successAvatarView {
    
    if (!_successAvatarView) {
        _successAvatarView = [UIImageView new];
        _successAvatarView.layer.cornerRadius = 85.0/ 2.0;
        _successAvatarView.clipsToBounds = YES;
        _successAvatarView.layer.borderWidth = 4.0;
        _successAvatarView.layer.borderColor = CustomColor(@"#FF1A69").CGColor;
    }
    return _successAvatarView;
}
- (UILabel *)successNameLabel{
    if (!_successNameLabel) {
        _successNameLabel = [UILabel new];
        _successNameLabel.textColor = CustomColor(@"#FFFFFF");
        _successNameLabel.font = kFont_Bold(18);
        _successNameLabel.textAlignment = NSTextAlignmentCenter;
        _successNameLabel.backgroundColor = UIColor.clearColor;
    }
    return _successNameLabel;
}
- (UIButton *)successBtn {
    if (!_successBtn) {
        _successBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_successBtn setBackgroundImage:kImage(@"icon_match_finish_btn") forState:UIControlStateNormal];
        [_successBtn setTitle:@"打招呼" forState:UIControlStateNormal];
        [_successBtn setTitleColor:CustomColor(@"#3B166F") forState:UIControlStateNormal];
        _successBtn.titleLabel.font = kFont_Medium(15);
    }
    return _successBtn;
}

//MARK: 匹配失败

- (void)matchFail{
    self.successView.hidden = YES;
    self.matchingView.hidden = NO;
    self.titleLabel.text = @"暂未匹配到";
    self.subTitleLabel.text = @"请更改选择标签，再次进行匹配";
}

//MARK: 匹配成功
- (void)matchSuccess {
    self.matchingView.hidden = YES;
    self.successView.hidden = NO;
    
    self.topAvatarLeftView.image = kImage(@"icon_default_head");
    self.topAvatarRightView.image = kImage(@"icon_default_head");
    
    self.successAvatarView.image = kImage(@"0");
    self.successNameLabel.text = @"傻菇凉@";
    
}

- (void)clickToSayHi:(UIButton *)sender {
    [self matchFail];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)getData{
    
    [RequestManager matchByTypeWithId:[[UserInfoManager shared] getUserID] typeId:self.matchModel.ID chatType:3 avatar:[[UserInfoManager shared] getUserAvatar] nickname:[[UserInfoManager shared] getUserNickName] withSuccess:^(id  _Nullable response) {
        NSLog(@"%@",response);
        
    } withFail:^(NSError * _Nullable error) {
        [self showText:@"匹配失败"];
    }];
}
@end
