//
//  HomeCornerViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
// headview search cycleview
//快速匹配
//排行榜
//推荐房间

#import "HomeCornerViewController.h"
#import "HomeHotTableViewCell.h"
#import "HomeRankTableViewCell.h"
#import "HomeRecommenRoomTableViewCell.h"
#import "HomeCollectionCellModel.h"
#import "HomeMoreViewController.h"

#import "HomeSearchPageViewController.h"// 搜索页面

#import "HomeCornerModel.h"


#import "SearchCoachViewController.h"// 找教练
#import "CoachDetailsViewController.h"// 教练详情
#import "RankingViewController.h"// 排行榜

#import "RankingViewController.h"
#import "CoachWithCourseModel.h"
#import "CoachWithCourseTableViewCell.h"
#import "HomeWebViewController.h"
#import "CoachTagListView.h"
#import "CourseDetailsViewController.h"// 课程详情

@interface HomeCornerViewController ()<SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) SDCycleScrollView *cycleView;
@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableDictionary *dataDictionary;

@property (nonatomic, strong) NSMutableArray *coachDataArray;

@property (nonatomic, strong) HomeCornerModel *cornerModel;
@end

@implementation HomeCornerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupSubview];
    [self getData];
}
- (void)setupSubview {
    UIView *searchView = [self searchView];
    [self.view addSubview:searchView];
    searchView.frame = CGRectMake(15, 0, KSW-30, 35);
    
    [self.view addSubview:self.tableV];
    self.view.backgroundColor = UIColor.clearColor;
    NSLog(@"height===%f screen==%f",self.view.height,ScreenHeight);//ScreenHeight - kScaleSize(136) - TabbarHeight+20
    self.tableV.frame = CGRectMake(0, 40, KSW, ScreenHeight-55-SBarHeight-TabbarHeight-40);
    
    self.tableV.tableHeaderView = [self tableViewHeadView];
    
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 100;
    self.tableV.rowHeight = UITableViewAutomaticDimension;
    
    UIImageView *topback = [[UIImageView alloc] initWithImage:kImage(@"icon_home_top_half")];
    topback.frame = CGRectMake(0, 35, KSW, kScaleSize(121.5));
    [self.view addSubview:topback];
    [self.view insertSubview:topback atIndex:0];
    self.tableV.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self getDataSource];
    }];
    
    

    
}
- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (NSMutableDictionary *)dataDictionary {
    if (!_dataDictionary) {
        _dataDictionary = [NSMutableDictionary dictionary];
    }
    return _dataDictionary;
}
- (UITableView *)tableV {
    if (!_tableV) {
        _tableV = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableV.backgroundColor = UIColor.clearColor;
        _tableV.showsVerticalScrollIndicator = NO;
        _tableV.estimatedRowHeight = 100;
        _tableV.delegate = self;
        _tableV.dataSource = self;
        _tableV.backgroundColor = UIColor.clearColor;
        _tableV.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11.0, *)) {
            _tableV.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else{
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    return _tableV;
}
- (UIView *)sectionHeaderViewWith:(NSString *)title withButton:(NSString * _Nullable)buttonString isJudge:(NSInteger)type{
    UIView *headView = [UIView new];
    headView.backgroundColor = UIColor.whiteColor;
    UILabel *titleLabel = [UILabel new];
    [headView addSubview:titleLabel];
    titleLabel.textColor = CustomColor(@"#222222");
    titleLabel.font = kFont_Bold(19);
    titleLabel.text = title;
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.centerY.mas_offset(0);
    }];
    if (buttonString) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [headView addSubview:btn];
        [btn setTitle:buttonString forState:UIControlStateNormal];
        [btn setTitleColor:RGBA(52, 120, 245, 1) forState:UIControlStateNormal];
        btn.titleLabel.font = kFont(12);
        [btn setBackgroundColor:CustomColor(@"#FCFCFC")];
        
        btn.layer.cornerRadius = 13.5;
        btn.layer.borderWidth = 1;
        btn.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
        
        [btn sizeToFit];
        CGSize size = btn.size;
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_offset(0);
                make.height.mas_equalTo(27);
                make.width.mas_equalTo(size.width+16);
                make.right.mas_offset(-15);
        }];
        [btn whenTapped:^{
            [self clickSectionBtn:type];
        }];
        [btn addTarget:self action:@selector(clickSectionBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    return headView;
}
#pragma mark -- 更多
/** 1 是: 快速匹配   2是: 明星教练*/
- (void)clickSectionBtn:(NSInteger)type {
    if (![UserInfoManager shared].isLogin) {
        [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            if (index == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
            }
        }];
        return;
    }
    NSLog(@"点击了%ld",type);
    if (type == 1) {
        HomeMoreViewController *morevc = [[HomeMoreViewController alloc] init];
        morevc.index = 0;
        [self.navigationController pushViewController:morevc animated:YES];
    }else{
        SearchCoachViewController *vc = [[SearchCoachViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
- (UIView *)searchView {
    UIView *searchView = [UIView new];
    searchView.size = CGSizeMake(KSW - 30, 36);
    searchView.backgroundColor = CustomColor(@"#F8F8F8");
    searchView.layer.cornerRadius = 18;
    UIImageView *iconV = [[UIImageView alloc] initWithImage:kImage(@"icon_home_seartch_black")];
    [searchView addSubview:iconV];
    UILabel *label = [[UILabel alloc]init];
    label.text = @"搜索";
    label.font = kFont_Medium(14);
    label.textColor = CustomColor(@"#999999");
    [searchView addSubview:label];
    [label sizeToFit];
    CGSize size = label.size;
    //15 + 5 + size.width
    CGFloat x = (KSW-30 - 15-5-size.width)/2;
    [iconV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(15);
            make.left.mas_offset(x);
        make.centerY.mas_offset(0);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(iconV.mas_right).mas_offset(5);
            make.centerY.mas_offset(0);
    }];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchView addSubview:btn];
    [btn setBackgroundColor:UIColor.clearColor];
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(searchView);
    }];
    [btn addTarget:self action:@selector(clickSearch:) forControlEvents:UIControlEventTouchUpInside];
    
    return searchView;
}
#pragma mark -- 搜索
- (void)clickSearch:(UIButton *)sender {
    HomeSearchPageViewController *vc = [[HomeSearchPageViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableViewHeadView {
    UIView *headView = [UIView new];
    headView.frame = CGRectMake(0, 0, KSW, (KSW-30) / 345 * 136+20);
    headView.backgroundColor = UIColor.clearColor;
    UIImageView *backImageView = [[UIImageView alloc] initWithImage:kImage(@"icon_home_top_half")];
    backImageView.frame = CGRectMake(0, 0, KSW, kScaleSize(121.5));
    //[headView addSubview:backImageView];
    self.cycleView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15, 10, KSW-30, (KSW-30) / 345 * 136) delegate:self placeholderImage:kImage(@"icon_home_banner")];
    self.cycleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
//    self.cycleView.localizationImageNamesGroup = @[@"icon_home_banner",@"icon_home_banner"];
    self.cycleView.layer.cornerRadius = 12;
    self.cycleView.delegate = self;
    self.cycleView.clipsToBounds = YES;
    [headView addSubview:self.cycleView];
    
    return headView;
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    //点击选择
    HomeImageListModel *model = self.cornerModel.imageList[index];
    NSLog(@"点击banner");
    if (![UserInfoManager shared].isLogin) {
        [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            if (index == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
            }
        }];
        return;
    }
    if (model.isH5 && model.h5Url) {
        HomeWebViewController *vc = [[HomeWebViewController alloc] init];
        vc.url = model.h5Url;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        //快速匹配 + 榜单
        return 2;
    }
    //明星教练
    
    if (self.coachDataArray.count>0) {
        //明星教练的数量
        return self.coachDataArray.count;
    }
//    if (self.cornerModel.roomTypeList.count > 0) {
//        HomeRoomTypeListModel *model = self.cornerModel.roomTypeList[0];
//        return model.roomList.count;
//    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            //快速匹配
            HomeHotTableViewCell *hotCell = [tableView dequeueReusableCellWithIdentifier:@"hot"];
            if (!hotCell) {
                hotCell = [[HomeHotTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hot"];
            }
            //hotCell.modelArray =
            NSArray *array = self.dataDictionary[@"fast"];
            hotCell.modelArray = array;
            
            hotCell.matchTypeList = self.cornerModel.matchTypeList;
            hotCell.selectionStyle = UITableViewCellSelectionStyleNone;
            hotCell.matchBlock = ^(HomeMatchTypeListModel * _Nonnull model) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Notice_Match" object:nil userInfo:@{@"content":model}];
            };
            return hotCell;
        }
        else {
            // 排行榜
            HomeRankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rank"];
            if (!cell) {
                cell = [[HomeRankTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"rank"];
            }

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
    }else {
        //明星教练
        CoachWithCourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"starCoach"];
        
        if (!cell) {
            cell = [[CoachWithCourseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"starCoach"];
        }
        cell.model = self.coachDataArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        kWeakSelf(self);
        cell.followBlock = ^(CoachWithCourseModel *model) {
          //关注
            [weakself showSVP];
            [RequestManager addFollowsMappingWithId:[[UserInfoManager shared] getUserID] followUserId:model.userId withSuccess:^(id  _Nullable response) {
                [weakself dissSVP];
                model.isFollow = YES;
                [weakself.tableV reloadData];
            } withFail:^(NSError * _Nullable error) {
                [weakself dissSVP];
            }];
        };
        
        cell.courseBlock = ^(CourseModel *courseModel) {
          //点击课程 跳转到课程详情
            NSLog(@"课程");
            CourseDetailsViewController *vc = [[CourseDetailsViewController alloc]init];
            vc.model = courseModel;
            [self.navigationController pushViewController:vc animated:YES];
        };
        return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return kScaleSize(600)+40;
        }
        return kScaleSize(96);
    }else{
        CoachWithCourseModel *model = self.coachDataArray[indexPath.row];
        return model.height;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        UIView *headv = [self sectionHeaderViewWith:@"快速匹配" withButton:@"更多" isJudge:1];
        return headv;
    }else {
        UIView *headv = [self sectionHeaderViewWith:@"明星教练" withButton:@"更多" isJudge:2];
        
        UIView *line = [UIView new];
        line.backgroundColor = CustomColor(@"#E7E7E7");
        [headv addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(1);
                    make.left.right.bottom.mas_offset(0);
        }];
        return headv;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            // 排行榜
            RankingViewController *vc = [[RankingViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }

    }else{
        // 教练详情
        CoachWithCourseModel *model = [[CoachWithCourseModel alloc]init];
        model = self.coachDataArray[indexPath.row];
        CoachDetailsViewController *vc = [[CoachDetailsViewController alloc]init];
        vc.courseModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}


- (void)getData {
    NSArray *nameArr = @[@"小学生英聊",@"中学生英聊",@"高中生英聊",@"大学生英聊",@"职场英聊",@"社交英聊"];
    NSArray *imageArr = @[@"icon_home_primary",@"icon_home_middle",@"icon_home_high",@"icon_home_univ",@"icon_home_work",@"icon_home_social"];
    NSMutableArray *muArr = [NSMutableArray array];
    
    for (int i = 0; i < 6; i ++) {
        HomeCollectionCellModel *model = [[HomeCollectionCellModel alloc] init];
        model.name = nameArr[i];
        model.popular = 112;
        model.mainImage = imageArr[i];
        [muArr addObject:model];
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    param[@"fast"] = muArr;
    
    NSMutableArray *roomArr = [[NSMutableArray alloc]init];
    for (int i =0; i <5; i ++) {
        NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
        muDic[@"name"] = @"英语狂欢厅";
        muDic[@"subname"] = @"开黑英聊";
        muDic[@"content"] =@"一起玩转英语聊天";
        [roomArr addObject:muDic];
    }
    
    param[@"room"] = roomArr;
    self.dataDictionary = param;
    [self.tableV reloadData];

    [self getDataSource];
}
- (void)getDataSource {
    
    kWeakSelf(self)
    [RequestManager englishIndexWithSuccess:^(id  _Nullable response) {
        weakself.cornerModel = [HomeCornerModel mj_objectWithKeyValues:response[@"data"]];
        NSLog(@"response==%@",response);
        [RequestManager starCoachWithUserId:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
            NSLog(@"star coach== %@",response);
            [self.coachDataArray removeAllObjects];
            NSArray *array = response[@"data"];
            for (int i = 0; i < array.count; i ++ ) {
                CoachWithCourseModel *model = [CoachWithCourseModel mj_objectWithKeyValues:array[i]];
                [self.coachDataArray addObject:model];
            }
            [weakself refresh];
            [weakself stopLoading];

        } withFail:^(NSError * _Nullable error) {
            
        }];
        
    } withFail:^(NSError * _Nullable error) {
        [weakself stopLoading];
        [weakself showText:error.localizedDescription];
    }];

    
}

- (void)refresh {
    //轮播图
    NSMutableArray *images = [NSMutableArray array];
    for (HomeImageListModel *model in self.cornerModel.imageList) {
        [images addObject:model.imageUrl];
    }
    self.cycleView.localizationImageNamesGroup = images;
    //快速匹配  +  明星教练
    //快速匹配
    [self.tableV reloadData];
}
- (void)stopLoading {
    if (self.tableV.mj_header.isRefreshing) {
        [self.tableV.mj_header endRefreshing];
    }
}
- (NSMutableArray *)coachDataArray{
    if (!_coachDataArray) {
        _coachDataArray = [NSMutableArray array];
    }
    return _coachDataArray;
}
@end
