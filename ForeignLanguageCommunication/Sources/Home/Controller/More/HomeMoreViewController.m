//
//  HomeMoreViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/6.
//

#import "HomeMoreViewController.h"
#import "HomeCollectionViewCell.h"

@interface HomeMoreViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation HomeMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navTitleString = @"更多";
    self.navBottomLine.hidden = NO;
    [self setupSubview];
    [self getData];
}

- (void)setupSubview {
    [self.view addSubview:self.collectionView];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.view.backgroundColor = UIColor.whiteColor;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_offset(0);
        make.top.mas_offset(SNavBarHeight);
        make.bottom.mas_offset(0);
    }];
    
    [self.collectionView registerClass:[HomeCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.collectionView);
    }];
    
}
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
           
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;//垂直滚动
        
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = KSW - 30 - kScaleSize(166)*2;
        layout.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
        layout.itemSize = CGSizeMake(kScaleSize(166), kScaleSize(200));
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = UIColor.whiteColor;
    }
    return _collectionView;
}



- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
//    cell.model = self.dataArray[indexPath.item];
    HomeMatchTypeListModel *model = self.dataArray[indexPath.item];
    cell.nameLabel.text = model.name;
    cell.popularLabel.text = [NSString stringWithFormat:@"%ld人在线匹配", model.virtualNum];
    [cell.mainImageView sd_setImageWithURL:model.image.mj_url];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"点击更多");
    HomeMatchTypeListModel *model = self.dataArray[indexPath.item];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notice_Match" object:nil userInfo:@{@"content":model}];
}

- (void)getData {
//    //获取数据
//    NSArray *nameArr = @[@"小学生英聊",@"中学生英聊",@"高中生英聊",@"大学生英聊",@"职场英聊",@"社交英聊"];
//    NSArray *imageArr = @[@"icon_home_primary",@"icon_home_middle",@"icon_home_high",@"icon_home_univ",@"icon_home_work",@"icon_home_social"];
//
//
//    for (int i = 0; i < 6; i ++) {
//        HomeCollectionCellModel *model = [[HomeCollectionCellModel alloc] init];
//        model.name = nameArr[i];
//        model.popular = 112;
//        model.mainImage = imageArr[i];
//        [self.dataArray addObject:model];
//    }
//
//    [self.mainCollectionView reloadData];
    kWeakSelf(self)
    if (self.index == 0) {
        [RequestManager matchWithSuccess:^(id  _Nullable response) {
            weakself.dataArray = [HomeMatchTypeListModel mj_objectArrayWithKeyValuesArray:response[@"data"]];
            weakself.nullView.hidden = weakself.dataArray.count;
            [weakself.collectionView reloadData];
        } withFail:^(NSError * _Nullable error) {
            [weakself showText:error.localizedDescription];
        }];
    } else {
        [RequestManager matchMinWithSuccess:^(id  _Nullable response) {
            weakself.dataArray = [HomeMatchTypeListModel mj_objectArrayWithKeyValuesArray:response[@"data"]];
            weakself.nullView.hidden = weakself.dataArray.count;
            [weakself.collectionView reloadData];
        } withFail:^(NSError * _Nullable error) {
            [weakself showText:error.localizedDescription];
        }];
    }
}

@end
