//
//  HomeMoreViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/6.
//更多房间

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeMoreViewController : BaseViewController
@property (nonatomic, assign) int index; // 0: 英语角的更多。1: 小语种的更多
@end

NS_ASSUME_NONNULL_END
