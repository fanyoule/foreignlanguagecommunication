//
//  SearchRankViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/8.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchRankViewController : BaseViewController
@property (nonatomic, strong) NSMutableArray *dataArr;
@end

NS_ASSUME_NONNULL_END
