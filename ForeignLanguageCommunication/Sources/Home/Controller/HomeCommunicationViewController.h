//
//  HomeCommunicationViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/6.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCommunicationViewController : BaseViewController<JXCategoryViewDelegate,JXPagerViewDelegate,JXPagerMainTableViewGestureDelegate>

@property (nonatomic, strong) JXPagerView *pagerView;
@property (nonatomic, strong) UIView *tableHeadView;
@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;
@property (nonatomic, strong) JXCategoryIndicatorLineView *indicatorLine;
- (JXPagerView *)preferPagerView;

@end

NS_ASSUME_NONNULL_END
