//
//  HomeCommunicationViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/6.
//

#import "HomeCommunicationViewController.h"
#import "HomeFindFriendViewController.h"
#import "HomeCommunicationHeadView.h"
#import "RankingViewController.h"
#import "HomeCornerModel.h"
#import "HomeWebViewController.h"
#import "HomeMoreViewController.h"

@interface HomeCommunicationViewController ()
@property (nonatomic, strong) HomeCommunicationHeadView *headView;

@property (nonatomic, strong) HomeCornerModel *cornerModel;
@end

@implementation HomeCommunicationViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    self.pagerView = [self preferPagerView];
    self.pagerView.mainTableView.gestureDelegate = self;
    [self.view addSubview:self.pagerView];
    kWeakSelf(self);
    
//    self.categoryTitleView.listContainer = (id<JXCategoryViewListContainer>)self.pagerView.listContainerView;
    self.pagerView.mainTableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
//        NSArray *titleArray = @[@"推荐",@"校园交友",@"扩展交友",@"户外交友"];
//        self.categoryTitleView.titles = titleArray;
//        self.categoryTitleView.defaultSelectedIndex = 0;
//        [self.categoryTitleView reloadData];
//        [self.pagerView reloadData];
//        [weakself.pagerView.mainTableView.mj_header endRefreshing];
        [weakself getData];
    }];
    [self getData];
}


- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.pagerView.frame = CGRectMake(0, 0, KSW,ScreenHeight - SNavBarHeight-TabbarHeight-5);
}
- (JXPagerView *)preferPagerView {
    
    return [[JXPagerView alloc] initWithDelegate:self];
}

- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView{
    return self.headView;
    
}
- (HomeCommunicationHeadView *)headView{
    if (!_headView) {
        _headView = [[HomeCommunicationHeadView alloc] initWithFrame:CGRectMake(0, 0, KSW, 15+kScaleSize(136)+15+kScaleSize(60)+10)];
        
        kWeakSelf(self)
        _headView.ranBlock = ^{
            if (![UserInfoManager shared].isLogin) {
                [weakself alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
                    if (index == 1) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
                    }
                }];
                return;
            }
            NSLog(@"rank");
            RankingViewController *vc = [[RankingViewController alloc] init];
            [weakself.navigationController pushViewController:vc animated:YES];
        };
        //点击banner
        _headView.bannerBlock = ^(NSInteger index) {
            NSLog(@"banner");
            [weakself bannerClick:index];
        };
    }
    return _headView;
}
- (void)bannerClick:(NSInteger)index {
    HomeImageListModel *model = self.cornerModel.imageList[index];
    NSLog(@"点击banner");
    if (![UserInfoManager shared].isLogin) {
        [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            if (index == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
            }
        }];
        return;
    }
    if (model.isH5 && model.h5Url) {
        HomeWebViewController *vc = [[HomeWebViewController alloc] init];
        vc.url = model.h5Url;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView{
    return  15+kScaleSize(136)+15+kScaleSize(96)+10;
}
- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView{
    return 50;
}
- (NSInteger)numberOfListsInPagerView:(JXPagerView *)pagerView{
    return 1;
    //self.categoryTitleView.titles.count;
}
- (id<JXPagerViewListViewDelegate>)pagerView:(JXPagerView *)pagerView initListAtIndex:(NSInteger)index{
    HomeFindFriendViewController *collectionVC = [[HomeFindFriendViewController alloc] init];
    //设置数据源
    //collectionVC.modelArray = ;
    collectionVC.dataSource = self.cornerModel.matchTypeList;
    return collectionVC;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
//    UIView *view = [UIView new];
//    view.frame = CGRectMake(0, 0, KSW, 50);
//    UILabel *label = [UILabel new];
//    label.text = @"语种匹配";
//    label.textColor = CustomColor(@"#222222");
//    label.font = kFont_Bold(19);
//    [view addSubview:label];
//    [label mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.mas_offset(0);
//        make.left.mas_offset(15);
//    }];
//    return view;
    UIView *headv = [self sectionHeaderViewWith:@"语种匹配" withButton:@"更多"];
    return headv;
}
- (UIView *)sectionHeaderViewWith:(NSString *)title withButton:(NSString * _Nullable)buttonString {
    UIView *headView = [UIView new];
    headView.backgroundColor = UIColor.whiteColor;
    UILabel *titleLabel = [UILabel new];
    [headView addSubview:titleLabel];
    titleLabel.textColor = CustomColor(@"#222222");
    titleLabel.font = kFont_Bold(19);
    titleLabel.text = title;
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.centerY.mas_offset(0);
    }];
    if (buttonString) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [headView addSubview:btn];
        [btn setTitle:buttonString forState:UIControlStateNormal];
        [btn setTitleColor:RGBA(52, 120, 245, 1) forState:UIControlStateNormal];
        btn.titleLabel.font = kFont(12);
        [btn setBackgroundColor:CustomColor(@"#FCFCFC")];
        
        btn.layer.cornerRadius = 13.5;
        btn.layer.borderWidth = 1;
        btn.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
        
        [btn sizeToFit];
        CGSize size = btn.size;
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_offset(0);
            make.height.mas_equalTo(27);
            make.width.mas_equalTo(size.width+16);
            make.right.mas_offset(-15);
        }];
        [btn whenTapped:^{
            [self clickSectionBtn];
        }];
        [btn addTarget:self action:@selector(clickSectionBtn) forControlEvents:UIControlEventTouchUpInside];
    }
    return headView;
}
- (void)clickSectionBtn {
    HomeMoreViewController *morevc = [[HomeMoreViewController alloc] init];
    morevc.index = 1;
    [self.navigationController pushViewController:morevc animated:YES];
}

- (JXCategoryTitleView *)categoryTitleView {
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titleColor = CustomColor(@"#666666");
        _categoryTitleView.titleSelectedColor = CustomColor(@"#181818");
        
        _categoryTitleView.titleFont = kFont_Bold(13);
        _categoryTitleView.titleSelectedFont = kFont_Bold(16);
        
        _categoryTitleView.indicators = @[self.indicatorLine];
        _categoryTitleView.delegate = self;

    }
    return _categoryTitleView;
}

- (JXCategoryIndicatorLineView *)indicatorLine{
    if (!_indicatorLine) {
        _indicatorLine = [[JXCategoryIndicatorLineView alloc] init];
        _indicatorLine.lineStyle = JXCategoryIndicatorLineStyle_Normal;
        _indicatorLine.indicatorWidth = 15;
        _indicatorLine.indicatorHeight = 2;
        _indicatorLine.indicatorColor = CustomColor(@"#3478F5");
    }
    return _indicatorLine;
}

- (void)getData {
    kWeakSelf(self)
    [RequestManager momentsIndexWithSuccess:^(id  _Nullable response) {
        weakself.cornerModel = [HomeCornerModel mj_objectWithKeyValues:response[@"data"]];
        NSLog(@"***");
        [weakself stopLoading];
        [weakself.pagerView reloadData];
        
        NSMutableArray *images = [NSMutableArray array];
        for (HomeImageListModel *model in weakself.cornerModel.imageList) {
            [images addObject:model.imageUrl];
        }
        weakself.headView.avatarArray = images;
    } withFail:^(NSError * _Nullable error) {
        [weakself stopLoading];
        [weakself showText:error.localizedDescription];
    }];
}
- (void)stopLoading {
    if (self.pagerView.mainTableView.mj_header.isRefreshing) {
        [self.pagerView.mainTableView.mj_header endRefreshing];
    }
}
@end
