//
//  HomeFindFriendViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeFindFriendViewController : BaseViewController <JXPagerViewListViewDelegate>
@property (nonatomic, strong) NSArray *modelArray;
@property (nonatomic, strong) NSMutableArray *dataSource;
//- (void)reloadData;
@end

NS_ASSUME_NONNULL_END
