//
//  HomeSearchCoachViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//搜索教练

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeSearchCoachViewController : BaseViewController <JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
