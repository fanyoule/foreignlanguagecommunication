//
//  HomeSearchRoomViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//搜索房间

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeSearchRoomViewController : BaseViewController <JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
