//
//  HomeSearchUserViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//搜索用户

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeSearchUserViewController : BaseViewController<JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
