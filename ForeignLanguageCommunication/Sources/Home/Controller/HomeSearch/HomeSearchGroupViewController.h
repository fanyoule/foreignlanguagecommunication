//
//  HomeSearchGroupViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//搜索群组

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeSearchGroupViewController : BaseViewController <JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
