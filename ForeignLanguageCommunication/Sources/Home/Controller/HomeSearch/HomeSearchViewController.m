//
//  HomeSearchViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import "HomeSearchViewController.h"

#import "YKSearchBar.h"
#import "HomeSearchRoomViewController.h"
#import "HomeSearchUserViewController.h"
#import "HomeSearchCoachViewController.h"
#import "HomeSearchGroupViewController.h"

@interface HomeSearchViewController ()<JXCategoryListContainerViewDelegate,JXCategoryViewDelegate,YKSearchBarDelegate>
@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;
@property (nonatomic, strong) JXCategoryIndicatorLineView *indicatorLineView;
@property (nonatomic, strong) JXCategoryListContainerView *containerView;

@property (nonatomic, strong) YKSearchBar *searchBarView;

@property (nonatomic, strong) NSMutableArray *childVCArray;
@end

@implementation HomeSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)setupSubview {
    self.navView.hidden = NO;
    self.backBtn.hidden = YES;
    [self.navView addSubview:self.searchBarView];
    [self.searchBarView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_offset(0);
            make.bottom.mas_offset(-4);
            make.height.mas_equalTo(36);
    }];
    
    [self.view addSubview:self.categoryTitleView];
    [self.view addSubview:self.containerView];
}
- (JXCategoryListContainerView *)containerView {
    if (!_containerView) {
        _containerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    }
    return _containerView;
}

- (YKSearchBar *)searchBarView {
    if (!_searchBarView) {
        _searchBarView = [[YKSearchBar alloc] init];
        _searchBarView.searchTextView.placeHolder = @"搜索房间/ID/交流内容";
        _searchBarView.searchTextView.font = kFont_Medium(14);
        _searchBarView.searchTextView.placeHolderColor = CustomColor(@"#999999");
        _searchBarView.searchTextView.searchIcon = @"icon_home_search_black";
        _searchBarView.searchTextView.leftMargin = 15;
        _searchBarView.searchTextView.placeHolderPadding = 5;
        _searchBarView.searchTextView.textColor = CustomColor(@"#181818");
        _searchBarView.searchTextView.tintColor = CustomColor(@"#3478F5");
        _searchBarView.searchTextView.placeHolderPosition = SearchViewPlaceHolderPositionDefault;
        
        _searchBarView.searchTextView.layer.cornerRadius = 18;
        _searchBarView.searchTextView.backgroundColor = CustomColor(@"#E5E5E7");
        _searchBarView.alwaysShowFinish = YES;
        [_searchBarView.finishButton setTitle:@"取消" forState:UIControlStateNormal];
        [_searchBarView.finishButton setTitleColor:CustomColor(@"#3478F5") forState:UIControlStateNormal];
        _searchBarView.finishButton.titleLabel.font = kFont_Medium(14);
        _searchBarView.margin = UIEdgeInsetsMake(0, 15, 0, 15);
        _searchBarView.paddingToButton = 15;
        _searchBarView.delegate = self;
    }
    return _searchBarView;
}

- (void)finishSearch:(UITextField *)textField {
    //结束搜索
    //返回
}
- (void)searchBarDidEndEdit:(UITextField *)textField{
    //输入结束 开始搜索
    
}


- (JXCategoryTitleView *)categoryTitleView {
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titles = @[@"房间",@"教练",@"用户",@"群组"];
        _categoryTitleView.titleFont = kFont_Medium(16);
        _categoryTitleView.titleColor = CustomColor(@"#666666");
        _categoryTitleView.titleSelectedColor = CustomColor(@"#3478F5");
        _categoryTitleView.indicators = @[self.indicatorLineView];
        _categoryTitleView.listContainer = self.containerView;
    }
    return _categoryTitleView;
}

- (JXCategoryIndicatorLineView *)indicatorLineView {
    if (!_indicatorLineView) {
        _indicatorLineView = [[JXCategoryIndicatorLineView alloc] init];
        _indicatorLineView.indicatorHeight = 2;
        _indicatorLineView.indicatorWidth = 15;
        _indicatorLineView.indicatorColor = CustomColor(@"#3478F5");
    }
    return _indicatorLineView;
}


- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView{
    return self.categoryTitleView.titles.count;
}

- (NSMutableArray *)childVCArray{
    if (!_childVCArray) {
        _childVCArray = [NSMutableArray array];
        HomeSearchRoomViewController *roomVC = [[HomeSearchRoomViewController alloc] init];
        HomeSearchUserViewController *userVC = [[HomeSearchUserViewController alloc] init];
        HomeSearchCoachViewController *coachVC = [[HomeSearchCoachViewController alloc] init];
        HomeSearchGroupViewController *groupVC = [[HomeSearchGroupViewController alloc] init];
        [_childVCArray addObject:roomVC];
        [_childVCArray addObject:userVC];
        [_childVCArray addObject:coachVC];
        [_childVCArray addObject:groupVC];
    }
    return _childVCArray;
}
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    //初始化subVC
    BaseViewController *vc = self.childVCArray[index];
    return  vc;
}



@end
