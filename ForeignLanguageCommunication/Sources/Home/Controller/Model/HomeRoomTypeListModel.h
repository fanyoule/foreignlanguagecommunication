//
//  HomeRoomTypeListModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/22.
//

#import <Foundation/Foundation.h>
#import "HomeRoomModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeRoomTypeListModel : NSObject
@property (nonatomic, strong) NSMutableArray <HomeRoomModel *>*roomList;          // 房间列表
@property (nonatomic) long roomTypeId;                                          // 群聊类型id
@property (nonatomic, copy) NSString *roomTypeName;                             // 群聊类型名称
@end

NS_ASSUME_NONNULL_END
