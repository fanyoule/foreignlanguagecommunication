//
//  MatchRemainCountModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/2/4.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MatchRemainCountModel : NSObject
//"data":{"price":25,"time":100,"vip":true,"remainCount":100}
//价格
@property (nonatomic) NSInteger price;
//次数
@property (nonatomic) NSInteger time;
//VIP
@property (nonatomic) BOOL vip;
//剩余次数
@property (nonatomic) NSInteger remainCount;
@end

NS_ASSUME_NONNULL_END
