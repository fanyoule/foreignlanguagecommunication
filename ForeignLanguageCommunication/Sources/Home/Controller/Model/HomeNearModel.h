//
//  HomeNearModel.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeNearModel : NSObject

@property (nonatomic, copy) NSString *birthday; // 生日
@property (nonatomic, copy) NSString *introduce;// 用户简介
@property (nonatomic, copy) NSString *nickname; // 用户名称
@property (nonatomic, copy) NSString *avatar;   // 用户头像
@property (nonatomic) CGFloat distance;         // 距离:m
@property (nonatomic) BOOL isRealName;          // 是否实名认证：0不是 1是
@property (nonatomic) NSInteger sex;            // 性别 0：女 1：男
@property (nonatomic) BOOL isVIP;               // 是否是VIP：0不是 1是
@property (nonatomic) NSInteger ID;             // 用户id
@end

NS_ASSUME_NONNULL_END
