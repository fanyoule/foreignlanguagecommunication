//
//  HomeRoomTypeListModel.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/22.
//

#import "HomeRoomTypeListModel.h"

@implementation HomeRoomTypeListModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"roomList": [HomeRoomModel class]};
}
@end
