//
//  HomeImageListModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeImageListModel : NSObject
@property (nonatomic) int belong;                       // 图片所属0首页，1交流圈
@property (nonatomic, copy) NSString *createTime;       // 创建时间
@property (nonatomic, copy) NSString *h5Url;            // H5连接
@property (nonatomic, copy) NSString *imageUrl;         // 图片url
@property (nonatomic) long isH5;                        // 是否是H5: 0是 1不是
@property (nonatomic) long status;                      // 状态（0启用，1禁用）
@end

NS_ASSUME_NONNULL_END
