//
//  HomeCornerModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/22.
//

#import <Foundation/Foundation.h>
#import "HomeChartsTopModel.h"
#import "HomeImageListModel.h"
#import "HomeMatchTypeListModel.h"
#import "HomeRoomTypeListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCornerModel : NSObject
@property (nonatomic, strong) NSMutableArray <HomeChartsTopModel *>*chartsTop;          // 排行榜前三名 首页用不到
@property (nonatomic, strong) NSMutableArray <HomeImageListModel *>*imageList;          // 轮播图列表 需要
/**快速匹配前六个*/
@property (nonatomic, strong) NSMutableArray <HomeMatchTypeListModel *>*matchTypeList;  // 快速匹配类型列表（前6个）需要
@property (nonatomic, strong) NSMutableArray <HomeRoomTypeListModel *>*roomTypeList;    // 房间类型列表 不使用
@end

NS_ASSUME_NONNULL_END
