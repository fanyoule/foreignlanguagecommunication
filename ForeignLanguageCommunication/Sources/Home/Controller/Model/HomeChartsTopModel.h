//
//  HomeChartsTopModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeChartsTopModel : NSObject
@property (nonatomic, copy) NSString *headUrl;          // 头像
@property (nonatomic, copy) NSString *nickName;         // 用户昵称
@property (nonatomic) int sex;                          // 性别：0 女，1 男
@property (nonatomic) long sort;                        // 序值
@property (nonatomic, copy) NSString *sysId;            // 账号
@property (nonatomic) long userId;                      // 用户id
@property (nonatomic) long voteNum;                     // 本月得票数
@property (nonatomic) int voteStatus;                   // 今日是否被查看者投过票0没投，1投过
@end

NS_ASSUME_NONNULL_END
