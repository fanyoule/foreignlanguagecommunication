//
//  HomeMatchTypeListModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeMatchTypeListModel : NSObject
/**创建时间*/
@property (nonatomic, copy) NSString *createTime;       // 创建时间
/**图片*/
@property (nonatomic, copy) NSString *image;            // 图片url
/**名字*/
@property (nonatomic, copy) NSString *name;             // 名称
/**序号*/
@property (nonatomic) long number;                      // 序值
/**虚拟人数*/
@property (nonatomic) long virtualNum;                  // 虚拟人数

@property (nonatomic) NSInteger ID;
@end

NS_ASSUME_NONNULL_END
