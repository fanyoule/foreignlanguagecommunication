//
//  HomeCollectionCellModel.h
//  VoiceLive
//
//  Created by mac on 2020/8/20.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeCollectionCellModel : NSObject

@property (nonatomic, copy) NSString *mainImage;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, assign) NSInteger popular;
//显示 交友 扩列等标签
@property (nonatomic, assign) BOOL showChat;
@property (nonatomic) BOOL showTag;
@property (nonatomic) NSInteger tagType;//交友 扩列
@end

NS_ASSUME_NONNULL_END
