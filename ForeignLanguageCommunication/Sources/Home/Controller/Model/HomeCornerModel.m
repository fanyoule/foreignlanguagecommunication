//
//  HomeCornerModel.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/22.
//

#import "HomeCornerModel.h"

@implementation HomeCornerModel
+ (NSDictionary *)mj_objectClassInArray {
    return @{@"chartsTop": [HomeChartsTopModel class], @"imageList": [HomeImageListModel class], @"matchTypeList": [HomeMatchTypeListModel class], @"roomTypeList": [HomeRoomTypeListModel class]};
}
@end
