//
//  HomeRoomModel.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeRoomModel : NSObject
@property (nonatomic, copy) NSString *masterHeadUrl;        // 房主头像
@property (nonatomic) long masterId;                        // 房主id
@property (nonatomic, copy) NSString *masterName;           // 房主昵称
@property (nonatomic) long onlineNum;                       // 在线人数
@property (nonatomic) int recommendType;                    // 推荐类型：0不推荐，1外语角推荐，2交流圈推荐
@property (nonatomic, copy) NSString *romPicUrl;            // 房间头像
@property (nonatomic) long roomId;                          // 房间id
@property (nonatomic, copy) NSString *roomName;             // 房间名称
@property (nonatomic, copy) NSString *roomNum;              // 房间号
@property (nonatomic, copy) NSString *thisTopic;            // 本期话题
@property (nonatomic, copy) NSString *thisTopicContent;     // 本期话题内容
@property (nonatomic) long typeId;                          // 房间类型id
@property (nonatomic, copy) NSString *typeName;             // 房间类型名称
@property (nonatomic, copy) NSString *welcome;              // 房间欢迎语
@end

NS_ASSUME_NONNULL_END
