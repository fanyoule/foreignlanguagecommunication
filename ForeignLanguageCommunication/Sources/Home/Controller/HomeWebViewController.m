//
//  HomeWebViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/2.
//

#import "HomeWebViewController.h"
#import <WebKit/WebKit.h>
@interface HomeWebViewController ()<WKUIDelegate>

@end

@implementation HomeWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.titleStr) {
        self.navTitleString = self.titleStr;
    }
    
    self.navView.backgroundColor = [UIColor whiteColor];
    // 初始化配置对象
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    // 默认是NO，这个值决定了用内嵌HTML5播放视频还是用本地的全屏控制
    configuration.allowsInlineMediaPlayback = YES;
    // 自动播放, 不需要用户采取任何手势开启播放
    if (@available(iOS 10.0, *)) {
      // WKAudiovisualMediaTypeNone 音视频的播放不需要用户手势触发, 即为自动播放
        configuration.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypeAudio;
    } else {
        configuration.mediaTypesRequiringUserActionForPlayback = NO;
    }
    WKWebView *webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, SNavBarHeight, APPSIZE.width, APPSIZE.height-SNavBarHeight-TabbarSafeMargin) configuration:configuration];
    webView.UIDelegate =self;
    webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    webView.contentMode = UIViewContentModeRedraw;
    webView.opaque = YES;
    [self.view addSubview:webView];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    // Do any additional setup after loading the view.
}
@end
