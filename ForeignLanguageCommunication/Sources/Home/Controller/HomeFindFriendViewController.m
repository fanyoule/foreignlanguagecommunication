//
//  HomeFindFriendViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import "HomeFindFriendViewController.h"
#import "HomeFindFriendCollectionViewCell.h"
#import "HomeCollectionViewCell.h"
#import "HomeMatchTypeListModel.h"
#import "CourseDetailsViewController.h"

@interface HomeFindFriendViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@end

@implementation HomeFindFriendViewController
- (UIScrollView *)getMyScrollView {
    return self.mainCollectionView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.dataSource = [NSMutableArray array];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColor.whiteColor;
    [self setupSubView];
    [self getData];
//    [self reloadData];
}
//- (void)reloadData {
//    [self getDataArr];
//}
- (void)setupSubView {
    [self.view addSubview:self.mainCollectionView];
    self.mainCollectionView.backgroundColor = UIColor.whiteColor;
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
//    layout.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
//    layout.minimumLineSpacing = 10;
//    layout.minimumInteritemSpacing = 10;
//    CGFloat w = (KSW - 30 -10) / 2;
//    layout.itemSize = CGSizeMake(w, w+40);
    
    
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = KSW - 30 - kScaleSize(166)*2;
    layout.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
    layout.itemSize = CGSizeMake(kScaleSize(166), kScaleSize(200));
    
    
    [self.mainCollectionView setCollectionViewLayout:layout];
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    [self.mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(KSW);
            make.left.top.mas_offset(0);
            make.height.mas_equalTo(self.view.mas_height);
    }];
    
    [self.mainCollectionView registerClass:[HomeCollectionViewCell class] forCellWithReuseIdentifier:@"friend"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"friend" forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.item];
    if (self.dataSource.count > 0 && indexPath.item <= self.dataSource.count - 1) {
        HomeMatchTypeListModel *m = self.dataSource[indexPath.item];
        cell.popularLabel.text = [NSString stringWithFormat:@"%ld人在线匹配", m.virtualNum];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dataSource.count>indexPath.item) {//安全处理 防止数组越界
        HomeMatchTypeListModel *model = self.dataSource[indexPath.item];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Notice_Match" object:nil userInfo:@{@"content":model}];
        NSLog(@"语种：%@",model.name);
    }
 
}
- (void)setDataSource:(NSMutableArray *)dataSource {
    _dataSource = dataSource;
    
    [self.mainCollectionView reloadData];
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}
- (void)getData {
    NSArray *mainImageArray = @[@"icon_japaness",@"icon_russia",@"icon_german",@"icon_french"];
    NSArray *nameArray = @[@"日语学习",@"俄语学习",@"德语学习",@"法语学习"];
    for (int i = 0; i < 4; i ++) {
        HomeCollectionCellModel *model = [[HomeCollectionCellModel alloc] init];
        model.avatar = @"icon_french";
        model.mainImage = mainImageArray[i];
        model.name = nameArray[i];
        model.popular = 222;
        
        [self.dataArray addObject:model];
        
    }
    [self.mainCollectionView reloadData];
    
}
- (void)setModelArray:(NSArray *)modelArray{
    _modelArray = modelArray;
    self.dataArray = modelArray.copy;
    [self.mainCollectionView reloadData];
}



- (UIView *)listView {
    return self.view;
}
- (UIScrollView *)listScrollView {
    return self.mainCollectionView;
}
- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback{
    self.scrollCallback = callback;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    !self.scrollCallback ?: self.scrollCallback(scrollView);
}

//- (void)getDataArr {
//    kWeakSelf(self)
//    [RequestManager matchMinWithSuccess:^(id  _Nullable response) {
//        weakself.dataSource = [HomeMatchTypeListModel mj_objectArrayWithKeyValuesArray:response[@"data"]];
//        NSLog(@"***");
//        [weakself.mainCollectionView reloadData];
//    } withFail:^(NSError * _Nullable error) {
//        [weakself showText:error.localizedDescription];
//    }];
//}

@end
