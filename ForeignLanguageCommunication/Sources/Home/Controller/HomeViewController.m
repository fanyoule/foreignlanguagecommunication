//
//  HomeViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/16.
// 外语角 附近人 交流圈

#import "HomeViewController.h"
#import "HomeCornerViewController.h"

#import "JLCardViewController.h"
#import "HomeCommunicationViewController.h"
#import "HomeDragViewController.h"
#import "HomeCollectionCellModel.h"
#import "MatchRemainCountModel.h"
#import "HomeMatchViewController.h"
#import "HomeMatchTypeListModel.h"

@interface HomeViewController ()<JXCategoryViewDelegate,UIScrollViewDelegate>
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;
@property (nonatomic, strong) JXCategoryIndicatorLineView *indicateView;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupSubview];
    [self addSubControllers];
    self.mainScrollView.contentOffset = CGPointMake(0, 0);
    [self scrollViewDidEndScrollingAnimation:self.mainScrollView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMatchNumber:) name:@"Notice_Match" object:nil];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
- (void)setupSubview {
    
    UIImageView *topImageV = [[UIImageView alloc] initWithImage:kImage(@"icon_home_top")];
    [self.view addSubview:topImageV];
    CGFloat h  = kScaleSize(136);
    topImageV.frame = CGRectMake(0, 0, KSW, h);
    
    
    [self.view addSubview:self.mainScrollView];
    [self.mainScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.view);
    }];
    
    self.mainScrollView.backgroundColor = UIColor.clearColor;
    self.mainScrollView.contentSize = CGSizeMake(KSW * 3, 0);
    
    self.mainScrollView.scrollEnabled = NO;
    self.mainScrollView.delegate = self;
    
    self.topView = [UIView new];
    [self.view addSubview:self.topView];
    self.topView.frame = CGRectMake(0, 0, KSW, 55+SBarHeight);
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:kImage(@"icon_home_nav")];
    [self.topView addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.topView);
    }];
    
    [self.topView addSubview:self.categoryTitleView];
    self.categoryTitleView.titles = @[@"英语角",@"附近人",@"小语种"];
    
    [self.categoryTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(35);
        make.bottom.mas_offset(-5);
    }];
    [self.view bringSubviewToFront:self.topView];
}

- (void)addSubControllers {
    
    HomeCornerViewController *cornerVC = [[HomeCornerViewController alloc] init];
    
    [self addChildViewController:cornerVC];
    
//    JLCardViewController *nearVC = [[JLCardViewController alloc] init];
//    [self addChildViewController:nearVC];
    
    HomeDragViewController *nearVC = [[HomeDragViewController alloc] init];
    [self addChildViewController:nearVC];
    
    HomeCommunicationViewController *comVC = [[HomeCommunicationViewController alloc] init];
    
//    CommunicationViewController *comVC = [[CommunicationViewController alloc] init];
    [self addChildViewController:comVC];
}


- (JXCategoryTitleView *)categoryTitleView {
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titleColor = CustomColor(@"#EFEFEF");
        _categoryTitleView.titleSelectedColor = CustomColor(@"#EFEFEF");
        _categoryTitleView.titleFont = kFont_Bold(16);
        _categoryTitleView.indicators = @[self.indicateView];
        _categoryTitleView.delegate = self;
        _categoryTitleView.titleSelectedFont = kFont_Bold(23);
        
    }
    return _categoryTitleView;
}

- (JXCategoryIndicatorLineView *)indicateView{
    if (!_indicateView) {
        _indicateView = [[JXCategoryIndicatorLineView alloc] init];
        _indicateView.lineStyle = JXCategoryIndicatorLineStyle_Normal;
        _indicateView.indicatorWidth = 17;
        _indicateView.indicatorHeight = 4;
        
        _indicateView.indicatorColor = CustomColor(@"#EFEFEF");
    }
    return _indicateView;
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    // 选择第几个 滚动到指定位置
    CGFloat x = KSW * index;
    CGPoint point = CGPointMake(x, 0);
    self.mainScrollView.contentOffset = point;
    [self scrollViewDidEndScrollingAnimation:self.mainScrollView];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    NSInteger index = scrollView.contentOffset.x / KSW;
    if (index >= self.childViewControllers.count) {
        return;
    }
    BaseViewController *newVC = self.childViewControllers[index];
    if (!newVC) {
        return;
    }
    if (newVC.isViewLoaded) {
        return;
    }
    newVC.view.frame = CGRectMake(KSW * index, 55+SBarHeight, KSW, self.view.height - 55-SBarHeight);
    [self.mainScrollView addSubview:newVC.view];
    
}
#pragma mark-- 点击 某个语种
- (void)getMatchNumber:(NSNotification *)notice {
    if (![UserInfoManager shared].isLogin) {
        [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            if (index == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
            }
        }];
        return;
    }
     HomeMatchTypeListModel *model = notice.userInfo[@"content"];
    
    [RequestManager remainMatchCount:[[UserInfoManager shared] getUserID] withSuccess:^(id  _Nullable response) {
        NSLog(@"respons==%@",response);
        
        //"data":{"price":25,"time":100,"vip":true,"remainCount":100}
        MatchRemainCountModel *countModel = [MatchRemainCountModel mj_objectWithKeyValues:response[@"data"]];
        //如果剩余次数大于0 去匹配
        if (countModel.remainCount >0) {
            HomeMatchViewController *matchVC = [[HomeMatchViewController alloc] init];
            matchVC.countModel = countModel;
            matchVC.matchModel = model;
            [self.navigationController pushViewController:matchVC animated:YES];
        }else{
            [self showText:@"匹配次数不足"];
        }
        
    } withFail:^(NSError * _Nullable error) {
        [self showText:error.localizedDescription];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
