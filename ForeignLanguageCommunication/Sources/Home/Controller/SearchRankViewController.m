//
//  SearchRankViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/8.
//

#import "SearchRankViewController.h"
#import "RankingTableViewCell.h"

@interface SearchRankViewController ()<UITableViewDelegate, UITableViewDataSource, RankingTableViewCellDelegate, UISearchBarDelegate> {
    BOOL isSearch;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) NSMutableArray *searchArr;
@end

@implementation SearchRankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isSearch = NO;
//    self.dataArr = [NSMutableArray array];
    // Do any additional setup after loading the view from its nib.
    self.topConstraint.constant = SNavBarHeight;
    [self.view layoutIfNeeded];
    
    [self setUpUI];
}
- (void)setUpUI {
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"排行榜";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = kBackgroundColor;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.sectionIndexColor = kColor(@"#181818");
    self.tableView.sectionIndexBackgroundColor = UIColor.clearColor;
    self.tableView.sectionIndexTrackingBackgroundColor = UIColor.clearColor;
    [self.tableView registerNib:[UINib nibWithNibName:[RankingTableViewCell toString] bundle:nil] forCellReuseIdentifier:@"RankingTableViewCell_search"];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60)];
    self.searchBar.placeholder = @"搜索";
    self.tableView.tableHeaderView = self.searchBar;
    self.searchBar.delegate = self;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchArr = [NSMutableArray array];
    if (searchText && searchText.length > 0) {
        isSearch = YES;
        for (RankingModel *model in self.dataArr) {
            if ([model.nickName containsString:searchText]) {
                [self.searchArr addObject:model];
            }
        }
    } else {
        isSearch = NO;
    }
    [self.tableView reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.001;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isSearch) {
        return self.searchArr.count;
    } else {
        return self.dataArr.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RankingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RankingTableViewCell_search"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (isSearch) {
        cell.model = self.searchArr[indexPath.row];
    } else {
        cell.model = self.dataArr[indexPath.row];
    }
    
    cell.index = indexPath.row;
    cell.delegate = self;
    return cell;
}
- (void)rankingTicketIndex:(NSInteger)index {
    RankingModel *model = isSearch ? self.searchArr[index] : self.dataArr[index];
    if (model.voteStatus == 1) {
        [self showText:@"已投过票"];
    } else {
        kWeakSelf(self)
        [self showSVP];
        [RequestManager voteWithUserId:[UserInfoManager shared].getUserID toUserId:[model.userId longValue] withSuccess:^(id  _Nullable response) {
            [weakself dissSVP];
            [weakself showText:@"投票成功"];
            model.voteStatus = 1;
            model.voteNum = [NSString stringWithFormat:@"%d", [model.voteNum intValue]+1];
            [self.tableView reloadData];
        } withFail:^(NSError * _Nullable error) {
            [weakself dissSVP];
            [weakself showText:error.localizedDescription];
        }];
    }
}
@end
