//
//  RankingViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/14.
//

#import "RankingViewController.h"
#import "RankingTableViewCell.h"
#import "ShareThePopupWindow.h"
#import "SearchRankViewController.h"

@interface RankingViewController ()
<
UITableViewDelegate,
UITableViewDataSource,
RankingTableViewCellDelegate
>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintH;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *rankNumLab;
@property (weak, nonatomic) IBOutlet UILabel *ticketNumLab;

@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation RankingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.topConstraint.constant = SNavBarHeight + 40;
    [self.view layoutIfNeeded];
    
    [self setUpUI];
}
- (void)share {
    ShareModel *model = [[ShareModel alloc] init];
    model.image = @"";
    model.title = @"排行榜";
    model.content = @"排行榜";
    ShareThePopupWindow *vc = [ShareThePopupWindow new];
    vc.model = model;
    [vc popupWithPopType:HWPopTypeShrinkIn dismissType: HWDismissTypeNone];
}
- (void)search {
    SearchRankViewController *vc = [[SearchRankViewController alloc] init];
    vc.dataArr = self.dataArr;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)setUpUI {
    self.navView.backgroundColor = UIColor.whiteColor;
    self.navTitleString = @"排行榜";
    
    UIButton *r1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [r1 setImage:kImage(@"rank_share") forState:UIControlStateNormal];
    [r1 addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:r1];
    [r1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_offset(0);
        make.top.mas_offset(SBarHeight);
        make.width.height.mas_equalTo(44);
        make.right.mas_equalTo(-8);
    }];
    
    UIButton *r2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [r2 setImage:kImage(@"rank_search") forState:UIControlStateNormal];
    [r2 addTarget:self action:@selector(search) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:r2];
    [r2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_offset(0);
        make.top.mas_offset(SBarHeight);
        make.width.height.mas_equalTo(44);
        make.right.equalTo(r1.mas_left).offset(-2);
    }];
    
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, SNavBarHeight, KSW, 40)];
    bg.backgroundColor = kBackgroundColor;
    [self.view addSubview:bg];
    
    UILabel *lab = [[UILabel alloc] init];
    lab.font = kFont(12);
    lab.textColor = kColor(@"#B4B4B4");
    lab.text = @"每人每天有3次投票机会";
    [bg addSubview:lab];
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(bg);
        make.centerX.mas_equalTo(bg).offset(-15);
    }];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:kImage(@"个人资料-已实名") forState:UIControlStateNormal];
    [bg addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lab.mas_right);
        make.centerY.equalTo(lab.mas_centerY);
        make.height.width.equalTo(@25);
    }];
    
    self.icon.layer.cornerRadius = 6;
    self.icon.layer.masksToBounds = YES;
    self.icon.contentMode = UIViewContentModeScaleAspectFill;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = kBackgroundColor;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.sectionIndexColor = kColor(@"#181818");
    self.tableView.sectionIndexBackgroundColor = UIColor.clearColor;
    self.tableView.sectionIndexTrackingBackgroundColor = UIColor.clearColor;
    [self.tableView registerNib:[UINib nibWithNibName:[RankingTableViewCell toString] bundle:nil] forCellReuseIdentifier:[RankingTableViewCell toString]];
    
    [self getMyCharts];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getData];
}
- (void)getData {
    [self showSVP];
    kWeakSelf(self)
    [RequestManager chartsUserId:[UserInfoManager shared].getUserID userName:[UserInfoManager shared].getUserNickName withSuccess:^(id  _Nullable response) {
        NSLog(@"排行榜 === %@",response);
        [weakself dissSVP];
        
        weakself.dataArr = [RankingModel mj_objectArrayWithKeyValuesArray:response[@"data"]];
        [weakself.tableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself dissSVP];
        [weakself showText:error.localizedDescription];
    }];
}
- (void)getMyCharts {
    kWeakSelf(self)
    [RequestManager chartsSelfUserId:[UserInfoManager shared].getUserID withSuccess:^(id  _Nullable response) {
        NSLog(@"排行榜 === %@",response);
        RankingModel *model = [RankingModel mj_objectWithKeyValues:response[@"data"]];
        [weakself.icon sd_setImageWithURL:[NSURL URLWithString:model.headUrl] placeholderImage:nil];
        weakself.rankNumLab.text = [NSString intStrong:model.sort];
        weakself.ticketNumLab.text = model.voteNum;
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.001;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RankingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[RankingTableViewCell toString]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.dataArr[indexPath.row];
    cell.index = indexPath.row;
    cell.delegate = self;
    return cell;
}
- (void)rankingTicketIndex:(NSInteger)index {
    RankingModel *model = self.dataArr[index];
    if (model.voteStatus == 1) {
        [self showText:@"已投过票"];
    } else {
        kWeakSelf(self)
        [self showSVP];
        [RequestManager voteWithUserId:[UserInfoManager shared].getUserID toUserId:[model.userId longValue] withSuccess:^(id  _Nullable response) {
            [weakself dissSVP];
            [weakself showText:@"过票成功"];
            model.voteStatus = 1;
            model.voteNum = [NSString stringWithFormat:@"%d", [model.voteNum intValue]+1];
            [self.tableView reloadData];
        } withFail:^(NSError * _Nullable error) {
            [weakself dissSVP];
            [weakself showText:error.localizedDescription];
        }];
        
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
