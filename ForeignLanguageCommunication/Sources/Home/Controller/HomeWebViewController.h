//
//  HomeWebViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/3/2.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeWebViewController : BaseViewController
@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *url;
@end

NS_ASSUME_NONNULL_END
