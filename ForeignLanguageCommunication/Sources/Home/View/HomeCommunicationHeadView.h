//
//  HomeCommunicationHeadView.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//
//交流圈 头视图 轮播图 排行榜
#import <UIKit/UIKit.h>

typedef void(^clickRankBlock)(void);
typedef void(^clickBannerBlock)(NSInteger index);
NS_ASSUME_NONNULL_BEGIN

@interface HomeCommunicationHeadView : UIView
/**轮播图数组*/
@property (nonatomic, strong) NSMutableArray *avatarArray;
@property (nonatomic, copy) clickRankBlock ranBlock;
@property (nonatomic, copy) clickBannerBlock bannerBlock;
@end

NS_ASSUME_NONNULL_END
