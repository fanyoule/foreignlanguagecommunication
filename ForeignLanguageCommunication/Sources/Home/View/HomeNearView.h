//
//  HomeNearView.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//
#import "HomeNearModel.h"
#import <UIKit/UIKit.h>
@class HomeNearView;
@protocol HomeNearViewDelegate <NSObject>

@optional
- (void)movedWithDistance:(CGFloat)distance;
- (void)movedEndWithBack:(BOOL)isBack withDistance:(CGFloat)distance;
/**滑动中*/
- (void)dragView:(HomeNearView *_Nullable)dragView movedDistance:(CGFloat)distance;
/**滑动结束*/
- (void)dragView:(HomeNearView *_Nullable)dragView moveEndWithBack:(BOOL)isBack distance:(CGFloat)distance;
/**点击*/
- (void)dragViewDidTap:(HomeNearView *)dragView;

@end

NS_ASSUME_NONNULL_BEGIN

@interface HomeNearView : UIView
@property (nonatomic, assign) BOOL canDrag;
@property (nonatomic, weak) id <HomeNearViewDelegate> delegate;

@property (nonatomic) CGAffineTransform originTrans;
@property (nonatomic) CGPoint originCenter;
@property (nonatomic) CGRect originalFrame;

@property (nonatomic) NSInteger index;
@property (nonatomic, strong) HomeNearModel *model;
@end

NS_ASSUME_NONNULL_END
