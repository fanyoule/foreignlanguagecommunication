//
//  HomeNearView.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/15.
//

#import "HomeNearView.h"

@interface HomeNearView ()
@property (nonatomic, strong) UIImageView *mainImageV;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) NSMutableArray *tagsArray;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UIImageView *rzImagv;

@property (nonatomic, strong) UIButton *ageBtn;
@property (nonatomic, strong) UILabel *xingLab;
@property (nonatomic, strong) UILabel *VIPLab;

@property (nonatomic) CGPoint originalPoint;
@end

@implementation HomeNearView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupSubview];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    [Util setCornerModelWithView:self.mainImageV model:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadius:10];
}
- (void)setupSubview {
    self.backgroundColor = UIColor.whiteColor;
//    self.layer.borderColor = UIColor.orangeColor.CGColor;
//    self.layer.borderWidth = 2;
    
    self.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    self.layer.shadowColor = [UIColor colorWithRed:136/255.0 green:136/255.0 blue:136/255.0 alpha:0.19].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, -2);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 5.0;
    self.layer.cornerRadius = 10;
    
    
    [self addSubview:self.mainImageV];
    [self addSubview:self.nameLabel];
    [self addSubview:self.contentLabel];
    [self addSubview:self.addressLabel];
    [self addSubview:self.rzImagv];
    
    [self addSubview:self.ageBtn];
    [self addSubview:self.xingLab];
    [self addSubview:self.VIPLab];
    
//    self.layer.cornerRadius = 10.0;
//    self.clipsToBounds = YES;
//    self.layer.shadowColor = rgba(1.0, 1.0, 1.0, 0.2).CGColor;
//    self.layer.shadowRadius = 5.0;
//    self.layer.shadowOpacity = 1;
    
    UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(beginDrag:)];
    [self addGestureRecognizer:panGes];
    
    [self.mainImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_offset(0);
        make.height.mas_equalTo(self.height*0.8);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(10);
        make.top.mas_equalTo(self.mainImageV.mas_bottom).mas_offset(10);
    }];
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(8);
        make.right.mas_offset(-10);
    }];
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel);
        make.top.mas_equalTo(self.contentLabel.mas_bottom).mas_offset(8);
    }];
    
    [self.rzImagv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(23);
        make.top.mas_offset(10);
        make.right.mas_offset(-10);
    }];
    
    [self.ageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(16);
        make.centerY.mas_equalTo(self.nameLabel.mas_centerY);
        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(5);
    }];
    [self.xingLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(16);
        make.centerY.mas_equalTo(self.nameLabel.mas_centerY);
        make.left.mas_equalTo(self.ageBtn.mas_right).mas_offset(5);
    }];
    [self.VIPLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(16);
        make.centerY.mas_equalTo(self.nameLabel.mas_centerY);
        make.left.mas_equalTo(self.ageBtn.mas_right).mas_offset(5);
    }];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDragView:)];
    [self addGestureRecognizer:tap];
}

- (UIImageView *)mainImageV {
    if (!_mainImageV) {
        _mainImageV = [UIImageView new];
        _mainImageV.userInteractionEnabled = YES;
        
        _mainImageV.contentMode = UIViewContentModeScaleAspectFill;
        _mainImageV.clipsToBounds = YES;
    }
    return _mainImageV;
}
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.textColor = CustomColor(@"#FD9E14");
        _nameLabel.font = kFont_Bold(17);
    }
    return _nameLabel;
}
- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.textColor = CustomColor(@"#181818");
        _contentLabel.font = kFont_Medium(14);
        _contentLabel.numberOfLines = 2;
    }
    return _contentLabel;
}
- (UILabel *)addressLabel{
    if (!_addressLabel) {
        _addressLabel = [UILabel new];
        _addressLabel.textColor = CustomColor(@"#999999");
        _addressLabel.font = kFont_Medium(12);
    }
    return _addressLabel;
}
- (UIImageView *)rzImagv {
    if (!_rzImagv) {
        _rzImagv = [UIImageView new];
        _rzImagv.image = kImage(@"icon_near_vip");
    }
    return _rzImagv;
}
- (UIButton *)ageBtn {
    if (!_ageBtn) {
        _ageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _ageBtn.layer.cornerRadius = 3;
        _ageBtn.layer.masksToBounds = YES;
        _ageBtn.titleLabel.font = kFont_Medium(10);
        [_ageBtn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    }
    return _ageBtn;
}
- (UILabel *)xingLab{
    if (!_xingLab) {
        _xingLab = [UILabel new];
        _xingLab.textColor = UIColor.whiteColor;
        _xingLab.font = kFont_Medium(10);
        _xingLab.layer.cornerRadius = 3;
        _xingLab.layer.masksToBounds = YES;
        _xingLab.backgroundColor = CustomColor(@"#8E8CDE");
        _xingLab.textAlignment = NSTextAlignmentCenter;
    }
    return _xingLab;
}

- (UILabel *)VIPLab{
    if (!_VIPLab) {
        _VIPLab = [UILabel new];
        _VIPLab.textColor = UIColor.whiteColor;
        _VIPLab.font = kFont_Medium(10);
        _VIPLab.layer.cornerRadius = 3;
        _VIPLab.layer.masksToBounds = YES;
        _VIPLab.text = @"VIP";
        _VIPLab.backgroundColor = CustomColor(@"#FECA67");
        _VIPLab.textAlignment = NSTextAlignmentCenter;
    }
    return _VIPLab;
}

- (void)beginDrag:(UIPanGestureRecognizer *)ges{
    if (!self.canDrag) {
        return;
    }
    
    switch (ges.state) {
        case UIGestureRecognizerStateBegan:{
            self.originalPoint = self.center;
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGFloat xFromDrag = [ges translationInView:self].x;
            CGFloat yFromDrag = [ges translationInView:self].y;
            
            //需要旋转的总角度的百分比
            CGFloat percent = MIN(1, xFromDrag / (KSW/2));
            //总的角度
            CGFloat rotation = M_PI /8 *percent;
            // 缩放比例
            CGFloat scale = MAX(1-fabs(percent/4), 0.8);
            //设置位置
            self.center = CGPointMake(self.originalPoint.x+xFromDrag, self.originalPoint.y+yFromDrag);
            
            CGAffineTransform transform = CGAffineTransformMakeRotation(rotation);
            CGAffineTransform scaleT = CGAffineTransformScale(transform, scale, scale);
            self.transform = scaleT;
            if (self.delegate && [self.delegate respondsToSelector:@selector(dragView:movedDistance:)]) {
                [self.delegate dragView:self movedDistance:xFromDrag];
            }
            
        }
            break;
        case UIGestureRecognizerStateEnded:{
            //判断结束后的点
            CGFloat velocityX = [ges velocityInView:self.superview].x;
            [self endMoved:[ges translationInView:self].x andVelocityX:velocityX];
            
        }
            break;
        default:
            break;
    }
}


- (void)endMoved:(CGFloat)distance andVelocityX:(CGFloat)velocity {
    BOOL isBack = YES;
    CGFloat lastX = distance;
    
    if (fabs(lastX) > KSW/2 || fabs(velocity)>KSW/2) {
        isBack = NO;
    }
    
    if (isBack) {
        [UIView animateWithDuration:0.25 animations:^{
            self.center = self.originalPoint;
            self.transform = CGAffineTransformMakeRotation(0);
            
        } completion:nil];
    }
   
    if (self.delegate && [self.delegate respondsToSelector:@selector(dragView:moveEndWithBack:distance:)]) {
        [self.delegate dragView:self moveEndWithBack:isBack distance:lastX];
    }
}

- (void)tapDragView:(UIGestureRecognizer *)ges {
    if (self.delegate && [self.delegate respondsToSelector:@selector(dragViewDidTap:)]) {
        [self.delegate dragViewDidTap:self];
    }
}

- (void)setModel:(HomeNearModel *)model{
    _model = model;
    
    self.nameLabel.text = model.nickname;
    self.contentLabel.text = model.introduce;
    self.rzImagv.hidden = !model.isRealName;
    self.VIPLab.hidden = !model.isVIP;
    
    if (model.distance > 1000) {
        self.addressLabel.text = [NSString stringWithFormat:@"%.2fkm", model.distance/1000.0];
    } else {
        self.addressLabel.text = [NSString stringWithFormat:@"%.2fk", model.distance];
    }
    if (model.sex == 1) {
        [self.ageBtn setBackgroundColor:kTHEMECOLOR];
        [self.ageBtn setImage:kImage(@"sex_man") forState:UIControlStateNormal];
    } else {
        [self.ageBtn setBackgroundColor:CustomColor(@"#FD6BAC")];
        [self.ageBtn setImage:kImage(@"sex_woman") forState:UIControlStateNormal];
    }
    [self.ageBtn setTitle:[NSString dateFromOldDateString:model.birthday] forState:UIControlStateNormal];
    if (model.birthday) {
        self.xingLab.hidden = NO;
        self.xingLab.text = [NSString getAstroWithString:model.birthday];
        [self.VIPLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.ageBtn.mas_right).mas_offset(5 + 45);
        }];
    } else {
        self.xingLab.hidden = YES;
        [self.VIPLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.ageBtn.mas_right).mas_offset(5);
        }];
    }
    
    NSString *mainURL = [NSString stringWithFormat:@"%@?imageView2/2/w/400/h/200",model.avatar];
    
    [self.mainImageV sd_setImageWithURL:[NSURL URLWithString:mainURL]];
    
}
@end
