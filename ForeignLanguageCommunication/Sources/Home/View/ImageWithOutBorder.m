//
//  ImageWithOutBorder.m
//  VoiceLive
//
//  Created by mac on 2020/8/21.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "ImageWithOutBorder.h"


@interface ImageWithOutBorder ()
@property (nonatomic, strong) UIImageView *borderImageView;
@property (nonatomic, strong) UIImageView *mainImageView;
@end

@implementation ImageWithOutBorder

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setUpSubView];
    }
    return self;
}

- (void)setUpSubView {
    self.borderImageView = UIImageView.new;
    self.mainImageView = UIImageView.new;
    [self addSubview:self.borderImageView];
    [self addSubview:self.mainImageView];
    [self.borderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
    [self.mainImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(7, 7, 7, 7));
    }];
}


- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    self.mainImageView.layer.cornerRadius = (frame.size.height - 7)/ 2;
    self.mainImageView.clipsToBounds = YES;
}


- (void)setBorderImage:(NSString *)borderImage {
    _borderImage = borderImage;
    self.borderImageView.image = kImage(borderImage);
}


- (void)setMainImage:(NSString *)mainImage {
    _mainImage = mainImage;
    self.mainImageView.image = kImage(mainImage);
}
@end
