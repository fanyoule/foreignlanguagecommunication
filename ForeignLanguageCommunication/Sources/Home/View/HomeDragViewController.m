//
//  HomeDragViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/18.
//

#define kCardNumber   4
#define kCardHeight  (ScreenHeight-SNavBarHeight-TabbarHeight-100)
#define kMaxDistance (KSW/2)
#define kCardScale   0.9


#import "HomeDragViewController.h"
#import "HomeNearView.h"
#import "MyPersonalInformationVC.h"

@interface HomeDragViewController ()<HomeNearViewDelegate>
/**所有卡片数组*/
@property (nonatomic, strong) NSMutableArray *allCardArray;
/**展示的卡片*/
@property (nonatomic, strong) NSMutableArray *showCardArray;

@property (nonatomic, strong) NSMutableArray *tempCardArray;

@property (nonatomic) CGAffineTransform lastTrans;

@property (nonatomic) CGPoint lastCenter;

@property (nonatomic) CGRect lastFrame;

@property (nonatomic) int page;
@end

@implementation HomeDragViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.page = 1;
    
    [self addCard];
    [self pageDown];
}

- (NSMutableArray *)showCardArray{
    if (!_showCardArray) {
        _showCardArray = [NSMutableArray array];
    }
    return _showCardArray;
}
- (NSMutableArray *)allCardArray {
    if (!_allCardArray) {
        _allCardArray = [NSMutableArray array];
    }
    return _allCardArray;
}
- (NSMutableArray *)tempCardArray{
    if (!_tempCardArray) {
        _tempCardArray = [NSMutableArray array];
    }
    return _tempCardArray;
}
//MARK: 初始化卡片
- (void)addCard {
    for (int i = 0; i < kCardNumber; i ++) {
        HomeNearView *card = [[HomeNearView alloc] initWithFrame:CGRectMake(15, 10, KSW-30, kCardHeight)];
        
        card.delegate = self;
        card.canDrag = NO;
        
//        if (i > 0 && i < kCardNumber-1) {
////            card.transform = CGAffineTransformScale(card.transform,pow(kCardScale, i), pow(kCardScale, i));
//            card.width = (KSW-30) *pow(kCardScale, i);
//            card.height = kCardHeight*pow(kCardScale, i);
//
//        }else if (i == kCardNumber-1 && i > 0){
//            HomeNearView *preCard = self.showCardArray[i-1];
////            card.transform = preCard.transform;
//            card.frame = preCard.frame;
//        }
        [self.showCardArray addObject:card];
        NSLog(@"frame==%@",NSStringFromCGRect(card.frame));
    }
    
    for (int i = kCardNumber-1; i >=0; i --) {
        HomeNearView *card = self.showCardArray[i];
        [self.view addSubview:card];
        card.hidden = NO;
    }
}

- (void)reloadCard{
    
    for (int i = 0; i < self.showCardArray.count; i ++) {
        HomeNearView *card = self.showCardArray[i];
        //如果有数据 显示 并给数据
        if (self.allCardArray.firstObject) {
            card.hidden = NO;
            //取出第一个数据 然后删除数组中数据
            card.model = self.allCardArray.firstObject;
            [self.allCardArray removeFirstObject];
            NSLog(@"i==%ld,frame==%@",i,NSStringFromCGRect(card.frame));
            if (i == 0) {
                card.canDrag = YES;
            }
        }else{
            card.hidden = YES;
        }
    }
    


    //开始布局 卡片位置 大小 最上面原始数值  下面分别按比例缩小并下移
    for (int i = 0; i < self.showCardArray.count; i ++) {
        HomeNearView *card = self.showCardArray[i];
//        card.transform = CGAffineTransformRotate(card.transform, 0);
        card.transform = CGAffineTransformMakeRotation(0);
        if (i > 0 && i < self.showCardArray.count-1) {
            HomeNearView *preCard = self.showCardArray[i -1];
            
            card.transform = CGAffineTransformScale(card.transform, pow(kCardScale, i), pow(kCardScale, i));
            
            CGRect frame = card.frame;
            frame.origin.y = preCard.frame.origin.y+(preCard.frame.size.height-frame.size.height)+10;
            card.frame = frame;
            
        }else if (i == self.showCardArray.count-1){
            HomeNearView *preCard = self.showCardArray[i -1];
            card.transform = preCard.transform;
            card.frame = preCard.frame;
            self.lastTrans = card.transform;
            self.lastCenter = card.center;
        }
        card.originTrans = card.transform;
        card.originCenter = card.center;
        
    }
    
}
- (void)dragViewDidTap:(HomeNearView *)dragView{
    //点击
    NSLog(@"点击");
    if (![UserInfoManager shared].isLogin) {
        [self alertView:@"提示" msg:@"该功能需要登录之后才能使用" btnArrTitles:@[@"暂不登录", @"前往登录"] showCacel:NO style:UIAlertControllerStyleAlert block:^(int index) {
            if (index == 1) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:nil];
            }
        }];
        return;
    }
    HomeNearModel *model = dragView.model;
    NSLog(@"tap %@",model.nickname);
    MyPersonalInformationVC *vc= [[MyPersonalInformationVC alloc]init];
    vc.ID = model.ID;
//      vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)dragView:(HomeNearView *)dragView movedDistance:(CGFloat)distance{
    if (fabs(distance) <= kMaxDistance) {
        //在允许范围内  滑动  其他卡片进行缩放 位移
        //根据滑动的距离百分比 计算其他卡片需要缩放  最小比例 1  最大比例 到上一张card的大小
        for (int i = 1; i < self.showCardArray.count; i ++) {
            HomeNearView *card = self.showCardArray[i];
            HomeNearView *preCard = self.showCardArray[i-1];
            card.transform = CGAffineTransformScale(card.originTrans, 1+(1/0.9 - 1)*fabs(distance / kMaxDistance)*0.6, 1+(1/0.9 - 1)*fabs(distance / kMaxDistance)*0.6);
            card.centerY = card.originCenter.y-(card.centerY - preCard.originCenter.y)*fabs(distance/kMaxDistance);
        }
    }
}

- (void)dragView:(HomeNearView *)dragView moveEndWithBack:(BOOL)isBack distance:(CGFloat)distance{
    if (isBack) {
        //回到原来的位置和大小
        for (HomeNearView *card in self.showCardArray) {
            [UIView animateWithDuration:0.3 animations:^{
                card.transform = card.originTrans;
                card.center = card.originCenter;
            }];   
        }
    }else{
        //最上面的card 需要移出可视范围 并从数组中删除 然后根据数据源剩下数据插入到最后
        //首先 调整其他卡片位置 大小与位置是前一张的原始值
        
        for (int i = 1; i <self.showCardArray.count; i ++) {
            HomeNearView *card = self.showCardArray[i];
            HomeNearView *preCard = self.showCardArray[i -1];
            
            card.transform = preCard.originTrans;
            card.center = preCard.originCenter;
        }
        
        [self.showCardArray removeObject:dragView];
        dragView.transform = self.lastTrans;
        dragView.center = self.lastCenter;
        dragView.canDrag = NO;
        [self.view insertSubview:dragView belowSubview:self.showCardArray.lastObject];
        [self.showCardArray addObject:dragView];
        
        
        
        if (self.allCardArray.firstObject) {
            //还有数据
            dragView.model= self.allCardArray[0];
            dragView.hidden = NO;
            [self.allCardArray removeFirstObject];
        }else{
            dragView.hidden = YES;
        }
        
        for (int i = 0; i < self.showCardArray.count; i ++) {
            HomeNearView *card = self.showCardArray[i];
            card.originCenter = card.center;
            card.originTrans = card.transform;
            if (i ==0) {
                card.canDrag = YES;
            }
        }
        
        if (self.allCardArray.count <= 0) {
            HomeNearView *view = self.showCardArray.firstObject;
            if (view.hidden == YES) {
                [self pageUp];
            }
        }
    }
}

- (void)pageDown {
    self.page = 1;
    [self getData];
}
- (void)pageUp {
    [self getData];
}

//MARK: 获取数据
- (void)getData {
    self.allCardArray = [NSMutableArray array];
    kWeakSelf(self)
    [RequestManager nearbyPersonalUserId:[UserInfoManager shared].getUserID pageNum:self.page pageSize:10 loginLat:@"34.257194" loginLot:@"117.254512" withSuccess:^(id  _Nullable response) {
        NSDictionary *data = response[@"data"];
        int lastPage = [data[@"lastPage"] intValue];
        weakself.allCardArray = [HomeNearModel mj_objectArrayWithKeyValuesArray:data[@"list"]];
        if (weakself.page < lastPage) {
            weakself.page++;
        } else {
            weakself.page = 1;
        }
        [weakself reloadCard];
    } withFail:^(NSError * _Nullable error) {
        [weakself showText:error.localizedDescription];
    }];
    
//    for (int i = 0; i < 10; i ++) {
//        HomeNearModel *model = [HomeNearModel new];
//        model.nickname = @"1231231";
//        model.isVIP = YES;
//        model.introduce = @"msdsdlsmcsdm";
//        model.distance = 79;
//        [self.allCardArray addObject:model];
//    }
//    [self reloadCard];
}





@end
