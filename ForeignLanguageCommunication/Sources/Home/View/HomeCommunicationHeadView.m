//
//  HomeCommunicationHeadView.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//
#import "ImageWithOutBorder.h"
#import "HomeCommunicationHeadView.h"
#import "RankingModel.h"
@interface HomeCommunicationHeadView ()<SDCycleScrollViewDelegate>
@property (nonatomic, strong) SDCycleScrollView *circleView;

@property (nonatomic, strong)UIButton *ranBtn;

@property (nonatomic, strong)NSMutableArray *dataArr;

@end

@implementation HomeCommunicationHeadView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.avatarArray = [NSMutableArray array];
        [self setUpSubView];
    }
    return self;
}

- (void)setUpSubView {
    /**初始化  默认图片*/
    self.circleView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15, 15, KSW - 30, kScaleSize(136)) delegate:self placeholderImage:kImage(@"icon_home_banner")];
    /**图片加载模式*/
    self.circleView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.circleView];
    /**本地轮播图*/
//    self.circleView.localizationImageNamesGroup = @[@"icon_home_banner",@"icon_home_banner"];
    self.circleView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.circleView.layer.shadowOffset = CGSizeMake(1, 1);
    self.circleView.layer.shadowRadius = 4;
    self.circleView.layer.cornerRadius = 12;
    self.circleView.layer.shadowOpacity = 0.5;
    self.circleView.clipsToBounds = YES;
    //排行榜
    self.ranBtn = [[UIButton alloc]init];
    
    [self addSubview:self.ranBtn];
    
    UIColor *colorOne = RGBA(254, 225, 64, 1);
        
    UIColor *colorTwo = RGBA(255, 88, 88, 1);
        
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, (id)colorTwo.CGColor, nil];
        
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    //设置开始和结束位置(通过开始和结束位置来控制渐变的方向)
        
    gradient.startPoint = CGPointMake(1, 1);
        
    gradient.endPoint = CGPointMake(0, 1);
        
    gradient.colors = colors;
        
    gradient.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, kScaleSize(60));
        
    [self.ranBtn.layer insertSublayer:gradient atIndex:0];
    
    self.ranBtn.layer.cornerRadius = 30;
    self.ranBtn.layer.masksToBounds = YES;
//    [ranBtn setBackgroundImage:kImage(@"icon_home_rank") forState:UIControlStateNormal];
    
    self.ranBtn.frame = CGRectMake(15,self.circleView.bottom+20, KSW-30, kScaleSize(60));
    [self.ranBtn addTarget:self action:@selector(clickRankList) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *trophyImageV = [[UIImageView alloc]init];
    trophyImageV.image = kImage(@"icon_home_cup");
    trophyImageV.contentMode = UIViewContentModeScaleAspectFit;
    [self.ranBtn addSubview:trophyImageV];
    [trophyImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.ranBtn);
        make.left.equalTo(self.ranBtn).offset(18);
        make.height.width.equalTo(@50);
    }];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"排行榜";
    titleLabel.textColor = RGBA(254, 254, 254, 1);
    titleLabel.font = kFont_Bold(18);
    titleLabel.textAlignment = 0;
    [self.ranBtn addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.ranBtn);
        make.left.equalTo(trophyImageV.mas_right).offset(10);
    }];
    [self getData];
}
- (void)getData {
    [RequestManager chartsUserId:[UserInfoManager shared].getUserID userName:[UserInfoManager shared].getUserNickName withSuccess:^(id  _Nullable response) {
        NSLog(@"排行榜 === %@",response);
        NSArray *array = response[@"data"];
        for (int i = 0; i < array.count; i++) {
            RankingModel *model = [RankingModel mj_objectWithKeyValues:array[i]];
            if (i < 3) {
                [self.dataArr addObject:model];
            }
        }
        
        [self addFirstThree:self.dataArr];
    } withFail:^(NSError * _Nullable error) {

    }];
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [[NSMutableArray alloc] init];

    }
    return _dataArr;
}

-(void)addFirstThree:(NSMutableArray *)array{
//    // 倒序
//    NSMutableArray *resultArr = (NSMutableArray *)[[array reverseObjectEnumerator] allObjects];
    for (int i = 0; i < array.count; i++) {
        RankingModel *model = [[RankingModel alloc]init];
        model = array[i];
        UIImageView *backImageV = [[UIImageView alloc]init];
        if (i == 0) {
            [self sendSubviewToBack:backImageV];
            backImageV.image = [UIImage imageNamed:@"icon_home_border_1"];
        }else if (i == 1){
            backImageV.image = [UIImage imageNamed:@"icon_home_border_2"];
        }else if (i == 2){
            
            [self bringSubviewToFront:backImageV];
            backImageV.image = [UIImage imageNamed:@"icon_home_border_3"];
        }
       
        [self.ranBtn addSubview:backImageV];
        [backImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.ranBtn);
            make.height.width.equalTo(@35);
            make.right.equalTo(self.ranBtn).offset(-65 + i*25);
        }];
        
        UIImageView *avataImageV = [[UIImageView alloc]init];
        [avataImageV sd_setImageWithURL:[NSURL URLWithString:model.headUrl]];
        avataImageV.layer.cornerRadius = 14;
        avataImageV.layer.masksToBounds = YES;
        [backImageV addSubview:avataImageV];
        [avataImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(backImageV).offset(1);
            make.bottom.equalTo(backImageV).offset(-1);
            make.width.height.equalTo(@28);
        }];
        
        
        
        
    }
    
}



- (void)setAvatarArray:(NSMutableArray *)avatarArray {
    _avatarArray = avatarArray;
    
    self.circleView.localizationImageNamesGroup = avatarArray;
}


- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (self.bannerBlock) {
        self.bannerBlock(index);
    }
}

- (void)clickRankList {
    if (self.ranBlock) {
        self.ranBlock();
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
