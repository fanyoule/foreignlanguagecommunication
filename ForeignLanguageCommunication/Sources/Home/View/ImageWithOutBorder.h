//
//  ImageWithOutBorder.h
//  VoiceLive
//
//  Created by mac on 2020/8/21.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//


//带图片作为外边框的图片View

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN



@interface ImageWithOutBorder : UIView

@property (nonatomic, copy) NSString *borderImage;//作为边框的图片
@property (nonatomic, copy) NSString *mainImage;//主要图片


@end

NS_ASSUME_NONNULL_END
