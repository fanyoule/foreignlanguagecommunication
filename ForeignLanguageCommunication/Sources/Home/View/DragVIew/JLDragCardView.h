//
//  JLDragCardView.h
//  JLCardAnimation
//
//  Created by job on 16/8/31.
//  Copyright © 2016年 job. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ROTATION_ANGLE M_PI/8
#define CLICK_ANIMATION_TIME 0.5
#define RESET_ANIMATION_TIME 0.3

@class JLDragCardView;
@protocol JLDragCardDelegate <NSObject>
/**扫动*/
-(void)swipCard:(JLDragCardView *)cardView Direction:(BOOL) isRight;
/**移动中*/
-(void)moveCards:(CGFloat)distance;
/**滑动结束 不删除*/
-(void)moveBackCards;
/**左右滑 结束 */
-(void)adjustOtherCards;


@end



@interface JLDragCardView : UIView

@property (weak,   nonatomic) id <JLDragCardDelegate> delegate;
@property (strong, nonatomic) UIPanGestureRecognizer *panGesture;
@property (assign, nonatomic) CGAffineTransform originalTransform;
@property (assign, nonatomic) CGPoint originalPoint;
@property (assign, nonatomic) CGPoint originalCenter;
@property (assign, nonatomic) BOOL canPan;
@property (strong, nonatomic) NSDictionary *infoDict;
/**主图*/
@property (strong, nonatomic) UIImageView *headerImageView;
/**名字*/
@property (strong, nonatomic) UILabel *numLabel;
/**地址*/
@property (strong, nonatomic) UILabel *addressLabel;
/**简介*/
@property (strong, nonatomic) UILabel *introduceLabel;
/**标签*/
@property (strong, nonatomic) NSMutableArray *tagsArray;



-(void) leftButtonClickAction;

-(void) rightButtonClickAction;

@end
