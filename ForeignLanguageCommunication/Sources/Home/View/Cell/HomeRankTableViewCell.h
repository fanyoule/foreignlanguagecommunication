//
//  HomeRankTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeRankTableViewCell : UITableViewCell
//前三名头像
@property (nonatomic, strong) NSArray *modelArray;
@end

NS_ASSUME_NONNULL_END
