//
//  HomeHotTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import <UIKit/UIKit.h>
#import "HomeMatchTypeListModel.h"
NS_ASSUME_NONNULL_BEGIN
@class HomeCollectionCellModel;

typedef void(^clickMatchBlock)(HomeMatchTypeListModel *model);

@interface HomeHotTableViewCell : UITableViewCell
@property (nonatomic, strong) NSArray *modelArray;

@property (nonatomic, strong) NSMutableArray *matchTypeList;

@property (nonatomic, copy) clickMatchBlock matchBlock;
@end

NS_ASSUME_NONNULL_END
