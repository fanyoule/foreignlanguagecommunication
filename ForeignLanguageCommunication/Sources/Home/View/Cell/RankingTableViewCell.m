//
//  RankingTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/14.
//

#import "RankingTableViewCell.h"

@implementation RankingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.button.layer.cornerRadius = 5;
    self.button.layer.masksToBounds = YES;
    self.icon.layer.cornerRadius = 6;
    self.icon.layer.masksToBounds = YES;
    self.icon.contentMode = UIViewContentModeScaleAspectFill;
    self.button.backgroundColor = UIColor.clearColor;
    [self.button setBackgroundImage:[UIImage imageWithColor:kTHEMECOLOR] forState:UIControlStateNormal];
    [self.button setBackgroundImage:[UIImage imageWithColor:kColor(@"#B4B4B4")] forState:UIControlStateSelected];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)buttonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(rankingTicketIndex:)]) {
        [self.delegate rankingTicketIndex:self.index];
    }
}
- (void)setModel:(RankingModel *)model {
    _model = model;
    
//    self.icon.image = kImage(model.headUrl);
    if (IS_VALID_STRING(model.headUrl)) {
        [self.icon sd_setImageWithURL:[NSURL URLWithString:model.headUrl] placeholderImage:kImage(@"测试头像") completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (error) {
                self.icon.image =kImage(@"测试头像");
            }
        }];
       
    }
    
//    [self.icon sd_setImageWithURL:[NSURL URLWithString:model.headUrl] placeholderImage:nil];
    self.nickName.text = model.nickName;
    self.ticketLab.text = [NSString stringWithFormat:@"%@票", model.voteNum];
    self.button.selected = model.voteStatus == 1;
    if (model.sort > 3) {
        self.rankLab.hidden = NO;
        self.rankImgV.hidden = YES;
        self.rankLab.text = [NSString stringWithFormat:@"%d", model.sort];
    } else {
        self.rankLab.hidden = YES;
        self.rankImgV.hidden = NO;
        NSString *rankName = [NSString stringWithFormat:@"rank_%d", model.sort];
        self.rankImgV.image = kImage(rankName);
    }
}

@end
