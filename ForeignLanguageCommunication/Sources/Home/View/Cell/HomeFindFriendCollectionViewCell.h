//
//  HomeFindFriendCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import <UIKit/UIKit.h>
#import "HomeCollectionCellModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeFindFriendCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) HomeCollectionCellModel *model;
@end

NS_ASSUME_NONNULL_END
