//
//  HomeCollectionViewCell.m
//  VoiceLive
//
//  Created by mac on 2020/8/20.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "HomeCollectionViewCell.h"

@interface HomeCollectionViewCell ()




@end


@implementation HomeCollectionViewCell



- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        [self setUpSUbView];
        
    }
    return self;
}



- (void)setUpSUbView {
//    self.backgroundColor = UIColor.clearColor;
//    UIView *backView = [UIView new];
//    backView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1];
//    backView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
//    backView.layer.shadowOffset = CGSizeMake(0, 1);
//    backView.layer.shadowOpacity = 0.5;
//    backView.layer.shadowRadius = 4;
//    [self.contentView addSubview:backView];
//    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.edges.mas_equalTo(self.contentView);
//    }];
    self.mainImageView = UIImageView.new;
    
    
    [self.contentView addSubview:self.mainImageView];
    
    [self.contentView addSubview:self.nameLabel];
    
    [self.contentView addSubview:self.popularLabel];
    
    [self.contentView addSubview:self.iconView];
   
}
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = UILabel.new;
        _nameLabel.text = @"大学生英聊";
        _nameLabel.textColor = CustomColor(@"#333333");
        _nameLabel.font = kFont_Medium(15);
    }
    return _nameLabel;
}
- (UILabel *)popularLabel {
    if (!_popularLabel) {
        _popularLabel = UILabel.new;
        _popularLabel.textColor = CustomColor(@"#A5A9AE");
        _popularLabel.font = kFont_Medium(13);
        _popularLabel.text = @"999人在线匹配";
    }
    return _popularLabel;
}
- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = UIImageView.new;
        _iconView.image = kImage(@"icon_home_blue_line");
    }
    return _iconView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    [self.mainImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_offset(0);
        make.height.mas_equalTo(kScaleSize(128));
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mainImageView.mas_bottom).mas_offset(kScaleSize(20));
        make.left.mas_offset(8);
        make.trailing.mas_offset(-10);
    }];
    
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(12.5);
        make.height.mas_equalTo(11.5);
        make.left.mas_offset(10);
        make.bottom.mas_offset(-10);
    }];
    
    [self.popularLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.iconView);
        make.left.mas_equalTo(self.iconView.mas_right).mas_offset(12);
        make.right.mas_offset(-10);
    }];
    
    
}
- (void)setModel:(HomeCollectionCellModel *)model {
    _model = model;
    self.nameLabel.text = model.name;
    self.popularLabel.text = [NSString stringWithFormat:@"%ld人在线匹配",model.popular];
    self.mainImageView.image = kImage(model.mainImage);
}


- (void)setMatchModel:(HomeMatchTypeListModel *)matchModel{
    _matchModel = matchModel;
    self.nameLabel.text = matchModel.name;
    self.popularLabel.text = [NSString stringWithFormat:@"%ld人在线匹配",matchModel.virtualNum];
    
    NSArray *imageArr = @[@"icon_home_primary",@"icon_home_middle",@"icon_home_high",@"icon_home_univ",@"icon_home_work",@"icon_home_social"];
    
    NSString *imageStr = imageArr[matchModel.number-1];
    
    
    [self.mainImageView sd_setImageWithURL:[NSURL URLWithString:matchModel.image] placeholderImage:kImage(imageStr)];
    
}
@end
