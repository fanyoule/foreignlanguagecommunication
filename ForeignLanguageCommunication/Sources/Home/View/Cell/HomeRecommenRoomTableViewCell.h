//
//  HomeRecommenRoomTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import <UIKit/UIKit.h>
#import "HomeRoomModel.h"

//推荐房间
NS_ASSUME_NONNULL_BEGIN

@interface HomeRecommenRoomTableViewCell : UITableViewCell
@property (nonatomic, strong) HomeRoomModel *model;


@end

NS_ASSUME_NONNULL_END
