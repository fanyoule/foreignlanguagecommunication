//
//  HomeFindFriendCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import "HomeFindFriendCollectionViewCell.h"

@interface HomeFindFriendCollectionViewCell ()
@property (nonatomic, strong) UIImageView *mainImageView;//主图 封面
@property (nonatomic, strong) UIImageView *avatarView;//头像

@property (nonatomic, strong) UILabel *nameLabel;//标题
@property (nonatomic, strong) UILabel *popularLabel;//人气
@property (nonatomic, strong) UIImageView *iconView;//正在聊天
@property (nonatomic, strong) UIImageView *tagIconView;//扩列 交友
@property (nonatomic, strong) UIButton *chatTagBtn;//正在聊天
@property (nonatomic, strong) UIButton *tagBtn;//交友 扩列
@end

@implementation HomeFindFriendCollectionViewCell



- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.contentView.layer.cornerRadius = 6;
        self.contentView.clipsToBounds = YES;
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"#AD93EB" alpha:1];
        [self setUpSUbView];
        
    }
    return self;
}

- (void)setUpSUbView {
    self.mainImageView = UIImageView.new;
    [self.contentView addSubview:self.mainImageView];
    self.avatarView = UIImageView.new;
    [self.contentView addSubview:self.avatarView];
    self.avatarView.layer.cornerRadius = 10;
    self.avatarView.clipsToBounds = YES;
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.popularLabel];
    [self.contentView addSubview:self.iconView];
    [self.contentView addSubview:self.chatTagBtn];
    [self.contentView addSubview:self.tagBtn];
    
    self.chatTagBtn.hidden = YES;
    self.tagBtn.hidden = YES;
    
    self.mainImageView.backgroundColor = UIColor.greenColor;
    self.avatarView.backgroundColor = UIColor.orangeColor;
    
}

- (UIButton *)tagBtn {
    if (!_tagBtn) {
        _tagBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_tagBtn setBackgroundImage:kImage(@"icon_home_jiaoyou_bg") forState:UIControlStateNormal];
        [_tagBtn setTitle:@"交友" forState:UIControlStateNormal];
        [_tagBtn setTitleColor:CustomColor(@"#FFFFFF") forState:UIControlStateNormal];
        _tagBtn.titleLabel.font = kFont_Medium(11);
    }
    return _tagBtn;
}
- (UIButton *)chatTagBtn {
    if (!_chatTagBtn) {
        _chatTagBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_chatTagBtn setBackgroundImage:kImage(@"icon_home_chat_bg") forState:UIControlStateNormal];
        [_chatTagBtn setTitle:@"正在聊天" forState:UIControlStateNormal];
        [_chatTagBtn setTitleColor:CustomColor(@"#FFFFFF") forState:UIControlStateNormal];
        _chatTagBtn.titleLabel.font = kFont_Bold(13);
    }
    return _chatTagBtn;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = UILabel.new;
        _nameLabel.text = @"lay遇见你lay遇见你";
        _nameLabel.textColor = UIColor.whiteColor;
        _nameLabel.font = kFont_Medium(12);
    }
    return _nameLabel;
}
- (UILabel *)popularLabel {
    if (!_popularLabel) {
        _popularLabel = UILabel.new;
        _popularLabel.textColor = UIColor.whiteColor;
        _popularLabel.font = kFont_Medium(11);
        _popularLabel.text = @"999";
    }
    return _popularLabel;
}
- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = UIImageView.new;
        _iconView.image = kImage(@"icon_home_hot_line");
    }
    return _iconView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    [self.mainImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_offset(0);
        make.height.mas_equalTo(self.contentView.mas_width);
        
    }];
    
    [self.avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(20);
        make.right.mas_offset(-10);
        make.bottom.mas_offset(-10);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mainImageView.mas_bottom).mas_offset(5);
        make.left.mas_offset(10);
        make.trailing.mas_equalTo(self.contentView.mas_centerX).mas_offset(30);
    }];
    
    [self.popularLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameLabel.mas_bottom).mas_offset(5);
        make.left.mas_offset(22);
    }];
    
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(10);
            make.height.mas_equalTo(6);
        make.left.mas_offset(10);
        make.centerY.mas_equalTo(self.popularLabel);
    }];
    [self.chatTagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(28);
        make.left.top.mas_offset(0);
    }];
    [self.tagBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(35);
            make.height.mas_equalTo(15);
            make.left.mas_offset(5);
            make.bottom.mas_equalTo(self.mainImageView).mas_offset(-5);
    }];
    
}

- (void)setModel:(HomeCollectionCellModel *)model {
    _model = model;
    self.mainImageView.image = kImage(model.mainImage);
    self.chatTagBtn.hidden = NO;
    self.tagBtn.hidden = NO;
    
}

@end
