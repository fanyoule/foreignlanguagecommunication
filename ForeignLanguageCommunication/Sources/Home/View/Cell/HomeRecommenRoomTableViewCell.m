//
//  HomeRecommenRoomTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import "HomeRecommenRoomTableViewCell.h"

@interface HomeRecommenRoomTableViewCell ()
/**主图*/
@property (nonatomic, strong) UIImageView *headImageV;
/**标题*/
@property (nonatomic, strong) UILabel *nameLabel;
/**标签*/
@property (nonatomic, strong) UILabel *subNameLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UIView *subBackView;
@property (nonatomic, strong) UIButton *tagButton;
/**人像*/
@property (nonatomic, strong) UIImageView *iconView;
/**线*/
@property (nonatomic, strong) UIImageView *lineIconV;
@property (nonatomic, strong) UIView *backView;
@end

@implementation HomeRecommenRoomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubview];
    }
    return self;
}
- (void)setupSubview {
    self.contentView.backgroundColor = CustomColor(@"#F8F8F8");
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.headImageV];
    [self.backView addSubview:self.nameLabel];
    [self.backView addSubview:self.contentLabel];
    [self.backView addSubview:self.numberLabel];
    [self.backView addSubview:self.iconView];
    [self.backView addSubview:self.subBackView];
    [self.backView addSubview:self.tagButton];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_offset(0);
        make.bottom.mas_offset(-10);
    }];
    
    [self.headImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(kScaleSize(95));
        make.left.mas_offset(15);
        make.top.mas_offset(20);
        make.bottom.mas_offset(-20);
        
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.headImageV.mas_right).mas_offset(10);
            make.right.mas_offset(-10);
            make.top.mas_equalTo(self.headImageV).mas_offset(10);
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.nameLabel);
            make.bottom.mas_equalTo(self.headImageV);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_offset(-10);
        make.bottom.mas_equalTo(self.headImageV);
            
    }];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(12);
            make.centerY.mas_equalTo(self.numberLabel);
            make.right.mas_equalTo(self.numberLabel.mas_left).mas_offset(-5);
    }];
    
    [self.tagButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_offset(0);
            make.left.mas_equalTo(self.headImageV.mas_right).mas_offset(10);
            make.height.mas_equalTo(21);
            make.width.mas_equalTo(73);
    }];
}

- (UIButton *)tagButton {
    if (!_tagButton) {
        _tagButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_tagButton setBackgroundImage:kImage(@"icon_home_room_tag") forState:UIControlStateNormal];
        [_tagButton setImage:kImage(@"icon_home_room_line") forState:UIControlStateNormal];
        [_tagButton setTitle:@"开黑英聊" forState:UIControlStateNormal];
        [_tagButton setTitleColor:CustomColor(@"#FFFFFF") forState:UIControlStateNormal];
        _tagButton.titleLabel.font = kFont(10);
    }
    return _tagButton;
}

- (UIView *)subBackView {
    if (!_subBackView) {
        _subBackView = [UIView new];
        _subBackView.layer.cornerRadius = 7.5;
    }
    return _subBackView;
}
-(UIImageView *)lineIconV {
    if (!_lineIconV) {
        _lineIconV = [[UIImageView alloc] initWithImage:kImage(@"icon_home_line")];
        _lineIconV.size = CGSizeMake(9, 8);
    }
    return _lineIconV;
}
- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] initWithImage:kImage(@"icon_home_people")];
    }
    return _iconView;
}
- (UILabel *)numberLabel {
    if (!_numberLabel) {
        _numberLabel = UILabel.new;
        _numberLabel.font = kFont_Bold(13);
        _numberLabel.textColor = CustomColor(@"#333333");
    }
    return _numberLabel;
}
- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.font = kFont_Medium(13);
        _contentLabel.textColor = CustomColor(@"#A5A9AE");
    }
    return _contentLabel;
}
- (UILabel *)subNameLabel {
    if (!_subNameLabel) {
        _subNameLabel = [UILabel new];
        _subNameLabel.textAlignment = NSTextAlignmentCenter;
        _subNameLabel.font = kFont_Medium(10);
        _subNameLabel.textColor = CustomColor(@"#FFFFFF");
    }
    return _subNameLabel;
}
-(UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.font = kFont_Bold(15);
        _nameLabel.textColor = CustomColor(@"#333333");
    }
    return _nameLabel;
}
- (UIImageView *)headImageV {
    if (!_headImageV) {
        _headImageV = [UIImageView new];
        _headImageV.layer.cornerRadius = 6;
        _headImageV.clipsToBounds = YES;
        _headImageV.image = kImage(@"icon_home_abc");
//        _headImageV.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _headImageV;
}
- (UIView *)backView {
    if (!_backView) {
        _backView = UIView.new;
        _backView.backgroundColor = CustomColor(@"#FFFFFF");
//        _backView.layer.cornerRadius = 12;
//        _backView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
//        _backView.layer.shadowOffset = CGSizeMake(0, 1);
//        _backView.layer.shadowOpacity = 0.5;
//        _backView.layer.shadowRadius = 4;
    }
    return _backView;
}

//- (void)setModel:(id)model {
//
//    self.nameLabel.text = @"英语狂欢-四厅/深拥奶酱";
//    self.contentLabel.text = @"一起玩转英语聊天";
//    self.numberLabel.text = @"112";
//
//}
- (void)setModel:(HomeRoomModel *)model {
    _model = model;
    
    self.nameLabel.text = model.roomName;
    self.contentLabel.text = model.welcome;
    self.numberLabel.text = [NSString longStrong:model.onlineNum];
    [self.headImageV sd_setImageWithURL:[NSURL URLWithString:model.romPicUrl] placeholderImage:nil];
}

@end
