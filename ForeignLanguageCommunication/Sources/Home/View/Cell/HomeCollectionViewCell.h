//
//  HomeCollectionViewCell.h
//  VoiceLive
//
//  Created by mac on 2020/8/20.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeCollectionCellModel.h"
#import "HomeMatchTypeListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UILabel *popularLabel;//人气
@property (nonatomic, strong) UIImageView *mainImageView;//主图 封面
@property (nonatomic, strong) UIImageView *avatarView;//头像

@property (nonatomic, strong) UILabel *nameLabel;//标题

@property (nonatomic, strong) UIImageView *iconView;

@property (nonatomic, strong) HomeCollectionCellModel *model;

@property (nonatomic, strong) HomeMatchTypeListModel *matchModel;


@end

NS_ASSUME_NONNULL_END
