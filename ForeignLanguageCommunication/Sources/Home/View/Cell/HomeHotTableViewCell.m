//
//  HomeHotTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import "HomeHotTableViewCell.h"
#import "HomeCollectionViewCell.h"

@interface HomeHotTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation HomeHotTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubview];
    }
    return self;
}

- (void)setupSubview {
    [self.contentView addSubview:self.collectionView];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.scrollEnabled = NO;
    self.contentView.backgroundColor = UIColor.whiteColor;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.contentView);
    }];
    
    [self.collectionView registerClass:[HomeCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
}
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
           
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;//垂直滚动
        
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = KSW - 30 - kScaleSize(166)*2;
        layout.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
        layout.itemSize = CGSizeMake(kScaleSize(166), kScaleSize(200));
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = UIColor.whiteColor;
    }
    return _collectionView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.matchTypeList.count>0) {
        return self.matchTypeList.count;
    }
    return 0;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.matchModel = self.matchTypeList[indexPath.item];
    if (self.matchTypeList && self.matchTypeList.count > 0) {
        HomeMatchTypeListModel *m = self.matchTypeList[indexPath.item];
        cell.popularLabel.text = [NSString stringWithFormat:@"%ld人在线匹配", m.virtualNum];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     HomeMatchTypeListModel *model = self.matchTypeList[indexPath.item];
    NSLog(@"点击了课程--%@",model.name);
    
    if (self.matchBlock) {
        self.matchBlock(model);
    }
}
- (void)setModelArray:(NSArray *)modelArray {
    _modelArray = modelArray;
    
//    [self.collectionView reloadData];
}
- (void)setMatchTypeList:(NSMutableArray *)matchTypeList {
    _matchTypeList = matchTypeList;
    
    [self.collectionView reloadData];
}

@end
