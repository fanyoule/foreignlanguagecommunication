//
//  RankingTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by mac-xdd on 2021/1/14.
//

#import <UIKit/UIKit.h>
#import "RankingModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol RankingTableViewCellDelegate <NSObject>

- (void)rankingTicketIndex:(NSInteger)index;

@end

@interface RankingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *rankImgV;
@property (weak, nonatomic) IBOutlet UILabel *rankLab;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *ticketLab;
@property (weak, nonatomic) IBOutlet UIButton *button;

@property (nonatomic, strong) RankingModel *model;
@property (nonatomic, assign) NSInteger index;
@property(nonatomic, weak) id<RankingTableViewCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
