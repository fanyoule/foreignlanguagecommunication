//
//  HomeRankTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2020/12/17.
//

#import "HomeRankTableViewCell.h"
#import "ImageWithOutBorder.h"


@interface HomeRankTableViewCell ()
@property (nonatomic, strong) NSMutableArray *avatarArray;

@property (nonatomic, strong) UIImageView *rankImageV;
@end
@implementation HomeRankTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubView];
    }
    return self;
}
- (void)setUpSubView {
    //[self createGradientRankView];
    self.rankImageV = [[UIImageView alloc] initWithImage:kImage(@"icon_home_rank")];
    [self.contentView addSubview:self.rankImageV];
    [self.rankImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.contentView);
    }];
}
- (NSMutableArray *)avatarArray {
    if (!_avatarArray) {
        _avatarArray = [[NSMutableArray alloc] init];
    }
    return _avatarArray;
}
- (void)createGradientRankView {
    UIControl *control = [[UIControl alloc] init];
    control.frame = CGRectMake(15, 10, KSW - 30, 60);
    control.layer.cornerRadius = 30;
    control.clipsToBounds = YES;
    [self.contentView addSubview:control];
    [Util createGradientLayerWithViwe:control colors:@[CustomColor(@"#FF5858"),CustomColor(@"#FEE140")] type:NO];
    [control addTarget:self action:@selector(clickRankList) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *iconView = [[UIImageView alloc] initWithImage:kImage(@"icon_home_cup")];
    
    [control addSubview:iconView];
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(45);
        make.left.mas_offset(18);
        make.centerY.mas_offset(0);
    }];
    
    UILabel *label = UILabel.new;
    label.text = @"排行榜";
    label.textColor = UIColor.whiteColor;
    label.font = kFont_Bold(18);
    [control addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(iconView.mas_right).mas_offset(8);
        make.centerY.mas_offset(0);
    }];
    
    
    [self.avatarArray removeAllObjects];
    for (int i = 0; i < 3; i ++) {
        ImageWithOutBorder *imageOutBorderView = [[ImageWithOutBorder alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
        [control addSubview:imageOutBorderView];
        imageOutBorderView.backgroundColor = UIColor.clearColor;
        
        //从右往左排列 最后一个其实是第一名的头像
        
        imageOutBorderView.borderImage =[NSString stringWithFormat:@"icon_home_border_%d",3-i];
        
        [imageOutBorderView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_offset(0);
            make.right.mas_offset(-(15+25*i));
            make.width.height.mas_equalTo(35);
        }];
        [self.avatarArray addObject:imageOutBorderView];
    }
}

- (void)setModelArray:(NSArray *)modelArray {
    _modelArray = modelArray;
    //刷新前三名头像
}
- (void)clickRankList {
//    if (self.rankBlock) {
//        self.rankBlock();
//    }
}
@end
