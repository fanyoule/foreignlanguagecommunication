//
//  GroupOfDataViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import "BaseViewController.h"
#import "SearchForCrowdModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface GroupOfDataViewController : BaseViewController
@property (nonatomic, strong)SearchForCrowdModel *crowdModel;
@end

NS_ASSUME_NONNULL_END
