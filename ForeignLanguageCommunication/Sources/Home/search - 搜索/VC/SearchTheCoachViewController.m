//
//  SearchTheCoachViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//  教练

#import "SearchTheCoachViewController.h"
#import "CoachWithCourseTableViewCell.h"
#import "CoachWithCourseModel.h"
#import "CoachDetailsViewController.h"// 教练详情
@interface SearchTheCoachViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)UITableView *tableView;

@property (nonatomic, assign)int paging;

@property (nonatomic, strong)NSString *nameStr;

@property (nonatomic, strong)NSMutableArray *dataArray;

@end

@implementation SearchTheCoachViewController
- (UIView *)listView{
    return self.view;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = UIColor.clearColor;
        
        _tableView.showsVerticalScrollIndicator = NO;
        
        
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else{
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        UITapGestureRecognizer *tableViewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentTableViewTouchInSide)];

        tableViewGesture.numberOfTapsRequired = 1;

        tableViewGesture.cancelsTouchesInView = NO;

        [_tableView addGestureRecognizer:tableViewGesture];
        
    }
    return _tableView;
}
- (void)commentTableViewTouchInSide{
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchTheCoach:) name:@"SearchTheCoach" object:nil];
    self.paging = 1;
    
    // MARK: 下拉刷新
        self.tableView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        
        // MARK: 上拉加载
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopic)];
    
    
    
}

// MARK: 下拉刷新回调
-(void)loadNewData{
    self.paging = 1;
    [self.dataArray removeAllObjects];
    [self getData];
}
// MARK: 上拉加载
-(void)loadMoreTopic{
    self.paging++;
    [self getData];
}

//MARK: 通知
- (void)searchTheCoach:(NSNotification *)notice {
    //刷新操作
    self.nameStr = notice.object;
    [self.dataArray removeAllObjects];
    [self getData];
}

-(void)getData{
    kWeakSelf(self)
    if (!self.nameStr) {
        self.nameStr = @"";
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@(self.paging) forKey:@"pageNum"];
    [dic setObject:@(10) forKey:@"pageSize"];
    [dic setObject:@(0) forKey:@"applyCrowd"];
    [dic setObject:self.nameStr forKey:@"name"];
    [dic setObject:@([[UserInfoManager shared] getUserID]) forKey:@"userId"];
    [RequestManager homeSearchCoach:dic withSuccess:^(id  _Nullable response) {
        NSArray *array = response[@"data"][@"list"];
        for (int i = 0; i < array.count; i ++ ) {
            CoachWithCourseModel *model = [CoachWithCourseModel mj_objectWithKeyValues:array[i]];
            [self.dataArray addObject:model];
        }
        
        [weakself stopLoading];
        [self.tableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        [weakself stopLoading];
    }];
}
- (void)stopLoading {
    if (self.tableView.mj_header.isRefreshing) {
        [self.tableView.mj_header endRefreshing];
    }
}

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CoachWithCourseModel *model = self.dataArray[indexPath.row];
    return model.height;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CoachWithCourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoachWithCourseTableViewCell"];
    if (!cell) {
        cell = [[CoachWithCourseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CoachWithCourseTableViewCell"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CoachWithCourseModel *model = [[CoachWithCourseModel alloc]init];
    model = self.dataArray[indexPath.row];
    CoachDetailsViewController *vc = [[CoachDetailsViewController alloc]init];
    vc.courseModel = model;
    [self.navigationController pushViewController:vc animated:YES];
    
}
    
    

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
