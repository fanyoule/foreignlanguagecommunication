//
//  SearchTheRoomViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//  群

#import "SearchTheRoomViewController.h"
#import "SearchGroupPageCollectionViewCell.h"

#import "GroupOfDataViewController.h"// 群资料
#import "ETNullDataView.h"
#import "SearchForCrowdModel.h"
@interface SearchTheRoomViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, assign)int paging;

@property (nonatomic, strong)NSString *nameStr;
@property(nonatomic,strong)ETNullDataView * nullView;
@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation SearchTheRoomViewController
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}
-(ETNullDataView *)nullView{
    if (!_nullView) {
        _nullView = [[ETNullDataView alloc]initWithFrame:CGRectMake(0, 0, YL_kScreenWidth, 100)];
        _nullView.hidden = YES;
    }
    return _nullView;
}
- (UIView *)listView{
    return self.view;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        _collectionView  = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = RGBA(255, 255, 255, 1);
        if (@available(iOS 11.0,*)) {
            _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    return _collectionView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self creatUI];
}
-(void)creatUI{
    self.view.backgroundColor  =UIColor.whiteColor;
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
    
    [self.collectionView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.collectionView);
    }];
    
    [self.collectionView registerClass:[SearchGroupPageCollectionViewCell class] forCellWithReuseIdentifier:@"SearchGroupPageCollectionViewCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchForCrowd:) name:@"SearchForCrowd" object:nil];
    
    self.paging = 1;
    
    // MARK: 下拉刷新
        self.collectionView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        
        // MARK: 上拉加载
    self.collectionView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopic)];
    
    
}
// MARK: 下拉刷新回调
-(void)loadNewData{
    self.paging = 1;
    [self.dataArray removeAllObjects];
    [self getData];
}
// MARK: 上拉加载
-(void)loadMoreTopic{
    self.paging++;
    [self getData];
}

//MARK: 通知
- (void)searchForCrowd:(NSNotification *)notice {
    
    self.nameStr = notice.object;
    [self.dataArray removeAllObjects];
    [self getData];
    
}

// MARK: 分页条件搜索个人群
-(void)getData{
    kWeakSelf(self)
    
    [RequestManager searchPersonalClustersPages:self.paging sysId:self.nameStr pageSize:10 name:self.nameStr withSuccess:^(id  _Nullable response) {
        NSLog(@"搜索群 === %@",response);
        NSArray *array = response[@"data"][@"list"];
        for (int i = 0; i < array.count; i++) {
            SearchForCrowdModel *model = [SearchForCrowdModel mj_objectWithKeyValues:array[i]];
            [weakself.dataArray addObject:model];
        }
        weakself.nullView.hidden = weakself.dataArray.count;
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView.mj_footer endRefreshing];
    } withFail:^(NSError * _Nullable error) {
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView.mj_footer endRefreshing];
    }];
    
}




- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;   //返回section数
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchGroupPageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchGroupPageCollectionViewCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 6;
    
    cell.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.layer.shadowOffset = CGSizeMake(0.1f, 0.1f);
    cell.layer.shadowOpacity = 0.5f;
    
    SearchForCrowdModel *model = [[SearchForCrowdModel alloc]init];
    model = self.dataArray[indexPath.row];
    cell.model = model;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(KSW-30, 80);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 15, 10, 15);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SearchForCrowdModel *model = [[SearchForCrowdModel alloc]init];
    model = self.dataArray[indexPath.row];
    
    GroupOfDataViewController *vc = [[GroupOfDataViewController alloc]init];
    vc.crowdModel = model;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
