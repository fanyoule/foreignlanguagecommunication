//
//  SearchTheUserViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//  用户

#import "SearchTheUserViewController.h"
#import "SearchTheUserTableViewCell.h"
#import "SearchTheUserModel.h"
#import "MyPersonalInformationVC.h"// 个人主页
#import "ThePersonalDataModel.h"
#import "ETNullDataView.h"

@interface SearchTheUserViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)UITableView *tableView;



@property (nonatomic, assign)int paging;

@property (nonatomic, strong)NSString *nameStr;
@property(nonatomic,strong)ETNullDataView * nullView;
@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation SearchTheUserViewController
-(ETNullDataView *)nullView{
    if (!_nullView) {
        _nullView = [[ETNullDataView alloc]initWithFrame:CGRectMake(0, 0, YL_kScreenWidth, 100)];
        _nullView.hidden = YES;
    }
    return _nullView;
}

- (UIView *)listView{
    return self.view;
}
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = UIColor.clearColor;
        
        _tableView.showsVerticalScrollIndicator = NO;
        
        
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else{
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        UITapGestureRecognizer *tableViewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentTableViewTouchInSide)];

        tableViewGesture.numberOfTapsRequired = 1;

        tableViewGesture.cancelsTouchesInView = NO;

        [_tableView addGestureRecognizer:tableViewGesture];
        
    }
    return _tableView;
}

- (void)commentTableViewTouchInSide{
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatUI];
    

}
-(void)creatUI{
    self.view.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.bottom.equalTo(self.view);
    }];
    [self.tableView addSubview:self.nullView];
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.tableView);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchTheUser:) name:@"SearchTheUser" object:nil];

    self.paging = 1;
    
    // MARK: 下拉刷新
        self.tableView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        
        // MARK: 上拉加载
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreTopic)];
    
}
// MARK: 下拉刷新回调
- (void)loadNewData {
    self.paging = 1;
    [self.dataArray removeAllObjects];
    [self getData];
}
// MARK: 上拉加载
- (void)loadMoreTopic {
    self.paging++;
    [self getData];
}

//MARK: 通知
- (void)searchTheUser:(NSNotification *)notice {
    //刷新操作
    self.nameStr = notice.object;
    [self.dataArray removeAllObjects];
    [self getData];
}

-(void)getData{
    kWeakSelf(self)
    
    [RequestManager searchUserWithUserId:[[UserInfoManager shared] getUserID] pageNum:self.paging pageSize:10 userName:self.nameStr withSuccess:^(id  _Nullable response) {
        NSLog(@"搜索用户 === %@",response);
        NSArray *array = response[@"data"][@"list"];
        for (int i = 0; i < array.count; i++) {
            SearchTheUserModel *model = [SearchTheUserModel mj_objectWithKeyValues:array[i]];
            [weakself.dataArray addObject:model];
        }
        weakself.nullView.hidden = weakself.dataArray.count;
        [weakself.tableView reloadData];
        [weakself.tableView.mj_header endRefreshing];
        [weakself.tableView.mj_footer endRefreshing];
    } withFail:^(NSError * _Nullable error) {
        [weakself.tableView.mj_header endRefreshing];
        [weakself.tableView.mj_footer endRefreshing];
    }];
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    return _dataArray;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 82;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SearchTheUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchTheUserTableViewCell"];
    if (!cell) {
        cell = [[SearchTheUserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SearchTheUserTableViewCell"];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.backgroundColor = [UIColor whiteColor];
    SearchTheUserModel *model = [[SearchTheUserModel alloc]init];
    cell.index = indexPath.row;
    model = self.dataArray[indexPath.row];
    kWeakSelf(self)
    cell.block = ^(long userId, NSInteger index) {
        [weakself follow:userId index:index];
    };
    cell.model = model;
    return cell;
}
- (void)follow:(long)userId index:(NSInteger)index {
    [RequestManager addFollowsMappingWithId:[UserInfoManager shared].getUserID followUserId:userId withSuccess:^(id  _Nullable response) {
        [self showText:@"关注成功"];
        SearchTheUserModel *model = self.dataArray[index];
        model.follow = 1;
        [self.tableView reloadData];
    } withFail:^(NSError * _Nullable error) {
        
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchTheUserModel *model = self.dataArray[indexPath.row];
    MyPersonalInformationVC *vc = [[MyPersonalInformationVC alloc] init];
    vc.ID = model.ID;
    [self.navigationController pushViewController:vc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
