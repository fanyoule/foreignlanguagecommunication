//
//  GroupOfDataViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//  群资料

#import "GroupOfDataViewController.h"
#import "FirstGroupDataView.h"
#import "GroupDataMemberView.h"

#import "GroupInformationDetailsModel.h"
@interface GroupOfDataViewController ()

@property (nonatomic, strong)FirstGroupDataView *headView;

@property (nonatomic, strong)GroupDataMemberView *memberView;

@property (nonatomic, strong)UIButton *goChatBtn;

@property (nonatomic, strong)GroupInformationDetailsModel *detailsModel;
@end

@implementation GroupOfDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navTitleString = @"群资料";
    self.view.backgroundColor = RGBA(245, 245, 245, 1);
    
    [self.view addSubview:self.headView];
    [self.view addSubview:self.memberView];
    [self.view addSubview:self.goChatBtn];
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@100);
    }];
    [self.memberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@160);
        make.top.equalTo(self.headView.mas_bottom);
        make.left.right.equalTo(self.view);
    }];
    [self.goChatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.height.equalTo(@40);
        make.bottom.equalTo(self.view).offset(-TabbarSafeMargin-10);
    }];
    
    
    // Do any additional setup after loading the view.
}

- (void)setCrowdModel:(SearchForCrowdModel *)crowdModel{
    _crowdModel = crowdModel;
    [self.headView.headImageV sd_setImageWithURL:[NSURL URLWithString:crowdModel.avatarUrl]];
    self.headView.nameLabel.text = crowdModel.clusterName;
    
    
    
    [RequestManager searchToClusterInfoWithId:[[UserInfoManager shared] getUserID] Id:crowdModel.ID withSuccess:^(id  _Nullable response) {
        NSLog(@"群信息 === %@",response);
        NSDictionary *dic = response[@"data"];
        if ([response[@"status"] integerValue] == 200) {
            self.detailsModel = [GroupInformationDetailsModel mj_objectWithKeyValues:dic];
        }
        self.memberView.model = self.detailsModel;
    } withFail:^(NSError * _Nullable error) {
        
    }];
    
    
    
}


- (FirstGroupDataView *)headView{
    if (!_headView) {
        _headView = [[FirstGroupDataView alloc]init];
        _headView.backgroundColor = UIColor.whiteColor;
    }
    return _headView;
}

- (GroupDataMemberView *)memberView{
    if (!_memberView) {
        _memberView = [[GroupDataMemberView alloc]init];
        _memberView.backgroundColor = UIColor.whiteColor;
    }
    return _memberView;
}

- (UIButton *)goChatBtn{
    if (!_goChatBtn) {
        _goChatBtn = [[UIButton alloc]init];
        [_goChatBtn setTitle:@"去聊天" forState:(UIControlStateNormal)];
        [_goChatBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        _goChatBtn.titleLabel.font = kFont(15);
        _goChatBtn.backgroundColor = RGBA(52, 120, 245, 1);
        _goChatBtn.layer.cornerRadius = 40/2;
        _goChatBtn.clipsToBounds = YES;
    }
    return _goChatBtn;
}

- (GroupInformationDetailsModel *)detailsModel{
    if (!_detailsModel) {
        _detailsModel = [[GroupInformationDetailsModel alloc]init];
    }
    return _detailsModel;
}

@end
