//
//  SearchTheRoomViewController.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchTheRoomViewController : UIViewController<JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
