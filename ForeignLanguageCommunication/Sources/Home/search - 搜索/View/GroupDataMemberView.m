//
//  GroupDataMemberView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import "GroupDataMemberView.h"
#import "GroupMembersModel.h"

@interface GroupDataMemberView ()
/** 标题*/
@property (nonatomic, strong)UILabel *titleLable;




@end
@implementation GroupDataMemberView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}

-(void)addControls{
    [self addSubview:self.titleLable];
    [self addSubview:self.numberLabel];
    [self.titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.left.equalTo(self).offset(15);
    }];
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.right.equalTo(self).offset(-15);
    }];
    
    
}
-(void)setModel:(GroupInformationDetailsModel *)model{
    _model = model;
    self.numberLabel.text = [NSString stringWithFormat:@"%ld人",model.count];
    
    NSArray *clusterMemberArray = model.clusterMember;
    NSMutableArray *dataArray = [NSMutableArray array];
    
    CGFloat w = (KSW - 60) / 5;
    
    for (int i = 0 ; i < clusterMemberArray.count; i++) {
        GroupMembersModel *membersModel = [GroupMembersModel mj_objectWithKeyValues:clusterMemberArray[i]];
        if (i <= 4) {
            [dataArray addObject:membersModel];
        }
    }
    for (int i = 0 ; i < dataArray.count; i++) {
        GroupMembersModel *groupModel = [[GroupMembersModel alloc]init];
        groupModel = dataArray[i];
        UIButton *btn = [[UIButton alloc]init];
        btn.backgroundColor = UIColor.clearColor;
        [self addSubview:btn];
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(w));
            make.left.equalTo(self).offset(i*w + i*10 + 10);
            make.height.equalTo(@100);
            make.top.equalTo(self.titleLable.mas_bottom).offset(15);
        }];
        
        UIImageView *imageV = [[UIImageView alloc]init];
        [imageV sd_setImageWithURL:[NSURL URLWithString:groupModel.avatarUrl]];
        imageV.layer.cornerRadius = 6;
        imageV.clipsToBounds = YES;
        [btn addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(btn);
            make.width.height.equalTo(@55);
            make.centerX.equalTo(btn);
        }];
        
        UILabel *label = [[UILabel alloc]init];
        label.text = groupModel.nickName;
        label.textColor = RGBA(24, 24, 24, 1);
        label.font = kFont(14);
        label.textAlignment = 1;
        [btn addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(imageV.mas_bottom).offset(10);
            make.centerX.equalTo(btn);
        }];
        
        UILabel *managerLabel = [[UILabel alloc]init];
        managerLabel.text = @"群主";
        managerLabel.textColor = RGBA(255, 255, 255, 1);
        managerLabel.font = kFont(10);
        managerLabel.textAlignment = 1;
        managerLabel.backgroundColor = RGBA(52, 120, 245, 1);
        managerLabel.layer.cornerRadius = 15/2;
        managerLabel.clipsToBounds = YES;
        
        managerLabel.hidden = YES;
        [btn addSubview:managerLabel];
        [managerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(imageV.mas_bottom);
            make.width.equalTo(@25);
            make.height.equalTo(@15);
            make.centerX.equalTo(btn);
        }];
        if (groupModel.chairman == YES) {
            managerLabel.hidden = NO;
        }else{
            managerLabel.hidden = YES;
        }
        
        
    }
}



- (UILabel *)titleLable{
    if (!_titleLable) {
        _titleLable = [[UILabel alloc]init];
        _titleLable.text = @"群成员";
        _titleLable.textColor = RGBA(24, 24, 24, 1);
        _titleLable.font = kFont(16);
        _titleLable.textAlignment = 0;
    }
    return _titleLable;
}

- (UILabel *)numberLabel{
    if (!_numberLabel) {
        _numberLabel =[[UILabel alloc]init];
        _numberLabel.text = @"8人";
        _numberLabel.textColor = RGBA(153, 153, 153, 1);
        _numberLabel.font = kFont(16);
        _numberLabel.textAlignment = 2;
    }
    return _numberLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
