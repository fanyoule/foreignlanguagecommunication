//
//  GroupDataMemberView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import <UIKit/UIKit.h>
#import "GroupInformationDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface GroupDataMemberView : UIView
/** 人数*/
@property (nonatomic, strong)UILabel *numberLabel;

/** 群成员信息*/

@property (nonatomic, strong)GroupInformationDetailsModel *model;
@end

NS_ASSUME_NONNULL_END
