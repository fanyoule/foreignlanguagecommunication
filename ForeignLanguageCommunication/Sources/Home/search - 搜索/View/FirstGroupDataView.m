//
//  FirstGroupDataView.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import "FirstGroupDataView.h"
@interface FirstGroupDataView ()



@end
@implementation FirstGroupDataView


-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addControls];
    }
    return self;
}
-(void)addControls{
    [self addSubview:self.headImageV];
    [self addSubview:self.nameLabel];
    [self.headImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(15);
        make.height.width.equalTo(@50);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.headImageV.mas_right).offset(10);
    }];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.left.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-15);
        make.height.equalTo(@0.5);
    }];
    
    
}
- (UIImageView *)headImageV{
    if (!_headImageV) {
        _headImageV = [[UIImageView alloc]init];
        _headImageV.image = kImage(@"测试头像");
        _headImageV.layer.cornerRadius = 6;
        _headImageV.clipsToBounds = YES;
    }
    return _headImageV;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"群名字";
        _nameLabel.textColor = RGBA(24, 24, 24, 1);
        _nameLabel.font = kFont_Medium(18);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
