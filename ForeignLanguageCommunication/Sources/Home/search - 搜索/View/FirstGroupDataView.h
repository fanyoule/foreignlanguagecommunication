//
//  FirstGroupDataView.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FirstGroupDataView : UIView
@property (nonatomic, strong)UIImageView *headImageV;

@property (nonatomic, strong)UILabel *nameLabel;
@end

NS_ASSUME_NONNULL_END
