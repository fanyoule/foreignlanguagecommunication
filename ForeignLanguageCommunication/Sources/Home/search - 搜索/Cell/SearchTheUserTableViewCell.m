//
//  SearchTheUserTableViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import "SearchTheUserTableViewCell.h"

@implementation SearchTheUserTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = RGBA(223, 223, 223, 1);
        [self.contentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(77);
            make.height.equalTo(@0.5);
        }];
        
        [self.contentView addSubview:self.portraitImageV];
        [self.contentView addSubview:self.nameLabel];
        [self.contentView addSubview:self.IDLabel];
        [self.contentView addSubview:self.focusBtn];
        [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(15);
            make.width.height.equalTo(@52);
        }];
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.portraitImageV).offset(8);
            make.left.equalTo(self.portraitImageV.mas_right).offset(9.5);
        }];
        [self.IDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.portraitImageV).offset(-8);
            make.left.equalTo(self.portraitImageV.mas_right).offset(9.5);
        }];
        
        [self.focusBtn sizeToFit];
        CGFloat w = self.focusBtn.bounds.size.width + 10;
        
        [self.focusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.right.equalTo(self.contentView).offset(-15);
            make.width.equalTo(@(w));
        }];
        
        
    }
    return self;
}

-(void)setModel:(SearchTheUserModel *)model{
    _model = model;
    
    if (IS_VALID_STRING(model.headUrl)) {
        [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.headUrl] placeholderImage:kImage(@"测试头像") completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (error) {
                self.portraitImageV.image = kImage(@"测试头像");
            }
            
        }];
    }
//    [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.headUrl]];
    if (IS_VALID_STRING(model.nickName)) {
        self.nameLabel.text = model.nickName;
    }
    
    self.IDLabel.text = [NSString stringWithFormat:@"ID:%@",model.sysId];
    if (model.follow == 0) {
        self.focusBtn.selected = NO;
        self.focusBtn.backgroundColor = RGBA(52, 120, 245, 1);
    }else{
        self.focusBtn.selected = YES;
        self.focusBtn.backgroundColor = RGBA(255, 255, 255, 1);
    }
    
}

- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = kImage(@"测试头像");
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"小姐妹";
        _nameLabel.textColor = RGBA(51, 51, 51, 1);
        _nameLabel.font = kFont_Medium(16);
        _nameLabel.textAlignment = 0;
    }
    return _nameLabel;
}
- (UILabel *)IDLabel{
    if (!_IDLabel) {
        _IDLabel = [[UILabel alloc]init];
        _IDLabel.text = @"ID:56641614";
        _IDLabel.textColor = RGBA(153, 153, 153, 1);
        _IDLabel.font = kFont_Medium(14);
        _IDLabel.textAlignment = 0;
    }
    return _IDLabel;
}
- (UIButton *)focusBtn{
    if (!_focusBtn) {
        _focusBtn = [[UIButton alloc]init];
        [_focusBtn setTitle:@"+关注" forState:(UIControlStateNormal)];
        [_focusBtn setTitleColor:RGBA(255, 255, 255, 1) forState:(UIControlStateNormal)];
        [_focusBtn setTitle:@"已关注" forState:(UIControlStateSelected)];
        [_focusBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateSelected)];
        _focusBtn.titleLabel.font = kFont_Regular(14);
        [_focusBtn addTarget:self action:@selector(focusBtnClick) forControlEvents:UIControlEventTouchUpInside];
        _focusBtn.layer.cornerRadius = 6;
        _focusBtn.clipsToBounds = YES;
        _focusBtn.layer.borderColor = RGBA(52, 120, 245, 1).CGColor;
        _focusBtn.layer.borderWidth = 0.5;
    }
    return _focusBtn;
}
- (void)focusBtnClick {
    if (self.focusBtn.selected) {
        return;
    }
    if (self.block) {
        self.block(self.model.ID, self.index);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
