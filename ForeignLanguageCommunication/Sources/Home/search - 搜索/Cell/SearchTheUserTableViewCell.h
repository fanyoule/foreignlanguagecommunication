//
//  SearchTheUserTableViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import <UIKit/UIKit.h>
#import "SearchTheUserModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^SearchTheUserTableViewCellBlock)(long userId, NSInteger index);
@interface SearchTheUserTableViewCell : UITableViewCell
/** 头像*/
@property (nonatomic, strong)UIImageView *portraitImageV;
/** 名字*/
@property (nonatomic, strong)UILabel *nameLabel;
/** ID*/
@property (nonatomic, strong)UILabel *IDLabel;
/** 关注按钮*/
@property (nonatomic, strong)UIButton *focusBtn;
@property (nonatomic, assign) NSInteger index;

@property (nonatomic, strong)SearchTheUserModel *model;
@property (nonatomic, copy) SearchTheUserTableViewCellBlock block;
@end

NS_ASSUME_NONNULL_END
