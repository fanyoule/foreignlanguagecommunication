//
//  SearchGroupPageCollectionViewCell.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import <UIKit/UIKit.h>
#import "SearchForCrowdModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SearchGroupPageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong)UIImageView *portraitImageV;

@property (nonatomic, strong)UILabel *titleLabel;

@property (nonatomic, strong)UILabel *groupIDLabel;

@property (nonatomic, strong)UIButton *applyBtn;

@property (nonatomic, strong)SearchForCrowdModel *model;
@end

NS_ASSUME_NONNULL_END
