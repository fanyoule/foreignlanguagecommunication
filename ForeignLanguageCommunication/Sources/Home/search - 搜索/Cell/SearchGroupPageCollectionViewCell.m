//
//  SearchGroupPageCollectionViewCell.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import "SearchGroupPageCollectionViewCell.h"

@implementation SearchGroupPageCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        
        [self addControls];
        
    }
    return self;
}
- (void)addControls{
    [self.contentView addSubview:self.portraitImageV];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.groupIDLabel];
    [self.contentView addSubview:self.applyBtn];
    
    [self.portraitImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(10);
        make.width.height.equalTo(@50);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.portraitImageV.mas_right).offset(8);
        make.top.equalTo(self.portraitImageV).offset(3);
    }];
    [self.groupIDLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.portraitImageV).offset(-3);
        make.left.equalTo(self.portraitImageV.mas_right).offset(8);
    }];
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    
}

- (void)setModel:(SearchForCrowdModel *)model{
    if (IS_VALID_STRING(model.avatarUrl)) {
        [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.avatarUrl] placeholderImage:kImage(@"测试头像") completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (error) {
                self.portraitImageV.image = kImage(@"测试头像");
            }
        }];
//        [self.portraitImageV sd_setImageWithURL:[NSURL URLWithString:model.avatarUrl]];
    }
    if (IS_VALID_STRING(model.clusterName)) {
        self.titleLabel.text = model.clusterName;
    }
    
    self.groupIDLabel.text = [NSString stringWithFormat:@"群ID:%ld",model.clusterSysId];
    
    
}


- (UIImageView *)portraitImageV{
    if (!_portraitImageV) {
        _portraitImageV = [[UIImageView alloc]init];
        _portraitImageV.image = kImage(@"测试头像");
        _portraitImageV.layer.cornerRadius = 6;
        _portraitImageV.clipsToBounds = YES;
    }
    return _portraitImageV;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"群名字";
        _titleLabel.textColor = RGBA(24, 24, 24, 1);
        _titleLabel.font = kFont_Medium(18);
        _titleLabel.textAlignment = 0;
    }
    return _titleLabel;
}

- (UILabel *)groupIDLabel{
    if (!_groupIDLabel) {
        _groupIDLabel = [[UILabel alloc]init];
        _groupIDLabel.text = @"群ID:546854";
        _groupIDLabel.textColor = RGBA(153, 153, 153, 1);
        _groupIDLabel.font = kFont_Medium(14);
        _groupIDLabel.textAlignment = 0;
    }
    return _groupIDLabel;
}

- (UIButton *)applyBtn{
    if (!_applyBtn) {
        _applyBtn = [[UIButton alloc]init];
        [_applyBtn setTitle:@"申请" forState:(UIControlStateNormal)];
        [_applyBtn setTitleColor:RGBA(52, 120, 245, 1) forState:(UIControlStateNormal)];
        _applyBtn.titleLabel.font = kFont(14);
    }
    return _applyBtn;
}

@end
