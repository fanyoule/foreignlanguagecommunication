//
//  GroupInformationDetailsModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//  群信息

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupInformationDetailsModel : NSObject
/** 是否需验证入群(【个人群0任何人都可以加入，1需要发送验证消息，2需要回答问题)】【公会群0任何人都可以加入，1需要发送验证消息】*/
@property (nonatomic, assign)NSInteger auth;
/** 验证答案*/
@property (nonatomic, strong)NSString *answer;
/** 群成员*/
@property (nonatomic, assign)NSArray *clusterMember;
/** 群介绍*/
@property (nonatomic, strong)NSString *content;
/** 群人数*/
@property (nonatomic, assign)NSInteger count;
/** 0:已在群，1：待审核，2：未在群*/
@property (nonatomic, assign)NSInteger userStatus;

/** 头像*/
@property (nonatomic, strong)NSString *url;

/** 昵称*/
@property (nonatomic, strong)NSString *name;
/** 群id*/
@property (nonatomic, assign)NSInteger ID;
/** 验证问题*/
@property (nonatomic, strong)NSString *question;



@end

NS_ASSUME_NONNULL_END
