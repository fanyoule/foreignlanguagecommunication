//
//  GroupMembersModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupMembersModel : NSObject
/** 头像*/
@property (nonatomic, strong)NSString *avatarUrl;
/** 是否群主*/
@property (nonatomic)BOOL chairman;
/** 昵称*/
@property (nonatomic, strong)NSString *nickName;
/** 用户id*/
@property (nonatomic, assign)NSInteger userId;
@end

NS_ASSUME_NONNULL_END
