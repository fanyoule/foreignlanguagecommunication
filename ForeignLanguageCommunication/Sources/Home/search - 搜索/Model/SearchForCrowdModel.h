//
//  SearchForCrowdModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchForCrowdModel : NSObject
/** 入群验证（0任何人都可以加入，1需要发送验证消息，2需要回答问题）*/
@property (nonatomic, assign)NSInteger audit;
/** 入群验证答案*/
@property (nonatomic, strong)NSString *auditAnswer;
/** 入群验证问题*/
@property (nonatomic, strong)NSString *auditQuestion;
/** 群头像id*/
@property (nonatomic, assign)NSInteger avatar;
/** 群头像*/
@property (nonatomic, strong)NSString *avatarUrl;
/** 是否开启主席模式（0关闭，1开启） 管理员能说话，其他人说不了话*/
@property (nonatomic, assign)NSInteger chair;
/** 群介绍*/
@property (nonatomic, strong)NSString *clusterIntro;
/** 群成员*/
@property (nonatomic, strong)NSArray *clusterMember;
/** 群名*/
@property (nonatomic, strong)NSString *clusterName;
/** 群组系统id*/
@property (nonatomic, assign)NSInteger clusterSysId;
/** 创建时间*/
@property (nonatomic, strong)NSString *createTime;
/** 是否管理员*/
@property (nonatomic)BOOL isAdmin;
/** 是否群主*/
@property (nonatomic)BOOL isMaster;
/** 是否免打扰：0关闭 1开启*/
@property (nonatomic, assign)NSInteger isNoDisturbing;
/** 是否屏蔽：0关闭 屏蔽 1开启 不屏蔽*/
@property (nonatomic, assign)NSInteger isShield;
/** 是否置顶：0不置顶 1置顶*/
@property (nonatomic, assign)NSInteger isTop;
/** 创建者id*/
@property (nonatomic, assign)NSInteger masterId;
/** 群人数*/
@property (nonatomic, assign)NSInteger peopleCount;
/** 禁言人数*/
@property (nonatomic, assign)NSInteger silenceCount;
/** 当前用户id*/
@property (nonatomic, assign)NSInteger userId;
/** 群ID*/
@property (nonatomic, assign)NSInteger ID;






@end

NS_ASSUME_NONNULL_END
