//
//  SearchTheUserModel.h
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchTheUserModel : NSObject
/** 是否被搜索者关注:0未关注，1关注*/
@property (nonatomic, assign)NSInteger follow;
/** 头像url*/
@property (nonatomic, strong)NSString *headUrl;
/** 昵称*/
@property (nonatomic, strong)NSString *nickName;
/** 账号*/
@property (nonatomic, strong)NSString *sysId;
/** 用户ID*/
@property (nonatomic, assign)NSInteger ID;

@end

NS_ASSUME_NONNULL_END
