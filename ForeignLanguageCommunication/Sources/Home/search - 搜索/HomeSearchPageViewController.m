//
//  HomeSearchPageViewController.m
//  ForeignLanguageCommunication
//
//  Created by 申修智 on 2021/1/26.
//

#import "HomeSearchPageViewController.h"
#import "SearchTheUserViewController.h"// 用户
#import "SearchTheRoomViewController.h"// 房间
#import "SearchTheCoachViewController.h"// 教练

#import "YKSearchBar.h"
#import "YKSearchTextField.h"
#import <JXCategoryView/JXCategoryView.h>
@interface HomeSearchPageViewController ()<UIScrollViewDelegate,SearchViewDelegate,JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;
@property (nonatomic, strong) JXCategoryIndicatorLineView *indicatorLine;

@property (nonatomic, strong) JXCategoryListContainerView *listView;



/** 判断搜索类型*/
@property (nonatomic, assign)NSInteger type;
@end

@implementation HomeSearchPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpNavView];
    [self setUpSubView];
    
    
    
}

- (void)setUpNavView {
    self.navView.backgroundColor = UIColor.whiteColor;
    self.backBtn.hidden = YES;
    [self addRightBtnWith:@"取消"];
    [self.rightBtn setTitleColor:RGBA(52, 120, 245, 1) forState:UIControlStateNormal];
    
    YKSearchTextField *searchBar = [[YKSearchTextField alloc] init];
    searchBar.placeHolder = @"搜索";
    searchBar.layer.cornerRadius = 16;
    searchBar.clipsToBounds = YES;
    searchBar.delegate = self;
    [self.navView addSubview:searchBar];
    
    [searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.right.mas_equalTo(self.rightBtn.mas_left).mas_offset(-15);
        make.height.mas_equalTo(36);
        make.center.mas_equalTo(self.rightBtn);
    }];
    
}


- (void)searchViewDidClickSearchKey:(UITextField *)textfield {
    // search
    NSInteger index = self.mainScrollView.contentOffset.x/ KSW;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HomeSearch" object:nil userInfo:@{@"index":@(index),@"content":textfield.text}];
}


- (void)rightClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
- (void)setUpSubView {
    self.view.backgroundColor = UIColor.whiteColor;

    [self.view addSubview:self.categoryTitleView];
    [self.categoryTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.left.mas_offset(0);
        make.top.mas_equalTo(self.navView.mas_bottom);
        make.height.mas_equalTo(45);
    }];
    
    
    self.categoryTitleView.titles = @[@"用户",@"教练",@"群"];
    self.categoryTitleView.delegate = self;
   
    [self.categoryTitleView selectItemAtIndex:0];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = RGBA(223, 223, 223, 1);
    [self.categoryTitleView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.categoryTitleView);
        make.height.equalTo(@0.5);
    }];
    
    [self.view addSubview:self.listView];
    self.listView.frame = CGRectMake(0, SNavBarHeight+45, KSW, ScreenHeight-SNavBarHeight-45);
    self.categoryTitleView.listContainer = self.listView;
}

- (JXCategoryTitleView *)categoryTitleView {
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titleColor = UIColor.blackColor;
        _categoryTitleView.titleSelectedColor = RGBA(52, 120, 245, 1);
        _categoryTitleView.titleFont = kFont_Medium(15);
        _categoryTitleView.indicators = @[self.indicatorLine];
        _categoryTitleView.delegate = self;
    }
    return _categoryTitleView;
}

- (JXCategoryIndicatorLineView *)indicatorLine {
    if (!_indicatorLine) {
        _indicatorLine = [[JXCategoryIndicatorLineView alloc] init];
        _indicatorLine.lineStyle = JXCategoryIndicatorLineStyle_Normal;
        _indicatorLine.indicatorWidth = 15;
        _indicatorLine.indicatorHeight = 2;
        _indicatorLine.indicatorColor = RGBA(52, 120, 245, 1);
    }
    return _indicatorLine;
}



- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    
    
    switch (index) {
        case 0: {
            SearchTheUserViewController *userVC = [[SearchTheUserViewController alloc] init];
            self.type = 0;
            return userVC;
        }
            
            break;
        case 1:{
            SearchTheCoachViewController *roomVC = [[SearchTheCoachViewController alloc] init];
            self.type = 1;
            
            return roomVC;
        }
            break;
        case 2:{
            SearchTheRoomViewController *clubVC = [[SearchTheRoomViewController alloc] init];
            self.type = 2;
            
            return clubVC;
        }
            break;
        default:
            break;
    }
    
    return nil;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView{
    
    return 3;
}

#pragma mark -delegate-

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    [self scrollViewDidEndScrollingAnimation:scrollView];
    NSInteger index = scrollView.contentOffset.x/KSW;
    [self.categoryTitleView selectItemAtIndex:index];
    
}


- (JXCategoryListContainerView *)listView {
    if (!_listView) {
        _listView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    }
    return _listView;
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index{
    self.type = index;
    NSLog(@"当前选中的 === %ld",self.type);
}

- (void)searchViewDidEndEdit:(UITextField *)textField {
    if (self.type == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchTheUser" object:textField.text];
        
    }else if (self.type == 1){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchTheCoach" object:textField.text];
        
    }else if (self.type == 2){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchForCrowd" object:textField.text];
        
    }
    
}

- (void)searchViewDIdChangeText:(UITextField *)textField{
    
    
    
}



@end
