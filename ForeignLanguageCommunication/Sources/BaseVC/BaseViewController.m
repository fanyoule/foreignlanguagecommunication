//
//  BaseViewController.m
//  GameLive
//
//  Created by mac-xdd on 2020/8/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "BaseViewController.h"
#import "ETNullDataView.h"

@interface BaseViewController ()
@property (nonatomic, strong) UIButton *leftBtn;


@end

@implementation BaseViewController

-(ETNullDataView *)nullView{
    if (!_nullView) {
        _nullView = [[ETNullDataView alloc]initWithFrame:CGRectMake(0, 0, YL_kScreenWidth, 100)];
        _nullView.hidden = YES;
    }
    return _nullView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    
   
    
    
    // 返回按钮
    self.backBtn = [[UIButton alloc] init];
    self.backBtn.frame = CGRectMake(0, 0, 30, 44);
    UIImage *bImage = [[UIImage imageNamed:@"icon_nav_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [self.backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    self.backBtn.backgroundColor = [UIColor clearColor];
    [self.backBtn setImage:bImage forState:UIControlStateNormal];
    [self.backBtn setTitle:@"" forState:UIControlStateNormal];
    self.backBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    if (self.navigationController.viewControllers.count > 1) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.backBtn];
    }
    
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSLog(@"CurrentViewController===: %@", NSStringFromClass([self class]));
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (void)rightClick:(UIButton *)sender {
    
}

- (void)backClick {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)navView{
    if (!_navView) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        _navView = [UIView new];
        _navView.backgroundColor = UIColor.clearColor;
        [self.view addSubview:_navView];
        //尺寸 包括状态栏高度
        _navView.frame = CGRectMake(0, 0, KSW, SNavBarHeight);
        self.navBottomLine.hidden = YES;
        [_navView addSubview:self.navBottomLine];
        [self.navBottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.mas_offset(0);
            make.height.mas_equalTo(1);
        }];
        
        
        if (self.navigationController.childViewControllers.count > 1) {
            //当前push 栈中控制器层级大于根控制器
            self.backBtn.hidden = NO;
            [_navView addSubview:self.backBtn];
            self.backBtn.frame = CGRectMake(0, SBarHeight, 30, 44);
        }
        else{
            self.backBtn.hidden = YES;
        }
    }
    return _navView;
}
- (UIView *)navBottomLine {
    if (!_navBottomLine) {
        _navBottomLine = UIView.new;
        _navBottomLine.backgroundColor = CustomColor(@"DFDFDF");
        
    }
    return _navBottomLine;
}
- (UILabel *)navTitleLabel{
    if (!_navTitleLabel) {
        _navTitleLabel = UILabel.new;
        _navTitleLabel.textAlignment = NSTextAlignmentCenter;
        _navTitleLabel.font = kFont_Bold(17);
        _navTitleLabel.numberOfLines  =  2;;
        _navTitleLabel.textColor = [UIColor colorWithHexString:@"#222222" alpha:1];
        _navTitleLabel.backgroundColor = UIColor.clearColor;
        [self.navView addSubview:_navTitleLabel];
        //布局尺寸
        [_navTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.navView);
            make.bottom.mas_offset(0);
            make.top.mas_offset(SBarHeight);
        }];
        
    }
    return _navTitleLabel;
}
//设置导航栏标题
- (void)setNavTitleString:(NSString *)navTitleString{
    _navTitleString = navTitleString;
    self.navTitleLabel.text = navTitleString;
}


#pragma mark -lazy load-

- (void)addRightBtnWith:(NSString *)titleStr {
    
    self.rightBtn.hidden = NO;
    [self.rightBtn setTitle:titleStr forState:UIControlStateNormal];
    [self.rightBtn sizeToFit];
    CGFloat w = self.rightBtn.bounds.size.width;
    [self.navView addSubview:self.rightBtn];
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.mas_offset(0);
        make.top.mas_offset(SBarHeight);
        make.width.mas_equalTo(w+30);
    }];
}

- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightBtn.hidden = YES;
        _rightBtn.backgroundColor = [UIColor clearColor];
        [_rightBtn addTarget:self action:@selector(rightClick:) forControlEvents:UIControlEventTouchUpInside];
        _rightBtn.titleLabel.font = kFont_Medium(14);
        [_rightBtn setTitleColor:kTabTextColor forState:UIControlStateNormal];
    }
    
    return _rightBtn;
}



- (UITableView *)mainTableView {
    if (!_mainTableView) {
        _mainTableView = [[UITableView alloc] init];
        _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _mainTableView.backgroundColor = UIColor.clearColor;
        
        _mainTableView.showsVerticalScrollIndicator = NO;
        
        
        _mainTableView.estimatedRowHeight = 100;
        _mainTableView.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11.0, *)) {
            _mainTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else{
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        UITapGestureRecognizer *tableViewGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentTableViewTouchInSide)];

        tableViewGesture.numberOfTapsRequired = 1;

        tableViewGesture.cancelsTouchesInView = NO;

        [self.mainTableView addGestureRecognizer:tableViewGesture];
        
    }
    return _mainTableView;
}


- (UIScrollView *)mainScrollView {
    if (!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc] init];
        _mainScrollView.showsVerticalScrollIndicator = NO;
        _mainScrollView.showsHorizontalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            _mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else self.automaticallyAdjustsScrollViewInsets = NO;
    }
    return _mainScrollView;
}

- (UICollectionView *)mainCollectionView {
    if (!_mainCollectionView) {
        
        _mainCollectionView  = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
        
        _mainCollectionView.showsHorizontalScrollIndicator = NO;
        _mainCollectionView.showsVerticalScrollIndicator = NO;
        
        if (@available(iOS 11.0,*)) {
            _mainCollectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        
    }
    return _mainCollectionView;
}

- (void)commentTableViewTouchInSide{
    [self.view endEditing:YES];
}

- (void)createGradientLayerWithViwe:(UIView *)gradientView colors:(NSArray<UIColor *> *)colors type:(GradientDirectionType)direction{
    
    CAGradientLayer * gradientLayer = [CAGradientLayer layer];
       gradientLayer.frame = gradientView.bounds;
    
    NSMutableArray *mutArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < colors.count; i ++) {
        UIColor *color = colors[i];
        
        [mutArray addObject:(__bridge id)color.CGColor];
    }
    
    gradientLayer.colors = [NSArray arrayWithArray:mutArray];
    switch (direction) {
        case GradientDirectionTypeVertical:
        {
            gradientLayer.startPoint = CGPointMake(0, 0);
            gradientLayer.endPoint = CGPointMake(0, 1);
        }
            break;
        case GradientDirectionTypeHorizontal:{
            gradientLayer.startPoint = CGPointMake(0, 0);
            gradientLayer.endPoint = CGPointMake(1, 0);
        }
            break;
        default:
            break;
    }
       
    
       gradientLayer.locations = @[@0,@1];
    [gradientView.layer insertSublayer:gradientLayer atIndex:0];
//    [gradientView.layer addSublayer:gradientLayer];
    
}

- (void)alertView:(NSString *)title msg:(NSString *)msg btnArrTitles:(NSArray *)strings showCacel:(BOOL)isCancel style:(UIAlertControllerStyle)style block:(alertBlock)block {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:(title && title.length > 0) ? title : nil message:(msg && msg.length > 0) ? msg : nil preferredStyle:style];
    for (int i = 0; i < strings.count; i++) {
        [alert addAction:[UIAlertAction actionWithTitle:strings[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (block) {
                block(i);
            }
        }]];
    }
    if (isCancel) {
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    }
    [self presentViewController:alert animated:YES completion:nil];
}


@end
