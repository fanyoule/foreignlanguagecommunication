//
//  RootTabBarController.h
//  GameLive
//
//  Created by mac-xdd on 2020/8/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface RootTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
