//
//  YKNestedViewController.h
//  VoiceLive
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//
/**管理垂直滑动的时候，顶层与底层滚动视图的手势冲突 临界点状态*/
/**需要继承此类*/
#import "BaseViewController.h"
#import "YKPageCategoryViewController.h"
#import "YKCategoryTableView.h"
NS_ASSUME_NONNULL_BEGIN

@interface YKNestedViewController : BaseViewController
@property (nonatomic, strong) NSArray <YKPageViewController *> *pagesArray;
@property (nonatomic, strong) NSArray <NSString *> *categoryTitles;
@property (nonatomic, strong) YKPageCategoryViewController *pageCategoryController;
/**可视区域高度*/
@property (nonatomic) CGFloat visiableHeight;
//当需要滚动的距离小于 临界点的时候 补偿高度  
@property (nonatomic) CGFloat compensateHeight;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, strong) YKCategoryTableView *myTableView;
@property (nonatomic) BOOL needMidleRefresh;

- (CGFloat)alphaPercentForNavView;


- (void)changeNavAlpha:(CGFloat)percent;
@end

NS_ASSUME_NONNULL_END
