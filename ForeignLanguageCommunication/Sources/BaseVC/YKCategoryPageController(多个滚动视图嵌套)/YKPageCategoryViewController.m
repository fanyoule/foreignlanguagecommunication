//
//  YKPageCategoryViewController.m
//  VoiceLive
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "YKPageCategoryViewController.h"
/**管理左右滑动的代理  选项卡的代理*/
@interface YKPageCategoryViewController ()<YKPageScrollDelegate,JXCategoryViewDelegate>

@end

@implementation YKPageCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addSubViews];
}

#pragma mark init subViews
- (void)addSubViews {
    [self.view addSubview:self.categoryTitleView];
    [self.categoryTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.left.mas_offset(0);
        make.height.mas_equalTo(35);
    }];
    [self addChildViewController:self.pagesManagerController];
    [self.view addSubview:self.pagesManagerController.view];
    self.pagesManagerController.view.backgroundColor = UIColor.clearColor;
    [self.pagesManagerController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.categoryTitleView.mas_bottom);
            make.left.right.bottom.mas_offset(0);
    }];
}
#pragma mark Setter
- (void)setPagesArray:(NSArray<YKPageViewController *> *)pagesArray {
    _pagesArray = pagesArray;
    self.pagesManagerController.pagesArray = pagesArray;
}
- (void)setIsTopStatus:(BOOL)isTopStatus {
    _isTopStatus = isTopStatus;
    [self.pagesArray enumerateObjectsUsingBlock:^(YKPageViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setIsTop:isTopStatus];
    }];
}
#pragma mark Getter
- (YKPageScrollViewController *)pagesManagerController {
    if (!_pagesManagerController) {
        _pagesManagerController = [[YKPageScrollViewController alloc] init];
        _pagesManagerController.delegate = self;
        
    }
    return _pagesManagerController;
}

- (JXCategoryTitleView *)categoryTitleView {
    if (!_categoryTitleView) {
        _categoryTitleView = [[JXCategoryTitleView alloc] init];
        _categoryTitleView.titleColor = UIColor.whiteColor;
        _categoryTitleView.titleSelectedColor = UIColor.blackColor;
        _categoryTitleView.titleFont = kFont_Medium(15);
        _categoryTitleView.indicators = @[self.indicatorLine];
        _categoryTitleView.delegate = self;
    }
    return _categoryTitleView;
}

- (JXCategoryIndicatorLineView *)indicatorLine {
    if (!_indicatorLine) {
        _indicatorLine = [[JXCategoryIndicatorLineView alloc] init];
        _indicatorLine.lineStyle = JXCategoryIndicatorLineStyle_Normal;
        _indicatorLine.indicatorWidth = 15;
        _indicatorLine.indicatorHeight = 2;
        _indicatorLine.indicatorColor = UIColor.whiteColor;
    }
    return _indicatorLine;
}

#pragma mark Public Method

- (void)makePagesCanScroll {
    [self.pagesArray enumerateObjectsUsingBlock:^(YKPageViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.canScroll = YES;
    }];
}

- (void)makePagesScrollToTop {
    [self.pagesArray enumerateObjectsUsingBlock:^(YKPageViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj scrollToTop];
    }];
}

#pragma mark  pagesManager delegate

- (void)pagesLeaveTop {
    if (self.delegate && [self.delegate respondsToSelector:@selector(pageCategoryView:pagesDidLeaveTop:)]) {
        [self.delegate pageCategoryView:self pagesDidLeaveTop:nil];
    }
}
//滑动到对应页面 选项卡选择对应选项
- (void)pagesDidSelectPage:(NSInteger)index {
    
    [self.categoryTitleView selectItemAtIndex:index];
}
//左右滑动  将要开始拖拽  此时禁止垂直滑动
- (void)pagesWillBeginDragging {
    if (self.delegate && [self.delegate respondsToSelector:@selector(pageCategoryViewWillBeginHorizontalDragging:)]) {
        [self.delegate pageCategoryViewWillBeginHorizontalDragging:self];
    }
}

- (void)pagesDidEndDragging {
    if (self.delegate && [self.delegate respondsToSelector:@selector(pageCategoryViewDidEndHorizontalDraggin:)]) {
        [self.delegate pageCategoryViewDidEndHorizontalDraggin:self];
    }
}

#pragma mark category delegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    [self.pagesManagerController setSelectedpage:index];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
