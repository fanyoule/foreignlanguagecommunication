//
//  YKPageCategoryViewController.h
//  VoiceLive
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//
/** 管理segmentView 和  左右滑动VC   负责 两者消息传递 数据源更新等  负责*/
#import "BaseViewController.h"
#import "YKPageScrollViewController.h"
#import <JXCategoryView/JXCategoryView.h>
@class YKPageCategoryViewController;

@protocol YKPageCategoryDelegate <NSObject>

- (void)pageCategoryView:(YKPageCategoryViewController *)pageCategoryController pagesDidLeaveTop:(YKPageViewController *)pageController;

- (void)pageCategoryViewWillBeginHorizontalDragging:(YKPageCategoryViewController *)pageCategory;
- (void)pageCategoryViewDidEndHorizontalDraggin:(YKPageCategoryViewController *)pageCategory;

@end

NS_ASSUME_NONNULL_BEGIN

@interface YKPageCategoryViewController : BaseViewController
@property (nonatomic, weak) id <YKPageCategoryDelegate> delegate;

@property (nonatomic, strong) NSArray <YKPageViewController *> *pagesArray;

@property (nonatomic, strong) JXCategoryTitleView *categoryTitleView;

@property (nonatomic, strong) YKPageScrollViewController *pagesManagerController;

@property (nonatomic, strong) JXCategoryIndicatorLineView *indicatorLine;

@property (nonatomic) BOOL isTopStatus;

/**子控制器 滚动到顶部*/
- (void)makePagesScrollToTop;
/**子控制器滚动视图可以开始滚动*/
- (void)makePagesCanScroll;
@end

NS_ASSUME_NONNULL_END
