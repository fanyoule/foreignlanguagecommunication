//
//  YKPageScrollViewController.h
//  VoiceLive
//
//  Created by mac on 2020/10/30.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//数据展示VC集合
//scrollview 左右滑动的时候，停止底层tableview的滑动  滑动结束 可以开始滑动




#import "BaseViewController.h"

#import "YKPageViewController.h"

@protocol YKPageScrollDelegate <NSObject>

- (void)pagesLeaveTop;
- (void)pagesDidSelectPage:(NSInteger)index;

@optional
- (void)pagesWillBeginDragging;
- (void)pagesDidEndDragging;
@end

NS_ASSUME_NONNULL_BEGIN
/** 只管理子控制器 左右滑动
 */
@interface YKPageScrollViewController : BaseViewController
@property (nonatomic, strong) NSArray <YKPageViewController *> *pagesArray;
@property (nonatomic, weak) id <YKPageScrollDelegate> delegate;
@property (nonatomic) NSInteger original;

- (void)setSelectedpage:(NSInteger)page;
- (void)makePagesScrollTop;
@end

NS_ASSUME_NONNULL_END
