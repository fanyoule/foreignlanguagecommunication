//
//  YKCategoryTableView.h
//  VoiceLive
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
/**允许多个滚动视图响应手势*/
NS_ASSUME_NONNULL_BEGIN

@interface YKCategoryTableView : UITableView

@end

NS_ASSUME_NONNULL_END
