//
//  YKPageViewController.m
//  VoiceLive
//
//  Created by mac on 2020/10/29.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "YKPageViewController.h"

@interface YKPageViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *myScrollView;
@end

@implementation YKPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)scrollToTop {
    if (self.myScrollView) {
        self.myScrollView.contentOffset = CGPointMake(0, 0);
    }
}

- (void)setCanScroll:(BOOL)canScroll {
    _canScroll = canScroll;
    if (self.myScrollView) {
        self.myScrollView.showsVerticalScrollIndicator = canScroll;
        if (!canScroll) {
            [self.myScrollView setContentOffset:CGPointZero];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //可以滚动状态 偏移量小于0  修改状态 通知leaveTop
    //不可以滚动的状态 保持偏移量为0
    self.myScrollView = scrollView;
    if (self.alwaysScroll) {
        return;
    }
    
    
    
    if (self.canScroll) {
        //可以滚动  如果允许中部刷新  如果此时 是中部状态 便宜量小于0 继续    偏移量大于0禁止 允许底层开始滚动 中部状态禁止
        //8-----------------------------不是中部状态  便宜量大于0 继续   小于0 禁止滚动 允许底层滚动
        //-----------不允许中部刷新  偏移量小于0 禁止 允许底层
        //不可以滚动  允许中部刷新  小于0 保持00  大于
        if (self.needMiddleRefresh) {
            if (!self.isTop) {
                if (scrollView.contentOffset.y <= 0) {
                    return;
                }else {
                    self.isTop = YES;
                    self.canScroll = NO;
                    if (self.delegate && [self.delegate respondsToSelector:@selector(pageScrollViewLeaveTop)]) {
                        [self.delegate pageScrollViewLeaveTop];
                    }
                    
                }
            }else {
                if (scrollView.contentOffset.y>0) {
                    return;
                }else {
                    if (self.delegate && [self.delegate respondsToSelector:@selector(pageScrollViewLeaveTop)]) {
                        [self.delegate pageScrollViewLeaveTop];
                    }
                    self.canScroll = NO;
                }
            }
        }else {
            if (scrollView.contentOffset.y < 0) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(pageScrollViewLeaveTop)]) {
                    [self.delegate pageScrollViewLeaveTop];
                }
                self.canScroll = NO;
            }
        }
    }else {
        //无法滚动状态 保持0，0
        scrollView.contentOffset = CGPointZero;
    }
            
            
            
            
            
//            if (scrollView.contentOffset.y <= 0 && !self.isTop) {
//                //可以继续向下滚动
//            }else {
//                self.canScroll = NO;
//                self.isTop = YES;
//                if (self.delegate&&[self.delegate respondsToSelector:@selector(pageScrollViewLeaveTop)]) {
//                [self.delegate pageScrollViewLeaveTop];
//                }
//            }
//        }
//        else{
//            if (scrollView.contentOffset.y<=0) {
//
//                self.canScroll = NO;
//                self.myScrollView.showsVerticalScrollIndicator = NO;
//                if (self.delegate&&[self.delegate respondsToSelector:@selector(pageScrollViewLeaveTop)]) {
//                [self.delegate pageScrollViewLeaveTop];
//
//                }
//            }
//        }
//
//    }else{
//        self.canScroll = NO;//保持偏移量为0
//
//    }
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
