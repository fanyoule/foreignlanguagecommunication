//
//  YKPageScrollViewController.m
//  VoiceLive
//
//  Created by mac on 2020/10/30.
//  Copyright © 2020 mac-yuefu. All rights reserved.
// 

#import "YKPageScrollViewController.h"

@interface YKPageScrollViewController ()<UIScrollViewDelegate,YKPageViewControllerDelegate>
@property (nonatomic) BOOL isManual;//初始化的默认设置，是否代码手动设置默认显示第几页

@end

@implementation YKPageScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isManual = YES;
    self.view.backgroundColor = UIColor.clearColor;
    
    // Do any additional setup after loading the view.
    [self.view addSubview:self.mainScrollView];
    self.mainScrollView.delegate = self;
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.bounces = NO;
    self.mainScrollView.backgroundColor = UIColor.clearColor;
    
    [self.mainScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (self.isManual) {
        //初始化 第一次展示的页面位置
        [self setSelectedpage:self.original];
        self.isManual = NO;
    }
}
//MARK: Setter
- (void)setPagesArray:(NSArray<YKPageViewController *> *)pagesArray {
    _pagesArray = pagesArray;
    [pagesArray enumerateObjectsUsingBlock:^(YKPageViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.delegate = self;
        [self addChildViewController:obj];
    }];
    self.mainScrollView.contentSize = CGSizeMake(pagesArray.count*self.view.width, 0);
}

//MARK: pageVC delegate 离开临界状态
- (void)pageScrollViewLeaveTop{
    if (self.delegate && [self.delegate respondsToSelector:@selector(pagesLeaveTop)]) {
        [self.delegate pagesLeaveTop];
    }
}
#pragma mark scrollview delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(pagesWillBeginDragging)]) {
        [self.delegate pagesWillBeginDragging];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger index = scrollView.contentOffset.x / self.view.width;
    //滑动
    [self scrollViewDidEndScrollingAnimation:scrollView];
    //通知当前滑动到第几个page
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(pagesDidSelectPage:)]) {
        [self.delegate pagesDidSelectPage:index];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(pagesDidEndDragging)]) {
        [self.delegate pagesDidEndDragging];
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 防止连续快速滑动
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scrollViewDidEndScrollingAnimation:) object:nil];
    [self performSelector:@selector(scrollViewDidEndScrollingAnimation:) withObject:nil afterDelay:0.1];
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scrollViewDidEndScrollingAnimation:) object:nil];
    //手动
    NSInteger index = scrollView.contentOffset.x / KSW;
    YKPageViewController *vc = self.childViewControllers[index];
    if (vc.isViewLoaded) {
        return;
    }
    [scrollView addSubview:vc.view];
    vc.view.frame = CGRectMake(self.view.width*index, 0, KSW, self.view.height);
    [vc didMoveToParentViewController:self];
}

#pragma mark public Method
- (void)setSelectedpage:(NSInteger)page{
    [self.mainScrollView setContentOffset:CGPointMake(page * self.view.width, 0)];
    [self scrollViewDidEndScrollingAnimation:self.mainScrollView];
}
- (void)makePagesScrollTop{
    [self.pagesArray enumerateObjectsUsingBlock:^(YKPageViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.isViewLoaded) {
            [obj scrollToTop];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
