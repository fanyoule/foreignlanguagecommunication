//
//  YKNestedViewController.m
//  VoiceLive
//
//  Created by mac on 2020/11/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//
//canscroll = !soncanscroll  top
//>0 <critical    soncanscroll updown
//<0 down     up >0    down >0 || up <0  canscroll
#import "YKNestedViewController.h"

@interface YKNestedViewController ()<UIScrollViewDelegate, YKPageCategoryDelegate>
@property (nonatomic) BOOL canScroll;//本控制器内只控制何时不能滑动  可以滑动的时机由子控制器的滚动视图决定
@property (nonatomic) CGFloat percentBetweenCriticalPoint;
//子控制器是否处于顶部临界点状态
@property (nonatomic) BOOL isTop;
@end

@implementation YKNestedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addSubViews];
    self.canScroll = YES;
    self.percentBetweenCriticalPoint = 0;
}
- (void)addSubViews {
    [self.view addSubview:self.myTableView];
    [self addChildViewController:self.pageCategoryController];
    [self.footerView addSubview:self.pageCategoryController.view];
    
    
}

#pragma mark Setter
- (void)setPagesArray:(NSArray<YKPageViewController *> *)pagesArray {
    _pagesArray = pagesArray;
    self.pageCategoryController.pagesArray = pagesArray;
    
}
- (void)setCategoryTitles:(NSArray<NSString *> *)categoryTitles {
    _categoryTitles = categoryTitles;
    self.pageCategoryController.categoryTitleView.titles = categoryTitles;
}

- (void)setVisiableHeight:(CGFloat)visiableHeight {
    _visiableHeight = visiableHeight;
    self.footerView.height = visiableHeight;
}
#pragma mark Getter
- (YKCategoryTableView *)myTableView {
    if (!_myTableView) {
        _myTableView = [[YKCategoryTableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if (@available(iOS 11.0, *)) {
            _myTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }else self.automaticallyAdjustsScrollViewInsets = NO;
        
        _myTableView.tableHeaderView = self.headerView;
        _myTableView.tableFooterView = self.footerView;
        _myTableView.showsVerticalScrollIndicator = NO;
    }
    return _myTableView;
}

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [UIView new];
        _headerView.frame = CGRectMake(0, 0, KSW, kScaleSize(217));
    }
    return _headerView;
}
- (UIView *)footerView {
    if (!_footerView) {
        _footerView = [UIView new];
        _footerView.frame = CGRectMake(0, 0, KSW, ScreenHeight - SNavBarHeight - TabbarHeight - TabbarSafeMargin);
    }
    return _footerView;
}

- (YKPageCategoryViewController *)pageCategoryController {
    if (!_pageCategoryController) {
        _pageCategoryController = [[YKPageCategoryViewController alloc] init];
        _pageCategoryController.delegate = self;
    }
    return _pageCategoryController;
}

#pragma mark scrollview delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.myTableView != scrollView) {
        return;
    }
    CGFloat currentY = scrollView.contentOffset.y;
    CGFloat criticalY = ceil(self.myTableView.contentSize.height)  - self.visiableHeight - self.compensateHeight;
//    if (self.needMidleRefresh) {
//
//        //需要子控制器在中间下拉刷新
//        //当超出滚动范围的时候，停止滚动并维持临界状态
//        //子控制器在中间范围不能滚动
//        if (currentY <0) {
//            self.canScroll = NO;
//            scrollView.contentOffset = CGPointMake(0, 0);
//
//            //允许子控制器继续向下滚动 且是在中间位置
//            //子控制器 scrolltotop
//            [self.pageCategoryController setIsTopStatus:NO];
//            [self.pageCategoryController makePagesCanScroll];
//
//        }else if (currentY - criticalY >= FLT_EPSILON){
//            self.canScroll = NO;
//            scrollView.contentOffset = CGPointMake(0, criticalY);
//            //允许子控制器继续向上滚动 且此时在top位置
//            [self.pageCategoryController setIsTopStatus:YES];
//            [self.pageCategoryController makePagesCanScroll];
//        }else{
//
//        }
//
//    }
//    else{
    if (self.canScroll) {
        if (currentY - criticalY >= FLT_EPSILON) {
            //需要悬停 子控制 开始滚动
            self.canScroll = NO;
            [self.pageCategoryController makePagesCanScroll];
            scrollView.contentOffset = CGPointMake(0, criticalY);
        }else if ( currentY <= 0){
            
            //小于0  如果要求中部刷新  停止滚动 子控制 开始滚动 并告诉子控制器现在是中部状态
            if (self.needMidleRefresh) {
                self.canScroll = NO;
                self.pageCategoryController.isTopStatus = NO;
                [self.pageCategoryController makePagesCanScroll];
                scrollView.contentOffset = CGPointZero;
            }//如果不要求 可以继续滑动
        }else {
            // 此时子控制器无法滑动  全部处于top状态
            [self.pageCategoryController makePagesScrollToTop];
        }
    }
    else {
        //不能滑动的状态   如果偏移量小于0
        if (currentY <= 0) {
            
            scrollView.contentOffset = CGPointZero;
        }else if (currentY - criticalY >= FLT_EPSILON){
            //大于临界点的状态 允许中部刷新的话 就允许子控制器滚动
            scrollView.contentOffset = CGPointMake(0, criticalY);
        }
        else{
            //此时不能滑动 但是已经开始要求向上滑动先维持当前的状态
            if (self.pageCategoryController.isTopStatus) {
                
            }
        }
    }
//    if (currentY - criticalY >= FLT_EPSILON) {
//        //大于等于 趋向0的值  维持临界点
//        scrollView.contentOffset = CGPointMake(0, criticalY);
//        [self.pageCategoryController makePagesCanScroll];
//        self.canScroll = NO;
//        
//    }else {
//        if (self.canScroll) {
//            // 未到达临界点位置 子控制器 回到顶部
//            if (self.needMidleRefresh) {
//                if (scrollView.contentOffset.y<0) {
//                    self.canScroll = NO;
//                    scrollView.contentOffset = CGPointMake(0, 0);
//                }else{
//                    [self.pageCategoryController makePagesScrollToTop];
//                }
//                
//            }else{
//                [self.pageCategoryController makePagesScrollToTop];
//            }
//            
//        }else {
//            if (self.needMidleRefresh) {
//                if (scrollView.contentOffset.y <= 0) {
//                    [self.pageCategoryController makePagesCanScroll];
//                    [self.pageCategoryController setIsTopStatus:NO];
//                }
//                
//                return;
//            }
//            //未到达临界点 且此时还不能滚动 那么维持这个状态
//            scrollView.contentOffset = CGPointMake(0, criticalY);
//        }
//    }
//    }
    
    self.percentBetweenCriticalPoint = scrollView.contentOffset.y / criticalY;
    if (self.percentBetweenCriticalPoint < 0) {
        self.percentBetweenCriticalPoint = 0.0;
    }else if (self.percentBetweenCriticalPoint >1.0) {
        self.percentBetweenCriticalPoint = 1.0;
    }
    
    [self changeNavAlpha:self.percentBetweenCriticalPoint];
}

#pragma mark page category delegate
- (void)pageCategoryView:(YKPageCategoryViewController *)pageCategoryController pagesDidLeaveTop:(YKPageViewController *)pageController {
    //离开临界点位置  可以开始滚动
    self.canScroll = YES;
    
}
- (void)pageCategoryViewDidEndHorizontalDraggin:(YKPageCategoryViewController *)pageCategory {
    self.myTableView.scrollEnabled = YES;
}
- (void)pageCategoryViewWillBeginHorizontalDragging:(YKPageCategoryViewController *)pageCategory {
    self.myTableView.scrollEnabled = NO;
}

#pragma mark public method

- (CGFloat)alphaPercentForNavView{
    return self.percentBetweenCriticalPoint;
}
- (void)changeNavAlpha:(CGFloat)percent {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
