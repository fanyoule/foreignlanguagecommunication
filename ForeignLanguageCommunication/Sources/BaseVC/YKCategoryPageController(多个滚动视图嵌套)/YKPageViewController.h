//
//  YKPageViewController.h
//  VoiceLive
//
//  Created by mac on 2020/10/29.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//
//最底层展示数据的VC  子控制器需要继承此类
#import "BaseViewController.h"

@protocol YKPageViewControllerDelegate <NSObject>

- (void)pageScrollViewLeaveTop;

@end

NS_ASSUME_NONNULL_BEGIN

@interface YKPageViewController : BaseViewController
@property (nonatomic) BOOL canScroll;
@property (nonatomic,weak) id<YKPageViewControllerDelegate> delegate;
//允许一直可以滚动
@property (nonatomic) BOOL alwaysScroll;
@property (nonatomic) BOOL needMiddleRefresh;
@property (nonatomic) BOOL isTop;
- (void)scrollToTop;
@end

NS_ASSUME_NONNULL_END
