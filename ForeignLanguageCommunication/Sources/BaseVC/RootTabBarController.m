//
//  RootTabBarController.m
//  GameLive
//
//  Created by mac-xdd on 2020/8/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.


#import "RootTabBarController.h"
#import "CustomNavViewController.h"

#import "HomeViewController.h"
#import "SearchCoachViewController.h"
#import "NewsViewController.h"
#import "WorldViewController.h"
#import "MineViewController.h"

#import "LoginVerifyViewController.h"
#import "TheMessageViewController.h"


@interface RootTabBarController ()
@end

@implementation RootTabBarController {
    HomeViewController *homeVC;
    SearchCoachViewController *searchVC;
    NewsViewController *newsVC;
//    TheMessageViewController *newsVC;
    MineViewController *myVC;
    WorldViewController *worldVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initTabViewController];
    [self initTabBarItem];
    [self addNotice];
}
    
- (void)initTabViewController {
    
    homeVC = [[HomeViewController alloc] init];
    CustomNavViewController *homeNav = [[CustomNavViewController alloc] initWithRootViewController:homeVC];
//    searchVC = [[SearchCoachViewController alloc] init];
//    CustomNavViewController *searchNav = [[CustomNavViewController alloc] initWithRootViewController:searchVC];
    worldVC = [[WorldViewController alloc] init];
    CustomNavViewController *worldNav = [[CustomNavViewController alloc] initWithRootViewController:worldVC];
//    newsVC = [[TheMessageViewController alloc] init];
    newsVC = [[NewsViewController alloc] init];
    CustomNavViewController *newsNav = [[CustomNavViewController alloc] initWithRootViewController:newsVC];
    myVC = [[MineViewController alloc] init];
    CustomNavViewController *myNav = [[CustomNavViewController alloc] initWithRootViewController:myVC];
    
    self.viewControllers = @[homeNav, worldNav, newsNav, myNav];
}

- (void)initTabBarItem {
    
    self.tabBar.tintColor = kTabTextColor;
    self.tabBar.unselectedItemTintColor = kC99;
    
    NSArray *tabTitles = @[@"首页", @"世界", @"消息", @"我的"];
    
    NSArray *tabImages = @[@"icon_tab_home", @"icon_tab_world",@"icon_tab_news", @"icon_tab_mine"];
    
    for (int i = 0; i < tabTitles.count; i++) {
        UITabBarItem *tabBarItem = self.tabBar.items[i];
        UIImage *norimg = [[UIImage imageNamed:[NSString stringWithFormat:@"%@",tabImages[i]]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImage *selimg = [[UIImage imageNamed:[NSString stringWithFormat:@"%@_select",tabImages[i]]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

        tabBarItem.title = [NSString stringWithFormat:@"%@",tabTitles[i]];
        tabBarItem.image = norimg;
        tabBarItem.selectedImage = selimg;
        tabBarItem.tag = i;
    }
    UITabBarItem *item = self.tabBar.items[2];
    item.badgeValue = nil;
}

- (void)addNotice{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoginVC) name:@"login" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addItemBadgeValue:) name:@"ChatTotalUnread" object:nil];
}

- (void)addItemBadgeValue:(NSNotification *)notice {
    NSDictionary *dic = notice.object;
    NSNumber *number = dic[@"badge"];
    UITabBarItem *item = self.tabBar.items[2];
    if (number.intValue > 0) {
        item.badgeValue = [NSString stringWithFormat:@"%d",number.intValue];
    }
    else{
        item.badgeValue  = nil;
    }
}

- (void)showLoginVC{
    LoginVerifyViewController *loginVC = [[LoginVerifyViewController alloc] init];
    
    CustomNavViewController *customNav = [[CustomNavViewController alloc] initWithRootViewController:loginVC];
    customNav.modalPresentationStyle = UIModalPresentationFullScreen;
    
    [self presentViewController:customNav animated:YES completion:nil];
}



@end
