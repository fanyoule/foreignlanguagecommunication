//
//  CustomNavViewController.m
//  GameLive
//
//  Created by mac-xdd on 2020/8/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "CustomNavViewController.h"

@interface CustomNavViewController ()<UIGestureRecognizerDelegate,UINavigationControllerDelegate>

@end

@implementation CustomNavViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    
    if([self respondsToSelector:@selector(interactivePopGestureRecognizer)]){
        self.interactivePopGestureRecognizer.delegate = self;
        self.delegate = self;
    }
    self.navigationBar.hidden = YES;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    //如果现在push的不是栈顶控制器，那么久隐藏tabbar工具条 (标注修改)后期加入隐藏动画
    if([self respondsToSelector:@selector(interactivePopGestureRecognizer)]){
        self.interactivePopGestureRecognizer.enabled = NO;
        if(self.viewControllers.count > 0){
            viewController.hidesBottomBarWhenPushed = YES;
        }
        [super pushViewController:viewController animated:animated];
    }
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated{
    if([self.topViewController respondsToSelector:@selector(viewWillBePopToDisappear)]){
        [self.topViewController performSelector:@selector(viewWillBePopToDisappear)];
    }
    UIViewController *controller = [super popViewControllerAnimated:animated];
    return controller;
}

- (nullable NSArray<__kindof UIViewController *> *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if([self.topViewController respondsToSelector:@selector(viewWillBePopToDisappear)]){
        [self.topViewController performSelector:@selector(viewWillBePopToDisappear)];
    }
    NSArray *array = [super popToViewController:viewController animated:animated];
    return array;
}

- (nullable NSArray<__kindof UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated{
    if([self.topViewController respondsToSelector:@selector(viewWillBePopToDisappear)]){
        [self.topViewController performSelector:@selector(viewWillBePopToDisappear)];
    }
    NSArray *array = [super popToRootViewControllerAnimated:animated];
    return array;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if([self respondsToSelector:@selector(interactivePopGestureRecognizer)]){
        
        BOOL allowPopGesture = YES;
        if([self.topViewController respondsToSelector:@selector(allowPopGestureBack)]){
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            allowPopGesture = [self.topViewController performSelector:@selector(allowPopGestureBack)];
#pragma clang diagnostic pop
        }
        self.interactivePopGestureRecognizer.enabled = allowPopGesture;
        
        if(navigationController.viewControllers.count == 1){
            self.interactivePopGestureRecognizer.enabled = NO;
        }
    }
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
