//
//  BaseViewController.h
//  GameLive
//
//  Created by mac-xdd on 2020/8/3.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ETNullDataView.h"

typedef void(^alertBlock)(int index);
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, GradientDirectionType) {
    GradientDirectionTypeVertical,
    GradientDirectionTypeHorizontal,
    
};

@interface BaseViewController : UIViewController

@property (nonatomic,retain) UIButton *backBtn;
@property (nonatomic,retain) UIButton *rightBtn;

- (void)backClick;
- (void)rightClick:(UIButton *)sender;
@property(nonatomic,strong)ETNullDataView * nullView;
@property (nonatomic, strong) UIView *navView;//导航
@property (nonatomic, strong) UILabel *navTitleLabel;//标题
@property (nonatomic, copy) NSString *navTitleString;
@property (nonatomic, strong) UIView *navBottomLine;
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UICollectionView *mainCollectionView;

- (void)addRightBtnWith:(NSString *)titleStr;

- (void)createGradientLayerWithViwe:(UIView *)gradientView colors:(NSArray <UIColor *> *)colors type:(GradientDirectionType)direction;

- (void)alertView:(NSString *)title msg:(NSString *)msg btnArrTitles:(NSArray *)strings showCacel:(BOOL)isCancel style:(UIAlertControllerStyle)style block:(alertBlock)block;
@end

NS_ASSUME_NONNULL_END
