//
//  ForgetPWDViewController.m
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.

#import "CQCountDownButton.h"
#import "ForgetPWDViewController.h"

@interface ForgetPWDViewController ()<UITextFieldDelegate,CQCountDownButtonDelegate,CQCountDownButtonDataSource>
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *verifyTF;
@property (weak, nonatomic) IBOutlet CQCountDownButton *countDownBtn;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwdTF;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation ForgetPWDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navTitleString = @"重置密码";
    self.topConstraint.constant = SNavBarHeight+10;
    [self setupSubView];
}
- (void)setupSubView{
    self.phoneTF.delegate = self;
    self.verifyTF.delegate = self;
    self.passwordTF.delegate = self;
    self.confirmPwdTF.delegate = self;
    self.phoneTF.returnKeyType = UIReturnKeyDone;
    self.verifyTF.returnKeyType =UIReturnKeyDone;
    self.passwordTF.returnKeyType =UIReturnKeyDone;
    self.confirmPwdTF.returnKeyType =UIReturnKeyDone;
    
    self.countDownBtn.delegate = self;
    self.countDownBtn.dataSource = self;
    [self.countDownBtn addTarget:self action:@selector(countDownButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.confirmBtn addTarget:self action:@selector(clickToResetPWD:) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmBtn setBackgroundColor:CustomColor(@"#3478F5")];
}

//MARK: 输入框代理
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.view endEditing:YES];
    if (textField.text.length > 0) {
        
    }
    //判断所有输入框数据
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
//    if (!textField.text.length) {
//        self.confirmBtn.enabled = NO;
//        [self.confirmBtn setBackgroundColor:CustomColor(@"#9ABCFA")];
//    }else{
//    self.confirmBtn.enabled = YES;
    
//    }
    
    return YES;
}



//MARK: 倒计时按钮 代理
- (NSUInteger)startCountDownNumOfCountDownButton:(CQCountDownButton *)countDownButton{
    //设置倒计时总时间
    return 60;
}
//倒计时点击事件
- (void)countDownButtonDidClick:(CQCountDownButton *)countDownButton{
    //发送验证码请求 成功后 开始倒计时
    if (!self.phoneTF.text.length) {
        //请输入手机号
        [self showText:@"请输入手机号"];
        [countDownButton setEnabled:YES];
        return;
    }else if (self.phoneTF.text.length != 11){
        [self showText:@"请输入正确的手机号"];
        [countDownButton setEnabled:YES];
    }else{
        NSLog(@"手机号 === %@",self.phoneTF.text);
        [RequestManager getVerifyCode:self.phoneTF.text withSuccess:^(id  _Nullable response) {
            if ([response[@"status"] integerValue] == 200) {
                [self showText:@"发送成功"];
                NSLog(@"msg--%@",response[@"msg"]);
                [countDownButton startCountDown];
            }else{
                [self showText:response[@"msg"]];
            }
                
        } withFail:^(NSError * _Nullable error) {
            NSLog(@"失败 === %@",error);
        }];
    }
    
}
- (void)setShouldStartCountDown:(BOOL)shouldStartCountDown {
    _shouldStartCountDown = shouldStartCountDown;
    if (shouldStartCountDown) {
        self.countDownBtn.enabled = NO;
        [self.countDownBtn startCountDown];
    }else{
        self.countDownBtn.enabled = YES;
    }
}
- (void)countDownButtonDidEndCountDown:(CQCountDownButton *)countDownButton{
    //倒计时结束的回调
    [countDownButton setEnabled:YES];
    [countDownButton setTitle:@"发送验证码" forState:UIControlStateNormal];
    [countDownButton setTitleColor:[UIColor colorWithHexString:@"#3478F5" alpha:1] forState:UIControlStateNormal];
    countDownButton.titleLabel.font = kFont_Medium(11);
}
- (void)countDownButtonDidStartCountDown:(CQCountDownButton *)countDownButton{
    //倒计时开始的回调
    NSLog(@"开始倒计时");
    
}
- (void)countDownButtonDidInCountDown:(CQCountDownButton *)countDownButton withRestCountDownNum:(NSInteger)restCountDownNum{
    //倒计时进行中的回调
    [countDownButton setEnabled:NO];
    
    NSLog(@"倒计时时间==%ld",restCountDownNum);
    [countDownButton setTitle:[NSString stringWithFormat:@"重新发送(%lds)",restCountDownNum] forState:UIControlStateNormal];
    
    countDownButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [countDownButton setTitleColor:RGBA(52, 120, 245, 1) forState:UIControlStateNormal];
    
}
//MARK: 重置密码
- (void)clickToResetPWD:(UIButton *)sender {
    //请求重置密码
    if (!self.phoneTF.text.length) {
        //请输入手机号
        [self showText:@"请输入手机号"];
        return;
    }else if (!self.verifyTF.text.length){
        //请输入验证码
        [self showText:@"请输入验证码"];
        return;
    }else if (!self.passwordTF.text.length){
        //密码
        [self showText:@"请输入密码"];
        return;
    }else if (!self.confirmPwdTF.text.length){
        //确认密码
        [self showText:@"请确认密码"];
        return;
    }else{
        [RequestManager forgetPwd:self.passwordTF.text phone:self.phoneTF.text vCode:self.verifyTF.text withSuccess:^(id  _Nullable response) {
            [self.navigationController popViewControllerAnimated:YES];
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
    
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
