//
//  InviteCodeViewController.m
//  VoiceLive
//
//  Created by mac on 2020/8/19.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "InviteCodeViewController.h"
#import "FillInfomationViewController.h"
@interface InviteCodeViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UITextField *inputTF;

@end

@implementation InviteCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = UIColor.whiteColor;
    self.topSpace.constant = SNavBarHeight+26;
    self.confirmBtn.layer.cornerRadius = 22;
    [self setUpNavView];
}
- (IBAction)confirm:(id)sender {
    
    if (!self.inputTF.text.length ) {
        [SVProgressHUD showErrorWithStatus:@"请输入邀请码"];
        return;
        
    }
    if (self.inputTF.text.length!=6) {
        [SVProgressHUD showErrorWithStatus:@"请输入正确的邀请码"];
        return;
    }
    FillInfomationViewController *fillVC = [[FillInfomationViewController alloc] init];
    fillVC.inviteCode = self.inputTF.text;
    [self.navigationController pushViewController:fillVC animated:YES];
    
}

- (void)setUpNavView {
    
    [self addRightBtnWith:@"跳过"];
    [self.rightBtn setTitleColor:CustomColor(@"#999999") forState:UIControlStateNormal];
}

- (void)rightClick:(UIButton *)sender {
    //直接跳过 进入下一页填写资料
    
    FillInfomationViewController *fillVC = [[FillInfomationViewController alloc] init];
    [self.navigationController pushViewController:fillVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
