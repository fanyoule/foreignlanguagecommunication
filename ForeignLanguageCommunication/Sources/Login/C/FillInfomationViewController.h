//
//  FillInfomationViewController.h
//  VoiceLive
//
//  Created by mac on 2020/8/19.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FillInfomationViewController : BaseViewController
@property (nonatomic, copy) NSString *inviteCode;
@end

NS_ASSUME_NONNULL_END
