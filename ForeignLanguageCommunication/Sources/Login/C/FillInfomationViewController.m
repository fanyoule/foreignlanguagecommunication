//
//  FillInfomationViewController.m
//  VoiceLive
//
//  Created by mac on 2020/8/19.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "FillInfomationViewController.h"
#import "DatePickerView.h"

//#import "UploadImageModel.h"
@interface FillInfomationViewController ()
@property (weak, nonatomic) IBOutlet UIButton *selectImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;
@property (weak, nonatomic) IBOutlet UITextField *nickNameTF;
@property (weak, nonatomic) IBOutlet UITextField *genderTF;
@property (weak, nonatomic) IBOutlet UITextField *birthDayTF;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (nonatomic, strong) UIImage *avatarImage;
@end

@implementation FillInfomationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpNavView];
    self.avatarView.contentMode = UIViewContentModeCenter;
    self.confirmBtn.layer.cornerRadius = 22;
    self.avatarView.layer.cornerRadius = 30;
    self.avatarView.clipsToBounds = YES;
    [self.selectImageBtn setBackgroundColor:UIColor.clearColor];
    [self.selectImageBtn setImage:[UIImage new] forState:UIControlStateNormal];
//    self.avatarView.image = GetImage(@"icon_relation_add");
}

- (void)setUpNavView {
    self.navTitleString = @"完善资料";
    self.navView.backgroundColor = UIColor.whiteColor;
    
}
//MARK: 选择头像
- (IBAction)selectImage:(id)sender {
//    CMTakePhotosTool *tool = [CMTakePhotosTool takePhotoWithMaxCount:1 target:self needCrop:NO];
//    tool.cancelblock = ^{
//
//    };
//    tool.photoBlock = ^(NSArray<UIImage *> *imagesArray) {
//        //此处选择照片 上传
//        UIImage *image = imagesArray[0];
//        self.avatarImage = image;
//        self.avatarView.image = [Util resizeImageWithImage:image size:CGSizeMake(60, 60)];
//
//    };
}
//MARK: 登录
- (void)logIn{
    
}
//MARK: 选择性别
- (IBAction)selectGender:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"选择性别" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *maleAction = [UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //选择男
        self.genderTF.text = @"男";
    }];
    UIAlertAction *femaleAction = [UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.genderTF.text = @"女";
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:maleAction];
    [alert addAction:femaleAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}
//MARK: 选择生日
- (IBAction)selectBirthday:(id)sender {
    [self.view endEditing:YES];
    DatePickerView *dateView = [[DatePickerView alloc] init];
    [dateView show];
    dateView.timeBlock = ^(NSDate * _Nonnull date, NSString * _Nonnull dateString) {
      //选择时间
        self.birthDayTF.text = dateString;
    };
}
//MARK: 确认
- (IBAction)confirm:(id)sender {
//    [self.view endEditing:YES];
//    //确认信息都已填写
//    if (!self.avatarImage) {
//        //头像
//        return;
//    }
//    if (!self.nickNameTF.text.length) {
//        //昵称
//        return;
//    }
//    if (!self.birthDayTF.text.length) {
//        //生日
//        return;
//    }
//    //先上传头像
//    WeakSelf;
//    [self uploadImage:^(int imageID) {
//        [weakSelf upDateUserInfoWith:imageID];
//    }];
    
}

- (void)uploadImage:(void(^)(int imageID))opt{
    //先上传头像
//        [RequestManager uploadImagesWith:@[self.avatarImage] withSuccess:^(id  _Nullable response) {
//            if (response) {
//                NSArray *dataArray = response[@"data"];
//                UploadImageModel *imagemodel = [[UploadImageModel alloc] mj_setKeyValues:dataArray[0]];
//                if (imagemodel) {
//                    if (opt) {
//                        opt(imagemodel.ID);
//                    }
//                }
//            }
//        } fail:^(NSError * _Nullable error) {
//            NSLog(@"%@",error);
//        } withProgress:^(int64_t bytesRead, int64_t totalBytes) {
//            CGFloat progress = bytesRead / totalBytes;
//            NSLog(@"thread == %@",[NSThread currentThread]);
//        }];
}

- (void)upDateUserInfoWith:(int)imageID {
    //成功之后 上传所有的资料 头像 昵称 性别 生日
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    params[@"avatar"] = @(imageID);
//    params[@"birthday"] = self.birthDayTF.text;
//    params[@"sex"] = [self.genderTF.text isEqualToString:@"男"]?@"1":([self.genderTF.text isEqualToString:@"女"]?@"0":@"1");
//    params[@"nickname"] = self.nickNameTF.text;
//
//    NSString *signStr = @"";
//    params[@"sign"] = signStr;
//    UserModel *model = [[UserFile shareUser]getUserInfo];
//    params[@"addStorageIds"] = @"";
//    params[@"deleteStorageIds"] = @"";
//    params[@"userId"] = @(model.user.ID);
//    [RequestManager upDateUserInfoWithParams:params withSuccess:^(id  _Nullable response) {
//       //成功 返回rootVC 并更新用户信息
//        NSNumber *code = response[@"status"];
//        if (code.intValue == 200) {
//            //成功
//            UserModel *userModel = [[UserFile shareUser] getUserInfo];
//            userModel.user = [[UserInfoModel alloc] mj_setKeyValues:response[@"data"]];
//            NSDictionary *dic = [userModel mj_keyValues];
//            [[UserFile shareUser] saveUserInfo:dic];
//            //返回rootvc 通知已经登录
//            [self goBack];
//        }else {
//
//        }
//    } withFail:^(NSError * _Nullable error) {
//        NSLog(@"%@",error);
//    }];

}

- (void)goBack{
    //登录成功后直接消失
//    [[NSNotificationCenter defaultCenter] postNotificationName:Notice_Change_Root_tab object:nil];
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
//选择照片 不裁剪，重绘  最小边
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
