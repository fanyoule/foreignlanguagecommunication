//
//  LoginVerifyViewController.m
//  VoiceLive
//
//  Created by mac-xdd on 2020/8/4.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//


#import "LoginVerifyViewController.h"

#import "PhoneNumberInputView.h"
#import "ForgetPWDViewController.h"
#import "RootTabBarController.h"
#import "InviteCodeViewController.h"
//#import <YYTextAttribute.h>
//#import <NSAttributedString+YYText.h>

#import <YYText/YYText.h>
#import "ForgetPWDViewController.h"

#import "ServiceAgreementViewController.h"// 用户使用协议
#import "PrivacyPolicyViewController.h"// 隐私协议
@interface LoginVerifyViewController ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) PhoneNumberInputView *inputView;
@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, assign) BOOL isPassword;
@property (nonatomic, strong) YYLabel *pLabel;
@end

@implementation LoginVerifyViewController {
    UILabel *loginTypeLab;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isPassword = NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endAllEdit:)];
    tap.delegate = self;
    //[self.view addGestureRecognizer:tap];
    UIButton *backButton = [UIButton initButtonImageName:@"icon_nav_back"];
    backButton.frame = CGRectMake(20, SBarHeight, 44, 44);
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    UIButton *rightBarBtn = [UIButton initButtonTitleFont:14 titleColor:CustomColor(@"#999999") titleName:@"密码登录"];
    [rightBarBtn setTitle:@"验证码登录" forState:UIControlStateSelected];
    rightBarBtn.frame = CGRectMake(KSW-90, SBarHeight, 100, 44);
    [rightBarBtn addTarget:self action:@selector(rightBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightBarBtn];
    
    [self createSubviews];
    
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (YYLabel *)pLabel{
    if (!_pLabel) {
        _pLabel = [YYLabel new];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:@"登录注册代表同意用户协议、隐私政策" attributes:@{NSForegroundColorAttributeName:CustomColor(@"#222222"),NSFontAttributeName:kFont(12)}];
        
        [text yy_setTextHighlightRange:[[text string] rangeOfString:@"用户协议、隐私政策"] color:CustomColor(@"3196FB") backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            NSLog(@"点击用户协议");
            NSLog(@"隐私协议");
        }];
        
        _pLabel.attributedText = text;
        _pLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _pLabel;
}
- (void)rightBarBtnClicked:(UIButton *)sender {
    [self.view endEditing:YES];
    sender.selected = !sender.isSelected;
    if (sender.isSelected) {
        self.inputView.inpuType = LoginTypePassword;
        loginTypeLab.text = @"密码登录";
        self.isPassword = YES;
    }
    else {
        self.inputView.inpuType = LoginTypeVerfyCode;
        loginTypeLab.text = @"验证码登录";
        self.isPassword = NO;
    }
    
}

- (void)backButtonClicked {
    [self.view endEditing:YES];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//    [self goBack];
}

- (void)createSubviews {
    loginTypeLab = [UILabel initLabelTextFont:27 textColor:CustomColor(@"#333333") title:@"验证码登录"];
    loginTypeLab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:loginTypeLab];
    [loginTypeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(120);
        make.size.mas_equalTo(CGSizeMake(200, 40));
    }];
    
    
    
    
    
     kWeakSelf(self);
    self.inputView = [[PhoneNumberInputView alloc] initWithFrame:CGRectMake(0, 200, KSW, 105) type:LoginTypePassword];
    [self.view addSubview:self.inputView];
    self.inputView.requestCodeBlock = ^{
        [weakself.view endEditing:YES];
        [weakself requestVerifyCode];
    };
    
    self.inputView.inpuType = LoginTypeVerfyCode;
    
   
    self.inputView.inputBlock = ^(NSString * _Nonnull phoneNumber, NSString * _Nonnull password, BOOL isOK) {
    
      //是否激活登录按钮
        if (isOK) {
            //登录按钮可以点击 取出账号密码
            weakself.loginButton.enabled = YES;
           
            [weakself.loginButton setBackgroundColor:[UIColor colorWithHexString:@"#745CF0" alpha:1]];
        }
        else{
            weakself.loginButton.enabled = NO;
           
            [weakself.loginButton setBackgroundColor:[UIColor colorWithHexString:@"#DFDFDF" alpha:1]];
        }
    };
    self.inputView.resetBlock = ^{
        //重置密码
        ForgetPWDViewController *forgetVC = [[ForgetPWDViewController alloc] init];
        [weakself.navigationController pushViewController:forgetVC animated:YES];
    };
    
    YYLabel *protocolLabel = [YYLabel new];
    protocolLabel.userInteractionEnabled = YES;
    [self.view addSubview:protocolLabel];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@"登录注册代表同意用户协议、隐私政策"];

    attrStr.yy_color = CustomColor(@"#222222");
    attrStr.yy_font = kFont(15);
    
    [attrStr yy_setTextHighlightRange:[[attrStr string] rangeOfString:(@"用户协议、")] color:CustomColor(@"#3196FB") backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        NSLog(@"点击用户协议");
        ServiceAgreementViewController *vc = [[ServiceAgreementViewController alloc]init];
        [weakself.navigationController pushViewController:vc animated:YES];
    }];
    [attrStr yy_setTextHighlightRange:[[attrStr string] rangeOfString:(@"隐私政策")] color:CustomColor(@"#3196FB") backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        NSLog(@"点击隐私政策");
        PrivacyPolicyViewController *vc = [[PrivacyPolicyViewController alloc]init];
        [weakself.navigationController pushViewController:vc animated:YES];
    }];
    protocolLabel.attributedText = attrStr;

    protocolLabel.textAlignment = NSTextAlignmentCenter;
    protocolLabel.frame = CGRectMake(0, 310, KSW, 20);
//    [protocolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.mas_offset(0);
//            make.top.mas_equalTo(self.inputView.mas_bottom).mas_offset(5);
//        make.height.mas_equalTo(20);
//    }];
    
    
   
    //登录按钮
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:self.loginButton];
    [self.loginButton setTitle:@"同意协议并登录" forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:1] forState:UIControlStateNormal];
    [self.loginButton setBackgroundColor:[UIColor colorWithHexString:@"#DFDFDF" alpha:1]];
    self.loginButton.titleLabel.font = kFont_Regular(18);
    self.loginButton.layer.cornerRadius = 22.0f;
    self.loginButton.enabled = NO;
    [self.loginButton addTarget:self action:@selector(clickLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(37);
        make.right.mas_offset(-37);
        make.height.mas_equalTo(44);
        make.top.mas_equalTo(self.inputView.mas_bottom).mas_offset(50);
    }];
    
}

- (void)clickLogin:(UIButton *)sender {
    
    if (self.isPassword) {
        [RequestManager loginByPwdWithAccount:self.inputView.phoneString passwd:self.inputView.pwdString loginLat:@"" loginLot:@"" withSuccess:^(id  _Nullable response) {
            
            NSLog(@"userInfo==%@",response);
            if (response) {
                NSNumber *status = response[@"status"];
                if (status.intValue == 200) {
                    //success
                    UserModel *userM = [UserModel mj_objectWithKeyValues:response[@"data"]];
                    UserInfoModel *userInfoM = [UserInfoModel mj_objectWithKeyValues:userM.user];
                    [UserInfoManager saveInfoWithUserModel:userM];
                    
                    NSString *avatar = [[UserInfoManager shared] getUserAvatar];
                    NSLog(@"%@",avatar);
                    //登录成功 登录IM
                    
                    [[ChatIMSend share] sendIMLogin];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didLogin" object:nil];
                    [self goBack];
                }
            }
            
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }else{
        [RequestManager getLoginByVerifyCodeWithPhone:self.inputView.phoneString vcode:self.inputView.pwdString loginLat:@"" loginLot:@"" withSuccess:^(id  _Nullable response) {
            NSLog(@"userInfo==%@",response);
            if (response) {
                NSNumber *status = response[@"status"];
                if (status.intValue == 200) {
                    //success
                    UserModel *userM = [UserModel mj_objectWithKeyValues:response[@"data"]];
                    UserInfoModel *userInfoM = [UserInfoModel mj_objectWithKeyValues:userM.user];
                    [UserInfoManager saveInfoWithUserModel:userM];
                    
                    NSString *avatar = [[UserInfoManager shared] getUserAvatar];
                    NSLog(@"%@",avatar);
                    [[ChatIMSend share] sendIMLogin];
                    [self goBack];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didLogin" object:nil];
                }
            }
        } withFail:^(NSError * _Nullable error) {
            
        }];
    }
    
//    UserModel *model = [[UserFile shareUser] getUserInfo];
//    NSLog(@"userToken==  %@",model.token);
//    //点击登录按钮
//    //查看当前是密码还是验证码
//    if (self.isPassword) {
//        //密码登录
//        [RequestManager loginWithPassword:self.inputView.phoneString withPassword:self.inputView.pwdString withSuccess:^(id  _Nullable response) {
//            if ([self isUserLogin]) {
//                [[ChatIMSend share]sendIMLogin];
//            }
//            if (response) {
//
//                  NSLog(@"responst===%@",response);
//                NSDictionary *dic = response;
//                NSNumber *code = dic[@"status"];
//                if (code.intValue == 200) {
//                    //成功 根据状态到主页/填写资料
//
//                    //保存用户信息
//                    NSDictionary *dic = response[@"data"];
//                    UserFile *file = [UserFile shareUser];
//                    [file saveUserInfo:dic];
//                    UserModel *um = [file getUserInfo];
//                    NSLog(@"tolen===%@",um.token);
//                    //判断是不是第一次
//                    UserModel *model = [file getUserInfo];
//
//                    if (model.firstLogin) {
//                        //填写邀请码
//                        [self goToInvitePage];
//                    }else{
//                        [self goBack];
//                    }
//
//
//                }
//
//                NSString *str = response[@"msg"];
//                NSLog(@"%@",str);
//            }
//        } withFail:^(NSError * _Nullable error) {
//            NSLog(@"%@",error);
//        }];
//    }
//    else {
//        //验证码登录
//        NSString *phone = self.inputView.phoneString;
//        NSString *verifycode = self.inputView.pwdString;
//        [RequestManager loginWithPhoneNumber:phone withAuthCode:verifycode withSuccess:^(id  _Nullable response) {
//            if (response) {
//                if ([self isUserLogin]) {
//                    [[ChatIMSend share]sendIMLogin];
//                }
//                NSNumber *code = response[@"status"];
//                UserModel *model = [[UserModel alloc] mj_setKeyValues:response[@"data"]];
//                if (code.intValue == 200) {
//
//                NSLog(@"responst===%@",response);
//
//
//                    //成功 根据状态到主页/填写资料
//                    NSDictionary *dic = response[@"data"];
//                    UserFile *file = [UserFile shareUser];
//                    [file saveUserInfo:dic];
//                    //判断是不是第一次
//                    UserModel *model = [file getUserInfo];
//                    if (model.firstLogin) {
//                        //填写邀请码
//                        [self goToInvitePage];
//                    }else{
//                        [self goBack];
//                    }
//
//                }
//            }
//                    //[self goBack];
//        } withFail:^(NSError * _Nullable error) {
//
//        }];
//
//
//    }
    
    
}


- (void)requestVerifyCode {
    kWeakSelf(self);
    NSString *phone = self.inputView.phoneString;
    if (!phone||phone.length!=11) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        self.inputView.shouldStartCountDown = NO;
        return;
    }
    
    self.inputView.shouldStartCountDown = YES;
    NSLog(@"手机号 ==== %@",self.inputView.phoneString);
    [RequestManager getVerifyCode:self.inputView.phoneString withSuccess:^(id  _Nullable response) {
        NSLog(@"%@",response);
        NSLog(@"msg--%@",response[@"msg"]);
    } withFail:^(NSError * _Nullable error) {
        NSLog(@"%@",error);
    }];
   /* [RequestManager getLoginAuthCodeRequestWithPhone:phone withSuccess:^(id  _Nullable response) {
       //成功 查看状态码
        if (response) {
            
            NSNumber *code = response[@"status"];
            if (code.intValue == 200) {
                
                
            }
            
            NSString *str = response[@"msg"];
            NSLog(@"%@",str);
        }

    } withFail:^(NSError * _Nullable error) {
        NSLog(@"%@",error);
        self.inputView.shouldStartCountDown = NO;
    }];*/
    
}


- (void)goBack{
    //登录成功后直接消失
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)endAllEdit:(UIGestureRecognizer *)ges{
    [self.view endEditing:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (void)goToInvitePage {
    InviteCodeViewController *inviteVC = [[InviteCodeViewController alloc] init];
    
    [self.navigationController pushViewController:inviteVC animated:YES];
}
- (BOOL)isUserLogin {
    //判断用户当前是否在登录状态
    BOOL isLogin = NO;
    
//    UserInfoModel *userM = [[UserFile shareUser] getUserPrivateInfo];
//    if (userM && userM.ID > 0) {
//        isLogin = YES;
//    }
    return isLogin;
}
@end
