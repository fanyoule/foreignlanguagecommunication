//
//  ForgetPWDViewController.h
//  ForeignLanguageCommunication
//
//  Created by mac on 2021/1/11.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ForgetPWDViewController : BaseViewController
@property (nonatomic, assign) BOOL shouldStartCountDown;
@end

NS_ASSUME_NONNULL_END
