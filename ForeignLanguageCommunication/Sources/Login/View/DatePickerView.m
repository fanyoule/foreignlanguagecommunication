//
//  DatePickerView.m
//  VoiceLive
//
//  Created by mac on 2020/8/6.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "DatePickerView.h"

@interface DatePickerView ()
//@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
//@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
//@property (weak, nonatomic) IBOutlet UIView *backView;
//@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UIButton *confirmBtn;
@property (nonatomic, strong) UIDatePicker *datePicker;

@property (nonatomic, strong) UILabel *titleLabel;


@end
@implementation DatePickerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init{
    if (self = [super init]) {
        [self setUpSubView];
    }
    return self;
}

- (void)setUpSubView{
    self.backgroundColor = [UIColor colorWithHexString:@"#7F7F7F" alpha:0.3];
    self.backView = UIView.new;
    
    self.backView.backgroundColor = UIColor.whiteColor;
    [self addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(220+TabbarSafeMargin);
    }];
    self.cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backView addSubview:self.cancelBtn];
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(10);
        make.left.mas_offset(15);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(30);
    }];
    [self.cancelBtn setTitleColor:RGBA(52, 120, 245, 1) forState:UIControlStateNormal];
    self.confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backView addSubview:self.confirmBtn];
    [self.confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.confirmBtn setTitleColor:RGBA(52, 120, 245, 1) forState:UIControlStateNormal];
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.width.height.mas_equalTo(self.cancelBtn);
        make.right.mas_offset(-15);
    }];
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.text = @"生日";
    self.titleLabel.textColor = RGBA(24, 24, 24, 1);
    self.titleLabel.font = kFont_Medium(16);
    self.titleLabel.textAlignment = 1;
    [self.backView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cancelBtn);
        make.centerX.equalTo(self.backView);
    }];
    
    [self.cancelBtn addTarget:self action:@selector(disappearDatePicker:) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmBtn addTarget:self action:@selector(confirmDate:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.datePicker = [[UIDatePicker alloc] init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    if (@available(iOS 13.4, *)) {
        self.datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    } 
    self.datePicker.maximumDate = [NSDate date];
    [self.backView addSubview:self.datePicker];
    self.datePicker.frame = CGRectMake(0, 50, KSW, 220+TabbarSafeMargin-50);
//    [self.datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.mas_offset(0);
//        make.top.mas_offset(50);
//        make.bottom.mas_offset(0);
//    }];
}

//- (void)awakeFromNib{
//    [super awakeFromNib];
//    [self setUpSubView];
//}

- (void)show{
    UIWindow *keyWindow = [UIApplication sharedApplication].delegate.window;
    [keyWindow addSubview:self];
    self.frame = keyWindow.bounds;
    
}
- (IBAction)disappearDatePicker:(id)sender {
    [self removeView];
}
- (IBAction)confirmDate:(id)sender {
    //确定时间
    NSLog(@"date==%@",self.datePicker.date);
    NSDateFormatter *formmater = [[NSDateFormatter alloc] init];
    [formmater setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [formmater stringFromDate:self.datePicker.date];
    NSLog(@"%@",dateStr);
    
    if (self.timeBlock) {
        self.timeBlock(self.datePicker.date, dateStr);
    }
    
    
    [self removeView];
    
}
- (void)removeView{
    [self removeFromSuperview];
}
@end
