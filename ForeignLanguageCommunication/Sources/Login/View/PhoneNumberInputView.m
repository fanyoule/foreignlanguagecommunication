//
//  PhoneNumberInputView.m
//  VoiceLive
//
//  Created by mac on 2020/8/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import "PhoneNumberInputView.h"
#import "CQCountDownButton.h"

@interface PhoneNumberInputView ()<UITextFieldDelegate,CQCountDownButtonDelegate,CQCountDownButtonDataSource>
@property (nonatomic, strong) UITextField *phoneNumberView;
@property (nonatomic, strong) UITextField *passwordView;
@property (nonatomic, strong) CQCountDownButton *countDownBtn;
/**忘记密码*/
@property (nonatomic, strong) UIButton *forgetBtn;
@end

@implementation PhoneNumberInputView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (UIButton *)forgetBtn {
    if (!_forgetBtn) {
        _forgetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_forgetBtn setTitleColor:CustomColor(@"#3196FB") forState:UIControlStateNormal];
        [_forgetBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
        _forgetBtn.titleLabel.font = kFont(17);
        [_forgetBtn addTarget:self action:@selector(clickResetPwd:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgetBtn;
}
//前往重置密码
- (void)clickResetPwd:(UIButton *)sender {
    if (self.resetBlock) {
        self.resetBlock();
    }
}

- (instancetype)initWithFrame:(CGRect)frame type:(LoginType)type {
    if (self = [super initWithFrame:frame]) {
        _inpuType = type;
        [self setUpSubView];
    }
    return self;
}

- (void)setUpSubView {
    
    [self addSubview:self.phoneNumberView];
    [self addSubview:self.passwordView];
    [self addSubview:self.countDownBtn];
    [self addSubview:self.forgetBtn];
    
    if (self.inpuType == LoginTypeVerfyCode) {
        //验证码登录
        self.countDownBtn.hidden = NO;
        self.forgetBtn.hidden = YES;
    }else{
        self.countDownBtn.hidden = YES;
        self.forgetBtn.hidden = NO;
    }
    //布局
    [self.phoneNumberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(25);
        make.left.mas_offset(38);
        make.right.mas_offset(-38);
        make.height.mas_equalTo(20);
    }];
    UIView *phoneLine = UIView.new;
    [self addSubview:phoneLine];
    phoneLine.backgroundColor = [UIColor colorWithHexString:@"#E7E7E7" alpha:1];
    [phoneLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.leading.trailing.mas_equalTo(self.phoneNumberView);
        make.top.mas_equalTo(self.phoneNumberView.mas_bottom).mas_offset(5);
    }];
    [self.passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.leading.trailing.mas_equalTo(self.phoneNumberView);
        make.top.mas_equalTo(self.phoneNumberView.mas_bottom).mas_offset(30);
        make.bottom.mas_offset(-10);
    }];
    UIView *bottomLine = UIView.new;
    [self addSubview:bottomLine];
    
    bottomLine.backgroundColor = [UIColor colorWithHexString:@"#E7E7E7" alpha:1];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.height.mas_equalTo(phoneLine);
        make.top.mas_equalTo(self.passwordView.mas_bottom).mas_offset(5);
    }];
}
- (UITextField *)phoneNumberView {
    if (!_phoneNumberView) {
        _phoneNumberView = [[UITextField alloc] init];
        _phoneNumberView.delegate = self;
        _phoneNumberView.placeholder = @"输入手机号";
        _phoneNumberView.font = kFont_Medium(17);
        
    }
    return _phoneNumberView;
}

- (UITextField *)passwordView {
    if (!_passwordView) {
        _passwordView = [[UITextField alloc] init];
        _passwordView.delegate = self;
        _passwordView.placeholder = @"输入密码";
        _passwordView.secureTextEntry = YES;
        _passwordView.font = kFont_Medium(17);
    }
    return _passwordView;
}

- (CQCountDownButton *)countDownBtn {
    if (!_countDownBtn) {
        _countDownBtn = [[CQCountDownButton alloc] init];
        _countDownBtn.delegate = self;
        _countDownBtn.dataSource = self;
        [_countDownBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countDownBtn setTitleColor:[UIColor colorWithHexString:@"#3196FB" alpha:1] forState:UIControlStateNormal];
        _countDownBtn.titleLabel.font = kFont_Medium(17);
        
    }
    return _countDownBtn;
}
- (NSUInteger)startCountDownNumOfCountDownButton:(CQCountDownButton *)countDownButton{
    //设置倒计时总时间
    return 60;
}
//倒计时点击事件
- (void)countDownButtonDidClick:(CQCountDownButton *)countDownButton{
    //发送验证码请求 成功后 开始倒计时
    
    if (self.requestCodeBlock) {
        self.requestCodeBlock();
    }
}
- (void)setShouldStartCountDown:(BOOL)shouldStartCountDown {
    _shouldStartCountDown = shouldStartCountDown;
    if (shouldStartCountDown) {
        self.countDownBtn.enabled = NO;
        [self.countDownBtn startCountDown];
    }else{
        self.countDownBtn.enabled = YES;
    }
}
- (void)countDownButtonDidEndCountDown:(CQCountDownButton *)countDownButton{
    //倒计时结束的回调
    [countDownButton setEnabled:YES];
    [countDownButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [countDownButton setTitleColor:[UIColor colorWithHexString:@"#745CF0" alpha:1] forState:UIControlStateNormal];
    countDownButton.titleLabel.font = kFont_Medium(17);
}
- (void)countDownButtonDidStartCountDown:(CQCountDownButton *)countDownButton{
    //倒计时开始的回调
    NSLog(@"开始倒计时");
    
}
- (void)countDownButtonDidInCountDown:(CQCountDownButton *)countDownButton withRestCountDownNum:(NSInteger)restCountDownNum{
    //倒计时进行中的回调
    [countDownButton setEnabled:NO];
    
    NSLog(@"倒计时时间==%ld",restCountDownNum);
    [countDownButton setTitle:[NSString stringWithFormat:@"重新发送(%lds)",restCountDownNum] forState:UIControlStateNormal];
    
    countDownButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [countDownButton setTitleColor:UIColor.lightGrayColor forState:UIControlStateNormal];
    
}

- (void)textFieldDidChangeSelection:(UITextField *)textField {
    //改变文字  判断是否要激活登录按钮
    //判断手机号
    //判断验证码或者密码
    if (self.inpuType == LoginTypePassword) {
        //判断密码
    }else {
        //判断验证码
    }
    
    if (self.inputBlock) {
        BOOL isActive = NO;
        if (self.phoneNumberView.text.length > 0 && self.passwordView.text.length > 0) {
            isActive = YES;
        }
        self.inputBlock(self.phoneNumberView.text, self.passwordView.text, isActive);
    }
}

- (void)setInpuType:(LoginType)inpuType {
    _inpuType = inpuType;//修改是密码登录还是验证码登录
    if (inpuType == LoginTypePassword) {
        //密码登录 隐藏倒计时
        self.countDownBtn.hidden = YES;
        self.phoneNumberView.placeholder = @"输入手机号/悦酷号";
        self.forgetBtn.hidden = NO;
        //密码输入
        [self.passwordView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.leading.mas_equalTo(self.phoneNumberView);
            make.top.mas_equalTo(self.phoneNumberView.mas_bottom).mas_offset(30);
            make.bottom.mas_offset(-10);
            make.trailing.mas_equalTo(self.forgetBtn.mas_leading);
        }];
        [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.passwordView);
            make.trailing.mas_offset(-38);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(20);
        }];
        self.passwordView.secureTextEntry = YES;
    }else {
        //显示倒计时按钮
        
        self.passwordView.secureTextEntry = NO;
        self.countDownBtn.hidden = NO;
        self.forgetBtn.hidden = YES;
        
        [self.passwordView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.leading.mas_equalTo(self.phoneNumberView);
            make.top.mas_equalTo(self.phoneNumberView.mas_bottom).mas_offset(30);
            make.bottom.mas_offset(-10);
            make.trailing.mas_equalTo(self.countDownBtn.mas_leading);
        }];
        [self.countDownBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.passwordView);
            make.trailing.mas_offset(-38);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(20);
        }];
        
    }
    self.phoneNumberView.text = @"";
    self.passwordView.text = @"";
}

- (NSString *)phoneString {
    return self.phoneNumberView.text;
}

- (NSString *)pwdString {
    return self.passwordView.text;
}
@end
