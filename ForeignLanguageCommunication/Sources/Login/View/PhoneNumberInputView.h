//
//  PhoneNumberInputView.h
//  VoiceLive
//
//  Created by mac on 2020/8/5.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**
@param isOK 是否激活登录按钮
*/
typedef void (^changeTextStatusBlock)(NSString *phoneNumber,NSString *password,BOOL isOK);
//发送验证码请求
typedef void(^requestVerifyCodeBlock)(void);
typedef NS_ENUM(NSUInteger, LoginType) {
    LoginTypePassword,//密码登录
    LoginTypeVerfyCode,//验证码登录，显示倒计时按钮
};
//忘记密码
typedef void(^goResetBlock)(void);

@interface PhoneNumberInputView : UIView
@property (nonatomic, assign) LoginType inpuType;
@property (nonatomic, copy) requestVerifyCodeBlock requestCodeBlock;
@property (nonatomic, copy) changeTextStatusBlock inputBlock;//输入框的回调
- (instancetype)initWithFrame:(CGRect)frame type:(LoginType)type;

@property (nonatomic,copy,readonly) NSString *phoneString;
@property (nonatomic, copy, readonly) NSString *pwdString;

@property (nonatomic, assign) BOOL shouldStartCountDown;
@property (nonatomic, copy) goResetBlock resetBlock;

@end

NS_ASSUME_NONNULL_END
