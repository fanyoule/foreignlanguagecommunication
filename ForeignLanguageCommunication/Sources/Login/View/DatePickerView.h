//
//  DatePickerView.h
//  VoiceLive
//
//  Created by mac on 2020/8/6.
//  Copyright © 2020 mac-yuefu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^selectTimeBlock)(NSDate *date,NSString *dateString);
@interface DatePickerView : UIView

@property (nonatomic, copy) selectTimeBlock timeBlock;

- (void)show;

@end

NS_ASSUME_NONNULL_END
